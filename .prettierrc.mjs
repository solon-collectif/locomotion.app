import * as Php from "@prettier/plugin-php";

/** @type {import("prettier").Config} */
const config = {
    trailingComma: "es5",
    arrowParens: "always",
    plugins: [Php],
    overrides: [
        {
            files: "*.blade.php",
            options: {
                parser: "html",
            },
        },
    ],
};
export default config;
