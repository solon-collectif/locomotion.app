<?php

use App\Helpers\Path;

return [
    "system_information" => [
        "languages" => ["fr-CA", "en-CA"],
        "name" => [
            [
                "text" => "LocoMotion",
                "language" => "fr-CA",
            ],
            [
                "text" => "LocoMotion",
                "language" => "en-CA",
            ],
        ],
        "opening_hours" => "24/7",
        "url" => "https://locomotion.app",
        "feed_contact_email" => "dev@locomotion.app",
        "manifest_url" => Path::url("api/gbfs/manifest"),
        "timezone" => "America/Toronto",
        "brand_assets" => [
            "brand_last_modified" => "2017-06-07",
            "brand_image_url" => Path::url("logo.svg"),
            "color" => "#00ADA8",
            "brand_terms_url" =>
                "https://docs.google.com/document/d/1hD0BrEJOSh3_cp_THcTuHqVZDa7OtGTQ6OSZlxYIL4A",
        ],
        "terms_url" => [
            [
                "text" => Path::url(
                    "conditions-generales-participation-programme-locomotion.html"
                ),
                "language" => "fr-CA",
            ],
            [
                "text" => Path::url(
                    "conditions-generales-participation-programme-locomotion.html"
                ),
                "language" => "en-CA",
            ],
        ],
        "terms_last_updated" => "2020-09-10",
        "privacy_url" => [
            [
                "text" => Path::url("politique-confidentialite.html"),
                "language" => "fr-CA",
            ],
            [
                "text" => Path::url("politique-confidentialite.html"),
                "language" => "en-CA",
            ],
        ],
        "privacy_last_updated" => "2020-09-10",
    ],
];
