<?php

return [
    "google" => [
        "client_id" => env("GOOGLE_CLIENT_ID"),
        "client_secret" => env("GOOGLE_CLIENT_SECRET"),
        "redirect" => env("GOOGLE_REDIRECT"),
    ],

    "mailchimp" => [
        "server_prefix" => env("MAILCHIMP_SERVER_PREFIX"),
        "key" => env("MAILCHIMP_KEY"),
        "newsletter_list_id" => env("MAILCHIMP_LIST_ID"),
    ],

    "mailgun" => [
        "domain" => env("MAILGUN_DOMAIN"),
        "secret" => env("MAILGUN_SECRET"),
        "endpoint" => env("MAILGUN_ENDPOINT", "api.mailgun.net"),
    ],

    "postmark" => [
        "token" => env("POSTMARK_TOKEN"),
    ],

    "kiwili" => [
        "enabled" => env("KIWILI_ENABLED", false),
        "base_url" => env("KIWILI_BASE_URL"),
        "api_user_id" => env("KIWILI_API_USER_ID"),
        "auth_token" => env("KIWILI_AUTH_TOKEN"),
        "project_id" => env("KIWILI_PROJECT_ID"),
        "payout_provider_id" => env("KIWILI_PAYOUT_PROVIDER_ID"),
        "payout_tax_profile_id" => env("KIWILI_PAYOUT_TAX_PROFILE_ID"),
        "payout_expense_account_id" => env("KIWILI_PAYOUT_EXPENSE_ACCOUNT_ID"),
    ],

    "ses" => [
        "key" => env("AWS_ACCESS_KEY_ID"),
        "secret" => env("AWS_SECRET_ACCESS_KEY"),
        "region" => env("AWS_DEFAULT_REGION", "us-east-1"),
    ],

    "sparkpost" => [
        "secret" => env("SPARKPOST_SECRET"),
    ],

    "stripe" => [
        "key" => env("STRIPE_KEY", "sk_test"),
        "secret" => env("STRIPE_SECRET", "pk_test"),
    ],
];
