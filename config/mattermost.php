<?php

return [
    "default_channels" => env("MATTERMOST_DEFAULT_CHANNEL"),
    "main_hook_url" => env("MATTERMOST_MAIN_HOOK_URL"),
];
