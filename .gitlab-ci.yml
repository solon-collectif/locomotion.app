# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
  - build
  - test
  - promote

variables:
  IMAGE_TAG: "${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHORT_SHA}"
  CACHE_IMAGE_TAG: $CI_COMMIT_REF_SLUG

build:
  image: docker:latest
  services:
    - docker:dind
  stage: build
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
      # setup docker builder that can export cache
    - docker context create my-builder
    - docker buildx create my-builder --driver docker-container --use
    # Build containers
    - docker compose -f docker-compose.yaml -f ci.compose.yaml build vue php postgres
    - docker compose -f docker-compose.yaml -f ci.compose.yaml push vue php postgres

test:prettier:
  image: node:15
  stage: test
  cache:
    paths:
      - resources/app/node_modules/
  script:
    - npm install
    - npx prettier . --list-different --plugin=@prettier/plugin-php

test:dusk:
  stage: test
  services:
    - docker:dind
  dependencies:
    - build
  image: docker:latest
  script:
    - docker compose -f docker-compose.yaml -f ci.compose.yaml -f dusk.compose.yaml up --quiet-pull --pull=always -d --wait php vue
    # Make sure buckets exist
    - docker compose up --quiet-pull createbuckets
    # Run tests, saving failure
    - docker compose exec php vendor/phpunit/phpunit/phpunit tests/Browser --log-junit browser-test-results.xml || ret=$?
    # Replace the docker container path mappings in the xml output
    - sed -i -e 's/\/var\/www\/html\///g' browser-test-results.xml
    - if [ $ret -ne 0 ]; then echo "Tests failed"; exit 1; fi;
  artifacts:
    paths:
      - ./storage/logs
      - ./tests/Browser/screenshots
      - ./tests/Browser/console
      - browser-test-results.xml
    expire_in: 1 days
    when: always
    reports:
      junit:
        - browser-test-results.xml

test:phpunit:
  stage: test
  services:
    - docker:dind
  dependencies:
    - build
  image: docker:latest
  script:
    - docker compose -f docker-compose.yaml -f ci.compose.yaml up --pull=always --quiet-pull -d php postgres --wait
    - docker compose exec -e DB_DATABASE=locomotion_test -e XDEBUG_MODE=off php php artisan migrate --seed --env testing
    # Run tests, saving failure
    - docker compose exec -e XDEBUG_MODE=coverage php php artisan test --parallel --coverage-cobertura=php-coverage.xml --coverage-text --colors=never --log-junit=test-results.xml || ret=$?
    # Replace the docker container path mappings in the xml output
    - sed -i -e 's/\/var\/www\/html\///g' test-results.xml
    - if [ $ret -ne 0 ]; then echo "Tests failed"; exit 1; fi;
    - docker compose exec php ./vendor/bin/phpstan  analyse --memory-limit 1G
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'
  artifacts:
    expire_in: 3 days
    when: always
    paths:
      - test-results.xml
      - php-coverage.xml
    reports:
      coverage_report:
        coverage_format: cobertura
        path: php-coverage.xml
      junit:
        - test-results.xml

test:jest:
  image: node:16
  stage: test
  cache:
    paths:
      - resources/app/node_modules/
  script:
    - cd resources/app
    - npm install
    - npm run test

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

sast:
  variables:
    SAST_EXCLUDED_ANALYZERS:
      bandit, brakeman, flawfinder, gosec, kubesec, nodejs-scan,
      pmd-apex, security-code-scan, sobelow, spotbugs
    SAST_EXCLUDED_PATHS: spec, test, tests, tmp, vendor, resources/app/node_modules, *.test.js
    DS_MAX_DEPTH: 3
    DS_INCLUDE_DEV_DEPENDENCIES: false # Do not scan dev dependencies
  stage: test

promote:
  stage: promote
  only:
    - main
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - docker pull registry.gitlab.com/solon-collectif/locomotion.app/php:$IMAGE_TAG
    - docker pull registry.gitlab.com/solon-collectif/locomotion.app/vue:$IMAGE_TAG
    - docker pull registry.gitlab.com/solon-collectif/locomotion.app/postgres:$IMAGE_TAG
    - docker image tag registry.gitlab.com/solon-collectif/locomotion.app/php:$IMAGE_TAG registry.gitlab.com/solon-collectif/locomotion.app/php:latest
    - docker image tag registry.gitlab.com/solon-collectif/locomotion.app/vue:$IMAGE_TAG registry.gitlab.com/solon-collectif/locomotion.app/vue:latest
    - docker image tag registry.gitlab.com/solon-collectif/locomotion.app/postgres:$IMAGE_TAG registry.gitlab.com/solon-collectif/locomotion.app/postgres:latest
    - docker push registry.gitlab.com/solon-collectif/locomotion.app/php:latest
    - docker push registry.gitlab.com/solon-collectif/locomotion.app/vue:latest
    - docker push registry.gitlab.com/solon-collectif/locomotion.app/postgres:latest
