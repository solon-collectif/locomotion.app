@aware(["renderText" => false]) @if($renderText) * {{$slot}} @else
<td style="padding: 0 5px; {{isset($width) ? 'width:'.$width.'px' : '' }}">
    {{$slot}}
</td>
@endif
