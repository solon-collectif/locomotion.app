@aware(["renderText" => false]) @if($renderText)
<x-text.message :message="$message" :message-author="$messageAuthor" />
@else @if(!empty($message)) @isset($messageAuthor)
<x-sectiontitle> Message de {{$messageAuthor}} </x-sectiontitle>
@endisset
<x-markdown-content
    style="
        font-weight: 390;
        font-size: 16px;
        line-height: 1.4;
        color: #343a40;
        background: #e5f8f6;
        border: 1px solid #bee6e4;
        border-radius: 8px;
        padding: 12px 20px;
        text-align: left;
        margin-top: 8px;
    "
    :markdown="$message"
/>
@endif @endif
