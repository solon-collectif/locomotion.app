@php use App\Helpers\Formatter; @endphp @aware(["renderText" => false])
@if($renderText)
<x-text.user-invoice :invoice="$invoice" :user="$user" />
@else
<div
    style="
        padding: 16px;
        border-radius: 8px;
        background-color: #f5f8fb;
        border: 1px solid #c9cdd4;
    "
>
    <table style="width: 100%; font-size: 15px">
        <tbody>
            <tr style="align-items: center">
                <td
                    style="
                        text-align: left;
                        vertical-align: top;
                        font-size: 15px;
                    "
                >
                    Réseau LocoMotion<br />
                    6450, Ave Christophe-Colomb<br />
                    Montréal, QC<br />
                    H2S 2G7
                </td>

                <td
                    style="
                        text-align: right;
                        vertical-align: top;
                        font-size: 15px;
                    "
                >
                    {{ $user->full_name }}<br />
                    @foreach($formattedAddress as $addressLine)
                    {{$addressLine}}<br />
                    @endforeach
                </td>
            </tr>
        </tbody>
    </table>

    <table style="width: 100%">
        <tbody>
            <tr>
                <td
                    colspan="3"
                    style="
                        padding: 20px 0;
                        border-bottom: 1px solid #343a40;
                        color: #343a40;
                        font-size: 15px;
                        text-align: start;
                    "
                >
                    <strong class="monospace"
                        >Facture #{{ $invoice->id }} - {{ $invoice->period
                        }}</strong
                    >
                    <br />
                    <span>
                        {{ Formatter::formatDate($invoice->created_at,
                        config("app.default_user_timezone"))}}
                    </span>
                </td>
            </tr>

            <tr>
                <td>
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th
                                    style="
                                        border-bottom: 1px solid #343a40;
                                        padding-bottom: 6px;
                                        color: #343a40;
                                        font-size: 15px;
                                    "
                                >
                                    Description
                                </th>
                                <th
                                    style="
                                        border-bottom: 1px solid #343a40;
                                        padding-bottom: 6px;
                                        color: #343a40;
                                        font-size: 15px;
                                    "
                                >
                                    Montant
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($invoice->billItems as $item)
                            <tr>
                                <td
                                    style="
                                        padding: 4px 0 4px 20px;
                                        text-align: right;
                                        max-width: 200px;
                                        font-size: 15px;
                                    "
                                >
                                    {{ $item->label }}
                                </td>
                                <td
                                    class="monospace"
                                    style="
                                        padding: 4px 0 4px 20px;
                                        text-align: right;
                                        font-size: 15px;
                                    "
                                >
                                    @money($item->amount)
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-size: 15px">
                                    <strong>Sous-total</strong>
                                </td>
                                <td
                                    class="monospace"
                                    style="text-align: right; font-size: 15px"
                                >
                                    @money($invoice->total)
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-size: 15px">
                                    <strong>TPS</strong>
                                </td>
                                <td
                                    class="monospace"
                                    style="text-align: right; font-size: 15px"
                                >
                                    @money($invoice->total_tps)
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-size: 15px">
                                    <strong>TVQ</strong>
                                </td>
                                <td
                                    class="monospace"
                                    style="text-align: right; font-size: 15px"
                                >
                                    @money($invoice->total_tvq)
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; font-size: 15px">
                                    <strong>Total</strong>
                                </td>
                                <td
                                    class="monospace"
                                    style="text-align: right; font-size: 15px"
                                >
                                    @money($invoice->total_with_taxes)
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endif
