@props(["title", "unregistered" => false])
{!!  $title !!}

{!! $slot !!}

L'équipe LocoMotion
info@locomotion.app
@if(!$unregistered)
----
Vous recevez ce courriel parce que vous êtes inscrit sur LocoMotion [{{ config('app.url') . '/' }}]
@endif

