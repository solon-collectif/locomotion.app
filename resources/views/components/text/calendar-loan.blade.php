@if($recipientIsBorrower)
<a href="{{$link}}" target="_blank">Merci de compléter les étapes ou d'annuler l'emprunt sur locomotion.app</a>
@else
<a href="{{$link}}" target="_blank">Voir sur locomotion.app</a>
@endif

@if($recipientIsBorrower)
<strong>Responsables</strong>
    @foreach($loan->loanable->mergedUserRoles as $userRole)
        @if($userRole->show_as_contact)
{{$userRole->user->name}}@if($userRole->title) ({{$userRole->title}})@endif: {{$userRole->user->phone}}
        @endif
    @endforeach

<strong>Instructions</strong>
<a href="{{$link}}" target="_blank">Voir sur locomotion.app</a>

   @if($loan->loanable->location_description)
<strong>Précisions sur l'emplacement</strong>
{{$loan->loanable->location_description}}
   @endif
@else
<strong>Contact</strong>
Nom : {{$loan->borrowerUser->full_name}}
Téléphone : {{$loan->borrowerUser->phone}}
Courriel : {{$loan->borrowerUser->email}}
@endif
