@php use App\Helpers\Formatter; @endphp
LocoMotion
Solon
6450, Ave Christophe-Colomb
Montréal, QC
H2S 2G7

{{ $user->full_name }}
{{ $user->address }}
{{ $user->postal_code }}

Facture #{{ $invoice->id }} - {{ $invoice->period }}
{{Formatter::formatDate($invoice->created_at, config("app.default_user_timezone"))}}

@foreach ($invoice->billItems as $item)
    |{!! str_pad($item->label, 60, ' ', STR_PAD_LEFT) !!} {!! str_pad(Formatter::currency($item->amount), 8,' ', STR_PAD_LEFT) !!}
@endforeach
|
|{{str_pad('Sous-total', 60, ' ', STR_PAD_LEFT)}} {!! str_pad(Formatter::currency($invoice->total), 8,' ', STR_PAD_LEFT) !!}
|{{str_pad('TPS', 60, ' ', STR_PAD_LEFT)}} {!! str_pad(Formatter::currency($invoice->total_tps), 8,' ', STR_PAD_LEFT) !!}
|{{str_pad('TVQ', 60, ' ', STR_PAD_LEFT)}} {!! str_pad(Formatter::currency($invoice->total_tvq), 8,' ', STR_PAD_LEFT) !!}
|{{str_pad('**Total**', 60, ' ', STR_PAD_LEFT)}} {!! str_pad(Formatter::currency($invoice->total_with_taxes), 8,' ', STR_PAD_LEFT) !!}


