@aware(["renderText" => false]) @if($renderText)
<x-text.a :href="$href">{{$slot}}</x-text.a>
@else
<a style="color: #16a59e" href="{{$href}}" target="_blank">{{$slot}}</a>@endif
