@aware(["renderText" => false]) @props(["withBottomMargin" => false])
@if($renderText) {!! $slot !!} @else
<p
    style="text-align: center; margin-bottom: {{$withBottomMargin ? '33px' : 0}}; margin-top: 33px;"
>
    {{ $slot }}
</p>
@endif
