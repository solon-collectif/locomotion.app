@aware(["renderText" => false]) @if($renderText)__{!! $slot !!}__ @else
<u>{!! $slot !!}</u>
@endif
