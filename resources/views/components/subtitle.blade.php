@aware(["renderText" => false]) @if($renderText) ## {!! $slot !!}
<x-br />
@else
<h2
    style="
        color: #229b93;
        font-size: 24px;
        margin-top: 32px;
        margin-bottom: 16px;
        font-weight: 400;
        line-height: 1.5;
    "
>
    {!! $slot !!}
</h2>
@endif
