@aware(["renderText" => false]) @if($renderText) {{$attributes->get("alt")}}
@else
<img {{$attributes}} />
@endif
