@aware(["renderText" => false]) @if($renderText) {!! $slot !!} @else
<div style="text-align: center">{!! $slot !!}</div>
@endif
