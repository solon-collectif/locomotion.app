<!doctype html>
<html>
    <body>
        <p>
            @if($recipientIsBorrower)
            <a href="{{$link}}" target="_blank"
                >Merci de compléter les étapes ou d'annuler l'emprunt sur
                locomotion.app</a
            >
            @else
            <a href="{{$link}}" target="_blank">Voir sur locomotion.app</a>
        </p>
        @endif @if($recipientIsBorrower)
        <p>
            <strong>Responsables</strong><br />

            @foreach($loan->loanable->mergedUserRoles as $userRole)
            @if($userRole->show_as_contact)
            {{$userRole->user->name}}({{$userRole->title}}):
            {{$userRole->user->phone}}<br />
            @endif @endforeach
        </p>
        <p>
            <strong>Instructions</strong><br />
            {{$loan->loanable->instructions}}
        </p>
        @if($loan->loanable->location_description)
        <p>
            <strong>Précisions sur l'emplacement</strong><br />
            {{$loan->loanable->location_description}}
        </p>
        @endif @else
        <p>
            <strong>Contact</strong><br />
            Nom&nbsp;: {{$loan->borrowerUser->full_name}}<br />
            Téléphone&nbsp;: {{$loan->borrowerUser->phone}}<br />
            Courriel&nbsp;: {{$loan->borrowerUser->email}}
        </p>
        @endif
    </body>
</html>
