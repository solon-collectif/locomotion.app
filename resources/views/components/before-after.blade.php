@aware(["renderText" => false]) {{$label}}<x-nbsp />: @if($before ===
null){{$after}}@elseif($before !== $after){{$before}} ➡️<x-nbsp /><x-strong
    >{{$after}}</x-strong
>@else {{$before}} @endif
