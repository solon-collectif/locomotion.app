@aware(["renderText" => false]) @if($renderText) Pour répondre, allez sur
locomotion.app avec le bouton ci-dessus. Ne répondez pas à ce courriel. @else
<p style="text-align: center; margin: 8px 0 0">
    <x-small>
        Pour répondre, allez sur locomotion.app avec le bouton ci-dessus. Ne
        répondez pas à ce courriel.
    </x-small>
</p>
@endif
