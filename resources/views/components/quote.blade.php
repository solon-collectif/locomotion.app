@aware(["renderText" => false]) @if($renderText)
<x-text.quote :message="$text" :author="$author" />
@else
<p style="text-align: center; font-style: italic; margin: 8px 16px 0">
    «&nbsp;{{$text}}&nbsp;»
</p>
<p style="text-align: right; margin-top: 4px; font-size: 15px">- {{$author}}</p>
@endif
