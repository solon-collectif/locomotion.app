@aware(["renderText" => false]) @if($renderText) {!!$slot!!} @else
<span style="font-size: 13px; color: #888; line-height: 20px">{{$slot}}</span>
@endif
