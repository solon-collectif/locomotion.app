@aware(["renderText" => false]) @if($renderText) ** {!! $slot !!} **
<x-br />@else
<h3 style="font-size: 17px; margin-top: 4px; margin-bottom: 8px">
    {!! $slot !!}
</h3>
@endif
