@aware(["renderText" => false]) @if($renderText) ## {!! $slot !!}
<x-br />
@else
<h2
    style="
        font-size: 24px;
        font-style: normal;
        font-weight: normal;
        line-height: 150%;
        letter-spacing: normal;
        text-align: left;
        margin-top: 0;
    "
>
    {!! $slot !!}
</h2>
@endif
