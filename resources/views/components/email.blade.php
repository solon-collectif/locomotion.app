@php use App\Helpers\Formatter; @endphp @props(["center" => false, "title",
"unregistered"=>false, "renderText" => false, "wide" => false, "withDisclaimer"
=> false]) @if($renderText)
<x-text.email :title="$title" :unregistered="$unregistered">
    {!! Formatter::trimEachLine($slot) !!}
</x-text.email>
@else
<html>
    <style type="text/css">
        * {
            font-family: "BrandonText", "-apple-system", "BlinkMacSystemFont",
                "Segoe UI", "Roboto", "Helvetica Neue", "Arial", "Noto Sans",
                "sans-serif", "Apple Color Emoji", "Segoe UI Emoji",
                "Segoe UI Symbol", "Noto Color emoji";
        }

        .monospace {
            font-family: "SFMono-Regular", "Menlo", "Monaco", "Consolas",
                "Liberation Mono", "Courier New", "monospace";
        }

        td {
            padding: 0;
        }
    </style>
    <div style="background-color: #f5f8fb">
        <table style="width: 100%; border-collapse: collapse">
            <tbody>
                <tr>
                    <td style="height: 60px">
                        <div style="text-align: center">
                            <img
                                src="{{ config('app.backend_url_from_browser') . '/mail-header-logo.png' }}"
                                alt="LocoMotion"
                                style="width: 150px; height: 17px"
                            />
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: center">
                        <table
                            style="
                                background-color: white;
                                width: 100%;
                                margin: 0 auto 32px auto;
                                padding: 0 32px 32px 32px;
                                max-width: {{$wide ? '650px': '536px'}};
                                border-radius: 8px;
                                box-shadow:
                                    0 1px 1px rgba(163, 175, 184, 0.6),
                                    0 2px 4px rgba(163, 175, 184, 0.4);
                            "
                        >
                            <tr>
                                <td>
                                    <h1
                                        style="
                                            text-align: center;
                                            font-weight: 420;
                                            margin: 0;
                                            padding: 44px 0 16px;
                                            font-size: 32px;
                                            line-height: 40px;
                                            color: #343a40;
                                        "
                                    >
                                        {{ $title }}
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style="
                                        text-align: {{$center ? 'center' : 'left'}};
                                        font-weight: 390;
                                        font-size: 16px;
                                        line-height: 24px;
                                        color: #343a40;
                                    "
                                >
                                    {{ $slot }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr style="height: 325px">
                    <td
                        style="
            background-position: bottom center;
            background-repeat: no-repeat;
            background-color: #1e4847;
            height: 263.27px;
            background-image: url('{{ config('app.backend_url_from_browser') }}/mail-footer-bg.png');
        "
                    >
                        @if($withDisclaimer)
                        <p
                            style="
                                position: relative;
                                color: white;
                                font-weight: 390;
                                font-size: 13px;
                                line-height: 16px;
                                text-align: center;
                                margin-bottom: 15px;
                                margin-top: 0;
                            "
                        >
                            Vous recevez ce courriel parce que vous avez accepté
                            les conditions d'utilisation à LocoMotion.
                        </p>
                        @endif
                        <p
                            style="
                                position: relative;
                                margin: 0;
                                color: white;
                                font-weight: bold;
                                font-size: 13px;
                                line-height: 16px;
                                text-align: center;
                            "
                        >
                            Envoyé par l'équipe LocoMotion
                        </p>
                        <p
                            style="
                                position: relative;
                                margin: 0;
                                color: white;
                                font-weight: 390;
                                font-size: 13px;
                                line-height: 16px;
                                text-align: center;
                            "
                        >
                            info@locomotion.app
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</html>
@endif
