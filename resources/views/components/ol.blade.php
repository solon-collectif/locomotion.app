@aware(["renderText" => false]) @if($renderText) {!! $slot !!}<x-text.br />
@else
<ol>
    {!! $slot !!}
</ol>
@endif
