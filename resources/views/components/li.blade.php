@aware(["renderText" => false]) @if($renderText)<x-text.li
    >{{ $slot }}</x-text.li
>
@else
<li>{!! $slot !!}</li>
@endif
