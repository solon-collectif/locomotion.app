@aware(["renderText" => false]) @if($renderText) {{$slot}} @else
<table style="margin: auto">
    <tr>
        {{$slot}}
    </tr>
</table>
@endif
