@aware(["renderText" => false]) @if($renderText)
<x-text.p>{!! $slot !!}</x-text.p>
@else
<p>{{$slot}}</p>
@endif
