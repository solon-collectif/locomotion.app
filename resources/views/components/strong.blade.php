@aware(["renderText" => false]) @if($renderText)**{!! $slot !!}**@else
<strong>{!! $slot !!}</strong>
@endif
