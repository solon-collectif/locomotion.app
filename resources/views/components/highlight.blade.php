@aware(["renderText" => false]) @if($renderText) {{$slot}} @else
<table
    style="
        font-weight: 390;
        font-size: 16px;
        line-height: 1.4;
        color: #0b5652;
        background: #d0edec;
        border: 1px solid #bee6e4;
        border-radius: 4px;
        padding: 12px 20px;
        text-align: left;
        margin-top: 8px;
    "
>
    <tr>
        <td>{!! $slot !!}</td>
    </tr>
</table>
@endif
