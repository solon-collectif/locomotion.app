@aware(["renderText" => false]) @if($renderText) {!! $slot !!}<x-text.br />
@else
<ul style="margin: 8px 0 16px">
    {!! $slot !!}
</ul>
@endif
