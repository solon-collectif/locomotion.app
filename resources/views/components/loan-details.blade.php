@php use App\Helpers\Formatter; @endphp @aware(["renderText" => false]) @props([
"loan", "recipientIsBorrower" => false, "newDuration" => null, "initialLoan" =>
[], "showContacts" => true])
<x-sectiontitle>Détails</x-sectiontitle>
<x-ul>
    <x-li>
        <x-before-after
            label="Départ"
            :before="isset($initialLoan['departure_at']) ? Formatter::formatLoanableTime($loan->loanable, $initialLoan['departure_at']) : null"
            :after="Formatter::formatDepartureTime($loan)"
        />
    </x-li>
    <x-li>
        <x-before-after
            label="Retour"
            :before="($newDuration || isset($initialLoan['duration_in_minutes']) || isset($initialLoan['departure_at']))
                ? Formatter::formatReturnTime(
                    $loan,
                    $initialLoan['duration_in_minutes'] ?? $loan->duration_in_minutes,
                    $initialLoan['departure_at'] ?? $loan->departure_at)
                : null"
            :after="Formatter::formatReturnTime($loan, $newDuration ?? $loan->duration_in_minutes)"
        />
        ({{Formatter::formatDuration($newDuration ??
        $loan->duration_in_minutes)}})
    </x-li>
    @if($loan->mileage_end && $loan->mileage_start)
    <x-li>
        <x-before-after
            label="KM au départ"
            :before="isset($initialLoan['mileage_start']) ?  $initialLoan['mileage_start'] . ' KM' : null"
            :after="$loan->mileage_start . ' KM'"
        />
    </x-li>
    <x-li>
        <x-before-after
            label="KM au retour"
            :before="isset($initialLoan['mileage_end']) ?  $initialLoan['mileage_end'] . ' KM' : null"
            :after="$loan->mileage_end . ' KM'"
        />
    </x-li>
    <x-li>
        <x-before-after
            label="Distance"
            :before="isset($initialLoan['actual_distance']) ? $initialLoan['actual_distance'] . ' KM' : null"
            :after="$loan->actual_distance . ' KM'"
        />
    </x-li>

    @else
    <x-li>
        <x-before-after
            label="Distance estimée"
            :before="isset($initialLoan['estimated_distance']) ? $initialLoan['estimated_distance'] . ' KM' : null"
            :after="$loan->estimated_distance . ' KM'"
        />
    </x-li>
    @endif @if($loan->expenses_amount !== null)
    <x-li>
        <x-before-after
            label="Dépenses"
            :before="isset($initialLoan['expenses_amount']) ? Formatter::currency($initialLoan['expenses_amount']) : null"
            :after="Formatter::currency($loan->expenses_amount)"
        />
    </x-li>
    @endif
</x-ul>

@if($showContacts)
<x-text.br />

@if($recipientIsBorrower)
<x-sectiontitle>Responsables</x-sectiontitle>
<x-ul>
    @foreach($loan->loanable->mergedUserRoles as $userRole)
    @if($userRole->show_as_contact)
    <x-li>
        {{$userRole->user->name}} ({{$userRole->title}})<x-nbsp />:
        {{$userRole->user->phone}}
    </x-li>
    @endif @endforeach
</x-ul>
@else
<x-sectiontitle>Contacts</x-sectiontitle>
<x-ul>
    <x-li>Nom<x-nbsp />: {{$loan->borrowerUser->full_name}}</x-li>
    <x-li>Téléphone<x-nbsp />: {{$loan->borrowerUser->phone}}</x-li>
    <x-li>Courriel<x-nbsp />: {{$loan->borrowerUser->email}}</x-li>
</x-ul>

@endif @endif
