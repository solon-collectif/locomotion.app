@aware(["renderText" => false]) @if($renderText) {!! $markdown !!} @else {{--
This style won't work on some email clients, but it's just a small visual
improvement --}}
<style>
    .markdown-content *:last-child {
        margin-bottom: 0;
    }
</style>
<div style="{{$style}}" class="markdown-content">{!! $generatedHtml !!}</div>
@endif
