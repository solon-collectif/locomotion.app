@aware(["renderText" => false]) @props(["lg" => false, "link", "label"])
@if($renderText)
<x-text.a :href="$link">{!! $label !!}</x-text.a>
@else
<a
    href="{{$link}}"
    style="
        display: inline-block;
        background-color: #16a59e;
        padding: 8px 16px ;
        border-radius: 5px;
        color: white;
        font-weight: bold;
        font-size: {{$lg ? '20px' : '17px' }};
        line-height: 1.5;
        text-decoration: none;
    "
    target="_blank"
>
    {{$label}}
</a>
@endif
