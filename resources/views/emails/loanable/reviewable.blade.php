<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        Un nouveau véhicule, <x-loanable-name :loanable="$loanable" />, a été
        publié par {{ $user->full_name }} dans {{ $community->name }} et peut
        être validé.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/admin/loanables/' . $loanable->id"
            label="Voir le véhicule"
        />
    </x-button-section>
</x-email>
