<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Félicitations, votre véhicule
        <x-loanable-name :loanable="$loanable" /> a bien été publié!
    </x-p>

    <x-p>
        Vous recevrez un message quand une personne du voisinage voudra
        l'utiliser.
    </x-p>

    <x-p
        >N'oubliez pas, en tout temps, vous pouvez modifier sa
        disponibilité.</x-p
    >

    <x-button-section>
        <x-button
            :link="config('app.url') . '/profile/loanables/' . $loanable->id"
            label="Voir le véhicule"
        />
    </x-button-section>
</x-email>
