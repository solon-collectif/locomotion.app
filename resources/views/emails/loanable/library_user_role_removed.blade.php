<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $exCoowner->name }},</x-p>

    <x-p>
        {{ $remover->name }} a retiré vos droits de gestion de la flotte {{
        $library->name }}. Vous n'avez maintenant plus accès à la modification
        des informations des véhicule de la flotte, à la modification de leurs
        disponibilités et à la gestion de leurs emprunts.
    </x-p>

    <x-p>
        S'il s'agit d'une erreur, contactez {{ $remover->full_name }}
        directement au {{ $remover->phone }}.
    </x-p>
</x-email>
