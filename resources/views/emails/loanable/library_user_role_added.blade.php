<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $added->name }},</x-p>

    <x-p>
        {{ $adder->name }} vous a donné les droits de gestion de la flotte {{
        $library->name }}! Vous pouvez maintenant modifier les informations des
        véhicule de la flotte, leurs disponibilités et gérer leurs emprunts.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/libraries/' . $library->id"
            label="
        Voir la flotte
    "
        />
    </x-button-section>
</x-email>
