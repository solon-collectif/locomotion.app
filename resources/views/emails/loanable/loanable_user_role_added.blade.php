<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $added->name }},</x-p>

    <x-p>
        {{ $adder->name }} vous a donné les droits de gestion du véhicule
        <x-loanable-name :loanable="$loanable" />! Vous pouvez maintenant
        modifier les informations du véhicule, ses disponibilités et gérer ses
        emprunts.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/profile/loanables/' . $loanable->id"
            label="
        Voir le véhicule
    "
        />
    </x-button-section>
</x-email>
