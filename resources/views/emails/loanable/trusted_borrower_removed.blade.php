<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $exTrustedBorrower->name }},</x-p>

    <x-p>
        {{ $remover->name }} vous a retiré du réseau de confiance pour le
        véhicule <x-loanable-name :loanable="$loanable" />. Vos emprunts futurs
        de ce véhicule devront être approuvés par une personne responsable.
    </x-p>
</x-email>
