<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $exCoowner->name }},</x-p>

    <x-p>
        {{ $remover->name }} a retiré vos droits de gestion du véhicule
        <x-loanable-name :loanable="$loanable" />. Vous n'avez maintenant plus
        accès à la modification des informations du véhicule, à la modification
        de ses disponibilités et à la gestion de ses emprunts.
    </x-p>

    <x-p>
        S'il s'agit d'une erreur, contactez {{ $remover->full_name }}
        directement au {{ $remover->phone }}.
    </x-p>
</x-email>
