<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $added->name }},</x-p>

    <x-p>
        {{ $adder->name }} vous a ajouté au réseau de confiance du véhicule
        <x-loanable-name :loanable="$loanable" />! Vous pouvez maintenant
        l'emprunter plus facilement sans attendre l'approbation d'une personne
        responsable.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/new?loanable_id=' . $loanable->id"
            label="
        Emprunter le véhicule
    "
        />
    </x-button-section>
</x-email>
