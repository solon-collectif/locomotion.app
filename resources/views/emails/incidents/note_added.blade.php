<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        {{$incidentNote->author->name}} a ajouté une mise à jour concernant
        l'incident affectant le véhicule
        <x-loanable-name :loanable="$incidentNote->incident->loanable" />.
    </x-p>

    <x-message
        :message="$incidentNote->text"
        :message-author="$incidentNote->author->name"
    />

    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loanables/' . $incidentNote->incident->loanable_id. '?incident.id='.$incidentNote->incident_id"
            label="Détails de l'incident"
        />

        <x-br />
        <x-small>
            Si vous ne désirez plus recevoir de mise à jour concernant cet
            incident, vous pouvez modifier vos préférences sur la page de
            détails de l'incident.
        </x-small>
    </x-button-section>

    <x-message-disclaimer />
</x-email>
