<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        Un incident affectant le véhicule
        <x-loanable-name :loanable="$incident->loanable" /> a été résolu!
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loanables/' . $incident->loanable_id. '?incident.id='.$incident->id"
            label="Voir le véhicule"
        />
    </x-button-section>
</x-email>
