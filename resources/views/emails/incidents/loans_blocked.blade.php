@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        Un incident (#{{$incident->id}}) qui pourrait vous affecter est en cours
        de résolution pour
        <x-loanable-name :loanable="$incident->loanable" />
        . Malheureusement, cet incident bloque les emprunts jusqu'au {{
        Formatter::formatLoanableTime($incident->loanable,
        $incident->blocking_until) }}. Vous serez avisés lorsque le problème
        sera résolu.
    </x-p>
    @if($blockedLoans->count() > 1)
    <x-p>
        Pour vous assurer de pouvoir effectuer vos trajet, considérez changer la
        date de départ ou annuler vos emprunt et réserver un autre véhicule.
    </x-p>

    <x-p>
        Vos emprunts affectés
        <x-nbsp />
        :

        <x-ul>
            @foreach($blockedLoans as $loan)
            <x-li>
                <x-a :href="config('app.url') . '/loans/'. $loan->id">
                    {{Formatter::formatLoanableTime($loan->loanable,
                    $loan->departure_at)}} ➡️
                    {{Formatter::formatLoanableTime($loan->loanable,
                    $loan->actual_return_at)}}
                </x-a>
            </x-li>
            @endforeach
        </x-ul>
    </x-p>
    @else
    <x-p>
        Pour vous assurer de pouvoir effectuer votre trajet, considérez changer
        la date de départ ou annuler votre emprunt et réserver un autre
        véhicule.

        <x-button-section>
            <x-button
                :link="config('app.url') . '/loans/'. $blockedLoans->first()->id"
                label="Voir mon emprunt bloqué"
            />
        </x-button-section>
    </x-p>
    @endif
</x-email>
