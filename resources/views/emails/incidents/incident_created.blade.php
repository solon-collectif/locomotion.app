@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($loan)
    <x-p>
        @if($reporter->id === $borrowerUser->id) {{ $reporter->name }} a
        rapporté un incident lors de son emprunt de
        <x-loanable-name :loanable="$loanable" /> qui commençait le {{
        Formatter::formatDepartureTime($loan) }}. @else {{ $reporter->name }} a
        rapporté un incident lors de l'emprunt de
        <x-loanable-name :loanable="$loanable" /> par {{$borrowerUser->name}}
        qui commençait le {{ Formatter::formatDepartureTime($loan) }}. @endif
    </x-p>
    @else
    <x-p
        >{{$reporter->name}} a déclaré un incident pour le véhicule
        <x-loanable-name :loanable="$loanable" />
        .
    </x-p>
    @endif

    <x-message
        :message="$incident->comments_on_incident"
        :message-author="$reporter->name"
    />

    @if($loan)
    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
    @else
    <x-button-section>
        <x-button
            :link="config('app.url') . '/loanables/' . $loanable->id . '?incident.id='.$incident->id"
            label="Détails de l'incident"
        />
    </x-button-section>
    @endif
    <x-message-disclaimer />
</x-email>
