<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>
    <x-p>
        {{$reopener->name}} a réouvert un incident pour le véhicule
        <x-loanable-name :loanable="$loanable" />.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loanables/' . $loanable->id. '?incident.id='.$incident->id"
            label="Détails de l'incident"
        />
    </x-button-section>
</x-email>
