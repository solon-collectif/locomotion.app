<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>
    <x-p>
        {{$assignedBy->name}} vous a assigné la gestion de l'incident
        #{{$incident->id}} affectant le véhicule
        <x-loanable-name :loanable="$incident->loanable" />.
    </x-p>

    <x-message
        :message="$incident->comments_on_incident"
        :message-author="$incident->reportedByUser?->name"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loanables/' .$incident->loanable->id . '?incident.id='.$incident->id"
            label="Détails de l'incident"
        />
    </x-button-section>
    <x-message-disclaimer />
</x-email>
