<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        Votre export de données est complété et
        <x-a href="{{$url}}" target="_blank"> est disponible ici</x-a>.
    </x-p>
</x-email>
