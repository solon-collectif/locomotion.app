<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        Vous recevez ce courriel parce que nous avons reçu une demande de
        réinitialisation de mot de passe.
    </x-p>

    <x-p>Ce lien expirera dans {{ $expiration }} heures.</x-p>

    <x-button-section>
        <x-button :link="$route" label="Réinitialiser le mot de passe" />
    </x-button-section>

    <x-p>
        Si vous n'avez pas demandé de réinitialisation de mot de passe. Vous
        pouvez ignorer ce message.
    </x-p>
</x-email>
