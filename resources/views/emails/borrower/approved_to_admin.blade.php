<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        Le dossier de conduite de {{ $user->full_name }} a été validé. Ce membre
        peut maintenant emprunter des autos.
    </x-p>

    <x-button-section>
        <x-button
            label="Voir le profil"
            :link="config('app.url') . '/admin/users/' . $user->id"
        />
    </x-button-section>
</x-email>
