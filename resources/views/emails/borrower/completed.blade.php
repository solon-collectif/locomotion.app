<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Vous avez bien complété votre dossier de conduite. Un-e membre de
        l'équipe va l'examiner. Vous serez informé lorsque votre dossier de
        conduite sera approuvé.
    </x-p>
</x-email>
