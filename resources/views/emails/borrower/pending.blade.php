<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Félicitations, votre dossier de conduite est
        approuvé!@if($isRegistrationSubmitted) Vous devez par contre attendre
        l'approbation de votre preuve de résidence pour pouvoir emprunter des
        véhicules.@else Vous devez par contre soumettre votre preuve de
        résidence pour pouvoir emprunter des véhicules.@endif
    </x-p>
</x-email>
