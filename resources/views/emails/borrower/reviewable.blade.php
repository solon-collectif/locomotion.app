<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Vous pouvez maintenant valider son dossier de conduite.</x-p>
    <x-button-section>
        <x-button
            label="Voir le profil"
            :link="config('app.url') . '/admin/users/' . $user->id . '#borrower'"
        />
    </x-button-section>
</x-email>
