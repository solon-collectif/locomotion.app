<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Félicitations, votre dossier de conduite est approuvé! Vous pouvez
        maintenant emprunter les autos de vos voisins et voisines ;-)
    </x-p>
    <x-button-section>
        <x-button
            label="Voir mon voisinage"
            :link="config('app.url') . '/search/map'"
        />
    </x-button-section>
</x-email>
