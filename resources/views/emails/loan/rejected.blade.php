@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $borrowerUser->name }},</x-p>

    <x-p>
        {{ $rejecter->name }} a refusé votre demande d'emprunt de
        <x-loanable-name :loanable="$loan->loanable" />
        à partir du {{ Formatter::formatDepartureTime($loan) }}.
    </x-p>

    @if(!is_null($comment))
    <x-message :message="$comment" :message-author="$rejecter->name" />
    @endif

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>

    @if(!is_null($comment))
    <x-message-disclaimer />
    @endif
</x-email>
