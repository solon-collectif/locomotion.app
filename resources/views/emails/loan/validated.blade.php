<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $loan->borrowerUser->name }},</x-p>

    <x-p>
        Merci d'avoir participé à LocoMotion pour votre emprunt de
        <x-loanable-name :loanable="$loan->loanable" />!
    </x-p>

    <x-p>
        @if($validator) {{$validator->name}} a validé les données de votre
        emprunt. @endif @if($loan->borrower_must_pay_compensation ||
        $loan->borrower_must_pay_insurance) Vous pouvez maintenant payer
        l'emprunt. @else Vous pouvez modifier votre contribution pour compléter
        l'emprunt. @endif
    </x-p>

    <x-loan-details :loan="$loan" :show-contacts="false" />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
