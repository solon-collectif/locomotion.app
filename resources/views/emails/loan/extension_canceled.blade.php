<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour,</x-p>

    <x-p>
        {{ $loan->borrowerUser->name }} a annulé sa demande de prolongation de
        son emprunt de
        <x-loanable-name :loanable="$loan->loanable" />.
    </x-p>

    <x-loan-details :show-contacts="false" :loan="$loan" />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
