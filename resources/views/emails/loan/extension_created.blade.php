<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour,</x-p>

    <x-p>
        {{ $borrowerUser->name }} a demandé à prolonger son emprunt de
        <x-loanable-name :loanable="$loan->loanable" />.
    </x-p>

    <x-loan-details
        :recipient-is-borrower="false"
        :loan="$loan"
        :new-duration="$loan->extension_duration_in_minutes"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
