@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $receiver->name }},</x-p>

    <x-p>
        L'emprunt de
        <x-loanable-name :loanable="$loan->loanable" />
        du {{ Formatter::formatDepartureTime($loan) }} au {{
        Formatter::formatReturnTime($loan, $loan->duration_in_minutes) }} doit
        être complété en ligne. Veuillez entrer les informations manquantes ou
        annuler l'emprunt s'il n'a pas eu lieu.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
