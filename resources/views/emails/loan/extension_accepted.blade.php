<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        @if($recipientIsBorrower) {{ $accepter->name }} a accepté la
        prolongation de votre emprunt de
        <x-loanable-name :loanable="$loan->loanable" />.
        @elseif($recipientIsAccepter) Vous avez accepté la prolongation de
        l'emprunt de <x-loanable-name :loanable="$loan->loanable" /> par
        {{$borrowerUser->name}}. @else {{ $accepter->name }} a accepté la
        prolongation de l'emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> par
        {{$borrowerUser->name}}. @endif
    </x-p>

    <x-loan-details
        :show-contacts="false"
        :loan="$loan"
        :initial-loan="['duration_in_minutes' => $previousDuration]"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
