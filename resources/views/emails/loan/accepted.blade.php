<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($accepter)
    <x-p>
        @if($recipientIsBorrower)
        <span>
            {{$accepter->name}} a accepté votre demande d'emprunt de
            <x-loanable-name :loanable="$loan->loanable" />. Pour avoir accès au
            véhicule, assurez-vous d'avoir assez d'argent dans votre solde en
            <x-a :href="config('app.url') . '/loans/' . $loan->id"
                >prépayant</x-a
            >.
        </span>
        @else
        <span>
            {{$accepter->name}} a accepté la demande d'emprunt de
            <x-loanable-name :loanable="$loan->loanable" /> par
            {{$loan->borrowerUser->name}}. {{$loan->borrowerUser->name}} doit
            prépayer avant que l'emprunt soit confirmé.
        </span>
        @endif
    </x-p>
    @else
    <p>
        @if($recipientIsBorrower)
        <span>
            Votre demande d'emprunt de
            <x-loanable-name :loanable="$loan->loanable" /> a été acceptée. Pour
            avoir accès au véhicule, assurez-vous d'avoir assez d'argent dans
            votre solde en
            <x-a :href="config('app.url') . '/loans/' . $loan->id"
                >prépayant</x-a
            >.
        </span>
        @else
        <span>
            La demande d'emprunt de
            <x-loanable-name :loanable="$loan->loanable" /> par
            {{$loan->borrowerUser->name}} a été acceptée.
            {{$loan->borrowerUser->name}} doit prépayer avant que l'emprunt soit
            confirmé.
        </span>
        @endif
    </p>
    @endif @if(!is_null($comment))
    <x-message :message="$comment" :message-author="$accepter->name" />
    @endif

    <x-loan-details
        :loan="$loan"
        :recipient-is-borrower="$recipientIsBorrower"
    />

    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loans/' . $loan->id"
            :label="$recipientIsBorrower ? 'Prépayer' : 'Voir l\'emprunt'"
        />
    </x-button-section>

    @if(!is_null($comment))<x-message-disclaimer />@endif
</x-email>
