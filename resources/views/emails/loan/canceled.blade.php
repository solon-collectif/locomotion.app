@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $receiver->name }},</x-p>

    <x-p>
        @if($receiver->is($sender)) Vous avez @else {{ $sender->name }} a @endif
        annulé @if($receiver->id === $loan->borrower_user_id) votre @else l'
        @endif()emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> @if($receiver->id !==
        $loan->borrower_user_id) par {{$loan->borrowerUser->name}} @endif() à
        partir du {{ Formatter::formatDepartureTime($loan)}}.
    </x-p>

    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
