<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($recipientIsBorrower)
    <x-p
        >Votre emprunt de <x-loanable-name :loanable="$loan->loanable" /> est
        confirmé!</x-p
    >
    @else
    <x-p>
        L'emprunt de <x-loanable-name :loanable="$loan->loanable" /> par {{
        $loan->borrowerUser->name}} est confirmé!
    </x-p>

    @endif @if(!is_null($comment))
    <x-message :message="$comment" :message-author="$accepter->name" />
    @endif

    <x-loan-details
        :loan="$loan"
        :recipient-is-borrower="$recipientIsBorrower"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>

    @if(!is_null($comment))<x-message-disclaimer />@endif
</x-email>
