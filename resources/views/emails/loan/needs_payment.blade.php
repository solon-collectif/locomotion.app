@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $loan->borrowerUser->name }},</x-p>

    <x-p>
        Votre emprunt de
        <x-loanable-name :loanable="$loan->loanable" />
        du {{ Formatter::formatDepartureTime($loan) }} au {{
        Formatter::formatReturnTime($loan, $loan->duration_in_minutes) }} doit
        être payé en ligne.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Payer l'emprunt"
        />
    </x-button-section>
</x-email>
