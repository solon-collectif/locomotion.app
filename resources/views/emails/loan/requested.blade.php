<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour,</x-p>

    <x-p>
        {{ $loan->borrowerUser->name }} a demandé à emprunter
        <x-loanable-name :loanable="$loan->loanable" />.
    </x-p>

    <x-message
        :message-author="$loan->borrowerUser->name"
        :message="$messageForOwner"
    />
    <x-loan-details :loan="$loan" :recipient-is-borrower="false" />

    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt pour l'approuver ou le refuser"
        />
    </x-button-section>

    <x-message-disclaimer />
</x-email>
