<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($recipientIsBorrower)
    <x-p>
        Votre réservation commence dans moins de 3 heures, veuillez prendre
        connaissance de la marche à suivre si ce n'est pas déjà fait.
    </x-p>

    <x-p>
        Si vous ne prévoyez pas utiliser le véhicule, vous pouvez annuler la
        réservation en ligne.
    </x-p>
    @else
    <x-p>
        L'emprunt de <x-loanable-name :loanable="$loan->loanable" /> par {{
        $loan->borrowerUser->name }} commence dans moins de 3 heures, veuillez
        prendre connaissance de la marche à suivre si ce n'est pas déjà fait.
    </x-p>

    @if($recipientIsOwnerOrCoowner)
    <x-p>
        Si le véhicule n'est plus disponible, communiquez directement avec
        l'emprunteur et annulez l'emprunt en ligne.
    </x-p>
    @endif @endif

    <x-loan-details
        :loan="$loan"
        :recipient-is-borrower="$recipientIsBorrower"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/'. $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
