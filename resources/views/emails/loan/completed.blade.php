@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title" center>
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        @if($recipientIsBorrower) Votre emprunt de
        <x-loanable-name :loanable="$loan->loanable" />
        @else L'emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> par
        {{$loan->borrowerUser->name}} @endif qui commençait le {{
        Formatter::formatDepartureTime($loan) }} s'est conclu avec succès!
    </x-p>

    @if($recipientIsBorrower && $loan->borrowerInvoice)
    <x-p
        >Le total de la facture a été appliqué à votre solde sur locomotion.app.
    </x-p>
    <x-user-invoice :invoice="$loan->borrowerInvoice" />

    @elseif($recipientIsOwner && $loan->ownerInvoice)
    <x-p
        >Le total de la facture a été appliqué à votre solde sur locomotion.app.
    </x-p>
    <x-user-invoice :invoice="$loan->ownerInvoice" />

    @endif
    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>

    @if($recipientIsBorrower || $recipientIsCoowner || $recipientIsOwner)
    <x-hr />

    <x-p>
        Pour nous permettre de continuer à améliorer l'expérience, laissez-nous
        savoir comment s'est déroulé votre emprunt. Ça prend moins de deux
        minutes à compléter!
    </x-p>

    <x-grid>
        <x-grid-item>
            <x-a
                target="_blank"
                href="{!!$link .  urlencode('Très insatisfait-e') !!}"
            >
                <x-img
                    src="{{ config('app.url') .  '/icons/satisfaction/angry.png'}}"
                    width="40px"
                    alt="Très insatisfait-e"
                />
            </x-a>
        </x-grid-item>
        <x-grid-item>
            <x-a
                target="_blank"
                href="{!!$link . urlencode('Un peu insatisfait-e') !!}"
            >
                <x-img
                    src="{{ config('app.url') .  '/icons/satisfaction/sad.png'}}"
                    width="40px"
                    alt="Un peu insatisfait-e"
                />
            </x-a>
        </x-grid-item>
        <x-grid-item>
            <x-a
                target="_blank"
                href="{!!$link . urlencode('Un peu satisfait-e') !!}"
            >
                <x-img
                    src="{{ config('app.url') .  '/icons/satisfaction/smile-beam.png'}}"
                    width="40px"
                    alt="Un peu satisfait-e"
                />
            </x-a>
        </x-grid-item>
        <x-grid-item>
            <x-a
                target="_blank"
                href="{!!$link . urlencode('Très satisfait-e') !!}"
            >
                <x-img
                    src="{{ config('app.url') .  '/icons/satisfaction/grin-beam.png '}}"
                    width="40px"
                    alt="Très satisfait.e"
                />
            </x-a>
        </x-grid-item>
    </x-grid>
    @endif
</x-email>
