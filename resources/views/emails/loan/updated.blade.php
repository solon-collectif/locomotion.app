<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($borrowerIsUpdater && $recipientIsBorrower)
    <x-p>
        Vous avez bien mis à jour votre emprunt du véhicule
        <x-loanable-name :loanable="$loan->loanable" />. @if($approvalReset)
        <x-u> Les responsables devront réapprouver l'emprunt. </x-u>
        @elseif($needsApproval)
        <x-u> Les responsables doivent encore approuver cet emprunt.</x-u>
        @elseif($needsValidation)
        <x-u>Les responsables doivent valider ces données.</x-u>
        @endif
    </x-p>
    @elseif($recipientIsBorrower)
    <x-p>
        {{ $updater->name }} a mis à jour votre emprunt du véhicule
        <x-loanable-name :loanable="$loan->loanable" />.@if($approvalReset)
        <x-u> Les responsables devront réapprouver l'emprunt. </x-u>
        @elseif($needsApproval)
        <x-u> Les responsables doivent encore approuver cet emprunt. </x-u>
        @elseif($needsRecipientValidation)
        <x-u>Votre validation de ces données est requise!</x-u>
        @elseif($needsValidation)
        <x-u>Les responsables doivent valider ces données.</x-u>
        @endif
    </x-p>
    @elseif($borrowerIsUpdater)
    <x-p>
        {{ $updater->name }} a mis à jour son emprunt de
        <x-loanable-name :loanable="$loan->loanable" />.@if($approvalReset)
        <x-u> Votre ré-approbation est requise. </x-u> @elseif($needsApproval)
        <x-u> Votre approbation ou refus est requis. </x-u>
        @elseif($needsRecipientValidation)
        <x-u>Votre validation de ces données est requise!</x-u>
        @endif()
    </x-p>
    @elseif($recipientIsUpdater)
    <x-p>
        Vous avez bien mis à jour l'emprunt du véhicule
        <x-loanable-name :loanable="$loan->loanable" /> par {{
        $loan->borrowerUser->name }}. @if($approvalReset)
        <x-u> Votre ré-approbation est requise. </x-u>
        @elseif($needsApproval)
        <x-u> Votre approbation ou refus est requis. </x-u>
        @elseif($needsValidation && !$loan->borrower_validated_at)
        <x-u>{{$loan->borrowerUser->name }} doit valider ces données.</x-u>
        @endif
    </x-p>
    @else
    <x-p>
        {{ $updater->name }} a mis à jour l'emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> par {{
        $loan->borrowerUser->name }}.@if($approvalReset)
        <x-u> Votre ré-approbation est requise. </x-u>
        @elseif($needsApproval)
        <x-u> Votre approbation ou refus est requis. </x-u>
        @elseif($needsRecipientValidation)
        <x-u>Votre validation de ces données est requise!</x-u>
        @elseif($needsValidation && !$loan->borrower_validated_at)
        <x-u>{{$loan->borrowerUser->name }} doit valider ces données.</x-u>
        @endif </x-p
    >@endif

    <x-loan-details
        :loan="$loan"
        :recipient-is-borrower="false"
        :initial-loan="$initialLoan"
        :show-contacts="false"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            :label="'Voir l\'emprunt'.
                ($needsApproval && !$recipientIsBorrower ?' pour
                l\'approuver ou le refuser.': '')"
        />
    </x-button-section>
</x-email>
