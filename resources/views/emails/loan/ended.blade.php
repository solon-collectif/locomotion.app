@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    @if($recipientIsBorrower)
    <x-p>
        Vous pouvez maintenant valider ou modifier les données de votre emprunt
        de
        <x-loanable-name :loanable="$loan->loanable" />
        et payer {{ $loan->loanable->owner_user->name }}.
    </x-p>

    <x-p>
        Si vous ne validez ni ne modifiez les données de l'emprunt, celui-ci
        sera validé et payé automatiquement depuis votre solde le
        {{Formatter::formatDate($loan->validationLimit()->tz($loan->loanable->timezone)
        ->locale("fr_CA"))}}.
    </x-p>

    @else
    <x-p>
        L'emprunt de
        <x-loanable-name :loanable="$loan->loanable" />
        par {{ $loan->borrowerUser->name }} est terminé. Vous pouvez maintenant
        valider ou modifier les données de l'emprunt pour reçevoir le paiement.
    </x-p>
    <x-p>
        Si vous ne validez ni ne modifiez les données de l'emprunt, celui-ci
        sera validé et payé automatiquement le
        {{Formatter::formatDate($loan->validationLimit()->tz($loan->loanable->timezone)
        ->locale("fr_CA"))}}.
    </x-p>
    @endif

    <x-loan-details :loan="$loan" :show-contacts="false"></x-loan-details>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
