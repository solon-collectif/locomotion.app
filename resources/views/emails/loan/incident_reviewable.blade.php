@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        @if($reporter->id === $borrowerUser->id) {{ $reporter->name }} a
        rapporté un incident lors de son emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> qui commençait le {{
        Formatter::formatDepartureTime($loan) }}. @else {{ $reporter->name }} a
        rapporté un incident lors de l'emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> par
        {{$borrowerUser->name}} qui commençait le {{
        Formatter::formatDepartureTime($loan) }}. @endif
    </x-p>

    <x-message
        :message="$incident->comments_on_incident"
        :message-author="$reporter->name"
    />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/admin/loans/'. $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>

    <x-message-disclaimer />
</x-email>
