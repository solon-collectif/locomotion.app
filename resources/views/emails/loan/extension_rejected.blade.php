<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $recipient->name }},</x-p>

    <x-p>
        @if($recipientIsBorrower) {{ $refuser->name }} a refusé la prolongation
        de votre emprunt de
        <x-loanable-name :loanable="$loan->loanable" />. @else {{ $refuser->name
        }} a refusé la prolongation de l'emprunt de
        <x-loanable-name :loanable="$loan->loanable" /> par
        {{$borrowerUser->name}}. @endif
    </x-p>

    <x-loan-details :show-contacts="false" :loan="$loan" />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
