<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{$recipient->name}},</x-p>

    <x-p>
        @if($comment->author->id === $recipient->id)Vous avez @else {{
        $comment->author->name }} a @endif envoyé le message suivant dans
        @if($recipient->id === $comment->loan->borrower_user_id) votre emprunt
        de
        <x-loanable-name :loanable="$comment->loan->loanable" />
        @elseif($comment->author->id === $comment->loan->borrower_user_id) son
        emprunt de
        <x-loanable-name :loanable="$comment->loan->loanable" /> @else dans
        l'emprunt de
        <x-loanable-name :loanable="$comment->loan->loanable" /> par {{
        $comment->loan->borrowerUser->name }} @endif().
    </x-p>

    <x-message
        :message-author="$comment->author->name"
        :message="$comment->text"
    />

    <x-button-section>
        <x-button
            :link=" config('app.url') . '/loans/' . $comment->loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>

    <x-message-disclaimer />
</x-email>
