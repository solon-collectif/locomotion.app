<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Votre réservation commence dans 24 heures et n'a pas encore été
        prépayée. Merci de procéder au prépaiement avant d'aller chercher le
        véhicule chez votre voisin. Pour ce faire, rendez-vous sur la page de
        l'emprunt.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/loans/' . $loan->id"
            label="Voir l'emprunt"
        />
    </x-button-section>
</x-email>
