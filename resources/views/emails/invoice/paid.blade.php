<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        @foreach(explode("\n", $text) as $line) {!! $line !!} <x-br />
        @endforeach
    </x-p>

    <x-user-invoice :invoice="$invoice" />

    <x-button-section>
        <x-button
            :link="config('app.url') . '/profile/invoices/' . $invoice->id"
            label="Voir la facture en ligne"
        />
    </x-button-section>
</x-email>
