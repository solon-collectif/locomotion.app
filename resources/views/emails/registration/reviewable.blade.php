@php use App\Helpers\Path; @endphp
<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        Un-e nouveau-lle membre, {{ $user->full_name }}, a complété son
        inscription dans {{ $community->name }} et peut être validé-e.
    </x-p>

    <x-button-section>
        <x-button
            label="Voir le profil"
            :link='Path::url("/admin/communities/$community->id/?user_id=$user->id#members")'
        />
    </x-button-section>
</x-email>
