<x-email
    :render-text="$renderText ?? false"
    title="Votre compte LocoMotion est validé!"
    :center="false"
    wide
>
    <x-p>C'est parti!</x-p>

    <x-p
        >Votre comité bénévole local a validé votre adhésion, {{$user->name}}.🎉
    </x-p>
    <x-p>
        Avec les membres de {{$community->name}}, vous avancez vers moins
        d’autos, plus d’espace et de plaisir!
    </x-p>

    @if($startingGuideUrl)
    <x-highlight>
        <x-highlight-title> 📖 Guide de départ </x-highlight-title>
        <x-p>
            Préparé par votre comité local, le guide de départ répond à bien des
            questions<x-nbsp />:
        </x-p>

        <x-ul>
            <x-li
                >Où aller chercher mon carnet de bord ou mon
                attache-remorque?</x-li
            >
            <x-li>Comment prêter ou emprunter des véhicules?</x-li>
            <x-li>Comment fonctionnent les cadenas dans mon quartier?</x-li>
            <x-li>Comment rejoindre mon comité local?</x-li>
        </x-ul>

        <x-p>
            <x-a :href="$startingGuideUrl">Consultez le guide de départ</x-a>
        </x-p>
    </x-highlight>
    @endif

    <x-subtitle>Vous êtes un moteur de changement</x-subtitle>

    <x-p
        >Le réseau LocoMotion repose sur {{$communityCount}} communautés et
        {{$userCount}} personnes engagées. LocoMotion dans {{$community->name}}
        est piloté par votre voisinage, et maintenant par vous! Chaque membre
        joue un rôle clé dans le succès de l’initiative locale et du
        réseau.</x-p
    >
    <x-p
        >En participant à l’aventure collective LocoMotion, vous pouvez
        contribuer
        <x-a href="https://info.locomotion.app/impact-de-votre-don/">
            financièrement </x-a
        >, en
        <x-a href="https://info.locomotion.app/contribuer/">temps</x-a> et/ou en
        prêtant votre véhicule. @if($community->contact_email) Contactez votre
        comité pour mettre l’épaule à la roue<x-nbsp />:
        <x-a :href="'mailto:'.$community->contact_email"
            >{{$community->contact_email}}</x-a
        >.@endif</x-p
    >

    <x-p>
        <x-strong>Votre engagement fait toute la différence.</x-strong>
    </x-p>

    <x-centered>
        <x-subtitle>🚲 À vos marques, partagez!</x-subtitle>

        <x-grid>
            <x-grid-item>
                <x-button
                    label="Prêter"
                    link="https://locomotion.app/profile/loanables"
                />
            </x-grid-item>
            <x-grid-item width="25" />
            <x-grid-item>
                <x-button
                    label="Emprunter"
                    link="https://locomotion.app/search/map"
                />
            </x-grid-item>
        </x-grid>

        <x-p
            >📚 Besoin d’aide?
            <x-a href="https://locomotion.app/faq"
                >Consultez la Foire aux questions (FAQ)</x-a
            ></x-p
        >
        <x-br />

        <x-p
            >Merci de faire partie de notre grand réseau.<x-br />
            Ensemble, on écrit l’histoire de la mobilité active et partagée au
            Québec.
        </x-p>
    </x-centered>
</x-email>
