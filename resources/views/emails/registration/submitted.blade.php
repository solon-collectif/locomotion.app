<x-email
    :render-text="$renderText ?? false"
    :title="'Vous y êtes presque'"
    :center="false"
    with-disclaimer
>
    <x-p>Votre inscription a été bien reçue.</x-p>

    <x-p>Votre aventure commence ici, {{$user->name}}.</x-p>
    <x-centered>
        <x-img
            src="https://media1.tenor.com/m/SxLrC4TviA4AAAAC/drifting-girl.gif"
            width="400"
            style="max-width: 100%"
        />
    </x-centered>
    <x-p>Enfin, presque.</x-p>
    <x-p
        >Votre comité de quartier validera votre preuve de résidence d'ici
        quelques jours. On vous avisera par courriel.</x-p
    >
    <x-p>Merci d’embarquer avec nous.</x-p>
    <x-p
        >En rejoignant LocoMotion, vous participez à offrir à votre communauté
        plus de moyens de se déplacer, plus d’opportunités d’agir ensemble.</x-p
    >
    <x-quote
        text="LocoMotion a définitivement un impact sur mes habitudes de mobilité!
        Nous avons utilisé le vélo-cargo tout l'été, pour des sorties en famille
        et pour faire des courses dans le quartier."
        author="membre LocoMotion Ahuntsic"
    />
    @if($chatGroupUrl)
    <x-p
        >Dans l’attente, si vous débordez d’affection au point de partager LA
        RAISON No1 qui vous a fait embarquer dans l’aventure, affichez votre
        appartenance à votre communauté en taguant le réseau @locomotion.qc</x-p
    >

    <x-p>
        Faites connaissance avec les membres près de chez vous dans le
        <x-a :href="$chatGroupUrl">groupe de votre communauté.</x-a>
    </x-p>
    @endif @if($startingGuideUrl)
    <x-p
        >Vous pouvez aussi prendre de l’avance en regardant le
        <x-a :href="$startingGuideUrl">guide de départ de votre communauté</x-a
        >.</x-p
    >
    @endif
</x-email>
