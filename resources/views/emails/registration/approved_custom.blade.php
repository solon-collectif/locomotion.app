<x-email
    :render-text="$renderText ?? false"
    title="Votre compte LocoMotion est validé!"
    :center="false"
    wide
>
    <x-markdown-content :markdown="$markdownContent"></x-markdown-content>
</x-email>
