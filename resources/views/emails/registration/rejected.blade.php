<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>Bonjour {{ $user->name }},</x-p>

    <x-p>
        Votre demande d'adhésion à LocoMotion a été refusée pour une des raisons
        suivantes:
    </x-p>

    <ol style="text-align: left">
        <li>Votre adresse n'est pas localisée dans un des voisinages.</li>
        <li>Il y a un problème avec votre preuve de résidence.</li>
    </ol>

    <x-p>
        Pour plus de détails, communiquez avec l'équipe LocoMotion au courriel
        <x-a href="mailto:info@locomotion.app">info@locomotion.app</x-a> .
    </x-p>
</x-email>
