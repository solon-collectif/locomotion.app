@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-markdown-content :markdown="$invitationMessage" />

    <x-p>
        Cette invitation est valide jusqu'au
        {{Formatter::formatDay($expiration)}}.
    </x-p>

    <x-button-section>
        <x-button :link="$invitationUrl" label="Accepter l'invitation" />
    </x-button-section>
</x-email>
