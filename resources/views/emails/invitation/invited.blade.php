@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour,</x-p>

    <x-p>
        {{ $senderName }} vous a invité à rejoindre la communauté
        {{$communityName }} sur
        <x-a href="https://info.locomotion.app">locomotion.app</x-a>
        !
    </x-p>

    <x-message :message-author="$senderName" :message="$invitationMessage" />

    <x-p>
        Cette invitation est valide jusqu'au
        {{Formatter::formatDay($expiration)}}.
    </x-p>

    <x-button-section>
        <x-button :link="$invitationUrl" label="Accepter l'invitation" />
    </x-button-section>
</x-email>
