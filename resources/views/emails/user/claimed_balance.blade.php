<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p>
        {{ $payout->user->full_name }} a demandé à ce que son solde lui soit
        remboursé.
    </x-p>

    @if(config('services.kiwili.enabled'))
    <x-p>
        Une erreur s'est produite lors de la génération de la facture sur
        Kiwili: vous devrez la générer manuellement.
    </x-p>
    @endif

    <x-button-section>
        <x-button
            :link="config('app.url') . '/admin/payouts?id='.$payout->id"
            label="Voir la demande de remboursement"
        />
    </x-button-section>
</x-email>
