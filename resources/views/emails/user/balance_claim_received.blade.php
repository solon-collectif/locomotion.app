@php use App\Helpers\Formatter; @endphp
<x-email :render-text="$renderText ?? false" :title="$title">
    <x-p>Bonjour {{ $payout->user->name }},</x-p>

    <x-p>
        Nous avons bien reçu votre demande de paiement. Votre solde de
        {{Formatter::currency($payout->amount)}} vous sera transféré dans les 10
        jours ouvrables.
    </x-p>
    <x-p> Merci pour votre participation :)</x-p>
</x-email>
