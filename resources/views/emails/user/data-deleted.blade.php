<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p> Bonjour {{$name}}, </x-p>

    <x-p>
        Vos données sur locomotion.app ont été supprimées définitivement. Merci
        pour votre participation!
    </x-p>

    @if($incomeReport)
    <x-p>
        Vous trouverez en pièce jointe un relevé de vos revenus pour l'année en
        cours.
    </x-p>
    @endif
</x-email>
