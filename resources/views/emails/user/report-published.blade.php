<x-email :render-text="$renderText ?? false" :title="$title" :center="true">
    <x-p> Bonjour {{$userReport->user->name}}, </x-p>

    <x-p>
        Un nouveau document est maintenant disponible sur votre profil
        locomotion.app<x-nbsp />: {{$documentName}}.
    </x-p>

    <x-button-section>
        <x-button
            :link="config('app.url') . '/profile/reports?id=' . $userReport->id"
            label="Voir le document"
        />
    </x-button-section>
</x-email>
