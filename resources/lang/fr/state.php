<?php

return [
    "action" => [
        "must_be_ongoing" => "L'étape :action doit être en cours.",
        "must_be_completed" => "L'étape :action doit être complétée.",
        "must_be_after_departure" =>
            "L'étape :action ne peut pas être complétée avant l'heure du départ.",
    ],
    "borrower" => [
        "must_be_active" => "L'emprunteur ne doit pas être archivé.",
    ],
    "loan" => [
        "status" => [
            "requested" => "nouvelle demande",
            "accepted" => "prépaiement",
            "confirmed" => "confirmé",
            "ongoing" => "en cours",
            "ended" => "validation",
            "validated" => "paiement",
            "completed" => "complété",
            "rejected" => "refusé",
            "canceled" => "annulé",
        ],
        "must_be_ongoing" => "L'emprunt doit être en cours.",
        "must_not_be_ongoing_with_cost" =>
            "L'emprunt payant ne doit pas être débuté.",
        "must_be_canceled" => "L'emprunt doit être annulé.",
        "must_be_contested" => "L'emprunt doit être contesté.",
        "must_not_have_open_incident" =>
            "L'emprunt ne doit pas déjà avoir un incident en cours.",
        "must_have_status" => "L'emprunt doit avoir le statut ':status'.",
        "must_have_required_info" =>
            "Des informations concernant l'emprunt sont manquantantes (ex. : kilométrage).",
        "auto_validation_question_missing" => "Aucune auto-validation requise.",
        "auto_validation_answer_wrong" =>
            "Réponse d'auto-validation incorrecte.",
    ],
    "loanable" => [
        "must_be_active" => "Le véhicule ne doit pas être archivé.",
        "user_already_has_role" =>
            "Le-a membre a déjà ce rôle pour ce véhicule.",
    ],
    "owner" => [
        "must_be_active" => "Le propriétaire ne doit pas être archivé.",
    ],
    "payment" => [
        "must_not_be_completed" =>
            "L'emprunt ne doit pas avoir un paiement complété.",
        "borrower_cant_pay" =>
            "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
        "loan_needs_validation" =>
            "L'emprunt doit être validé avant d'être payé.",
    ],
    "pricing" => [
        "must_not_overlap" =>
            "Une facturation du type ':pricing_type' existe déjà pour les véhicules de type ':pricing_loanable_type'.",
        "type" => [
            "contribution" => "contribution",
            "price" => "prix",
            "insurance" => "assurance",
        ],
        "loanable_type" => [
            "car" => "auto",
            "car_small" => "petite auto",
            "car_large" => "grosse auto",
            "car_small_electric" => "petite auto électrique",
            "car_large_electric" => "grosse auto électrique",
            "bike_regular" => "vélo",
            "bike_electric" => "vélo électrique",
            "bike_cargo_regular" => "vélo-cargo",
            "bike_cargo_electric" => "vélo-cargo électrique",
            "trailer" => "remorque",
        ],
    ],
    "reports" => [
        "no_owners_with_income" =>
            "Aucun propriétaire avec revenu pour l'année :year",
    ],
    "subscription" => [
        "cannot_pay" =>
            "Votre solde n'est pas assez élevé pour payer la contribution annuelle.",
        "no_available_subscription_for_types" =>
            "Aucune contribution annuelle disponible pour :pricing_loanable_types.",
        "cannot_reduce" =>
            "Impossible de réduire la contribution actuelle. Incluez les types suivants: :pricing_loanable_types.",
    ],
    "user" => [
        "balance_cannot_be_negative" =>
            "Le solde du compte ne peut être négatif.",
        "balance_too_low" => "Le solde est trop bas pour être remboursé.",
    ],
];
