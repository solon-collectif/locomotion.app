const { sentryWebpackPlugin } = require("@sentry/webpack-plugin");
const { resolve } = require("path");
const ESLintPlugin = require("eslint-webpack-plugin");

// This has to be set during build time
process.env.VUE_APP_VERSION = require("./package.json").version;

module.exports = {
  chainWebpack: (config) => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();
    svgRule.delete("type");
    svgRule.delete("generator");

    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader");

    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .type("javascript/auto")
      .use("i18n")
      .loader("@kazupon/vue-i18n-loader")
      .end()
      .use("yaml")
      .loader("yaml-loader")
      .end();
  },

  css: {
    loaderOptions: {
      css: {
        url: { filter: (url) => !url.startsWith("/") },
      },
      sass: {
        prependData: '@import "@/assets/scss/_variables.scss";',
      },
    },
  },

  configureWebpack: (config) => {
    config.resolve.roots = [__dirname, resolve(__dirname, "public")];
    config.devServer = {
      proxy: {
        "^/api": {
          target: process.env.BACKEND_URL,
          changeOrigin: true,
        },
      },
      hot: true,

      host: "0.0.0.0",
      client: {
        webSocketURL: "ws://localhost:8080/ws",
        logging: "info",
        overlay: {
          warnings: true,
          errors: true,
        },
      },
      allowedHosts: "all",
    };

    config.devtool = "source-map"; // Source map generation must be turned on for sentry
    //
    // // from https://github.com/vuejs/vue-cli/issues/2978#issuecomment-577364101
    if (process.env.NODE_ENV === "development") {
      config.plugins.push(new ESLintPlugin({ extensions: ["js", "vue"] }));
      config.output.devtoolModuleFilenameTemplate = (info) => {
        let resPath = info.resourcePath;
        let isVue = resPath.match(/\.vue$/);
        let isGenerated = info.allLoaders;

        let lastDir = resPath.lastIndexOf("/");
        let dir = resPath.substring(0, lastDir);
        let name = resPath.substring(lastDir + 1);

        let generated = `webpack-generated:///${dir}/gen_${name}?${info.hash}`;
        let vuesource = `vue-source:///${resPath}`;

        return isVue && isGenerated ? generated : vuesource;
      };

      config.output.devtoolFallbackModuleFilenameTemplate = "webpack:///[resource-path]?[hash]";
    }
    // Put the Sentry Webpack plugin after all other plugins
    // This allows the use of source-maps in sentry.
    if (process.env.SENTRY_AUTH_TOKEN) {
      config.plugins.push(
        sentryWebpackPlugin({
          authToken: process.env.SENTRY_AUTH_TOKEN,
          org: "locomotion",
          project: "javascript-vue",
        })
      );
    }
  },

  assetsDir: "dist/",
};
