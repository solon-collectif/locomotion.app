module.exports = {
  extends: ["eslint:recommended", "plugin:vue/recommended", "prettier"],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
    "vue/no-mutating-props": [
      "error",
      {
        shallowOnly: true,
      },
    ], // We should remove these errors, but there are a lot and they are complex to fix
    "vue/valid-v-slot": "off", // Bootstrap vue uses slots with parentheses cell(columnName) which eslint takes issue with
  },
  globals: {
    process: "readonly",
  },

  parserOptions: {
    ecmaVersion: 2022,
  },

  ignorePatterns: ["**/*.test.js"],
};
