import { extractErrors } from "@/helpers";
import { capitalize } from "@/helpers/filters";
import i18n from "@/i18n";
import fr from "@/locales/fr";
import { LoggedOutException } from "@/requests/LoggedOutException";
import axios from "axios";
import dayjs from "dayjs";
import Vue from "vue";

/**
 * @typedef {Object} NotificationOptions
 * @property {string} [action] - Action performed, acts as main label of notifications. If absent no
 *  notification will be shown.
 * @property {boolean|string} [onSuccess] - Whether to show a success notification on success. If
 *  string: the success notification to show.
 * @property {string} [ressourceName] - Name of the ressource being acted upon. Is shown in some
 *  error message descriptions. If absent, will be guessed from the request url.
 */

/**
 * @typedef RequestOptions
 * @property {string} [cancelId] - id for deduping requests. Should be set for requests which may
 *  overlap (e.g. polling some status update or validating content on typing) to avoid race
 *  conditions on returned values.
 * @property {int[]} [expects] - error status codes that should not be handled.
 */

/** @type {Object.<string, CancelTokenSource>}  */
const cancelTokens = {};
let refreshingTokenPromise = null;

const errorMessages = {
  400: {
    contentFunc: ({ response }) => (response?.data ? extractErrors(response.data).join(", ") : ""),
    title: "erreur dans la requête",
    variant: "danger",
  },
  401: {
    content: "Votre connexion est expirée, veuillez vous reconnecter.",
    title: "erreur d'authentification",
    variant: "danger",
  },
  403: {
    contentFunc: ({ ressourceName, response }) =>
      response?.data?.message && response.data.message !== "This action is unauthorized."
        ? response.data.message
        : `Vous n'avez pas les permissions nécessaires pour faire cette action sur ${ressourceName}.`,
    title: "accès interdit",
    variant: "danger",
  },
  404: {
    contentFunc: ({ ressourceName }) => `Impossible de trouver ${ressourceName}.`,
    title: "ressource introuvable",
    variant: "danger",
  },
  422: {
    contentFunc: ({ response }) => extractErrors(response.data).join(", "),
    title: "erreur de validation",
    variant: "danger",
  },
  429: {
    contentFunc: ({ ressourceName }) =>
      `Nous recevons trop de demandes pour ${ressourceName}. Veuillez réessayer plus tard.`,
    title: "trop de requêtes",
    variant: "danger",
  },
  500: {
    content: "Une erreur système s'est produite.",
    title: "erreur",
    variant: "danger",
  },
};

function formatErrorTitle(title, notificationLabel = null) {
  if (!notificationLabel) {
    return title;
  }

  return `${capitalize(notificationLabel)}: ${title}`;
}

function guessRessourceName(url) {
  for (const slugKey in fr.errorRessourceNames) {
    const slugIndex = url.indexOf(slugKey);
    if (slugIndex >= 0) {
      const single = url.slice(slugIndex + 1 + slugKey.length).match(/\d+/);

      return i18n.tc(`errorRessourceNames.${slugKey}`, single ? 1 : 2);
    }
  }

  return "cette ressource";
}

function checkTokenSyncedInLocalStorage() {
  // Do not sync anything in localStorage if mandated
  if (Vue.$store.state.mandated) {
    return;
  }

  if (!localStorage?.locomotion) {
    return;
  }
  // Token may have been refreshed in a different window, updating the localstorage token.
  try {
    const localStorageState = JSON.parse(localStorage.locomotion);
    if (
      localStorageState.token !== Vue.$store.state.token &&
      localStorageState.user?.id === Vue.$store.state.user?.id
    ) {
      Vue.$store.commit("setTokens", {
        token: localStorageState.token,
        refreshToken: localStorageState.refreshToken,
        tokenExpiresOn: localStorageState.tokenExpiresOn,
      });
    }
  } catch (e) {
    // Do nothing if locomotion localstorage is malformed
  }
}

async function maybeRefreshToken() {
  // Do not refresh mandated tokens
  if (Vue.$store.state.mandated) {
    return;
  }

  // Validate all required info is present for refresh
  if (
    !Vue.$store.state.token ||
    !Vue.$store.state.tokenExpiresOn ||
    !Vue.$store.state.refreshToken
  ) {
    return;
  }

  // Do not have more than one request refreshing the token at a time
  if (refreshingTokenPromise) {
    await refreshingTokenPromise;
    return;
  }

  if (dayjs().isAfter(Vue.$store.state.tokenExpiresOn)) {
    await logout();
    return;
  }

  let shouldRefresh = false;

  if (Vue.$store.state.loggedInOn) {
    // refresh after half of the token lifetime has expired.
    let tokenLifetime = dayjs(Vue.$store.state.tokenExpiresOn).diff(Vue.$store.state.loggedInOn);
    shouldRefresh = dayjs(Vue.$store.state.tokenExpiresOn).diff(dayjs()) < tokenLifetime / 2;
  } else {
    shouldRefresh = dayjs().add(1, "day").isAfter(Vue.$store.state.tokenExpiresOn);
  }

  if (!shouldRefresh) {
    return;
  }
  try {
    refreshingTokenPromise = axios.post("/auth/token/refresh", {
      refresh_token: Vue.$store.state.refreshToken,
    });
    const { data } = await refreshingTokenPromise;

    Vue.$store.commit("setTokens", {
      token: data.access_token,
      refreshToken: data.refresh_token,
      expiresIn: data.expires_in,
    });
  } catch (e) {
    await logout();
  } finally {
    refreshingTokenPromise = null;
  }
}

async function logout() {
  // Token is not valid.
  Vue.$store.commit("setTokens", {});
  if (Vue.$router.currentRoute.name !== "login") {
    await Vue.$router.push(`/login?logout=1&r=${Vue.$router.currentRoute.fullPath}`);
  }
  await Vue.$store.dispatch("logout");

  // abort current request
  throw new LoggedOutException();
}

/**
 * Sends a XHR using the provided requestConfig.
 *
 * @param {AxiosRequestConfig} requestConfig
 * @param {?RequestOptions} requestOptions
 * @param {?NotificationOptions} notifications
 * @param {?function} cleanupCallback - function to run after the request completes or fails, but not
 *  if cancelled because of requestOptions.cancelId.
 *
 * @return {any} Response from the server
 * @throws {Error} - Axios error if request fails.
 */
async function send(
  requestConfig,
  requestOptions = {},
  notifications = {},
  cleanupCallback = null
) {
  const { cancelId } = requestOptions;

  if (cancelId) {
    if (cancelTokens[cancelId]) {
      cancelTokens[cancelId].cancel();
    }
    cancelTokens[cancelId] = axios.CancelToken.source();
    requestConfig.cancelToken = cancelTokens[cancelId].token;
  }

  checkTokenSyncedInLocalStorage();
  await maybeRefreshToken();

  try {
    const response = await axios(requestConfig);

    if (cancelId) {
      delete cancelTokens[cancelId];
    }

    if (cleanupCallback && typeof cleanupCallback === "function") {
      cleanupCallback();
    }

    if (notifications.action && notifications.onSuccess) {
      Vue.$store.commit("addNotification", {
        content: "",
        title: capitalize(
          typeof notifications.onSuccess === "string"
            ? notifications.onSuccess
            : `${notifications.action} réussi(e)!`
        ),
        variant: "success",
      });
    }

    return response;
  } catch (e) {
    if (axios.isCancel(e) || e instanceof LoggedOutException) {
      // Request was canceled. No need to handle it, but need
      // to interrupt callers waiting on response.
      throw e;
    }

    if (cleanupCallback && typeof cleanupCallback === "function") {
      cleanupCallback();
    }

    if (cancelId) {
      delete cancelTokens[cancelId];
    }

    const expectedCodes = new Set(requestOptions.expects || []);

    const { request, response } = e;
    if (request?.status && expectedCodes.has(request.status)) {
      // do not handle, but throw the exception so the caller can handle it
      throw e;
    }

    // If auth error, logout and redirect to login
    if (request?.status === 401) {
      await logout();
    }

    if (!notifications.action) {
      throw e;
    }

    const ressourceName = notifications.ressourceName ?? guessRessourceName(requestConfig.url);

    if (request?.status && errorMessages[request.status]) {
      const errorMessage = errorMessages[request.status];
      Vue.$store.commit(
        "addNotification",
        {
          content: errorMessage.contentFunc
            ? errorMessage.contentFunc({ request, response, ressourceName })
            : errorMessage.content,
          title: formatErrorTitle(errorMessage.title, notifications.action),
          variant: errorMessage.variant,
        },
        { root: true }
      );
    } else {
      Vue.$store.commit(
        "addNotification",
        {
          content: "Une erreur inconnue s'est produite.",
          title: formatErrorTitle("Erreur", notifications.action),
          variant: "danger",
        },
        { root: true }
      );
    }

    throw e;
  }
}

/**
 * @param {string} url
 * @param data
 * @param {AxiosRequestConfig} axiosRequestConfig
 * @param {?RequestOptions} requestOptions
 * @param {?NotificationOptions} notifications
 * @param {?function} cleanupCallback
 *
 * @see {send}
 */
export async function put(
  url,
  data = null,
  { axiosRequestConfig = {}, requestOptions = {}, notifications = {}, cleanupCallback = null } = {}
) {
  return send(
    {
      ...axiosRequestConfig,
      method: "put",
      url,
      data: data,
    },
    requestOptions,
    notifications,
    cleanupCallback
  );
}

/**
 * @param {string} url
 * @param data
 * @param {AxiosRequestConfig} axiosRequestConfig
 * @param {?NotificationOptions} notifications
 * @param {?RequestOptions} requestOptions
 * @param {?function} cleanupCallback
 *
 * @see {send}
 */
export async function post(
  url,
  data = null,
  { axiosRequestConfig = {}, requestOptions = {}, notifications = {}, cleanupCallback = null } = {}
) {
  return send(
    {
      ...axiosRequestConfig,
      method: "post",
      url,
      data: data,
    },
    requestOptions,
    notifications,
    cleanupCallback
  );
}

/**
 * @param {string} url
 * @param {AxiosRequestConfig} axiosRequestConfig
 * @param {?NotificationOptions} notifications
 * @param {?RequestOptions} requestOptions
 * @param {?function} cleanupCallback
 *
 * @see {send}
 */
export async function get(
  url,
  { axiosRequestConfig = {}, requestOptions = {}, notifications = {}, cleanupCallback = null } = {}
) {
  return send(
    {
      ...axiosRequestConfig,
      method: "get",
      url,
    },
    requestOptions,
    notifications,
    cleanupCallback
  );
}

/**
 * @param {string} url
 * @param {AxiosRequestConfig} axiosRequestConfig
 * @param {?RequestOptions} requestOptions
 * @param {?NotificationOptions} notifications
 * @param {?function} cleanupCallback
 *
 * @see {send}
 */
export async function del(
  url,
  { axiosRequestConfig = {}, requestOptions = {}, notifications = {}, cleanupCallback = null } = {}
) {
  return send(
    {
      ...axiosRequestConfig,
      method: "delete",
      url,
    },
    requestOptions,
    notifications,
    cleanupCallback
  );
}

export async function options(url) {
  return send({
    method: "options",
    url,
  });
}
