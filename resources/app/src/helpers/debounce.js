export function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    if (timer) {
      clearTimeout(timer); // clear any pre-existing timer
    }
    const context = this; // get the current context
    timer = setTimeout(() => {
      func.apply(context, args); // call the function if time expires
    }, timeout);
  };
}
