import { get, put } from "@/requests/server";

export default {
  namespaced: true,
  state: {
    data: null,
    error: null,
    loaded: false,
    claiming: false,
  },
  mutations: {
    data(state, data) {
      state.data = data;
    },
    error(state, error) {
      state.error = error;
    },
    loaded(state, loaded) {
      state.loaded = loaded;
    },
    claiming(state, claiming) {
      state.claiming = claiming;
    },
    cancelToken(state, cancelToken) {
      state.cancelToken = cancelToken;
    },
  },
  actions: {
    async buyCredit({ commit, dispatch }, { amount, paymentMethodId }) {
      commit("loaded", false);
      const { data } = await put(
        "/auth/user/balance",
        {
          amount,
          payment_method_id: paymentMethodId,
        },
        {
          requestOptions: { cancelId: "add-to-balance" },
          notifications: { action: "ajout au solde" },
        }
      );

      commit("data", data);
      commit("loaded", true);
      dispatch("loadUser", {}, { root: true });
    },
    async claimCredit({ commit, dispatch }) {
      commit("loaded", false);
      commit("claiming", true);
      const { data } = await put(
        "/auth/user/claim",
        {},
        {
          notifications: { action: "demande de transfer" },
          cleanupCallback: () => {
            commit("loaded", true);
            commit("claiming", false);
          },
        }
      );
      commit("data", data);
      dispatch("loadUser", {}, { root: true });
    },
    async mandate({ commit, dispatch }, { mandatedUserId }) {
      const response = await get(`/auth/password/mandate/${mandatedUserId}`);
      await commit("setTokens", { token: response.data, mandated: true }, { root: true });
      await dispatch("loadUser", {}, { root: true });
    },
  },
};
