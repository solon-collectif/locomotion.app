FROM php:8.2-apache AS base

# installing dependecies
RUN apt-get update && apt-get install -y \
    libpq-dev \
    imagemagick \
    libmagickwand-dev \
    libzip-dev \
    git \
    npm \
    postgresql

# installing php extensions
RUN docker-php-ext-install \
    pdo \
    pdo_pgsql \
    pgsql \
    gd \
    zip \
    pcntl

# installing imagick through pecl because it is not working with docker-php-ect-install
RUN pecl install imagick ds swoole
RUN docker-php-ext-enable imagick ds

# Giving a server name to stop a warning
RUN echo "ServerName laravel-app.local" >> /etc/apache2/apache2.conf

# setting the correct doc root
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# enabling mod rewrite in apache
RUN a2enmod rewrite headers

# putting our own php.ini
COPY ./php.ini ${PHP_INI_DIR}/conf.d/php.ini

# installing composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer


##################
FROM base AS dev
# xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug
RUN echo "xdebug.mode = debug" >> ${PHP_INI_DIR}/conf.d/php.ini
RUN echo "xdebug.client_host = host.docker.internal" >> ${PHP_INI_DIR}/conf.d/php.ini


# Change inner www-data user gid-uid to match outer users', to fix access permissions with
# files created from this container not being accessible in bind-mounted volumes
ARG USER_ID
ARG GROUP_ID
RUN if [ ${USER_ID:-0} -ne 0 ] && [ ${GROUP_ID:-0} -ne 0 ]; then \
    userdel -f www-data &&\
    if getent group www-data ; then groupdel www-data; fi &&\
    groupadd -g ${GROUP_ID} www-data &&\
    useradd -l -u ${USER_ID} -g www-data www-data \
;fi
# Home needed for npm install in gitlab ci.
RUN install -d -m 0755 -o www-data -g www-data /home/www-data

USER www-data

CMD bash -c "npm install && composer install && \
             ./start_php_container.sh"


###################
FROM base AS prod

COPY . /var/www/html/
RUN composer install --no-dev
RUN chown -R www-data.www-data /var/www/html/
RUN docker-php-ext-install opcache

RUN php artisan route:cache

CMD ./start_php_container.sh
