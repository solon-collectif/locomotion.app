<?php

namespace App\Logging;

use Google\Cloud\Logging\LoggingClient;
use Google\Cloud\Logging\PsrLogger;

class CreateGoogleCloudLogger
{
    public function __invoke(): PsrLogger
    {
        $logging = new LoggingClient([
            "projectId" => config("logging.channels.googlecloud.project_id"),
        ]);
        return $logging->psrLogger("app", [
            "resource" => [
                "type" => "cloud_run_revision",
                "labels" => [
                    "configuration_name" => config(
                        "logging.channels.googlecloud.configuration"
                    ),
                    "project_id" => config(
                        "logging.channels.googlecloud.project_id"
                    ),
                    "revision_name" => config(
                        "logging.channels.googlecloud.revision"
                    ),
                    "service_name" => config(
                        "logging.channels.googlecloud.service"
                    ),
                ],
            ],
        ]);
    }
}
