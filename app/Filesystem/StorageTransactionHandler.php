<?php

namespace App\Filesystem;

use App\Helpers\Path;
use Illuminate\Support\Facades\Storage;
use Log;

class StorageTransactionHandler
{
    private bool $inTransaction = false;
    private array $pathsToDelete = [];
    private array $directoriesToDelete = [];

    private array $newFiles = [];
    private array $originalData = [];

    public function isInTransaction(): bool
    {
        return $this->inTransaction;
    }

    public function beginTransaction(): void
    {
        if ($this->inTransaction) {
            Log::warning(
                "[StorageTransaction] Attempt to start already ongoing transaction"
            );
            return;
        }

        $this->inTransaction = true;
        $this->pathsToDelete = [];
        $this->directoriesToDelete = [];
        $this->newFiles = [];
        $this->originalData = [];
    }

    /**
     * Deletes any file scheduled for deletion in the transaction.
     */
    public function commit(): void
    {
        if (!$this->inTransaction) {
            Log::warning(
                "[StorageTransaction] Attempt to commit with no ongoing transaction"
            );
            return;
        }

        foreach ($this->pathsToDelete as $fileToDelete) {
            $deleted = Storage::delete($fileToDelete);
            if (!$deleted) {
                Log::warning(
                    "[StorageTransaction] Commit: Failed deleting $fileToDelete."
                );
            }
        }

        foreach ($this->directoriesToDelete as $dirToDelete) {
            $deleted = Storage::deleteDirectory($dirToDelete);
            if (!$deleted) {
                Log::warning(
                    "[StorageTransaction] Commit: Failed deleting $dirToDelete."
                );
            }
        }
        $this->inTransaction = false;
    }

    /**
     * Deletes any new file created during the transaction, and reverts any overridden file.
     * Keeps any file and directory scheduled for deletion.
     */
    public function rollback(): void
    {
        if (!$this->inTransaction) {
            Log::warning(
                "[StorageTransaction] Attempt to rollback with no ongoing transaction"
            );
            return;
        }

        foreach ($this->newFiles as $newFile) {
            $deleted = Storage::delete($newFile);
            if (!$deleted) {
                Log::warning(
                    "[StorageTransaction] Rollback: Failed deleting new file $newFile"
                );
            }
        }

        foreach ($this->originalData as $filepath => $content) {
            $put = Storage::put($filepath, $content);
            if (!$put) {
                Log::error(
                    "[StorageTransaction] Rollback: Failed reverting the contents of $filepath."
                );
            }
        }

        $this->inTransaction = false;
    }

    /**
     * Schedules a file to be deleted and returns true if in a transaction,
     * otherwise deletes it immediately.
     */
    public function delete($paths): bool
    {
        if (!$this->inTransaction) {
            return Storage::delete($paths);
        }

        $paths = is_array($paths) ? $paths : func_get_args();
        array_push($this->pathsToDelete, ...$paths);
        return true;
    }

    /**
     * Schedules a directory to be deleted and returns true if in a transaction,
     * otherwise deletes it immediately.
     */
    public function deleteDirectory($directory): bool
    {
        if (!$this->inTransaction) {
            return Storage::deleteDirectory($directory);
        }

        $this->directoriesToDelete[] = $directory;
        return true;
    }

    public function copy($source, $destination): bool
    {
        return $this->handleNewFile(
            $destination,
            fn() => Storage::copy($source, $destination)
        );
    }

    public function put($destination, $data): bool
    {
        return $this->handleNewFile(
            $destination,
            fn() => Storage::put($destination, $data)
        );
    }

    public function putFileAs($path, $file, $name)
    {
        $destination = Path::join($path, $name);
        return $this->handleNewFile(
            $destination,
            fn() => Storage::putFileAs($path, $file, $name)
        );
    }

    public function handleNewFile($destination, $newFileCallback)
    {
        if (!$this->inTransaction) {
            return $newFileCallback();
        }

        // When creating/putting a file after delete, avoid deleting newly created file on commit.
        $this->pathsToDelete = array_diff($this->pathsToDelete, [$destination]);

        $overridden = Storage::exists($destination);
        if ($overridden) {
            // Keep original
            $this->originalData[$destination] = Storage::get($destination);
        }

        $newFileCreated = $newFileCallback();

        if ($newFileCreated && !$overridden) {
            $this->newFiles[] = $destination;
        }

        return $newFileCreated;
    }
}
