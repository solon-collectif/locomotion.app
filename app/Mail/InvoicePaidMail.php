<?php

namespace App\Mail;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class InvoicePaidMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public $subject;

    public function __construct(
        public User $user,
        public Invoice $invoice,
        public ?string $title,
        public $text = null
    ) {
        $this->invoice->load("billItems");
        $this->view("emails.invoice.paid");
    }
}
