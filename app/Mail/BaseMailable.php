<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Mime\Email;

abstract class BaseMailable extends Mailable
{
    public ?string $title = null;
    public function send($mailer)
    {
        $mailableClass = static::class;
        // Prepare a semicolon-separated string of email
        // addresses.
        $emailAddresses = implode(
            "; ",
            array_map(function ($recipient) {
                return $recipient["address"];
            }, $this->to)
        );

        // Add some extra parameters to the message in
        // order to write them in the logs.
        $this->withSymfonyMessage(function (Email $message) use (
            $mailableClass
        ) {
            $message->mailable_class = $mailableClass;
            $message->sent_at = date("Y-m-d H:i:s");
        });

        Log::info("OK Sending $mailableClass to {$emailAddresses}");

        parent::send($mailer);
    }

    protected function buildSubject($message): BaseMailable
    {
        if ($this->title && !$this->subject) {
            $message->subject("LocoMotion - $this->title");
            return $this;
        }

        return parent::buildSubject($message);
    }

    protected function buildView(): array|string
    {
        if (!$this->textView && $this->view) {
            $this->text("emails.generic_email_text", ["view" => $this->view]);
        }

        return parent::buildView();
    }
}
