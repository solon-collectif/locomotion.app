<?php

namespace App\Mail;

use App\Enums\LoanableUserRoles;
use App\Enums\LoanNotificationLevel;
use App\Models\Loan;
use App\Models\User;
use Closure;
use Illuminate\Mail\Mailable;

class UserMail
{
    /**
     * Adds a task to the queue to send the input mail. Use this when mail is not
     * critical and could wait about a minute to be sent.
     */
    public static function queue(Mailable $mailable, User $user): void
    {
        if ($user->trashed()) {
            return;
        }
        \Mail::queue($mailable->to($user->email, $user->full_name));
    }

    /**
     * Sends mail immediately. Use this when mail is critical (such as password reset)
     * or is already called from the queue.
     */
    public static function send(Mailable $mailable, User $user): void
    {
        if ($user->trashed()) {
            return;
        }
        \Mail::send($mailable->to($user->email, $user->full_name));
    }

    /**
     * Queues a mailable to all co-owners and owners of a loan's loanable which aren't the borrower
     * and are not deleted. This sends an email with all the recipients in the 'to' field, rather
     * than multiple individual emails.
     *
     * @param Mailable $mailable
     * @param Loan $loan
     * @return void
     */
    public static function queueToLoanOwners(
        Mailable $mailable,
        Loan $loan
    ): void {
        $recipients = [];

        $userRoles = $loan->loanable
            ->mergedUserRoles()
            ->whereIn("role", [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Coowner,
            ])
            ->with("user")
            ->get();

        foreach ($userRoles->map(fn($role) => $role->user) as $user) {
            if ($user->trashed() || $user->is($loan->borrowerUser)) {
                continue;
            }
            $recipients[] = [
                "name" => $user->name,
                "email" => $user->email,
            ];
        }

        if (count($recipients) > 0) {
            \Mail::queue($mailable->to($recipients));
        }
    }

    public static function queueToLoanNotifiedUsers(
        Closure $mailableFunction,
        Loan $loan,
        ?User $exception = null,
        array $acceptedLevels = [LoanNotificationLevel::All]
    ): void {
        foreach ($loan->notifiedUsers as $user) {
            if (!in_array($user->pivot->level, $acceptedLevels)) {
                continue;
            }

            if ($exception && $user->id === $exception->id) {
                continue;
            }

            self::queue($mailableFunction($user), $user);
        }
    }

    public static function queueToLoanBorrower(
        Mailable $mailable,
        Loan $loan
    ): void {
        $borrowerNotificationSettings = $loan->notifiedUsers
            ->filter(fn($user) => $user->id === $loan->borrower_user_id)
            ->first();
        if (
            $borrowerNotificationSettings &&
            $borrowerNotificationSettings->pivot->level ===
                LoanNotificationLevel::All
        ) {
            self::queue($mailable, $loan->borrowerUser);
        }
    }
}
