<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Mail\ICalHelper;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanCanceledMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $sender,
        public User $receiver,
        public Loan $loan
    ) {
        $this->title = "Emprunt annulé";
    }

    public function build(): self
    {
        if ($this->loan->accepted_at) {
            $this->attach(
                ICalHelper::generateICSAttachment(
                    $this->loan,
                    $this->receiver,
                    canceled: true
                )
            );
        }

        return $this->view("emails.loan.canceled");
    }
}
