<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionRequestedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Loan $loan, public User $borrowerUser)
    {
        $this->title = "Demande de prolongation";

        $this->view("emails.loan.extension_created");
    }
}
