<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanRequestedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Loan $loan,
        public ?string $messageForOwner
    ) {
        $this->title = "Nouvelle demande d'emprunt";

        $this->view("emails.loan.requested");
    }
}
