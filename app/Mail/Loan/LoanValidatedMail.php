<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LoanValidatedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public function __construct(
        public Loan $loan,
        public ?User $validator = null
    ) {
        $this->title =
            $this->loan->borrower_must_pay_insurance ||
            $this->loan->borrower_must_pay_compensation
                ? "Paiement requis"
                : "Contribuez à ce que LocoMotion continue à rouler";

        $this->view("emails.loan.validated");
    }
}
