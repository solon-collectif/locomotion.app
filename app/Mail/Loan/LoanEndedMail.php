<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LoanEndedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public bool $recipientIsBorrower;
    public function __construct(public Loan $loan, public User $recipient)
    {
        $this->recipientIsBorrower = $loan->borrower_user_id === $recipient->id;
        $this->title = "Données d'emprunt à valider";

        $this->view("emails.loan.ended");
    }
}
