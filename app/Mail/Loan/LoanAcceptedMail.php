<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanAcceptedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public bool $recipientIsBorrower;

    public function __construct(
        public Loan $loan,
        public User $recipient,
        public ?User $accepter = null,
        public ?string $comment = null
    ) {
        $this->recipientIsBorrower =
            $this->recipient->id === $this->loan->borrowerUser->id;
        $this->title = "Emprunt accepté";
    }

    public function build(): self
    {
        return $this->view("emails.loan.accepted");
    }
}
