<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanPrePaymentMissingMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user, public Loan $loan)
    {
        $this->title =
            "Merci de faire le prépaiement avant d'emprunter le " .
            "véhicule de votre voisin-e";

        $this->view("emails.loan.pre_payment_missing");
    }
}
