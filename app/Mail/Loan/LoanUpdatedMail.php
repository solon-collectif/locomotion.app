<?php

namespace App\Mail\Loan;

use App\Enums\LoanStatus;
use App\Mail\BaseMailable;
use App\Mail\ICalHelper;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanUpdatedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public bool $borrowerIsUpdater;
    public bool $recipientIsBorrower;
    public bool $needsApproval;
    public bool $needsRecipientValidation;
    public bool $needsValidation;
    public bool $hasUpdatedDuration;
    public bool $hasUpdatedDeparture;
    public bool $recipientIsOwnerOrCoowner;
    public bool $recipientIsUpdater;
    public bool $needsRecipientApproval;

    public function __construct(
        public User $updater,
        public User $recipient,
        public Loan $loan,
        public bool $approvalReset,
        public array $initialLoan
    ) {
        $this->borrowerIsUpdater =
            $this->loan->borrower_user_id === $this->updater->id;
        $this->recipientIsBorrower =
            $this->loan->borrower_user_id === $this->recipient->id;
        $this->recipientIsUpdater = $this->updater->id === $this->recipient->id;
        $this->needsApproval = !$this->loan->is_accepted;

        $this->recipientIsOwnerOrCoowner = $this->loan->loanable->hasOwnerOrCoowner(
            $this->recipient
        );

        $this->needsValidation =
            $this->loan->needs_validation &&
            $this->loan->status === LoanStatus::Ended;
        $this->needsRecipientValidation =
            $this->needsValidation &&
            (($this->recipientIsBorrower &&
                !$this->loan->borrower_validated_at) ||
                ($this->recipientIsOwnerOrCoowner &&
                    !$this->loan->owner_validated_at));

        $this->needsRecipientApproval =
            $this->needsApproval && $this->recipientIsOwnerOrCoowner;

        $this->hasUpdatedDuration =
            $this->initialLoan["duration_in_minutes"] !==
            $this->loan->duration_in_minutes;
        $this->hasUpdatedDeparture = !$this->loan->departure_at->isSameMinute(
            $this->initialLoan["departure_at"]
        );

        if ($this->needsRecipientApproval) {
            $this->title = "Emprunt mis à jour - Approbation Requise!";
        } elseif ($this->needsRecipientValidation) {
            $this->title = "Emprunt mis à jour - Validation Requise!";
        } else {
            $this->title = "Emprunt mis à jour";
        }
    }

    public function build(): self
    {
        //If params requiring a new calendar invite have changed
        if ($this->hasUpdatedDuration || $this->hasUpdatedDeparture) {
            if ($this->approvalReset) {
                $this->attach(
                    ICalHelper::generateICSAttachment(
                        $this->loan,
                        $this->recipient,
                        canceled: true
                    )
                );
            } elseif (!$this->needsApproval) {
                $this->attach(
                    ICalHelper::generateICSAttachment(
                        $this->loan,
                        $this->recipient
                    )
                );
            }
            // Else: if loan still needs approval and approval has not been reset,
            // there is no calendar event to cancel or to confirm
        }

        return $this->view("emails.loan.updated");
    }
}
