<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Mail\ICalHelper;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanConfirmedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public bool $recipientIsBorrower;

    public function __construct(
        public User $recipient,
        public Loan $loan,
        // LoanConfirmedMail can be sent directly after accepting if no prepayment is required,
        // hence the accepter and their comment
        public ?User $accepter = null,
        public ?string $comment = null
    ) {
        $this->recipientIsBorrower = $loan->borrower_user_id === $recipient->id;
        $this->title = "Emprunt confirmé!";
    }

    public function build(): self
    {
        return $this->view("emails.loan.confirmed")->attach(
            ICalHelper::generateICSAttachment($this->loan, $this->recipient)
        );
    }
}
