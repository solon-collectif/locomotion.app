<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanRejectedMail extends BaseMailable
{
    use Queueable, SerializesModels;
    public User $borrowerUser;

    public function __construct(
        public Loan $loan,
        public User $rejecter,
        public ?string $comment
    ) {
        $this->title = "Emprunt refusé";
        $this->borrowerUser = $this->loan->borrowerUser;

        $this->view("emails.loan.rejected");
    }
}
