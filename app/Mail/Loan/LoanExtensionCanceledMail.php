<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionCanceledMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Loan $loan)
    {
        $this->title = "Demande de prolongation annulée";

        $this->view("emails.loan.extension_canceled");
    }
}
