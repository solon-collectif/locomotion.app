<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanNeedsPaymentMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Loan $loan)
    {
        $this->title = "Emprunt à payer!";

        $this->view("emails.loan.needs_payment");
    }
}
