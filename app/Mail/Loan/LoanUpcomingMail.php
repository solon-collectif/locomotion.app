<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Mail\ICalHelper;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanUpcomingMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public bool $recipientIsBorrower = false;
    public bool $recipientIsOwnerOrCoowner = false;
    public function __construct(public User $recipient, public Loan $loan)
    {
        $this->recipientIsBorrower =
            $this->loan->borrower_user_id === $this->recipient->id;
        $this->recipientIsOwnerOrCoowner = $this->loan->loanable->hasOwnerOrCoowner(
            $this->recipient
        );

        if ($this->recipientIsBorrower) {
            $this->title = "Votre réservation commence dans moins de 3 heures!";
        } elseif ($this->recipientIsOwnerOrCoowner) {
            $this->title =
                "Un emprunt de votre véhicule commence dans 3 heures!";
        } else {
            $this->title = "L'emprunt débute dans 3 heures!";
        }
    }

    public function build(): self
    {
        $this->attach(
            ICalHelper::generateICSAttachment($this->loan, $this->recipient)
        );

        return $this->view("emails.loan.upcoming");
    }
}
