<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanStalledMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Loan $loan, public User $receiver)
    {
        $this->title = "Emprunt à compléter";

        $this->view("emails.loan.stalled");
    }
}
