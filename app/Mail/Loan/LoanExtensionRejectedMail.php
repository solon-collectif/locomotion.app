<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionRejectedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public User $borrowerUser;
    public bool $recipientIsBorrower;

    public function __construct(
        public Loan $loan,
        public User $recipient,
        public User $refuser
    ) {
        $this->borrowerUser = $this->loan->borrowerUser;
        $this->recipientIsBorrower =
            $this->recipient->id === $loan->borrower_user_id;
        $this->title = "Demande de prolongation refusée";

        $this->view("emails.loan.extension_rejected");
    }
}
