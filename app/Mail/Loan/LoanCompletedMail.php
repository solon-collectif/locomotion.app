<?php

namespace App\Mail\Loan;

use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanCompletedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public string $link;
    public bool $recipientIsOwner = false;
    public bool $recipientIsCoowner = false;
    public bool $recipientIsBorrower = false;

    public function __construct(public User $recipient, public Loan $loan)
    {
        $this->loan->load(["ownerInvoice", "borrowerInvoice"]);

        $this->recipientIsOwner = $loan->loanable->userHasRole(
            $this->recipient,
            LoanableUserRoles::Owner
        );
        $this->recipientIsCoowner = $loan->loanable->userHasRole(
            $this->recipient,
            LoanableUserRoles::Coowner
        );

        $this->recipientIsBorrower =
            $loan->borrower_user_id === $this->recipient->id;

        $this->title = "Emprunt Complété 🎉";
        $loanableType = urlencode(
            self::formatLoanableType($this->loan->loanable->type)
        );

        $role = urlencode(
            $this->recipientIsOwner || $this->recipientIsCoowner
                ? "Propriétaire"
                : "Emprunteur"
        );
        $this->link = "https://docs.google.com/forms/d/e/1FAIpQLSdaErI3R0BaDePfJvWfedX_4kXkX2uLs9yHqckqGZ9mZyME1Q/viewform?usp=pp_url&entry.59820943=$loanableType&entry.1498982694=$role&entry.370978204={$this->recipient->email}&entry.225549194=";

        $this->view("emails.loan.completed");
    }

    private static function formatLoanableType(LoanableTypes $type): string
    {
        return match ($type) {
            LoanableTypes::Car => "Auto",
            LoanableTypes::Bike => "Vélo",
            LoanableTypes::Trailer => "Remorque",
        };
    }
}
