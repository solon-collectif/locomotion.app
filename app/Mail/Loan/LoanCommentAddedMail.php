<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\LoanComment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanCommentAddedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public LoanComment $comment,
        public User $recipient
    ) {
        $this->title = "Nouveau message de {$comment->author->name}";

        $this->view("emails.loan.comment_added");
    }
}
