<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Mail\ICalHelper;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionAcceptedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public bool $recipientIsBorrower;
    public bool $recipientIsAccepter;
    public User $borrowerUser;

    public function __construct(
        public Loan $loan,
        public User $recipient,
        public User $accepter,
        public int $previousDuration
    ) {
        $this->borrowerUser = $this->loan->borrowerUser;
        $this->recipientIsBorrower =
            $this->recipient->id === $this->borrowerUser->id;
        $this->recipientIsAccepter = $this->recipient->is($this->accepter);
        $this->title = "Demande de prolongation acceptée";
    }

    public function build(): self
    {
        return $this->view("emails.loan.extension_accepted")->attach(
            ICalHelper::generateICSAttachment($this->loan, $this->recipient)
        );
    }
}
