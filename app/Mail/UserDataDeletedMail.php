<?php

namespace App\Mail;

use App\Enums\ReportType;
use App\Models\UserPublishableReport;
use Carbon\CarbonImmutable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Attachment;
use Illuminate\Queue\SerializesModels;

class UserDataDeletedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public string $name,
        public ?string $incomeReport = null
    ) {
        $this->title = "Données supprimées définitivement";
        $year = CarbonImmutable::now()->year;
        if ($incomeReport) {
            $this->attach(
                Attachment::fromData(
                    fn() => $incomeReport,
                    "Revenus_$year"
                )->withMime("application/pdf")
            );
        }
    }

    public function build(): self
    {
        return $this->view("emails.user.data-deleted");
    }
}
