<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class TrustedBorrowerRemovedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $exTrustedBorrower,
        public Loanable $loanable,
        public User $remover
    ) {
        $this->title = "Vous avez été retiré du réseau de confiance de {$this->loanable->name}";

        $this->view("emails.loanable.trusted_borrower_removed");
    }
}
