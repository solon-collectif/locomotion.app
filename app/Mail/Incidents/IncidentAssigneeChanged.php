<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\Incident;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class IncidentAssigneeChanged extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Incident $incident,
        public User $recipient,
        public User $assignedBy
    ) {
        $this->title = "Incident #{$this->incident->id} vous a été assigné";

        $this->view("emails.incidents.assignee_changed");
    }
}
