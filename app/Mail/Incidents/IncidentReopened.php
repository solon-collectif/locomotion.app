<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\Incident;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class IncidentReopened extends BaseMailable
{
    use Queueable, SerializesModels;
    public Loanable $loanable;
    public User $reporter;

    public function __construct(
        public Incident $incident,
        public User $recipient,
        public user $reopener
    ) {
        $this->loanable = $this->incident->loanable;
        $this->title = "Incident #{$this->incident->id} réouvert";
        $this->reporter = $incident->reportedByUser;

        $this->view("emails.incidents.incident_reopened");
    }
}
