<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\IncidentNote;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class NoteAdded extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public IncidentNote $incidentNote,
        public User $recipient
    ) {
        $this->title = "Mise à jour de l'incident #{$this->incidentNote->incident_id}";
        $this->view("emails.incidents.note_added");
    }
}
