<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\Incident;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class LoansBlocked extends BaseMailable
{
    use Queueable, SerializesModels;
    public User $recipient;
    public Collection $blockedLoans;
    public Incident $incident;

    public function __construct(
        User $recipient,
        Collection $blockedLoansList,
        Incident $incident
    ) {
        $this->recipient = $recipient;
        $this->blockedLoans = $blockedLoansList;
        $this->incident = $incident;

        $this->title = "Emprunts bloqués pour {$incident->loanable->name}";
        $this->view("emails.incidents.loans_blocked");
    }
}
