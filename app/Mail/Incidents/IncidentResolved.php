<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\Incident;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class IncidentResolved extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public Incident $incident,
        public User $recipient
    ) {
        $this->title = "Incident #{$this->incident->id} résolu";

        $this->view("emails.incidents.incident_resolved");
    }
}
