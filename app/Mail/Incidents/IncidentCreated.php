<?php

namespace App\Mail\Incidents;

use App\Mail\BaseMailable;
use App\Models\Incident;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class IncidentCreated extends BaseMailable
{
    use Queueable, SerializesModels;

    public ?Loan $loan;
    public Loanable $loanable;
    public ?User $borrowerUser;
    public User $reporter;

    public function __construct(
        public Incident $incident,
        public User $recipient
    ) {
        $this->loan = $incident->loan;
        $this->loanable = $this->incident->loanable;
        $this->borrowerUser = $this->loan?->borrowerUser;
        $this->title = "Rapport d'incident #{$this->incident->id}";
        $this->reporter = $incident->reportedByUser;

        $this->view("emails.incidents.incident_created");
    }
}
