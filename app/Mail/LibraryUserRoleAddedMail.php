<?php

namespace App\Mail;

use App\Models\Library;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LibraryUserRoleAddedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $adder,
        public User $added,
        public Library $library
    ) {
        $this->title = "Nouveaux droits de gestion d'une flotte";

        $this->view("emails.loanable.library_user_role_added");
    }
}
