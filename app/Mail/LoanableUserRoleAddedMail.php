<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LoanableUserRoleAddedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $adder,
        public User $added,
        public Loanable $loanable
    ) {
        $this->title = "Nouveaux droits de gestion d'un véhicule";

        $this->view("emails.loanable.loanable_user_role_added");
    }
}
