<?php

namespace App\Mail\Borrower;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ApprovedToAdmin extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user)
    {
        $this->title = "Dossier de conduite approuvé!";
        $this->view("emails.borrower.approved_to_admin");
    }
}
