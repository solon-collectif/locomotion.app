<?php

namespace App\Mail\Borrower;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Reviewable extends BaseMailable
{
    use Queueable, SerializesModels;
    public function __construct(public User $user)
    {
        $this->title = "{$this->user->full_name} a complété son dossier de conduite.";

        $this->view("emails.borrower.reviewable");
    }
}
