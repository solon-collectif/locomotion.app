<?php

namespace App\Mail\Borrower;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Approved extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user)
    {
        $this->title = "Empruntez dès maintenant l'auto de vos voisin-es!";
        $this->view("emails.borrower.approved");
    }
}
