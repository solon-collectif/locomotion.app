<?php

namespace App\Mail\Borrower;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Completed extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user)
    {
        $this->title = "Dossier de conduite complété";
        $this->view("emails.borrower.completed");
    }
}
