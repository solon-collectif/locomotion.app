<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class PasswordRequest extends BaseMailable
{
    use Queueable, SerializesModels;

    public string $route;
    public float $expiration;

    public function __construct(public User $user, public $token)
    {
        $encodedEmail = urlencode($this->user->email);
        $this->route =
            config("app.url") .
            "/password/reset?token=$this->token&email={$encodedEmail}";
        $this->expiration = floor(
            config(
                "auth.passwords." .
                    config("auth.defaults.passwords") .
                    ".expire"
            ) / 60
        );
        $this->title = "Réinitialisation de mot de passe";

        $this->view("emails.password.request");
    }
}
