<?php

namespace App\Mail;

use App\Helpers\Formatter;
use App\Helpers\Path;
use App\Models\Loan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Mail\Attachment;

class ICalHelper
{
    public static function generateICalData(
        Loan $loan,
        User $recipient,
        bool $canceled
    ): string {
        $recipientIsBorrower = $recipient->is($loan->borrowerUser);

        $location = implode(",", $loan->loanable->position->latLong());
        $now = Carbon::now();
        $link = Path::build(config("app.url"), "loans", $loan->id);

        $summary = "[LocoMotion] Emprunt de {$loan->loanable->name} par {$loan->borrowerUser->name}";
        if ($recipientIsBorrower) {
            $summary = "[LocoMotion] Mon emprunt de {$loan->loanable->name}";
        }

        $status = "STATUS:CANCELLED";
        $method = "METHOD:CANCEL";
        $description = "Emprunt: $link";
        $htmlDescription = "";

        if (!$canceled) {
            $description = view("components.text.calendar-loan", [
                "loan" => $loan,
                "recipientIsBorrower" => $recipientIsBorrower,
                "link" => $link,
            ])->toHtml();
            $htmlDescription = Formatter::trimEachLine(
                view("components.calendar-loan", [
                    "loan" => $loan,
                    "recipientIsBorrower" => $recipientIsBorrower,
                    "link" => $link,
                ])->toHtml()
            );
            $status = "STATUS:CONFIRMED";
            $method = "METHOD:REQUEST";
        }

        // Ensure we override old events with updated data. This may overflow if sequence numbers
        // are parsed in 32 bits, if the loan is updated > 136 years after its creation
        // (I think we're safe).
        $sequenceNumber = (int) $loan->created_at->diffInMinutes(
            $loan->updated_at
        );

        $attendee = $canceled
            ? []
            : [
                "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;CN=$recipient->full_name:MAILTO:$recipient->email",
            ];

        $icalData = [
            "BEGIN:VCALENDAR",
            "VERSION:2.0",
            "PRODID:-//Reseau LocoMotion//locomotion.app//EN",
            $method,
            "BEGIN:VEVENT",
            $status,
            "DTSTART:{$loan->departure_at->format("Ymd\THis\Z")}",
            "DTEND:{$loan->actual_return_at->format("Ymd\THis\Z")}",
            "LOCATION:$location",
            "TRANSP:OPAQUE",
            "ORGANIZER;CN=LocoMotion:MAILTO:emprunts@locomotion.app",
            ...$attendee,
            "SEQUENCE:$sequenceNumber",
            "UID:loan-$loan->id@locomotion.app",
            "DTSTAMP:{$now->format("Ymd\THis\Z")}",
            "SUMMARY:$summary",
            "X-ALT-DESC;FMTTYPE=text/html:$htmlDescription",
            "DESCRIPTION:$description",
            "PRIORITY:1",
            "CLASS:PUBLIC",
            "END:VEVENT",
            "END:VCALENDAR",
        ];

        return implode("\n", array_map(self::foldICalLine(...), $icalData));
    }

    public static function generateICSAttachment(
        Loan $loan,
        User $recipient,
        bool $canceled = false
    ): Attachment {
        return Attachment::fromData(
            fn() => self::generateICalData($loan, $recipient, $canceled),
            "invite.ics"
        )->withMime("text/calendar");
    }

    // Adapted from https://stackoverflow.com/questions/48696534/ics-content-line-validation-and-html-in-description
    private static function foldICalLine(string $line): string
    {
        $value = str_replace("\n", "\\n", trim($line));
        $lines = [];
        while (strlen($value) > 75) {
            $line = mb_substr($value, 0, 73); // less than 75, to leave space for CRLF
            $llength = mb_strlen($line); //  must use mb_strlen with mb_substr otherwise will not work things like &nbsp;
            $lines[] = $line . chr(13) . chr(10) . chr(32); /* CRLF and space*/
            $value = mb_substr(
                $value,
                $llength
            ); /* set value to what's left of the string */
        }

        if (!empty($value)) {
            $lines[] = $value; /* the last line does not need a white space */
        }
        return implode($lines);
    }
}
