<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanablePublished extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user, public Loanable $loanable)
    {
        $this->title = "Véhicule publié";

        $this->view("emails.loanable.published");
    }
}
