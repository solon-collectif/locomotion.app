<?php

namespace App\Mail;

use App\Models\Payout;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserBalanceClaimReceived extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Payout $payout)
    {
        $this->title = "Demande de paiement reçue!";

        $this->view("emails.user.balance_claim_received");
    }
}
