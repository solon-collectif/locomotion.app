<?php

namespace App\Mail;

use App\Helpers\Path;
use App\Models\Export;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ExportCompletedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public string $url;

    public function __construct(public Export $export)
    {
        $this->url = Path::join(
            config("app.url"),
            "admin/exports?id={$this->export->id}"
        );
        $this->title = "Export Complété";

        $this->view("emails.export-completed");
    }
}
