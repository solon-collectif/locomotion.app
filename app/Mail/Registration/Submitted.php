<?php

namespace App\Mail\Registration;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Submitted extends BaseMailable
{
    use Queueable, SerializesModels;

    public $chatGroupUrl;
    public $startingGuideUrl;

    public function __construct(public User $user)
    {
        $this->title = "Bienvenue dans LocoMotion, vous y êtes presque!";
        if ($user->main_community) {
            $this->startingGuideUrl = $user->main_community->starting_guide_url;
            $this->chatGroupUrl = $user->main_community->chat_group_url;
        }

        $this->view("emails.registration.submitted");
    }
}
