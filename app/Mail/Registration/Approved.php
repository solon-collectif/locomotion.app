<?php

namespace App\Mail\Registration;

use App\Mail\BaseMailable;
use App\Models\Community;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Approved extends BaseMailable
{
    use Queueable, SerializesModels;

    public User $user;
    public Community $community;
    public ?string $startingGuideUrl;
    public int $userCount;
    public int $communityCount;

    public function __construct(CommunityUser $communityUser)
    {
        $this->title = "Bienvenue dans LocoMotion, c'est parti!";
        $this->user = $communityUser->user;
        $this->community = $communityUser->community;
        $this->startingGuideUrl = $communityUser->community->starting_guide_url;
        $this->communityCount = Community::where("type", "borough")->count();
        $this->userCount = User::whereHas("approvedCommunities")->count();

        $this->view("emails.registration.approved");
    }
}
