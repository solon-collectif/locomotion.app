<?php

namespace App\Mail\Registration;

use App\Mail\BaseMailable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Rejected extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user)
    {
        $this->title = "Votre inscription a été refusée";

        $this->view("emails.registration.rejected");
    }
}
