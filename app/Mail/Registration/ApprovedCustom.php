<?php

namespace App\Mail\Registration;

use App\Mail\BaseMailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ApprovedCustom extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public string $markdownContent)
    {
        $this->title = "Votre compte LocoMotion est validé!";
        $this->view("emails.registration.approved_custom");
    }
}
