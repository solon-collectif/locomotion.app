<?php

/**
 * App\Mail\Registration\Reviewable
 *
 * Event triggered when the user has completed its registration.
 * Send a notification to the community administrators so they can approve the registration.
 *
 */

namespace App\Mail\Registration;

use App\Mail\BaseMailable;
use App\Models\Community;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Reviewable extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public User $user, public Community $community)
    {
        $this->title =
            $this->user->full_name .
            " s'est inscrit-e dans " .
            $this->community->name;

        $this->view("emails.registration.reviewable");
    }
}
