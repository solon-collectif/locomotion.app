<?php

namespace App\Mail\Invitation;

use App\Helpers\Path;
use App\Mail\BaseMailable;
use App\Models\Invitation;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserInvitedMail extends BaseMailable
{
    use Queueable, SerializesModels;
    public string $communityName;
    public string $invitationUrl;
    public string $senderName;
    public Carbon $expiration;

    public ?string $invitationMessage;

    public function __construct(public Invitation $invitation)
    {
        $this->senderName = $this->invitation->inviter->name;
        $this->communityName = $this->invitation->community->name;
        $this->invitationMessage = $this->invitation->message;
        $this->expiration = $this->invitation->expires_at;
        $this->invitationUrl =
            Path::join(config("app.url"), "/invitation/accept") .
            "?id={$this->invitation->id}&code={$this->invitation->confirmation_code}";

        $this->title = "Rejoignez $this->communityName!";

        if ($this->invitation->template === "partial") {
            $this->view("emails.invitation.invited_partial");
        } else {
            $this->view("emails.invitation.invited");
        }
    }
}
