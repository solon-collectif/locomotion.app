<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class TrustedBorrowerAddedMail extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $adder,
        public User $added,
        public Loanable $loanable
    ) {
        $this->title = "Bienvenue dans le réseau de confiance de {$this->loanable->name}";

        $this->view("emails.loanable.trusted_borrower_added");
    }
}
