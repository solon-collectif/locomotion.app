<?php

namespace App\Mail;

use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanableReviewable extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $user,
        public Community $community,
        public Loanable $loanable
    ) {
        $this->title = "Nouveau véhicule publié dans " . $this->community->name;

        $this->view("emails.loanable.reviewable");
    }
}
