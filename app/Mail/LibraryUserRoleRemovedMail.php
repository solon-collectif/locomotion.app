<?php

namespace App\Mail;

use App\Models\Library;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LibraryUserRoleRemovedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $exCoowner,
        public Library $library,
        public User $remover
    ) {
        $this->title = "Droits de gestion de flotte révoqués";

        $this->view("emails.loanable.library_user_role_removed");
    }
}
