<?php

namespace App\Mail;

use App\Enums\ReportType;
use App\Models\UserPublishableReport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ReportPublishedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public string $documentName;

    public function __construct(public UserPublishableReport $userReport)
    {
        $this->documentName = match ($this->userReport->report->type) {
            ReportType::YearlyOwnerIncome
                => "Relevé de revenus {$userReport->report->report_key}",
        };

        $this->title = $this->documentName . " disponible maintenant";
    }

    public function build(): self
    {
        return $this->view("emails.user.report-published");
    }
}
