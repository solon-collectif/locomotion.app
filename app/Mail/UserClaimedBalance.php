<?php

namespace App\Mail;

use App\Models\Payout;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserClaimedBalance extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(public Payout $payout)
    {
        $this->title = "{$this->payout->user->full_name} réclame son solde";

        $this->view("emails.user.claimed_balance");
    }
}
