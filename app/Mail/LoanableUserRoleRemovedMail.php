<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanableUserRoleRemovedMail extends BaseMailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public User $exCoowner,
        public Loanable $loanable,
        public User $remover
    ) {
        $this->title = "Droits de gestion de véhicule révoqués";

        $this->view("emails.loanable.loanable_user_role_removed");
    }
}
