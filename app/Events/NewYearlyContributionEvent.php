<?php

namespace App\Events;

use App\Models\Invoice;
use App\Models\User;

class NewYearlyContributionEvent extends SendInvoiceEmailEvent
{
    public function __construct(User $user, Invoice $invoice)
    {
        $this->user = $user;
        $this->invoice = $invoice;

        $this->title = "Contribution annuelle payée";
        $this->text = <<<TEXT
Vous trouvez ci-contre le relevé de votre plus récente contribution annuelle sur LocoMotion.
TEXT;
    }
}
