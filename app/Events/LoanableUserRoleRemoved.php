<?php

namespace App\Events;

use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class LoanableUserRoleRemoved
{
    use Dispatchable;

    /**
     * @var LoanableUserRole
     */
    public LoanableUserRole $loanableUserRole;
    /**
     * @var User
     */
    public User $remover;

    public function __construct(
        LoanableUserRole $loanableUserRole,
        User $remover
    ) {
        $this->loanableUserRole = $loanableUserRole;
        $this->remover = $remover;
    }
}
