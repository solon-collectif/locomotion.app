<?php

namespace App\Events\Loan;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CanceledEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(public User $canceler, public Loan $loan)
    {
    }
}
