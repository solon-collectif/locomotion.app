<?php

namespace App\Events;

use App\Models\LoanComment;
use Illuminate\Foundation\Events\Dispatchable;

class LoanCommentAddedEvent
{
    use Dispatchable;

    public function __construct(public LoanComment $comment)
    {
    }
}
