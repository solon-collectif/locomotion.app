<?php

namespace App\Events;

use App\Models\Invoice;
use App\Models\User;

abstract class SendInvoiceEmailEvent
{
    public Invoice $invoice;
    public User $user;
    public ?string $title;
    public ?string $text;
}
