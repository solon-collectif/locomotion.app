<?php

namespace App\Events;

use App\Models\Invoice;
use App\Models\User;

class AddedToUserBalanceEvent extends SendInvoiceEmailEvent
{
    public function __construct(User $user, Invoice $invoice)
    {
        $this->user = $user;
        $this->invoice = $invoice;

        $this->title = "Facture payée";
        $this->text = <<<TEXT
Vous trouvez ci-contre le relevé de votre plus récent paiement sur LocoMotion.

Le total a été chargé sur votre carte de crédit. Le montant pour 'Ajout au compte LocoMotion' a été ajouté à votre solde sur locomotion.app. 
TEXT;
    }
}
