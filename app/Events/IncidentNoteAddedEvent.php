<?php

namespace App\Events;

use App\Models\IncidentNote;
use Illuminate\Foundation\Events\Dispatchable;

class IncidentNoteAddedEvent
{
    use Dispatchable;

    public function __construct(public IncidentNote $incidentNote)
    {
    }
}
