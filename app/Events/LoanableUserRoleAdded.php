<?php

namespace App\Events;

use App\Models\Library;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class LoanableUserRoleAdded
{
    use Dispatchable;

    /**
     * @var User
     */
    public User $adder;
    /**
     * @var Loanable
     */
    public Loanable|Library $ressource;
    /**
     * @var LoanableUserRole
     */
    public LoanableUserRole $loanableUserRole;

    public function __construct(
        User $adder,
        Loanable|Library $ressource,
        LoanableUserRole $loanableUserRole
    ) {
        $this->adder = $adder;
        $this->ressource = $ressource;
        $this->loanableUserRole = $loanableUserRole;
    }
}
