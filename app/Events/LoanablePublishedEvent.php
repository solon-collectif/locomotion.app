<?php

namespace App\Events;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LoanablePublishedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $loanable;

    public function __construct(User $user, Loanable $loanable)
    {
        $this->user = $user;
        $this->loanable = $loanable;
    }
}
