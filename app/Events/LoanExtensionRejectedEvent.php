<?php

namespace App\Events;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionRejectedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(public Loan $loan, public User $refuser)
    {
    }
}
