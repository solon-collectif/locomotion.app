<?php

namespace App\Models;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Calendar\AvailabilityHelper;
use App\Calendar\Interval;
use App\Casts\Point;
use App\Casts\PointCast;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\SharingModes;
use App\Transformers\LoanableTransformer;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

/**
 * Soft-deleted (deleted_at not null) loanables are considered archived. They will not be returned
 * for default queries, but they can be restored.
 *
 * @property string $availability_json
 * @property string $availability_mode
 * @property string $comments
 * @property string $instructions
 * @property string $location_description
 * @property string $name
 * @property Carbon $deleted_at
 * @property ?Point $position
 * @property int $owner_id
 */
class Loanable extends BaseModel
{
    use SoftDeletes, HasFactory;
    use HasSqlCasts;

    protected static $transformer = LoanableTransformer::class;

    public static $filterTypes = [
        "id" => "number",
        "name" => "text",
        "type" => LoanableTypes::possibleTypes,
        "deleted_at" => "date",
        "is_deleted" => "boolean",
        "library_id" => "number",
        "max_loan_duration_in_minutes" => "number",
    ];

    public static $rules = [
        "type" => ["required", "in:bike,car,trailer"],
        "comments" => "present",
        "instructions" => "present",
        "return_instructions" => "nullable",
        "location_description" => "present",
        "name" => "required",
        "position" => "",
        "details" => "",
        "shared_publicly" => "boolean",
        "sharing_mode" => ["in:on_demand,self_service,hybrid"],
        "max_loan_duration_in_minutes" => ["nullable", "integer", "min:0"],
    ];

    protected $fillable = [
        "type",
        "availability_json",
        "availability_mode",
        "comments",
        "instructions",
        "return_instructions",
        "trusted_borrower_instructions",
        "sharing_mode",
        "location_description",
        "name",
        "position",
        "shared_publicly",
        "max_loan_duration_in_minutes",
    ];

    /**
     * @return MorphTo<LoanableDetails, $this>
     */
    public function details(): MorphTo
    {
        return $this->morphTo(__FUNCTION__, "type", "id");
    }

    public static function boot()
    {
        parent::boot();

        // Handle archiving loanables.
        self::deleted(function (Loanable $model) {
            LoanablesByTypeAndCommunity::forgetForLoanable($model);

            // Cancel all ongoing loans
            $model
                ->loans()
                ->inProcess()
                ->each(function (Loan $loan) {
                    $loan->setLoanStatusCanceled();
                    $loan->save();
                });
        });

        self::restored(function ($model) {
            $model->loans()->restore();
        });

        self::saving(function (Loanable $loanable) {
            if ($loanable->isDirty(["position"])) {
                $loanable->updateTimezoneFromPosition();
            }
        });

        self::saved(function (Loanable $loanable) {
            if ($loanable->isDirty()) {
                LoanablesByTypeAndCommunity::forgetForLoanable($loanable);
            }
        });
    }

    public function getCommunityIdsAttribute()
    {
        return $this->mergedUserRoles
            ->where("role", LoanableUserRoles::Owner)
            ->flatMap(fn($r) => $r->user->getAccessibleCommunityIds())
            ->unique()
            ->values();
    }

    protected $casts = [
        "type" => LoanableTypes::class,
        "sharing_mode" => SharingModes::class,
        "published" => "boolean",
        "max_loan_duration_in_minutes" => "integer",
    ];

    protected static array $sqlCasts = [
        "position" => PointCast::class,
    ];

    protected $with = ["details"];

    public $computed = [
        "car_insurer",
        "position_google",
        "community_ids",
        "availability_status",
        "can_edit",
        "owner_user",
    ];

    public static $sizes = [
        "loan" => [
            // Aspect ratio 16/10
            "dimensions" => [
                "width" => 352,
                "height" => 220,
            ],
            "resizing" => "crop",
            "format" => "jpg",
        ],
    ];

    public $items = ["owner", "details", "current_loan"];

    public $morphOnes = [
        "image" => "imageable",
    ];

    public $morphManys = [
        "images" => "imageable",
    ];

    public function library(): BelongsTo
    {
        return $this->belongsTo(Library::class);
    }

    // All images
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, "imageable")->orderBy("order");
    }

    // Main image
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->orderBy("order");
    }

    /**
     * @return BelongsTo<Owner, $this>
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(Owner::class);
    }

    /*
     * Use for getOwnerUserAttribute only.
     * */
    public function ownerUsers(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            "loanable_user_roles",
            "ressource_id",
            "user_id"
        )
            ->wherePivot("role", LoanableUserRoles::Owner)
            ->wherePivot("ressource_type", "loanable");
    }

    /**
     * Only use this attribute when the assumption of single owner is
     * necessary, such as when we are transferring a loan contribution from
     * borrower to owner.
     */
    public function getOwnerUserAttribute(): User|null
    {
        if ($this->library) {
            return $this->library->owner_user;
        }

        return $this->ownerUsers->first();
    }

    public $collections = [
        "loans",
        "incidents",
        "active_incidents",
        "merged_user_roles",
    ];

    /**
     * @return HasMany<Loan, $this>
     */
    public function loans(): HasMany
    {
        return $this->hasMany(Loan::class);
    }

    public function nextLoan(): HasOne
    {
        return $this->hasOne(Loan::class)->ofMany(
            ["departure_at" => "min"],
            fn(Builder $q) => $q->where("departure_at", ">=", Carbon::now())
        );
    }

    public function currentLoan(): HasOne
    {
        return $this->hasOne(Loan::class)->ofMany(
            ["id" => "max"],
            fn(Builder $q) => $q
                ->where("status", "in_process")
                // Loan which starts at most in 15 minutes or ended at most 15 mintes ago
                ->where("departure_at", "<=", Carbon::now()->addMinutes(15))
                ->where("actual_return_at", ">", Carbon::now()->subMinutes(15))
        );
    }

    /**
     * @return MorphMany<LoanableUserRole, $this>
     */
    public function userRoles(): MorphMany
    {
        return $this->morphMany(LoanableUserRole::class, "ressource");
    }

    // Used for reading all roles, including the ones inherited from libraries. Should never
    // be written.
    public function mergedUserRoles(): HasMany
    {
        return $this->hasMany(MergedLoanableUserRole::class);
    }

    public function getCoownersAttribute()
    {
        return $this->mergedUserRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->sortBy("id");
    }

    /**
     * @return HasMany<Incident, $this>
     */
    public function incidents(): HasMany
    {
        return $this->hasMany(Incident::class);
    }

    public function activeIncidents(): HasMany
    {
        return $this->hasMany(Incident::class)
            ->where("status", "in_process")
            ->orderBy("id");
    }

    public function hasOwner(User $user): bool
    {
        return $this->userHasRole($user, LoanableUserRoles::Owner);
    }

    public function hasOwnerOrCoowner(User $user): bool
    {
        return $this->userHasSomeRole($user, [
            LoanableUserRoles::Owner,
            LoanableUserRoles::Manager,
            LoanableUserRoles::Coowner,
        ]);
    }

    public function hasTrustedUser(User $user): bool
    {
        return $this->userHasSomeRole($user, [
            LoanableUserRoles::Owner,
            LoanableUserRoles::Manager,
            LoanableUserRoles::Coowner,
            LoanableUserRoles::TrustedBorrower,
        ]);
    }

    public function shouldLoanBeSelfServiceForUser(User $user): bool
    {
        return $this->sharing_mode === SharingModes::SelfService ||
            ($this->sharing_mode === SharingModes::Hybrid &&
                $this->hasTrustedUser($user));
    }

    public function userHasRole(User $user, LoanableUserRoles $role): bool
    {
        return $this->mergedUserRoles
            ->where("user_id", "=", $user->id)
            ->where("role", $role)
            ->isNotEmpty();
    }
    public function userHasSomeRole(User $user, array $roles): bool
    {
        return $this->mergedUserRoles
            ->where("user_id", $user->id)
            ->whereIn("role", $roles)
            ->isNotEmpty();
    }

    public function userGetRoleParams($user): ?array
    {
        $role = $this->mergedUserRoles->where("user_id", $user->id)->first();
        if (!$role) {
            return null;
        }

        return [
            "pays_loan_price" => $role->pays_loan_price,
            "pays_loan_insurance" => $role->pays_loan_insurance,
        ];
    }

    /**
     * Updates current loanable's timezone according to its position.
     *
     * @todo Not Implemented. Once this becomes necessary, we can compute the timezone from the
     *       position using Google APIs for example:
     *       https://developers.google.com/maps/documentation/timezone/requests-timezone
     *
     * @return void
     */
    private function updateTimezoneFromPosition(): void
    {
        $this->timezone = config("app.default_user_timezone");
    }

    public function getAvailabilityRules()
    {
        try {
            return $this->availability_json
                ? json_decode(
                    $this->availability_json,
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                )
                : [];
        } catch (\Throwable) {
            // Logging the error rather than throwing allows the rules to be modified.
            Log::error(
                "Could not parse availability json: \"$this->availability_json\" for loanable id: $this->id."
            );
            return [];
        }
    }

    public function isAvailable(
        Carbon|CarbonImmutable $departureAt,
        int $durationInMinutes,
        int $updatingLoanId = null
    ): bool {
        if (
            !is_null($this->max_loan_duration_in_minutes) &&
            $durationInMinutes > $this->max_loan_duration_in_minutes
        ) {
            return false;
        }

        $returnAt = $departureAt->toImmutable()->addMinutes($durationInMinutes);
        $interval = new Interval($departureAt, $returnAt);

        if (
            !$this->isLoanableScheduleOpen(
                $interval,
                canStartBeforeNow: !is_null($updatingLoanId)
            )
        ) {
            return false;
        }

        if (is_null($updatingLoanId)) {
            // Incidents are only blocking availabilities for new loans.
            if (
                $this->activeIncidents()
                    ->whereNotNull("blocking_until")
                    ->where(
                        "blocking_until",
                        ">",
                        $interval->start->toISOString()
                    )
                    // We do not need to check incident start date, since it's necessarily in the
                    // past and we only end up in this branch for new loans which can only be
                    // created in the future.
                    // However, for more robustness it would be nice to check against the incident
                    // start date if we ever add it as a separate column, rather than a computed
                    // property.
                    ->count() > 0
            ) {
                return false;
            }
        }

        $query = Loan::where("loanable_id", $this->id);

        if ($updatingLoanId) {
            $query = $query->where("loans.id", "!=", $updatingLoanId);
        }

        $query->isPeriodUnavailable($interval);

        return $query->count() === 0;
    }

    public function isLoanableScheduleOpen(
        Interval $range,
        bool $canStartBeforeNow = false
    ): bool {
        if (!$canStartBeforeNow && $range->start->isBefore(Carbon::now())) {
            return false;
        }

        return AvailabilityHelper::isScheduleAvailable(
            [
                "available" => "always" == $this->availability_mode,
                "rules" => $this->getAvailabilityRules(),
            ],
            $range,
            $this->timezone
        );
    }

    public function getCommunityForLoanBy(User $user): ?Community
    {
        /*
         * Precedence order of communities:
         * 1. Shared private community;
         * 2. Owner's geographic community if borrower is approved;
         * 3. Borrower's geographic community if owner is approved;
         * 4. Any other shared community;
         * 5. If no shared community, use owner's geographic community.
         *
         * In any case, if there is more than one community of the same type, we'll select
         * the last one in which the borrower was approved, and finally the greatest id.
         * We then have a completely defined order.
         * */

        $sharedCommunities = \DB::select(
            "
            SELECT
                communities.id,
                communities.type,
                owner.user_id AS owner_user_id,
                owner.join_method AS owner_join_method,
                borrower.user_id AS borrower_user_id,
                borrower.join_method AS borrower_join_method
            FROM communities
            INNER JOIN community_user AS owner ON owner.community_id = communities.id
            INNER JOIN community_user AS borrower ON borrower.community_id = communities.id
            WHERE owner.user_id = ?
            AND borrower.user_id = ?
            AND owner.approved_at IS NOT NULL AND owner.suspended_at IS NULL
            AND borrower.approved_at IS NOT NULL AND borrower.suspended_at IS NULL
            AND communities.deleted_at is null
            ORDER BY owner.approved_at DESC, borrower.approved_at DESC, id DESC",
            [$user->id, $this->owner_user->id]
        );

        if ($this->library) {
            $libraryCommunityIds = $this->library->communities
                ->pluck("id")
                ->toArray();
            $sharedCommunities = array_filter(
                $sharedCommunities,
                fn($community) => in_array($community->id, $libraryCommunityIds)
            );
        }

        $loanCommunity = null;
        $loanCommunityPrecedence = 999;
        foreach ($sharedCommunities as $community) {
            // 1. Shared private community.
            if (
                $community->type === "private" &&
                $loanCommunityPrecedence > 1
            ) {
                $loanCommunity = $community;
                $loanCommunityPrecedence = 1;
                break;
            }
            // 2. Owner's geographic community if borrower is approved.
            elseif (
                $community->owner_join_method === "geo" &&
                $loanCommunityPrecedence > 2
            ) {
                $loanCommunity = $community;
                $loanCommunityPrecedence = 2;
            }
            // 3. Borrower's geographic community if owner is approved.
            elseif (
                $community->borrower_join_method === "geo" &&
                $loanCommunityPrecedence > 3
            ) {
                $loanCommunity = $community;
                $loanCommunityPrecedence = 3;
            }
            // 4. Any other shared community.
            elseif ($loanCommunityPrecedence > 4) {
                $loanCommunity = $community;
                $loanCommunityPrecedence = 4;
            }
        }

        // 5. If no shared community, use owner's geographic community.
        // This case might happen if borrower is co-owner of loanable but doesn't share any
        // communities with the owner.
        // Don't account for cases where owner or user would be in no community.
        return $loanCommunity
            ? Community::find($loanCommunity->id)
            : $this->owner_user->geo_community;
    }

    public function getCanEditAttribute()
    {
        /** @var ?User $user */
        $user = \Auth::user();
        if ($user) {
            return $this->hasOwnerOrCoowner($user);
        }
        return false;
    }

    public function getCarInsurerAttribute()
    {
        if ($this->details && is_a($this->details, Car::class)) {
            return $this->details->insurer;
        }

        return null;
    }

    public function getPositionGoogleAttribute()
    {
        if (!$this->position) {
            return null;
        }

        return [
            "lat" => $this->position->lat,
            "lng" => $this->position->long,
        ];
    }

    public function handleInstructionVisibility(User $user, Loan $loan = null)
    {
        if (
            $user->can("viewInstructions", $this) ||
            ($loan && $user->can("viewInstructions", $loan))
        ) {
            $this->makeVisible("instructions");
            $this->makeVisible("return_instructions");
            if ($user->can("viewTrustedBorrowerInstructions", $this)) {
                $this->makeVisible("trusted_borrower_instructions");
            } else {
                $this->makeHidden("trusted_borrower_instructions");
            }
        } else {
            $this->makeHidden("instructions");
            $this->makeHidden("return_instructions");
            $this->makeHidden("trusted_borrower_instructions");
        }
    }

    /**
     * @return Builder<Loanable>
     */
    public function scopeWhereHasOwnerUser(
        Builder $query,
        Closure $callback
    ): Builder {
        return $query->whereHas(
            "mergedUserRoles",
            fn(Builder $userRoles) => $userRoles
                ->where("role", LoanableUserRoles::Owner)
                ->whereHas("user", $callback)
        );
    }

    /**
     * @return Builder<Loanable>
     */
    public function scopeWhereDoesntHaveOwnerUser(
        Builder $query,
        Closure $callback
    ): Builder {
        return $query->whereDoesntHave(
            "mergedUserRoles",
            fn(Builder $userRoles) => $userRoles
                ->where("role", LoanableUserRoles::Owner)
                ->whereHas("user", $callback)
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @param int $id the owner user id
     * @return Builder<Loanable>
     */
    public function scopeOwnerUserId(Builder $query, int $id): Builder
    {
        return $query->whereHasOwnerUser(
            fn(Builder $q) => $q->where("users.id", $id)
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @return Builder<Loanable>
     */
    public function scopeWhereHasSharedCommunities(
        Builder $query,
        Closure $callback = null
    ): Builder {
        return $query
            ->whereHasOwnerUser(
                fn(Builder $q) => $q->whereHas("approvedCommunities", $callback)
            )
            ->where(
                fn(Builder $q) => $q
                    ->whereNull("library_id")
                    ->orWhereHas("library.communities", $callback)
            );
    }

    /**
     * @param Builder<Loanable> $query
     * @return Builder<Loanable>
     */
    public function scopeWhereDoesntHaveSharedCommunities(
        Builder $query,
        Closure $callback = null
    ): Builder {
        return $query->whereDoesntHaveOwnerUser(
            fn(Builder $q) => $q->whereHas("approvedCommunities", $callback)
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @return Builder<Loanable>
     */
    public function scopeSharedInSomeCommunity(
        Builder $query,
        array $communityIds
    ): Builder {
        return $query->whereHasSharedCommunities(
            fn(Builder $q) => $q->whereIn("communities.id", $communityIds)
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @return Builder<Loanable>
     */
    public function scopeSharedInCommunity(
        Builder $query,
        int $communityId
    ): Builder {
        return $query->whereHasSharedCommunities(
            fn(Builder $q) => $q->where("communities.id", $communityId)
        );
    }

    public function scopeSharedInUserCommunity(Builder $query, User $user)
    {
        return $query->whereHasSharedCommunities(
            fn(Builder $q) => $q->whereHas(
                "approvedUsers",
                fn(Builder $q) => $q->where("users.id", $user->id)
            )
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @param User $user
     * @return Builder<Loanable>
     */
    public function scopeAccessibleBy(Builder $query, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->where(
            fn(Builder $query) => $query
                ->sharedInUserCommunity($user)
                ->orWhere(fn(Builder $q) => $q->ownedOrCoownedBy($user->id))
        );
    }

    // Todo: maybe rename to ManagedBy()
    public function scopeOwnedOrCoownedBy(Builder $query, $userId): Builder
    {
        return $query->whereHas(
            "mergedUserRoles",
            fn(Builder $q) => $q
                ->where("user_id", $userId)
                ->whereIn("role", [
                    LoanableUserRoles::Owner,
                    LoanableUserRoles::Coowner,
                    LoanableUserRoles::Manager,
                ])
        );
    }

    public function getAvailabilityStatusAttribute()
    {
        if (!$this->published) {
            return "unpublished";
        }
        // we can load user's approvedCommunities here since we would load them anyway
        // to get the available loanable types for the owner.
        if ($this->community_ids->count() === 0) {
            return "no_approved_communities";
        }
        if (!$this->has_shared_type) {
            return "wrong_type";
        }
        if (!$this->has_availabilities) {
            return "no_availabilities";
        }
        return "has_availabilities";
    }

    /**
     * @param Builder<Loanable> $query
     * @param $status
     * @return Builder<Loanable>
     */
    public function scopeAvailabilityStatus(Builder $query, $status): Builder
    {
        if ($status === "no_approved_communities") {
            return $query->whereDoesntHaveSharedCommunities();
        }

        if ($status === "wrong_type") {
            return $query->whereHasSharedCommunities()->hasValidType(false);
        }

        if ($status === "no_availabilities") {
            return $query
                ->hasValidType()
                ->where("availability_mode", "=", "never")
                ->whereJsonLength("availability_json", "=", 0);
        }

        if ($status === "unpublished") {
            return $query->where("published", false);
        }

        if ($status === "has_availabilities") {
            return $query
                ->where("published", true)
                ->hasValidType()
                ->hasAvailabilities();
        }
        return $query;
    }

    public function scopeHasValidType(Builder $query, $valid = true)
    {
        if ($valid) {
            return $query->whereHasSharedCommunities(
                fn(Builder $type) => $type->whereHas(
                    "allowedLoanableTypes",
                    fn(Builder $type) => $type->where(
                        "name",
                        \DB::raw("loanables.type")
                    )
                )
            );
        }

        return $query->whereDoesntHaveSharedCommunities(
            fn(Builder $type) => $type->whereHas(
                "allowedLoanableTypes",
                fn(Builder $type) => $type->where(
                    "name",
                    \DB::raw("loanables.type")
                )
            )
        );
    }

    public function getHasSharedTypeAttribute()
    {
        return in_array(
            $this->type->value,
            $this->owner_user->available_loanable_types
        );
    }

    public function getHasAvailabilitiesAttribute()
    {
        return $this->availability_mode != "never" ||
            strlen($this->availability_json) > 2; // [] means no exceptions
    }

    public function scopeHasAvailabilities(Builder $query)
    {
        return $query->where(function ($q) {
            return $q
                ->where("availability_mode", "!=", "never")
                ->orWhereJsonLength("availability_json", ">", 0);
        });
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getTable() . ".name";

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    /**
     * @param Builder<Loanable> $query
     * @param string|null $for
     * @param User $user
     * @return Builder<Loanable>
     */
    public function scopeFor(Builder $query, ?string $for, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit", "admin" => $query->whereHasSharedCommunities(
                fn(Builder $q) => $q->whereHas(
                    "users",
                    fn($q) => $q
                        ->where("community_user.user_id", $user->id)
                        ->whereNotNull("community_user.approved_at")
                        ->whereNull("community_user.suspended_at")
                        ->where("community_user.role", "admin")
                )
            ),
            "owner" => $query->ownerUserId($user->id),
            "profile" => $query->ownedOrCoownedBy($user->id),
            default => $query,
        };
    }

    public function deleteData(): void
    {
        $this->comments = "";
        $this->instructions = "";
        $this->return_instructions = "";
        $this->trusted_borrower_instructions = "";
        $this->position = null;
        $this->location_description = "";
        $this->name = "";
        $this->saveQuietly();
        foreach ($this->images as $image) {
            $image->delete();
        }
        $this->deleteQuietly();
        LoanablesByTypeAndCommunity::forgetForLoanable($this);
    }
}
