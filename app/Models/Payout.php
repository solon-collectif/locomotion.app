<?php

namespace App\Models;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payout extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "invoice_id",
        "kiwili_expense_id",
        "amount",
    ];

    public static $filterTypes = [
        "user_id" => "number",
        "invoice_id" => "number",
        "kiwili_expense_id" => "number",
        "amount" => "number",
        "created_at" => "date",
    ];

    public $items = ["user", "invoice"];

    public $computed = ["status"];

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo<Invoice, $this>
     */
    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function getStatusAttribute(): string
    {
        if ($this->kiwili_expense_id) {
            return "linked";
        }
        return "unlinked";
    }

    public function scopeStatus(Builder $query, $status): Builder
    {
        if ($status === "linked") {
            return $query->whereNotNull("kiwili_expense_id");
        }
        if ($status === "unlinked") {
            return $query->whereNull("kiwili_expense_id");
        }
        return $query;
    }

    public function getBankInfoUpdatedSinceLastPayoutAttribute()
    {
        if (!$this->user->bank_info_updated_at) {
            return false;
        }

        $lastPayout = Payout::where("user_id", $this->user_id)
            ->where("id", "!=", $this->id)
            ->orderBy("created_at", "desc")
            ->first();

        if (!$lastPayout) {
            return true;
        }

        return $this->user->bank_info_updated_at->isBefore(
            $this->created_at ?? CarbonImmutable::now()
        ) &&
            $lastPayout->created_at->isBefore(
                $this->user->bank_info_updated_at
            );
    }
}
