<?php

namespace App\Models;

use App\Enums\BillItemTypes;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    public static $rules = [
        "period" => "required",
        "user_id" => "required",
    ];

    public static $filterTypes = [
        "user_id" => "number",
        "created_at" => "date",
        "paid_at" => "date",
        "total" => "number",
        "items_count" => "number",
        "total_tps" => "number",
        "total_tvq" => "number",
        "total_with_taxes" => "number",
        "period" => "text",
    ];

    protected static array $customJoins = [
        "billItems" => "group",
    ];

    protected $casts = [
        "paid_at" => "immutable_datetime",
    ];

    protected static array $customColumns = [
        "items_count" => "count(%table%_bill_items.id)",
        "total" => "sum(%table%_bill_items.amount)",
        "total_tps" => "sum(%table%_bill_items.taxes_tps)::decimal(8, 2)",
        "total_tvq" => "sum(%table%_bill_items.taxes_tvq)::decimal(8, 2)",
        "total_with_taxes" =>
            "(sum(%table%_bill_items.amount) + sum(%table%_bill_items.taxes_tps)::decimal(8, 2) + sum(%table%_bill_items.taxes_tvq)::decimal(8, 2))::decimal(8, 2)",
    ];

    public static function formatAmountForDisplay($amount)
    {
        return number_format($amount, 2, ",", " ");
    }

    protected $fillable = ["period"];

    public $items = ["payment_method", "user"];

    public $computed = [
        "items_count",
        "total",
        "total_tps",
        "total_tvq",
        "total_with_taxes",
    ];

    /**
     * @return BelongsTo<PaymentMethod, $this>
     */
    public function paymentMethod(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public $collections = ["bill_items"];

    /**
     * @return HasMany<BillItem, $this>
     */
    public function billItems(): HasMany
    {
        return $this->hasMany(BillItem::class);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->whereUserId($user->id);
    }

    public function payWith($paymentMethod)
    {
        if (!$this->paid_at) {
            $this->paymentMethod()->associate($paymentMethod);
            $this->paid_at = CarbonImmutable::now();
            $this->save();
        }
    }

    public function getTotalTpsAttribute()
    {
        if (isset($this->attributes["total_tps"])) {
            return $this->attributes["total_tps"];
        }

        return $this->billItems->sum("taxes_tps");
    }

    public function getTotalTvqAttribute()
    {
        if (isset($this->attributes["total_tvq"])) {
            return $this->attributes["total_tvq"];
        }

        return $this->billItems->sum("taxes_tvq");
    }

    public function getTotalWithTaxesAttribute()
    {
        if (isset($this->attributes["total_with_taxes"])) {
            return $this->attributes["total_with_taxes"];
        }

        return $this->total + $this->total_tps + $this->total_tvq;
    }

    public function getTotalAttribute()
    {
        if (isset($this->attributes["total"])) {
            return $this->attributes["total"];
        }

        return $this->billItems->sum("amount");
    }

    public function getItemsCountAttribute()
    {
        if (isset($this->attributes["items_count"])) {
            return $this->attributes["items_count"];
        }

        return $this->billItems->count();
    }

    public function getUserBalanceChange(): float|int
    {
        $balanceChange = 0;
        foreach ($this->billItems as $invoiceItem) {
            $balanceChange += $invoiceItem->getUserBalanceChange();
        }

        return round($balanceChange, 2);
    }

    public function getUserBalanceChangeForType(
        BillItemTypes $billItemType
    ): float|int {
        $balanceChange = 0;
        foreach ($this->billItems as $invoiceItem) {
            if ($invoiceItem->item_type === $billItemType) {
                $balanceChange += $invoiceItem->getUserBalanceChange();
            }
        }

        return $balanceChange;
    }

    public function hasNonZeroItems()
    {
        foreach ($this->billItems as $invoiceItem) {
            if (abs($invoiceItem->total) >= 0.005) {
                return true;
            }
        }

        return false;
    }
}
