<?php

namespace App\Models;

use App\Helpers\Path;
use App\Http\Controllers\GbfsController;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GbfsDataset extends BaseModel
{
    protected $primaryKey = "name";
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = "string";
    protected $fillable = ["name", "notes"];

    public $collections = ["communities"];

    public $computed = ["link", "communities_count"];
    public $appends = ["link"];
    public static $rules = [
        "name" => ["required", "not_in:new", "alpha_dash"],
    ];

    public static $filterTypes = [
        "name" => "text",
        "notes" => "text",
    ];

    public function communities(): BelongsToMany
    {
        return $this->belongsToMany(
            Community::class,
            "gbfs_dataset_communities"
        )->using(GbfsDatasetCommunity::class);
    }

    public function getCommunitiesCountAttribute(): int
    {
        return $this->communities()->count();
    }

    public function getLinkAttribute(): string
    {
        return Path::url(
            "api/gbfs",
            GbfsController::latestVersion,
            $this->name
        );
    }
}
