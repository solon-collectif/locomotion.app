<?php

namespace App\Models;

use App\Enums\BillItemTypes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rules\Enum;

/**
 * @property string $label
 * @property float $amount
 * @property int $invoice_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property float $taxes_tps
 * @property float $taxes_tvq
 * @property array|null $meta
 * @property BillItemTypes|null $item_type
 */
class BillItem extends BaseModel
{
    use HasFactory;

    protected $casts = [
        "amount" => "float",
        "taxes_tps" => "float",
        "taxes_tvq" => "float",
        "meta" => "array",
        "item_type" => BillItemTypes::class,
    ];

    public static $rules = [
        "label" => "required",
        "amount" => "required|numeric",
        "taxes_tps" => "required|numeric",
        "taxes_tvq" => "required|numeric",
    ];

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            default => [
                ...static::$rules,
                "item_type" => ["required", new Enum(BillItemTypes::class)],
            ],
        };
    }

    protected $fillable = [
        "item_type",
        "label",
        "amount",
        "invoice_id",
        "item_date",
        "payment_id",
        "taxes_tps",
        "taxes_tvq",
        "meta",
        "contribution_community_id",
    ];

    public $items = ["invoice", "payment"];

    /**
     * @return BelongsTo<Invoice, $this>
     */
    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function getTotalAttribute()
    {
        return round($this->amount + $this->taxes_tps + $this->taxes_tvq, 2);
    }

    public function getUserBalanceChange(): float
    {
        // Don't apply Stripe fees to user balance.
        if ($this->item_type === BillItemTypes::feesStripe) {
            return 0;
        }

        return $this->total;
    }
}
