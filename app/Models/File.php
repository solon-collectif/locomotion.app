<?php

namespace App\Models;

use App\Enums\FileMoveResult;
use App\Facades\StorageTransaction;
use App\Helpers\Path;
use App\Helpers\StorageHelper;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\UploadedFile;
use Storage;

class File extends BaseModel
{
    use HasFactory;

    public static $rules = [
        "fileable_type" => "nullable",
        "fileable_id" => "nullable",
        "path" => "required",
        "filename" => "required",
        "original_filename" => "required",
        "filesize" => "required",
        "field" => "nullable",
    ];

    protected $fillable = [
        "field",
        "fileable_id",
        "fileable_type",
        "filename",
        "filesize",
        "original_filename",
        "path",
    ];

    public static $filterTypes = [
        "id" => "number",
        "original_filename" => "text",
    ];

    protected $hidden = ["fileable", "fileable_type", "fileable_id"];

    public static function fetch($path): ?string
    {
        return Storage::get($path);
    }

    public static function store(
        $path,
        \Illuminate\Http\File|UploadedFile $file
    ): bool {
        return StorageTransaction::putFileAs(
            dirname($path),
            $file,
            basename($path)
        );
    }

    public function updateFilePath()
    {
        if ($this->isTemp()) {
            return FileMoveResult::unchanged;
        }

        $filePath = $this->generatePath();
        $newFilename = $this->generateUniqueName();
        $destination = Path::asAbsolute(Path::join($filePath, $newFilename));
        $source = $this->full_path;

        $moveResult = StorageHelper::move($source, $destination);

        if ($moveResult !== FileMoveResult::failed) {
            $this->path = $filePath;
            $this->filename = $newFilename;
        }

        return $moveResult;
    }

    public function generatePath()
    {
        return Path::build(
            "files",
            $this->fileable->getMorphClass(),
            $this->fileable_id,
            $this->field
        );
    }

    public function generateUniqueName()
    {
        return hash("md5", $this->id . config("app.key")) .
            "." .
            pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function (File $model) {
            $copyResult = $model->updateFilePath();
            if ($copyResult === FileMoveResult::failed) {
                throw new Exception(
                    "[File.php] Failed updating file $model->id location."
                );
            }
        });

        self::deleted(function (File $model) {
            $model->deleteFile();
        });
    }

    public function deleteFile(): void
    {
        $deleted = StorageTransaction::delete($this->full_path);

        if (!$deleted) {
            \Log::warning("[File.php] Failed to delete file $this->full_path");
        }
    }

    public $items = ["user"];

    public function fileable(): MorphTo
    {
        return $this->morphTo();
    }

    public function borrower()
    {
        return $this->belongsTo(
            Borrower::class,
            "id",
            "fileable_id"
        )->whereFileableType(Borrower::class);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // File is...
        return $query
            // ...associated to a borrower
            ->where(function ($q) use ($user) {
                return $q->whereHas("borrower", function ($q) use ($user) {
                    return $q->whereHas("user", function ($q) use ($user) {
                        return $q->accessibleBy($user);
                    });
                });
            })
            // ...or a temporary file
            ->orWhere(function ($q) {
                return $q->whereFileableType(null);
            });
    }

    public function getFullPathAttribute(): string
    {
        return Path::join($this->path, $this->filename);
    }

    public function isTemp(): bool
    {
        return !$this->load("fileable")->fileable;
    }

    public function replaceFileData(\Illuminate\Http\File $newFile): bool
    {
        $success = StorageTransaction::putFileAs(
            $this->path,
            $newFile,
            $this->filename
        );
        if ($success && ($size = $newFile->getSize())) {
            $this->filesize = (string) $size;
            $this->save();
        }
        return $success;
    }
}
