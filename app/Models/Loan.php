<?php

namespace App\Models;

use App\Calendar\AvailabilityHelper;
use App\Calendar\CalendarHelper;
use App\Calendar\Interval;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Enums\PricingLoanableTypeValues;
use App\Enums\Requirement;
use App\Pricings\BorrowerApplicableAmountTypes;
use App\Pricings\LoanInvoiceHelper;
use App\Pricings\LoanPricingsHelper;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Prevent directly writing to status
 * @access private
 * @property  LoanStatus $status
 */
class Loan extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    // Amount of time before loan auto-completes (if not prompting for validation, contribution or payment)
    // after expected return time.
    public static $GRACE_PERIOD_MINUTES = 60;

    public static $rules = [
        "departure_at" => ["required"],
        "duration_in_minutes" => ["integer", "required", "min:15"],
        "estimated_distance" => ["integer", "nullable"],
        // Allow setting platform tip later.
        "platform_tip" => ["nullable", "numeric", "min:0"],
        "loanable_id" => "available",
    ];

    public static $filterTypes = [
        "id" => "number",
        "loanable_id" => "number",
        "borrower_id" => "number",
        "duration_in_minutes" => "number",
        "departure_at" => "date",
        "actual_return_at" => "date",
        "calendar_days" => "number",
        "status" => "enum",
        "is_self_service" => "boolean",
        "alternative_to" => "text",
        "community_id" => "number",
    ];

    private ?Invoice $cachedBorrowerInvoice = null;
    private ?Invoice $cachedOwnerInvoice = null;
    private ?LoanPricingsHelper $cachedLoanPricings = null;
    private ?BorrowerApplicableAmountTypes $cachedApplicableAmountTypes = null;

    public static function boot()
    {
        parent::boot();

        self::creating(function (Loan $loan) {
            $loan->is_self_service = $loan->loanable->shouldLoanBeSelfServiceForUser(
                $loan->borrowerUser
            );

            $loan->actual_return_at = $loan->departure_at->addMinutes(
                $loan->duration_in_minutes
            );

            $loan->setLoanStatusRequested();
        });

        self::saving(function (Loan $loan) {
            if (
                $loan->isDirty([
                    "departure_at",
                    "duration_in_minutes",
                    "paid_at",
                ])
            ) {
                $loan->actual_return_at = $loan->departure_at->addMinutes(
                    $loan->duration_in_minutes
                );
                if ($loan->paid_at) {
                    $loan->actual_return_at = $loan->actual_return_at->min(
                        $loan->paid_at
                    );
                }
            }
        });
    }

    protected static array $customJoins = [
        "loanable" => "",
    ];

    protected static array $customColumns = [
        "calendar_days" => <<<SQL
EXTRACT(
    'day'
    FROM
        date_trunc('day', actual_return_at at time zone 'UTC' at time zone %table%_loanables.timezone - interval '1 second')
        - date_trunc('day', departure_at  at time zone 'UTC' at time zone %table%_loanables.timezone)
        + interval '1 day'
)::integer
SQL
    ,
    ];

    // Overload the refresh method to invalidate our cache when loan changes.
    public function refresh()
    {
        $this->cachedLoanPricings = null;
        $this->cachedBorrowerInvoice = null;
        $this->cachedOwnerInvoice = null;
        $this->cachedApplicableAmountTypes = null;
        return parent::refresh();
    }

    public static function getLoanableAgeForInsurance($loanable, $departureAt)
    {
        if (!$loanable || $loanable->type !== LoanableTypes::Car) {
            return 0;
        }

        return $loanable->details->getAgeForInsurance($departureAt);
    }

    public static function getRules($action = "", $auth = null)
    {
        $rules = parent::getRules($action, $auth);
        switch ($action) {
            case "create":
                $rules["community_id"] = "required";
                return $rules;
            default:
                return $rules;
        }
    }

    protected $casts = [
        "accepted_at" => "immutable_datetime",
        "rejected_at" => "immutable_datetime",
        "paid_at" => "immutable_datetime",
        "prepaid_at" => "immutable_datetime",
        "departure_at" => "immutable_datetime",
        "canceled_at" => "immutable_datetime",
        "actual_return_at" => "immutable_datetime",
        "borrower_validated_at" => "immutable_datetime",
        "owner_validated_at" => "immutable_datetime",
        "auto_validated_at" => "immutable_datetime",
        "duration_in_minutes" => "integer",
        "meta" => "array",
        "platform_tip" => "float",
        "status" => LoanStatus::class,
        "mileage_end" => "int",
        "mileage_start" => "int",
        "expenses_amount" => "float",
        "attempt_autocomplete_at" => "immutable_datetime",
    ];

    protected $fillable = [
        "borrower_user_id",
        "canceled_at",
        "community_id",
        "departure_at",
        "duration_in_minutes",
        "estimated_distance",
        "loanable_id",
        "platform_tip",
        "alternative_to",
        "alternative_to_other",
        "mileage_end",
        "mileage_start",
        "expenses_amount",
    ];

    public $computed = [
        "actual_expenses",
        "actual_distance",
        "calendar_days",
        "borrower_total",
        "owner_total",
        "is_free",
        "needs_validation",
        "borrower_must_pay_compensation",
        "borrower_must_pay_insurance",
        "borrower_may_contribute",
        "owner_action_required",
        "borrower_action_required",
    ];

    public $items = ["borrower_user", "community", "loanable"];

    public $collections = ["comments"];

    public function borrowerUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "borrower_user_id")->withTrashed();
    }

    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class)->withTrashed();
    }

    // Incidents declared from this loan
    /**
     * @return HasMany<Incident, $this>
     */
    public function incidents(): HasMany
    {
        return $this->hasMany(Incident::class);
    }

    /**
     * We include archived loanables here, so we can get some information when fetching loans.
     * The @see ArchivedLoanableResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function loanable(): BelongsTo
    {
        return $this->belongsTo(Loanable::class)->withTrashed();
    }

    public function comments(): HasMany
    {
        return $this->hasMany(LoanComment::class)
            ->withTrashed()
            ->orderBy("created_at");
    }

    public function notifiedUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, "loan_notifications")
            ->using(LoanNotification::class)
            ->withTimestamps()
            ->withPivot(["level", "last_seen"]);
    }

    /**
     * @return BelongsTo<Invoice, $this>
     */
    public function borrowerInvoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class, "borrower_invoice_id");
    }

    /**
     * @return BelongsTo<Invoice, $this>
     */
    public function ownerInvoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class, "owner_invoice_id");
    }

    public function mileageStartImage(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "mileage_start_image"
        );
    }

    public function expenseImage(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "expense_image"
        );
    }

    public function mileageEndImage(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "mileage_end_image"
        );
    }

    public function autoValidatedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "auto_validated_by_user_id");
    }

    /**
     * Current distance for the loan, either estimated or real.
     */
    public function getActualDistanceAttribute()
    {
        if (
            $this->requires_detailed_mileage &&
            !is_null($this->mileage_start) &&
            !is_null($this->mileage_end)
        ) {
            return $this->mileage_end - $this->mileage_start;
        }

        return $this->estimated_distance;
    }

    public function getLoanableAgeForInsuranceAttribute()
    {
        return $this->getLoanableAgeForInsurance(
            $this->loanable,
            $this->departure_at
        );
    }

    public function getCalendarDaysAttribute()
    {
        // Calendar days is meaningful for insurance mostly, which is computed in default_user_timezone
        return CalendarHelper::getCalendarDays(
            $this->departure_at,
            $this->actual_return_at,
            config("app.default_user_timezone")
        );
    }

    // This attribute is pretty expensive to calculate since it takes into account borrower roles,
    // pricings and subscriptions.
    // It should not be computed when fetching lists of loans.
    public function getIsFreeAttribute(): bool
    {
        $borrowerInvoice = $this->getBorrowerInvoice();

        if (!$borrowerInvoice || !$borrowerInvoice->hasNonZeroItems()) {
            return true;
        }

        // A loan is considered free if it has no non-zero mandatory pricing item.
        foreach ($borrowerInvoice->billItems as $invoiceItem) {
            if (
                abs($invoiceItem->total) >= 0.005 &&
                $this->applicable_amount_types->isBillItemTypeRequired(
                    $invoiceItem->item_type
                )
            ) {
                return false;
            }
        }

        return true;
    }

    private function getContributionRequirement(): Requirement
    {
        if (
            $this->borrowerUser->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::forLoanable($this->loanable),
                $this->community_id,
                new Carbon($this->departure_at, $this->loanable->timezone)
            ) ||
            $this->community->exempt_from_contributions
        ) {
            return Requirement::notApplicable;
        }

        if ($this->getPricings()->isContributionRequired()) {
            return Requirement::required;
        }

        return Requirement::optional;
    }

    public function getApplicableAmountTypesAttribute(): BorrowerApplicableAmountTypes
    {
        if (
            $this->cachedApplicableAmountTypes &&
            !$this->isDirty() &&
            !$this->loanable->isDirty()
        ) {
            return $this->cachedApplicableAmountTypes;
        }

        $borrowerUser = $this->borrowerUser;
        $contributionRequirement = $this->getContributionRequirement();

        if (
            $this->loanable->userHasRole(
                $borrowerUser,
                LoanableUserRoles::Owner
            )
        ) {
            // None of loan.price, loan.expenses, loan.insurance applies to loanable owner.
            $this->cachedApplicableAmountTypes = new BorrowerApplicableAmountTypes(
                price: Requirement::notApplicable,
                insurance: Requirement::notApplicable,
                expenses: Requirement::notApplicable,
                contributions: $contributionRequirement
            );
        } elseif (
            $this->loanable->userHasSomeRole($borrowerUser, [
                LoanableUserRoles::Manager,
                LoanableUserRoles::Coowner,
            ])
        ) {
            $roleParams = $this->loanable->userGetRoleParams($borrowerUser);
            // Amounts that apply to coowner are defined in coowner configuration.
            $this->cachedApplicableAmountTypes = new BorrowerApplicableAmountTypes(
                price: $roleParams["pays_loan_price"]
                    ? Requirement::required
                    : Requirement::notApplicable,
                insurance: $roleParams["pays_loan_insurance"]
                    ? Requirement::required
                    : Requirement::notApplicable,
                expenses: $roleParams["pays_loan_price"]
                    ? Requirement::required
                    : Requirement::notApplicable,
                contributions: $contributionRequirement
            );
        } else {
            $this->cachedApplicableAmountTypes = new BorrowerApplicableAmountTypes(
                contributions: $contributionRequirement
            );
        }

        return $this->cachedApplicableAmountTypes;
    }

    public function getIsBorrowerTrustedAttribute()
    {
        return $this->loanable->hasTrustedUser($this->borrowerUser);
    }

    public function getActualExpensesAttribute()
    {
        return $this->expenses_amount ?? 0;
    }

    public function getBorrowerTotalAttribute()
    {
        return $this->getBorrowerInvoice()?->getUserBalanceChange() ?? 0;
    }

    public function getBorrowerMandatoryTotalAttribute()
    {
        $borrowerInvoice = $this->getBorrowerInvoice();

        if (!$borrowerInvoice || !$borrowerInvoice->hasNonZeroItems()) {
            return 0;
        }

        $mandatoryTotal = 0;
        foreach ($borrowerInvoice->billItems as $invoiceItem) {
            if (
                $this->applicable_amount_types->isBillItemTypeRequired(
                    $invoiceItem->item_type
                )
            ) {
                $mandatoryTotal += $invoiceItem->getUserBalanceChange();
            }
        }

        return round($mandatoryTotal, 2);
    }

    public function getOwnerTotalAttribute()
    {
        return $this->getOwnerInvoice()?->getUserBalanceChange() ?? 0;
    }

    public function borrowedByOwner(): bool
    {
        return $this->loanable->mergedUserRoles
            ->where("role", LoanableUserRoles::Owner)
            ->where("user_id", $this->borrowerUser->id)
            ->isNotEmpty();
    }

    public function borrowedByCoownerOrOwner(): bool
    {
        return $this->borrowedByOwner() ||
            $this->loanable->hasOwnerOrCoowner($this->borrowerUser);
    }

    public function getPricings()
    {
        if (
            !$this->cachedLoanPricings ||
            $this->isDirty() ||
            $this->loanable->isDirty()
        ) {
            $this->cachedLoanPricings = new LoanPricingsHelper($this);
        }

        return $this->cachedLoanPricings;
    }
    // If payment is completed, a null invoice indicates that no borrower invoice
    // was generated during payment, meaning loan was free.
    public function getBorrowerInvoice(): ?Invoice
    {
        if ($this->status === LoanStatus::Completed) {
            return $this->borrowerInvoice;
        }

        if (
            $this->cachedBorrowerInvoice &&
            !$this->isDirty() &&
            !$this->loanable->isDirty()
        ) {
            return $this->cachedBorrowerInvoice;
        }

        $borrowerInvoice = LoanInvoiceHelper::getBorrowerInvoice($this);

        $this->cachedBorrowerInvoice = $borrowerInvoice;

        return $borrowerInvoice;
    }

    public function getDesiredContributionAttribute()
    {
        return $this->getPricings()->getDesiredContribution();
    }

    public function getSubscriptionAvailableAttribute()
    {
        return $this->getPricings()->isSubscriptionPossible();
    }

    public function getCanAddExpensesAttribute()
    {
        // Maybe add this as an option for the vehicle
        if ($this->loanable->type !== LoanableTypes::Car) {
            return false;
        }

        return $this->borrower_must_pay_compensation;
    }

    public function getRequiresDetailedMileageAttribute(): bool
    {
        // Maybe add this as an option for the vehicle
        if ($this->loanable->type !== LoanableTypes::Car) {
            return false;
        }

        return $this->requires_mileage;
    }

    public function getRequiresMileageAttribute(): bool
    {
        return $this->getPricings()->requiresMileage();
    }

    public function getBorrowerMayContributeAttribute()
    {
        return $this->applicable_amount_types->contributions !==
            Requirement::notApplicable;
    }

    public function getBorrowerMustPayCompensationAttribute()
    {
        return $this->applicable_amount_types->price ===
            Requirement::required &&
            $this->getPricings()
                ->getPricingPriceItems()
                ->count() > 0;
    }

    public function getBorrowerMustPayInsuranceAttribute()
    {
        return $this->applicable_amount_types->insurance ===
            Requirement::required &&
            $this->getPricings()
                ->getPricingInsuranceItems()
                ->count() > 0;
    }

    public function getHasNoApplicablePricingAttribute(): bool
    {
        return !$this->borrower_must_pay_compensation &&
            !$this->borrower_must_pay_insurance &&
            !$this->borrower_may_contribute;
    }

    public function getBorrowerActionRequiredAttribute(): bool
    {
        if ($this->status === LoanStatus::Accepted) {
            return true;
        }

        // Borrower must validate or add info
        if (
            $this->status === LoanStatus::Ended &&
            !$this->borrower_validated_at
        ) {
            return true;
        }

        // Borrower must pay or contribute
        if (
            $this->status === LoanStatus::Validated &&
            ($this->borrower_may_contribute ||
                $this->borrower_must_pay_insurance ||
                $this->borrower_must_pay_compensation)
        ) {
            return true;
        }

        return false;
    }

    public function getOwnerActionRequiredAttribute(): bool
    {
        if ($this->status === LoanStatus::Requested) {
            return true;
        }

        if ($this->status === LoanStatus::Ended && !$this->owner_validated_at) {
            return true;
        }

        return false;
    }

    public function userActionRequired(User $user): bool
    {
        return ($this->borrower_action_required &&
            $this->borrower_user_id === $user->id) ||
            ($this->loanable->hasOwnerOrCoowner($user) &&
                $this->owner_action_required);
    }

    // If payment is completed, a null invoice indicates that no owner invoice
    // was generated during payment.
    public function getOwnerInvoice(): ?Invoice
    {
        if ($this->status === LoanStatus::Completed) {
            return $this->ownerInvoice;
        }

        if (
            $this->cachedOwnerInvoice &&
            !$this->isDirty() &&
            !$this->loanable->isDirty()
        ) {
            return $this->cachedOwnerInvoice;
        }

        $ownerInvoice = LoanInvoiceHelper::getOwnerInvoice($this);

        $this->cachedOwnerInvoice = $ownerInvoice;

        return $ownerInvoice;
    }

    public function clearInvoices()
    {
        $this->cachedOwnerInvoice = null;
        $this->cachedBorrowerInvoice = null;
    }

    public function scopeIntersects(Builder $query, Interval $range)
    {
        return $query
            /*
            Intersection if: a1 > b0 and a0 < b1

                a0           a1
                [------------)
                      [------------)
                      b0           b1
        */
            ->where("actual_return_at", ">", $range->start->toISOString())
            ->where("departure_at", "<", $range->end->toISOString());
    }

    /**
     * Returns a query for loans which intersect with the given interval, comparing UTC values
     * directly in the DB. This differs from @see self::scopeIsLocalPeriodUnavailable which
     * interprets given datetimes in the related loanable's timezone.
     *
     * @param Builder<Loan> $query
     * @param Interval $range
     * @return Builder
     */
    public function scopeIsPeriodUnavailable(
        Builder $query,
        Interval $range
    ): Builder {
        return $query->accepted()->intersects($range);
    }

    /**
     * Returns a query for loans which intersect with the given interval, interpreting the datetimes
     * in the related loanable's timezone. This differs from @see self::scopeIsPeriodUnavailable
     * which compares times in UTC.
     *
     * @param Builder<Loan> $query
     * @param string $departureAt a YYYY-MM-DD HH:mm datetime string (or similar, but
     *      without timezone).
     * @param string $returnAt a YYYY-MM-DD HH:mm datetime string (or similar, but without timezone).
     * @return Builder
     */
    public function scopeIsLocalPeriodUnavailable(
        Builder $query,
        string $departureAt,
        string $returnAt
    ): Builder {
        // alias for loanables is required here, since if this is used from a relationship query
        // such as loanables::whereHas('loans', ...), there will be a condition set on
        // 'loanables.id'.
        $subQuery = $query
            ->join("loanables as l2", "l2.id", "loans.loanable_id")
            ->accepted()
            /*
                Intersection if: a1 > b0 and a0 < b1

                    a0           a1
                    [------------)
                          [------------)
                          b0           b1
            */
            ->where(
                "loans.actual_return_at",
                ">",
                \DB::raw("timestamp '$departureAt' at time zone l2.timezone")
            )
            ->where(
                "loans.departure_at",
                "<",
                \DB::raw("timestamp '$returnAt' at time zone l2.timezone")
            )
            ->select("loans.*");

        return $query->fromSub($subQuery, "loans");
    }

    public function scopeForUser(Builder $query, $userId): Builder
    {
        // Loans where user is either owner, coowner or borrower.
        // This is similar to Loan::scopeAccessibleBy, but doesn't take into account admin
        // privileges.
        return Loan::query()->fromSub(
            $query
                ->join("loanables", "loanables.id", "loanable_id")
                ->leftJoin(
                    "merged_loanable_user_roles",
                    "merged_loanable_user_roles.loanable_id",
                    "loanables.id"
                )
                ->where(
                    fn(Builder $q) => $q
                        ->where("borrower_user_id", $userId)
                        ->orWhere(
                            fn(Builder $roleQuery) => $roleQuery
                                ->where(
                                    "merged_loanable_user_roles.user_id",
                                    $userId
                                )
                                ->whereIn("merged_loanable_user_roles.role", [
                                    LoanableUserRoles::Owner,
                                    LoanableUserRoles::Manager,
                                    LoanableUserRoles::Coowner,
                                ])
                        )
                )
                ->select("loans.*")
                ->distinct(),
            "loans"
        );
    }

    public function scopeForLibrary(Builder $query, int $libraryId): Builder
    {
        return $query->whereHas(
            "loanable",
            fn(Builder $q) => $q->where("library_id", $libraryId)
        );
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        $subQuery = Loan::query();

        // We use a join here to optimize the SQL query, which otherwise would use
        // where exist clause for both owners and coowners which when combined with an "or" operator
        // results in a very slow scan (>400ms).
        $subQuery
            ->join("loanables", "loanables.id", "loanable_id")
            ->select("loans.*");

        $subQuery->orWhere("borrower_user_id", $user->id);

        $subQuery->orWhereExists(function ($q) use ($user) {
            $q->select(\DB::raw(1))
                ->from("merged_loanable_user_roles")
                ->where("user_id", $user->id)
                ->whereIn("role", [
                    LoanableUserRoles::Owner,
                    LoanableUserRoles::Manager,
                    LoanableUserRoles::Coowner,
                ])
                ->whereColumn(
                    "loanables.id",
                    "merged_loanable_user_roles.loanable_id"
                );
        });

        // Or belonging to its admin communities
        $subQuery->orWhereHas(
            "community.approvedUsers",
            fn($q) => $q
                ->where("community_user.user_id", $user->id)
                ->where("community_user.role", "admin")
        );

        return $query->fromSub($subQuery, "loans");
    }

    public function scopeHasOwnerAccess(Builder $query, int $userId)
    {
        $query->where(
            fn(Builder $q) => $q->whereHas(
                "loanable.mergedUserRoles",
                fn(Builder $q) => $q
                    ->where("user_id", $userId)
                    ->whereIn("role", [
                        LoanableUserRoles::Owner,
                        LoanableUserRoles::Manager,
                        LoanableUserRoles::Coowner,
                    ])
            )
        );
    }

    /**
     * @param Builder<Loan> $query
     * @param string|null $for
     * @param User $user
     * @return Builder<Loan>
     */
    public function scopeFor(Builder $query, ?string $for, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit", "admin" => $query->whereHas(
                "community.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            "profile" => $query->forUser($user->id),

            default => $query,
        };
    }

    /**
     * @param Builder<Loan> $query
     * @return Builder<Loan>
     */
    public function scopeFuture(Builder $query): Builder
    {
        return $query->inProcess()->where("departure_at", ">", Carbon::now());
    }

    /**
     * Returns loans which are in periods of availability or unavailability given the rules.
     *
     * @param Builder $query Loan query
     * @param array $availabilityParams Rules which can be interpreted by AvailabilityHelper
     * @param bool $isAvailable whether to return loans in which are in periods of availability of unavailability.
     * @return Collection the loans
     */
    public function scopeDuringAvailability(
        Builder $query,
        array $availabilityParams,
        bool $isAvailable = true
    ): Collection {
        return $query
            ->with("loanable")
            ->get()
            ->filter(
                fn(Loan $loan) => $isAvailable ==
                    AvailabilityHelper::isScheduleAvailable(
                        $availabilityParams,
                        new Interval(
                            $loan->departure_at,
                            $loan->actual_return_at
                        ),
                        $loan->loanable->timezone
                    )
            )
            ->values();
    }

    public function scopeExtensionRequested(
        Builder $query,
        $value = true
    ): Builder {
        $onlyExtensions = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        if ($onlyExtensions) {
            return $query->whereNotNull("extension_duration_in_minutes");
        }
        return $query->whereNull("extension_duration_in_minutes");
    }

    public function setLoanStatusCanceled($at = null): void
    {
        $this->status = LoanStatus::Canceled;
        $this->canceled_at = new CarbonImmutable($at);
    }

    public function isCanceled(): bool
    {
        return $this->status == LoanStatus::Canceled;
    }

    public function getIsInProcessAttribute(): bool
    {
        return $this->status->isInProcess();
    }

    public function getIsAcceptedAttribute(): bool
    {
        return in_array($this->status, LoanStatus::ACCEPTED_STATES);
    }

    public function scopeInProcess(Builder $query): Builder
    {
        return $query->whereIn("status", LoanStatus::IN_PROCESS_STATES);
    }

    public function scopeAccepted(Builder $query): Builder
    {
        return $query->where(
            fn(Builder $q) => $q->whereIn("status", LoanStatus::ACCEPTED_STATES)
        );
    }

    public function setLoanStatusCompleted(): void
    {
        $this->status = LoanStatus::Completed;
    }

    public function hasBorrowerValidated(): bool
    {
        return !!$this->borrower_validated_at;
    }

    public function hasOwnerValidated(): bool
    {
        return !!$this->owner_validated_at;
    }

    public function isFullyValidated(): bool
    {
        return $this->hasBorrowerValidated() && $this->hasOwnerValidated();
    }

    public function borrowerCanPay(): bool
    {
        // borrower_total is negative when loan cost is positive, hence the sign inversion.
        return floatval($this->borrowerUser->balance) >= -$this->borrower_total;
    }

    public function borrowerCanPayMandatoryTotal()
    {
        // borrower_total is negative when loan cost is positive, hence the sign inversion.
        return floatval($this->borrowerUser->balance) >=
            -$this->borrower_mandatory_total;
    }

    /**
     * Whether either participants of the loan would need to validate mileage/expenses.
     *
     * @return bool
     */
    public function getNeedsValidationAttribute(): bool
    {
        return ($this->requires_mileage ||
            ($this->can_add_expenses && $this->expenses_amount > 0)) &&
            $this->validationLimit()->isAfter(CarbonImmutable::now()) &&
            !$this->borrowedByCoownerOrOwner();
    }

    /**
     * Whether all necessay information has been filled prior to validation and completion of the
     * loan. This currently includes the mileage (if required) and auto-validation (if required).
     *
     * If this is false, loan should not proceed from the ended state, and participants should be
     * notified that information is missing.
     *
     * @return bool
     */
    public function getHasRequiredInfoToValidateAttribute()
    {
        if ($this->requires_detailed_mileage) {
            if (is_null($this->mileage_start) || is_null($this->mileage_end)) {
                return false;
            }
        }

        if ($this->loanable->library?->loan_auto_validation_question) {
            if (is_null($this->auto_validated_at)) {
                return false;
            }
        }

        return true;
    }

    public function validationLimit(): CarbonImmutable
    {
        return $this->attempt_autocomplete_at ??
            $this->actual_return_at->addHours(48);
    }

    public function scopeBlockingLoanableDeletion(Builder $query)
    {
        // Loans which have been confirmed, and still in process
        return $query->whereIn("status", [
            LoanStatus::Confirmed,
            LoanStatus::Ongoing,
            LoanStatus::Ended,
            LoanStatus::Validated,
        ]);
    }

    // Loan is in grace period only if it is validated and would be paid if not for the fact that
    // it just recently ended.
    public function isInGracePeriod()
    {
        if ($this->status !== LoanStatus::Validated) {
            return false;
        }

        if (Carbon::now()->isBefore($this->actual_return_at)) {
            return false;
        }

        if (
            Carbon::now()->gte(
                $this->actual_return_at->addMinutes(self::$GRACE_PERIOD_MINUTES)
            )
        ) {
            return false;
        }

        if ($this->has_no_applicable_pricing) {
            return true;
        }

        return $this->borrowerCanPay() &&
            (($this->platform_tip && $this->platform_tip > 0) ||
                $this->getContributionRequirement() ===
                    Requirement::notApplicable);
    }

    /**
     * Checks if loan can be moved forward from its current status.
     */
    public function refreshStatus(): void
    {
        match ($this->status) {
            LoanStatus::Requested => $this->setLoanStatusRequested(),
            LoanStatus::Accepted => $this->setLoanStatusAccepted(),
            LoanStatus::Confirmed => $this->setLoanStatusConfirmed(),
            LoanStatus::Ongoing => $this->setLoanStatusOngoing(),
            LoanStatus::Ended => $this->setLoanStatusEnded(),
            LoanStatus::Validated => $this->setLoanStatusValidated(),
            default => null,
        };
    }

    public function setLoanStatusRequested(): void
    {
        if ($this->is_self_service || $this->accepted_at) {
            $this->setLoanStatusAccepted();
            return;
        }

        $this->rejected_at = null;
        $this->accepted_at = null;
        $this->status = LoanStatus::Requested;
    }

    public function setLoanStatusAccepted(): void
    {
        $this->accepted_at = CarbonImmutable::now();
        if ($this->borrowerCanPayMandatoryTotal()) {
            $this->setLoanStatusConfirmed();
            return;
        }
        $this->prepaid_at = null;
        $this->status = LoanStatus::Accepted;
    }

    public function prepay(): void
    {
        $this->prepaid_at = CarbonImmutable::now();
        $this->setLoanStatusConfirmed();
    }

    public function setLoanStatusConfirmed(): void
    {
        if (CarbonImmutable::now()->isAfter($this->departure_at)) {
            $this->setLoanStatusOngoing();
            return;
        }

        $this->status = LoanStatus::Confirmed;
    }

    public function setLoanStatusOngoing(): void
    {
        if (CarbonImmutable::now()->isAfter($this->actual_return_at)) {
            $this->setLoanStatusEnded();
            return;
        }
        $this->status = LoanStatus::Ongoing;
    }

    public function setLoanStatusRejected(): void
    {
        $this->rejected_at = CarbonImmutable::now();
        $this->status = LoanStatus::Rejected;
    }

    public function setLoanStatusEnded(): void
    {
        // Loan is validated when it has required info and that info is validated (if need be)
        if (
            $this->has_required_info_to_validate &&
            (!$this->needs_validation || $this->isFullyValidated())
        ) {
            $this->setLoanStatusValidated();
            return;
        }

        $this->status = LoanStatus::Ended;
    }

    public function setLoanStatusValidated(): void
    {
        // Give one hour grace period: never complete right after the loan has ended
        // to give time to ask for an extension if necessary.
        if (
            $this->actual_return_at
                ->addMinutes(self::$GRACE_PERIOD_MINUTES)
                ->isAfter(CarbonImmutable::now())
        ) {
            $this->attempt_autocomplete_at = null;
            $this->status = LoanStatus::Validated;
            return;
        }

        if ($this->has_no_applicable_pricing) {
            $this->setLoanStatusCompleted();
            return;
        }

        // If borrower has already added enough to their account and is either exempt from
        // contributions or has set a tip, we can pay straight away.
        if (
            $this->borrowerCanPay() &&
            (($this->platform_tip && $this->platform_tip > 0) ||
                $this->getContributionRequirement() ===
                    Requirement::notApplicable)
        ) {
            $this->pay();
            return;
        }

        $this->status = LoanStatus::Validated;
    }

    public function setLoanStatusPaid(): void
    {
        $this->paid_at = CarbonImmutable::now();
        if ($this->paid_at->isBefore($this->actual_return_at)) {
            $this->actual_return_at = $this->paid_at;
        }
        $this->setLoanStatusCompleted();
    }

    public function pay(): void
    {
        $ownerUser = $this->loanable->owner_user;

        // Invoices are in memory, but not saved yet.
        $borrowerInvoice = $this->getBorrowerInvoice();
        $ownerInvoice = $this->getOwnerInvoice();

        if ($borrowerInvoice->hasNonZeroItems()) {
            // At the moment, Invoices must be "paid" so they are not considered as manual invoices.
            $borrowerInvoice->paid_at = CarbonImmutable::now();
            $this->borrowerUser->addToBalance(
                $borrowerInvoice->getUserBalanceChange()
            );

            $borrowerInvoice->save();

            $borrowerInvoice
                ->billItems()
                ->saveMany($borrowerInvoice->billItems);
            $this->borrower_invoice_id = $borrowerInvoice->id;
        }

        if ($this->borrowedByOwner()) {
            $this->owner_invoice_id = $borrowerInvoice->id;
        } else {
            if ($ownerInvoice->hasNonZeroItems()) {
                // At the moment, Invoices must be "paid" so they are not considered as manual invoices.
                $ownerInvoice->paid_at = CarbonImmutable::now();
                $ownerUser->addToBalance($ownerInvoice->getUserBalanceChange());

                $ownerInvoice->save();

                $ownerInvoice->billItems()->saveMany($ownerInvoice->billItems);
                $this->owner_invoice_id = $ownerInvoice->id;
            }
        }

        $this->setLoanStatusPaid();
    }

    public function acceptExtension(): void
    {
        if ($this->extension_duration_in_minutes) {
            $this->duration_in_minutes = $this->extension_duration_in_minutes;
            $this->extension_duration_in_minutes = null;
        }
    }
}
