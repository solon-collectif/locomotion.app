<?php

namespace App\Models\Pivots;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Events\CommunityUserApprovedEvent;
use App\Events\CommunityUserUnapprovedEvent;
use App\Models\Community;
use App\Models\Subscription;
use App\Models\File;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property string $proof_evaluation
 * @property array $meta
 */
class CommunityUser extends BasePivot
{
    use HasFactory;

    public $incrementing = true;

    public $items = [
        "user",
        "community",
        "approver",
        "current_granted_subscription",
    ];

    public $collections = ["subscriptions"];

    public static function boot()
    {
        parent::boot();

        // Do not fetch community_users for archived users
        static::addGlobalScope(
            "active-user",
            fn(Builder $builder) => $builder->whereHas("user")
        );

        // Do not fetch community_users for archived communities
        static::addGlobalScope(
            "active-community",
            fn(Builder $builder) => $builder->whereHas(
                "community",
                fn(Builder $q) => $q->withoutTrashed()
            )
        );

        self::saving(function (CommunityUser $communityUser) {
            if ($communityUser->hasBecomeApproved()) {
                $communityUser->proof_evaluation = "valid";
            }
        });

        self::saved(function (CommunityUser $model) {
            if ($model->isDirty()) {
                LoanablesByTypeAndCommunity::forgetForCommunityUser($model);
                if (!$model->wasApproved() && $model->isApproved()) {
                    event(new CommunityUserApprovedEvent($model));
                } elseif ($model->wasApproved() && !$model->isApproved()) {
                    event(new CommunityUserUnapprovedEvent($model));
                }
            }
        });

        self::deleting(function (CommunityUser $model) {
            LoanablesByTypeAndCommunity::forgetForCommunityUser($model);

            // Sometimes when detaching from users, the CommunityUser model will only have the
            // relation fields set. We need the whole model here.
            $model->refresh();
            if ($model->isApproved()) {
                event(new CommunityUserUnapprovedEvent($model));
            }
        });
    }

    public static $filterTypes = [
        "id" => "number",
        "user_id" => "number",
        "community_id" => "number",
        "updated_at" => "date",
        "approved_at" => "date",
        "suspended_at" => "date",
        "role" => "enum",
        "proof_evaluation" => "enum",
        "approver_id" => "number",
        "join_method" => "enum",
        "approval_note" => "text",
    ];

    protected $casts = [
        "approved_at" => "datetime",
        "suspended_at" => "datetime",
        "meta" => "array",
    ];

    protected $fillable = [
        "role",
        "proof_evaluation",
        "approver_id",
        "approval_note",
        "join_method",
        // TODO: remove these from fillables to ensure they are set by the appropriate methods
        "approved_at",
        "suspended_at",
    ];

    public static $rules = [
        "approved_at" => ["nullable", "date"],
        "suspended_at" => ["nullable", "date"],
        "role" => ["nullable", "in:admin"],
        "proof_evaluation" => ["in:valid,invalid,unevaluated"],
        "join_method" => ["in:geo,manual,invitation"],
        "approval_note" => ["nullable"],
    ];

    public $morphManys = [
        "custom_proof" => "fileable",
    ];

    public function customProof(): MorphMany
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "custom_proof"
        );
    }

    public static function getRules($action = "", $auth = null)
    {
        if ($action === "update" && !$auth->isAdmin()) {
            return [...self::$rules, "role" => ["missing"]];
        }

        if ($action === "create") {
            $rules = self::$rules;
            $rules["join_method"][] = "required";
            return $rules;
        }

        return parent::getRules($action, $auth);
    }

    public $computed = ["status", "proof_state"];

    private function hasBecomeApproved(): bool
    {
        return !$this->originalIsEquivalent("approved_at") &&
            $this->isApproved();
    }

    public function isApproved(): bool
    {
        return !!$this->approved_at && !$this->suspended_at;
    }

    private function wasApproved(): bool
    {
        return !!$this->getOriginal("approved_at") &&
            !$this->getOriginal("suspended_at");
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo<Community, $this>
     */
    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function approver(): BelongsTo
    {
        return $this->belongsTo(User::class, "approver_id");
    }

    /**
     * @return HasMany<Subscription, $this>
     */
    public function subscriptions(): HasMany
    {
        return $this->hasMany(Subscription::class, "community_user_id");
    }

    public function currentGrantedSubscription(): HasOne
    {
        return $this->hasOne(Subscription::class, "community_user_id")
            ->active()
            ->where("type", "granted");
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->whereHas("user", function ($q) use ($user) {
            return $q->accessibleBy($user);
        });
    }

    public function scopeNeedApproval(Builder $query)
    {
        return $query->whereNull([
            "community_user.approved_at",
            "community_user.suspended_at",
        ]);
    }

    public function getStatusAttribute()
    {
        if ($this->suspended_at) {
            return "suspended";
        }

        if ($this->approved_at) {
            return "approved";
        }

        return "new";
    }

    public function scopeStatus(Builder $query, string $value, $negative)
    {
        $value = strtolower($value);
        return match ($value) {
            "suspended" => $negative
                ? $query->whereNull("suspended_at")
                : $query->whereNotNull("suspended_at"),
            "approved" => $negative
                ? $query
                    ->whereNull("approved_at")
                    ->orWhereNotNull("suspended_at")
                : $query
                    ->whereNotNull("approved_at")
                    ->whereNull("suspended_at"),
            "new" => $negative
                ? $query
                    ->whereNotNull("approved_at")
                    ->orWhereNotNull("suspended_at")
                : $query->whereNull("suspended_at")->whereNull("approved_at"),
            default => $query,
        };
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit", "admin" => $query->whereHas(
                "community.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            default => $query,
        };
    }

    public function scopeInCommunityArea(Builder $query, $value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === false) {
            $subQuery = CommunityUser::query()
                ->join("users", "users.id", "community_user.user_id")
                ->join(
                    "communities",
                    "communities.id",
                    "community_user.community_id"
                )
                ->select("community_user.*")
                ->whereNull("users.address_position")
                ->orWhereNull("communities.area")
                ->orWhereRaw(
                    "not public.ST_Contains(communities.area::geometry, users.address_position::geometry)"
                );
        } else {
            $subQuery = CommunityUser::query()
                ->join("users", "users.id", "community_user.user_id")
                ->join(
                    "communities",
                    "communities.id",
                    "community_user.community_id"
                )
                ->select("community_user.*")
                ->whereNotNull("users.address_position")
                ->whereNotNull("communities.area")
                ->whereRaw(
                    "public.ST_Contains(communities.area::geometry, users.address_position::geometry)"
                );
        }

        return $query->fromSub($subQuery, "community_user");
    }

    public function resetProofEvaluation(): void
    {
        $this->proof_evaluation = "unevaluated";
    }

    /**
     * @return Attribute<boolean, never>
     */
    protected function hasProof(): Attribute
    {
        return Attribute::get(
            fn() => $this->customProof->isNotEmpty() ||
                $this->user->residencyProof->isNotEmpty() ||
                $this->user->identityProof->isNotEmpty()
        );
    }

    public function scopeHasProof(
        Builder $query,
        $value = true,
        $negative = false
    ): void {
        $value = filter_var(
            $value,
            FILTER_VALIDATE_BOOLEAN,
            FILTER_NULL_ON_FAILURE
        );
        if (is_null($value)) {
            return;
        }

        if ($value === $negative) {
            $query
                ->whereDoesntHave("customProof")
                ->whereDoesntHave("user.identityProof")
                ->whereDoesntHave("user.residencyProof");
            return;
        }

        $query->where(
            fn(Builder $q) => $q
                ->whereHas("customProof")
                ->orWhereHas("user.residencyProof")
                ->orWhereHas("user.identityProof")
        );
    }

    /**
     * @return Attribute<string, never>
     */
    protected function proofState(): Attribute
    {
        return Attribute::get(
            fn() => $this->has_proof ? $this->proof_evaluation : "absent"
        );
    }

    public function scopeProofState(Builder $query, string $value): void
    {
        match ($value) {
            "present" => $query->hasProof(),
            "absent" => $query->hasProof(false),
            "unevaluated" => $query
                ->hasProof()
                ->where("proof_evaluation", "unevaluated"),
            "valid" => $query->hasProof()->where("proof_evaluation", "valid"),
            "invalid" => $query
                ->hasProof()
                ->where("proof_evaluation", "invalid"),
            default => $query,
        };
    }

    public function approve(int $approverId, ?string $approvalNote): void
    {
        $this->approver_id = $approverId;
        $this->approval_note = $approvalNote;
        $this->approved_at = Carbon::now();

        $this->save();
    }
}
