<?php

namespace App\Models;

use App\Enums\PricingLoanableTypeValues;

class SubscriptionLoanableTypes extends BaseModel
{
    public $timestamps = false;

    protected $casts = [
        "pricing_loanable_type" => PricingLoanableTypeValues::class,
    ];

    protected $fillable = ["subscription_id", "pricing_loanable_type"];
}
