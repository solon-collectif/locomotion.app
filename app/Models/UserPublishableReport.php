<?php

namespace App\Models;

use App\Enums\ReportStatus;
use App\Models\Pivots\BasePivot;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserPublishableReport extends BasePivot
{
    use HasFactory;

    public $timestamps = false;
    public $incrementing = true;
    protected $table = "user_publishable_reports";
    protected $fillable = ["user_id", "publishable_report_id", "path"];

    public $items = ["user", "report"];

    public static $filterTypes = [
        "id" => "number",
        "user_id" => "number",
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return BelongsTo<PublishableReport, $this>
     */
    public function report(): BelongsTo
    {
        return $this->belongsTo(
            PublishableReport::class,
            "publishable_report_id"
        );
    }

    public function scopeAccessibleBy(Builder $builder, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $builder;
        }
        return $builder
            ->where("user_id", $user->id)
            ->whereHas(
                "report",
                fn(Builder $query) => $query->where(
                    "status",
                    ReportStatus::Published
                )
            );
    }
}
