<?php

namespace App\Models;

use App\Casts\GeoJsonCast;
use App\Casts\Point;
use App\Casts\PointCast;
use App\Models\Pivots\CommunityUser;
use App\Rules\IsGeoJsonMultiPolygon;
use App\Transformers\CommunityTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property-read Point $center
 */
class Community extends BaseModel
{
    use SoftDeletes;
    use HasFactory;
    use HasSqlCasts;

    public static $rules = [
        "name" => "required",
        "chat_group_url" => "url|nullable",
        "description" => "nullable",
        "long_description" => "nullable",
        "starting_guide_url" => "url|nullable",
        "area" => "nullable",
        "type" => ["nullable", "in:private,borough"],
        "requires_residency_proof" => "boolean",
        "requires_identity_proof" => "boolean",
        "requires_custom_proof" => "boolean",
        "custom_proof_name" => "nullable",
        "custom_proof_desc" => ["string", "nullable", "max:10000"],
        "contact_email" => ["nullable", "email"],
        "exempt_from_contributions" => "boolean",
        "custom_registration_approved_email" => [
            "string",
            "nullable",
            "max:10000",
        ],
    ];

    public static $filterTypes = [
        "id" => "number",
        "name" => "text",
        "type" => ["borough", "private"],
        "deleted_at" => "date",
    ];

    protected static $transformer = CommunityTransformer::class;

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            default => array_merge(static::$rules, [
                "area" => ["nullable", new IsGeoJsonMultiPolygon()],
            ]),
        };
    }
    protected static array $customColumns = [
        "center" => "st_asGeoJSON(ST_Centroid(%table%.area::geometry))",
        "approved_users_count" => <<<SQL
  (
    SELECT count(id)
    FROM community_user
    WHERE community_user.community_id = %table%.id
    AND community_user.approved_at IS NOT NULL
    AND community_user.suspended_at IS NULL
  )
SQL
    ,
    ];

    protected $fillable = [
        "area",
        "chat_group_url",
        "description",
        "long_description",
        "starting_guide_url",
        "name",
        "type",
        "requires_residency_proof",
        "requires_identity_proof",
        "requires_custom_proof",
        "custom_proof_name",
        "custom_proof_desc",
        "contact_email",
        "exempt_from_contributions",
        "custom_registration_approved_email",
    ];

    protected $casts = [
        // Center is a custom column, so we do not need to use a sql cast
        "center" => PointCast::class,
    ];

    protected static array $sqlCasts = [
        "area" => GeoJsonCast::class,
    ];

    public $collections = [
        "users",
        "pricings",
        "allowed_loanable_types",
        "libraries",
    ];

    public $items = ["community_mailing_list"];

    public $computed = [
        "center_google",
        "approved_users_count",
        "loanables_count",
        "loanables_count_by_type",
        "shared_loanable_types",
        "users_count",
        "loans_count",
        "invitations_count",
    ];

    public function admins()
    {
        $globalAdmins = User::query()
            ->globalAdmins()
            ->get();
        $localAdmins = $this->communityAdmins()->get();
        return $globalAdmins->merge($localAdmins);
    }

    /**
     * @return BelongsToMany<LoanableTypeDetails, $this>
     */
    public function allowedLoanableTypes(): BelongsToMany
    {
        return $this->belongsToMany(
            LoanableTypeDetails::class,
            "community_loanable_types",
            "community_id", // community_loanable_types.community_id
            "loanable_type_id" // community_loanable_types.loanable_type_id
        );
    }

    public function getSharedLoanableTypesAttribute()
    {
        return $this->allowedLoanableTypes
            ->map(fn(LoanableTypeDetails $type) => $type->name)
            ->values()
            ->toArray();
    }

    public function getLoanablesCountByTypeAttribute()
    {
        $data = Loanable::sharedInCommunity($this->id)
            ->groupBy("loanables.type")
            ->whereNull("deleted_at")
            ->select(["loanables.type", \DB::raw("count(*)")])
            ->getQuery()
            ->get();

        $countByType = [];
        foreach ($data as $item) {
            $countByType[$item->type] = $item->count;
        }

        return $countByType;
    }

    /**
     * @return HasMany<Loan, $this>
     */
    public function loans(): HasMany
    {
        return $this->hasMany(Loan::class);
    }

    /**
     * @return HasMany<Pricing, $this>
     */
    public function pricings(): HasMany
    {
        return $this->hasMany(Pricing::class);
    }

    public function communityMailingList(): HasOne
    {
        return $this->hasOne(MailingListIntegration::class)->where(
            "community_editable",
            true
        );
    }

    public function mailingLists(): HasMany
    {
        return $this->HasMany(MailingListIntegration::class);
    }

    public function libraries(): BelongsToMany
    {
        return $this->belongsToMany(Library::class);
    }

    public function gbfsDatasets(): BelongsToMany
    {
        return $this->belongsToMany(
            GbfsDataset::class,
            "gbfs_dataset_communities"
        )->using(GbfsDatasetCommunity::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)
            ->using(CommunityUser::class)
            ->withTimestamps()
            ->withPivot([
                "id",
                "approved_at",
                "created_at",
                "role",
                "suspended_at",
                "updated_at",
            ]);
    }

    /**
     * @return BelongsToMany<User, $this>
     */
    public function approvedUsers(): BelongsToMany
    {
        return $this->users()
            ->whereNotNull("community_user.approved_at")
            ->whereNull("community_user.suspended_at");
    }

    /**
     * @return BelongsToMany<User, $this>
     */
    public function communityAdmins(): BelongsToMany
    {
        return $this->approvedUsers()->where("community_user.role", "admin");
    }

    /**
     * @return HasMany<Invitation, $this>
     */
    public function invitations(): HasMany
    {
        return $this->hasMany(Invitation::class);
    }

    public function getCenterGoogleAttribute()
    {
        if (!$this->center) {
            return null;
        }

        return [
            "lat" => $this->center->lat,
            "lng" => $this->center->long,
        ];
    }

    public function getApprovedUsersCountAttribute()
    {
        if (isset($this->attributes["approved_users_count"])) {
            return $this->attributes["approved_users_count"];
        }

        return $this->approvedUsers()->count();
    }

    public function getLoanablesCountAttribute()
    {
        return Loanable::sharedInCommunity($this->id)->count();
    }

    public function getUsersCountAttribute()
    {
        return $this->users()->count();
    }

    public function getLoansCountAttribute()
    {
        return $this->loans()->count();
    }

    public function getInvitationsCountAttribute()
    {
        return $this->invitations()
            ->status("active")
            ->count();
    }

    public function scopeAccessibleBy(Builder $query, ?User $user): Builder
    {
        if ($user && $user->isAdmin()) {
            return $query;
        }

        return $query->where(function ($q) use ($user) {
            if ($user) {
                $q->whereHas(
                    "users",
                    fn($q2) => $q2->where("users.id", $user->id)
                );
            }
            return $q->orWhere("communities.type", "!=", "private");
        });
    }

    public function scopeWithApprovedUser(Builder $query, int $userId)
    {
        return $query->whereHas(
            "users",
            fn($q) => $q
                ->where("community_user.user_id", $userId)
                ->whereNotNull("community_user.approved_at")
                ->whereNull("community_user.suspended_at")
        );
    }

    public function scopeWithCommunityAdmin(Builder $query, int $userId)
    {
        return $query->whereHas(
            "users",
            fn($q) => $q
                ->where("community_user.user_id", $userId)
                ->whereNotNull("community_user.approved_at")
                ->whereNull("community_user.suspended_at")
                ->where("community_user.role", "admin")
        );
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getTable() . ".name";

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    public function scopeFor(Builder $query, $for, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return match ($for) {
            "loan", "profile" => $query->withApprovedUser($user->id),
            "edit", "admin" => $query
                ->whereHas("users", function ($q) use ($user) {
                    return $q
                        ->where("community_user.user_id", $user->id)
                        ->where("community_user.role", "admin");
                })
                ->orWhereHas("users", function ($q) use ($user) {
                    return $q
                        ->where("users.id", $user->id)
                        ->where("users.role", "admin");
                }),
            default => $query,
        };
    }

    public function scopeApprovedInAny(Builder $query, $communityIds): Builder
    {
        return $query
            ->whereIn("community_user.community_id", $communityIds)
            ->whereNotNull("community_user.approved_at")
            ->whereNull("community_user.suspended_at");
    }
}
