<?php

namespace App\Models\Policies;

use App\Enums\LoanableUserRoles;
use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanableUserRolePolicy
{
    use HandlesAuthorization;

    public function delete(User $user, LoanableUserRole $loanableUserRole): bool
    {
        // No one can delete owner roles.
        if ($loanableUserRole->role === LoanableUserRoles::Owner) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        // Removing your own privileges is acceptable
        if ($loanableUserRole->user_id === $user->id) {
            return true;
        }

        // Library or loanable, the logic is the same.
        $ressource = $loanableUserRole->ressource;

        // Owners can remove all non-owner
        if ($ressource->userHasRole($user, LoanableUserRoles::Owner)) {
            return true;
        }

        // Managers can remove non-manager, non-owner roles
        if ($loanableUserRole->role === LoanableUserRoles::Manager) {
            return false;
        }
        if ($ressource->userHasRole($user, LoanableUserRoles::Manager)) {
            return true;
        }

        // Coowners can remove non-manager, non-owner, non-coowner roles
        if ($loanableUserRole->role === LoanableUserRoles::Coowner) {
            return false;
        }
        if ($ressource->userHasRole($user, LoanableUserRoles::Coowner)) {
            return true;
        }

        return false;
    }

    public function update(User $user, LoanableUserRole $loanableUserRole): bool
    {
        // Admins can update any role, users can update their roles.
        if ($user->isAdmin() || $user->is($loanableUserRole->user)) {
            return true;
        }

        // Library or loanable, the logic is the same.
        $ressource = $loanableUserRole->ressource;

        // Owners can manage all roles
        if ($ressource->userHasRole($user, LoanableUserRoles::Owner)) {
            return true;
        }

        // Managers can manage non-manager, non-owner roles
        if ($loanableUserRole->role === LoanableUserRoles::Owner) {
            return false;
        }
        if ($loanableUserRole->role === LoanableUserRoles::Manager) {
            return false;
        }
        if ($ressource->userHasRole($user, LoanableUserRoles::Manager)) {
            return true;
        }

        // Coowners can manage non-manager, non-owner, non-coowner roles
        if ($loanableUserRole->role === LoanableUserRoles::Coowner) {
            return false;
        }
        if ($ressource->userHasRole($user, LoanableUserRoles::Coowner)) {
            return true;
        }

        return false;
    }

    public function changeOwner(User $user, LoanableUserRole $role, $data): bool
    {
        // Only admins can update role, to set new owners.
        return $user->isAdmin() &&
            $data["role"] === LoanableUserRoles::Owner->value;
    }

    public function updatePaidAmountTypes(
        User $user,
        LoanableUserRole $userRole
    ): bool {
        // Only owners or admins can update the amounts another role should pay.
        if ($user->isAdmin()) {
            return true;
        }
        // Logic is same for library and loanables
        if (
            $userRole->ressource->userHasRole($user, LoanableUserRoles::Owner)
        ) {
            return true;
        }
        return false;
    }
}
