<?php

namespace App\Models\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $accessingUser, User $accessedUser): bool
    {
        if ($accessingUser->id == $accessedUser->id) {
            return true;
        }

        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunityFor($accessedUser);
    }

    public function update(User $accessingUser, User $accessedUser): bool
    {
        if ($accessingUser->id == $accessedUser->id) {
            return true;
        }

        return $accessingUser->isAdmin();
    }
    public function deactivate(User $accessingUser, User $accessedUser): bool
    {
        return $accessingUser->isAdmin() || $accessedUser->is($accessingUser);
    }

    public function reactivate(User $accessingUser, User $accessedUser): bool
    {
        return !$accessedUser->data_deleted_at &&
            ($accessingUser->isAdmin() || $accessedUser->is($accessingUser));
    }

    public function listArchived(User $accessingUser)
    {
        return $accessingUser->isAdmin();
    }

    public function seeArchivedDetails(
        User $accessingUser,
        User $accessedUser
    ): bool {
        return $accessingUser->isAdmin();
    }

    public function exportMailboxes(User $accessingUser, $communityId): bool
    {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunity($communityId);
    }

    public function claimBalance(User $accessingUser, User $accessedUser): bool
    {
        return $accessedUser->is($accessingUser);
    }
    public function addToBalance(User $accessingUser, User $accessedUser): bool
    {
        return $accessedUser->is($accessingUser);
    }
}
