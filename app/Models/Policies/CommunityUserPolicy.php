<?php

namespace App\Models\Policies;

use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommunityUserPolicy
{
    use HandlesAuthorization;

    public function approve(
        User $accessingUser,
        CommunityUser $communityUser
    ): bool {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunity($communityUser->community_id);
    }
    public function suspend(
        User $accessingUser,
        CommunityUser $communityUser
    ): bool {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunity($communityUser->community_id);
    }
    public function unsuspend(
        User $accessingUser,
        CommunityUser $communityUser
    ): bool {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunity($communityUser->community_id);
    }

    public function update(
        User $accessingUser,
        CommunityUser $communityUser
    ): bool {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunity($communityUser->community_id);
    }

    public function updateProofs(
        User $accessingUser,
        CommunityUser $communityUser
    ): bool {
        return $accessingUser->isAdmin() ||
            $accessingUser->id === $communityUser->user->id;
    }
}
