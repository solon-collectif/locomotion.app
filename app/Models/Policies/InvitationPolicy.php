<?php

namespace App\Models\Policies;

use App\Models\Invitation;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvitationPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin() || $user->isCommunityAdmin();
    }

    public function create(User $user, int $communityId): bool
    {
        return $user->isAdmin() || $user->isAdminOfCommunity($communityId);
    }

    public function delete(User $user, Invitation $invitation): bool
    {
        return $user->isAdmin() ||
            $user->isAdminOfCommunity($invitation->community_id);
    }
}
