<?php

namespace App\Models\Policies;

use App\Models\Payout;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PayoutPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin();
    }

    public function update(User $user, Payout $payout): bool
    {
        return $user->isAdmin();
    }
}
