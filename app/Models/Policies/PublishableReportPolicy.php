<?php

namespace App\Models\Policies;

use App\Enums\ReportStatus;
use App\Models\PublishableReport;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PublishableReportPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin();
    }

    public function view(User $user): bool
    {
        return $user->isAdmin();
    }

    public function create(User $user): bool
    {
        return $user->isAdmin();
    }

    public function publish(User $user, PublishableReport $report): bool
    {
        return $user->isAdmin() && $report->status === ReportStatus::Generated;
    }
}
