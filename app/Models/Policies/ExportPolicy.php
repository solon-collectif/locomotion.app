<?php

namespace App\Models\Policies;

use App\Models\Export;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExportPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Export $export): bool
    {
        return $user->isAdmin() || $export->user_id === $user->id;
    }

    public function cancel(User $user, Export $export): bool
    {
        return $user->isAdmin() || $export->user_id === $user->id;
    }

    public function cancelBatch(User $user): bool
    {
        return $user->isAdmin();
    }
    public function destroy(User $user): bool
    {
        return $user->isAdmin();
    }
}
