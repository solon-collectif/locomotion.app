<?php

namespace App\Models\Policies;

use App\Models\Community;
use App\Models\Pricing;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PricingPolicy
{
    use HandlesAuthorization;

    private function canCommunityAdminEditPricing(
        string $pricingType,
        bool $isCommunityPrivate
    ): bool {
        return match ($pricingType) {
            "insurance" => false,
            "price" => $isCommunityPrivate,
            "contribution" => true,
            default => false,
        };
    }

    public function create(User $user, array $data): bool
    {
        return $user->isAdmin() ||
            ($user->isAdminOfCommunity($data["community_id"]) &&
                self::canCommunityAdminEditPricing(
                    $data["pricing_type"],
                    Community::findOrFail($data["community_id"])->type ===
                        "private"
                ));
    }

    public function update(User $user, Pricing $pricing, array $data): bool
    {
        return $user->isAdmin() ||
            ($user->isAdminOfCommunity($pricing->community_id) &&
                self::canCommunityAdminEditPricing(
                    $pricing->pricing_type,
                    $pricing->community->type === "private"
                ) &&
                (!isset($data["pricing_type"]) ||
                    self::canCommunityAdminEditPricing(
                        $data["pricing_type"],
                        $pricing->community->type === "private"
                    )));
    }

    public function delete(User $user, Pricing $pricing): bool
    {
        return $user->isAdmin() ||
            ($user->isAdminOfCommunity($pricing->community_id) &&
                self::canCommunityAdminEditPricing(
                    $pricing->pricing_type,
                    $pricing->community->type === "private"
                ));
    }
}
