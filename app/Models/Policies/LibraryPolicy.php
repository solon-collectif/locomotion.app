<?php

namespace App\Models\Policies;

use App\Enums\LoanableUserRoles;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LibraryPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }

    public function seeValidationAnswer(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }

    public function create(User $user): bool
    {
        return $user->isAdmin();
    }

    public function update(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }
    public function addLoanable(
        User $user,
        Library $library,
        Loanable $loanable
    ): bool {
        return $user->isAdmin() ||
            ($library->userHasRole($user, LoanableUserRoles::Owner) &&
                $loanable->userHasRole($user, LoanableUserRoles::Owner));
    }

    public function removeLoanable(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasRole($user, LoanableUserRoles::Owner);
    }

    public function viewLoanables(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }

    public function addToCommunity(
        User $user,
        Library $library,
        Community $community
    ): bool {
        return $user->isAdmin() ||
            ($library->userHasRole($user, LoanableUserRoles::Owner) &&
                $user->approvedIn($community));
    }

    public function removeFromCommunity(
        User $user,
        Library $library,
        Community $community
    ): bool {
        return $user->isAdmin() ||
            $library->userHasRole($user, LoanableUserRoles::Owner);
    }

    public function viewCommunities(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }

    public function delete(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasRole($user, LoanableUserRoles::Owner);
    }

    public function changeOwner(User $user, Library $library): bool
    {
        return $user->isAdmin();
    }

    public function createUserRole(User $user, Library $library, $data): bool
    {
        // No one can create owner roles.
        if ($data["role"] === LoanableUserRoles::Owner->value) {
            return false;
        }

        // Only admins owners can create library roles
        return $user->isAdmin() ||
            $library->userHasRole($user, LoanableUserRoles::Owner);
    }

    public function viewUserRoles(User $user, Library $library): bool
    {
        return $user->isAdmin() ||
            $library->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
    }
}
