<?php

namespace App\Models\Policies;

use App\Enums\LoanableUserRoles;
use App\Models\Image;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ImagePolicy
{
    use HandlesAuthorization;

    private string $tempImageUserIdRegex = "/\/?images\/tmp\/([0-9]+)\//";

    public function view(User $user, Image $image): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($image->isTemp()) {
            return $this->viewTempImage($user, $image);
        }

        $imageable = $image->imageable;

        return match (get_class($imageable)) {
            User::class => $this->viewUserImage($user, $imageable),
            Loanable::class => $this->viewLoanableImage($user, $imageable),
            Loan::class => $this->viewLoanImage($user, $imageable),
            Library::class => $this->viewLibraryImage($user, $imageable),
            default => false,
        };
    }

    private function viewTempImage(User $user, Image $image): bool
    {
        $matches = [];
        $validPath = preg_match(
            $this->tempImageUserIdRegex,
            $image->path,
            $matches
        );

        return $validPath && count($matches) > 1 && $matches[1] == $user->id;
    }

    private function viewUserImage(
        User $accessingUser,
        User $accessedUser
    ): bool {
        return $accessingUser->is($accessedUser) ||
            $accessingUser->approvedInCommunityWith($accessedUser->id) ||
            $accessingUser->isAdminOfCommunityFor($accessedUser);
    }

    private function viewLoanableImage(
        User $accessingUser,
        Loanable $loanable
    ): bool {
        return $accessingUser
            ->approvedCommunities()
            ->whereIn("communities.id", $loanable->community_ids)
            ->exists();
    }

    private function viewLoanImage(User $accessingUser, Loan $loan): bool
    {
        return $accessingUser->is($loan->borrowerUser) ||
            $loan->loanable->hasOwnerOrCoowner($accessingUser) ||
            $accessingUser->isAdminOfCommunity($loan->community_id);
    }

    private function viewLibraryImage(
        User $accessingUser,
        Library $library
    ): bool {
        return $library->owner_user?->approvedInCommunityWith(
            $accessingUser->id
        );
    }
}
