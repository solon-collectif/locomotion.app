<?php

namespace App\Models\Policies;

use App\Enums\LoanableUserRoles;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanablePolicy
{
    use HandlesAuthorization;

    public function view(User $user, Loanable $loanable): bool
    {
        return $user->isAdmin() ||
            $loanable->hasOwnerOrCoowner($user) ||
            $user->approvedInCommunityWith($loanable->owner_user->id);
    }

    public function viewInstructions(User $user, Loanable $loanable): bool
    {
        return $loanable->hasOwnerOrCoowner($user) ||
            $user->isAdmin() ||
            $user->isAdminOfCommunityFor($loanable->owner_user);
    }

    public function viewTrustedBorrowerInstructions(
        User $user,
        Loanable $loanable
    ): bool {
        return $user->isAdmin() ||
            $loanable->hasTrustedUser($user) ||
            $user->isAdminOfCommunityFor($loanable->owner_user);
    }

    public function delete(User $user, Loanable $loanable): bool
    {
        return $loanable->hasOwner($user) ||
            $user->isAdmin() ||
            $user->isAdminOfCommunityFor($loanable->owner_user);
    }

    public function restore(User $user, Loanable $loanable): bool
    {
        return $this->delete($user, $loanable);
    }

    public function update(User $user, Loanable $loanable): bool
    {
        return $loanable->hasOwnerOrCoowner($user) ||
            $user->isAdminOfCommunityFor($loanable->owner_user) ||
            $user->isAdmin();
    }

    public function updateOwner(User $user, Loanable $loanable)
    {
        if (
            !$user->isAdmin() &&
            !$user->isAdminOfCommunityFor($loanable->owner_user)
        ) {
            return $this->deny("Only admins can change owners.");
        }
        return true;
    }

    public function listArchived(User $user)
    {
        return $user->isAdmin() || $user->isCommunityAdmin();
    }

    public function publish(User $user, Loanable $loanable): bool
    {
        return $user->isAdmin() || $loanable->hasOwnerOrCoowner($user);
    }

    public function updatePublishedFields(User $user, Loanable $loanable): bool
    {
        return $user->isAdmin();
    }

    public function createUserRole(User $user, Loanable $loanable, $data): bool
    {
        // No one can create owner roles.
        if ($data["role"] === LoanableUserRoles::Owner->value) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        // Owners can create roles for coowners and trusted borrowers.
        if (
            $loanable->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ])
        ) {
            return true;
        }

        // Coowners can create roles for trusted borrowers.
        if ($loanable->userHasRole($user, LoanableUserRoles::Coowner)) {
            return $data["role"] === LoanableUserRoles::TrustedBorrower->value;
        }

        return false;
    }

    public function viewUserRoles(User $user, Loanable $loanable): bool
    {
        return $user->isAdmin() ||
            $loanable->userHasSomeRole($user, [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
                LoanableUserRoles::Coowner,
            ]) ||
            $user->isAdminOfCommunityFor($loanable->owner_user);
    }
}
