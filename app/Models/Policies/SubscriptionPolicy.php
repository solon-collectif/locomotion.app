<?php

namespace App\Models\Policies;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin();
    }

    public function grant(User $user, array $data): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        $communityId = (int) $data["community_id"];
        return $user->isAdminOfCommunity($communityId);
    }

    public function delete(User $user, Subscription $subscription): bool
    {
        return $user->isAdmin() ||
            ($subscription->type === "granted" &&
                $user->isAdminOfCommunity(
                    $subscription->communityUser->community_id
                ));
    }

    public function restore(User $user, Subscription $subscription): bool
    {
        return $user->isAdmin();
    }
}
