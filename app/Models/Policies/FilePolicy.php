<?php

namespace App\Models\Policies;

use App\Models\Borrower;
use App\Models\Car;
use App\Models\Pivots\CommunityUser;
use App\Models\Export;
use App\Models\File;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

class FilePolicy
{
    use HandlesAuthorization;

    private string $tempFileUserIdRegex = "/\/?files\/tmp\/([0-9]+)/";

    public function view(User $user, File $file): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($file->isTemp()) {
            return $this->viewTempFile($user, $file);
        }

        $fileable = $file->fileable;

        return match (get_class($fileable)) {
            // User residency and identity proofs
            User::class => $this->viewUserFile($user, $fileable),
            // User custom proofs
            CommunityUser::class => $this->viewCommunityUserFile(
                $user,
                $fileable
            ),
            // Car state report
            Car::class => $this->viewCarFile($user, $fileable),
            // Driver proofs (gaa, saaq, insurance): only global admins can see these
            Borrower::class => $user->is($fileable->user),
            // ExportPolicy::view
            Export::class => $user->can("view", $fileable),
            default => false,
        };
    }

    private function viewTempFile(User $user, File $file): bool
    {
        $matches = [];
        $validPath = preg_match(
            $this->tempFileUserIdRegex,
            $file->path,
            $matches
        );

        return $validPath && count($matches) > 1 && $matches[1] == $user->id;
    }

    // This refers only to user residency and user identity proofs
    private function viewUserFile(User $accessingUser, User $accessedUser): bool
    {
        if ($accessingUser->is($accessedUser)) {
            return true;
        }

        // A community admin can only access files of users not yet approved.
        return $accessingUser
            ->adminCommunities()
            ->whereHas(
                "users",
                fn(Builder $query) => $query
                    ->whereNull("community_user.approved_at")
                    ->where("users.id", $accessedUser->id)
            )
            ->exists();
    }

    // This refers to community user custom proofs
    private function viewCommunityUserFile(
        User $accessingUser,
        CommunityUser $fileable
    ): bool {
        $accessedUser = $fileable->user;

        if ($accessingUser->is($accessedUser)) {
            return true;
        }

        // A community admin can only access files of users not yet approved in this community.
        return $accessingUser
            ->adminCommunities()
            ->where("id", $fileable->community_id)
            ->whereHas(
                "users",
                fn(Builder $query) => $query
                    ->whereNull("community_user.approved_at")
                    ->where("users.id", $accessedUser->id)
            )
            ->exists();
    }

    private function viewCarFile(User $user, Car $fileable): bool
    {
        return $user
            ->approvedCommunities()
            ->whereIn("communities.id", $fileable->loanable->community_ids)
            ->exists();
    }
}
