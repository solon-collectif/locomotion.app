<?php

namespace App\Models\Policies;

use App\Models\IncidentNote;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IncidentNotePolicy
{
    use HandlesAuthorization;

    public function updateNote(User $user, IncidentNote $note): bool
    {
        return $user->isAdmin() || $user->id === $note->author_id;
    }

    public function deleteNote(User $user, IncidentNote $note): bool
    {
        return $user->isAdmin() || $user->id === $note->author_id;
    }
}
