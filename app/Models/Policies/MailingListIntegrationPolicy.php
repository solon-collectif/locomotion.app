<?php

namespace App\Models\Policies;

use App\Models\MailingListIntegration;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class MailingListIntegrationPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin();
    }

    public function create(User $user, Request $request): bool
    {
        return $user->isAdmin() ||
            ($request->community_editable &&
                $user->isAdminOfCommunity($request->community_id));
    }

    public function delete(
        User $user,
        MailingListIntegration $mailingListIntegration
    ): bool {
        return $user->isAdmin() ||
            $user->is($mailingListIntegration->createdByUser) ||
            ($mailingListIntegration->community_editable &&
                $user->isAdminOfCommunity(
                    $mailingListIntegration->community_id
                ));
    }
}
