<?php

namespace App\Models\Policies;

use App\Models\Community;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CommunityPolicy
{
    use HandlesAuthorization;

    private $communityAdminFieldWritePermissions = [
        "id" => true,
        "chat_group_url" => true,
        "requires_residency_proof" => true,
        "requires_identity_proof" => true,
        "requires_custom_proof" => true,
        "starting_guide_url" => true,
        "contact_email" => true,
        "description" => true,
        "pricings" => true,
    ];

    private $privateCommunityAdminFieldWritePermissions = [
        "id" => true,
        "chat_group_url" => true,
        "requires_identity_proof" => true,
        "custom_registration_approved_email" => true,
        "starting_guide_url" => true,
        "contact_email" => true,
        "description" => true,
        "pricings" => true,
    ];

    public function view(User $user, Community $community): bool
    {
        return $user->isAdmin() ||
            $user->approvedIn($community) ||
            $community->type === "borough";
    }

    public function update(
        User $user,
        Community $community,
        array $data
    ): Response|bool {
        if ($user->isAdmin()) {
            return true;
        }

        if (!$user->isAdminOfCommunity($community->id)) {
            return false;
        }

        $permissions =
            $community->type === "private"
                ? $this->privateCommunityAdminFieldWritePermissions
                : $this->communityAdminFieldWritePermissions;

        foreach ($data as $key => $value) {
            if (!isset($permissions[$key])) {
                return $this->deny(
                    __("validation.custom.permissions_insufficient_for_field", [
                        "attribute" => $key,
                    ])
                );
            }
        }

        return true;
    }

    public function destroy(User $user): bool
    {
        return $user->isAdmin();
    }

    public function listArchived(User $user): bool
    {
        return $user->isAdmin();
    }

    public function addMember(User $user): bool
    {
        return $user->isAdmin();
    }

    public function removeMember(User $user, Community $community): bool
    {
        return $user->isAdmin() || $user->isAdminOfCommunity($community->id);
    }

    public function sendSampleEmail(User $user, Community $community): bool
    {
        return $user->isAdmin() || $user->isAdminOfCommunity($community->id);
    }
}
