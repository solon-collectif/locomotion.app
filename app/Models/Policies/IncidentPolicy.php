<?php

namespace App\Models\Policies;

use App\Models\Incident;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IncidentPolicy
{
    use HandlesAuthorization;

    public function create(User $user, ?Loan $loan, Loanable $loanable): bool
    {
        return $user->isAdmin() ||
            $loanable->hasOwnerOrCoowner($user) ||
            ($loan &&
                ($user->id === $loan->borrower_user_id ||
                    $user->isAdminOfCommunity($loan->community_id)));
    }

    public function updateDescription(User $user, Incident $incident): bool
    {
        return $user->isAdmin() || $user->id === $incident->reported_by_user_id;
    }

    public function addNote(User $user, Incident $incident): bool
    {
        return $user->isAdmin() ||
            ($incident->loan &&
                $user->isAdminOfCommunity($incident->loan->community_id)) ||
            $user->id === $incident->assignee_id ||
            $user->id === $incident->reported_by_user_id ||
            $user->id === $incident->resolved_by_user_id ||
            $incident->loanable->hasOwnerOrCoowner($user);
    }

    public function resolve(User $user, Incident $incident): bool
    {
        return $user->isAdmin() ||
            ($incident->loan &&
                $user->isAdminOfCommunity($incident->loan->community_id)) ||
            $user->id === $incident->assignee_id ||
            $incident->loanable->hasOwnerOrCoowner($user);
    }

    public function reopen(User $user, Incident $incident): bool
    {
        return $user->isAdmin() ||
            ($incident->loan &&
                $user->isAdminOfCommunity($incident->loan->community_id)) ||
            $user->id === $incident->assignee_id ||
            $incident->loanable->hasOwnerOrCoowner($user);
    }

    public function changeAssignee(User $user, Incident $incident): bool
    {
        return $user->isAdmin() ||
            ($incident->loan &&
                $user->isAdminOfCommunity($incident->loan->community_id)) ||
            $user->id === $incident->assignee_id ||
            $incident->loanable->hasOwnerOrCoowner($user);
    }

    // Comments and notes are only accessible if incident is blocking or if user is (co)-owner or admin
    public function viewIncidentDetails(User $user, Incident $incident): bool
    {
        return $user->isAdmin() ||
            // Admin or borrower of source loan
            ($incident->loan &&
                ($user->isAdminOfCommunity($incident->loan->community_id) ||
                    $incident->loan->borrower_user_id === $user->id)) ||
            $user->id === $incident->assignee_id ||
            $incident->loanable->hasOwnerOrCoowner($user) ||
            ($incident->show_details_to_blocked_borrowers &&
                $incident->isBlockingUserLoan($user));
    }
}
