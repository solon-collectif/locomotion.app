<?php

namespace App\Models\Policies;

use App\Enums\ReportStatus;
use App\Models\User;
use App\Models\UserPublishableReport;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPublishableReportPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->isAdmin();
    }

    public function view(User $user, UserPublishableReport $userReport): bool
    {
        return $user->isAdmin() ||
            ($userReport->user_id === $user->id &&
                $userReport->report->status === ReportStatus::Published);
    }
}
