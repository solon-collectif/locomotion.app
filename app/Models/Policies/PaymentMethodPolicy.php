<?php

namespace App\Models\Policies;

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentMethodPolicy
{
    use HandlesAuthorization;

    public function view(User $user, PaymentMethod $paymentMethod): bool
    {
        return $user->isAdmin() || $paymentMethod->user_id === $user->id;
    }

    public function delete(User $user, PaymentMethod $paymentMethod): bool
    {
        return $user->isAdmin() || $paymentMethod->user_id === $user->id;
    }
}
