<?php

namespace App\Models\Policies;

use App\Enums\LoanStatus;
use App\Models\Incident;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanComment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class LoanPolicy
{
    use HandlesAuthorization;

    public static function isAdminOfLoanCommunity(User $user, Loan $loan): bool
    {
        if (!$loan->community_id) {
            return false;
        }
        return $user->isAdminOfCommunity($loan->community_id);
    }

    public static function isLoanAdmin(User $user, Loan $loan): bool
    {
        return $user->isAdmin() || self::isAdminOfLoanCommunity($user, $loan);
    }

    public static function isBorrower(User $user, Loan $loan): bool
    {
        return $user->is($loan->borrowerUser);
    }

    public static function isCoownerOrOwner(User $user, Loan $loan): bool
    {
        return $loan->loanable->hasOwnerOrCoowner($user);
    }

    public function create(User $user, Loanable $loanable): bool
    {
        if ($user->isAdmin()) {
            return true;
        }

        return Loanable::accessibleBy($user)
            ->where("id", $loanable->id)
            ->exists();
    }

    public function update(User $user, Loan $loan): bool
    {
        if (!$loan->is_in_process) {
            return false;
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function updateDates(User $user, Loan $loan): Response|bool
    {
        if (
            $loan->status !== LoanStatus::Requested &&
            $loan->status !== LoanStatus::Accepted &&
            $loan->status !== LoanStatus::Confirmed
        ) {
            return $this->deny(__("validation.custom.departure_time"));
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function updateTip(User $user, Loan $loan): bool
    {
        if (!$loan->is_in_process) {
            return false;
        }

        return self::isBorrower($user, $loan);
    }

    public function view(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function viewOwnerInvoice(User $user, Loan $loan): bool
    {
        return self::isCoownerOrOwner($user, $loan) || $user->isAdmin();
    }

    public function viewBorrowerInvoice(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function comment(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }
    public function deleteLoanComment(
        User $user,
        Loan $loan,
        LoanComment $comment
    ): bool {
        return $comment->author_id === $user->id || $user->isAdmin();
    }

    public function viewInstructions(User $user, Loan $loan): bool
    {
        return in_array($loan->status, [
            LoanStatus::Confirmed,
            LoanStatus::Ongoing,
            LoanStatus::Ended,
            LoanStatus::Validated,
        ]) &&
            (self::isBorrower($user, $loan) ||
                $user->can("viewInstructions", $loan->loanable));
    }

    public function viewTrustedBorrowerInstructions(
        User $user,
        Loan $loan
    ): bool {
        return in_array($loan->status, [
            LoanStatus::Confirmed,
            LoanStatus::Ongoing,
            LoanStatus::Ended,
            LoanStatus::Validated,
        ]) && $user->can("viewTrustedBorrowerInstructions", $loan->loanable);
    }

    public function accept(User $user, Loan $loan)
    {
        if ($loan->status !== LoanStatus::Requested) {
            return $this->deny(
                __("state.loan.must_have_status", [
                    "status" => LoanStatus::Requested->value,
                ])
            );
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function reject(User $user, Loan $loan)
    {
        if ($loan->status !== LoanStatus::Requested) {
            return $this->deny(
                __("state.loan.must_have_status", [
                    "status" => LoanStatus::Requested->value,
                ])
            );
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function cancel(User $user, Loan $loan)
    {
        if (!$loan->is_in_process) {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        if (
            self::isLoanAdmin($user, $loan) ||
            self::isCoownerOrOwner($user, $loan)
        ) {
            // Admins, owners and coowners can cancel any time
            return true;
        }

        if (!self::isBorrower($user, $loan)) {
            return false;
        }

        if (
            $loan->is_free ||
            $loan->status !== LoanStatus::Ongoing ||
            Carbon::now()->isBefore($loan->departure_at) ||
            $loan->loanable->activeIncidents
                ->filter(
                    fn(Incident $incident) => $incident->isBlockingLoan($loan)
                )
                ->count() > 0
        ) {
            return true;
        }

        return $this->deny(__("state.loan.must_not_be_ongoing_with_cost"));
    }

    public function resume(User $user, Loan $loan)
    {
        if ($loan->loanable->trashed()) {
            return $this->deny(__("state.loanable.must_be_active"));
        }

        if ($loan->loanable->owner_user->trashed()) {
            return $this->deny(__("state.owner.must_be_active"));
        }

        if ($loan->borrowerUser->trashed()) {
            return $this->deny(__("state.borrower.must_be_active"));
        }

        if (!$loan->isCanceled()) {
            return $this->deny(__("state.loan.must_be_canceled"));
        }

        return self::isLoanAdmin($user, $loan);
    }

    public function requestExtension(User $user, Loan $loan)
    {
        if (!$loan->is_in_process) {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function acceptExtension(User $user, Loan $loan)
    {
        if (!$loan->is_in_process) {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function rejectExtension(User $user, Loan $loan)
    {
        if (!$loan->is_in_process) {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function cancelExtension(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function updateLoanInfo(User $user, Loan $loan): Response|bool
    {
        if (!$loan->is_in_process) {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        // Admins can change info at all times
        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan);
    }

    public function validateLoanInfo(User $user, Loan $loan)
    {
        if (
            $loan->status !== LoanStatus::Ended &&
            $loan->status !== LoanStatus::Ongoing
        ) {
            return $this->deny(
                __("state.loan.must_have_status", ["status" => "ended"])
            );
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            $user->isAdmin();
    }

    public function skipAutoValidation(User $user, Loan $loan)
    {
        if (
            $loan->status !== LoanStatus::Ended &&
            $loan->status !== LoanStatus::Ongoing
        ) {
            return $this->deny(
                __("state.loan.must_have_status", ["status" => "ended"])
            );
        }

        return self::isCoownerOrOwner($user, $loan) || $user->isAdmin();
    }

    public function prepay(User $user, Loan $loan): Response|bool
    {
        if ($loan->status !== LoanStatus::Accepted) {
            return $this->deny(
                __("state.loan.must_have_status", [
                    "status" => LoanStatus::Accepted->value,
                ])
            );
        }

        if (!$user->isAdmin() && !$loan->borrowerCanPay()) {
            return $this->deny(__("state.payment.borrower_cant_pay"));
        }

        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function pay(
        User $user,
        Loan $loan,
        bool $canIgnoreTip = false
    ): Response|bool {
        // Cannot pay (or complete loan) before loan departure.
        if ((new Carbon())->lt(new Carbon($loan->departure_at))) {
            return $this->deny(
                __("state.action.must_be_after_departure", [
                    "action" => "payment",
                ])
            );
        }

        if (!$loan->borrowerCanPayMandatoryTotal()) {
            return $this->deny(__("state.payment.borrower_cant_pay"));
        }

        if (self::isLoanAdmin($user, $loan) && $loan->is_accepted) {
            return true;
        }

        if (!$loan->has_required_info_to_validate) {
            return $this->deny(__("state.loan.must_have_required_info"));
        }

        if ($loan->needs_validation && !$loan->isFullyValidated()) {
            return $this->deny(__("state.payment.loan_needs_validation"));
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan);
    }
}
