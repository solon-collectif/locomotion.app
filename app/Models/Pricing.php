<?php

namespace App\Models;

use App\Calendar\Interval;
use App\Enums\PricingLoanableTypeValues;
use App\Http\Resources\LoanablePricingResource;
use App\Rules\PricingRule;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

/**
 * @property-read PricingLoanableTypeValues[] $pricing_loanable_types
 */
class Pricing extends BaseModel
{
    use HasFactory;
    use SoftDeletes;

    public static $language;

    public static $rules = [
        "name" => "required",
        "rule" => ["nullable"],
        "pricing_type" => ["required", "in:price,insurance,contribution"],
        "description" => ["nullable", "max:150"],
        "loanable_ownership_type" => ["required", "in:all,fleet,non_fleet"],
        "yearly_target_per_user" => ["nullable", "numeric"],
        "is_mandatory" => ["nullable", "boolean"],
        "start_date" => ["required", "date", "date_format:Y-m-d"],
        "end_date" => [
            "nullable",
            "date",
            "date_format:Y-m-d",
            "after:start_date",
        ],
    ];

    protected $casts = [
        "yearly_target_per_user" => "float",
        "is_mandatory" => "boolean",
        "start_date" => "string",
        "end_date" => "string",
    ];

    public $computed = ["is_global", "pricing_loanable_types"];

    public static $filterTypes = [
        "id" => "number",
        "name" => "text",
        "pricing_type" => "enum",
        "loanable_ownership_type" => "enum",
        "community_id" => "number",
    ];

    public static function getRules($action = "", $auth = null)
    {
        $rules = static::$rules;
        $rules["rule"][] = new PricingRule();
        $rules["pricing_loanable_types"] = [
            "required",
            "array",
            Rule::in(PricingLoanableTypeValues::possibleTypes),
        ];
        return $rules;
    }

    /**
     * @return null|array|float
     *     null if line is invalid
     *     array of two floats for [trip_cost, insurance_cost]
     *     float for trip_cost only
     */
    public static function evaluateRuleLine($line, $data)
    {
        $language = static::getExpressionLanguage();

        // Skip:
        // - comments (lines starting with # preceded by any whitespace).
        // - empty lines or containing whitespace only
        if (preg_match("/^\s*#/", $line) || preg_match('/^\s*$/', $line)) {
            return null;
        }

        if (preg_match('/^SI\s+.+?\s+ALORS\s+.+$/', $line)) {
            $line = str_replace("SI", "", $line);
            $line = str_replace("ALORS", "?", $line);
            $line .= ": null";
        }

        $line = str_replace('$KM', "km", $line);
        $line = str_replace('$MINUTES', "minutes", $line);
        $line = str_replace('$OBJET', "loanable", $line);
        $line = str_replace('$EMPRUNT', "loan", $line);
        $line = str_replace(
            '$PRIME_PAR_JOUR',
            "loanable.daily_premium_from_value_category",
            $line
        );

        $line = str_replace(
            '$FACTEUR_PRIME_PAR_JOUR',
            "(loanable.daily_premium_factor_from_value_category
             + (loanable.type == 'car' and (loan.loanable_age_for_insurance <= 5))) / 4",
            $line
        );

        $line = str_replace(
            '$SURCOUT_ASSURANCE',
            "(loanable.type == 'car' and (loan.loanable_age_for_insurance <= 5))",
            $line
        );

        $line = str_replace(" NON ", " !", $line);
        $line = str_replace(" OU ", " or ", $line);
        $line = str_replace(" ET ", " and ", $line);

        $line = str_replace(" PAS DANS ", " not in ", $line);
        $line = str_replace(" DANS ", " in ", $line);

        return $language->evaluate($line, $data);
    }

    public static function getExpressionLanguage()
    {
        if (self::$language) {
            return self::$language;
        }

        $language = new ExpressionLanguage();
        $language->register(
            "MIN",
            function ($a, $b) {
                return sprintf(
                    '(is_numeric(%1$s) && is_numeric(%2$s) ? min(%1$s, %2$s) : null)',
                    $a,
                    $b
                );
            },
            function ($arguments, $a, $b) {
                if (!is_numeric($a) || !is_numeric($b)) {
                    return null;
                }

                return min($a, $b);
            }
        );
        $language->register(
            "MAX",
            function ($a, $b) {
                return sprintf(
                    '(is_numeric(%1$s) && is_numeric(%2$s) ? max(%1$s, %2$s) : null)',
                    $a,
                    $b
                );
            },
            function ($arguments, $a, $b) {
                if (!is_numeric($a) || !is_numeric($b)) {
                    return null;
                }

                return max($a, $b);
            }
        );
        $language->register(
            "PLANCHER",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(floor(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(floor($str));
            }
        );
        $language->register(
            "PLAFOND",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(ceil(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(ceil($str));
            }
        );
        $language->register(
            "ARRONDI",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(round(%1$s)) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return intval(round($str));
            }
        );
        $language->register(
            "DOLLARS",
            function ($str) {
                return sprintf(
                    '(is_numeric(%1$s) ? intval(round(%1$s), 2) : %1$s)',
                    $str
                );
            },
            function ($arguments, $str) {
                if (!is_numeric($str)) {
                    return $str;
                }

                return number_format(round($str, 2), 2);
            }
        );

        self::$language = $language;

        return self::$language;
    }

    public static function dateToDataObject($date)
    {
        $date = new CarbonImmutable($date);
        return (object) [
            "year" => $date->year,
            "month" => $date->month,
            "day" => $date->day,
            "hour" => $date->hour,
            "minute" => $date->minute,
            "day_of_year" => $date->dayOfYear,
        ];
    }

    protected $fillable = [
        "name",
        "rule",
        "community_id",
        "description",
        "pricing_type",
        "loanable_ownership_type",
        "yearly_target_per_user",
        "is_mandatory",
        "start_date",
        "end_date",
    ];

    public $items = ["community"];

    /**
     * @return BelongsTo<Community, $this>
     */
    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return HasMany<PricingLoanableTypes, $this>
     */
    public function pricingLoanableTypes(): HasMany
    {
        return $this->hasMany(PricingLoanableTypes::class);
    }

    /**
     * @param array{days?:mixed,start?:mixed,end?:mixed,loanable_age_for_insurance?:int} $loanData
     */
    public function evaluateRule(
        $km,
        $minutes,
        ?Loanable $loanable = null,
        array $loanData = []
    ): float|int|null {
        $lines = explode("\n", $this->rule);

        $loanableData = $loanable
            ? (new LoanablePricingResource($loanable))->resolve()
            : [];

        if (isset($loanData["start"])) {
            $loanData["start"] = self::dateToDataObject($loanData["start"]);
        }
        if (isset($loanData["end"])) {
            $loanData["end"] = self::dateToDataObject($loanData["end"]);
        }

        $roundCost = function ($currency) {
            return round($currency * 100.0) / 100;
        };

        $response = null;
        foreach ($lines as $line) {
            try {
                $response = static::evaluateRuleLine($line, [
                    "km" => $km,
                    "minutes" => $minutes,
                    "loanable" => (object) $loanableData,
                    "loan" => (object) $loanData,
                ]);

                // Skip lines that return null (comments, etc.)
                if ($response !== null) {
                    break;
                }
            } catch (SyntaxError) {
                // Should not happen but let it go at this point
            }
        }
        return is_numeric($response) ? $roundCost($response) : null;
    }

    public function scopeSortForCommunity(
        Builder $query,
        bool $hasCommunity
    ): Builder {
        if (!$hasCommunity) {
            return $query;
        }
        return $query->orderBy("community_id")->orderBy("pricing_type");
    }

    public function scopeForCommunity(Builder $query, $communityId): Builder
    {
        if ($communityId === "new") {
            return $query->whereNull("community_id");
        }

        return $query->where(
            fn(Builder $b) => $b
                ->whereNull("community_id")
                ->orWhere("community_id", $communityId)
        );
    }

    public function scopeHasLibrary(Builder $query, bool $hasLibrary = true)
    {
        if ($hasLibrary) {
            return $query->whereIn("loanable_ownership_type", ["fleet", "all"]);
        }

        return $query->whereIn("loanable_ownership_type", ["non_fleet", "all"]);
    }

    public function scopeForLoanable(
        Builder $query,
        Loanable $loanable
    ): Builder {
        return $query
            ->pricingLoanableTypes(
                PricingLoanableTypeValues::forLoanable($loanable)
            )
            ->hasLibrary(!!$loanable->library_id);
    }

    public function scopeYearlyContribution(Builder $query): Builder
    {
        return $query
            ->whereNotNull("yearly_target_per_user")
            ->where("pricing_type", "contribution");
    }

    public function scopeExpired(Builder $query, $value): Builder
    {
        $expired = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        if ($expired) {
            return $query->where(
                "end_date",
                "<=",
                CarbonImmutable::today(
                    config("app.default_user_timezone")
                )->format("Y-m-d")
            );
        }

        return $query->where(
            fn(Builder $query) => $query
                ->where(
                    "end_date",
                    ">",
                    CarbonImmutable::today(
                        config("app.default_user_timezone")
                    )->format("Y-m-d")
                )
                ->orWhereNull("end_date")
        );
    }

    public function activeOn($date): bool
    {
        $date = (new CarbonImmutable(
            $date,
            config("app.default_user_timezone")
        ))->format("Y-m-d");

        return (new CarbonImmutable($this->start_date))->lte($date) &&
            ($this->end_date === null ||
                (new CarbonImmutable($this->end_date))->gt($date));
    }

    public function scopeActiveOn(Builder $query, $date): Builder
    {
        $date = (new CarbonImmutable(
            $date,
            config("app.default_user_timezone")
        ))->format("Y-m-d");
        return $query
            ->where("start_date", "<=", $date)
            ->where(
                fn(Builder $query) => $query
                    ->whereNull("end_date")
                    ->orWhere("end_date", ">", $date)
            );
    }

    public function intersects(Interval $interval): bool
    {
        $interval = $interval->atTimezone(config("app.default_user_timezone"));

        if ($this->end_date && $interval->start->gt($this->end_date)) {
            return false;
        }
        // If interval end is start of day, then we do not need the pricings that start on the end
        // day since it actually is excluded.
        if ($interval->end->isStartOfDay()) {
            return $interval->end->gt($this->start_date);
        }
        return $interval->end->gte($this->start_date);
    }
    public function scopeIntersects(Builder $query, Interval $interval)
    {
        $interval = $interval->atTimezone(config("app.default_user_timezone"));

        // If interval end is start of day, then we do not need the pricings that start on the end
        // day since it actually is excluded.
        $includeIntervalEndDay = !$interval->end->isStartOfDay();

        return $query
            ->where(
                "start_date",
                $includeIntervalEndDay ? "<=" : "<",
                $interval->end->format("Y-m-d")
            )
            ->where(
                fn(Builder $query) => $query
                    ->whereNull("end_date")
                    ->orWhere(
                        "end_date",
                        ">",
                        $interval->start->format("Y-m-d")
                    )
            );
    }

    public function getPricingLoanableTypesAttribute()
    {
        return $this->pricingLoanableTypes()
            ->pluck("pricing_loanable_type")
            ->values()
            ->toArray();
    }

    public function scopeIsGlobal(Builder $query, $value, $negative): Builder
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) !== $negative) {
            return $query->whereNull("community_id");
        }
        return $query->whereNotNull("community_id");
    }

    public function scopePricingLoanableTypes(Builder $query, $type): Builder
    {
        return $query->whereHas(
            "pricingLoanableTypes",
            fn(Builder $loanableType) => $loanableType->where(
                "pricing_loanable_type",
                $type
            )
        );
    }

    public function getIsGlobalAttribute()
    {
        return $this->community_id === null;
    }

    public function getStartDateCarbonAttribute(): CarbonImmutable
    {
        return (new CarbonImmutable(
            $this->start_date,
            config("app.default_user_timezone")
        ))->startOfDay();
    }

    public function getEndDateCarbonAttribute(): ?CarbonImmutable
    {
        return $this->end_date
            ? (new CarbonImmutable(
                $this->end_date,
                config("app.default_user_timezone")
            ))->startOfDay()
            : null;
    }
}
