<?php

namespace App\Models;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Enums\LoanableUserRoles;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class LoanableUserRole extends BaseModel
{
    use HasFactory;

    protected $casts = [
        "role" => LoanableUserRoles::class,
    ];

    public static function booted()
    {
        self::creating(function (LoanableUserRole $loanableUserRole) {
            if (
                $loanableUserRole->loanable &&
                $loanableUserRole->role === LoanableUserRoles::Coowner
            ) {
                LoanablesByTypeAndCommunity::forgetForLoanable(
                    $loanableUserRole->loanable
                );
            }
        });

        self::updating(function (LoanableUserRole $model) {
            if (
                $model->loanable &&
                $model->wasChanged("show_as_contact") &&
                $model->role === LoanableUserRoles::Coowner
            ) {
                LoanablesByTypeAndCommunity::forgetForLoanable(
                    $model->loanable
                );
            }
        });

        self::deleting(function (LoanableUserRole $model) {
            if (
                $model->loanable &&
                $model->role === LoanableUserRoles::Coowner &&
                $model->show_as_contact
            ) {
                LoanablesByTypeAndCommunity::forgetForLoanable(
                    $model->loanable
                );
            }
        });
    }

    public static $rules = [
        "loanable_id" => "int",
        "user_id" => "int",
        "role" => "in:owner,coowner,manager,trusted_borrower",
        "title" => "nullable|string",
        "show_as_contact" => "boolean",
        "pays_loan_price" => "boolean",
        "pays_loan_insurance" => "boolean",
    ];

    public $fillable = [
        "user_id",
        "loanable_id",
        "role",
        "title",
        "show_as_contact",
        "pays_loan_price",
        "pays_loan_insurance",
    ];
    public $items = ["user", "loanable", "granted_by_user"];

    public static $filterTypes = [
        "ressource_type" => "enum",
        "ressource_id" => "number",
        "user_id" => "number",
        "role" => "enum",
        "title" => "text",
        "show_as_contact" => "boolean",
        "pays_loan_price" => "boolean",
        "pays_loan_insurance" => "boolean",
    ];

    /**
     * We include archived users here, so we can get some information from co-owners when fetching
     * loanables. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function grantedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return MorphTo<Loanable|Library, LoanableUserRole>
     */
    public function ressource(): MorphTo
    {
        return $this->morphTo("ressource");
    }

    public function getLoanableAttribute(): ?Loanable
    {
        if ($this->ressource_type === "loanable") {
            return $this->ressource;
        }
        return null;
    }
}
