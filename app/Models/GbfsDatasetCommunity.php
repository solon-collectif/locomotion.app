<?php

namespace App\Models;

use App\Models\Pivots\BasePivot;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GbfsDatasetCommunity extends BasePivot
{
    protected $table = "gbfs_dataset_communities";
    public $timestamps = false;

    public $items = ["gbfs_dataset", "community"];
    public $computed = ["loanables_count"];

    public static $filterTypes = [
        "gbfs_dataset_name" => "text",
        "community_id" => "number",
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(
            "active-community",
            fn(Builder $builder) => $builder->whereHas(
                "community",
                fn(Builder $q) => $q->withoutTrashed()
            )
        );
    }

    /**
     * @return BelongsTo<GbfsDataset, $this>
     */
    public function gbfsDataset(): BelongsTo
    {
        return $this->belongsTo(GbfsDataset::class);
    }

    /**
     * @return BelongsTo<Community, $this>
     */
    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class);
    }

    public function getLoanablesCountAttribute()
    {
        return Loanable::query()
            ->sharedInCommunity($this->community_id)
            ->where("published", true)
            ->where("shared_publicly", true)
            ->count();
    }
}
