<?php

namespace App\Models;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Casts\Bytea;
use App\Enums\FileMoveResult;
use App\Facades\StorageTransaction;
use App\Helpers\Path;
use App\Helpers\StorageHelper;
use App\Models\Pivots\CommunityUser;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Intervention\Image\ImageManager;
use Storage;

class Image extends BaseModel
{
    use HasFactory;

    public static $rules = [
        "imageable_type" => "nullable",
        "imageable_id" => "nullable",
        "path" => "required",
        "filename" => "required",
        "original_filename" => "required",
        "field" => "nullable",
        "width" => "required",
        "height" => "required",
        "orientation" => "required",
    ];

    protected $fillable = [
        "field",
        "filename",
        "filesize",
        "height",
        "imageable_id",
        "imageable_type",
        "original_filename",
        "path",
        "width",
        "order",
    ];

    public static string $blurFormat = "webp";

    public static string $orginalSizeName = "original";

    public static array $sizes = [
        "thumbnail" => [
            "dimensions" => [
                "width" => 256,
            ],
            "resizing" => "fit",
            "format" => "jpg",
        ],
    ];

    protected $casts = [
        "blur" => Bytea::class,
    ];

    public static function fetch($path): ?\Intervention\Image\Image
    {
        $file = Storage::get($path);
        if (!$file) {
            return null;
        }

        $manager = new ImageManager(["driver" => "imagick"]);
        return $manager->make($file);
    }

    public static function store(
        $path,
        \Intervention\Image\Image $image,
        $format = null
    ): bool {
        $image->encode($format);
        return StorageTransaction::put($path, $image->getEncoded());
    }

    public function updateFilePath()
    {
        if ($this->isTemp()) {
            $this->syncThumbnailFiles();
            return FileMoveResult::unchanged;
        }

        $filePath = $this->generatePath();

        if ($filePath === $this->path) {
            return FileMoveResult::unchanged;
        }

        $newFilename =
            $this->field . "." . pathinfo($this->filename, PATHINFO_EXTENSION);

        // Move folder with all the thumbnails.
        $moveResult = StorageHelper::moveDirectoryFiles(
            $this->path,
            $filePath,
            $this->filename,
            $newFilename
        );

        if ($moveResult !== FileMoveResult::failed) {
            // Cleanup previous directory
            $this->deleteDirectory();

            $this->path = $filePath;
            $this->filename = $newFilename;
            $this->syncThumbnailFiles();
        }

        return $moveResult;
    }

    public function deleteDirectory(): void
    {
        $deleted = StorageTransaction::deleteDirectory($this->path);
        if (!$deleted) {
            \Log::warning("[Image.php] Failed to delete directory $this->path");
        }
    }

    public function getThumbnailSizesAttribute(): array
    {
        $imageableClass = Relation::getMorphedModel($this->imageable_type);

        if ($imageableClass && isset($imageableClass::$sizes)) {
            return array_merge(self::$sizes, $imageableClass::$sizes);
        }
        return self::$sizes;
    }

    public function isValidSize(string $size): bool
    {
        if ($size === self::$orginalSizeName) {
            return true;
        }

        return array_has($this->thumbnail_sizes, $size);
    }

    /**
     * Ensures expected thumbnails are present in the file folder and that no extra files are
     * present in the image directory.
     */
    public function syncThumbnailFiles(array $sizesToRegenerate = []): array
    {
        // Ensure expected sizes exist
        $expectedSizePaths = [];
        $imagePath = $this->full_path;
        $canvas = null;
        $sizesCreated = [];

        foreach ($this->thumbnail_sizes as $sizeName => $sizeDefinition) {
            $targetPath = $this->getImageSizePath($sizeName);
            $expectedSizePaths[] = $targetPath;
            if (
                Storage::exists($targetPath) &&
                !in_array($sizeName, $sizesToRegenerate)
            ) {
                continue;
            }

            // Fetch the image at most once for all sizes to generate
            if (!$canvas) {
                $canvas = self::fetch($imagePath);
                if (!$canvas) {
                    \Log::error("Could not create thumbnail for $imagePath");
                    return [];
                }
            }

            $thumbnail = $this->createImageSize($sizeDefinition, clone $canvas);

            $saved = Image::store(
                $targetPath,
                $thumbnail,
                $sizeDefinition["format"]
            );
            if (!$saved) {
                \Log::error("Could not save thumbnail $targetPath");
            } else {
                $sizesCreated[] = $sizeName;
            }
        }

        $expectedSizePaths[] = $this->full_path;

        $toRemove = Path::arrayDiffAbsolute(
            Storage::files($this->path),
            $expectedSizePaths
        );

        $deleted = StorageTransaction::delete($toRemove);
        if (!$deleted) {
            \Log::warning("Failed deleting image $this->id old thumbnails.");
        }

        return [
            "created" => $sizesCreated,
            "deleted" => $toRemove,
        ];
    }

    /**
     * Creates a thumnail image and saves it to storage
     *
     * @param array $size the size for the thumbnail
     * @param \Intervention\Image\Image $canvas
     * @return \Intervention\Image\Image
     */
    private function createImageSize(
        array $size,
        \Intervention\Image\Image $canvas
    ): \Intervention\Image\Image {
        $width = $size["dimensions"]["width"] ?? null;
        $height = $size["dimensions"]["height"] ?? null;

        $type = $size["resizing"] ?? "fit";

        // We mean fit as keep the aspect ratio the same and only resize.
        if ($type === "fit") {
            $canvas->resize($width, $height, function ($constraint) {
                // preserve aspect ratio
                $constraint->aspectRatio();
                // Do not upsize
                $constraint->upsize();
            });
        } elseif ($type === "crop") {
            // Intervention means fit as crop then resize to given dimenions
            $canvas->fit(
                $width,
                $height,
                function ($constraint) {
                    // Do not upsize
                    $constraint->upsize();
                },
                "top"
            );
        }

        /** @var \Imagick $core */
        $core = $canvas->getCore();
        try {
            // Remove comments and metadata for thumbnails, which can greatly inflate the size
            // Especially for the blurred placeholder.
            $core->stripImage();
        } catch (\ImagickException $e) {
            \Log::warning("[Image] Failed to strip the image $this->id.", [
                "exception" => $e,
            ]);
        }
        return $canvas;
    }

    public static function boot(): void
    {
        parent::boot();

        self::saving(function (Image $model) {
            $copyResult = $model->updateFilePath();
            if ($copyResult === FileMoveResult::failed) {
                throw new Exception(
                    "[Image.php] Failed updating image $model->id location."
                );
            }
            if (!$model->blur) {
                $model->createBlur();
            }
        });

        self::saved(function (Image $model) {
            if ($model->isDirty()) {
                self::clearLoanableCache($model);
            }
        });

        self::deleted(function (Image $model) {
            $model->deleteDirectory();
            self::clearLoanableCache($model);
        });
    }

    public function createBlur(): void
    {
        $canvas = self::fetch($this->full_path);
        if (!$canvas) {
            \Log::error("Could not create blur thumbnail for image $this->id");
            return;
        }
        $image = $this->createImageSize(
            [
                "dimensions" => [
                    "width" => 16,
                ],
                "resizing" => "fit",
            ],
            $canvas
        );

        $image->encode(self::$blurFormat);
        $this->blur = $image->getEncoded();
    }

    private static function clearLoanableCache(Image $image): void
    {
        if ($image->imageable_type === "user" && $image->field === "avatar") {
            /** @var User|null $user */
            $user = User::find($image->imageable_id);
            if (!$user) {
                return;
            }
            LoanablesByTypeAndCommunity::forgetForUser($user);
            return;
        }

        if ($image->imageable_type === "loanable") {
            $loanable = $image->imageable;
            if (!$loanable || !is_a($loanable, Loanable::class)) {
                return;
            }
            LoanablesByTypeAndCommunity::forgetForLoanable($loanable);
        }
    }

    protected $hidden = ["imageable", "imageable_type", "imageable_id", "blur"];

    protected $appends = ["sizes"];

    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            "imageable_id"
        )->whereImageableType(User::class);
    }

    public function communityUser()
    {
        return $this->belongsTo(
            CommunityUser::class,
            "imageable_id"
        )->whereImageableType(CommunityUser::class);
    }

    public function isTemp()
    {
        return !$this->load("imageable")->imageable;
    }

    public function getSizesAttribute(): array
    {
        $sizes = [
            "api" => array_merge(
                [self::$orginalSizeName],
                array_keys($this->thumbnail_sizes)
            ),
        ];

        if ($this->blur) {
            $sizes["blur"] = $this->encodeBase64String(
                $this->blur,
                self::$blurFormat
            );
        }

        return $sizes;
    }

    public function getFullPathAttribute(): string
    {
        return Path::join($this->path, $this->filename);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if (!$user) {
            return $query->where(\DB::raw("1 = 0"));
        }

        if ($user->isAdmin()) {
            return $query;
        }

        // Image is...
        return $query
            // ...associated to a user
            ->where(function ($q) use ($user) {
                return $q->whereHas("user", function ($q) use ($user) {
                    return $q->accessibleBy($user);
                });
            })
            // ...or associated to a community user
            ->orWhere(function ($q) use ($user) {
                return $q->whereHas("communityUser", function ($q) use ($user) {
                    return $q->accessibleBy($user);
                });
            })
            // ...or a temporary file
            ->orWhere(function ($q) {
                return $q->whereImageableType(null);
            });
    }

    /**
     * @param string $sizeName
     * @return string
     */
    public function getImageSizePath(string $sizeName): string
    {
        if ($sizeName === self::$orginalSizeName) {
            return $this->full_path;
        }

        $filename = pathinfo($this->filename, PATHINFO_FILENAME);
        $format =
            $this->thumbnail_sizes[$sizeName]["format"] ??
            pathinfo($this->filename, PATHINFO_EXTENSION);

        // All other sizes are saved as jpg for better compression
        return Path::join($this->path, "{$sizeName}_$filename.$format");
    }

    public function encodeBase64String($data, $type): string
    {
        return "data:image/" . $type . ";base64," . base64_encode($data);
    }

    /**
     * @return string
     */
    private function generatePath(): string
    {
        return Path::build(
            "images",
            $this->imageable->getMorphClass(),
            $this->imageable_id,
            $this->field,
            $this->generateUniqueFolderName()
        );
    }

    public function generateUniqueFolderName(): string
    {
        return hash("md5", $this->id . config("app.key"));
    }
}
