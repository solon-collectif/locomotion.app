<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

abstract class AuthenticatableBaseModel extends Authenticatable
{
    use BaseModelTrait;
}
