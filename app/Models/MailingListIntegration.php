<?php

namespace App\Models;

use App\Enums\MailingListIntegrationProviders;
use App\Enums\MailingListIntegrationStatus;
use App\Http\Resources\MailingListIntegrationResource;
use App\Services\MailingListException;
use App\Services\MailingListService;
use Brevo\Client\ApiException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class MailingListIntegration extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        "community_id",
        "created_by_user_id",
        "provider",
        "api_key",
        "list_id",
        "list_name",
        "status",
        "community_editable",
        "tag",
    ];

    protected $casts = [
        "community_editable" => "boolean",
        "created_at" => "immutable_datetime",
        "updated_at" => "immutable_datetime",
        "status" => MailingListIntegrationStatus::class,
        "provider" => MailingListIntegrationProviders::class,
    ];

    public static $filterTypes = [
        "community_id" => "number",
        "created_by_user_id" => "number",
        "provider" => "enum",
        "list_name" => "text",
        "status" => "enum",
        "community_editable" => "boolean",
        "tag" => "text",
    ];

    public $items = ["createdByUser", "community"];

    protected static $defaultResource = MailingListIntegrationResource::class;

    /**
     * @return BelongsTo<Community, $this>
     */
    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class);
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function createdByUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "created_by_user_id");
    }

    public function getService(): MailingListService
    {
        return \App::makeWith(MailingListService::class, [
            "provider" => $this->provider->value,
            "api_key" => $this->api_key,
            "list_id" => $this->list_id,
            "tag" => $this->tag,
        ]);
    }

    public function getImportBatchSize(): int
    {
        return $this->getService()->getImportBatchSize();
    }

    public function getRemoveBatchSize(): int
    {
        return $this->getService()->getRemoveBatchSize();
    }

    /**
     * @throws \Throwable
     */
    public function sendRequest(\Closure $callback)
    {
        if (!$this->canSendRequests()) {
            return null;
        }

        try {
            $response = $callback($this->getService());
        } catch (MailingListException $e) {
            $this->handleServiceError($e);
            throw $e->getPrevious();
        }
        $this->consecutive_error_count = 0;
        $this->saveQuietly();
        return $response;
    }

    public function canSendRequests(): bool
    {
        return $this->status !== MailingListIntegrationStatus::Suspended;
    }

    public function handleServiceError(MailingListException $exception)
    {
        $prefix = "[MailingList] [{$this->provider->value}]";
        $errorText = match (get_class($exception->getPrevious())) {
            ApiException::class,
            \MailchimpMarketing\ApiException::class
                => "$prefix Failed $exception->action. [{$exception->getCode()}]: {$exception->getMessage()}",
            TooManyRequestsHttpException::class
                => "$prefix Failed $exception->action. Too many simultaneous requests",
            default
                => "$prefix Failed $exception->action: {$exception->getMessage()}",
        };
        $this->consecutive_error_count += 1;
        $this->last_error = $errorText;
        if ($this->status === MailingListIntegrationStatus::Synchronizing) {
            $this->status = MailingListIntegrationStatus::Suspended;
        }
        $this->saveQuietly();
    }

    public function markSynchronized(): void
    {
        $this->status = MailingListIntegrationStatus::Active;
    }
}
