<?php

namespace App\Models;

use App\Enums\LoanableUserRoles;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read LoanableUserRoles $role
 * @property-read int $user_id
 * @property-read int $loanable_id
 * @property-read int $granted_by_user_id
 * @property-read boolean $show_as_contact
 * @property-read boolean $pays_loan_price
 * @property-read boolean $pays_loan_insurance
 * @property-read string $title
 * @property-read string $ressource_type
 */
class MergedLoanableUserRole extends BaseModel
{
    public static $filterTypes = [
        "loanable_id" => "number",
        "user_id" => "number",
        "granted_by_user_id" => "number",
        "role" => "enum",
        "title" => "text",
        "show_as_contact" => "boolean",
        "pays_loan_price" => "boolean",
        "pays_loan_insurance" => "boolean",
    ];

    public $items = ["user", "loanable", "granted_by_user"];

    public function save(array $options = [])
    {
        throw new \Exception("Cannot create or update read-only model");
    }

    public function update(array $attributes = [], array $options = [])
    {
        throw new \Exception("Cannot update read-only model");
    }

    public function delete()
    {
        throw new \Exception("Cannot delete read-only model");
    }

    protected $casts = [
        "role" => LoanableUserRoles::class,
    ];

    /**
     * We include archived users here, so we can get some information from co-owners when fetching
     * loanables. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function grantedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function loanable(): BelongsTo
    {
        return $this->belongsTo(Loanable::class);
    }

    public function getInheritedFromLibraryAttribute()
    {
        return $this->ressource_type === "library";
    }
}
