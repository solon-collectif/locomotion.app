<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bike extends LoanableDetails
{
    use HasFactory;

    public static $rules = [
        "bike_type" => [
            "in:regular,cargo,cargo_electric,electric,fixed_wheel",
            "nullable",
        ],
        "model" => [],
        "size" => ["in:big,medium,small,kid", "nullable"],
    ];

    protected $fillable = ["bike_type", "model", "size"];
}
