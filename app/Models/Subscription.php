<?php

namespace App\Models;

use App\Calendar\Interval;
use App\Enums\BillItemTypes;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Pivots\CommunityUser;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends BaseModel
{
    use SoftDeletes, HasFactory;

    protected $fillable = [
        "start_date",
        "end_date",
        "type",
        "reason",
        "user_id",
        "granted_by_user_id",
        "community_id",
    ];

    protected $casts = [
        "start_date" => "immutable_datetime",
        "end_date" => "immutable_datetime",
    ];

    public static $filterTypes = [
        "start_date" => "date",
        "end_date" => "date",
    ];

    public $items = ["granted_by_user"];

    /**
     * @return BelongsTo<CommunityUser, $this>
     */
    public function communityUser(): BelongsTo
    {
        return $this->belongsTo(CommunityUser::class);
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function grantedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "granted_by_user_id");
    }

    /**
     * @return BelongsTo<Invoice, $this>
     */
    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * @return HasMany<SubscriptionLoanableTypes, $this>
     */
    public function subscriptionLoanableTypes(): HasMany
    {
        return $this->hasMany(SubscriptionLoanableTypes::class);
    }

    public function getLoanableTypesAttribute(): array
    {
        return $this->subscriptionLoanableTypes
            ->pluck("pricing_loanable_type")
            ->values()
            ->toArray();
    }

    public function scopeActive(
        Builder $query,
        CarbonInterface $date = null
    ): Builder {
        if (!$date) {
            $date = Carbon::now();
        }

        return $query
            ->where("start_date", "<=", $date)
            ->where("end_date", ">", $date);
    }

    public function activeOn(?CarbonInterface $date = null): bool
    {
        if ($date === null) {
            $date = CarbonImmutable::now();
        }

        return $this->start_date->lte($date) && $this->end_date->gt($date);
    }

    public function scopeAppliesFor(
        Builder $query,
        PricingLoanableTypeValues $loanableType
    ) {
        return $query->where(
            fn(Builder $q) => $q
                ->where("type", "granted")
                ->orWhereHas(
                    "subscriptionLoanableTypes",
                    fn(Builder $loanableTypeQuery) => $loanableTypeQuery->where(
                        "pricing_loanable_type",
                        $loanableType
                    )
                )
        );
    }

    public function appliesFor(
        PricingLoanableTypeValues $pricingLoanableType
    ): bool {
        return $this->type === "granted" ||
            $this->subscriptionLoanableTypes
                ->where(
                    fn(
                        SubscriptionLoanableTypes $t
                    ) => $t->pricing_loanable_type === $pricingLoanableType
                )
                ->count() > 0;
    }

    public function remainingDays()
    {
        // Compare days in users's timezone, including today
        return max(
            0,
            (int) CarbonImmutable::now(config("app.default_user_timezone"))
                ->startOfDay()
                ->diffInDays(
                    $this->end_date->tz(config("app.default_user_timezone"))
                )
        );
    }

    public function durationInDays()
    {
        // Compare days in user's timezone.
        return (int) $this->start_date
            ->tz(config("app.default_user_timezone"))
            ->diffInDays(
                $this->end_date->tz(config("app.default_user_timezone"))
            );
    }

    public function getRefundBillItems(float $max = null): array
    {
        if ($this->type != "paid" || !$this->invoice) {
            return [];
        }

        $remainingDays = $this->remainingDays();
        $durationInDays = $this->durationInDays();
        $ratio = $remainingDays / $durationInDays;

        $billItemsToReinburse = [];
        $totalToReinburse = 0;

        foreach ($this->invoice->billItems as $item) {
            if (
                $item->item_type !== BillItemTypes::contributionYearly ||
                // We do not re-apply previous reinbursment
                $item->amount > 0
            ) {
                continue;
            }
            $billItemsToReinburse[] = $item;
            $totalToReinburse +=
                round(-$item->amount * $ratio, 2) +
                round(-$item->taxes_tvq * $ratio, 2) +
                round(-$item->taxes_tps * $ratio, 2);
        }

        if ($max && $max < $totalToReinburse) {
            $ratio = ($max / $totalToReinburse) * $ratio;
        }

        $billItems = [];
        foreach ($billItemsToReinburse as $item) {
            $billItems[] = new BillItem([
                "item_type" => BillItemTypes::contributionYearly,
                "label" => sprintf(
                    "Remboursement partiel: %s (%s $ x %d / %d )",
                    $item->label,
                    number_format(-$item->total, 2, ","),
                    $remainingDays,
                    $durationInDays
                ),
                "amount_type" => "corrected",
                "amount" => round(-$item->amount * $ratio, 2),
                "taxes_tvq" => round(-$item->taxes_tvq * $ratio, 2),
                "taxes_tps" => round(-$item->taxes_tps * $ratio, 2),
                "meta" => [
                    ...$item->meta ?? [],
                    "reinbursed_bill_item" => $item->id,
                ],
                "contribution_community_id" => $item->contribution_community_id,
            ]);
        }

        return $billItems;
    }

    public static function newSubscriptionInterval(): Interval
    {
        return new Interval(
            CarbonImmutable::now(config("app.default_user_timezone"))
                ->startOfDay()
                ->tz(config("app.timezone")),
            CarbonImmutable::now(config("app.default_user_timezone"))
                ->addYear()
                ->startOfDay()
                ->tz(config("app.timezone"))
        );
    }

    public static function createForPayment(
        CommunityUser $communityUser,
        $payerUserId,
        Invoice $invoice
    ): Subscription {
        $subscription = new Subscription();
        $subscription->community_user_id = $communityUser->id;
        $subscription->granted_by_user_id = $payerUserId;
        $subscription->type = "paid";
        $subscriptionInterval = self::newSubscriptionInterval();
        $subscription->start_date = $subscriptionInterval->start;
        $subscription->end_date = $subscriptionInterval->end;
        $subscription->invoice_id = $invoice->id;
        $subscription->save();
        return $subscription;
    }

    public static function createForGrant(
        CommunityUser $communityUser,
        $granterUserId,
        $durationInDays = null,
        $reason = null
    ): Subscription {
        $subscription = new Subscription();
        $subscription->community_user_id = $communityUser->id;
        $subscription->granted_by_user_id = $granterUserId;
        $subscription->type = "granted";
        $subscription->reason = $reason;
        $subscriptionInterval = self::newSubscriptionInterval();
        $subscription->start_date = $subscriptionInterval->start;

        $endDate = $durationInDays
            ? CarbonImmutable::now(config("app.default_user_timezone"))
                ->addDays($durationInDays)
                ->startOfDay()
                ->tz(config("app.timezone"))
            : $subscriptionInterval->end;

        $subscription->end_date = $endDate;
        $subscription->save();
        return $subscription;
    }

    public function changeDuration(?int $durationInDays): void
    {
        $this->end_date = $durationInDays
            ? (new CarbonImmutable(
                $this->start_date,
                config("app.default_user_timezone")
            ))
                ->addDays($durationInDays)
                ->startOfDay()
                ->tz(config("app.timezone"))
            : (new CarbonImmutable(
                $this->start_date,
                config("app.default_user_timezone")
            ))
                ->addYear()
                ->startOfDay()
                ->tz(config("app.timezone"));
    }
}
