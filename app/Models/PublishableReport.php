<?php

namespace App\Models;

use App\Enums\ReportStatus;
use App\Enums\ReportType;
use App\Facades\StorageTransaction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PublishableReport extends BaseModel
{
    use HasFactory;

    protected $fillable = ["status", "type", "report_key", "batch_id"];

    protected $casts = [
        "status" => ReportStatus::class,
        "created_at" => "immutable_datetime",
        "updated_at" => "immutable_datetime",
        "type" => ReportType::class,
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            "user_publishable_reports"
        )->using(UserPublishableReport::class);
    }

    public function getProgressAttribute()
    {
        if (!$this->batch_id) {
            return null;
        }

        \Bus::findBatch($this->batch_id)?->progress();
    }

    public function delete()
    {
        $userReportsPaths = UserPublishableReport::where(
            "publishable_report_id",
            $this->id
        )
            ->pluck("path")
            ->toArray();
        StorageTransaction::delete($userReportsPaths);

        return parent::delete();
    }
}
