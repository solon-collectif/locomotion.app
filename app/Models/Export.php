<?php

namespace App\Models;

use App\Helpers\Path;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @property string $model
 * @property int $user_id
 */
class Export extends BaseModel
{
    public $timestamps = false;
    public static function createWithFile(
        string $path,
        string $filename,
        string $model,
        int $user_id
    ): Export {
        $export = new Export();

        $export->model = $model;
        $export->user_id = $user_id;

        $export->save();

        // Create a new file
        \Storage::put(Path::join($path, $filename), "");

        $export->file()->create([
            "original_filename" => $filename,
            "filename" => $filename,
            "field" => "csv",
            "path" => $path,
            "filesize" => 0,
        ]);

        return $export;
    }

    public $morphOnes = [
        "file" => "fileable",
    ];

    public $items = ["user"];

    public static $filterTypes = [
        "id" => "number",
        "started_at" => "date",
        "duration_in_seconds" => "number",
        "status" => "enum",
        "user_id" => "number",
        "progress" => "number",
    ];

    protected $casts = [
        "duration_in_seconds" => "float",
        "progress" => "float",
        "started_at" => "datetime",
    ];

    protected static function boot()
    {
        parent::boot();

        self::deleting(function (Export $deletedExport) {
            if ($deletedExport->file) {
                $deletedExport->file->delete();
            }
        });
    }

    public function start(): void
    {
        $this->status = "in_process";
        $this->progress = 0;
        $this->started_at = Carbon::now();
    }

    public function complete(): void
    {
        $this->status = "completed";
        $this->duration_in_seconds = $this->started_at->diffInMilliseconds();
    }

    public function file(): MorphOne
    {
        return $this->morphOne(File::class, "fileable")->where("field", "csv");
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeAccessibleBy($query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->where("user_id", $user->id);
    }

    public function cancel(): void
    {
        // Can only cancel an ongoing export.
        if ($this->status !== "in_process" || !$this->batch_id) {
            return;
        }

        \Bus::findBatch($this->batch_id)->cancel();
        $this->status = "canceled";
        $this->save();
    }

    public function isCanceled(): bool
    {
        return $this->status === "canceled";
    }
}
