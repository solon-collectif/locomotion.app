<?php

namespace App\Models;

use App\Enums\PricingLoanableTypeValues;

class PricingLoanableTypes extends BaseModel
{
    public $timestamps = false;

    protected $casts = [
        "pricing_loanable_type" => PricingLoanableTypeValues::class,
    ];

    protected $fillable = ["pricing_id", "pricing_loanable_type"];
}
