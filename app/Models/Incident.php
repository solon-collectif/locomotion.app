<?php

namespace App\Models;

use App\Calendar\Interval;
use App\Enums\LoanableUserRoles;
use App\Http\Resources\IncidentResource;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Incident extends BaseModel
{
    use HasFactory;
    public static $rules = [
        "incident_type" => ["required", "in:accident,small_incident,general"],
        "blocking_until" => ["nullable", "date"],
        "comments_on_incident" => ["required"],
        "loan_id" => ["nullable"],
        "loanable_id" => ["required"],
        "show_details_to_blocked_borrowers" => ["boolean"],
    ];

    protected $fillable = [
        "loan_id",
        "loanable_id",
        "incident_type",
        "comments_on_incident",
        "blocking_until",
    ];

    public $items = [
        "loan",
        "resolved_by_user",
        "reported_by_user",
        "assignee",
        "loanable",
    ];
    public $collections = ["notes"];

    public static $filterTypes = [
        "assignee_id" => "number",
        "blocking_until" => "date",
        "comments_on_incident" => "text",
        "created_at" => "date",
        "incident_type" => "enum",
        "loan_id" => "number",
        "loanable_id" => "number",
        "reported_by_user_id" => "number",
        "resolved_by_user_id" => "number",
        "status" => "enum",
        "updated_at" => "date",
    ];

    protected $casts = [
        "executed_at" => "immutable_datetime",
        "created_at" => "immutable_datetime",
        "updated_at" => "immutable_datetime",
        "blocking_until" => "immutable_datetime",
    ];

    public $computed = ["is_blocking", "start_at"];

    protected static $defaultResource = IncidentResource::class;

    public static function boot()
    {
        parent::boot();

        self::saving(function (Incident $model) {
            if ($model->isDirty("status")) {
                if ($model->status === "in_process") {
                    $model->executed_at = null;
                } else {
                    $model->executed_at = CarbonImmutable::now();
                }
            }
        });
    }
    /**
     * @return BelongsTo<User, $this>
     */
    public function reportedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "reported_by_user_id");
    }
    /**
     * @return BelongsTo<User, $this>
     */
    public function resolvedByUser(): BelongsTo
    {
        return $this->belongsTo(User::class, "resolved_by_user_id");
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function assignee(): BelongsTo
    {
        return $this->belongsTo(User::class, "assignee_id");
    }

    public function notes(): HasMany
    {
        return $this->hasMany(IncidentNote::class)->orderBy("id");
    }

    /**
     * @return BelongsTo<Loanable, $this>
     */
    public function loanable(): BelongsTo
    {
        return $this->belongsTo(Loanable::class);
    }

    /**
     * @return BelongsTo<Loan, $this>
     */
    public function loan(): BelongsTo
    {
        return $this->belongsTo(Loan::class);
    }

    public function potentiallyBlockedLoans(): HasManyThrough
    {
        return $this->hasManyThrough(
            Loan::class,
            Loanable::class,
            /*loanables.*/ "id",
            /*loans.*/ "loanable_id",
            /*incident.*/ "loanable_id",
            /*loanables.*/ "id"
        )
            ->inProcess()
            ->accepted();
    }

    public function getBlockedLoansAttribute()
    {
        if (is_null($this->blocking_until)) {
            return collect([]);
        }

        return $this->potentiallyBlockedLoans
            ->filter(
                fn(Loan $loan) => $loan->actual_return_at->gt($this->start_at)
            )
            ->filter(
                fn(Loan $loan) => $loan->departure_at->lt($this->blocking_until)
            );
    }

    public function notifiedUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, "incident_notifications")
            ->using(IncidentNotification::class)
            ->withTimestamps()
            ->withPivot(["level"]);
    }

    public function getStartAtAttribute(): ?CarbonImmutable
    {
        return $this->loan
            ? $this->loan->departure_at->toImmutable()
            : $this->created_at;
    }

    public function getIsBlockingAttribute(): bool
    {
        return $this->blocking_until !== null &&
            CarbonImmutable::now()->lessThan($this->blocking_until);
    }

    public function scopeIsBlocking(Builder $query)
    {
        return $query
            ->whereNotNull("blocking_until")
            ->where("blocking_until", ">", CarbonImmutable::now());
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($for !== "edit" || $user->isAdmin()) {
            return $query;
        }

        return $query->where(
            fn(Builder $q) => $q
                // Community admin
                ->whereHas(
                    "loan.community.approvedUsers",
                    fn(Builder $communityUsers) => $communityUsers
                        ->where("community_user.role", "admin")
                        ->where("community_user.user_id", $user->id)
                )
                // Borrowers
                ->orWhereHas(
                    "loan.borrowerUser",
                    fn(Builder $borrowerUsers) => $borrowerUsers->where(
                        "users.id",
                        $user->id
                    )
                )
                // Assignee
                ->orWhere("assignee_id", $user->id)
                // Co-owners
                ->orWhereHas(
                    "loanable.userRoles",
                    fn(Builder $roles) => $roles
                        ->whereIn("loanable_user_roles.role", [
                            LoanableUserRoles::Owner,
                            LoanableUserRoles::Coowner,
                        ])
                        ->where("loanable_user_roles.user_id", $user->id)
                )
        );
    }

    public function isBlockingUserLoan(User $user): bool
    {
        return $this->blocking_until &&
            $user
                ->loansAsBorrower()
                ->where("loanable_id", $this->loanable_id)
                ->inProcess()
                ->where("departure_at", "<", $this->blocking_until)
                ->where("departure_at", ">", $this->start_at)
                ->exists();
    }

    public function isBlockingInterval(Interval $interval): bool
    {
        return $this->blocking_until &&
            $this->blocking_until->gt($interval->start) &&
            $this->start_at->lte($interval->end);
    }

    public function isBlockingLoan(Loan $loan): bool
    {
        return $loan->status->isInProcess() &&
            $this->blocking_until &&
            $this->blocking_until->gt($loan->departure_at) &&
            $this->start_at->lte($loan->actual_return_at);
    }
}
