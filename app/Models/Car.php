<?php

namespace App\Models;

use App\Enums\CarPricingCategories;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Car extends LoanableDetails
{
    public static $rules = [
        "brand" => [],
        "engine" => ["in:fuel,diesel,electric,hybrid", "nullable"],
        "insurer" => [],
        "model" => [],
        "papers_location" => ["in:in_the_car,to_request_with_car", "nullable"],
        "plate_number" => [],
        "pricing_category" => [
            "in:small,large,small_electric,large_electric",
            "nullable",
        ],
        "transmission_mode" => ["in:manual,automatic", "nullable"],
        "value_category" => ["in:lte50k,lte70k,lte100k", "nullable"],
        "year_of_circulation" => ["digits:4", "numeric", "nullable"],
    ];

    public static function getRules($action = "", $auth = null)
    {
        $rules = parent::getRules($action, $auth);
        $rules["year_of_circulation"][] = "max:" . ((int) date("Y") + 1);
        return $rules;
    }

    protected $fillable = [
        "brand",
        "engine",
        "has_informed_insurer",
        "has_report_in_notebook",
        "has_onboard_notebook",
        "insurer",
        "value_category",
        "model",
        "papers_location",
        "plate_number",
        "pricing_category",
        "transmission_mode",
        "year_of_circulation",
    ];

    protected $casts = [
        "pricing_category" => CarPricingCategories::class,
    ];

    public $items = ["owner"];

    public $computed = [
        "daily_premium_from_value_category",
        "daily_premium_factor_from_value_category",
    ];

    public $morphOnes = [
        "report" => "fileable",
    ];

    public function report(): MorphOne
    {
        return $this->morphOne(File::class, "fileable")->where(
            "field",
            "report"
        );
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // If borrower is not validated, user can only see their own Cars.
        if (!$user->borrower || !$user->borrower->validated) {
            return $query->whereHas(
                "loanable",
                fn($q) => $q->ownedOrCoownedBy($user->id)
            );
        }

        return parent::scopeAccessibleBy($query, $user);
    }

    public function getDailyPremiumFromValueCategoryAttribute()
    {
        return match ($this->value_category) {
            "lte50k" => 4,
            "lte70k" => 5,
            "lte100k" => 6,
            default => 4,
        };
    }

    // It looks like the same code as
    // getDailyPremiumFromValueCategoryAttribute, but it is a different
    // concept.
    public function getDailyPremiumFactorFromValueCategoryAttribute()
    {
        return match ($this->value_category) {
            "lte50k" => 4,
            "lte70k" => 5,
            "lte100k" => 6,
            default => 4,
        };
    }

    public function getAgeForInsurance($loanDepartureAt)
    {
        // Car insurance is renewed Sept. 1st Compute the age of the car at the moment of the renewal.
        $eightMonthsBeforeLoanStart = (new CarbonImmutable(
            $loanDepartureAt
        ))->subMonths(8)->year;

        return $eightMonthsBeforeLoanStart - (int) $this->year_of_circulation;
    }
}
