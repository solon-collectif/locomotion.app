<?php

namespace App\Models;

use App\Enums\LoanNotificationLevel;
use App\Models\Pivots\BasePivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LoanNotification extends BasePivot
{
    protected $table = "loan_notifications";
    protected $casts = [
        "level" => LoanNotificationLevel::class,
        "last_seen" => "datetime",
    ];

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo<Loan, $this>
     */
    public function loan(): BelongsTo
    {
        return $this->belongsTo(Loan::class);
    }
}
