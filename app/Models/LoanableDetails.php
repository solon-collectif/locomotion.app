<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

abstract class LoanableDetails extends BaseModel
{
    use HasFactory;

    public $timestamps = false;
    public $incrementing = false;

    /**
     * @return BelongsTo<Loanable, $this>
     */
    public function loanable(): BelongsTo
    {
        return $this->belongsTo(Loanable::class, "id", "id");
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        return $query->whereHas("loanable", fn($q) => $q->accessibleBy($user));
    }
}
