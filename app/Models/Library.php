<?php

namespace App\Models;

use App\Enums\LoanableUserRoles;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Library extends BaseModel
{
    use HasFactory;

    public $collections = ["loanables", "communities"];

    protected $fillable = [
        "name",
        "phone_number",
        "loan_auto_validation_question",
        "loan_auto_validation_answer",
    ];

    protected $casts = [
        "created_at" => "immutable_datetime",
        "updated_at" => "immutable_datetime",
    ];

    public static $sizes = [
        "library" => [
            // Aspect ratio 16/10
            "dimensions" => [
                "width" => 400,
                "height" => 400,
            ],
            "resizing" => "crop",
            "format" => "jpg",
        ],
    ];

    public function avatar(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "avatar"
        );
    }

    public function getOwnerUserAttribute()
    {
        return $this->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first()?->user;
    }

    public function userRoles(): MorphMany
    {
        return $this->morphMany(LoanableUserRole::class, "ressource");
    }

    public function loanables(): HasMany
    {
        return $this->hasMany(Loanable::class);
    }

    public function communities(): BelongsToMany
    {
        return $this->belongsToMany(Community::class);
    }

    public function userHasRole(User $user, LoanableUserRoles $role): bool
    {
        return $this->userRoles
            ->where("user_id", $user->id)
            ->where("role", $role)
            ->isNotEmpty();
    }

    /**
     * @param User $user
     * @param LoanableUserRoles[] $roles
     * @return bool
     */
    public function userHasSomeRole(User $user, array $roles): bool
    {
        return $this->userRoles
            ->where("user_id", $user->id)
            ->whereIn("role", $roles)
            ->isNotEmpty();
    }

    public function scopeFor(Builder $query, ?string $for, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit", "admin" => $query->whereHas(
                "communities.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            "profile" => $query->whereHas(
                "userRoles",
                fn($q) => $q->where("user_id", $user->id)
            ),

            default => $query,
        };
    }

    public function scopeAccessibleBy(Builder $builder, User $user): Builder
    {
        if ($user->isAdmin()) {
            return $builder;
        }
        return $builder->where(function ($query) use ($user) {
            $query
                ->whereHas(
                    "communities.users",
                    fn($q) => $q
                        ->where("community_user.user_id", $user->id)
                        ->whereNotNull("community_user.approved_at")
                        ->whereNull("community_user.suspended_at")
                        ->where("community_user.role", "admin")
                )
                ->orWhereHas(
                    "userRoles",
                    fn($q) => $q->where("user_id", $user->id)
                );
        });
    }
}
