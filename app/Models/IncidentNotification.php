<?php

namespace App\Models;

use App\Enums\IncidentNotificationLevel;
use App\Models\Pivots\BasePivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IncidentNotification extends BasePivot
{
    protected $table = "incident_notifications";
    protected $casts = [
        "level" => IncidentNotificationLevel::class,
    ];

    /**
     * @return BelongsTo<User, $this>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo<Incident, $this>
     */
    public function incident(): BelongsTo
    {
        return $this->belongsTo(Incident::class);
    }
}
