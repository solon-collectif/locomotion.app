<?php

namespace App\Models;

use App\Models\Pivots\CommunityUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invitation extends BaseModel
{
    use SoftDeletes, HasFactory;

    protected $casts = [
        "expires_at" => "date:Y-m-d",
        "auto_approve" => "boolean",
        "accepted_at" => "datetime",
        "refused_at" => "datetime",
    ];

    public static $rules = [
        "email" => ["required", "email", "max:254"],
        "inviter_user_id" => ["required", "integer"],
        "community_id" => ["required", "integer"],
        "reason" => ["max:1000", "required_if:auto_approve,true"],
        "message" => ["max:2048"],
        "template" => ["in:full,partial"],
        "auto_approve" => ["boolean"],
        "expires_at" => ["required", "date"],
    ];

    protected $hidden = ["confirmation_code"];

    protected $fillable = [
        "email",
        "inviter_user_id",
        "community_id",
        "reason",
        "message",
        "auto_approve",
        "expires_at",
        "template",
    ];

    public $items = ["inviter", "community"];

    public static $filterTypes = [
        "inviter_user_id" => "number",
        "expires_at" => "date",
        "accepted_at" => "date",
        "refused_at" => "date",
        "created_at" => "date",
        "community_id" => "number",
        "auto_approve" => "boolean",
        "reason" => "text",
        "message" => "text",
        "email" => "text",
    ];

    public $computed = ["status"];

    protected static function boot()
    {
        parent::boot();

        self::creating(function (Invitation $invitation) {
            $invitation->generateConfirmationCode();
        });
    }

    /**
     * @return BelongsTo<User, $this>
     */
    public function inviter(): BelongsTo
    {
        return $this->belongsTo(User::class, "inviter_user_id");
    }

    public function community(): BelongsTo
    {
        return $this->belongsTo(Community::class)->withTrashed();
    }

    public function generateConfirmationCode(): void
    {
        $this->confirmation_code = bin2hex(random_bytes(16));
    }

    public function scopeStatus(Builder $query, string $status)
    {
        if ($status === "active") {
            return $query
                ->whereNull("accepted_at")
                ->whereNull("refused_at")
                ->where(
                    fn(Builder $q) => $q
                        ->whereNull("expires_at")
                        ->orWhere("expires_at", ">=", Carbon::now())
                );
        }
        if ($status === "accepted") {
            return $query->whereNotNull("accepted_at");
        }
        if ($status === "refused") {
            return $query->whereNotNull("refused_at");
        }
        if ($status === "expired") {
            return $query
                ->whereNull("accepted_at")
                ->whereNull("refused_at")
                ->whereNotNull("expires_at")
                ->where("expires_at", "<", Carbon::now());
        }

        if ($status === "canceled") {
            return $query->onlyTrashed();
        }
        return $query;
    }

    public function getStatusAttribute(): string
    {
        if (!!$this->deleted_at) {
            return "canceled";
        }

        if (!!$this->refused_at) {
            return "refused";
        }

        if (!!$this->accepted_at) {
            return "accepted";
        }

        if ($this->expires_at && Carbon::now()->isAfter($this->expires_at)) {
            return "expired";
        }

        return "active";
    }

    public function refuse(User $user): void
    {
        if ($this->status !== "active") {
            \Log::warning(
                "[Invitation] Attempted refusing inactive invitation (id: $this->id)"
            );
            return;
        }

        if ($user->email !== $this->email) {
            \Log::warning(
                "[Invitation] Attempted refusing invitation for another user (id: $this->id, email: $user->email)"
            );
            return;
        }

        $this->refused_at = Carbon::now();
        $this->save();
    }

    public function accept(User $user): void
    {
        if ($this->status !== "active") {
            \Log::warning(
                "[Invitation] Attempted accepting inactive invitation (id: $this->id)"
            );
            return;
        }

        if ($user->email !== $this->email) {
            \Log::warning(
                "[Invitation] Attempted accepting invitation for antoher user (id: $this->id, email: $user->email)"
            );
            return;
        }

        $existingCommunityUser = CommunityUser::where("user_id", $user->id)
            ->where("community_id", $this->community_id)
            ->first();

        if (!$existingCommunityUser) {
            $user
                ->communities()
                ->attach($this->community_id, ["join_method" => "invitation"]);

            $existingCommunityUser = CommunityUser::where("user_id", $user->id)
                ->where("community_id", $this->community_id)
                ->firstOrFail();
        }

        if (!$existingCommunityUser->approved_at && $this->auto_approve) {
            $existingCommunityUser->approve(
                $this->inviter_user_id,
                $this->reason
            );
        }

        $this->accepted_at = Carbon::now();
        $this->save();
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->whereHas(
            "community.communityAdmins",
            fn(Builder $communityAdmin) => $communityAdmin->where(
                "user_id",
                "$user->id"
            )
        );
    }
}
