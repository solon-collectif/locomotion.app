<?php

namespace App\Models;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Casts\PointCast;
use App\Enums\LoanableUserRoles;
use App\Enums\PricingLoanableTypeValues;
use App\Events\UserDeletedEvent;
use App\Events\UserEmailUpdatedEvent;
use App\Events\UserNameUpdatedEvent;
use App\Facades\StorageTransaction;
use App\Mail\PasswordRequest;
use App\Mail\UserMail;
use App\Models\Pivots\CommunityUser;
use App\Services\GeocoderService;
use App\Transformers\UserTransformer;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Database\Seeders\CommunitiesTableSeeder;
use Geocoder\Location;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;
use Stripe;

/**
 * Soft-deleted (deleted_at not null) Users are considered archived. They will not be returned by
 * default in queries, but they can be restored.
 *
 */
class User extends AuthenticatableBaseModel
{
    use HasApiTokens, Notifiable;
    use HasFactory;
    use SoftDeletes;
    use HasSqlCasts;

    public static $rules = [
        "accept_conditions" => ["nullable", "accepted"],
        "address" => ["nullable"],
        "date_of_birth" => ["nullable", "date", "before:18 years ago"],
        "description" => "nullable",
        "email" => "email",
        "is_smart_phone" => "nullable|boolean",
        "last_name" => "nullable",
        "name" => ["nullable"],
        "other_phone" => ["nullable"],
        "password" => ["min:8"],
        "phone" => ["nullable"],
        "postal_code" => ["nullable"],
    ];

    public static $filterTypes = [
        "id" => "number",
        "created_at" => "date",
        "full_name" => "text",
        "name" => "text",
        "last_name" => "text",
        "address" => "text",
        "email" => "text",
        "phone" => "phone",
    ];

    public $computed = [
        "admin_link",
        "available_loanable_types",
        "neighbor_count",
        "trusted_for_loanables",
        "has_fleet",
    ];

    public static function getRules($action = "", $auth = null)
    {
        switch ($action) {
            case "submit":
                $rules = array_merge(static::$rules, [
                    "address" => "required",
                    "date_of_birth" => "required",
                    "first_name" => "required",
                    "last_name" => "required",
                    "telephone" => "required",
                ]);
                break;
            case "template":
                $rules = parent::getRules($action, $auth);
                $rules["name"][] = "required";
                $rules["phone"][] = "required";
                $rules["address"][] = "required";
                break;
            default:
                $rules = parent::getRules($action, $auth);
                break;
        }

        if ($auth && $auth->isAdmin()) {
            unset($rules["accept_conditions"]);
            unset($rules["avatar"]);
        }

        return $rules;
    }

    protected static $transformer = UserTransformer::class;

    public static function booted()
    {
        self::saving(function (User $user) {
            // Make sure the date for accepting the conditions is set
            // Todo(#1121) remove the 'accept_conditions' field and move this
            // code when updating an user.
            if (
                $user->isDirty("accept_conditions") &&
                $user->accept_conditions
            ) {
                $user->acceptConditions();
            }

            if (
                $user->isDirty([
                    "bank_transit_number",
                    "bank_institution_number",
                    "bank_account_number",
                ])
            ) {
                $user->bank_info_updated_at = CarbonImmutable::now();
            }
        });

        self::saved(function (User $user) {
            if ($user->id) {
                if ($user->wasChanged(["name", "last_name", "description"])) {
                    LoanablesByTypeAndCommunity::forgetForUser($user);
                }
                if ($user->wasChanged("address")) {
                    $user->updateAddressAndRelocateCommunity();
                }
            }
        });

        self::updated(function ($model) {
            // Detect email change
            if ($model->wasChanged("email")) {
                $previousEmail = $model->getOriginal("email");
                event(
                    new UserEmailUpdatedEvent(
                        $model,
                        $previousEmail,
                        $model->email
                    )
                );
            }

            if ($model->wasChanged(["name", "last_name"])) {
                $previousName = $model->getOriginal("name");
                $previousLastName = $model->getOriginal("last_name");

                event(
                    new UserNameUpdatedEvent(
                        $model,
                        $previousName,
                        $previousLastName
                    )
                );
            }
        });

        self::deleted(function (User $user) {
            // Delete vehicles
            $user->loanablesAsOwner()->delete();

            // Delete ongoing loans
            $user
                ->loansAsBorrower()
                ->inProcess()
                ->each(function (Loan $loan) {
                    $loan->setLoanStatusCanceled();
                    $loan->save();
                });

            // remove connection tokens
            $user->tokens->each(fn($token) => $token->revoke());

            // Remove admin privileges
            $user
                ->communities()
                ->newPivotQuery()
                ->update(["community_user.role" => null]);

            $user
                ->communities()
                ->newPivotQuery()
                ->whereNotNull("approved_at")
                ->whereNull("suspended_at")
                ->update(["suspended_at" => Carbon::now()]);

            $user->role = null;
            $user->save();

            event(new UserDeletedEvent($user));
        });
    }

    protected static array $customColumns = [
        "full_name" => "CONCAT(%table%.name, ' ', %table%.last_name)",
    ];

    public static $sizes = [
        "avatar" => [
            "dimensions" => [
                "width" => 48,
                "height" => 48,
            ],
            "resizing" => "crop",
            "format" => "jpg",
        ],
    ];

    protected $fillable = [
        "accept_conditions",
        "name",
        "last_name",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
        "is_proof_invalid",
        "bank_account_number",
        "bank_institution_number",
        "bank_transit_number",
    ];

    protected $hidden = ["password", "current_bill"];

    protected $casts = [
        "accept_conditions" => "boolean",
        "balance" => "decimal:2",
        "email_verified_at" => "datetime",
        "meta" => "array",
        "conditions_accepted_at" => "datetime",
        "data_deleted_at" => "datetime",
        "bank_info_updated_at" => "immutable_datetime",
    ];

    protected static array $sqlCasts = [
        "address_position" => PointCast::class,
    ];

    protected $with = [];

    public $collections = [
        "invoices",
        "communities",
        "approved_communities",
        "files",
        "loans_as_borrower",
        "merged_loanable_roles",
        "library_roles",
        "payment_methods",
    ];

    public $items = ["borrower", "google_account"];

    public $morphOnes = [
        "avatar" => "imageable",
    ];

    public $morphManys = [
        "residency_proof" => "fileable",
        "identity_proof" => "fileable",
    ];

    /**
     * @return HasMany<MergedLoanableUserRole, $this>
     */
    public function mergedLoanableRoles(): HasMany
    {
        return $this->hasMany(MergedLoanableUserRole::class);
    }

    public function libraryRoles(): HasMany
    {
        return $this->hasMany(LoanableUserRole::class, "user_id")->where(
            "ressource_type",
            "library"
        );
    }

    public function avatar(): MorphOne
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "avatar"
        );
    }

    public function residencyProof(): MorphMany
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "residency_proof"
        );
    }

    public function identityProof(): MorphMany
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "identity_proof"
        );
    }

    public function approvedIn(Community $community): bool
    {
        return $this->approvedCommunities
            ->where("id", $community->id)
            ->isNotEmpty();
    }

    public function googleAccount()
    {
        return $this->hasOne(GoogleAccount::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * @return HasOne<Borrower, $this>
     */
    public function borrower(): HasOne
    {
        return $this->hasOne(Borrower::class);
    }

    public function getAvailableLoanableTypesAttribute()
    {
        $set = [];

        foreach ($this->approvedCommunities as $community) {
            foreach ($community->allowedLoanableTypes as $loanableType) {
                $set[$loanableType->name->value] = true;
            }
        }

        return array_keys($set);
    }

    public function getTrustedForLoanablesAttribute()
    {
        return $this->mergedLoanableRoles
            ->whereIn("role", [
                LoanableUserRoles::TrustedBorrower,
                LoanableUserRoles::Manager,
                LoanableUserRoles::Coowner,
                LoanableUserRoles::Owner,
            ])
            ->pluck("loanable_id");
    }

    /**
     * One of the user's communities.
     */
    public function getMainCommunityAttribute()
    {
        return $this->communities->first();
    }

    public function getGeoCommunityAttribute()
    {
        return $this->communities
            ->where(fn($c) => $c->pivot->join_method == "geo")
            ->first();
    }

    private function assignCommunityForDevelopment(string $address): bool
    {
        if (app()->environment() !== "local" || config("geocoding.api_key")) {
            return false;
        }

        // Locally, without Geolocalisation, use seed RPP community for magic address
        if (str_contains($address, "6450")) {
            $this->attachGeoCommunity(
                Community::find(CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID)
            );
        }

        return true;
    }

    /**
     * Geocodes user's current address and updates postal_code, address and address_position.
     *
     * @return Location|null GoogleAddress if geocoding successful, null otherwise.
     */
    public function geocodeAndUpdateAddress(): ?Location
    {
        if (empty($this->address)) {
            $this->postal_code = "";
            $this->address_position = null;
            return null;
        }

        $address = GeocoderService::geocode($this->address);

        if (!$address || !$address->getPostalCode()) {
            \Log::warning(
                "[User] Could not geocode position for address $this->address for user $this->id"
            );
            return null;
        }

        $this->postal_code = $address->getPostalCode();
        $this->address = GeocoderService::formatAddressToText($address);
        $this->address_position = $address->getCoordinates();

        return $address;
    }

    /**
     * Update User Address And Relocate Community
     *
     * @return void
     *
     * SCENARIOS COVERED:
     *  1) Moved within the same community
     *  2) Moved from covered to non-covered
     *  3) Moved from non-covered to non-covered
     *  4) Moved from covered to covered
     *  5) Moved from non-covered to covered
     *
     */
    public function updateAddressAndRelocateCommunity(): void
    {
        if ($this->assignCommunityForDevelopment($this->address)) {
            return;
        }

        // Geocode the text address into an Address object
        $address = $this->geocodeAndUpdateAddress();
        $this->saveQuietly();

        // If the address has been located by the geocoder
        if ($address) {
            $coordinates = $address->getCoordinates();

            // Find if the new address is within a community
            $latitude = $coordinates->getLatitude();
            $longitude = $coordinates->getLongitude();
            $community = GeocoderService::findCommunityFromCoordinates(
                $latitude,
                $longitude
            );

            // If so, attach or switch community
            if ($community) {
                $this->attachGeoCommunity($community);
            } else {
                \Log::warning(
                    "Could not find a community for coordinates $latitude $longitude for user $this->id with address '$this->address'"
                );
                // User has moved from covered to a non-covered area
                $this->detachGeoCommunities();
            }
        } else {
            \Log::error(
                "Could not geocode user $this->id address '$this->address'"
            );
            // Users cannot have an invalid address
            abort(422, "The provided user address was not found.");
        }
    }

    public function attachGeoCommunity(Community $community): void
    {
        if ($this->geo_community) {
            if ($this->geo_community->id === $community->id) {
                // Nothing to do.
                return;
            }
            // User was assigned to some other geo community in the past: it needs to be detached.
            $this->detachGeoCommunities();
        }

        // This will only update the community_user if they were already added manually for
        // instance.
        $this->communities()->sync(
            [
                $community->id => [
                    "join_method" => "geo",
                ],
            ],
            detaching: false
        );
    }

    public function detachGeoCommunities(): void
    {
        // There should only ever be one geo community for a user, but to be more robust, we detach
        // any geo community
        foreach ($this->communities as $community) {
            if ($community->pivot->join_method == "geo") {
                $this->communities()->detach($community);
            }
        }
    }

    public function communities(): BelongsToMany
    {
        return $this->belongsToMany(Community::class)
            ->using(CommunityUser::class)
            ->withTimestamps()
            ->withPivot([
                "id",
                "approved_at",
                "created_at",
                "role",
                "suspended_at",
                "updated_at",
                "proof_evaluation",
                "approval_note",
                "join_method",
                "approver_id",
            ])
            ->distinct();
    }

    /**
     * Larastan needs this return definition for type inference
     * @return BelongsToMany<Community, $this>
     */
    public function approvedCommunities(): BelongsToMany
    {
        return $this->communities()
            ->whereNotNull("approved_at")
            ->whereNull("suspended_at");
    }

    /**
     * Larastan needs this return definition for type inference
     * @return BelongsToMany<Community, $this>
     */
    public function adminCommunities(): BelongsToMany
    {
        return $this->approvedCommunities()->wherePivot("role", "admin");
    }

    public function defaultPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::class)->whereIsDefault(true);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    /**
     * @return HasMany<Loan, $this>
     */
    public function loansAsBorrower(): HasMany
    {
        return $this->hasMany(Loan::class, "borrower_user_id");
    }

    /**
     * @return HasMany<IncidentNotification, $this>
     */
    public function incidentNotifications(): HasMany
    {
        return $this->hasMany(IncidentNotification::class);
    }

    public function loanablesAsOwner(): BelongsToMany
    {
        return $this->belongsToMany(
            Loanable::class,
            "merged_loanable_user_roles",
            "user_id",
            "loanable_id"
        )->wherePivot("role", LoanableUserRoles::Owner);
    }

    /**
     * @return HasMany<PaymentMethod, $this>
     */
    public function paymentMethods(): HasMany
    {
        return $this->hasMany(PaymentMethod::class);
    }

    /**
     * @return HasManyThrough<Subscription, CommunityUser, $this>
     */
    public function subscriptions(): HasManyThrough
    {
        return $this->hasManyThrough(
            Subscription::class,
            CommunityUser::class,
            "user_id",
            "community_user_id",
            "id"
        );
    }

    public function hasActiveSubscriptionFor(
        PricingLoanableTypeValues $loanableType,
        int $communityId,
        CarbonInterface $date = null
    ): bool {
        return $this->subscriptions
            ->where(
                fn(Subscription $sub) => $sub->appliesFor($loanableType) &&
                    $sub->activeOn($date) &&
                    $sub->communityUser->community_id === $communityId
            )
            ->count() > 0;
    }

    public function isAdmin()
    {
        return $this->role === "admin";
    }

    public function getAdminCommunityIdsAttribute(): Collection
    {
        return $this->adminCommunities->map(fn($c) => $c->id)->flip();
    }

    public function isCommunityAdmin()
    {
        return $this->admin_community_ids->count() > 0;
    }

    public function isAdminOfCommunity(int $communityId)
    {
        return $this->admin_community_ids->has($communityId);
    }

    public function isAdminOfCommunityFor(User|int $user)
    {
        if (!is_a($user, User::class)) {
            /** @var ?User $user */
            $user = User::find($user);
            if (!$user) {
                return false;
            }
        }

        $userCommunities = $user->communities
            ->map(fn($c) => $c->id)
            ->values()
            ->toArray();
        return $this->admin_community_ids->hasAny($userCommunities);
    }

    public function getStripeCustomer()
    {
        return Stripe::getUserCustomer($this);
    }

    public function getAccessibleCommunityIds()
    {
        return $this->approvedCommunities->pluck("id");
    }

    public function addToBalance($amount)
    {
        $this->balance = floatval($this->balance) + $amount;

        // This function will be called with negative amounts.
        if (floatval($this->balance) < 0) {
            abort(422, __("state.user.balance_cannot_be_negative"));
        }

        $this->save();
    }

    public function approvedInCommunityWith(int $userId)
    {
        return $this->approvedCommunities()
            ->getQuery()
            ->withApprovedUser($userId)
            ->exists();
    }

    public function scopeApprovedInSharedCommunities(
        Builder $query,
        int $userId
    ) {
        return $query->where(
            fn($user) => $user->whereHas(
                "approvedCommunities",
                fn($community) => $community->withApprovedUser($userId)
            )
        );
    }

    public function scopeAccessibleInSharedCommunity(
        Builder $query,
        int $userId
    ) {
        // User can either access all users because they are community admin or
        // to all approved members because they are also approvet in this
        // community.
        return $query->where(
            fn($user) => $user
                ->whereHas(
                    "communities",
                    fn($community) => $community->withCommunityAdmin($userId)
                )
                ->orWhereHas(
                    "approvedCommunities",
                    fn($community) => $community->withApprovedUser($userId)
                )
        );
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // A user has access to...
        return $query
            // ...himself or herself
            ->whereId($user->id)
            // ...or belonging to a community of which he or she is a member
            ->orWhere(fn($q) => $q->accessibleInSharedCommunity($user->id));
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getCustomColumnSql("full_name");

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    public function scopeIsInCommunityRegion(Builder $query, $communityId)
    {
        return $query
            ->whereRaw(
                <<<SQL
    (SELECT public.ST_Contains(area::geometry, users.address_position::geometry)
     FROM communities
     WHERE communities.type = 'borough'
       and communities.id = $communityId
     )
SQL
            )
            ->whereDoesntHave("communities");
    }

    public function sendPasswordResetNotification($token)
    {
        UserMail::send(new PasswordRequest($this, $token), $this);
    }

    public function getFullNameAttribute()
    {
        return trim($this->name . " " . $this->last_name);
    }

    public function getAdminLinkAttribute()
    {
        return config("app.url") . "/admin/users/" . $this->id;
    }

    public function getNeighborCountAttribute()
    {
        return \DB::table("community_user", "cu")
            ->join(
                "community_user as cu2",
                "cu.community_id",
                "cu2.community_id"
            )
            // Approved users sharing $this user's communities
            ->where("cu.user_id", $this->id)
            ->whereNotNull("cu2.approved_at")
            ->whereNull("cu2.suspended_at")
            ->distinct("cu2.user_id")
            ->count("cu2.user_id");
    }

    public function getHasFleetAttribute()
    {
        return $this->libraryRoles->count() > 0;
    }

    public function acceptConditions()
    {
        $this->conditions_accepted_at = new Carbon();
        $this->accept_conditions = true;
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return match ($for) {
            "edit", "admin" => $query->whereHas(
                "communities.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            "profile" => $query->whereHas(
                "approvedCommunities.approvedUsers",
                fn($q) => $q->where("community_user.user_id", $user->id)
            ),
            default => $query,
        };
    }

    public function scopeOwnsLoanables(Builder $query)
    {
        return $query->whereHas("loanablesAsOwner");
    }

    public function scopeGlobalAdmins(Builder $query)
    {
        return $query->where("role", "admin");
    }

    public function scopeCommunityAdmins(Builder $query)
    {
        return $query->whereHas(
            "approvedCommunities",
            fn(Builder $q) => $q->where("role", "admin")
        );
    }

    public function scopeAdminOfCommunity(Builder $query, int $communityId)
    {
        return $query->whereHas(
            "approvedCommunities",
            fn(Builder $q) => $q
                ->where("role", "admin")
                ->where("communities.id", $communityId)
        );
    }

    public function scopeAssignableToIncident(Builder $query, int $incidentId)
    {
        // We use union rather than _or_ which is very slow for these complex queries
        return $query->distinct()->fromSub(
            User::query()
                ->select(["*"])
                ->whereHas(
                    "loansAsBorrower.incidents",
                    fn($incident) => $incident->where("id", $incidentId)
                )
                ->union(
                    User::query()
                        ->select(["*"])
                        ->whereHas(
                            "mergedLoanableRoles",
                            fn($role) => $role
                                ->whereHas(
                                    "loanable.incidents",
                                    fn($incident) => $incident->where(
                                        "id",
                                        $incidentId
                                    )
                                )
                                ->whereIn("role", [
                                    LoanableUserRoles::Owner,
                                    LoanableUserRoles::Manager,
                                    LoanableUserRoles::Coowner,
                                ])
                        )
                )
                ->union(
                    User::query()
                        ->select(["*"])
                        ->whereHas(
                            "approvedCommunities",
                            fn($community) => $community
                                ->where("community_user.role", "admin")
                                ->whereHas(
                                    "loans.incidents",
                                    fn($incident) => $incident->where(
                                        "id",
                                        $incidentId
                                    )
                                )
                        )
                )
                ->union(
                    User::query()
                        ->select(["*"])
                        ->where("role", "admin")
                ),
            "users"
        );
    }

    /**
     * Users which could be added to the trusted borrowers list for the given loanable. Doesn't
     * include users which are already trusted borrowers.
     */
    public function scopeCanBeAddedToTrustedBorrowers(
        Builder $query,
        $loanableId
    ) {
        return $query
            ->whereHas(
                "approvedCommunities.approvedUsers.loanablesAsOwner",
                fn(Builder $q) => $q->where("loanables.id", $loanableId)
            )
            ->whereDoesntHave(
                "mergedLoanableRoles",
                fn(Builder $q) => $q
                    ->where("role", LoanableUserRoles::TrustedBorrower)
                    ->where("ressource_type", "loanable")
                    ->where("ressource_id", $loanableId)
            )
            ->whereDoesntHave(
                "loanablesAsOwner",
                fn(Builder $q) => $q->where("loanables.id", $loanableId)
            );
    }

    public function deleteData(): void
    {
        $this->name = "";
        $this->last_name = "";
        $this->email = "deleted-email-$this->id";
        $this->description = "";
        $this->date_of_birth = null;
        $this->address = "";
        $this->postal_code = "";
        $this->phone = "";
        $this->other_phone = "";
        $this->role = null;
        $this->meta = [];
        $this->bank_account_number = null;
        $this->bank_institution_number = null;
        $this->bank_transit_number = null;
        $this->address_position = null;
        $this->data_deleted_at = Carbon::now();
        // Do not trigger events, such as relocation
        $this->saveQuietly();

        // We delete each file/image individually to make sure the files are also deleted, since
        // mass deletion doesn't trigger events.
        if ($this->avatar) {
            $this->avatar->delete();
        }
        foreach ($this->identityProof as $proof) {
            $proof->delete();
        }
        foreach ($this->residencyProof as $proof) {
            $proof->delete();
        }

        // Delete custom proofs
        foreach ($this->communities as $community) {
            foreach ($community->pivot->customProof as $proof) {
                $proof->delete();
            }
        }

        foreach (
            $this->loanablesAsOwner()
                ->withTrashed()
                ->get()
            as $loanable
        ) {
            $loanable->deleteData();
        }

        if ($this->borrower) {
            $this->borrower->deleteData();
        }

        $reports = UserPublishableReport::where("user_id", $this->id)
            ->pluck("path")
            ->toArray();

        StorageTransaction::delete($reports);
        UserPublishableReport::where("user_id", $this->id)->delete();

        $this->deleteQuietly();
    }

    public function __sleep()
    {
        if (\App::environment() === "testing") {
            $this->accessToken = null;
        }
        return parent::__sleep();
    }

    public function getMailbox(): string
    {
        if ($this->full_name) {
            return sprintf("%s <%s>", $this->full_name, $this->email);
        }
        return sprintf("<%s>", $this->email);
    }
}
