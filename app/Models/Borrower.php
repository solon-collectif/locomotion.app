<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Borrower extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    public static $rules = [
        "user_id" => ["required"],
        "drivers_license_number" => ["nullable"],
        "has_not_been_sued_last_ten_years" => ["boolean"],
        "approved_at" => ["nullable", "date"],
    ];

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            "submit" => [
                "user_id" => ["required"],
                "drivers_license_number" => ["required"],
                "has_not_been_sued_last_ten_years" => [
                    "boolean",
                    "required",
                    "accepted",
                ],
                "saaq" => ["required"],
                "gaa" => ["required"],
            ],
            default => self::$rules,
        };
    }

    public static $filterTypes = [
        "submitted_at" => "date",
        "approved_at" => "date",
    ];

    protected $fillable = [
        "drivers_license_number",
        "has_not_been_sued_last_ten_years",
        "user_id",
    ];

    protected $casts = [
        "submitted_at" => "datetime",
        "approved_at" => "datetime",
        "suspended_at" => "datetime",
    ];

    public $items = ["user"];

    public $collections = ["saaq", "gaa"];

    public $computed = ["approved", "suspended", "validated"];

    public $morphOnes = [
        "insurance" => "fileable",
    ];

    public $morphManys = [
        "saaq" => "fileable",
        "gaa" => "fileable",
    ];

    /**
     * We include archived users here, so we can get some information from borrowers when fetching
     * loans. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function getApprovedAttribute()
    {
        return !!$this->approved_at;
    }

    public function getSuspendedAttribute()
    {
        return !!$this->suspended_at;
    }

    public function getValidatedAttribute()
    {
        return $this->approved && !$this->suspended;
    }

    public function gaa(): MorphMany
    {
        return $this->morphMany(File::class, "fileable")->where("field", "gaa");
    }

    public function insurance(): MorphOne
    {
        return $this->morphOne(File::class, "fileable")->where(
            "field",
            "insurance"
        );
    }

    public function saaq(): MorphMany
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "saaq"
        );
    }

    public function deleteData(): void
    {
        $this->drivers_license_number = null;
        $this->save();
        foreach ($this->gaa as $gaa) {
            $gaa->delete();
        }
        foreach ($this->saaq as $saaq) {
            $saaq->delete();
        }

        $this->insurance?->delete();
        $this->delete();
    }
}
