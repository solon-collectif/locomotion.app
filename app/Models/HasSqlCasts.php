<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Builder;

/**
 * This will add a global scope adding columns to be cast using SQL functions.
 *
 * Models using this trait should override {@link self::$sqlCasts} to define which
 * columns should be casted and how.
 */
trait HasSqlCasts
{
    private static $sqlCastColumns = [];

    /**
     * Casts matching the laravel model's class $casts property,
     * with 'attribute => cast class' pairs.
     */
    private static array $additionalCasts = [];

    /**
     * Array of pairs: table column to class implementing the {@see CastsInSql} trait.
     */
    private static array $sqlCastsClasses = [];

    /**
     * Get a new query builder that doesn't have any global scopes.
     *
     * @return Builder<static>
     */
    public function newQueryWithoutScopes(): Builder
    {
        return $this->addSqlCastsToBuilder(parent::newQueryWithoutScopes());
    }

    /**
     * @return Builder<static>
     */
    private function addSqlCastsToBuilder(Builder $builder): Builder
    {
        $table = $this->getTable();
        // Adding a select without $table.* results in a single column being returned, which
        // breaks everything. If no columns have been specified, we add all columns.
        if (empty($builder->getQuery()->columns)) {
            $builder->addSelect($table . ".*");
        }

        foreach (self::$sqlCastsClasses as $column => $sqlCastClass) {
            $builder->addSelect(
                \DB::raw(
                    $sqlCastClass::sqlColumnCast($table, $column) . "AS $column"
                )
            );
        }
        return $builder;
    }

    /**
     * Initializes the $additionalCasts and $sqlCastsClasses arrays when model is booted.
     *
     * Laravel calls this whenever it boots a new model with this trait.
     *
     * @return void
     */
    protected static function bootHasSqlCasts(): void
    {
        $sqlCasts = static::$sqlCasts;
        if (empty($sqlCasts)) {
            return;
        }

        foreach ($sqlCasts as $sqlCastKey => $sqlCastClass) {
            if (
                class_exists($sqlCastClass) &&
                method_exists($sqlCastClass, "sqlColumnCast")
            ) {
                self::$sqlCastsClasses[$sqlCastKey] = $sqlCastClass;
            }

            if (
                in_array(
                    CastsAttributes::class,
                    class_implements($sqlCastClass)
                )
            ) {
                self::$additionalCasts[$sqlCastKey] = $sqlCastClass;
            }
        }
    }

    /**
     * Add additional SqlCasts which also cast attributes.
     */
    public function casts(): array
    {
        return self::$additionalCasts;
    }
}
