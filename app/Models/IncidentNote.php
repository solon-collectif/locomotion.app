<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IncidentNote extends BaseModel
{
    public static $rules = [
        "text" => ["required"],
    ];

    protected $fillable = ["text"];
    public $items = ["incident", "author"];

    protected static array $customColumns = [
        "type" => "'incident'",
    ];

    /**
     * @return BelongsTo<Incident, $this>
     */
    public function incident(): BelongsTo
    {
        return $this->belongsTo(Incident::class);
    }
    /**
     * @return BelongsTo<User, $this>
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, "author_id");
    }
}
