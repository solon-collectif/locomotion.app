<?php

namespace App\Enums;

enum Requirement: string
{
    case notApplicable = "not_applicable";
    case optional = "optional";
    case required = "required";
}
