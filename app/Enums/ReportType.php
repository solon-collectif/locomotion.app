<?php

namespace App\Enums;

enum ReportType: string
{
    const ALL = ["yearly-owner-income"];
    case YearlyOwnerIncome = "yearly-owner-income";
}
