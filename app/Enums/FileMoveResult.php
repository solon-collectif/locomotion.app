<?php

namespace App\Enums;

enum FileMoveResult
{
    case unchanged;
    case moved;
    case failed;

    case copied;
}
