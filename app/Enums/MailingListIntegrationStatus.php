<?php

namespace App\Enums;

enum MailingListIntegrationStatus: string
{
    case Active = "active";
    case Suspended = "suspended";
    case Synchronizing = "synchronizing";
}
