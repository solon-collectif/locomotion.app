<?php

namespace App\Enums;

enum CarPricingCategories: string
{
    case Small = "small";
    case Large = "large";
    case SmallElectric = "small_electric";
    case LargeElectric = "large_electric";
}
