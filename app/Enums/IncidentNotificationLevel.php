<?php

namespace App\Enums;

enum IncidentNotificationLevel: string
{
    case All = "all";
    case None = "none";
    case ResolvedOnly = "resolved_only";
}
