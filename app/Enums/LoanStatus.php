<?php

namespace App\Enums;

enum LoanStatus: string
{
    const ALL = [
        "requested",
        "accepted",
        "confirmed",
        "ongoing",
        "ended",
        "validated",
        "completed",
        "canceled",
        "rejected",
    ];

    const IN_PROCESS_STATES = [
        LoanStatus::Requested,
        LoanStatus::Accepted,
        LoanStatus::Confirmed,
        LoanStatus::Ongoing,
        LoanStatus::Ended,
        LoanStatus::Validated,
    ];

    const ACCEPTED_STATES = [
        LoanStatus::Accepted,
        LoanStatus::Confirmed,
        LoanStatus::Ongoing,
        LoanStatus::Ended,
        LoanStatus::Validated,
        LoanStatus::Completed,
    ];

    case New = "new";
    case Requested = "requested"; // Needs approval from (co-)owners
    case Accepted = "accepted"; // Needs pre-payment
    case Confirmed = "confirmed"; // Waiting to start
    case Ongoing = "ongoing"; // Waiting to end
    case Ended = "ended"; // Needs validation
    case Validated = "validated"; // Needs payment, or just some time before ending

    case Completed = "completed";
    case Canceled = "canceled";
    case Rejected = "rejected";

    public function isInProcess(): bool
    {
        return in_array($this, self::IN_PROCESS_STATES);
    }
}
