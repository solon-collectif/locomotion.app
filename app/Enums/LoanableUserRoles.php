<?php

namespace App\Enums;

enum LoanableUserRoles: string
{
    case Owner = "owner";
    case Coowner = "coowner";
    case TrustedBorrower = "trusted_borrower";
    case Manager = "manager";
}
