<?php

namespace App\Enums;

use App\Models\Bike;
use App\Models\Car;
use App\Models\LoanableDetails;
use App\Models\LoanableTypeDetails;
use App\Models\Trailer;

/*
 * Must be kept in sync with the LoanableTypeDetails table.
 */
enum LoanableTypes: string
{
    const possibleTypes = ["bike", "trailer", "car"];

    case Bike = "bike";
    case Trailer = "trailer";
    case Car = "car";

    function getLoanableModel(): LoanableDetails
    {
        return match ($this) {
            self::Bike => new Bike(),
            self::Trailer => new Trailer(),
            self::Car => new Car(),
        };
    }

    function getTypeDetails()
    {
        return LoanableTypeDetails::where("name", $this)->first();
    }
}
