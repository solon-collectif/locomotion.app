<?php

namespace App\Enums;

enum BillItemTypes: string
{
    case balanceProvision = "balance.provision";
    case balanceRefund = "balance.refund";
    case loanPrice = "loan.price";
    case loanExpenses = "loan.expenses";
    case loanInsurance = "loan.insurance";
    case donationLoan = "donation.loan";
    case donationBalance = "donation.balance";
    case feesStripe = "fees.stripe";
    case loanContribution = "loan.contribution";
    case contributionYearly = "contribution.yearly";

    public static function fromPricingType(string $pricingType)
    {
        return match ($pricingType) {
            "insurance" => BillItemTypes::loanInsurance,
            "price" => BillItemTypes::loanPrice,
            "contribution" => BillItemTypes::loanContribution,
            default => null,
        };
    }
}
