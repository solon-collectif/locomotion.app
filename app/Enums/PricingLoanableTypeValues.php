<?php

namespace App\Enums;

use App\Enums\CarPricingCategories;
use App\Models\Bike;
use App\Models\Car;
use App\Models\Loanable;
use App\Models\Trailer;

/*
 * Types used for pricings.
 */

enum PricingLoanableTypeValues: string
{
    const possibleTypes = [
        "car_small",
        "car_large",
        "car_small_electric",
        "car_large_electric",
        "trailer",
        "bike_regular",
        "bike_electric",
        "bike_cargo_regular",
        "bike_cargo_electric",
    ];

    case CarSmall = "car_small";
    case CarLarge = "car_large";
    case CarSmallElectric = "car_small_electric";
    case CarLargeElectric = "car_large_electric";
    case Trailer = "trailer";
    case BikeRegular = "bike_regular";
    case BikeElectric = "bike_electric";
    case BikeCargoRegular = "bike_cargo_regular";
    case BikeCargoElectric = "bike_cargo_electric";

    static function forLoanable(Loanable $loanable): PricingLoanableTypeValues
    {
        return match (get_class($loanable->details)) {
            Bike::class => match ($loanable->details->bike_type) {
                "electric" => PricingLoanableTypeValues::BikeElectric,
                "cargo" => PricingLoanableTypeValues::BikeCargoRegular,
                "cargo_electric"
                    => PricingLoanableTypeValues::BikeCargoElectric,
                default => PricingLoanableTypeValues::BikeRegular,
            },
            Trailer::class => PricingLoanableTypeValues::Trailer,
            Car::class => match ($loanable->details->pricing_category) {
                CarPricingCategories::SmallElectric
                    => PricingLoanableTypeValues::CarSmallElectric,
                CarPricingCategories::LargeElectric
                    => PricingLoanableTypeValues::CarLargeElectric,
                CarPricingCategories::Large
                    => PricingLoanableTypeValues::CarLarge,
                default => PricingLoanableTypeValues::CarSmall,
            },
        };
    }

    function translate(): string
    {
        return __("state.pricing.loanable_type.{$this->value}");
    }

    /**
     * @param (PricingLoanableTypeValues|string)[] $loanableTypes
     * @return string
     */
    static function translateList(array $loanableTypes): string
    {
        return implode(
            ", ",
            array_map(
                fn(PricingLoanableTypeValues|string $value) => is_string($value)
                    ? __("state.pricing.loanable_type.$value")
                    : $value->translate(),
                $loanableTypes
            )
        );
    }
}
