<?php

namespace App\Enums;

enum SharingModes: string
{
    case OnDemand = "on_demand";
    case SelfService = "self_service";
    case Hybrid = "hybrid";
}
