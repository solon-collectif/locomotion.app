<?php

namespace App\Enums;

enum MailingListIntegrationProviders: string
{
    case Brevo = "brevo";
    case Mailchimp = "mailchimp";
}
