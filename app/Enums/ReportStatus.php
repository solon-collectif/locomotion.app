<?php

namespace App\Enums;

enum ReportStatus: string
{
    const ALL = ["generating", "generated", "published", "failed"];
    case Generating = "generating";
    case Generated = "generated";
    case Published = "published";
    case Failed = "failed";
}
