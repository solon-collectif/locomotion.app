<?php

namespace App\Enums;

enum LoanNotificationLevel: string
{
    case All = "all";
    case None = "none";
    case MessagesOnly = "messages_only";
}
