<?php

namespace App\Jobs;

use App\Helpers\Path;
use App\Models\PublishableReport;
use App\Models\User;
use App\Reports\YearlyIncomeReport;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateOwnerIncomeReport implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        Batchable;

    public function __construct(
        public $userId,
        public $year,
        public PublishableReport $report
    ) {
    }

    public function handle(): void
    {
        $pdf = new YearlyIncomeReport(
            User::withTrashed()->findOrFail($this->userId),
            $this->year
        );
        $pdfData = $pdf->generate();
        $path = Path::build(
            "files",
            "users",
            $this->userId,
            "reports",
            "income",
            $this->year . ".pdf"
        );
        \StorageTransaction::put($path, $pdfData);
        $this->report->users()->syncWithPivotValues(
            [$this->userId],
            [
                "path" => $path,
            ],
            detaching: false
        );
    }
}
