<?php

namespace App\Jobs;

use App\Exports\BaseExport;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AppendExportToFile implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        Batchable;

    private string $filename;

    public function __construct(
        private readonly BaseExport $export,
        private readonly int $jobNumber = 0,
        private readonly int $totalJobCount = 1
    ) {
        $this->filename = $this->export->exportData->file->original_filename;
    }

    public function getJobNumber(): int
    {
        return $this->jobNumber;
    }

    private function getLocalFilePath(): string
    {
        return storage_path("framework/exports/$this->filename");
    }

    public function handle(): void
    {
        $this->export->exportData->refresh();

        if ($this->export->exportData->isCanceled()) {
            // We need to fail the job, to avoid subsequent jobs from running
            $this->fail("Export canceled");
            return;
        }

        $export = $this->export;
        $localFilePath = $this->getLocalFilePath();
        if ($this->jobNumber !== 0) {
            $localFileSaved = \File::put(
                $localFilePath,
                \Storage::get($this->export->exportData->file->full_path)
            );

            if (!$localFileSaved) {
                throw new \Exception(
                    "Could not copy in progress export from server"
                );
            }

            $file = fopen($localFilePath, "a");
        } else {
            $file = fopen($localFilePath, "w");
            fputcsv($file, $this->export->headings());
        }

        $chunkCount = $this->export->jobChunkCount();
        $page = $this->jobNumber * $chunkCount + 1;
        $count = $this->export->chunkSize();
        $query = $this->export->query();

        for ($i = 0; $i < $chunkCount; $i++) {
            $lines = $query->forPage($page + $i, $count)->get();
            foreach ($lines as $line) {
                fputcsv($file, $export->map($line));
            }
            unset($lines);
        }

        fclose($file);

        $saved = $export->exportData->file
            ->refresh()
            ->replaceFileData(new File($localFilePath));

        if (!$saved) {
            throw new \Exception(
                "Could not save export $localFilePath to storage."
            );
        }

        \File::delete($localFilePath);

        $this->export->exportData->progress =
            (($this->jobNumber + 1) * 100) / $this->totalJobCount;

        if ($this->jobNumber + 1 === $this->totalJobCount) {
            $this->export->exportData->complete();
        }

        $this->export->exportData->save();
    }

    public function failed()
    {
        $localFilePath = $this->getLocalFilePath();
        if (\File::exists($localFilePath)) {
            \File::delete($localFilePath);
        }

        $this->export->exportData->refresh();
        if (!$this->export->exportData->isCanceled()) {
            $this->export->exportData->status = "failed";
            $this->export->exportData->save();
        }
    }
}
