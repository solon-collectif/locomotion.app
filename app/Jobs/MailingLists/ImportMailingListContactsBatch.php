<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Services\MailingListService;
use Illuminate\Bus\Batchable;

class ImportMailingListContactsBatch extends MailingListJob
{
    use Batchable;
    public function __construct(
        MailingListIntegration $mailingList,
        public int $batchSize,
        public int $offset,
        public bool $notify = false
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $users = $this->mailingList->community
            ->approvedUsers()
            ->toBase()
            ->select([
                "users.id",
                "users.name",
                "users.last_name",
                "users.email",
                "community_user.join_method",
            ])
            ->orderBy("users.id")
            ->skip($this->offset)
            ->take($this->batchSize)
            ->get()
            ->toArray();

        $usersAsArrays = [];
        foreach ($users as $user) {
            $userArray = (array) $user;
            $userArray["community"] = $this->mailingList->community->name;
            $usersAsArrays[] = $userArray;
        }

        $this->mailingList->sendRequest(
            fn(MailingListService $service) => $service->importContactBatch(
                $usersAsArrays
            )
        );
    }
}
