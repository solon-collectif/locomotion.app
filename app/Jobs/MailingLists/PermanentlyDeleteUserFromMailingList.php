<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Services\MailingListService;

class PermanentlyDeleteUserFromMailingList extends MailingListJob
{
    public function __construct(
        MailingListIntegration $mailingList,
        public string $userEmail
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $this->mailingList->sendRequest(
            fn(
                MailingListService $service
            ) => $service->permanentlyDeleteContact($this->userEmail)
        );
    }
}
