<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Models\Pivots\CommunityUser;
use App\Services\MailingListService;

class AddOrUpdateUserInMailingList extends MailingListJob
{
    public function __construct(
        MailingListIntegration $mailingList,
        public CommunityUser $communityUser
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $this->mailingList->sendRequest(
            fn(MailingListService $service) => $service->addOrUpdateContact(
                $this->communityUser
            )
        );
    }
}
