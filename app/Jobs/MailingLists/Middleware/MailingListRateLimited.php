<?php

namespace App\Jobs\MailingLists\Middleware;

use App\Jobs\MailingLists\MailingListJob;
use Closure;

class MailingListRateLimited
{
    public function handle(MailingListJob $job, Closure $next): void
    {
        $job->mailingList
            ->getService()
            ->throttle(fn() => $next($job), fn() => $job->release(1));
    }
}
