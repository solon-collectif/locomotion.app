<?php

namespace App\Jobs\MailingLists\Middleware;

use App\Enums\MailingListIntegrationStatus;
use App\Jobs\MailingLists\MailingListJob;
use Closure;

class SkipIfMailingListSuspended
{
    /**
     * Process the queued job.
     *
     * @param Closure(object): void $next
     */
    public function handle(MailingListJob $job, Closure $next): void
    {
        if (
            $job->mailingList->status ===
            MailingListIntegrationStatus::Suspended
        ) {
            $job->delete();
            return;
        }
        $next($job);
    }
}
