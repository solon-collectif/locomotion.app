<?php

namespace App\Jobs\MailingLists;

use App\Services\MailingListService;
use Ds\Set;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class RemoveAllExtraMailingListContacts extends MailingListJob
{
    // Throttling happens the handle method, since we may send multiple requests and each must
    // be throttled.
    public bool $withThrottlingMiddleware = false;
    public function handle(): void
    {
        try {
            $emails = $this->mailingList->sendRequest(
                fn(
                    MailingListService $service
                ) => $service->getAllContactsEmailsThrottled()
            );
        } catch (TooManyRequestsHttpException $e) {
            // This counts towards retries because of failures, but not retries because of
            // exceptions, just like the MailingListRateLimited middleware.
            $this->job->release(1);
            return;
        }

        if (count($emails) === 0) {
            // No contacts to remove
            return;
        }

        $contactEmailsSet = new Set($emails);
        $userEmails = new Set(
            $this->mailingList->community
                ->approvedUsers()
                ->pluck("email")
                ->toArray()
        );

        $contactsToRemoveChunks = array_chunk(
            $contactEmailsSet->diff($userEmails)->toArray(),
            $this->mailingList->getRemoveBatchSize()
        );

        $jobs = [];
        foreach ($contactsToRemoveChunks as $contactsToRemoveChunk) {
            $jobs[] = new RemoveMailingListContactsBatch(
                $this->mailingList,
                $contactsToRemoveChunk
            );
        }

        // TODO(#1398): after migrating to laravel 10+, use prependToChain(Bus::batch($jobs))
        foreach ($jobs as $job) {
            $this->prependToChain($job);
        }
    }
}
