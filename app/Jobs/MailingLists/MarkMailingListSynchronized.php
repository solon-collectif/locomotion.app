<?php

namespace App\Jobs\MailingLists;

class MarkMailingListSynchronized extends MailingListJob
{
    public bool $withThrottlingMiddleware = false;
    public function handle(): void
    {
        $this->mailingList->markSynchronized();
        $this->mailingList->save();
    }
}
