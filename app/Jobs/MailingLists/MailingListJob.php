<?php

namespace App\Jobs\MailingLists;

use App\Jobs\MailingLists\Middleware\MailingListRateLimited;
use App\Jobs\MailingLists\Middleware\SkipIfMailingListSuspended;
use App\Models\MailingListIntegration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class MailingListJob implements ShouldQueue
{
    use Queueable, InteractsWithQueue, Dispatchable, SerializesModels;

    public bool $withThrottlingMiddleware = true;

    /**
     * Number of times to retry if request is throttled
     */
    public int $tries = 10;

    /**
     * The maximum number of unhandled exceptions to allow before failing.
     */
    public int $maxExceptions = 3;

    public function __construct(public MailingListIntegration $mailingList)
    {
    }

    public function middleware(): array
    {
        $middlewares = [new SkipIfMailingListSuspended()];

        if ($this->withThrottlingMiddleware) {
            $middlewares[] = new MailingListRateLimited();
        }

        return $middlewares;
    }
}
