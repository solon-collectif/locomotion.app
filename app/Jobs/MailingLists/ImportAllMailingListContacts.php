<?php

namespace App\Jobs\MailingLists;

class ImportAllMailingListContacts extends MailingListJob
{
    // Does not send requests, only creates jobs
    public bool $withThrottlingMiddleware = false;

    public function handle(): void
    {
        $approvedUserCount = $this->mailingList->community
            ->approvedUsers()
            ->count();
        $batchSize = $this->mailingList->getImportBatchSize();
        $jobs = [];

        for ($offset = 0; $offset < $approvedUserCount; $offset += $batchSize) {
            $jobs[] = new ImportMailingListContactsBatch(
                $this->mailingList,
                $batchSize,
                $offset
            );
        }

        // TODO(#1398): after migrating to laravel 10+, use prependToChain(Bus::batch($jobs))
        foreach ($jobs as $job) {
            $this->prependToChain($job);
        }
    }
}
