<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Services\MailingListService;
use Illuminate\Bus\Batchable;

class RemoveMailingListContactsBatch extends MailingListJob
{
    use Batchable;

    public function __construct(
        MailingListIntegration $mailingList,
        public array $contactEmails
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $this->mailingList->sendRequest(
            fn(MailingListService $service) => $service->removeContactBatch(
                $this->contactEmails
            )
        );
    }
}
