<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Models\User;
use App\Services\MailingListService;

class RemoveUserFromMailingList extends MailingListJob
{
    public function __construct(
        MailingListIntegration $mailingList,
        public User $user
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $this->mailingList->sendRequest(
            fn(MailingListService $service) => $service->removeContact(
                $this->user
            )
        );
    }
}
