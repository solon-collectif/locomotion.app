<?php

namespace App\Jobs\MailingLists;

use App\Models\MailingListIntegration;
use App\Models\Pivots\CommunityUser;
use App\Services\MailingListService;

class UpdateUserEmailInMailingList extends MailingListJob
{
    public function __construct(
        MailingListIntegration $mailingList,
        public CommunityUser $communityUser,
        public string $previousEmail
    ) {
        parent::__construct($mailingList);
    }

    public function handle(): void
    {
        $this->mailingList->sendRequest(
            fn(MailingListService $service) => $service->updateEmail(
                $this->communityUser,
                $this->previousEmail
            )
        );
    }
}
