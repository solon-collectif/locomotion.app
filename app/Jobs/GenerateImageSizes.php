<?php

namespace App\Jobs;

use App\Models\Image;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Generates missing sizes for images (or regenerates existings sizes, if desired).
 *
 * Normally, image sizes are generated upon image upload, but if a new size is defined,
 * all existing images need to be updated. This is what this job is for.
 */
class GenerateImageSizes implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        Batchable;

    public function __construct(
        private readonly int $from,
        private readonly int $count,
        private readonly array $regenerateSizes = []
    ) {
    }

    public function handle(): void
    {
        $sizesCreated = 0;
        $sizesDeleted = 0;
        $unchangedImages = 0;

        \Log::info(
            "[GenerateImageSizes] Start generating sizes for $this->count images from $this->from."
        );

        if (!empty($this->regenerateSizes)) {
            $sizesString = implode(",", $this->regenerateSizes);
            \Log::info("[GenerateImageSizes] Regenerating: $sizesString");
        }

        foreach (
            Image::orderBy("id")
                ->skip($this->from)
                ->take($this->count)
                ->cursor()
            as $image
        ) {
            $result = $image->syncThumbnailFiles($this->regenerateSizes);
            $created = $result["created"] ?? 0;
            $deleted = $result["deleted"] ?? 0;

            if (!empty($created)) {
                $sizesCreated += count($created);
            }
            if (!empty($deleted)) {
                $sizesDeleted += count($deleted);
            }
            if (empty($deleted) && empty($created)) {
                $unchangedImages++;
            }
        }

        \Log::info(
            "[GenerateImageSizes] Done generating sizes: $sizesCreated created, $sizesDeleted deleted, $unchangedImages unchanged."
        );
    }
}
