<?php

namespace App\Services;

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\CardException;
use Stripe\Stripe;

class StripeService
{
    // Keep in sync with resources/app/src/helpers/transactionFees.js
    // Constant is in cents.
    public static $feeSpec = [
        "amex" => [
            "ratio" => 0.035,
            "constant" => 0,
        ],
        "foreign" => [
            "ratio" => 0.032,
            "constant" => 30,
        ],
        "default" => [
            "ratio" => 0.022,
            "constant" => 30,
        ],
    ];

    /**
     * Passing fees on to customer:
     *     https://support.stripe.com/questions/passing-the-stripe-fee-on-to-customers
     *
     * @param int $amount
     *     Amount in cents onto which fees are to be applied.
     *
     * @param PaymentMethod $paymentMethod
     */
    public static function computeAmountWithFee(
        int $amount,
        PaymentMethod $paymentMethod
    ) {
        $feeType = "default";

        if ($paymentMethod->credit_card_type === "American Express") {
            $feeType = "amex";
        } elseif ($paymentMethod->country !== "CA") {
            $feeType = "foreign";
        }

        $feeConstant = static::$feeSpec[$feeType]["constant"];
        $feeRatio = static::$feeSpec[$feeType]["ratio"];

        return (int) round(($amount + $feeConstant) / (1 - $feeRatio));
    }

    public function __construct(private string $apiKey)
    {
        Stripe::setApiKey($this->apiKey);
    }

    public function getSource(PaymentMethod $method)
    {
        $customer = $this->getUserCustomer($method->user);
        try {
            return Customer::retrieveSource(
                $customer->id,
                $method->external_id
            );
        } catch (ApiErrorException $e) {
            Log::error($e);
            return null;
        }
    }

    public function getUserCustomer(User $user)
    {
        $customers = Customer::all([
            "email" => $user->email,
            "limit" => 1,
        ]);

        $customer = array_pop($customers->data);

        if ($customer) {
            return $customer;
        }

        return Customer::create([
            "description" => "{$user->full_name} <{$user->email}> ({$user->id})",
            "email" => $user->email,
            "name" => $user->full_name,
            "address" => [
                "line1" => $user->address,
                "country" => "CA",
                "postal_code" => $user->postal_code,
            ],
        ]);
    }

    public function createCardBySourceId($customerId, $sourceId)
    {
        try {
            return Customer::createSource($customerId, [
                "source" => $sourceId,
            ]);
        } catch (CardException $e) {
            throw ValidationException::withMessages([
                "stripe" => $e->getMessage(),
            ]);
        }
    }

    public function deleteSource($customerId, $sourceId)
    {
        try {
            Customer::deleteSource($customerId, $sourceId);
        } catch (\Exception) {
            // Doesn't really matter
        }
    }

    /**
     * @param $amountWithFeeInCents
     * @param $customerId
     * @param $description
     * @param $paymentMethodId
     * @return Charge
     * @throws ApiErrorException
     */
    public function createCharge(
        $amountWithFeeInCents,
        $customerId,
        $description,
        $paymentMethodId
    ) {
        return Charge::create([
            "amount" => $amountWithFeeInCents,
            "currency" => "cad",
            "source" => $paymentMethodId,
            "customer" => $customerId,
            "description" => $description,
        ]);
    }
}
