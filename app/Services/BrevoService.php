<?php

namespace App\Services;

use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Brevo\Client\Api\ContactsApi;
use Brevo\Client\Configuration;
use Brevo\Client\Model\CreateContact;
use Brevo\Client\Model\RemoveContactFromList;
use Brevo\Client\Model\RequestContactImport;
use Brevo\Client\Model\RequestContactImportJsonBody;
use Brevo\Client\Model\UpdateContact;
use GuzzleHttp\Client;

class BrevoService extends MailingListService
{
    private static int $MAX_REQUESTS_PER_SECOND = 10;
    public function __construct(
        private readonly string $apiKey,
        private readonly ?int $listId
    ) {
    }

    private function getClient()
    {
        $config = Configuration::getDefaultConfiguration();
        $config->setApiKey("api-key", $this->apiKey);
        return new ContactsApi(new Client(), $config);
    }

    public function throttle(\Closure $ifAvailable, \Closure $ifNotAvailable)
    {
        // Max 10 requests per second. This checks across all processes, using a cache to keep
        // track of the number of ongoing requests.
        $timestamp = time();
        $currentSecondRequestCount = \Cache::remember(
            "brevo-request-count:$timestamp",
            1,
            fn() => 0
        );
        if ($currentSecondRequestCount >= self::$MAX_REQUESTS_PER_SECOND) {
            return $ifNotAvailable();
        }

        \Cache::increment("brevo-request-count:$timestamp");
        return $ifAvailable();
    }

    public function getList(): array
    {
        try {
            $reponse = $this->getClient()->getList((int) $this->listId);

            return [
                "list" => [
                    "name" => $reponse->getName(),
                    "id" => $reponse->getId(),
                ],
            ];
        } catch (\Exception $e) {
            throw new MailingListException($e, "fetching list");
        }
    }

    public function getImportBatchSize(): int
    {
        return 10000;
    }

    public function getRemoveBatchSize(): int
    {
        return 150;
    }

    public function importContactBatch(array $users): void
    {
        try {
            $client = $this->getClient();
            $importRequest = new RequestContactImport();
            $importRequest->setListIds([$this->listId]);

            $importRequest->setJsonBody(
                array_map(
                    fn($user) => new RequestContactImportJsonBody([
                        "email" => $user["email"],
                        "attributes" => [
                            "FIRSTNAME" => $user["name"],
                            "LASTNAME" => $user["last_name"],
                            "JOIN_METHOD" => $user["join_method"],
                            "COMMUNITY" => $user["community"],
                        ],
                    ]),
                    $users
                )
            );

            $client->importContacts($importRequest);
        } catch (\Exception $e) {
            throw new MailingListException($e, "importing contacts");
        }
    }

    public function fetchLists($page = 0, $perPage = 10): array
    {
        try {
            $response = $this->getClient()->getLists(
                $perPage,
                $page * $perPage
            );
            return [
                "lists" => array_map(
                    fn($list) => [
                        "id" => $list["id"],
                        "name" => $list["name"],
                        "contact_count" => $list["uniqueSubscribers"],
                    ],
                    $response->getLists()
                ),
                "count" => $response->getCount(),
            ];
        } catch (\Exception $e) {
            throw new MailingListException($e, "fetching lists");
        }
    }

    public function addOrUpdateContact(CommunityUser $communityUser): void
    {
        try {
            $this->getClient()->createContact(
                new CreateContact([
                    "email" => $communityUser->user->email,
                    "attributes" => [
                        "FIRSTNAME" => $communityUser->user->name,
                        "LASTNAME" => $communityUser->user->last_name,
                        "COMMUNITY" => $communityUser->community->name,
                        "JOIN_METHOD" => $communityUser->join_method,
                    ],
                    "listIds" => [$this->listId],
                    "updateEnabled" => true,
                ])
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "adding or editing contact");
        }
    }

    public function updateEmail(
        CommunityUser $communityUser,
        string $previousEmail
    ): void {
        try {
            try {
                $contact = $this->getClient()->getContactInfo(
                    $previousEmail,
                    "email_id"
                );
            } catch (\Exception $e) {
                if ($e->getCode() === 404) {
                    $this->addOrUpdateContact($communityUser);
                    return;
                }
                throw $e;
            }
            $this->getClient()->updateContact(
                $contact->getId(),
                new UpdateContact([
                    "attributes" => [
                        "EMAIL" => $communityUser->user->email,
                    ],
                ]),
                "contact_id"
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "update contact email");
        }
    }

    public function removeContact(User $user): void
    {
        try {
            $this->getClient()->removeContactFromList(
                $this->listId,
                new RemoveContactFromList([
                    "emails" => [$user->email],
                ])
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "adding or editing contact");
        }
    }

    public function getAllContactsEmailsThrottled(): array
    {
        $contactEmails = [];

        try {
            $client = $this->getClient();
            $hasMore = true;
            $offset = 0;
            while ($hasMore) {
                $contacts = $this->retryThrottled(
                    fn() => $client->getContactsFromList(
                        $this->listId,
                        limit: 500,
                        offset: $offset
                    )
                );
                $offset += 500;
                $hasMore = $offset < $contacts->getCount();
                foreach ($contacts->getContacts() as $contact) {
                    $contactEmails[] = $contact["email"];
                }
            }
            return $contactEmails;
        } catch (\Exception $e) {
            throw new MailingListException($e, "Getting all contacts");
        }
    }

    public function removeContactBatch(array $emails): void
    {
        try {
            $this->getClient()->removeContactFromList(
                $this->listId,
                new RemoveContactFromList([
                    "emails" => $emails,
                ])
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "Removing contacts");
        }
    }

    public function permanentlyDeleteContact(string $userEmail): void
    {
        try {
            $this->getClient()->deleteContact($userEmail, "email_id");
        } catch (\Exception $e) {
            throw new MailingListException($e, "delete contact");
        }
    }
}
