<?php

namespace App\Services;

use App\Models\Community;
use DB;
use Geocoder\Location;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Geocoder\Query\GeocodeQuery;
use Geocoder\StatefulGeocoder;
use Http\Adapter\Guzzle7\Client;

/** LocoMotion Geocoder
 *
 * This custom-made geocoder is built on top of 'geocoder-php'.
 *
 * Useful documentation:
 * - https://geocoder-php.org/docs/
 * - https://github.com/geocoder-php/Geocoder/blob/main/src/Common/Model/AddressCollection.php
 * - https://github.com/geocoder-php/Geocoder/blob/main/src/Common/Model/Address.php (Address)
 */
class LocoMotionGeocoderService
{
    public function __construct()
    {
        // Nothing yet
    }

    /**
     * RETURN A GEOCODER-PHP ADDRESS OBJECT FROM A TEXTUAL ADDRESS
     * @return Location|null null if not found
     */
    public function geocode(string $full_text_address): ?Location
    {
        $apiKey = config("geocoding.api_key");

        // If no key, then don't geocode.
        if (!$apiKey) {
            return null;
        }

        $httpClient = new Client();
        $provider = new GoogleMaps($httpClient, null, $apiKey);
        $geocoder = new StatefulGeocoder($provider);
        $geocoder->setLimit(1);

        $result = $geocoder->geocodeQuery(
            GeocodeQuery::create($full_text_address)->withLocale("fr-CA")
        );

        if ($result->isEmpty()) {
            return null;
        }

        return $result->first();
    }

    /**
     * Convert address object to a full textual address
     * *
     * @input Location
     * @return String
     * Ex: '9155 Rue St-Hubert, Montréal, H2M 1Y8, Canada'
     */
    public function formatAddressToText(Location $address): string
    {
        if (
            is_a($address, GoogleAddress::class) &&
            ($full_address = $address->getFormattedAddress())
        ) {
            return $full_address;
        }

        $full_address =
            $address->getStreetNumber() .
            " " .
            $address->getStreetName() .
            ", " .
            $address->getLocality() .
            ", " .
            $address->getPostalCode() .
            ", " .
            $address->getCountry();
        return $full_address;
    }

    /**
     * Find Community From Coordinates
     * *
     * @input (float:latitude, float:longitude)
     * @return Community
     */
    public function findCommunityFromCoordinates(
        float $latitude,
        float $longitude
    ): ?Community {
        $rawQuery =
            "* from(SELECT public.ST_Contains(area::geometry,ST_SetSRID('POINT(" .
            $longitude .
            " " .
            $latitude .
            ")'::geometry,4326)) in_area, communities.id, communities.name from communities WHERE communities.type = 'borough') table_results where in_area is TRUE LIMIT 1";

        $results = DB::query()
            ->selectRaw($rawQuery)
            ->get();
        if (count($results) > 0) {
            return Community::find($results[0]->id);
        } else {
            return null;
        }
    }
}
