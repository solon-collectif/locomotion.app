<?php

namespace App\Services;

class MailingListException extends \Exception
{
    public function __construct(\Exception $previous, public string $action)
    {
        parent::__construct(
            $previous->getMessage(),
            $previous->getCode(),
            $previous
        );
    }
}
