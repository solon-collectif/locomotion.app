<?php

namespace App\Services;

use App\Helpers\Path;
use App\Models\Payout;
use Carbon\CarbonImmutable;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class KiwiliService
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @throws \Exception if request failed or service not enabled
     */
    public function createExpense(Payout $payout)
    {
        if (!config("services.kiwili.enabled")) {
            throw new \Exception("Kiwili is not enabled");
        }

        $user = $payout->user;
        $locomotionInvoiceUrl = Path::url(
            "admin/invoices/",
            $payout->invoice_id
        );

        $itemDescription = <<<TEXT
Remboursement du solde pour {$user->full_name} (#$payout->user_id)
Facture: $locomotionInvoiceUrl
TEXT;

        if ($payout->bank_info_updated_since_last_payout) {
            // Weird spacing since display font is not monospace
            $itemDescription .=
                "\n\n" .
                <<<TEXT
**Mise à jour des informations bancaires**
# Transit :        $user->bank_transit_number
# Institution :  $user->bank_institution_number
# Compte :      $user->bank_account_number
TEXT;
        }

        try {
            $response = $this->client->post(
                Path::join(config("services.kiwili.base_url"), "expense"),
                [
                    "json" => [
                        "IncurredByUserId" => config(
                            "services.kiwili.api_user_id"
                        ),
                        "ProviderId" => config(
                            "services.kiwili.payout_provider_id"
                        ),
                        "ProjectId" => config("services.kiwili.project_id"),
                        "Description" => "Dépense automatique pour remboursement du solde de {$user->full_name}",
                        "Status" => 0, // Draft
                        "TermsAndConditions" => "", // Remove default value
                        "TaxProfileId" => config(
                            "services.kiwili.payout_tax_profile_id"
                        ),
                        "ExpenseDate" => (
                            $payout->created_at ?? CarbonImmutable::now()
                        )->toISOString(),
                        "ProviderReference" => "$payout->invoice_id",
                        "Lines" => [
                            [
                                "ItemDescription" => $itemDescription,
                                "ItemType" => "d", // expense account
                                "ItemId" => config(
                                    "services.kiwili.payout_expense_account_id"
                                ),
                                "Quantity" => 1,
                                "UnitCost" => $payout->amount,
                                "ItemTaxProfileId" => config(
                                    "services.kiwili.payout_tax_profile_id"
                                ),
                            ],
                        ],
                    ],
                    "headers" => [
                        "Authorization" =>
                            "Bearer " . config("services.kiwili.auth_token"),
                    ],
                ]
            );

            return json_decode($response->getBody(), true);
        } catch (\Exception | GuzzleException $e) {
            throw new \Exception("Error creating expense: " . $e->getMessage());
        }
    }
}
