<?php

namespace App\Services;

use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

abstract class MailingListService
{
    /**
     * @return array{lists?: array{array{id: mixed, name: string, contact_count: int}}, count?: int}
     * @throws MailingListException
     */
    abstract public function fetchLists($page = 0, $perPage = 10): array;

    /**
     * @return array{list?: array{id: mixed, name: string}}
     * @throws MailingListException
     */
    abstract public function getList(): array;

    /**
     * @return array{} emails of contacts
     * @throws MailingListException
     */
    abstract public function getAllContactsEmailsThrottled(): array;
    abstract public function getImportBatchSize(): int;

    /**
     * @param array{array{id:int,name:string,last_name:string,email:string,join_method:string,community:string}} $users
     * @throws MailingListException
     */
    abstract public function importContactBatch(array $users);
    abstract public function getRemoveBatchSize(): int;

    /**
     * @param array $emails
     * @throws MailingListException
     */
    abstract public function removeContactBatch(array $emails);

    /**
     * @param CommunityUser $communityUser
     * @throws MailingListException
     */
    abstract public function addOrUpdateContact(CommunityUser $communityUser);

    /**
     * @param CommunityUser $communityUser
     * @param string $previousEmail
     * @throws MailingListException
     */
    abstract public function updateEmail(
        CommunityUser $communityUser,
        string $previousEmail
    );

    /**
     * @param User $user
     * @throws MailingListException
     */
    abstract public function removeContact(User $user): void;

    abstract public function permanentlyDeleteContact(string $userEmail): void;

    abstract public function throttle(
        \Closure $ifAvailable,
        \Closure $ifNotAvailable
    );

    public function retryThrottled(
        \Closure $request,
        $currentTry = 0,
        $maxRetries = 10,
        $sleep = 0.25
    ) {
        if ($currentTry > $maxRetries) {
            throw new TooManyRequestsHttpException();
        }

        return $this->throttle($request, function () use (
            $maxRetries,
            $currentTry,
            $request,
            $sleep
        ) {
            usleep((int) ceil($sleep * 1000000));
            return $this->retryThrottled(
                $request,
                $currentTry + 1,
                $maxRetries
            );
        });
    }
}
