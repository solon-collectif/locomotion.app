<?php

namespace App\Services;

use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Log;
use MailchimpMarketing\ApiClient as MailchimpApiClient;

class MailchimpService extends MailingListService
{
    private static int $MAX_CONCURRENT_REQUESTS = 10;
    private MailchimpApiClient|null $apiClient = null;

    private string $endpoint;

    public function __construct(
        private readonly string $apiKey,
        private readonly ?string $listId,
        private readonly ?string $tag
    ) {
        $this->endpoint = substr(
            $this->apiKey,
            strrpos($this->apiKey, "-") + 1
        );
    }

    public function getClient(): MailchimpApiClient
    {
        if (null == $this->apiClient) {
            $this->apiClient = (new MailchimpApiClient())->setConfig([
                "apiKey" => $this->apiKey,
                "server" => $this->endpoint,
            ]);
        }

        return $this->apiClient;
    }

    /**
     * This ensures we never have more than 10 active connections to mailchimp, per documentation
     * here: https://mailchimp.com/developer/marketing/docs/fundamentals/#api-limits
     *
     * The limit is most likely per API key, so this throttle could be improved as it currently
     * applies to all keys.
     */
    public function throttle(\Closure $ifAvailable, \Closure $ifNotAvailable)
    {
        $currentRequestCount = \Cache::remember(
            "mailchimp-request-count",
            120,
            fn() => 0
        );
        if ($currentRequestCount >= self::$MAX_CONCURRENT_REQUESTS) {
            return $ifNotAvailable();
        }
        \Cache::increment("mailchimp-request-count");
        try {
            return $ifAvailable();
        } finally {
            \Cache::decrement("mailchimp-request-count");
        }
    }

    public function fetchLists($page = 0, $perPage = 10): array
    {
        try {
            $data = $this->getClient()->lists->getAllLists(
                fields: "lists.id,lists.name,total_items,lists.stats.member_count",
                count: $perPage,
                offset: $page * $perPage
            );
            return [
                "lists" => array_map(
                    fn($list) => [
                        "id" => $list->id,
                        "name" => $list->name,
                        "contact_count" => $list->stats->member_count,
                    ],
                    $data->lists
                ),
                "count" => $data->total_items ?? 0,
            ];
        } catch (\Exception $e) {
            throw new MailingListException($e, "fetching all lists");
        }
    }

    public function getList(): array
    {
        try {
            $data = $this->getClient()->lists->getList(
                $this->listId,
                fields: "id,name"
            );

            return [
                "list" => [
                    "id" => $data->id,
                    "name" => $data->name,
                ],
            ];
        } catch (\Exception $e) {
            throw new MailingListException($e, "fetching list");
        }
    }

    public function addOrUpdateContact(CommunityUser $communityUser): void
    {
        try {
            $this->getClient()->lists->setListMember(
                $this->listId,
                $communityUser->user->email,
                [
                    "email_address" => $communityUser->user->email,
                    "status_if_new" => "subscribed",
                    "merge_fields" => [
                        "FNAME" => $communityUser->user->name,
                        "LNAME" => $communityUser->user->last_name,
                        "COMMUNITY" => $communityUser->community->name,
                        "JOIN_METHOD" => $communityUser->join_method,
                    ],
                    "tags" => [$this->tag],
                ]
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "adding contact");
        }
    }

    public function updateEmail(
        CommunityUser $communityUser,
        string $previousEmail
    ): void {
        try {
            try {
                $contact = $this->getClient()->lists->getListMember(
                    $this->listId,
                    $previousEmail,
                    [
                        "fields" => "contact_id",
                    ]
                );
            } catch (\Exception $e) {
                if ($e->getCode() == 404) {
                    $this->addOrUpdateContact($communityUser);
                    return;
                }

                throw $e;
            }

            $this->getClient()->lists->updateListMember(
                $this->listId,
                $contact->contact_id,
                [
                    "email_address" => $communityUser->user->email,
                ]
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "updating contact email");
        }
    }

    public function removeContact(User $user): void
    {
        try {
            $this->getClient()->lists->updateListMemberTags(
                $this->listId,
                md5(strtolower($user->email)),
                [
                    "tags" => [
                        [
                            "name" => $this->tag,
                            "status" => "inactive",
                        ],
                    ],
                ]
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "removing contact");
        }
    }

    public function getAllContactsEmailsThrottled(): array
    {
        try {
            $hasMore = true;
            $offset = 0;
            $client = $this->getClient();
            $contactEmails = [];
            while ($hasMore) {
                $contacts = $this->retryThrottled(
                    fn() => $client->lists->getListMembersInfo($this->listId, [
                        "status" => "subscribed",
                        "offset" => $offset,
                        "count" => 1000,
                        "fields" =>
                            "members.id,members.email_address,total_items",
                    ])
                );
                $offset += 1000;
                if ($offset >= $contacts->total_items) {
                    $hasMore = false;
                }
                foreach ($contacts->members as $contact) {
                    $contactEmails[] = $contact->email_address;
                }
            }
            return $contactEmails;
        } catch (\Exception $e) {
            throw new MailingListException($e, "fetching all contacts");
        }
    }

    public function getImportBatchSize(): int
    {
        return 500;
    }

    public function importContactBatch(array $users): array
    {
        $response = $this->getClient()->lists->batchListMembers($this->listId, [
            "members" => array_map(
                fn($user) => [
                    "email_address" => $user["email"],
                    "merge_fields" => [
                        "FNAME" => $user["name"],
                        "LNAME" => $user["last_name"],
                        "COMMUNITY" => $user["community"],
                    ],
                    "status_if_new" => "subscribed", // Do not update status of existing contacts
                    "tags" => [$this->tag],
                ],
                $users
            ),
            "update_existing" => true, // Update merge_fields and tags of existing contacts
        ]);
        if (!empty($response->errors)) {
            // Request succeded, but some contacts might not have been imported
            // (such as permanently deleted contacts). This is expected, so not throwing exception.
            Log::warning(
                "[Mailchimp] Errors while importing contacts",
                $response->errors
            );
        }
        return (array) $response;
    }

    public function getRemoveBatchSize(): int
    {
        return 500;
    }

    public function removeContactBatch(array $emails): void
    {
        try {
            $operations = [];
            foreach ($emails as $email) {
                $emailId = md5(strtolower($email));
                $operations[] = [
                    "method" => "POST",
                    "path" => "/lists/{$this->listId}/members/{$emailId}/tags",
                    "body" => "{\"tags\": [{\"name\": \"$this->tag\", \"status\": \"inactive\"}]}",
                ];
            }
            $this->getClient()->batches->start([
                "operations" => $operations,
            ]);
        } catch (\Exception $e) {
            throw new MailingListException($e, "removing contacts");
        }
    }

    public function permanentlyDeleteContact(string $userEmail): void
    {
        try {
            $this->getClient()->lists->deleteListMemberPermanent(
                $this->listId,
                md5(strtolower($userEmail))
            );
        } catch (\Exception $e) {
            throw new MailingListException($e, "deleting contact");
        }
    }
}
