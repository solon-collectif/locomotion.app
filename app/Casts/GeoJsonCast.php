<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * Converts data from requests or database to a GeoJson object.
 * The GeoJson object handles serialization to response and to the database.
 */
class GeoJsonCast implements CastsAttributes
{
    use CastsInSql;

    public function set($model, $key, $value, $attributes)
    {
        if (!$value) {
            return null;
        }

        if (is_a($value, GeoJson::class)) {
            return $value;
        }

        // We may get an array from the frontend, which should be JSON
        if (is_array($value)) {
            $value = json_encode($value);
        }

        // In case we have a JSON string
        if (is_string($value)) {
            return new GeoJson($value);
        }

        throw new \Exception("invalid");
    }

    public function get($model, $key, $value, $attributes)
    {
        if (!$value) {
            return null;
        }

        if (is_a($value, GeoJson::class)) {
            return $value;
        }

        // Database will return a json string.
        if (is_string($value)) {
            return new GeoJson($value);
        }

        throw new \Exception("invalid");
    }

    public static function sqlColumnCast($table, $column): string
    {
        return "ST_AsGeoJson($table.$column)";
    }
}
