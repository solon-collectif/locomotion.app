<?php

namespace App\Casts;

use App\Models\HasSqlCasts;

/**
 * Trait to be used by casts returned by models implementing {@see HasSqlCasts}.
 */
trait CastsInSql
{
    /**
     * Produce SQL expression that will be used in a Select statement in place of the
     * column name. This should include the invocation of some SQL function to cast the
     * orginial column value.
     */
    abstract public static function sqlColumnCast($table, $column): string;
}
