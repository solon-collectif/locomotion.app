<?php

namespace App\Casts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Query\Expression;
use JsonSerializable;

class Point extends Expression implements Arrayable, JsonSerializable
{
    public function __construct(
        public readonly float $lat,
        public readonly float $long
    ) {
        // Set the expression value which will be used in DB queries
        parent::__construct("ST_GeomFromText('POINT($this->long $this->lat)')");
    }

    public function latLong(): array
    {
        return [$this->lat, $this->long];
    }

    public function toArray(): array
    {
        return $this->latLong();
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}
