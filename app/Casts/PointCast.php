<?php

namespace App\Casts;

use Geocoder\Model\Coordinates;
use GeoJson\GeoJson;
use GeoJson\Geometry\Point as GeoPoint;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * Converts data from requests or database to a Point object.
 * The Point object handles serialization to response and to the database.
 */
class PointCast implements CastsAttributes
{
    use CastsInSql;

    public function set($model, $key, $value, $attributes): ?Point
    {
        if (null === $value) {
            return null;
        }

        if ($value instanceof Point) {
            return $value;
        }

        if (
            is_array($value) &&
            isset($value["latitude"]) &&
            isset($value["longitude"])
        ) {
            $latitude = $value["latitude"];
            $longitude = $value["longitude"];
        } elseif (
            is_array($value) &&
            count(array_filter($value, "is_numeric")) === 2
        ) {
            [$latitude, $longitude] = $value;
        } elseif ($value instanceof Coordinates) {
            $latitude = $value->getLatitude();
            $longitude = $value->getLongitude();
        } elseif (
            is_string($value) &&
            preg_match("/-?\d+(?:\.\d+)?[, ] *-?\d+(?:\.\d+)?/", $value)
        ) {
            /*
            '/\d+/'
                Accept an integer number
            '/\d+\.\d+/'
                Decimal number with mandatory decimals
            '/\d+(?:\.\d+)?/'
                Optional decimals
            '/-?\d+(?:\.\d+)?/'
                Accept negative numbers
            '/-?\d+(?:\.\d+)?[, ]-?\d+(?:\.\d+)/?'
                Two numbers, separated by a comma or a space.
                Accept any number of spaces after separator.
            */
            [$latitude, $longitude] = preg_split("/[, ] */", $value);
        } else {
            throw new \Exception("invalid");
        }

        return new Point($latitude, $longitude);
    }

    public function get($model, $key, $value, $attributes): ?Point
    {
        if (null === $value) {
            return null;
        }

        if ($value instanceof Point) {
            return $value;
        }

        if (is_array($value)) {
            // Assume regular arrays are lat, long
            return new Point($value[0], $value[1]);
        }

        // We expect a JSON string from the DB
        if (is_string($value)) {
            $value = GeoJson::jsonUnserialize(json_decode($value));
        }

        // GeoJSON point parsed for the DB string
        if (is_a($value, GeoPoint::class)) {
            $coordinates = $value->getCoordinates();
            // GeoJson is long, lat
            return new Point($coordinates[1], $coordinates[0]);
        }

        return null;
    }

    public static function sqlColumnCast($table, $column): string
    {
        return "ST_AsGeoJson($table.$column)";
    }
}
