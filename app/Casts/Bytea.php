<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * Casts for Bytea pgsql type using hex encoding.
 */
class Bytea implements CastsAttributes
{
    public function set($model, string $key, $value, array $attributes)
    {
        return bin2hex($value);
    }

    public function get($model, string $key, $value, array $attributes)
    {
        if (is_resource($value)) {
            rewind($value);
            $value = stream_get_contents($value);
        }

        return hex2bin($value);
    }
}
