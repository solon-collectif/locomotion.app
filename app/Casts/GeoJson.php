<?php

namespace App\Casts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Query\Expression;
use JsonSerializable;

class GeoJson extends Expression implements Arrayable, JsonSerializable
{
    public function __construct(public string $geoJson)
    {
        // Set the expression value which will be used in DB queries
        parent::__construct("ST_GeomFromGeoJson('$this->geoJson')");
    }

    public function toArray(): array
    {
        return json_decode($this->geoJson, true);
    }

    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }
}
