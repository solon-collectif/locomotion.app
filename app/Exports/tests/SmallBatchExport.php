<?php

namespace App\Exports\tests;

use App\Exports\BaseExport;

class SmallBatchExport extends BaseExport
{
    public function chunkSize(): int
    {
        return 3;
    }

    public function jobChunkCount(): int
    {
        return 2;
    }
}
