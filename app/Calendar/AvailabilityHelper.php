<?php

namespace App\Calendar;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonTimeZone;

class AvailabilityHelper
{
    /**
     * Parse period string of the form 12:34-23:45 to arrays of integers
     * representing time. Accounts for expressions with or without seconds.
     * Will define them if not.
     * Will correct 23:59 end time to 24:00 to comply with the [ , ) interval
     * convention.
     */
    public static function ruleParsePeriodStr($periodStr): array
    {
        [$startTime, $endTime] = explode("-", $periodStr);

        // Explode and convert to integers.
        $startTime = array_map(function ($s) {
            return intval($s);
        }, explode(":", $startTime));
        $endTime = array_map(function ($s) {
            return intval($s);
        }, explode(":", $endTime));

        // Set seconds if not set.
        $startTime[2] = $startTime[2] ?? 0;

        // Set seconds if not set.
        $endTime[2] = $endTime[2] ?? 0;

        // Enforce [, ) interval convention for the end of the day.
        if (23 == $endTime[0] && 59 == $endTime[1]) {
            $endTime = [24, 0, 0];
        }

        // Account for the exception 00:00:00-00:00:00 to be interpreted as
        // full day.
        if (
            0 == $startTime[0] &&
            0 == $startTime[1] &&
            0 == $startTime[2] &&
            0 == $endTime[0] &&
            0 == $endTime[1] &&
            0 == $endTime[2]
        ) {
            $endTime = [24, 0, 0];
        }

        return [$startTime, $endTime];
    }

    /**
     * @param array $rule
     *   Array containing:
     *     - type: "dates"
     *     - scope:
     *           An array of individual unordered dates.
     *     - [period]:
     *           Optional string defining availability or unavailability times.
     *
     * @param Interval|null $context
     * @param CarbonTimeZone|string $timezone
     * @return Interval[]
     */
    public static function ruleGetDatesIntervals(
        array $rule,
        Interval $context = null,
        CarbonTimeZone|string $timezone = "UTC"
    ): array {
        if (isset($rule["period"])) {
            $periodInterval = self::ruleParsePeriodStr($rule["period"]);
        } else {
            $periodInterval = [[0, 0, 0], [24, 0, 0]];
        }

        $intervals = [];
        foreach ($rule["scope"] as $dateStr) {
            $currentDate = new CarbonImmutable($dateStr, $timezone);

            // setTime gracefully accounts for time = 24:00:00
            // and will set to 00:00:00 on the next day :)
            $interval = new Interval(
                $currentDate->setTime(
                    $periodInterval[0][0],
                    $periodInterval[0][1],
                    $periodInterval[0][2]
                ),
                $currentDate->setTime(
                    $periodInterval[1][0],
                    $periodInterval[1][1],
                    $periodInterval[1][2]
                )
            );

            if (!$context || $interval->intersects($context)) {
                $intervals[] = $interval;
            }
        }

        return $intervals;
    }

    /**
     * @param array $rule
     *   Array containing:
     *     - type: "dateRange"
     *     - scope:
     *           An array of dates from which the first and the last represent the endpoints of the range.
     *           This is a [, ] interval
     *     - [period]:
     *           Optional string defining availability or unavailability times.
     *
     * @param Interval|null $context
     * @param CarbonTimeZone|string $timezone
     * @return Interval[]
     */
    public static function ruleGetDateRangeIntervals(
        array $rule,
        Interval $context = null,
        CarbonTimeZone|string $timezone = "UTC"
    ): array {
        if (isset($rule["period"])) {
            $periodInterval = self::ruleParsePeriodStr($rule["period"]);
        } else {
            $periodInterval = [[0, 0, 0], [24, 0, 0]];
        }

        // Get first and last days of interval no matter the
        // format of scope (list of all dates or start and end date).
        // Assume they are in order.
        $ruleRange = [null, null];
        foreach ($rule["scope"] as $dateStr) {
            if (!$ruleRange[0]) {
                $ruleRange[0] = $dateStr;
            }
            $ruleRange[1] = $dateStr;
        }

        $ruleRange = new Interval(
            (new Carbon($ruleRange[0], $timezone))->setTime(
                $periodInterval[0][0],
                $periodInterval[0][1],
                $periodInterval[0][2]
            ),
            (new Carbon($ruleRange[1], $timezone))->setTime(
                $periodInterval[1][0],
                $periodInterval[1][1],
                $periodInterval[1][2]
            )
        );

        // Prepare range.
        if ($context) {
            // Intersection of the two ranges so as to have the least number of days to check.
            $dateRange = $context->intersection($ruleRange);

            // If no intersection, then no interval.
            if (empty($dateRange)) {
                return [];
            }
        } else {
            $dateRange = $ruleRange;
        }

        // Set the timezone, since the context might not share the same tz.
        $currentDate = $dateRange->start->timezone($timezone)->startOfDay();
        $end = $dateRange->end->setTimezone($timezone);
        $intervals = [];
        while ($currentDate->lessThan($end)) {
            $intervals[] = new Interval(
                $currentDate->setTime(
                    $periodInterval[0][0],
                    $periodInterval[0][1],
                    $periodInterval[0][2]
                ),
                $currentDate->setTime(
                    $periodInterval[1][0],
                    $periodInterval[1][1],
                    $periodInterval[1][2]
                )
            );

            $currentDate = $currentDate->addDay();
        }

        return $intervals;
    }

    /**
     * @param array $rule
     *   Array containing:
     *     - type: "weekdays"
     *     - scope:
     *           Weekdays on which the rule applies.
     *     - [period]:
     *           Optional string defining availability or unavailability times.
     *
     * @param Interval $context
     * @param CarbonTimeZone|string $timezone
     * @return Interval[]
     */
    public static function ruleGetWeekdaysIntervals(
        array $rule,
        Interval $context,
        CarbonTimeZone|string $timezone = "UTC"
    ): array {
        static $isoWeekdays = [
            1 => "MO",
            2 => "TU",
            3 => "WE",
            4 => "TH",
            5 => "FR",
            6 => "SA",
            7 => "SU",
        ];

        if (isset($rule["period"])) {
            $periodInterval = self::ruleParsePeriodStr($rule["period"]);
        } else {
            $periodInterval = [[0, 0, 0], [24, 0, 0]];
        }

        $currentDate = $context->start->setTimezone($timezone)->startOfDay();
        $end = $context->end->setTimezone($timezone);
        $intervals = [];
        while ($currentDate->lessThan($end)) {
            if (
                in_array(
                    $isoWeekdays[$currentDate->isoWeekday()],
                    $rule["scope"]
                )
            ) {
                $intervals[] = new Interval(
                    $currentDate->setTime(
                        $periodInterval[0][0],
                        $periodInterval[0][1],
                        $periodInterval[0][2]
                    ),
                    $currentDate->setTime(
                        $periodInterval[1][0],
                        $periodInterval[1][1],
                        $periodInterval[1][2]
                    )
                );
            }

            $currentDate = $currentDate->addDay();
        }

        return $intervals;
    }

    /**
     * For all availability rules, will generate daily intervals over a period
     * given by dateRange.
     * Daily means that any interval such as those returned by date ranges will
     * be split into individual days.
     *
     * @param array $availabilityParams
     *     available: boolean indicating the default availability.
     *     rules: Exceptions to the default availability.
     *
     * @param Interval $context
     * @param CarbonTimeZone|string $timezone
     *
     * @return Interval[]
     */
    public static function getScheduleIntervals(
        array $availabilityParams,
        Interval $context,
        CarbonTimeZone|string $timezone = "UTC"
    ): array {
        $intervals = [];

        // Get availability or unavailability intervals.
        foreach ($availabilityParams["rules"] as $rule) {
            $ruleIntervals = match ($rule["type"]) {
                "dates" => self::ruleGetDatesIntervals(
                    $rule,
                    $context,
                    $timezone
                ),
                "dateRange" => self::ruleGetDateRangeIntervals(
                    $rule,
                    $context,
                    $timezone
                ),
                "weekdays" => self::ruleGetWeekdaysIntervals(
                    $rule,
                    $context,
                    $timezone
                ),
                default => [],
            };

            $intervals = array_merge($intervals, $ruleIntervals);
        }

        return DateIntervalHelper::simplify($intervals);
    }

    /**
     *  Return availability intervals according to availability rules for the given date range.
     *
     * @param array $availabilityParams
     *     available: boolean indicating the default availability.
     *     rules: Exceptions to the default availability.
     *
     * @param Interval $context
     *     The date range over which to compute availability intervals
     * @param CarbonTimeZone|string $timezone
     *
     * @return Interval[]
     */
    public static function getAvailability(
        array $availabilityParams,
        Interval $context,
        CarbonTimeZone|string $timezone = "UTC"
    ): array {
        $intervals = self::getScheduleIntervals(
            $availabilityParams,
            $context,
            $timezone
        );

        if ($availabilityParams["available"]) {
            // rules gave us intervals of unavailability
            return DateIntervalHelper::invert($context, $intervals);
        }

        return $intervals;
    }

    /**
     * This method checks whether the loanable is available based on the
     * availability schedule.
     *
     * @param array $availabilityParams
     *     available: boolean indicating the default availability.
     *     rules: Exceptions to the default availability.
     * @param Interval $loanInterval
     * @param CarbonTimeZone|string $timezone
     * @return bool
     */
    public static function isScheduleAvailable(
        array $availabilityParams,
        Interval $loanInterval,
        CarbonTimeZone|string $timezone = "UTC"
    ): bool {
        $availabilityIntervals = self::getAvailability(
            $availabilityParams,
            $loanInterval,
            $timezone
        );

        // If intervals cover loanInterval, then loanable is available
        return DateIntervalHelper::cover($availabilityIntervals, $loanInterval);
    }
}
