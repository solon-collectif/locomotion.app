<?php

namespace App\Calendar;

use Carbon\Carbon;
use Carbon\CarbonImmutable;

class Interval
{
    public CarbonImmutable $start;
    public CarbonImmutable $end;

    /**
     * @param CarbonImmutable $start
     * @param CarbonImmutable $end
     */
    public function __construct(
        Carbon|CarbonImmutable $start,
        Carbon|CarbonImmutable $end
    ) {
        $this->start =
            $start instanceof CarbonImmutable ? $start : $start->toImmutable();
        $this->end =
            $end instanceof CarbonImmutable ? $end : $end->toImmutable();
    }

    public static function of(
        string $start,
        string $end,
        ?string $timezone = null
    ): Interval {
        if ($timezone) {
            return new Interval(
                new CarbonImmutable($start, $timezone),
                new CarbonImmutable($end, $timezone)
            );
        }

        return new Interval(
            new CarbonImmutable($start),
            new CarbonImmutable($end)
        );
    }

    public function atTimezone(string $tz): Interval
    {
        return new Interval(
            $this->start->timezone($tz),
            $this->end->timezone($tz)
        );
    }

    public function isEmpty(): bool
    {
        return $this->end->lessThanOrEqualTo($this->start);
    }

    public function intersects(Interval $other): bool
    {
        // We do not check for equality: $this->start = $other->end means we're touching, not
        // intersecting
        return !$this->isEmpty() &&
            !$other->isEmpty() &&
            $this->end->greaterThan($other->start) &&
            $this->start->lessThan($other->end);
    }

    public function intersection(Interval $other): ?Interval
    {
        if (!$this->intersects($other)) {
            return null;
        }

        if ($this->start->lessThanOrEqualTo($other->start)) {
            if ($this->end->lessThan($other->end)) {
                return new Interval($other->start, $this->end);
            }
            return $other;
        }

        if ($this->end->greaterThanOrEqualTo($other->end)) {
            return new Interval($this->start, $other->end);
        }

        return $this;
    }
}
