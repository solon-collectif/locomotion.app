<?php

namespace App\Calendar;

use Carbon\Carbon;
use Carbon\CarbonImmutable;

class CalendarHelper
{
    public static function getCalendarDays(
        Carbon|CarbonImmutable $start,
        Carbon|CarbonImmutable $end,
        string $timezone = "UTC"
    ): int {
        // These variables are built gradually to become start and end of the
        // day as we move forward, hence their name.
        $startOfDaysCovered = $start
            ->copy()
            ->tz($timezone)
            ->setMilliseconds(0);
        $endOfDaysCovered = $end
            ->copy()
            ->tz($timezone)
            ->setMilliseconds(0);

        // Milliseconds must be set so the comparison is accurate.
        // Return 0 for degenerate loan intervals.
        if ($endOfDaysCovered->lessThanOrEqualTo($startOfDaysCovered)) {
            return 0;
        }

        // Snap to start of day.
        $startOfDaysCovered = $startOfDaysCovered->startOfDay();

        // Snap to end of day (beginning of next day). Consider [, ) intervals.
        if (
            $endOfDaysCovered->hour > 0 ||
            $endOfDaysCovered->minute > 0 ||
            $endOfDaysCovered->second > 0
        ) {
            $endOfDaysCovered = $endOfDaysCovered->addDay()->startOfDay();
        }

        return (int) $startOfDaysCovered->diffInDays($endOfDaysCovered);
    }
}
