<?php

namespace App\Calendar;

use Ds\PriorityQueue;

class DateIntervalHelper
{
    /**
     * Returns whether intervals cover a given interval
     *
     * Will chew intervals from interval to cover and return whether the result
     * is empty at the end.
     *
     * @param Interval[] $intervals
     *     Array of intervals that are expected to cover given interval.
     *
     * @param Interval $toCover
     *     Interval to cover.
     */
    public static function cover(array $intervals, Interval $toCover): bool
    {
        return empty(self::subtractList([$toCover], $intervals));
    }

    /**
     * @param Interval[] $intervals.
     * @return Interval[] Interval list without empty intervals
     */
    public static function filterEmpty(array $intervals): array
    {
        return array_filter($intervals, fn($interval) => !$interval->isEmpty());
    }

    /**
     * Returns a sorted, simplified list of included intervals without the excluded intervals.
     *
     * @param Interval[] $includedList
     * @param Interval[] $excludedList
     * @return Interval[]
     */
    public static function subtractList(
        array $includedList,
        array $excludedList
    ): array {
        return self::simplify($includedList, $excludedList);
    }

    /**
     * Returns a sorted, simplified list of included intervals without the excluded intervals.
     *
     * @param Interval[] $includedList
     * @param Interval[] $excludedList
     * @return Interval[]
     */
    public static function simplify(
        array $includedList,
        array $excludedList = []
    ): array {
        $includedList = self::filterEmpty($includedList);
        $excludedList = self::filterEmpty($excludedList);

        $events = new PriorityQueue();
        $events->allocate(count($includedList) * 2 + count($excludedList) * 2);

        /*
         * If events happen at the same time, priority is given to:
         * 1. Exlusion start
         *      This avoid an included span starting at the same time creating a 0 length interval
         * 2. Inclusion start
         *      This is to allow contiguous intervals to be joined in a single interval
         * 3. Inclusion end
         *      To avoid starting a 0 length interval if an inclusion and an exlusion end at the
         *      same time
         * 4. Exclusion end
         */
        foreach ($excludedList as $excludedInterval) {
            $events->push(
                [$excludedInterval->start, "type" => "exludedIntervalStart"],
                priority: PHP_INT_MAX - $excludedInterval->start->timestamp
            );
        }
        foreach ($includedList as $includedInterval) {
            $events->push(
                [$includedInterval->start, "type" => "includedIntervalStart"],
                priority: PHP_INT_MAX - $includedInterval->start->timestamp
            );
        }
        foreach ($includedList as $includedInterval) {
            $events->push(
                [$includedInterval->end, "type" => "includedIntervalEnd"],
                priority: PHP_INT_MAX - $includedInterval->end->timestamp
            );
        }
        foreach ($excludedList as $excludedInterval) {
            $events->push(
                [$excludedInterval->end, "type" => "excludedIntervalEnd"],
                priority: PHP_INT_MAX - $excludedInterval->end->timestamp
            );
        }
        $includedDepth = 0;
        $excludedDepth = 0;
        $currentIntervalStart = null;
        $finalIntervals = [];

        while (!$events->isEmpty()) {
            $event = $events->pop();
            switch ($event["type"]) {
                case "includedIntervalStart":
                    if ($includedDepth === 0 && $excludedDepth === 0) {
                        $currentIntervalStart = $event[0];
                    }

                    $includedDepth++;
                    break;
                case "includedIntervalEnd":
                    if ($includedDepth === 1 && $excludedDepth === 0) {
                        // Do not create 0 width intervals
                        if ($event[0]->greaterThan($currentIntervalStart)) {
                            $finalIntervals[] = new Interval(
                                $currentIntervalStart,
                                $event[0]
                            );
                        }
                        $currentIntervalStart = null;
                    }

                    $includedDepth--;
                    break;
                case "exludedIntervalStart":
                    if ($includedDepth > 0 && $excludedDepth === 0) {
                        // avoid cases where events overlap
                        if ($event[0]->greaterThan($currentIntervalStart)) {
                            $finalIntervals[] = new Interval(
                                $currentIntervalStart,
                                $event[0]
                            );
                        }
                        $currentIntervalStart = null;
                    }
                    $excludedDepth++;
                    break;
                case "excludedIntervalEnd":
                    if ($includedDepth > 0 && $excludedDepth === 1) {
                        $currentIntervalStart = $event[0];
                    }
                    $excludedDepth--;
                    break;
            }
        }

        return $finalIntervals;
    }

    /**
     * Returns the complementary intervals to $intervals within $context.
     *
     * @param Interval $context
     * @param Interval[] $intervals
     * @return Interval[]
     */
    public static function invert(Interval $context, array $intervals): array
    {
        return self::subtractList([$context], $intervals);
    }

    /**
     * Joins and simplifies the input intervals
     *
     * @param Interval[] $includedList
     * @param Interval $interval
     * @return Interval[]
     */
    public static function union(array $includedList, Interval $interval): array
    {
        return self::simplify([$interval, ...$includedList]);
    }

    /**
     * Subtracts and simplifies the input intervals
     *
     * @param Interval[] $includedList
     * @param Interval $intervalToExclude
     * @return Interval[]
     */
    public static function subtraction(
        array $includedList,
        Interval $intervalToExclude
    ): array {
        return self::simplify($includedList, [$intervalToExclude]);
    }
}
