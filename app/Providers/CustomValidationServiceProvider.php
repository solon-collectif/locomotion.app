<?php

namespace App\Providers;

use App\Models\Loan;
use App\Models\Loanable;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Validator;

class CustomValidationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend("available", function (
            $attribute,
            $value,
            $parameters,
            $validator
        ) {
            $validatorData = $validator->getData();
            [
                "departure_at" => $departureAt,
                "duration_in_minutes" => $durationInMinutes,
            ] = $validatorData;

            $loanId = null;
            if (isset($validatorData["id"])) {
                $loanId = $validatorData["id"];
            }

            $loanable = Loanable::find($value);

            return !!$loanable &&
                $loanable->isAvailable(
                    new Carbon($departureAt, $loanable->timezone),
                    $durationInMinutes,
                    $loanId
                );
        });

        Validator::replacer("available", function () {
            return "Le véhicule n'est pas disponible sur cette période.";
        });

        Validator::extend("extendable", function (
            $attribute,
            $value,
            $parameters,
            $validator
        ) {
            [
                "new_duration" => $newDuration,
            ] = $validator->getData();
            $loan = Loan::find($value);
            return $loan->loanable->isAvailable(
                $loan->departure_at,
                $newDuration,
                $loan->id
            );
        });

        Validator::replacer("extendable", function () {
            return "Le véhicule n'est pas disponible sur cette période.";
        });
    }
}
