<?php

namespace App\Providers;

use App\Services\BrevoService;
use App\Services\MailchimpService;
use App\Services\MailingListService;
use http\Exception\InvalidArgumentException;
use Illuminate\Support\ServiceProvider;

class MailingListServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(MailingListService::class, function ($app, $params) {
            return match ($params["provider"]) {
                "mailchimp" => new MailchimpService(
                    $params["api_key"],
                    $params["list_id"] ?? null,
                    $params["tag"] ?? null
                ),
                "brevo" => new BrevoService(
                    $params["api_key"],
                    isset($params["list_id"]) ? (int) $params["list_id"] : null
                ),
                default => throw new InvalidArgumentException(
                    "Unknown provider '{$params["provider"]}'"
                ),
            };
        });
    }
}
