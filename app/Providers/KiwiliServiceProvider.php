<?php

namespace App\Providers;

use App\Services\KiwiliService;
use Illuminate\Support\ServiceProvider;

class KiwiliServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton("kiwili", fn() => new KiwiliService());
    }

    public function boot(): void
    {
    }
}
