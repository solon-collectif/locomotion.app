<?php

namespace App\Providers;

use App\Events\CommunityUserApprovedEvent;
use App\Events\CommunityUserUnapprovedEvent;
use App\Events\IncidentAssigneeChangedEvent;
use App\Events\IncidentCreatedEvent;
use App\Events\IncidentNoteAddedEvent;
use App\Events\IncidentReopenedEvent;
use App\Events\IncidentResolvedEvent;
use App\Events\LoanablePublishedEvent;
use App\Events\LoanableUserRoleAdded;
use App\Events\LoanableUserRoleRemoved;
use App\Events\LoanAcceptedEvent;
use App\Events\LoanCommentAddedEvent;
use App\Events\LoanCreatedEvent;
use App\Events\LoanEndedEvent;
use App\Events\LoanExtensionAcceptedEvent;
use App\Events\LoanExtensionCanceledEvent;
use App\Events\LoanExtensionRejectedEvent;
use App\Events\LoanExtensionRequestedEvent;
use App\Events\LoanPaidEvent;
use App\Events\LoanPrepaidEvent;
use App\Events\LoanRejectedEvent;
use App\Events\LoanUpdatedEvent;
use App\Events\LoanValidatedEvent;
use App\Events\NewYearlyContributionEvent;
use App\Events\PermanentlyDeletingUserDataEvent;
use App\Events\UserClaimedBalanceEvent;
use App\Events\UserDeletedEvent;
use App\Events\UserEmailUpdatedEvent;
use App\Events\UserNameUpdatedEvent;
use App\Listeners\AddUserToCommunityMailingLists;
use App\Listeners\EnsureLoanableTypesExist;
use App\Listeners\PermanentlyDeleteUserFromMailingLists;
use App\Listeners\RemoveUserFromCommunityMailingLists;
use App\Listeners\RemoveUserFromMailingLists;
use App\Listeners\RemoveUserLoanablesFromRemovedCommunity;
use App\Listeners\ResetProofEvaluation;
use App\Listeners\SendIncidentAssigneeChangedEmails;
use App\Listeners\SendIncidentCreatedEmails;
use App\Listeners\SendIncidentNoteAddedEmails;
use App\Listeners\SendIncidentReopenedEmails;
use App\Listeners\SendIncidentResolvedEmails;
use App\Listeners\SendInvoiceEmail;
use App\Listeners\SendLoanablePublishedEmails;
use App\Listeners\SendLoanableUserRoleAddedEmails;
use App\Listeners\SendLoanableUserRoleRemovedMail;
use App\Listeners\SendLoanAcceptedEmails;
use App\Listeners\SendLoanCanceledEmails;
use App\Listeners\SendLoanCommentAddedEmails;
use App\Listeners\SendLoanCreatedEmails;
use App\Listeners\SendLoanEndedEmails;
use App\Listeners\SendLoanExtensionAcceptedEmails;
use App\Listeners\SendLoanExtensionCanceledEmails;
use App\Listeners\SendLoanExtensionRejectedEmails;
use App\Listeners\SendLoanExtensionRequestedEmails;
use App\Listeners\SendLoanPaidEmails;
use App\Listeners\SendLoanPrepaidEmails;
use App\Listeners\SendLoanRejectedEmails;
use App\Listeners\SendLoanUpdatedEmails;
use App\Listeners\SendLoanValidatedEmails;
use App\Listeners\SendUserClaimedBalanceEmails;
use App\Listeners\UpdateUserInMailingLists;
use App\Mail\Loan\LoanCanceledMail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Database\Events\MigrationsEnded;
use Illuminate\Database\Events\NoPendingMigrations;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [SendEmailVerificationNotification::class],
        "App\Events\AddedToUserBalanceEvent" => [
            "App\Listeners\SendInvoiceEmail",
        ],
        UserClaimedBalanceEvent::class => [SendUserClaimedBalanceEmails::class],
        "App\Events\Loan\CanceledEvent" => [
            "App\Listeners\SendLoanCanceledEmails",
        ],
        LoanCreatedEvent::class => [SendLoanCreatedEmails::class],
        LoanAcceptedEvent::class => [SendLoanAcceptedEmails::class],
        LoanRejectedEvent::class => [SendLoanRejectedEmails::class],
        LoanPrepaidEvent::class => [SendLoanPrepaidEmails::class],
        LoanEndedEvent::class => [SendLoanEndedEmails::class],

        LoanPaidEvent::class => [SendLoanPaidEmails::class],
        LoanCanceledMail::class => [SendLoanCanceledEmails::class],
        LoanExtensionRequestedEvent::class => [
            SendLoanExtensionRequestedEmails::class,
        ],
        LoanExtensionCanceledEvent::class => [
            SendLoanExtensionCanceledEmails::class,
        ],
        LoanExtensionAcceptedEvent::class => [
            SendLoanExtensionAcceptedEmails::class,
        ],
        LoanExtensionRejectedEvent::class => [
            SendLoanExtensionRejectedEmails::class,
        ],

        LoanCommentAddedEvent::class => [SendLoanCommentAddedEmails::class],

        "App\Events\RegistrationSubmittedEvent" => [
            "App\Listeners\SendRegistrationSubmittedEmails",
            ResetProofEvaluation::class,
        ],
        CommunityUserApprovedEvent::class => [
            "App\Listeners\SendRegistrationApprovedEmails",
            AddUserToCommunityMailingLists::class,
        ],
        "App\Events\RegistrationRejectedEvent" => [
            "App\Listeners\SendRegistrationRejectedEmails",
        ],
        "App\Events\BorrowerCompletedEvent" => [
            "App\Listeners\SendBorrowerCompletedEmails",
        ],
        "App\Events\BorrowerApprovedEvent" => [
            "App\Listeners\SendBorrowerApprovedEmails",
        ],
        LoanablePublishedEvent::class => [SendLoanablePublishedEmails::class],
        LoanableUserRoleAdded::class => [
            SendLoanableUserRoleAddedEmails::class,
        ],
        LoanableUserRoleRemoved::class => [
            SendLoanableUserRoleRemovedMail::class,
        ],
        LoanValidatedEvent::class => [SendLoanValidatedEmails::class],
        LoanUpdatedEvent::class => [SendLoanUpdatedEmails::class],
        NewYearlyContributionEvent::class => [SendInvoiceEmail::class],
        IncidentCreatedEvent::class => [SendIncidentCreatedEmails::class],
        IncidentResolvedEvent::class => [SendIncidentResolvedEmails::class],
        IncidentReopenedEvent::class => [SendIncidentReopenedEmails::class],
        IncidentNoteAddedEvent::class => [SendIncidentNoteAddedEmails::class],
        IncidentAssigneeChangedEvent::class => [
            SendIncidentAssigneeChangedEmails::class,
        ],

        CommunityUserUnapprovedEvent::class => [
            RemoveUserFromCommunityMailingLists::class,
            RemoveUserLoanablesFromRemovedCommunity::class,
        ],

        UserEmailUpdatedEvent::class => [UpdateUserInMailingLists::class],
        UserNameUpdatedEvent::class => [UpdateUserInMailingLists::class],
        UserDeletedEvent::class => [RemoveUserFromMailingLists::class],
        PermanentlyDeletingUserDataEvent::class => [
            PermanentlyDeleteUserFromMailingLists::class,
        ],
        MigrationsEnded::class => [EnsureLoanableTypesExist::class],
        NoPendingMigrations::class => [EnsureLoanableTypesExist::class],
    ];

    public function boot()
    {
        parent::boot();
    }
}
