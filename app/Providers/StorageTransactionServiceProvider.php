<?php

namespace App\Providers;

use App\Filesystem\StorageTransactionHandler;
use Illuminate\Support\ServiceProvider;

class StorageTransactionServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(
            "storagetransaction",
            fn() => new StorageTransactionHandler()
        );
    }
}
