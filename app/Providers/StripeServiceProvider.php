<?php

namespace App\Providers;

use App\Services\StripeService;
use Illuminate\Support\ServiceProvider;

/** @see StripeService */
class StripeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton("stripe", function ($app) {
            return new StripeService(config("services.stripe.secret"));
        });
    }
}
