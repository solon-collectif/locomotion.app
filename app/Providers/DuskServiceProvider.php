<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Dusk;

class DuskServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function boot(): void
    {
        Dusk::selectorHtmlAttribute("data-dusk");

        Browser::macro("as", function (User $user) {
            $token = $user->createToken("test_token");
            $this->visit("/");
            $tokenScript = "window.localStorage.locomotion = '{\"token\": \"$token->accessToken\"}';";
            $this->script($tokenScript);
            $this->refresh();
            $this->waitFor(".nav-item .generic-avatar");
            // Setting the token and refreshing will lead to navigating to user dashboard
            // which will be slow and seems to sometimes pause the webdriver for a few seconds.
            // To prevent this, we forcibly navigate to the new tab page, which interrupts our
            // app.
            $this->driver->navigate()->to("chrome://newtab");

            return $this;
        });

        Browser::macro("go", function (string $path) {
            $this->visit($path);
            $this->waitForTransition();
        });

        Browser::macro("waitForTransition", function () {
            $this->waitFor(".layout-page");
            $this->waitUntilMissing("::view-transition-new(*)");
            $this->pause(200);
        });

        Browser::macro("clickAndWait", function (string $target) {
            $this->click($target);
            $this->waitForTransition();
        });
    }

    public function provides(): array
    {
        return [Dusk::class, Browser::class];
    }
}
