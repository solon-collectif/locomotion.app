<?php

namespace App\Providers;

use App\Http\Resources\ExportResource;
use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Export;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\Trailer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        if ($this->app->environment("local", "testing")) {
            $this->app->register(
                "Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider"
            );
            $this->app->register("App\Providers\DuskServiceProvider");
        }

        Passport::enablePasswordGrant();
        Passport::tokensExpireIn(
            Carbon::now()->addDays(config("auth.token.expiration_in_days"))
        );
        Passport::refreshTokensExpireIn(
            Carbon::now()->addDays(config("auth.token.expiration_in_days"))
        );

        Client::creating(function (Client $client) {
            $client->incrementing = false;
            $client->id = $this->generateClientId();
        });

        Client::retrieved(function (Client $client) {
            $client->incrementing = false;
        });

        Blade::directive("money", function ($amount) {
            return "<?php echo \App\Helpers\Formatter::currency($amount); ?>";
        });

        Relation::enforceMorphMap([
            "library" => Library::class,
            "loanable" => Loanable::class,
            "loan" => Loan::class,
            "car" => Car::class,
            "bike" => Bike::class,
            "trailer" => Trailer::class,

            "user" => User::class,
            "communityUser" => CommunityUser::class,
            "borrower" => Borrower::class,

            "export" => Export::class,
        ]);

        ExportResource::withoutWrapping();
    }

    private function generateClientId(
        int $length = 16,
        string $keyspace = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    ): string {
        if ($length < 1) {
            throw new \RangeException("Length must be a positive integer");
        }

        $pieces = [];
        $max = mb_strlen($keyspace, "8bit") - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $keyspace[random_int(0, $max)];
        }

        return implode("", $pieces);
    }
}
