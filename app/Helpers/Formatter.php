<?php

namespace App\Helpers;

use App\Models\Loan;
use App\Models\Loanable;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;

class Formatter
{
    public static function trimEachLine($text): string
    {
        return implode("\n", array_map("trim", explode("\n", $text)));
    }

    public static function joinEachLine($text): string
    {
        return preg_replace("/\s{3,}/", " ", $text);
    }

    public static function formatDay(CarbonInterface $date): string
    {
        if ($date->year === Carbon::now()->year) {
            return $date->translatedFormat("j F");
        }
        return $date->translatedFormat("j F Y");
    }

    public static function currency($amount): string
    {
        return number_format($amount, 2, ",", " ") . '$';
    }

    public static function formatDate(
        CarbonInterface|string $date,
        string $explicitTimezone = null,
        bool $forceYear = false
    ): string {
        if (!$date instanceof Carbon) {
            $date = new Carbon($date);
        }

        if ($explicitTimezone) {
            $date->timezone($explicitTimezone);
        }

        $format = "j F Y \à H:i";

        if ($date->year === Carbon::now()->year && !$forceYear) {
            $format = "j F \à H:i";
        }
        return $date->translatedFormat(
            $format . ($explicitTimezone ? $date->format(" T") : "")
        );
    }

    public static function formatDepartureTime(Loan $loan): string
    {
        return self::formatLoanableTime($loan->loanable, $loan->departure_at);
    }

    public static function formatLoanableTime(
        Loanable $loanable,
        CarbonInterface|string $time
    ): string {
        if (!is_a($time, CarbonInterface::class)) {
            $time = new CarbonImmutable($time);
        }

        return self::formatDate(
            $time->tz($loanable->timezone)->locale("fr_CA")
        );
    }

    public static function formatDuration(int $duration): string
    {
        if ($duration < 60) {
            return "$duration minutes";
        }
        $hours = floor($duration / 60);
        $minutes = $duration - $hours * 60;

        if ($minutes == 0) {
            return "{$hours}h";
        }

        return "{$hours}h {$minutes}m";
    }

    public static function formatReturnTime(
        Loan $loan,
        int $duration,
        CarbonImmutable|string $departure = null
    ): string {
        if ($departure === null) {
            $departure = $loan->departure_at;
        }
        if (!is_a($departure, CarbonImmutable::class)) {
            $departure = new CarbonImmutable($departure);
        }

        return self::formatDate(
            $departure
                ->tz($loan->loanable->timezone)
                ->locale("fr_CA")
                ->addMinutes($duration)
        );
    }

    public static function formatCurrency($amount)
    {
        return number_format($amount, 2, ",", " ") . ' $';
    }
}
