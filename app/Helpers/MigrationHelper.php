<?php

namespace App\Helpers;

use DB;

class MigrationHelper
{
    public static function changeEnumOptions(
        string $table,
        string $column,
        array $options
    ): void {
        $constraint = $table . "_" . $column . "_check";
        $optionsLiteral = implode(", ", array_map(fn($o) => "'$o'", $options));

        DB::statement(
            "ALTER TABLE $table DROP CONSTRAINT IF EXISTS $constraint;"
        );
        DB::statement(
            <<<SQL
ALTER TABLE $table
    add CONSTRAINT $constraint
        CHECK ($column = ANY (ARRAY[$optionsLiteral]))
SQL
        );
    }
}
