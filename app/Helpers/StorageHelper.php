<?php

namespace App\Helpers;

use App\Enums\FileMoveResult;
use App\Facades\StorageTransaction;
use Storage;

class StorageHelper
{
    public static function copy(
        string $source,
        string $destination
    ): FileMoveResult {
        if (Path::isSameAbsolute($source, $destination)) {
            return FileMoveResult::unchanged;
        }

        // Not overwriting: we have unique paths for our images and files DB entries, and each new
        // uploaded file is a new DB entry.
        if (Storage::exists($destination)) {
            return FileMoveResult::unchanged;
        }

        $copied = StorageTransaction::copy($source, $destination);

        if ($copied) {
            return FileMoveResult::copied;
        }

        // Sometimes moving fails with the s3 adapter with a 404 for the file to be
        // created. It's fairly mysterious. Work around: fetching the file and creating a new one.
        $data = Storage::get($source);
        if (!$data) {
            return FileMoveResult::failed;
        }

        $copied = StorageTransaction::put($destination, $data);

        if ($copied) {
            return FileMoveResult::copied;
        }
        return FileMoveResult::failed;
    }

    public static function move(
        string $source,
        string $destination
    ): FileMoveResult {
        if (Path::isSameAbsolute($source, $destination)) {
            return FileMoveResult::unchanged;
        }

        if (!Storage::exists($destination)) {
            // We use our copy to fall back to fetching and storing in case the Storage::copy call fails
            // In the background, Storage::move also simply does a copy followed by a delete.
            $copied = self::copy($source, $destination);
            if ($copied === FileMoveResult::failed) {
                return FileMoveResult::failed;
            }
        } else {
            // File already existed at destination
            if (!Storage::exists($source)) {
                return FileMoveResult::unchanged;
            }
        }

        if (!StorageTransaction::delete($source)) {
            \Log::warning(
                "Failed deleting original $source after copying it to $destination"
            );
            return FileMoveResult::copied;
        }
        return FileMoveResult::moved;
    }

    public static function copyDirectory(
        string $sourceDir,
        string $destDir,
        string $originalName,
        string $newName
    ): FileMoveResult {
        if (
            Path::isSameAbsolute($sourceDir, $destDir) &&
            $originalName === $newName
        ) {
            return FileMoveResult::unchanged;
        }

        $files = Storage::files($sourceDir);
        $allFilesMoveResult = FileMoveResult::unchanged;

        foreach ($files as $filePath) {
            $filename = basename($filePath);

            $newFileName = str_replace($originalName, $newName, $filename);

            $newFilePath = Path::join($destDir, $newFileName);
            $moveResult = self::copy($filePath, $newFilePath);
            if ($moveResult === FileMoveResult::failed) {
                // Attempt to move back the files that have been moved
                \Log::warning(
                    "Copying directories failed, could not copy $filePath to $newFilePath"
                );
                return FileMoveResult::failed;
            }

            // Once we copied one file, we consider the whole operation a copy.
            if ($allFilesMoveResult !== FileMoveResult::copied) {
                $allFilesMoveResult = $moveResult;
            }
        }

        return $allFilesMoveResult;
    }

    public static function moveDirectoryFiles(
        string $sourceDir,
        string $destDir,
        string $originalName,
        string $newName
    ): FileMoveResult {
        $copyResult = self::copyDirectory(
            $sourceDir,
            $destDir,
            $originalName,
            $newName
        );
        if ($copyResult === FileMoveResult::failed) {
            return FileMoveResult::failed;
        }

        if (Path::isSameAbsolute($sourceDir, $destDir)) {
            return $copyResult;
        }

        $deleted = StorageTransaction::deleteDirectory($sourceDir);
        if (!$deleted) {
            return FileMoveResult::copied;
        }
        return FileMoveResult::moved;
    }
}
