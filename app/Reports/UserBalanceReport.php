<?php

namespace App\Reports;

use App\Enums\BillItemTypes;
use Carbon\CarbonImmutable;

class UserBalanceReport
{
    public static function generate(
        ?int $userId = null,
        ?int $minUserId = null,
        ?CarbonImmutable $at = null,
        bool $historyOnly = false,
        bool $nonZeroOnly = false,
        bool $mismatchOnly = false,
        string $format = "csv",
        $outputStream = STDOUT
    ) {
        if ($at) {
            $historyOnly = true;
        } else {
            $at = CarbonImmutable::now();
        }

        $fieldTitles = [
            "user.id" => "ID utilisateur",
            "user.full_name" => "Utilisateur",
            "invoices_count" => "Nb factures",
            "user_balance_from_invoices" => "Solde (factures)",
        ];

        if (!$historyOnly) {
            $fieldTitles["user.balance"] = "Solde";
        }

        if ($format === "csv") {
            fputcsv($outputStream, ["Soldes utilisateur le {$at}."]);
            fputcsv($outputStream, $fieldTitles);
        }

        $totalBalance = 0;
        $totalBalanceFromInvoices = 0;

        $usersMatching = 0;
        $usersMismatching = 0;

        $userQuery = \DB::table("users")
            ->select(["id", "name", "last_name", "balance"])
            ->orderBy("id");

        if ($userId) {
            $userQuery = $userQuery->where("id", $userId);
        } elseif ($minUserId) {
            $userQuery = $userQuery->where("id", ">=", $minUserId);
        }

        $allUsers = $userQuery->get();
        $userIds = $allUsers->pluck("id")->toArray();
        $usersToInvoices = \DB::table("users")
            ->join("invoices", "users.id", "=", "invoices.user_id")
            ->join("bill_items", "invoices.id", "=", "bill_items.invoice_id")
            ->whereNull("invoices.deleted_at")
            ->where("invoices.created_at", "<=", $at->toISOString())
            ->whereIn("user_id", $userIds)
            ->where(
                "bill_items.item_type",
                "!=",
                BillItemTypes::feesStripe->value
            )
            ->select(
                "users.id as user_id",
                "invoices.id as invoice_id",
                \DB::raw(
                    "ROUND(SUM(bill_items.amount) + SUM(bill_items.taxes_tps) + SUM(bill_items.taxes_tvq), 2) as total"
                )
            )
            ->groupBy("users.id", "invoices.id")
            ->get()
            ->groupBy("user_id");

        foreach ($allUsers as $user) {
            $userBalanceFromInvoices = 0;
            $userInvoices = $usersToInvoices->get($user->id, collect([]));
            foreach ($userInvoices as $userInvoice) {
                $userBalanceFromInvoices += $userInvoice->total;
            }

            $userBalanceFromInvoices = round($userBalanceFromInvoices, 2);

            if (
                $nonZeroOnly &&
                ($historyOnly || $user->balance == 0) &&
                $userBalanceFromInvoices < 0.005
            ) {
                continue;
            }

            $totalBalance += $user->balance;
            $totalBalanceFromInvoices += $userBalanceFromInvoices;

            if (!$historyOnly) {
                if (abs($user->balance - $userBalanceFromInvoices) < 0.005) {
                    $usersMatching++;

                    if ($mismatchOnly) {
                        continue;
                    }
                } else {
                    $usersMismatching++;
                }
            }

            $fieldValues = [
                "user.id" => $user->id,
                "user.full_name" => trim($user->name . " " . $user->last_name),
                "invoices_count" => $userInvoices->count(),
                "user_balance_from_invoices" => self::formatCurrency(
                    $userBalanceFromInvoices
                ),
            ];

            if (!$historyOnly) {
                $fieldValues["user.balance"] = self::formatCurrency(
                    $user->balance
                );
            }

            if ($format === "csv") {
                fputcsv($outputStream, $fieldValues);
            }
        }

        if ($format === "csv") {
            if ($historyOnly) {
                fputcsv($outputStream, [
                    "Total",
                    "",
                    "",
                    self::formatCurrency($totalBalanceFromInvoices),
                ]);
            } else {
                fputcsv($outputStream, [
                    "Total",
                    "",
                    "",
                    self::formatCurrency($totalBalanceFromInvoices),
                    self::formatCurrency($totalBalance),
                ]);
                fputcsv($outputStream, []);
                fputcsv($outputStream, [
                    "Utilisateurs qui balancent",
                    $usersMatching,
                ]);
                fputcsv($outputStream, [
                    "Utilisateurs qui ne balancent pas",
                    $usersMismatching,
                ]);
            }
        }
    }

    protected static function formatCurrency($amount): string
    {
        return str_replace(".", ",", (string) round($amount, 2));
    }
}
