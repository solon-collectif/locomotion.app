<?php

namespace App\Reports;

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Loan;
use Carbon\CarbonImmutable;
use Log;

class InsuranceQuarterlyReport
{
    public static function generate(
        CarbonImmutable $from,
        CarbonImmutable $to,
        $format = "csv",
        $outputStream = STDOUT
    ): void {
        $fieldTitles = [
            "id" => "ID item",
            "invoice.id" => "ID facture",
            "invoice.date" => "Date facture",
            "user.id" => "ID Utilisateur",
            "loan.id" => "ID emprunt",
            "loan.departure_at" => "Départ",
            "loan.actual_return_at" => "Retour",
            "loanable.id" => "ID Véhicule",
            "loanableValueCategory" => "Catégorie",
            "without43ae" => "Sans 43 a et e",
            "with43ae" => "Avec 43 a et e",
            "amount" => "Coût assurance",
            "notes" => "Remarques",
        ];

        // Print titles for csv format.
        if ($format === "csv") {
            fputcsv($outputStream, $fieldTitles);
        }

        // Get insurance invoice items for invoices that were issued in the
        // given interval.
        $invoiceItems = BillItem::where(
            "item_type",
            BillItemTypes::loanInsurance->value
        )
            ->whereHas(
                "invoice",
                fn($q) => $q
                    ->where("created_at", ">=", $from)
                    ->where("created_at", "<", $to)
            )
            ->cursor();

        // Number of calendar days for each car pricing category depending if
        // they are under replacement-cost protection (vehicles less than 5
        // years old) or not.
        $insuranceSummary = [
            "lte50k" => ["without43ae" => 0, "with43ae" => 0, "amount" => 0],
            "lte70k" => ["without43ae" => 0, "with43ae" => 0, "amount" => 0],
            "lte100k" => ["without43ae" => 0, "with43ae" => 0, "amount" => 0],
            "total" => ["without43ae" => 0, "with43ae" => 0, "amount" => 0],
        ];

        foreach ($invoiceItems as $item) {
            $insuranceSummary["total"]["amount"] += $item->amount;

            $itemMeta = $item->meta;

            // Insurance invoice items that were created at loan conclusion
            // have metainformation. Items that were created manually don't. We
            // log them so they can be accounted for in accounting.
            if (!$itemMeta) {
                if ($format === "csv") {
                    $fieldValues = [
                        "id" => $item->id,
                        "invoice.id" => $item->invoice_id,
                        "invoice.date" => "",
                        "user.id" => "",
                        "loan.id" => "",
                        "loan.departure_at" => "",
                        "loan.actual_return_at" => "",
                        "loanable.id" => "",
                        "loanableValueCategory" => "",
                        "without43ae" => "",
                        "with43ae" => "",
                        "amount" => str_replace(".", ",", "$item->amount"),
                        "notes" => "Item has no metainformation.",
                    ];

                    fputcsv($outputStream, $fieldValues);
                } else {
                    Log::error(
                        "Item {$item->id} of invoice {$item->invoice_id} has no meta information."
                    );
                }
                continue;
            }

            if (!isset($itemMeta["loanableValueCategory"])) {
                if ($format === "csv") {
                    $fieldValues = [
                        "id" => $item->id,
                        "invoice.id" => $item->invoice_id,
                        "invoice.date" => "",
                        "user.id" => "",
                        "loan.id" => $itemMeta["entityId"],
                        "loan.departure_at" => "",
                        "loan.actual_return_at" => "",
                        "loanable.id" => "",
                        "loanableValueCategory" => "",
                        "without43ae" => "",
                        "with43ae" => "",
                        "amount" => str_replace(".", ",", "$item->amount"),
                        "notes" => "Item has no insurance metainformation.",
                    ];

                    fputcsv($outputStream, $fieldValues);
                } else {
                    Log::error(
                        "Item {$item->id} of invoice {$item->invoice_id} has no insurance metainformation."
                    );
                }
                continue;
            }

            $dailyPremiumFactorFromValueCategoryAttribute = match (
            $itemMeta["loanableValueCategory"]
            ) {
                "lte50k" => 4,
                "lte70k" => 5,
                "lte100k" => 6,
                default => 4,
            };

            $with43ae = $itemMeta["loanableAgeForInsurance"] <= 5;

            $invoice = $item->invoice;
            $user = $invoice->user;

            $loan = Loan::find($itemMeta["entityId"]);

            $fieldValues = [
                "id" => $item->id,
                "invoice.id" => $item->invoice_id,
                "invoice.date" => $invoice->created_at,
                "user.id" => $user->id,
                "loan.id" => $itemMeta["entityId"],
                "loan.departure_at" => $loan->departure_at,
                "loan.actual_return_at" => $loan->actual_return_at,
                "loanable.id" => $loan->loanable_id,
                "loanableValueCategory" => $itemMeta["loanableValueCategory"],
                "without43ae" => $with43ae ? "" : $itemMeta["calendarDays"],
                "with43ae" => $with43ae ? $itemMeta["calendarDays"] : "",
                "amount" => str_replace(".", ",", "$item->amount"),
                "notes" => "",
            ];

            if ($format === "csv") {
                fputcsv($outputStream, $fieldValues);
            }

            $insuranceSummary[$itemMeta["loanableValueCategory"]][
                $with43ae ? "with43ae" : "without43ae"
            ] += $itemMeta["calendarDays"];
            $insuranceSummary[$itemMeta["loanableValueCategory"]]["amount"] +=
                $item->amount;
        }

        $fieldValues = [
            "id" => "",
            "invoice.id" => "",
            "invoice.date" => "",
            "user.id" => "",
            "loan.id" => "",
            "loan.departure_at" => "",
            "loan.actual_return_at" => "",
            "loanable.id" => "",

            "loanableValueCategory" => "Catégorie",
            "without43ae" => "Sans 43 a et e",
            "with43ae" => "Avec 43 a et e",
            "amount" => "Coût assurance",

            "notes" => "",
        ];
        if ($format === "csv") {
            // Add blank line before sumary.
            fputcsv($outputStream, []);
            fputcsv($outputStream, $fieldValues);
        }

        foreach ($insuranceSummary as $category => $summary) {
            $fieldValues["loanableValueCategory"] = $category;
            $fieldValues["without43ae"] = $summary["without43ae"];
            $fieldValues["with43ae"] = $summary["with43ae"];
            $fieldValues["amount"] = $summary["amount"];

            if ($format === "csv") {
                fputcsv($outputStream, $fieldValues);
            }
        }
    }
}
