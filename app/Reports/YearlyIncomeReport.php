<?php

namespace App\Reports;

use App\Enums\BillItemTypes;
use App\Helpers\Formatter;
use App\Models\Invoice;
use App\Models\Loan;
use App\Models\User;
use Carbon\CarbonImmutable;
use Ds\Set;

class YearlyIncomeReport extends PdfReport
{
    private ?array $data = null;

    public function __construct(private User $user, private int $year)
    {
        parent::__construct();
    }

    public function title(): string
    {
        return "Relevé des revenus {$this->year} | {$this->user->full_name}";
    }

    public function currency($amount)
    {
        return Formatter::currency(round($amount, 2));
    }

    public function hasData()
    {
        [$linesByLoanables] = $this->getData();
        return count($linesByLoanables) > 0;
    }

    public function generate()
    {
        [
            $linesByLoanables,
            $sumsByLoanables,
            $extraInvoiceLines,
            $globalSums,
        ] = $this->getData();

        $this->h1("Relevé des revenus {$this->year}");
        $this->fontMain();

        $this->cols([
            function () {
                $this->printLn($this->user->full_name, bold: true);
                foreach (explode(",", $this->user->address) as $addressLine) {
                    $this->printLn(trim($addressLine));
                }
                $this->Ln();
            },
            function () {
                $this->printLn("Produit le", align: "R");
                $this->print(
                    Formatter::formatDate(
                        CarbonImmutable::now(
                            config("app.default_user_timezone")
                        ),
                        forceYear: true
                    ),
                    align: "R",
                    bold: true
                );
            },
        ]);

        $this->h2("Sommaire");
        $this->fontMain();
        $this->SetDrawColor(100);

        if (count($sumsByLoanables) + count($extraInvoiceLines) > 1) {
            $this->printLn(
                "Total pour tous les véhicules",
                lineHeight: 1.5,
                bold: true
            );
            $this->table(
                [
                    ["header" => "Distance", "align" => "R"],
                    ["header" => "Revenu bruts", "align" => "R"],
                    ["header" => "Dépenses", "align" => "R"],
                    ["header" => "Revenu nets", "align" => "R"],
                ],
                [
                    [
                        $globalSums["distance"] . " km",
                        $this->currency($globalSums["price"]),
                        $this->currency($globalSums["expenses"]),
                        $this->currency($globalSums["net"]),
                    ],
                ]
            );
            $this->Ln($this->rem(2));
        }

        foreach ($sumsByLoanables as $loanableId => $data) {
            $this->printLn(
                $data["name"] . " (#$loanableId)",
                lineHeight: 1.5,
                bold: true
            );
            $this->table(
                [
                    ["header" => "Jours empruntés", "align" => "R"],
                    ["header" => "Distance", "align" => "R"],
                    ["header" => "Revenu bruts", "align" => "R"],
                    ["header" => "Dépenses", "align" => "R"],
                    ["header" => "Revenu nets", "align" => "R"],
                ],
                [
                    [
                        $data["covered_days"],
                        $data["distance"] . " km",
                        $this->currency($data["price"]),
                        $this->currency($data["expenses"]),
                        $this->currency($data["net"]),
                    ],
                ]
            );
            $this->Ln($this->rem(2));
        }

        if (count($extraInvoiceLines) > 0) {
            $this->printLn("Factures correctives", lineHeight: 1.5, bold: true);
            $rows = [];
            foreach ($extraInvoiceLines as $line) {
                $rows[] = [
                    $line["invoice_id"],
                    $this->currency($line["price"]),
                    $this->currency($line["expenses"]),
                    $this->currency($line["net"]),
                ];
            }

            $this->table(
                [
                    ["header" => "ID de la facture", "align" => "R"],
                    ["header" => "Revenu bruts", "align" => "R"],
                    ["header" => "Dépenses", "align" => "R"],
                    ["header" => "Revenu nets", "align" => "R"],
                ],
                $rows
            );
            $this->Ln($this->rem(2));
        }

        $this->Ln();
        $this->fontSmall();
        $this->printLn("Détails par emprunt sur la page suivante ", align: "C");

        $this->AddPage();

        $this->h2("Emprunts facturés en " . $this->year);
        $this->fontSmall();

        foreach ($linesByLoanables as $loanableId => $lines) {
            $this->printLn(
                $sumsByLoanables[$loanableId]["name"] . " (#$loanableId)",
                lineHeight: 1.5,
                bold: true
            );
            $this->table(
                [
                    ["header" => "# Emprunt", "width" => "6rem"],
                    ["header" => "Date du paiement", "width" => "10rem"],
                    ["header" => "Distance", "align" => "R"],
                    ["header" => "Revenu bruts", "align" => "R"],
                    ["header" => "Dépenses", "align" => "R"],
                    ["header" => "Revenu nets", "align" => "R"],
                ],
                array_map(
                    fn($line) => [
                        $line["loan_id"],
                        $line["payment_date"],
                        $line["distance"] . " km",
                        $this->currency($line["price"]),
                        $this->currency($line["expenses"]),
                        $this->currency($line["net"]),
                    ],
                    $lines
                )
            );
            $this->Ln($this->rem(2));
        }

        return $this->Output("S");
    }

    public function getData(): ?array
    {
        if ($this->data) {
            return $this->data;
        }

        $followingYear = $this->year + 1;
        $invoiceQuery = $this->user
            ->invoices()
            ->where(
                "created_at",
                ">=",
                (new CarbonImmutable(
                    "{$this->year}-01-01",
                    config("app.default_user_timezone")
                ))
                    ->startOfDay()
                    ->toISOString()
            )
            ->where(
                "created_at",
                "<",
                (new CarbonImmutable(
                    "{$followingYear}-01-01",
                    config("app.default_user_timezone")
                ))
                    ->startOfDay()
                    ->toISOString()
            )
            ->with("billItems");

        $invoices = $invoiceQuery->get()->groupBy("id");

        $loansAsOwner = Loan::whereIn(
            "owner_invoice_id",
            $invoices->keys()
        )->get();

        $linesPerLoanables = [];
        $sumsPerLoanables = [];
        $globalSums = [
            "price" => 0,
            "expenses" => 0,
            "net" => 0,
            "distance" => 0,
            "covered_days" => new Set(),
        ];

        foreach ($loansAsOwner as $loan) {
            $ownerInvoice = $invoices[$loan->owner_invoice_id][0];
            $price = $ownerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanPrice
            );
            $expenses = $ownerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanExpenses
            );
            $net = $ownerInvoice->getUserBalanceChange();
            if (!$expenses && !$price && !$net) {
                continue;
            }

            if (!isset($linesPerLoanables[$loan->loanable_id])) {
                $linesPerLoanables[$loan->loanable_id] = [];
            }

            $linesPerLoanables[$loan->loanable_id][] = [
                "loan_id" => $loan->id,
                "loanable_id" => $loan->loanable_id,
                "payment_date" => $ownerInvoice->created_at->toDateString(),
                "price" => $price,
                "expenses" => $expenses,
                "net" => $net,
                "distance" => $loan->actual_distance,
            ];

            if (!isset($sumsPerLoanables[$loan->loanable_id])) {
                $sumsPerLoanables[$loan->loanable_id] = [
                    "price" => 0,
                    "expenses" => 0,
                    "net" => 0,
                    "distance" => 0,
                    "covered_days" => new Set(),
                    "name" => $loan->loanable->name,
                ];
            }

            $sumsPerLoanables[$loan->loanable_id]["price"] += $price;
            $globalSums["price"] += $price;
            $sumsPerLoanables[$loan->loanable_id]["expenses"] += $expenses;
            $globalSums["expenses"] += $expenses;
            $sumsPerLoanables[$loan->loanable_id]["net"] += $net;
            $globalSums["net"] += $net;
            $sumsPerLoanables[$loan->loanable_id]["distance"] +=
                $loan->actual_distance;
            $globalSums["distance"] += $loan->actual_distance;

            $day = $loan->departure_at->startOfDay();

            do {
                $sumsPerLoanables[$loan->loanable_id]["covered_days"]->add(
                    $day->toDateString()
                );
                $globalSums["covered_days"]->add($day->toDateString());
                $day = $day->addDay();
            } while ($day < $loan->actual_return_at);
        }

        // sort lines by date
        foreach ($linesPerLoanables as $loanableId => $linesPerLoanable) {
            $linesPerLoanables[$loanableId] = array_values(
                array_sort($linesPerLoanable, "payment_date")
            );
        }

        foreach ($sumsPerLoanables as $index => $sumsPerLoanable) {
            $sumsPerLoanables[$index]["covered_days"] = $sumsPerLoanable[
                "covered_days"
            ]->count();
        }
        $globalSums["covered_days"] = $globalSums["covered_days"]->count();

        // Extra invoices that are not linked to a loan but still have expenses or payments
        /** @var Invoice[] $extraInvoices */
        $extraInvoices = $invoiceQuery
            ->whereNotExists(
                fn($query) => $query
                    ->select(\DB::raw("1"))
                    ->from("loans")
                    ->whereColumn("loans.owner_invoice_id", "invoices.id")
                    ->orWhereColumn("loans.borrower_invoice_id", "invoices.id")
            )
            ->whereHas(
                "billItems",
                fn($query) => $query
                    ->where("item_type", BillItemTypes::loanExpenses)
                    ->orWhere("item_type", BillItemTypes::loanPrice)
            )
            ->get();

        $extraInvoiceLines = [];

        foreach ($extraInvoices as $invoice) {
            $price = $invoice->getUserBalanceChangeForType(
                BillItemTypes::loanPrice
            );
            $expenses = $invoice->getUserBalanceChangeForType(
                BillItemTypes::loanExpenses
            );
            $net = round($price + $expenses, 2);

            $extraInvoiceLines[] = [
                "invoice_id" => $invoice->id,
                "price" => $price,
                "expenses" => $expenses,
                "net" => $net,
            ];

            $globalSums["price"] += $price;
            $globalSums["expenses"] += $expenses;
            $globalSums["net"] += $net;
        }

        $this->data = [
            $linesPerLoanables,
            $sumsPerLoanables,
            $extraInvoiceLines,
            $globalSums,
        ];
        return $this->data;
    }
}
