<?php

namespace App\Reports;

use Fpdf\Fpdf;

class PdfReport extends Fpdf
{
    private string $font = "Helvetica";
    public function __construct()
    {
        parent::__construct(unit: "pt", size: "Letter");
        $this->fontMain();
        $this->SetMargins($this->margin(), $this->margin());
        $this->AddPage();
        $this->AliasNbPages();
    }

    protected function rem(float $rem = 1): float|int
    {
        return $this->FontSizePt * $rem;
    }

    protected function inch(float $inches = 1): float|int
    {
        return 72 * $inches;
    }

    protected function widthPercent(float $percent): float
    {
        return ((72 * 8.5 - $this->margin() * 2) * $percent) / 100.0;
    }

    protected function margin(): float|int
    {
        return $this->inch(0.75);
    }

    protected function tableMargin()
    {
        return $this->margin() + $this->rem(0.25);
    }

    public function Header(): void
    {
        $this->Image(
            public_path("logo_small.png"),
            $this->margin(),
            $this->margin() / 2 - $this->rem(1.5),
            $this->rem(15),
            h: $this->rem(3)
        );
        $title = $this->title();
        if ($title) {
            $this->fontSmall();
            $this->SetY($this->margin() / 2 - $this->rem(1.5));
            $this->print($title, lineHeight: 3, align: "R");
        }
        $this->SetXY($this->tableMargin(), $this->margin() - $this->rem(0.75));
        $this->SetFillColor(100);
        $this->Cell(0, 1, "", fill: true);
        $this->fontMain();
        $this->SetY($this->margin());
    }

    public function title()
    {
        return null;
    }

    public function Footer(): void
    {
        $this->SetY(
            $this->GetPageHeight() - ($this->margin() - $this->rem()) / 2
        );
        $this->SetFont($this->font, "", 8);
        $this->Cell(0, txt: $this->PageNo() . "/{nb}", align: "C");
    }

    private function parseWitdh(string $width)
    {
        if (str_ends_with($width, "%")) {
            return $this->widthPercent((float) str_replace("%", "", $width));
        }
        if (str_ends_with($width, "in")) {
            return $this->inch((float) str_replace("in", "", $width));
        }
        if (str_ends_with($width, "rem")) {
            return $this->rem((float) str_replace("in", "", $width));
        }
        return (float) $width;
    }

    protected function fontH1()
    {
        $this->SetFont($this->font, "B", 16);
    }

    protected function fontH2()
    {
        $this->SetFont($this->font, "", 15);
    }

    protected function fontMain()
    {
        $this->SetFont($this->font, "", 11);
    }

    protected function fontSmall()
    {
        $this->SetFont($this->font, "", 10);
    }

    protected function weightBold()
    {
        $this->SetFont($this->FontFamily, "B", $this->FontSizePt);
    }

    protected function weightRegular()
    {
        $this->SetFont($this->FontFamily, "", $this->FontSizePt);
    }

    protected function print(
        $text,
        $width = 0,
        $lineHeight = 1,
        $border = 0,
        $align = "",
        $fill = false,
        $link = "",
        $bold = false
    ): void {
        $currentFontStyle = $this->FontStyle;
        if ($bold) {
            $this->weightBold();
        }
        $this->Cell(
            $width,
            $this->rem($lineHeight),
            mb_convert_encoding($text, "ISO-8859-1"),
            border: $border,
            align: $align,
            fill: $fill,
            link: $link
        );
        if ($bold) {
            $this->SetFont(
                $this->FontFamily,
                $currentFontStyle,
                $this->FontSizePt
            );
        }
    }

    protected function printLn(
        $text,
        $width = 0,
        $lineHeight = 1,
        $border = 0,
        $align = "",
        $fill = false,
        $link = "",
        $bold = false
    ): void {
        $this->print(
            $text,
            $width,
            $lineHeight,
            $border,
            $align,
            $fill,
            $link,
            $bold
        );
        $this->Ln();
    }

    protected function h1(
        $text,
        $width = 0,
        $lineHeight = 1.5,
        $border = 0,
        $align = "",
        $fill = false,
        $link = "",
        $bold = true
    ) {
        $fontStyle = $this->FontStyle;
        $fontSize = $this->FontSizePt;

        $this->fontH1();
        $this->printLn(
            $text,
            $width,
            $lineHeight,
            $border,
            $align,
            $fill,
            $link,
            $bold
        );
        $this->Ln($this->rem(1));

        $this->SetFont($this->FontFamily, $fontStyle, $fontSize);
    }

    protected function h2(
        $text,
        $width = 0,
        $lineHeight = 2,
        $border = 0,
        $align = "",
        $fill = false,
        $link = "",
        $bold = false
    ) {
        $fontStyle = $this->FontStyle;
        $fontSize = $this->FontSizePt;

        $this->fontH2();
        $this->printLn(
            $text,
            $width,
            $lineHeight,
            $border,
            $align,
            $fill,
            $link,
            $bold
        );

        $this->SetFont($this->FontFamily, $fontStyle, $fontSize);
    }

    protected function table($columns, $data, $dataFont = "courier")
    {
        $parsedWidths = [];
        $missingColumnWidths = 0;
        foreach ($columns as $index => $column) {
            if (isset($column["width"])) {
                $parsedWidths[$index] = $this->parseWitdh($column["width"]);
            } else {
                $parsedWidths[$index] = 0;
                $missingColumnWidths++;
            }
        }

        if ($missingColumnWidths > 0) {
            $sumWidths = array_sum($parsedWidths);
            $remainingWidth = 72 * 8.5 - $this->tableMargin() * 2 - $sumWidths;
            $remaingColWidth = $remainingWidth / $missingColumnWidths;
            foreach ($parsedWidths as $index => $parsedWidth) {
                if ($parsedWidth == 0) {
                    $parsedWidths[$index] = $remaingColWidth;
                }
            }
        }

        $this->SetDrawColor(240);
        $this->SetFillColor(100);
        $this->SetTextColor(240);
        // Header
        $borderWidth = 0.25;
        // We do not have left or right borders for the header, but we want it to fit
        // with the content which does. So we slightly increase the size of the first and last
        // header columns to compensate.
        $this->setX($this->tableMargin() - $borderWidth);
        foreach ($columns as $index => $column) {
            $isFirstOrLast = $index == 0 || $index === count($columns) - 1;
            $this->print(
                $column["header"],
                width: $parsedWidths[$index] +
                    ($isFirstOrLast ? $borderWidth : 0),
                lineHeight: 1.5,
                border: $index === 0 ? "B" : "LB",
                align: $column["align"] ?? "L",
                fill: 1
            );
        }
        $this->Ln();

        $this->SetLeftMargin($this->tableMargin());

        // Data
        $previousFont = $this->FontFamily;
        $this->SetDrawColor(150);
        $this->SetFillColor(230);
        $this->SetTextColor(20);
        $this->SetFont($dataFont, "", $this->FontSizePt);
        foreach ($data as $rowIndex => $row) {
            foreach ($row as $index => $col) {
                $this->print(
                    $col,
                    width: $parsedWidths[$index],
                    lineHeight: 1.5,
                    border: $rowIndex == count($data) - 1 ? "LRB" : "LR",
                    align: $columns[$index]["align"] ?? "L",
                    fill: $rowIndex % 2 == 0 ? 0 : 1
                );
            }
            $this->Ln();
        }
        $this->SetFont($previousFont, $this->FontStyle, $this->FontSizePt);
        $this->SetLeftMargin($this->margin());
    }

    protected function cols($columns)
    {
        $columnCount = count($columns);
        $columnOffset = $this->margin();
        $initialY = $this->GetY();
        $maxY = $initialY;
        foreach ($columns as $column) {
            $this->SetY($initialY);
            $this->SetX($columnOffset);
            $this->SetLeftMargin($columnOffset);
            $column();
            $maxY = max($maxY, $this->GetY());

            $columnOffset += $this->widthPercent(100 / $columnCount);
        }

        $this->SetLeftMargin($this->margin());
        $this->SetXY($this->margin(), $maxY);
    }
}
