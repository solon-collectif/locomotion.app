<?php

namespace App\Facades;

use App\Filesystem\StorageTransactionHandler;
use Sentry\Laravel\Facade;

/**
 * This handler defers deleting files on transaction commit and deletes newly created files on
 * transaction rollback. Whenever files are referenced by the database, this facade should be used
 * for any create, update or delete operation. The Storage service can still be used for any
 * read-only operation (Storage::exists or Storage::files, for example).
 *
 * @mixin StorageTransactionHandler
 */
class StorageTransaction extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "storagetransaction";
    }
}
