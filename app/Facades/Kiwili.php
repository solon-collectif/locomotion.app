<?php

namespace App\Facades;

use App\Services\KiwiliService;
use Illuminate\Support\Facades\Facade;

/**
 * @see KiwiliService
 */
class Kiwili extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return "kiwili";
    }
}
