<?php

namespace App\Caches;

use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Http\Resources\LoanableResource;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Caches searchable loanables by type & community.
 */
class LoanablesByTypeAndCommunity
{
    // Cache for one day
    private static int $CACHE_DURATION = 3600 * 24;

    /**
     * Retrieves the cached loanables of the given type in the community from the
     * cache if present, otherise fetches from the DB and saves in the cache.
     *
     * @return array loanable_id->loanable pairs of available loanables of the given type
     *      in the community. @see self::format()
     */
    public static function get(int $communityId, LoanableTypes $type): array
    {
        return \Cache::remember(
            self::formatCacheName($communityId, $type),
            self::$CACHE_DURATION,
            fn() => self::doGet($communityId, $type)
        );
    }

    public static function doGet(int $communityId, LoanableTypes $type): array
    {
        // If this loanable type is not available in this community return early;
        if (
            !\DB::table("community_loanable_types")
                ->where("community_id", $communityId)
                ->join(
                    "loanable_type_details as details",
                    "loanable_type_id",
                    "details.id"
                )
                ->where("details.name", $type)
                ->exists()
        ) {
            return [];
        }

        // Formatting here in order to avoid saving information that is not necessary.
        return self::format(
            Loanable::with(["image", "mergedUserRoles.user.avatar", "details"])
                ->where("type", $type)
                ->where("published", true)
                ->whereHas(
                    "mergedUserRoles",
                    fn(Builder $userRole) => $userRole
                        ->where("role", LoanableUserRoles::Owner)
                        ->whereHas(
                            "user.approvedCommunities",
                            fn(Builder $communityUser) => $communityUser->where(
                                "community_id",
                                $communityId
                            )
                        )
                )
                ->where(
                    fn(Builder $loanable) => $loanable
                        ->whereNull("library_id")
                        ->orWhereHas(
                            "library.communities",
                            fn(Builder $community) => $community->where(
                                "communities.id",
                                $communityId
                            )
                        )
                )
                ->hasAvailabilities()
                ->get()
        );
    }

    public static function forget(int $communityId, LoanableTypes $type): bool
    {
        return \Cache::forget(self::formatCacheName($communityId, $type));
    }

    public static function forgetForCommunity(int $communityId)
    {
        foreach (LoanableTypes::possibleTypes as $loanableType) {
            \Cache::forget(self::formatCacheName($communityId, $loanableType));
        }
    }

    public static function forgetForUser(User $user): void
    {
        foreach ($user->mergedLoanableRoles as $userLoanableRole) {
            if (
                $userLoanableRole->loanable &&
                in_array($userLoanableRole->role, [
                    LoanableUserRoles::Owner,
                    LoanableUserRoles::Coowner,
                ]) &&
                $userLoanableRole->show_as_contact
            ) {
                self::forgetForLoanable($userLoanableRole->loanable);
            }
        }
    }

    public static function forgetForLoanable(Loanable $loanable): void
    {
        // In case the loanable was just created
        if (!$loanable->owner_user) {
            return;
        }

        foreach ($loanable->community_ids as $loanableCommunityIds) {
            self::forget($loanableCommunityIds, $loanable->type);
        }
    }

    public static function forgetForCommunityUser(
        CommunityUser $communityUser
    ): void {
        $loanablesWithAvailabilities = $communityUser->user
            ->loanablesAsOwner()
            ->hasAvailabilities();

        if (!$loanablesWithAvailabilities->exists()) {
            return;
        }

        foreach (
            $loanablesWithAvailabilities->pluck("type")->unique()
            as $type
        ) {
            self::forget($communityUser->community_id, $type);
        }
    }

    private static function formatCacheName(
        int $communityId,
        LoanableTypes|string $type
    ): string {
        if ($type instanceof LoanableTypes) {
            $type = $type->value;
        }

        return "loanables_{$type}_$communityId";
    }

    /**
     * Formats a collection of loanables into an array with loanable_id keys and
     * @see LoanableResource array values.
     */
    public static function format($loanables): array
    {
        $loanables = self::resolveChildren(
            LoanableResource::collection($loanables)
        );

        // Transform into id->loanable pairs
        return array_combine(
            array_map(fn($loanable) => $loanable["id"], $loanables),
            $loanables
        );
    }

    private static function resolveChildren(JsonResource|array $resource): array
    {
        if (is_a($resource, ResourceCollection::class)) {
            return array_map(self::resolveChildren(...), $resource->resolve());
        }

        if (is_a($resource, JsonResource::class)) {
            $data = $resource->resolve();
        } else {
            $data = $resource;
        }

        foreach ($data as $key => $value) {
            if (is_a($value, JsonResource::class)) {
                $data[$key] = self::resolveChildren($value);
            }
        }
        return $data;
    }
}
