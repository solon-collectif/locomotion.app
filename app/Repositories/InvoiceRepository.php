<?php

namespace App\Repositories;

use App\Models\Invoice;

class InvoiceRepository extends RestRepository
{
    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }

    public function create($data, $saveRelations = true)
    {
        $this->model->fill($data);

        if (array_key_exists("user_id", $data)) {
            $this->model->user_id = $data["user_id"];
        }

        if ($saveRelations) {
            static::saveItemAndRelations($this->model, $data);
        } else {
            $this->model->save();
        }

        return $this->model;
    }
}
