<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Str;

class RestRepository
{
    protected $model;

    /**
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public static function for($model): RestRepository
    {
        return new RestRepository($model);
    }

    public function create($data, $saveRelations = true)
    {
        $this->model->fill($data);

        if ($saveRelations) {
            self::saveItemAndRelations($this->model, $data);
        } else {
            $this->model->save();
        }

        $model = $this->model;

        $className = get_class($this->model);
        $this->model = new $className();

        return $model;
    }

    public function update($request, $id, $data)
    {
        $query = $this->model;

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);
        $this->model->fill($data);
        static::saveItemAndRelations($this->model, $data);

        $className = get_class($this->model);
        $this->model = new $className();

        return $this->model->find($id);
    }

    public static function saveItemAndRelations($model, $relationData)
    {
        $model->save();
        // We sync the original here, so that checks for $model->wasChanged are false
        // on the second save.
        $model->syncOriginal();

        static::saveRelations($model, $relationData);

        $model->save();
    }

    public function destroy($request, $id)
    {
        $query = $this->model;

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->delete();

        return $this->model;
    }

    public function restore($request, $id)
    {
        $query = $this->model->withTrashed();

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->restore();

        return $this->model;
    }

    public static function saveRelations($model, $data)
    {
        static::saveItems($model, $data);
        static::saveCollections($model, $data);
    }

    protected static function saveItems($model, $data)
    {
        $skipObjectRelation = [];
        foreach (
            array_diff(array_keys($data), $model->getFillable())
            as $field
        ) {
            if ($field === "id") {
                continue;
            }

            if (preg_match('/_id$/', $field)) {
                $relationName = str_replace("_id", "", $field);

                if (in_array($relationName, $skipObjectRelation)) {
                    continue;
                }

                if (in_array($relationName, $model->items)) {
                    $newAssoc = $data[$field];

                    $skipObjectRelation[] = $relationName;

                    $camelRelationName = Str::camel($relationName);
                    $relation = $model->{$camelRelationName}();

                    if (is_a($relation, BelongsTo::class)) {
                        if ($newAssoc) {
                            $relation->associate($data[$field]);
                        } else {
                            $relation->dissociate();
                        }
                    } else {
                        if ($newAssoc) {
                            $newData = ["id" => $newAssoc];
                            static::saveRelatedItem(
                                $model,
                                $relationName,
                                $newData
                            );
                        } else {
                            static::deleteRelatedItem($model, $relationName);
                        }
                    }
                }
            } elseif (
                in_array($field, $model->items) &&
                is_array($data[$field])
            ) {
                $camelField = Str::camel($field);
                $related = static::saveRelatedItem(
                    $model,
                    $field,
                    $data[$field]
                );
                $relation = $model->{$camelField}();

                if (
                    $model->{$camelField} &&
                    $model->{$camelField}->id !== $related->id
                ) {
                    if (is_a($relation, HasOne::class)) {
                        $model->{$camelField}[
                            $model->{$camelField}()->getForeignKeyName()
                        ] = null;
                        $model->{$camelField}->save();
                    } else {
                        $relation->dissociate();
                    }
                }

                foreach (array_keys($related->morphOnes) as $morphOne) {
                    static::savePolymorphicRelation(
                        $related,
                        $morphOne,
                        $data[$field]
                    );
                }

                foreach (array_keys($related->morphManys) as $morphMany) {
                    static::saveMorphManyRelation(
                        $related,
                        $morphMany,
                        $data[$field]
                    );
                }

                if (is_a($relation, HasOne::class)) {
                    $related->{$relation->getForeignKeyName()} = $model->id;
                    $related->save();
                }

                if (is_a($relation, BelongsTo::class)) {
                    $relation->associate($related);
                }
            } elseif (
                in_array($field, array_keys($model->morphOnes)) &&
                array_key_exists($field, $data)
            ) {
                if (is_array($data[$field]) && !!$data[$field]) {
                    static::savePolymorphicRelation($model, $field, $data);
                } else {
                    static::deletePolymorphicRelation($model, $field);
                }
            }
        }
    }

    protected static function saveCollections($model, $data)
    {
        foreach (
            array_diff(array_keys($data), $model->getFillable())
            as $field
        ) {
            if (
                $field === "id" ||
                !array_key_exists($field, $data) ||
                !is_array($data[$field]) ||
                !in_array(
                    $field,
                    array_merge(
                        $model->collections,
                        array_keys($model->morphManys)
                    )
                )
            ) {
                continue;
            }

            $relation = $model->{Str::camel($field)}();

            if (is_a($relation, HasManyThrough::class)) {
                continue;
            }

            $ids = [];

            if (method_exists($relation, "getPivotClass")) {
                $pivotClass = $relation->getPivotClass();
                $pivot = new $pivotClass();
                $pivotAttributes = $pivot->getFillable();

                $isExtendedPivot = static::isBaseModel($pivot);
                if ($isExtendedPivot) {
                    $pivotItems = array_merge(
                        $pivot->items,
                        array_keys($pivot->morphOnes)
                    );
                    $pivotCollections = array_merge(
                        $pivot->collections,
                        array_keys($pivot->morphManys)
                    );
                } else {
                    $pivotItems = [];
                    $pivotCollections = [];
                }

                foreach ($data[$field] as $element) {
                    $pivotData = [];
                    $pivotItemData = [];

                    foreach ($pivotAttributes as $pivotAttribute) {
                        if (!array_key_exists($pivotAttribute, $element)) {
                            continue;
                        }

                        $pivotData[$pivotAttribute] = $element[$pivotAttribute];
                        unset($element[$pivotAttribute]);
                    }

                    foreach ($pivotItems as $pivotItem) {
                        if (!array_key_exists($pivotItem, $element)) {
                            continue;
                        }

                        $pivotItemData[$pivotItem] = $element[$pivotItem];
                        unset($element[$pivotItem]);
                    }

                    if (array_key_exists("id", $element) && $element["id"]) {
                        $sync = [];
                        $sync[$element["id"]] = $pivotData;
                        $relation->syncWithoutDetaching($sync);

                        $targetPivot = $model
                            ->{Str::camel($field)}()
                            ->find($element["id"])->pivot;

                        foreach ($pivotItems as $pivotItem) {
                            static::savePolymorphicRelation(
                                $targetPivot,
                                $pivotItem,
                                $pivotItemData
                            );
                        }

                        foreach ($pivotCollections as $pivotCollection) {
                            if (isset($element[$pivotCollection])) {
                                static::saveMorphManyRelation(
                                    $targetPivot,
                                    $pivotCollection,
                                    $element
                                );
                            }
                        }

                        $ids[] = $element["id"];
                    }
                }
            } else {
                foreach ($data[$field] as $element) {
                    if (array_key_exists("id", $element) && $element["id"]) {
                        $ids[] = $element["id"];
                    }
                }
            }

            $ids = array_values(array_unique($ids));

            $relatedClass = $relation->getRelated();

            if ($relatedClass->readOnly) {
                continue;
            }

            if (is_a($relation, MorphMany::class)) {
                static::saveMorphManyRelation($model, $field, $data);
            } elseif (is_a($relation, HasMany::class)) {
                $newItems = [];
                foreach ($data[$field] as $element) {
                    if (array_key_exists("id", $element) && $element["id"]) {
                        $existingItem = $relatedClass->find($element["id"]);
                        $existingItem->fill($element);
                        $newItems[] = $existingItem;
                    } else {
                        $newItem = new $relatedClass();
                        $newItem->fill($element);
                        $newItems[] = $newItem;
                    }
                }

                $existingIds = $relation
                    ->get()
                    ->pluck("id")
                    ->toArray();
                $removedIds = array_diff($existingIds, $ids);

                if (!empty($removedIds)) {
                    $relation
                        ->getRelated()
                        ->whereIn("id", $removedIds)
                        ->delete();
                }

                $relation->saveMany($newItems);
            } else {
                $relation->sync($ids);
            }
        }
    }

    protected static function deletePolymorphicRelation(&$model, $field)
    {
        if ($currentRelation = $model->{$field}()->first()) {
            $currentRelation->delete();
        }

        return $currentRelation;
    }

    protected static function saveMorphManyRelation(&$model, $field, &$data)
    {
        if (!array_key_exists($field, $data)) {
            // No data for the current relationship
            return;
        }

        $relation = $model->{Str::camel($field)}();

        $relatedModel = $relation->getRelated();

        $ids = [];
        $newItems = [];
        foreach ($data[$field] as $element) {
            if (array_key_exists("id", $element) && $element["id"]) {
                $existingItem = $relatedModel->find($element["id"]);
                $existingItem->fill($element);
                $newItems[] = $existingItem;
                $ids[] = $element["id"];
            } else {
                $newItem = new $relatedModel();
                $newItem->fill($element);
                $newItems[] = $newItem;
            }
        }

        $itemsToDelete = $relation->whereNotIn("id", $ids)->get();

        foreach ($itemsToDelete as $itemToDelete) {
            $itemToDelete->delete();
        }

        $relation->saveMany($newItems);
    }

    protected static function savePolymorphicRelation(&$model, $field, &$data)
    {
        if (!array_key_exists($field, $data)) {
            return $model->{$field};
        }

        if (!$data[$field]) {
            if ($model->{$field}) {
                $model->{$field}->delete();
            }

            return null;
        }

        $relation = $model->{$field}();
        $related = $relation->getRelated();
        $existingRelation = $relation->getResults();

        $modelToSave = $related->newInstance();

        if (isset($data[$field]["id"])) {
            $id = $data[$field]["id"];
            $modelToSave = $related->findOrFail($id);
            if ($existingRelation && $existingRelation->id !== $id) {
                //delete existing relation.
                $existingRelation->delete();
            }
        }

        $modelToSave->fill($data[$field])->save();
        $relation->save($modelToSave);

        return $related;
    }

    protected static function deleteRelatedItem(&$model, $field)
    {
        $relation = $model->{$field}();
        $related = $model->{$field};

        if (!$related) {
            return $related;
        }

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = null;
        }

        $related->save();

        return $related;
    }

    protected static function saveRelatedItem(&$model, $field, &$data)
    {
        $camelField = Str::camel($field);
        $relation = $model->{$camelField}();

        if (isset($data["id"])) {
            // if the ID is provided, we want to update and associate an existing model.
            $related = $relation->getRelated()->find($data["id"]);
        } else {
            // if not, we update the existing item.
            $related = $relation->getResults();
        }

        // If the existing model is not found, in either case we create a new one
        if (!$related) {
            $related = $relation->getRelated()->newInstance();

            if (is_a($relation, HasOne::class)) {
                $related->{$relation->getForeignKeyName()} = $model->id;
            } elseif (is_a($relation, BelongsTo::class)) {
                // We cannot use $relation->associate($related) since that
                // would set the $model's owner key based on the related's foreign key.
                // Essentially the inverse of what we would like, since our model does exist
                // and the related is new.
                $related->{$relation->getForeignKeyName()} =
                    $model->{$relation->getOwnerKeyName()};
            }
        }

        if ($related->readOnly) {
            return $related;
        }

        $relatedData = array_intersect_key(
            $data,
            array_flip($related->getFillable())
        );

        $related->fill($relatedData);
        $related->save();

        return $related;
    }

    protected static function isBaseModel($class, $autoload = true)
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return in_array("App\Models\BaseModelTrait", $traits);
    }
}
