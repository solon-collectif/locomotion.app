<?php

namespace App\Repositories;

use App\Models\Loanable;

class LoanableRepository extends RestRepository
{
    public function __construct(Loanable $model)
    {
        $this->model = $model;
    }
}
