<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\CommonMark\Node\Block\BlockQuote;
use League\CommonMark\Extension\CommonMark\Node\Block\Heading;
use League\CommonMark\Extension\CommonMark\Node\Inline\Image;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;
use League\CommonMark\Extension\DefaultAttributes\DefaultAttributesExtension;
use League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use League\CommonMark\Extension\Table\Table;
use League\CommonMark\MarkdownConverter;
use League\CommonMark\Node\Block\Paragraph;

class MarkdownContent extends Component
{
    public string $generatedHtml;
    public function __construct(
        public string $markdown,
        public string $style = ""
    ) {
        // Define your configuration, if needed
        $config = [];

        // Configure the Environment with all the CommonMark and GFM parsers/renderers
        $environment = new Environment([
            "html_input" => "strip",
            "allow_unsafe_links" => false,
            "autolink" => [
                "allowed_protocols" => ["https", "http", "mailto"],
            ],
            "renderer" => [
                "soft_break" => "\n<br />\n",
            ],
            "default_attributes" => [
                Heading::class => [
                    "style" => static fn(Heading $node) => match (
                    $node->getLevel()
                    ) {
                        1
                            => "margin-top: 0;margin-bottom: 8px;line-height: 1.5;font-size: 24px;font-weight: 400;",
                        2
                            => "margin-top: 0;margin-bottom: 8px;line-height: 1.5;font-size: 20px;font-weight: 600;",
                        3
                            => "margin-top: 0;margin-bottom: 8px;line-height: 1.5;font-size: 20px;font-weight: 500;",
                        4
                            => "margin-top: 0;margin-bottom: 8px;line-height: 1.5;font-size: 16px;font-weight: 600;",
                        5,
                        6
                            => "margin-top: 0;margin-bottom: 8px;line-height: 1.5;font-size: 16px;font-weight: 500;",
                        default => "",
                    },
                ],
                Image::class => [
                    "style" => "max-width: 200px",
                    "width" => "200px",
                ],
                Link::class => [
                    "target" => "_blank",
                    "rel" => "noopener noreferrer nofollow",
                    "style" =>
                        "color: #16a59e;background-color: transparent;text-decoration: underline;",
                ],
                BlockQuote::class => [
                    "style" =>
                        "margin-left: 8px;padding: 4px 0 4px 8px;border-left: 3px solid rgba(0, 0, 0, 0.1);",
                ],
                Paragraph::class => [
                    "style" => "line-height: 1.5;margin-top:0;",
                ],
                Table::class => [
                    "style" => "width: 100%",
                ],
            ],
        ]);
        $environment->addExtension(new CommonMarkCoreExtension());
        $environment->addExtension(new DefaultAttributesExtension());
        $environment->addExtension(new GithubFlavoredMarkdownExtension());

        $converter = new MarkdownConverter($environment);
        $this->generatedHtml = $converter->convert($this->markdown);
    }

    public function render(): View
    {
        return view("components.markdown-content");
    }
}
