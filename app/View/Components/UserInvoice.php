<?php

namespace App\View\Components;

use App\Models\Invoice;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class UserInvoice extends Component
{
    public User $user;
    public array $formattedAddress;

    public function __construct(public Invoice $invoice)
    {
        $this->user = $invoice->user;
        $this->formattedAddress = explode(",", $this->user->address);
    }

    public function render(): View
    {
        return view("components.user-invoice");
    }
}
