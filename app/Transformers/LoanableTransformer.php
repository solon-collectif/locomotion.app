<?php

namespace App\Transformers;

use App\Http\Resources\ArchivedLoanableResource;
use App\Models\Loan;
use App\Models\Loanable;
use Auth;

/**
 * @extends  Transformer<Loanable>
 */
class LoanableTransformer extends Transformer
{
    public function transform($options = [])
    {
        $user = Auth::user();
        if ($user) {
            /** @var Loan $loan */
            $loan = $this->getFirstAncestor(Loan::class);
            $this->item->handleInstructionVisibility($user, $loan);
        }

        $output = parent::transform($options);

        if ($this->item->trashed()) {
            return new ArchivedLoanableResource($this->item);
        }

        return $output;
    }
}
