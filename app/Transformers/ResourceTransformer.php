<?php

namespace App\Transformers;

use App\Models\AuthenticatableBaseModel;
use App\Models\BaseModel;
use App\Models\Pivots\BasePivot;

/**
 * Generic Transformer bridging the gap between transformers and ressource. This transformer
 * will simply use the given ressource to format the model.
 *
 * @template T of BaseModel|AuthenticatableBaseModel|BasePivot
 * @extends Transformer<T>
 */
class ResourceTransformer extends Transformer
{
    private string $resourceClass;

    /**
     * @param string $resourceClass
     * @param T $item
     * @param Transformer|null $parent
     */
    public function __construct(
        string $resourceClass,
        BaseModel|AuthenticatableBaseModel|BasePivot $item,
        ?Transformer $parent = null
    ) {
        $this->resourceClass = $resourceClass;
        parent::__construct($item, $parent);
    }

    public function transform($options = [])
    {
        return new $this->resourceClass($this->item);
    }
}
