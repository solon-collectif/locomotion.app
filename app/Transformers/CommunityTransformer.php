<?php

namespace App\Transformers;

use App\Http\Resources\ArchivedCommunityResource;
use App\Models\Community;
use App\Models\User;
use Auth;

/**
 * @extends Transformer<Community>
 */
class CommunityTransformer extends Transformer
{
    protected array $pivots = [User::class];

    public function transform($options = [])
    {
        if ($this->item->trashed()) {
            return new ArchivedCommunityResource($this->item);
        }

        $output = parent::transform($options);

        $user = Auth::user();
        if ($user && $user->isAdmin()) {
            return $output;
        }

        if (!isset($output["users"])) {
            return $output;
        }

        if (!$user) {
            unset($output["users"]);
        }

        if (!$user->isAdminOfCommunity($this->item->id)) {
            unset($output["users"]);
        }

        return $output;
    }
}
