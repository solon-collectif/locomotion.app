<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\BaseRequest;
use App\Models\User;

class CreateRequest extends BaseRequest
{
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    public function rules()
    {
        $userId = $this->input("user_id");
        /** @var User $user */
        $user = User::findOrFail($userId);
        $billItems = $this->input("bill_items");

        return [
            "user_id" => "required",
            "period" => "required",
            "bill_items" => ["array", "min:1"],
        ];
    }

    public function attributes()
    {
        return [
            "period" => "Titre",
        ];
    }
}
