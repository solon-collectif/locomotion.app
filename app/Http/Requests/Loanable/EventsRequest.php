<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;
use App\Rules\OrderRule;

class EventsRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            "start" => ["date", "required"],
            "end" => ["date", "required"],
            "order" => [new OrderRule()],
        ];

        return $rules;
    }
}
