<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;
use App\Models\Loanable;
use Illuminate\Validation\Rule;

class AvailabilityRequest extends BaseRequest
{
    public function authorize()
    {
        $user = $this->user();
        if (!$user) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        // Is requested loanable accessible by user?
        if (
            Loanable::query()
                ->accessibleBy($user)
                ->find($this->loanable->id)
        ) {
            return true;
        }

        return false;
    }

    public function rules()
    {
        return [
            "start" => ["date", "required"],
            "end" => ["date", "required"],
            "responseMode" => [
                "required",
                Rule::in(["available", "unavailable"]),
            ],
        ];
    }
}
