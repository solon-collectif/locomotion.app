<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;
use App\Models\User;

class CreateLoanableRoleRequest extends BaseRequest
{
    public function rules(): array
    {
        $loanable = $this->route("loanable");

        $usersSharingCommunity = User::query()
            ->whereHas(
                "approvedCommunities",
                fn($q) => $q->whereIn(
                    "communities.id",
                    $loanable->community_ids
                )
            )
            ->pluck("users.id")
            ->join(",");

        return [
            "user_id" => ["required", "in:$usersSharingCommunity"],
        ];
    }
}
