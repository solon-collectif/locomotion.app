<?php

namespace App\Http\Requests\Loanable;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            "type" => ["required", "in:bike,car,trailer"],
            "comments" => "present",
            "instructions" => "present",
            "return_instructions" => "nullable",
            "location_description" => "present",
            "name" => "required",
            "position" => "",
            "owner_user.id" => "required",
            "details" => "",
            "shared_publicly" => "boolean",
            "sharing_mode" => ["in:on_demand,self_service,hybrid"],
        ];

        return $rules;
    }
}
