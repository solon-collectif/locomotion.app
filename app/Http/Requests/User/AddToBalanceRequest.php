<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

class AddToBalanceRequest extends BaseRequest
{
    public function authorize()
    {
        $user = $this->user();

        if (!$user) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        $routeUserId = $this->route("user")?->id ?: $this->get("user_id");

        return $user->id === $routeUserId &&
            $user->approvedCommunities->count() > 0;
    }

    public function rules()
    {
        $user = $this->user();

        $rules = [
            "amount" => ["numeric", "required"],
        ];

        return $rules;
    }
}
