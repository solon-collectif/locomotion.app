<?php

namespace App\Http\Requests;

class ParseFieldsHelper
{
    public static function joinFieldsTree(
        array $fields,
        $prefix = "",
        &$output = []
    ) {
        foreach ($fields as $field => $rest) {
            if (is_array($rest)) {
                self::joinFieldsTree(
                    $rest,
                    implode(".", array_filter([$prefix, $field])),
                    $output
                );
            } else {
                $output[] = implode(".", array_filter([$prefix, $field]));
            }
        }

        return $output;
    }

    public static function parseFieldList(array|string $fieldString)
    {
        return self::parseFields(self::splitFields($fieldString));
    }

    public static function splitFields(array|string $fields)
    {
        if (is_array($fields)) {
            $parts = $fields;
        } else {
            $parts = array_map("trim", explode(",", $fields));
        }
        return array_map(function ($f) {
            return array_map("trim", explode(".", $f, 2));
        }, $parts);
    }

    public static function parseFields($fields, $acc = [])
    {
        return array_reduce(
            $fields,
            function ($acc, $t) {
                switch (count($t)) {
                    case 2:
                        if (!isset($acc[$t[0]])) {
                            $acc[$t[0]] = [];
                        }

                        if (str_contains($t[1], ".")) {
                            $st = self::splitFields($t[1]);
                            $acc[$t[0]] = self::parseFields($st, $acc[$t[0]]);
                        } else {
                            if (is_string($acc[$t[0]])) {
                                $str = $acc[$t[0]];
                                $acc[$t[0]] = [];
                                $acc[$t[0]][$str] = $str;
                            }
                            $acc[$t[0]][$t[1]] = $t[1];
                        }
                        break;
                    case 1:
                        $acc[$t[0]] = $t[0];
                        break;
                    default:
                        $acc[] = $t;
                        break;
                }
                return $acc;
            },
            $acc
        );
    }
}
