<?php

namespace App\Http\Requests\Loan;

use App\Http\Requests\BaseRequest;
use App\Models\Community;
use App\Models\Loanable;

class CreateRequest extends BaseRequest
{
    public function rules()
    {
        $user = $this->user();
        $accessibleCommunityIds = implode(
            ",",
            Community::query()
                ->accessibleBy($user)
                ->for("loan", $user)
                ->pluck("id")
                ->toArray()
        );
        $accessibleLoanableIds = implode(
            ",",
            Loanable::query()
                ->accessibleBy($user)
                ->pluck("id")
                ->toArray()
        );

        return [
            "loanable_id" => [
                "numeric",
                "required",
                "in:$accessibleLoanableIds",
            ],
            "community_id" => [
                "numeric",
                "filled",
                "in:$accessibleCommunityIds",
            ],
            "borrower_user_id" => ["required", "int"],
            "departure_at" => ["required", "date"],
            "duration_in_minutes" => ["integer", "required", "min:15"],
            "estimated_distance" => ["integer", "required"],
            "alternative_to" => ["string", "required"],
            "alternative_to_other" => ["string", "nullable"],
            "message_for_owner" => ["string", "nullable", "max:1024"],
        ];
    }

    public function messages()
    {
        return [
            "community_id.in" => "Vous n'avez pas accès à cette communauté.",
        ];
    }
}
