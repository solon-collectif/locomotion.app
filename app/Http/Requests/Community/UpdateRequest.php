<?php

namespace App\Http\Requests\Community;

use App\Http\Requests\BaseRequest;

class UpdateRequest extends BaseRequest
{
    public function authorize()
    {
        $community = $this->route("community");
        return $this->user()->isAdmin() ||
            $this->user()->isAdminOfCommunity($community->id);
    }
}
