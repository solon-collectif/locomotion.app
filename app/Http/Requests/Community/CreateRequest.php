<?php

namespace App\Http\Requests\Community;

use App\Http\Requests\BaseRequest;

class CreateRequest extends BaseRequest
{
    public function authorize()
    {
        return $this->user()->isAdmin();
    }
}
