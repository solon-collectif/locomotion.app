<?php
namespace App\Http;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\MessageBag;

class ErrorResponse
{
    protected static $errorMessages = [
        422 => "Validation errors",
        404 => "Not found",
        400 => "Bad request",
    ];

    public static function withMessage($message = null, $status = 400)
    {
        return response()->json(
            [
                "message" =>
                    $message ?:
                    array_get(static::$errorMessages, $status, "Error"),
            ],
            $status
        );
    }

    /**
     * Returns an HTTP error data formatted like this:
     *
     *  {
     *      "message" : "Some error message",
     *      "errors"  : [
     *          "Some error name" => [
     *              "Some error description 1",
     *              "Some error description 2"
     *          ],
     *          "Other error name" => [
     *              "Some other error description"
     *          ]
     *      ]
     *  }
     *
     * @param MessageBag|array $errors
     * @param string|null $message
     * @param int $code
     * @return JsonResponse
     */
    public static function withErrors(
        MessageBag|array $errors,
        string $message = null,
        int $code = 422
    ): JsonResponse {
        return response()->json(
            [
                "message" => $message ?: static::$errorMessages[$code],
                "errors" => $errors,
            ],
            $code
        );
    }

    public static function withError(string $error, int $code): JsonResponse
    {
        return static::withErrors(new MessageBag([$error]), null, $code);
    }
}
