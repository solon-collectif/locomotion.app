<?php

namespace App\Http;

use App\Http\Requests\BaseRequest;
use App\Models\AuthenticatableBaseModel;
use App\Models\BaseModel;
use App\Models\Pivots\BasePivot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Helper class to transform requests into eloquent query {@see Builder} handling filtering,
 * ordering and eager loading of relations. This could be replaced by something like
 * spatie/laravel-query-builder down the line.
 */
class WebQueryBuilder
{
    protected static array $reservedParams = [
        "!fields",
        "fields",
        "for",
        "order",
        "page",
        "per_page",
        "q",
        "export_filename",
    ];

    public static function parseRequestParams(BaseRequest $request): array
    {
        // $request->all replaces all '.' that we might want to keep with '_'
        $params =
            self::parseQueryString(
                array_get($request->server->all(), "QUERY_STRING")
            ) ?:
            $request->all();

        // Validate there aren't extra params in request->all() that we might have missed
        $paramKeys = array_keys($params);
        $underscoredParamKeys = array_map(
            fn($val) => str_replace(".", "_", $val),
            $paramKeys
        );
        $missedParams = array_diff_key(
            $request->all(),
            array_flip($underscoredParamKeys)
        );

        return array_merge($params, $missedParams);
    }

    /**
     * @template TModel of BaseModel|BasePivot|AuthenticatableBaseModel
     * @param BaseRequest $request
     * @param TModel $model
     * @return Builder<TModel>
     */
    public static function for(BaseRequest $request, $model): Builder
    {
        $fields = $request->getFields();
        $user = $request->user();
        return self::buildQuery(
            self::parseRequestParams($request),
            $user,
            $model,
            $fields
        );
    }

    public static function buildQuery(
        array $params,
        User $user,
        $model,
        array $fields = null
    ): Builder {
        /** @var Builder $query */
        $query = $model->newQuery();

        if (method_exists($model, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($user);
        }

        if (isset($params["for"]) && method_exists($model, "scopeFor")) {
            $query = $query->for($params["for"], $user);
        }

        if ($fields) {
            self::applyWithFromQuery($fields, $query, $model);
            $query->withCustomColumns(array_keys($fields));
        }
        // The following condition is a small improvement to an edge case where we both filter and order by
        // different custom columns which depend on a join. Filtering (which happens first) would wrap
        // the base query so that the custom columns could be filtered on, which would make the joined
        // table unaccessible when trying to add the orderBy. This line prevents an identical second join
        // from being added at the orderBy step, by selecting preemptively the required column into the
        // base query.
        if (isset($params["order"])) {
            $query->withCustomColumns(self::getOrderByFields($params["order"]));
        }

        $query = self::applyFilters(
            self::parseFilters($params),
            $model,
            $query,
            $user
        );

        if (isset($params["q"]) && method_exists($model, "scopeSearch")) {
            $query = $query->search($params["q"]);
        }

        if (isset($params["order"])) {
            $query = self::orderBy($query, $params["order"], $model);
        }

        return $query;
    }

    private static function applyWithFromQuery($fields, &$query, $model): void
    {
        self::addModelEagerLoads($model, $fields, $query);
    }

    private static function addModelEagerLoads(
        $currentModel,
        $fields,
        &$query,
        $prefix = null
    ): void {
        $relations = array_merge(
            $currentModel->items,
            $currentModel->collections,
            array_keys($currentModel->morphOnes),
            array_keys($currentModel->morphManys)
        );

        foreach ($fields as $field => $nestedFields) {
            if (str::endsWith($field, "_count")) {
                $field = substr($field, 0, -6);
                if (!in_array($field, $relations)) {
                    continue;
                }
                $childRelation = str::camel($field);
                $qualifiedChildRelation = $prefix
                    ? $prefix . "." . $childRelation
                    : $childRelation;
                $query->withCount($qualifiedChildRelation);
                continue;
            }

            if (!in_array($field, $relations)) {
                continue;
            }

            $childRelation = str::camel($field);
            $qualifiedChildRelation = $prefix
                ? $prefix . "." . $childRelation
                : $childRelation;

            $query->with($qualifiedChildRelation);

            if (!is_array($nestedFields)) {
                continue;
            }

            $childModel = $currentModel->{$childRelation}()->getRelated();
            self::addModelEagerLoads(
                $childModel,
                $nestedFields,
                $query,
                $qualifiedChildRelation
            );
        }
    }

    private static function getOrderByFields($param): array
    {
        return explode(",", str_replace("-", "", $param));
    }

    private static function orderBy($query, $def, $model)
    {
        if ($def === "") {
            return $query;
        }

        $columns = explode(",", $def);

        foreach ($columns as $column) {
            if (str_starts_with($column, "-")) {
                $direction = "desc";
                $column = substr($column, 1);
            } else {
                $direction = "asc";
            }

            if (str_contains($column, ".")) {
                $query = self::orderByRelation(
                    $query,
                    $column,
                    $direction,
                    $model
                );
            } elseif ($model->isCustomColumn($column)) {
                $query = $query
                    ->withCustomColumn($column)
                    ->orderBy($column, $direction);
            } else {
                $table = $model->getTable();
                $query = $query->orderBy("$table.$column", $direction);
            }
        }

        return $query;
    }

    private static function orderByRelation(
        Builder $query,
        $column,
        $direction,
        $model
    ): Builder {
        $currentModel = $model;
        $currentTable = $model->getTable();

        while (str_contains($column, ".")) {
            [$relation, $column] = explode(".", $column, 2);
            $relation = Str::camel($relation);
            [$currentTable, $currentModel] = $currentModel->addRelationshipJoin(
                $query,
                $relation,
                $currentTable
            );
        }

        if (!empty($query->getQuery()->groups)) {
            // If the query already has a group by, we  must also add the last table in this groupby
            $query->groupBy("{$currentTable}.{$currentModel->getKeyName()}");
        }

        // Make sure we only select relevant columns. By default, eloquent selects *, which may result in
        // conflicts during joins.
        $allColumns = "{$model->getTable()}.*";
        if (!in_array($allColumns, $query->getQuery()->columns ?: [])) {
            $query->addSelect($allColumns);
        }

        if ($currentModel->isCustomColumn($column)) {
            $currentModel->addCustomColumn($query, $column, $currentTable);
            return $query->orderBy($column, $direction);
        } else {
            // Make sure the order by column is selected as well in case distinct is used.
            $query->addSelect("$currentTable.$column");
            return $query->orderBy("$currentTable.$column", $direction);
        }
    }
    /**
     * Transforms an array of key-value filter params into a nested structure,
     * removing any reserved params.
     *
     * @see RestRepository::parseFilter()
     */
    private static function parseFilters(array $filterParams): array
    {
        $filters = [];
        foreach ($filterParams as $filterKey => $filterValue) {
            if (in_array($filterKey, self::$reservedParams)) {
                continue;
            }

            self::parseFilter($filterKey, $filterValue, $filters);
        }
        return $filters;
    }

    /**
     * Transforms a key-value filter param array into a nested array structure.
     * For example
     *
     * From:
     *
     * [
     *   "a.b.c.d" => 3,
     *   "a.b.c.e" => 4,
     *   "a.b.f.g" => 5,
     * ]
     *
     * to this:
     * [
     *   a => [
     *     b => [
     *       c => [
     *         d => 3,
     *         e => 4,
     *       ],
     *       f => [
     *         g => 5,
     *       ],
     *     ]
     *   ]
     * ]
     *
     * @param int|string $filterKey a possibly nested filter key (e.g. a.b.c)
     * @param mixed $filterValue the value for the filter
     * @param array $filters the mutable filter array. It is built through the recursive calls.
     */
    private static function parseFilter(
        int|string $filterKey,
        mixed $filterValue,
        array &$filters
    ): void {
        // Todo change this into a loop
        if (str_contains($filterKey, ".")) {
            [$filterKey, $rest] = explode(".", $filterKey, 2);
            if (!isset($filters[$filterKey])) {
                $filters[$filterKey] = [];
            }

            self::parseFilter($rest, $filterValue, $filters[$filterKey]);
        } else {
            $filters[$filterKey] = $filterValue;
        }
    }

    /**
     * Applies filters on the query. This is called recursively once for every
     * relation the filters depend on.
     *
     * @param array $filters nested array of filters, where child arrays
     *      indicate filtering on a relation's property.
     * @param mixed $model model currently being filtered.
     * @param Builder $query a query or subquery for the current model to apply
     *      the filter on
     * @param User $user the current users id
     * @return Builder
     */
    private static function applyFilters(
        array $filters,
        mixed $model,
        Builder &$query,
        User $user
    ): Builder {
        $relationFilters = [];
        $directFilter = [];
        $customColumnFilters = [];

        foreach ($filters as $filterKey => $filterValue) {
            if (is_array($filterValue)) {
                $relationFilters[$filterKey] = $filterValue;
            } elseif ($model->isCustomColumn($filterKey)) {
                $customColumnFilters[$filterKey] = $filterValue;
            } else {
                $directFilter[$filterKey] = $filterValue;
            }
        }

        // We recurse on every relation that is filtered
        foreach ($relationFilters as $filterKey => $filterValue) {
            $negative = $filterKey[0] === "!";
            $paramName = str_replace("!", "", $filterKey);

            $camelRelation = Str::camel($paramName);
            $targetRelation = $model->{$camelRelation}();
            $targetModel = $targetRelation->getRelated();

            if ($negative) {
                $query->whereDoesntHave(
                    $camelRelation,
                    fn(Builder $q) => self::applyFilters(
                        $filterValue,
                        $targetModel,
                        $q,
                        $user
                    )
                );
            } else {
                $query->whereHas(
                    $camelRelation,
                    fn($q) => self::applyFilters(
                        $filterValue,
                        $targetModel,
                        $q,
                        $user
                    )
                );
            }
        }

        // Add model's property filters
        foreach ($directFilter as $filterKey => $filterValue) {
            self::applyFilter($model, $filterKey, $filterValue, $query, $user);
        }

        if (!empty($customColumnFilters)) {
            $query->withCustomColumns(array_keys($customColumnFilters));
            // To add a where clause on an aliased column, we need to wrap the current query in a sub query.
            self::wrapQuery($query, $model);
            foreach ($customColumnFilters as $filterKey => $filterValue) {
                self::applyFilter(
                    $model,
                    $filterKey,
                    $filterValue,
                    $query,
                    $user
                );
            }
        }

        return $query;
    }

    private static function applyFilter(
        $model,
        $param,
        $value,
        Builder &$query,
        User $user
    ): Builder {
        $negative = $param[0] === "!";
        $paramName = str_replace("!", "", $param);

        $camelParamName = Str::camel($paramName);
        if (method_exists($model, "scope$camelParamName")) {
            return $query->{$camelParamName}($value, $negative);
        }

        if (in_array($paramName, $model->items)) {
            if ($negative) {
                return $query->doesntHave($camelParamName);
            }

            return $query->whereHas($camelParamName);
        }

        $scopedParam = "{$model->getTable()}.$paramName";

        if ($paramName === "id" && $value === "me") {
            $value = $user->id;
        }

        if (
            $user->cant("listArchived", $model::class) &&
            ($paramName === "deleted_at" ||
                $paramName === "is_deleted" ||
                $paramName === "with_deleted")
        ) {
            return $query;
        }

        if ($paramName === "is_deleted") {
            if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                return $query->onlyTrashed();
            } else {
                return $query->withoutTrashed();
            }
        }

        if ($paramName === "with_deleted") {
            if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                return $query->withTrashed();
            } else {
                return $query->withoutTrashed();
            }
        }

        if ($paramName === "deleted_at" && !!$value) {
            $query->withTrashed();
        }

        // If a type is defined for this filter, use the query
        // language, otherwise this is not a valid filter
        $filterType = array_get($model::$filterTypes, $paramName);
        if (is_array($filterType)) {
            $filterType = "enum";
        }
        if (!$filterType && $paramName === "id") {
            $filterType = "number";
        }

        switch ($filterType) {
            case "enum":
                $values = explode(",", $value);
                return self::applyWhereInFilter(
                    $values,
                    $negative,
                    $scopedParam,
                    $query
                );
            case "boolean":
                return $query->where(
                    $scopedParam,
                    $negative ? "!=" : "=",
                    filter_var($value, FILTER_VALIDATE_BOOLEAN) || $value === ""
                );
            case "number":
                if (is_array($value)) {
                    $value = implode(",", $value);
                }

                if ($value === "") {
                    return $query;
                }

                if (str_contains($value, ":")) {
                    $matches = [];
                    $regex = "(\d*)?";
                    preg_match("/^$regex:$regex$/", $value, $matches);

                    $start = $end = null;
                    if (isset($matches[1])) {
                        $start = $matches[1];
                    }
                    if (isset($matches[2])) {
                        $end = $matches[2];
                    }
                    $range = [$start, $end];

                    return self::parseRangeFilter($scopedParam, $range, $query);
                }

                $values = array_map(
                    "intval",
                    array_map(
                        "trim",
                        array_filter(explode(",", $value), function ($i) {
                            return $i !== "";
                        })
                    )
                );

                return self::applyWhereInFilter(
                    $values,
                    $negative,
                    $scopedParam,
                    $query
                );
            case "text":
                if (
                    $model->getConnection()->getConfig()["driver"] === "pgsql"
                ) {
                    // Use prepared statement to search.
                    return $query->whereRaw(
                        "unaccent($scopedParam) ILIKE unaccent(?)",
                        ["%" . $value . "%"]
                    );
                } else {
                    return $query->where($scopedParam, "LIKE", "%$value%");
                }
            case "phone":
                $valueString = sprintf(
                    "%%%s%%",
                    preg_replace("/\D/", "", $value)
                );
                return $query->where(
                    \DB::raw("regexp_replace($scopedParam, '\D','','g')"),
                    "ILIKE",
                    $valueString
                );

            case "date":
                return self::applyDateRangeFilter($scopedParam, $value, $query);

            default:
                return $query;
        }
    }

    private static function parseRangeFilter(
        $scopedParam,
        $range,
        &$query
    ): Builder {
        [$start, $end] = array_map(function ($r) {
            if ($r === "" || $r === null || !is_numeric($r)) {
                return null;
            }

            return $r;
        }, $range);

        if ($start === null && $end === null) {
            return $query;
        } elseif ($start === null) {
            return $query->where($scopedParam, "<=", $end);
        } elseif ($end === null) {
            return $query->where($scopedParam, ">=", $start);
        }

        return $query->whereBetween($scopedParam, [$start, $end]);
    }

    // PHP's default query string parser replaces . by _
    // which would break our query language
    private static function parseQueryString($data): array
    {
        // replace all param keys in the provided string with hexadecimal versions
        // e.g. foo=12&bar=true to 666f6f6=12&616172=true
        $data = preg_replace_callback(
            // Matches any string between (start of line or &) and (= or [)
            // Essentially, param keys in url query string
            "/(?:^|(?<=&))[^=[]+/",
            function ($match) {
                return bin2hex(urldecode($match[0]));
            },
            $data
        );

        // Default php function to parse query strings into an array.
        // Now that . have been converted to hexadecimal, they'll sneak through
        parse_str($data, $values);

        return array_combine(
            // Undo that sneaky string manipulation to get the original param keys
            array_map("hex2bin", array_keys($values)),
            $values
        );
    }

    public static function applyWhereInFilter(
        $values,
        $negative,
        $scopedParam,
        &$query
    ): Builder {
        if ($negative) {
            return $query->whereNotIn($scopedParam, $values);
        }

        return $query->whereIn($scopedParam, $values);
    }

    /**
     * @param string $scopedParam
     *   Database column name prepended by table name if necessary.
     *
     * @param string $value
     *   Right-open interval : [, ) containing ISO-8601 timestamps separated by @.
     *
     *   Examples:
     *      Bounded interval:
     *       2021-06-01T14:00:00Z@2021-07-01T08:00:00Z
     *     Left-bounded interval
     *       2021-06-01T14:00:00Z
     *       2021-06-01T14:00:00Z@
     *     Right-bounded interval
     *       @2021-07-01T08:00:00Z
     *     Unbounded interval
     *       @
     *       Empty string.
     *
     * @param Builder $query
     *   Query object to apply filter to.
     * @throws \Exception if timestamp is malformed
     */
    public static function applyDateRangeFilter(
        string $scopedParam,
        string $value,
        Builder &$query
    ): Builder {
        // Don't filter if value is empty.
        if ($value === "") {
            return $query;
        }

        $intervalRegex = "(?<start>[0-9TZ\.:-]*)@{0,1}(?<end>[0-9TZ\.:-]*)";

        $timestampRegex =
            "(?<year>[0-9]{4})" .
            "-(?<month>[0-9]{2})" .
            "-(?<day>[0-9]{2})" .
            "T(?<hour>[0-9]{2})" .
            ":(?<minute>[0-9]{2})" .
            ":(?<second>[0-9]{2})" .
            "\.{0,1}(?<millisecond>[0-9]*)" .
            "(?<timezone>Z)";

        $intervalMatches = [];
        $timestampMatches = [];

        if (preg_match("/^$intervalRegex?$/", $value, $intervalMatches)) {
            // Apply left boundary if exists
            if ($intervalMatches["start"]) {
                if (
                    preg_match(
                        "/^$timestampRegex?$/",
                        $intervalMatches["start"],
                        $timestampMatches
                    )
                ) {
                    $start = Carbon::parse(
                        $intervalMatches["start"]
                    )->setTimezone(config("app.timezone"));
                    // Match found, then add constraint.
                    $query->where($scopedParam, ">=", $start);
                } else {
                    // Timestamp is malformed: log and do not apply the filter
                    \Log::warning("Malformed timestamp: $value");
                    return $query;
                }
            }

            // Apply right boundary if exists
            if ($intervalMatches["end"]) {
                if (
                    preg_match(
                        "/^$timestampRegex?$/",
                        $intervalMatches["end"],
                        $timestampMatches
                    )
                ) {
                    $end = Carbon::parse($intervalMatches["end"])->setTimezone(
                        config("app.timezone")
                    );
                    // Match found, then add constraint.
                    $query->where($scopedParam, "<", $end);
                } else {
                    // Timestamp is malformed: log and do not apply the filter
                    \Log::warning("Malformed timestamp: $value");
                    return $query;
                }
            }

            // '@' open interval
            if (!$intervalMatches["start"] && !$intervalMatches["end"]) {
                $query->whereNotNull($scopedParam);
            }
        } else {
            $query->whereNotNull($scopedParam);
        }

        return $query;
    }

    /**
     * Wraps the query in a subquery with the same bindings (will get resolved
     * intothe same model) and the same name. This enables filtering on
     * custom columns.
     */
    private static function wrapQuery(Builder $query, mixed $model): void
    {
        $query->setQuery(
            // Remove global scopes here, they will apply on the outer query still
            \DB::query()->fromRaw(
                "({$query->clone()->withoutGlobalScopes()->toSql()}) AS {$model->getTable()}",
                $query->getQuery()->bindings
            )
        );
    }
}
