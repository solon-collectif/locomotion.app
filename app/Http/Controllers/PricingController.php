<?php

namespace App\Http\Controllers;

use App\Enums\PricingLoanableTypeValues;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\Pricing\EvaluateRequest;
use App\Models\Loanable;
use App\Models\Pricing;
use App\Repositories\PricingRepository;
use Carbon\CarbonImmutable;
use Illuminate\Database\Query\Builder;

class PricingController extends RestController
{
    public function __construct(PricingRepository $repository, Pricing $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function evaluate(EvaluateRequest $request, Pricing $pricing)
    {
        \Gate::authorize("create", [Pricing::class, $pricing->toArray()]);
        // Fake Loanable
        $loanableData = $request->input("loanable");
        $loanableDetails = $loanableData["details"];
        $loanable = new Loanable($loanableData);
        $loanable->setRelation(
            "details",
            $loanable->details()->make($loanableDetails)
        );

        $response = $pricing->evaluateRule(
            $request->get("km"),
            $request->get("minutes"),
            $loanable,
            $request->get("loan")
        );

        if (!$response) {
            return ErrorResponse::withMessage("Rule does not evaluate.", 400);
        }

        if (!is_numeric($response)) {
            return ErrorResponse::withMessage(
                "Rule does not evaluate properly.",
                400
            );
        }

        return $response;
    }

    /**
     * Makes sure the initial model values are the same in the request, except for the $modifyableAttributes.
     *
     * @param Pricing $pricing
     * @param BaseRequest $request
     * @param array $modifyableAttributes
     * @return void
     */
    public function validateUnchanged(
        Pricing $pricing,
        BaseRequest $request,
        array $modifyableAttributes
    ) {
        $expectedValues = array_diff_key(
            $pricing->append("pricing_loanable_types")->attributesToArray(),
            array_flip($modifyableAttributes)
        );

        $expectedValues = array_map(
            fn($expectedValue) => function (
                string $attribute,
                mixed $requestValue,
                \Closure $fail
            ) use ($expectedValue) {
                if ($attribute === "pricing_loanable_types") {
                    $requestValue = array_map(
                        fn($loanableType) => PricingLoanableTypeValues::from(
                            $loanableType
                        ),
                        $requestValue
                    );
                }
                if ($expectedValue != $requestValue) {
                    $fail(
                        __("validation.unchanged", [
                            "attribute" => __(
                                "validation.attributes.$attribute"
                            ),
                        ])
                    );
                }
            },
            $expectedValues
        );

        $request->validate($expectedValues);
    }

    public function update(BaseRequest $request, Pricing $pricing)
    {
        \Gate::authorize("update", [$pricing, $request->all()]);

        $request->validate(Pricing::getRules("update"));
        if (
            $request->community_id &&
            $request->community_id != $pricing->community_id
        ) {
            abort(422, "Cannot change pricing community id");
        }

        if (
            !is_null($pricing->end_date) &&
            self::isInThePast($pricing->end_date)
        ) {
            // If pricing has expired, we cannot modify anything.
            abort(503, "Cannot change pricing that has expired");
        }

        if (self::isInThePast($pricing->start_date)) {
            // If pricing is currently active, we can only modify the name, description and end_date
            self::validateUnchanged($pricing, $request, [
                "name",
                "description",
                "end_date",
            ]);
        } else {
            // If pricing is not yet active, we can modify everything, but we make sure the start
            // day stays in the future
            // Maybe we would want to further restrict this to only name, description, start_date
            // and end_date, but the flexibility seems important. Also, since we can still delete
            // pricings as long as they have not started, we should also be able to fully modify
            // them.
            self::validateStartDateIsFuture($request->start_date);
        }

        $pricing->fill($request->all());
        $this->validateOverlaps($pricing, $request->pricing_loanable_types);
        $pricing->save();

        if (!self::isInThePast($pricing->start_date)) {
            $loanableTypes = $request->pricing_loanable_types;
            \DB::table("pricing_loanable_types")
                ->where("pricing_id", $pricing->id)
                ->delete();
            \DB::table("pricing_loanable_types")->insert(
                array_map(
                    fn($pricingLoanableType) => [
                        "pricing_id" => $pricing->id,
                        "pricing_loanable_type" => $pricingLoanableType,
                    ],
                    $loanableTypes
                )
            );

            $pricing->refresh()->append("pricing_loanable_types");
        }
        return $pricing;
    }

    /**
     * Check that no existing pricing overlaps with the provided pricing for the given
     * loanable types. Pricings overlap when they share the same community_id, the same type, apply
     * for the same loanable and apply on the same type of ownership ('non_fleet' doesn't overlap
     * with 'fleet', but 'all' overlaps with both).
     *
     * @param Pricing $pricing
     * @param array $pricingLoanableTypes
     * @return void
     */
    public function validateOverlaps(
        Pricing $pricing,
        array $pricingLoanableTypes
    ) {
        $query = \DB::table("pricings")
            ->leftJoin(
                "pricing_loanable_types",
                "pricings.id",
                "pricing_loanable_types.pricing_id"
            )
            ->whereNull("deleted_at")
            ->whereIn("pricing_loanable_type", $pricingLoanableTypes)
            ->where("pricing_type", $pricing->pricing_type)
            ->where(
                fn(Builder $query) => $query
                    ->whereNull("end_date")
                    ->orWhere("end_date", ">", $pricing->start_date)
            );

        if ($pricing->end_date) {
            $query->where("start_date", "<", $pricing->end_date);
        }

        if ($pricing->community_id) {
            $query->where("community_id", $pricing->community_id);
        } else {
            $query->whereNull("community_id");
        }

        if ($pricing->id) {
            $query->where("id", "!=", $pricing->id);
        }

        if ($pricing->loanable_ownership_type === "fleet") {
            $query->where(
                fn(Builder $q) => $q
                    ->where("loanable_ownership_type", "fleet")
                    ->orWhere("loanable_ownership_type", "all")
            );
        } elseif ($pricing->loanable_ownership_type === "non_fleet") {
            $query->where(
                fn(Builder $q) => $q
                    ->where("loanable_ownership_type", "non_fleet")
                    ->orWhere("loanable_ownership_type", "all")
            );
        }
        // else ownership_type === "all", would overlap with any ownership_type

        $value = $query->first();

        if ($value) {
            abort(
                422,
                __("state.pricing.must_not_overlap", [
                    "pricing_type" => __(
                        "state.pricing.type.$value->pricing_type"
                    ),
                    "pricing_loanable_type" => PricingLoanableTypeValues::from(
                        $value->pricing_loanable_type
                    )->translate(),
                ])
            );
        }
    }

    private static function isInThePast(string $date)
    {
        return CarbonImmutable::now(
            config("app.default_user_timezone")
        )->greaterThanOrEqualTo(
            (new CarbonImmutable(
                $date,
                config("app.default_user_timezone")
            ))->startOfDay()
        );
    }

    private static function validateStartDateIsFuture(string $date)
    {
        if (self::isInThePast($date)) {
            abort(
                422,
                __("validation.after", [
                    "attribute" => __("validation.attributes.start_date"),
                    "date" => CarbonImmutable::today(
                        config("app.default_user_timezone")
                    )->format("Y-m-d"),
                ])
            );
        }
    }

    public function create(BaseRequest $request)
    {
        \Gate::authorize("create", [Pricing::class, $request->all()]);
        $request->validate(Pricing::getRules("create"));

        self::validateStartDateIsFuture($request->start_date);

        $pricing = new Pricing($request->all());
        $this->validateOverlaps($pricing, $request->pricing_loanable_types);
        $pricing->save();

        $loanableTypes = $request->pricing_loanable_types;
        \DB::table("pricing_loanable_types")->insert(
            array_map(
                fn($pricingLoanableType) => [
                    "pricing_id" => $pricing->id,
                    "pricing_loanable_type" => $pricingLoanableType,
                ],
                $loanableTypes
            )
        );

        $pricing->refresh()->append("pricing_loanable_types");

        return $pricing;
    }

    public function destroy(Pricing $pricing)
    {
        \Gate::authorize("delete", $pricing);

        // Prevent deleting pricing once it's active.
        self::validateStartDateIsFuture($pricing->start_date);

        $pricing->delete();
    }
}
