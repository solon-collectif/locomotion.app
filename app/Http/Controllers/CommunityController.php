<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Community\CreateRequest;
use App\Http\Requests\Community\UpdateRequest;
use App\Http\Resources\CommunityOverviewLoanableResource;
use App\Http\Resources\CommunityOverviewResource;
use App\Mail\Registration\ApprovedCustom;
use App\Models\Community;
use App\Models\Invitation;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Repositories\CommunityRepository;
use Carbon\CarbonImmutable;

class CommunityController extends RestController
{
    public function __construct(
        CommunityRepository $repository,
        Community $model
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function view(BaseRequest $request, Community $community)
    {
        \Gate::authorize("view", $community);
        return $this->respondWithItem(
            $request,
            $this->retrieveWithRequestFields($request, $community)
        );
    }

    public function create(CreateRequest $request)
    {
        $item = parent::validateAndCreate($request);
        return $this->respondWithItem($request, $item, 201);
    }

    public function update(UpdateRequest $request, Community $community)
    {
        $item = parent::validateAndUpdate($request, $community->id);
        return $this->respondWithItem($request, $item);
    }

    public function destroy(BaseRequest $request, Community $community)
    {
        \Gate::authorize("destroy", $community);

        $request->validate([
            "merge_destination" => ["nullable", "int"],
        ]);

        $newCommunityId = $request->input("merge_destination");

        if (!$newCommunityId) {
            return $community->delete();
        }

        if (!Community::where("id", $newCommunityId)->exists()) {
            abort(404);
        }

        // Move loans
        if ($request->boolean("merge_loans")) {
            Loan::whereNull("meta->original_community_id")
                ->where("community_id", $community->id)
                ->update(["meta->original_community_id" => $community->id]);
            Loan::where("community_id", $community->id)->update([
                "community_id" => $newCommunityId,
            ]);
        }

        if ($request->boolean("merge_users")) {
            $now = CarbonImmutable::now();
            // For every communityUser in old community, create a copy in new community
            // if it didn't already exist
            $query = CommunityUser::where("community_id", $community->id)
                // Do not copy if already exists in new community
                ->whereDoesntHave(
                    "user.communities",
                    fn($community) => $community->where(
                        "community_id",
                        $newCommunityId
                    )
                );

            $mergeUserStatus = $request->boolean("merge_user_status");
            if (!$mergeUserStatus) {
                $query->whereNull("suspended_at");
            }

            $communityUsers = $query->getQuery()->get();

            if ($mergeUserStatus) {
                $communityUsers = $communityUsers->map(
                    fn($c) => [
                        "approved_at" => $c->approved_at,
                        "suspended_at" => $c->suspended_at,
                        "user_id" => $c->user_id,
                        "created_at" => $now,
                        "updated_at" => $now,
                        "community_id" => $newCommunityId,
                    ]
                );
            } else {
                $communityUsers = $communityUsers->map(
                    fn($c) => [
                        "user_id" => $c->user_id,
                        "created_at" => $now,
                        "updated_at" => $now,
                        "community_id" => $newCommunityId,
                    ]
                );
            }

            \DB::table("community_user")->insert($communityUsers->toArray());
        }

        if ($request->boolean("merge_invitations")) {
            Invitation::where("community_id", $community->id)
                ->status("active")
                ->update(["community_id" => $newCommunityId]);
        }

        return $community->delete();
    }

    public function restore(BaseRequest $request, Community $community)
    {
        \Gate::authorize("destroy", $community);
        $community->restore();
    }

    public function addMember(Request $request, Community $community)
    {
        \Gate::authorize("add-member", $community);
        $userId = $request->get("id");
        $user = User::findOrFail($userId);

        if ($community->users->where("id", $userId)->isEmpty()) {
            $community->users()->attach($userId);

            return $this->respondWithItem($request, $user);
        }

        return $this->respondWithItem(
            $request,
            $community->users->where("id", $userId)->first()
        );
    }

    public function removeMember(
        Request $request,
        Community $community,
        User $user
    ) {
        \Gate::authorize("remove-member", $community);
        if ($community->users->where("id", $user->id)->isEmpty()) {
            return ErrorResponse::withMessage(null, 404);
        }

        $community->users()->detach($user);

        return $this->respondWithItem($request, $user);
    }

    public function overview(Request $request)
    {
        // Use the guard directly, rather than relying on middleware which will 401 non-registered users
        $user = \Auth::guard("api")->user();
        $communities = Community::accessibleBy($user)->get();

        $loanables = [];
        if ($user) {
            $loanables = Loanable::accessibleBy($user)
                // Load approved communities of all users with a role on the
                // loanable to ease computation of the community_ids attribute.
                ->with("userRoles.user.approvedCommunities")
                ->with("details")
                ->where("published", true)
                ->get();
        }

        $data = [
            "loanables" => CommunityOverviewLoanableResource::collection(
                $loanables
            ),
            "communities" => CommunityOverviewResource::collection(
                $communities
            ),
        ];

        if ($user) {
            $data["user_positions"] = User::for("admin", $user)
                ->whereNotNull("address_position")
                ->pluck("address_position");
        }

        return $data;
    }

    public function sendApprovingSample(
        BaseRequest $request,
        Community $community
    ) {
        \Gate::authorize("send-sample-email", $community);
        $fakeCommunityUser = new CommunityUser([
            "user_id" => $request->user()->id,
            "community_id" => $community->id,
        ]);
        \Mail::send(
            (new ApprovedCustom($request->get("markdown")))->to(
                $request->user()->email
            )
        );
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
                "chat_group_url" => "",
                "starting_guide_url" => "",
                "description" => null,
                "contact_email" => null,
                "area" => [],
                "type" => "private",
                "pricings" => [],
            ],
            "form" => [
                "name" => [
                    "type" => "text",
                ],
                "starting_guide_url" => [
                    "type" => "text",
                ],
                "chat_group_url" => [
                    "type" => "text",
                ],
                "contact_email" => [
                    "type" => "text",
                ],
                "description" => [
                    "type" => "textarea",
                ],
                "type" => [
                    "type" => "select",
                    "label" => "Type",
                    "options" => [
                        [
                            "text" => "Privée",
                            "value" => "private",
                        ],
                        [
                            "text" => "Quartier",
                            "value" => "borough",
                        ],
                    ],
                ],
                "exempt_from_contributions" => ["type" => "checkbox"],
                "requires_residency_proof" => ["type" => "checkbox"],
                "requires_identity_proof" => ["type" => "checkbox"],
                "requires_custom_proof" => ["type" => "checkbox"],
                "custom_proof_name" => ["type" => "text"],
                "custom_proof_desc" => ["type" => "textarea"],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }
}
