<?php

namespace App\Http\Controllers;

use App\Http\ErrorResponse;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\User\AddToBalanceRequest as UserAddToBalanceRequest;
use App\Http\Requests\User\UpdateRequest as UserUpdateRequest;
use App\Models\Invitation;
use App\Models\User;
use App\Services\GoogleAccountService;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Laravel\Passport\Token;
use Laravel\Passport\TokenRepository;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends RestController
{
    protected TokenRepository $tokens;
    protected UserController $userController;

    public function __construct(
        TokenRepository $tokens,
        UserController $userController
    ) {
        $this->tokens = $tokens;
        $this->userController = $userController;
    }

    public function login(LoginRequest $request)
    {
        $email = $request->get("email");
        $password = $request->get("password");

        $data = [
            "username" => $email,
            "password" => $password,
            "client_id" => config("auth.client.id"),
            "client_secret" => config("auth.client.secret"),
            "grant_type" => "password",
        ];

        $req = Request::create("/oauth/token", "POST", $data);
        $response = app()->handle($req);
        if ($response->getStatusCode() === 400) {
            return response()->json(
                [
                    "message" => "Invalid username or password.",
                    "error" => "invalid_username_or_password",
                    "error_description" =>
                        "The username or password provided are invalid.",
                ],
                401
            );
        }
        return $response;
    }

    public function refreshToken(Request $request)
    {
        $data = [
            "refresh_token" => $request->input("refresh_token"),
            "client_id" => config("auth.client.id"),
            "client_secret" => config("auth.client.secret"),
            "grant_type" => "refresh_token",
        ];

        $req = Request::create("/oauth/token", "POST", $data);
        return app()->handle($req);
    }

    public function logout(Request $request)
    {
        $user = $request->user();

        if (!$user) {
            return response()->json("", 204);
        }

        $user->token()->revoke();

        return response()->json("", 204);
    }

    public function register(RegisterRequest $request)
    {
        $email = $request->get("email");
        $password = $request->get("password");

        $user = new User();
        $user->email = $email;
        $user->password = Hash::make($password);

        // If we have an invitation code, we might be able to say that the email has been validated
        if ($code = $request->input("code")) {
            $invitation = Invitation::where("confirmation_code", $code)
                ->where("email", $email)
                ->status("active")
                ->first();

            if ($invitation) {
                $user->email_verified_at = Carbon::now();
                $invitation->validated_on_registration = true;
                $invitation->expires_at = null;
                $invitation->save();
            }
        }

        $user->save();

        $loginRequest = new LoginRequest();
        $loginRequest->setMethod("POST");
        $loginRequest->request->add([
            "email" => $email,
            "password" => $password,
        ]);
        return $this->login($loginRequest);
    }

    public function retrieveUser(Request $request)
    {
        $authUser = $request->user();

        // Geocode address only if user is not yet in any community. Don't change global admins.
        if (
            !$authUser->isAdmin() &&
            $authUser->address &&
            !$authUser->main_community
        ) {
            $authUser->updateAddressAndRelocateCommunity();
        }

        return $this->respondWithItem($request, $authUser);
    }

    public function getUserBalance(Request $request)
    {
        return $this->userController->getBalance($request, $request->user());
    }

    public function addToUserBalance(Request $request)
    {
        $request->merge(["user_id" => $request->user()->id]);
        $addBalanceRequest = $request->redirect(UserAddToBalanceRequest::class);
        return $this->userController->addToBalance(
            $addBalanceRequest,
            $request->user()
        );
    }

    public function updateUser(UserUpdateRequest $request)
    {
        $user = $request->user();

        $request->merge(["email" => $user->email]);

        return $this->userController->update($request, $user);
    }

    public function google()
    {
        return Socialite::driver("google")
            ->stateless()
            ->redirect();
    }

    public function callback(GoogleAccountService $service)
    {
        $user = $service->createOrGetUser(
            Socialite::driver("google")
                ->stateless()
                ->user()
        );
        auth()->login($user);
        $token = $user->createToken("API Token")->accessToken;
        return redirect()->to(
            config("app.url") . "/login/callback?token=" . $token
        );
    }

    public function passwordRequest(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $request->validate(["email" => "required|email"]);

        $response = $this->broker()->sendResetLink($request->only("email"));

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse()
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    public function passwordReset(ResetPasswordRequest $request)
    {
        $response = $this->broker()->reset(
            $request->only(
                "email",
                "password",
                "password_confirmation",
                "token"
            ),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    /*
     * If the current user is an admin, we create an
     * access token for the provided user. The frontend
     * will use it to mandate the admin to act on behalf
     * of the user.
     */
    public function mandate(Request $request, $userId)
    {
        if (!$request->user()->isAdmin()) {
            return new Response("you are not authorized", 403);
        }

        $user = User::find($userId);
        return $user->createToken("mandate token")->accessToken;
    }

    protected function sendResetLinkResponse()
    {
        return ErrorResponse::withMessage("Token sent.", 200);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return ErrorResponse::withMessage(trans($response), 400);
    }

    protected function sendResetResponse(Request $request, $response)
    {
        return ErrorResponse::withMessage("Reset succesful.", 200);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return ErrorResponse::withMessage(trans($response), 400);
    }

    protected function resetPassword($user, $password)
    {
        // Revoke currently existing tokens
        $id = $user->getKey();
        $tokens = $this->tokens->forUser($id);
        $tokens->map(function (Token $t) {
            $t->revoke();
        });

        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->save();
    }

    private function broker()
    {
        return Password::broker();
    }
}
