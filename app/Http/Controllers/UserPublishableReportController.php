<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Models\UserPublishableReport;
use App\Repositories\RestRepository;

class UserPublishableReportController extends RestController
{
    public function __construct(UserPublishableReport $report)
    {
        $this->model = $report;
        $this->repo = RestRepository::for($report);
    }

    public function download(
        BaseRequest $request,
        UserPublishableReport $userReport
    ) {
        \Gate::authorize("view", $userReport);

        return \Storage::response($userReport->path);
    }
}
