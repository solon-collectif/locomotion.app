<?php

namespace App\Http\Controllers;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Calendar\AvailabilityHelper;
use App\Calendar\DateIntervalHelper;
use App\Calendar\Interval;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Events\LoanablePublishedEvent;
use App\Events\LoanableUserRoleAdded;
use App\Helpers\Order as OrderHelper;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Loanable\AvailabilityRequest;
use App\Http\Requests\Loanable\CreateRequest;
use App\Http\Requests\Loanable\EventsRequest;
use App\Http\Requests\Loanable\SearchRequest;
use App\Http\Requests\Loanable\TestRequest;
use App\Http\Resources\DashboardLoanableResource;
use App\Http\Resources\DashboardLoanResource;
use App\Http\Resources\InvoiceSummaryResource;
use App\Http\Resources\LibraryResource;
use App\Http\Resources\LoanableResource;
use App\Http\Resources\LoanableUserRoleResource;
use App\Http\WebQueryBuilder;
use App\Models\Bike;
use App\Models\Car;
use App\Models\Community;
use App\Models\Incident;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\MergedLoanableUserRole;
use App\Models\User;
use App\Pricings\LoanInvoiceHelper;
use App\Repositories\LoanableRepository;
use App\Repositories\LoanRepository;
use App\Rules\LoanableTypeListRule;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Validator;

class LoanableController extends RestController
{
    protected LoanRepository $loanRepo;
    protected LoanController $loanController;

    public function __construct(
        LoanableRepository $repository,
        Loanable $model,
        LoanRepository $loanRepository,
        LoanController $loanController
    ) {
        $this->repo = $repository;
        $this->model = $model;

        $this->loanRepo = $loanRepository;
        $this->loanController = $loanController;
    }

    public function create(CreateRequest $request)
    {
        // Make sure to create a details relation, even if no data is present.
        if (!$request->get("details")) {
            $request->merge([
                "details" => [],
            ]);
        }

        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "create");

        /** @var Loanable $loanable */
        $loanable = $this->repo->create($input);

        $ownerUser = $request->get("owner_user");
        if ($ownerUser) {
            $userRole = $loanable->userRoles()->make([
                "show_as_contact" => true,
                "pays_loan_insurance" => false,
                "pays_loan_price" => false,
            ]);
            $userRole->user_id = $ownerUser["id"];
            $userRole->role = LoanableUserRoles::Owner;
            $userRole->granted_by_user_id = $request->user()->id;

            $userRole->save();
        }

        return (new LoanableResource($loanable->refresh()))
            ->response()
            ->setStatusCode(201);
    }

    public function view(Request $request, Loanable $loanable)
    {
        Gate::authorize("view", $loanable);

        $loanable->load([
            "activeIncidents.assignee.avatar",
            "activeIncidents.resolvedByUser.avatar",
            "activeIncidents.notes.author.avatar",
            "activeIncidents.reportedByUser.avatar",
        ]);
        return new LoanableResource($loanable);
    }

    /**
     * @param Interval $dateRange
     * @param string $availabilityMode
     * @param $availabilityRules
     * @param ?string $timezone
     * @return Interval[]
     */
    public function computeAvailability(
        Interval $dateRange,
        string $availabilityMode,
        $availabilityRules,
        ?string $timezone = null
    ): array {
        $availabilityIntervals = AvailabilityHelper::getAvailability(
            [
                "available" => "always" == $availabilityMode,
                "rules" => $availabilityRules,
            ],
            $dateRange,
            $timezone
        );

        $now = Carbon::now();
        if ($dateRange->start->isBefore($now)) {
            $availabilityIntervals = DateIntervalHelper::subtractList(
                $availabilityIntervals,
                [new Interval($dateRange->start, $now)]
            );
        }

        return $availabilityIntervals;
    }

    /**
     * @param Interval $dateRange
     * @param Interval[] $availabilityIntervals
     * @param string $responseMode
     * @param ?string $timezone
     * @return array
     */
    public function formatAvailabilities(
        Interval $dateRange,
        array $availabilityIntervals,
        string $responseMode,
        string $timezone = null
    ): array {
        $now = Carbon::now();
        if ($dateRange->start->isBefore($now)) {
            $availabilityIntervals = DateIntervalHelper::subtractList(
                $availabilityIntervals,
                [new Interval($dateRange->start, $now)]
            );
        }

        if ($responseMode == "available") {
            $intervals = $availabilityIntervals;
            $data = [
                "available" => true,
            ];
        } else {
            $intervals = DateIntervalHelper::invert(
                $dateRange,
                $availabilityIntervals
            );
            $data = [
                "available" => false,
            ];
        }

        $events = [];
        // Generate events from intervals.
        foreach ($intervals as $interval) {
            $events[] = [
                "start" => $interval->start->tz($timezone)->toDateTimeString(),
                "end" => $interval->end->tz($timezone)->toDateTimeString(),
                "data" => $data,
            ];
        }
        return $events;
    }

    public function availability(
        Loanable $loanable,
        AvailabilityRequest $request
    ) {
        Gate::authorize("view", $loanable);

        // Start and end in the request are relative to the loanable timezone
        $dateRange = Interval::of(
            $request->start,
            $request->end,
            $loanable->timezone
        );

        $availabilityMode = $loanable->availability_mode;
        $availabilityRules = $loanable->getAvailabilityRules();

        $availabilityIntervals = $this->computeAvailability(
            $dateRange,
            $availabilityMode,
            $availabilityRules,
            $loanable->timezone
        );

        $loans = Loan::where("loanable_id", $loanable->id)
            ->isPeriodUnavailable($dateRange)
            ->get();

        $loanIntervals = [];

        foreach ($loans as $loan) {
            $loanIntervals[] = new Interval(
                $loan->departure_at,
                $loan->actual_return_at
            );
        }

        $availabilityIntervals = DateIntervalHelper::subtractList(
            $availabilityIntervals,
            $loanIntervals
        );

        $incidentIntervals = $loanable->activeIncidents
            ->filter(
                fn(Incident $incident) => !is_null($incident->blocking_until)
            )
            ->map(
                fn(Incident $incident) => new Interval(
                    $incident->start_at,
                    $incident->blocking_until
                )
            )
            ->toArray();

        $availabilityIntervals = DateIntervalHelper::subtractList(
            $availabilityIntervals,
            $incidentIntervals
        );

        return response(
            $this->formatAvailabilities(
                $dateRange,
                $availabilityIntervals,
                $request->responseMode,
                $loanable->timezone
            ),
            200
        );
    }

    public function genericAvailability(BaseRequest $request)
    {
        $request->validate([
            "start" => ["date", "required"],
            "end" => ["date", "required"],
            "responseMode" => [
                "required",
                Rule::in(["available", "unavailable"]),
            ],
            "availability_mode" => ["required", Rule::in(["always", "never"])],
            "availability_json" => ["required", "json"],
        ]);

        $timezone = $request->get("timezone", "utc");
        // Start and end in the request are relative to the loanable timezone
        $dateRange = Interval::of($request->start, $request->end, $timezone);

        $availabilityMode = $request->availability_mode;
        $availabilityRules = json_decode(
            $request->availability_json,
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        $availabilityIntervals = $this->computeAvailability(
            $dateRange,
            $availabilityMode,
            $availabilityRules,
            $timezone
        );

        return response(
            $this->formatAvailabilities(
                $dateRange,
                $availabilityIntervals,
                $request->responseMode,
                $timezone
            ),
            200
        );
    }

    public function events(EventsRequest $request, Loanable $loanable)
    {
        Gate::authorize("view", $loanable);

        $range = Interval::of(
            $request->start,
            $request->end,
            $loanable->timezone
        );

        $events = [];

        $availabilityRules = $loanable->getAvailabilityRules();
        $availabilityMode = $loanable->availability_mode;

        $intervals = AvailabilityHelper::getScheduleIntervals(
            [
                "available" => "always" == $availabilityMode,
                "rules" => $availabilityRules,
            ],
            $range,
            $loanable->timezone
        );

        foreach ($intervals as $interval) {
            $events[] = [
                "type" => "availability_rule",
                "start" => $interval->start,
                "end" => $interval->end,
                "uri" => "/loanables/$loanable->id",
                "data" => [
                    // availability_mode == "always" means that events are of unavailability.
                    "available" => $availabilityMode != "always",
                ],
            ];
        }

        // TODO: unaccessible loans should still show as generic unavailability
        $loans = Loan::query()
            ->accessibleBy($request->user())
            ->where("loanable_id", "=", $loanable->id)
            ->where("status", "!=", LoanStatus::Canceled)
            ->where("status", "!=", LoanStatus::Rejected)
            ->intersects($range)
            ->orderBy("departure_at")
            ->with(["borrowerUser"])
            ->get();

        foreach ($loans as $loan) {
            $events[] = [
                "type" => "loan",
                "start" => $loan->departure_at,
                "end" => $loan->actual_return_at,
                "uri" => "/loans/$loan->id",
                "data" => [
                    "loan_id" => $loan->id,
                    "status" => $loan->status,
                    "accepted" => $loan->is_accepted,
                    "borrower_name" => $loan->borrowerUser->name,
                    "action_required" => $loan->userActionRequired(
                        $request->user()
                    ),
                ],
            ];
        }

        // Event field definitions for sorting.
        $eventFieldDefs = [
            "type" => ["type" => "string"],
            "uri" => ["type" => "string"],
            "start" => ["type" => "carbon"],
            "end" => ["type" => "carbon"],
        ];

        $orderArray = OrderHelper::parseOrderRequestParam(
            $request->order,
            $eventFieldDefs
        );
        $events = OrderHelper::sortArray($events, $orderArray);

        // Prepare data for response.
        foreach ($events as $key => $event) {
            $events[$key]["start"] = $event["start"]
                ->tz($loanable->timezone)
                ->toDateTimeString();
            $events[$key]["end"] = $event["end"]
                ->tz($loanable->timezone)
                ->toDateTimeString();
        }

        return response($events, 200);
    }

    public function loansDuringUnavailabilities(
        Request $request,
        Loanable $loanable
    ) {
        Gate::authorize("view", $loanable);

        // If availability mode and rules come from the request, then use that
        // to compute availability.
        if ($request->availability_mode || $request->availability_json) {
            $availabilityMode = $request->availability_mode ?? "always";
            $availabilityRules = $request->availability_json
                ? json_decode(
                    $request->availability_json,
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                )
                : [];
        } else {
            $availabilityMode = $loanable->availability_mode;
            $availabilityRules = $loanable->getAvailabilityRules();
        }

        $resource = DashboardLoanResource::collection(
            Loan::query()
                ->accessibleBy($request->user())
                ->where("loanable_id", $loanable->id)
                ->future()
                ->duringAvailability(
                    [
                        "available" => "always" == $availabilityMode,
                        "rules" => $availabilityRules,
                    ],
                    false
                )
        );
        $resource->withoutWrapping();
        return $resource;
    }

    public function update(Request $request, Loanable $loanable)
    {
        Gate::authorize("update", $loanable);

        if (
            $loanable->published &&
            $loanable->type === LoanableTypes::Car &&
            !Gate::check("updatePublishedFields", $loanable)
        ) {
            Validator::make(
                $request->get("details", []),
                [
                    "brand" => "missing",
                    "engine" => "missing",
                    "model" => "missing",
                    "pricing_category" => "missing",
                    "transmission_mode" => "missing",
                    "value_category" => "missing",
                    "year_of_circulation" => "missing",
                    // plate number & papers location can still be modified.
                ],
                [
                    "missing" => __(
                        "validation.custom.field_cannot_be_updated"
                    ),
                ]
            )->validate();
        }

        $item = parent::validateAndUpdate($request, $loanable->id);

        return new LoanableResource($item);
    }

    public function publish(Loanable $loanable, BaseRequest $request)
    {
        Gate::authorize("publish", $loanable);

        Validator::make($loanable->toArray(), [
            "position" => "required",
        ])->validate();

        $detailsPublicationValidation = match ($loanable->type) {
            LoanableTypes::Bike => [
                "bike_type" => "required",
                "model" => "required",
                "size" => "required",
            ],
            LoanableTypes::Car => [
                "brand" => "required",
                "engine" => "required",
                "model" => "required",
                "papers_location" => "required",
                "plate_number" => "required",
                "pricing_category" => "required",
                "transmission_mode" => "required",
                "value_category" => "required",
                "year_of_circulation" => "required",
            ],
            default => [],
        };

        Validator::make(
            $loanable->details->toArray(),
            $detailsPublicationValidation
        )->validate();

        $loanable->published = true;
        $loanable->save();

        event(new LoanablePublishedEvent($request->user(), $loanable));

        return new LoanableResource($loanable);
    }

    public function destroy(Request $request, Loanable $loanable)
    {
        Gate::authorize("delete", $loanable);

        if (
            $loanable
                ->loans()
                ->blockingLoanableDeletion()
                ->exists()
        ) {
            throw ValidationException::withMessages([
                __("validation.custom.vehicle_must_not_have_ongoing_loans"),
            ]);
        }

        return parent::validateAndDestroy($request, $loanable->id);
    }

    public function restore(Request $request, Loanable $loanable)
    {
        Gate::authorize("restore", $loanable);

        /** @var Loanable $loanable */
        $loanable = parent::validateAndRestore($request, $loanable->id);

        if (!$request->boolean("restore_availability")) {
            // Delete all availabilities.
            $loanable->availability_json = "[]";
            $loanable->availability_mode = "never";
            $loanable->save();
        }
    }

    /**
     * @return array<string, array> with loanable type -> LoanableRessource array shape
     */
    public function listByType(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "types" => ["string", new LoanableTypeListRule()],
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // Default to all types
        $types = LoanableTypes::possibleTypes;
        if ($request->has("types")) {
            $types = explode(",", $request->query("types"));
        }

        /** @var User $user */
        $user = $request->user();
        $loanables = [];
        foreach ($types as $type) {
            $loanables[$type] = [];
            // At this point $loanable[$type] will have entries with loanable_id->loanable.
            // Using the array union allows us to easily dedupe loanables which may be
            // in multiple communities.
            $loanables[$type] += LoanablesByTypeAndCommunity::format(
                Loanable::with([
                    "image",
                    "userRoles",
                    "userRoles.user.avatar",
                    "details",
                ])
                    ->ownedOrCoownedBy($user->id)
                    ->where("type", $type)
                    ->get()
            );

            foreach ($user->approvedCommunities as $community) {
                $loanables[$type] += LoanablesByTypeAndCommunity::get(
                    $community->id,
                    LoanableTypes::from($type)
                );
            }

            // Finally, we keep only the loanable from the loanable_id->loanable array entries.
            $loanables[$type] = array_values($loanables[$type]);
        }

        return $loanables;
    }

    public function search(SearchRequest $request)
    {
        // For simplicity, we ignore timezones if they are passed in the request, since
        // we do not anticipate passing a timezone in the search a valid use-case (users search
        // and take out loans in the timezone of the related loanable).
        $departureAt = (new Carbon(
            $request->get("departure_at")
        ))->toDateTimeLocalString();
        $durationInMinutes = (int) $request->get("duration_in_minutes");
        $returnAt = (new Carbon($departureAt))
            ->addMinutes($durationInMinutes)
            ->toDateTimeLocalString();

        $loanables = Loanable::query()
            ->accessibleBy($request->user())
            ->where(
                fn(Builder $query) => $query
                    ->whereNull("max_loan_duration_in_minutes")
                    ->orWhere(
                        "max_loan_duration_in_minutes",
                        ">=",
                        $durationInMinutes
                    )
            );

        // Check no other loans intersect
        $availableLoanables = $loanables
            ->whereDoesntHave(
                "loans",
                fn($loans) => $loans->isLocalPeriodUnavailable(
                    $departureAt,
                    $returnAt
                )
            )
            ->with([
                "activeIncidents" => fn(Relation $q) => $q->whereNotNull(
                    "blocking_until"
                ),
            ])
            ->get();

        // Check schedule is open for remaining loanables
        $availableLoanableIds = $availableLoanables
            ->reject(function ($loanable) use ($departureAt, $returnAt) {
                // Parse the interval in the loanable's timezone
                $potentialLoanInterval = new Interval(
                    new Carbon($departureAt, $loanable->timezone),
                    new Carbon($returnAt, $loanable->timezone)
                );
                return !$loanable->isLoanableScheduleOpen(
                    $potentialLoanInterval
                ) ||
                    $loanable->activeIncidents
                        ->filter(
                            fn(Incident $i) => $i->isBlockingInterval(
                                $potentialLoanInterval
                            )
                        )
                        ->count() > 0;
            })
            ->map(fn($loanable) => $loanable->id)
            ->sort()
            ->values();

        return response($availableLoanableIds, 200);
    }

    public function test(TestRequest $request, Loanable $loanable)
    {
        Gate::authorize("create", [Loan::class, $loanable]);

        $estimatedDistance = intval($request->get("estimated_distance", 0));
        $departureAt = new CarbonImmutable(
            $request->get("departure_at"),
            $loanable->timezone
        );
        $durationInMinutes = intval($request->get("duration_in_minutes"));

        $communityId = $request->get("community_id");
        if ($communityId) {
            $community = Community::query()
                ->accessibleBy($request->user())
                ->find($communityId);
        } else {
            $community = $loanable->getCommunityForLoanBy($request->user());
        }

        if (!$community) {
            // User cannot borrow this loanable (no communities in common)
            abort(403, "Loanable not available in borrower's communities");
        }

        $loanData = [
            "borrower_user_id" => $request->user()->id,
            "community_id" => $community->id,
            "departure_at" => $departureAt->toISOString(),
            "duration_in_minutes" => $durationInMinutes,
            "estimated_distance" => $estimatedDistance,
            "loanable_id" => $loanable->id,
            "platform_tip" => $request->get("platform_tip", 0),
        ];

        $loan = new Loan($loanData);
        $loan->actual_return_at = $departureAt
            ->addMinutes($durationInMinutes)
            ->tz(config("app.timezone"));
        $loan->is_self_service = $loan->loanable->shouldLoanBeSelfServiceForUser(
            $loan->borrowerUser
        );

        $borrowerInvoice = LoanInvoiceHelper::getBorrowerInvoice($loan);

        return response(
            [
                "community" => [
                    "id" => $community->id,
                    "name" => $community->name,
                ],
                "available" => $loanable->isAvailable(
                    $departureAt,
                    $durationInMinutes
                ),
                "borrower_invoice" => $borrowerInvoice->hasNonZeroItems()
                    ? new InvoiceSummaryResource($borrowerInvoice)
                    : null,
            ],
            200
        );
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "type" => null,
                "comments" => "",
                "instructions" => "",
                "return_instructions" => "",
                "location_description" => "",
                "name" => "",
                "details" => new \stdClass(),
                "position" => null,
                "images" => [],
                "availability_json" => "[]",
                "availability_mode" => "never",
                "owner_user" => new \stdClass(),
            ],
            "form" => [
                "general" => [
                    "name" => [
                        "type" => "text",
                    ],
                    "images" => [
                        "type" => "images",
                        "aspect_ratio" => "16 / 10",
                    ],
                    "position" => [
                        "type" => "point",
                    ],
                    "location_description" => [
                        "type" => "textarea",
                    ],
                    "comments" => [
                        "type" => "textarea",
                    ],
                    "instructions" => [
                        "type" => "textarea",
                    ],
                    "return_instructions" => [
                        "type" => "textarea",
                    ],
                    "type" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Auto",
                                "value" => "car",
                            ],
                            [
                                "text" => "Vélo",
                                "value" => "bike",
                            ],
                            [
                                "text" => "Remorque",
                                "value" => "trailer",
                            ],
                        ],
                    ],
                    "sharing_mode" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Libre partage",
                                "value" => "self_service",
                            ],
                            [
                                "text" => "Sur demande",
                                "value" => "on_demand",
                            ],
                            [
                                "text" => "Libre partage limité",
                                "value" => "hybride",
                            ],
                        ],
                    ],
                ],
                "bike" => [
                    "model" => [
                        "type" => "text",
                    ],
                    "bike_type" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Vélo régulier",
                                "value" => "regular",
                            ],
                            [
                                "text" => "Vélo électrique",
                                "value" => "electric",
                            ],
                            [
                                "text" => "Vélo-cargo",
                                "value" => "cargo",
                            ],
                            [
                                "text" => "Vélo-cargo électrique",
                                "value" => "cargo_electric",
                            ],
                            [
                                "text" => "Roue fixe",
                                "value" => "fixed_wheel",
                            ],
                        ],
                    ],
                    "size" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Grand",
                                "value" => "big",
                            ],
                            [
                                "text" => "Moyen",
                                "value" => "medium",
                            ],
                            [
                                "text" => "Petit",
                                "value" => "small",
                            ],
                            [
                                "text" => "Enfant",
                                "value" => "kid",
                            ],
                        ],
                    ],
                ],
                "car" => [
                    "brand" => [
                        "type" => "text",
                    ],
                    "model" => [
                        "type" => "text",
                    ],
                    "pricing_category" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" =>
                                    "Petite auto (compacte, sous-compacte, hybride non-branchable)",
                                "value" => "small",
                            ],
                            [
                                "text" => "Grosse auto (van, VUS, pick-up)",
                                "value" => "large",
                            ],
                            [
                                "text" =>
                                    "Petite auto électrique (électrique, hybride branchable)",
                                "value" => "small_electric",
                            ],
                            [
                                "text" =>
                                    "Grosse auto électrique (électrique, hybride branchable)",
                                "value" => "large_electric",
                            ],
                        ],
                    ],
                    "year_of_circulation" => [
                        "type" => "number",
                        "max" => (int) date("Y") + 1,
                        "min" => 1900,
                    ],
                    "transmission_mode" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Automatique",
                                "value" => "automatic",
                            ],
                            [
                                "text" => "Manuelle",
                                "value" => "manual",
                            ],
                        ],
                    ],
                    "engine" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Essence",
                                "value" => "fuel",
                            ],
                            [
                                "text" => "Diesel",
                                "value" => "diesel",
                            ],
                            [
                                "text" => "Électrique",
                                "value" => "electric",
                            ],
                            [
                                "text" => "Hybride",
                                "value" => "hybrid",
                            ],
                        ],
                    ],
                    "plate_number" => [
                        "type" => "text",
                    ],
                    "value_category" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "50 000$ et moins",
                                "value" => "lte50k",
                            ],
                            [
                                "text" => "50 001$ à 70 000$",
                                "value" => "lte70k",
                            ],
                            [
                                "text" => "70 001$ à 100 000$",
                                "value" => "lte100k",
                            ],
                        ],
                    ],
                    "has_onboard_notebook" => [
                        "type" => "checkbox",
                    ],
                    "has_report_in_notebook" => [
                        "type" => "checkbox",
                    ],
                    "report" => [
                        "type" => "file",
                    ],
                    "papers_location" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Dans l'auto",
                                "value" => "in_the_car",
                            ],
                            [
                                "text" => "À récupérer avec la clé",
                                "value" => "to_request_with_car",
                            ],
                        ],
                    ],
                    "insurer" => [
                        "type" => "text",
                    ],
                    "has_informed_insurer" => [
                        "type" => "checkbox",
                    ],
                ],
                "trailer" => [
                    "maximum_charge" => [
                        "type" => "text",
                    ],
                    "dimensions" => [
                        "type" => "text",
                    ],
                ],
            ],
        ];

        $user = $request->user();

        $generalRules = $this->model->getRules("template", $user);
        $generalRulesKeys = array_keys($generalRules);
        foreach ($generalRules as $field => $rules) {
            if (!isset($template["form"]["general"][$field])) {
                continue;
            }
            $template["form"]["general"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $bikeRules = Bike::getRules("template", $user);
        foreach ($bikeRules as $field => $rules) {
            if (in_array($field, $generalRulesKeys)) {
                continue;
            }
            if (!isset($template["form"]["bike"][$field])) {
                continue;
            }
            $template["form"]["bike"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $carRules = Car::getRules("template", $user);
        foreach ($carRules as $field => $rules) {
            if (in_array($field, $generalRulesKeys)) {
                continue;
            }
            if (!isset($template["form"]["car"][$field])) {
                continue;
            }
            $template["form"]["car"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        return $template;
    }

    public function dashboard(Request $request)
    {
        $user = $request->user();

        $ownedLoanables = Loanable::whereHas("mergedUserRoles", function (
            Builder $q
        ) use ($user) {
            $q->where("role", LoanableUserRoles::Owner)->where(
                "user_id",
                $user->id
            );
        })->with(["image", "activeIncidents", "userRoles.user.avatar"]);

        $coOwnedLoanables = Loanable::whereHas("mergedUserRoles", function (
            Builder $q
        ) use ($user) {
            $q->whereIn("role", [
                LoanableUserRoles::Coowner,
                LoanableUserRoles::Manager,
            ])->where("user_id", $user->id);
        })->with([
            "image",
            "activeIncidents",
            "userRoles.user.avatar",
            "library.avatar",
        ]);

        $libraries = Library::whereHas("userRoles", function (Builder $q) use (
            $user
        ) {
            $q->where("user_id", $user->id)->whereIn("role", [
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ]);
        })->with(["avatar"]);

        return [
            "loanables" => [
                "owned" => [
                    "total" => $ownedLoanables->count(),
                    "data" => DashboardLoanableResource::collection(
                        $ownedLoanables->limit(5)->get()
                    ),
                ],
                "managed" => [
                    "total" => $coOwnedLoanables->count(),
                    "data" => DashboardLoanableResource::collection(
                        $coOwnedLoanables->limit(5)->get()
                    ),
                ],
            ],
            "libraries" => [
                "total" => $libraries->count(),
                "data" => LibraryResource::collection(
                    $libraries->limit(5)->get()
                ),
            ],
        ];
    }

    protected function validateAction(array $data, User $user, string $action)
    {
        parent::validateAction($data, $user, $action);

        if ($action === "destroy") {
            return;
        }

        $loanableDetails = LoanableTypes::from(
            $data["type"]
        )->getLoanableModel();

        $detailsValidator = Validator::make(
            $data["details"],
            $loanableDetails->getRules($action, $user),
            $loanableDetails::$validationMessages
        );

        if ($detailsValidator->fails()) {
            throw new ValidationException($detailsValidator);
        }
    }

    public function createUserRole(Request $request, Loanable $loanable)
    {
        Gate::authorize("createUserRole", [$loanable, $request->all()]);

        // Users must share community with owner.
        $ownerUser = $loanable->owner_user;

        $userForRole = User::findOrFail($request->get("user_id"));
        if (!$userForRole->approvedInCommunityWith($ownerUser->id)) {
            return ErrorResponse::withMessage(
                "La personne doit être approuvée dans une communauté dans laquelle se trouve le propriétaire du véhicule.",
                403
            );
        }

        $userRole = $loanable
            ->userRoles()
            ->make($request->validate(LoanableUserRole::getRules("create")));
        $userRole->granted_by_user_id = $request->user()->id;
        $userRole->save();

        event(
            new LoanableUserRoleAdded($request->user(), $loanable, $userRole)
        );

        return (new LoanableUserRoleResource($userRole->load("user.avatar")))
            ->response()
            ->setStatusCode(201);
    }

    public function viewUserRoles(BaseRequest $request, Loanable $loanable)
    {
        Gate::authorize("viewUserRoles", $loanable);

        $request->merge([
            "loanable_id" => $loanable->id,
        ]);

        return LoanableUserRoleResource::collection(
            WebQueryBuilder::for(
                $request,
                new MergedLoanableUserRole()
            )->paginate()
        );
    }
}
