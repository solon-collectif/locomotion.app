<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\RefreshToken;
use Laravel\Passport\Token;
use Symfony\Component\Process\Exception\ProcessSignaledException;
use Symfony\Component\Process\Process;

class SchedulerController extends Controller
{
    private static function validateAndCall(
        $appKey,
        string $command,
        $params = []
    ) {
        if ($appKey !== config("app.key")) {
            return new Response("Wrong app key", 403);
        }

        Artisan::call($command, $params);
    }

    public static function loansForward($appKey)
    {
        return self::validateAndCall($appKey, "loans:forward");
    }

    public static function emailLoanUpcoming($appKey)
    {
        return self::validateAndCall($appKey, "email:loan:upcoming");
    }

    public static function emailPrePayement($appKey)
    {
        return self::validateAndCall($appKey, "email:loan:pre_payment_missing");
    }

    public static function filesCleanDB(Request $request, $appKey)
    {
        $params = ["--pretend" => $request->boolean("pretend", true)];

        return self::validateAndCall($appKey, "files:clean:db", $params);
    }

    public static function filesCleanStorage(Request $request, $appKey)
    {
        $params = [
            "--pretend" => $request->boolean("pretend", true),
            "--verbose" => $request->boolean("verbose", false),
            "--delete-missing" => $request->boolean("delete-missing", false),
        ];

        return self::validateAndCall($appKey, "files:clean:storage", $params);
    }

    public static function imagesGenerateSizes(Request $request, $appKey)
    {
        $params = [
            "--verbose" => $request->boolean("verbose", false),
            "--regenerate" => $request->string("regenerate", null),
        ];

        return self::validateAndCall($appKey, "images:generate:sizes", $params);
    }

    public static function maintainDb($appKey)
    {
        if ($appKey !== config("app.key")) {
            return new Response("Wrong app key", 403);
        }

        Artisan::call("auth:clear-resets");
        Artisan::call("queue:prune-batches");
        Artisan::call("queue:prune-failed");
        // For some reason we cannot call passport purge with the Artisan::call syntax.
        self::runArtisanAsProcess(
            "php artisan passport:purge",
            "passport:purge"
        );
    }

    public static function queueWork()
    {
        // Do not wait for tasks in the queue if it is empty. (min request time ~0s)
        self::runArtisanAsProcess(
            "php artisan queue:work --max-time=20 --timeout=30 --sleep=0 --stop-when-empty --queue=high,default,low,reports",
            "queue:work"
        );
    }
    public static function runArtisanAsProcess($command, $label)
    {
        $workerProcess = new Process(explode(" ", $command), base_path(), [
            ...getenv(),
            "XDEBUG_MODE" => "off",
        ]);

        $workerProcess->start();

        try {
            $currentLine = "";
            foreach ($workerProcess as $output) {
                $currentLine .= $output;
                if (str_ends_with($output, "\n")) {
                    $currentLine = rtrim($currentLine);
                    \Log::info("[$label] $currentLine");
                    $currentLine = "";
                }
            }
            $workerProcess->wait();
        } catch (ProcessSignaledException $e) {
            if ($e->getSignal() === 9) {
                \Log::warning("[$label] Timed out.");
                return;
            }
            throw $e;
        }
    }

    public static function updatePasswordExpiration($appKey)
    {
        if ($appKey !== config("app.key")) {
            return new Response("Wrong app key", 403);
        }

        $latestExpiration = Carbon::now()->addDays(
            config("auth.token.expiration_in_days")
        );

        Token::where("revoked", false)
            ->where("expires_at", ">", $latestExpiration)
            ->update([
                "expires_at" => $latestExpiration,
            ]);

        RefreshToken::where("revoked", false)
            ->where("expires_at", ">", $latestExpiration)
            ->update([
                "expires_at" => $latestExpiration,
            ]);
    }

    public static function contributionsSplit($appKey)
    {
        return self::validateAndCall($appKey, "contributions:split");
    }
}
