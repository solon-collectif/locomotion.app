<?php

namespace App\Http\Controllers;

use App\Events\RegistrationSubmittedEvent;
use App\Http\Requests\BaseRequest;
use App\Http\WebQueryBuilder;
use App\Models\File;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Repositories\RestRepository;
use Carbon\Carbon;

class CommunityUserController extends RestController
{
    public function __construct(CommunityUser $model)
    {
        $this->repo = new RestRepository($model);
        $this->model = $model;
    }

    public function updateCommunityUser(
        BaseRequest $request,
        CommunityUser $communityUser
    ) {
        \Gate::authorize("update", $communityUser);

        $rules = CommunityUser::getRules("update", $request->user());

        $validated = $request->validate($rules);
        $communityUser->fill($validated);
        $communityUser->save();

        return $this->transformItem($request, $communityUser);
    }

    public function updateProofs(
        BaseRequest $request,
        CommunityUser $communityUser
    ) {
        \Gate::authorize("updateProofs", $communityUser);

        $community = $communityUser->community;

        // Proof has changed if the number of files, or the ids of the files
        // are different.
        $proofsChanged = false;
        $requestProofs = $request->json()->all();
        $user = $communityUser->user;

        $proofParams = [
            "identity_proof" => [
                "required" => $community->requires_identity_proof,
                "target" => $user->identityProof(),
            ],
            "residency_proof" => [
                "required" => $community->requires_residency_proof,
                "target" => $user->residencyProof(),
            ],
            "custom_proof" => [
                "required" => $community->requires_custom_proof,
                "target" => $communityUser->customProof(),
            ],
        ];

        foreach ($proofParams as $proofName => $params) {
            $proofIds = array_map(
                function ($p) {
                    return $p["id"];
                },
                array_key_exists($proofName, $requestProofs)
                    ? $requestProofs[$proofName]
                    : []
            );
            $previousProofIds = $params["target"]
                ->get()
                ->map(function ($p) {
                    return $p->id;
                })
                ->toArray();

            if (
                sizeof($proofIds) !== sizeof($previousProofIds) ||
                array_sort($proofIds) != array_sort($previousProofIds)
            ) {
                $proofsChanged = true;

                // Delete unsubmitted proofs if exist.
                $params["target"]->whereNotIn("id", $proofIds)->delete();

                // Add new proofs if required.
                if ($params["required"]) {
                    $proofFiles = [];
                    foreach ($requestProofs[$proofName] as $proofData) {
                        if (in_array($proofData["id"], $previousProofIds)) {
                            continue;
                        }

                        $proofFile = File::find($proofData["id"]);
                        $proofFiles[] = $proofFile;
                    }

                    $params["target"]->saveMany($proofFiles);
                }
            }
        }

        if ($proofsChanged) {
            $communityUser->refresh();

            // Has all required proofs (not required, or present)?
            if (
                (!$community->requires_identity_proof ||
                    $communityUser->user->identityProof->isNotEmpty()) &&
                (!$community->requires_residency_proof ||
                    $communityUser->user->residencyProof->isNotEmpty()) &&
                (!$community->requires_custom_proof ||
                    $communityUser->customProof->isNotEmpty())
            ) {
                event(new RegistrationSubmittedEvent($communityUser));
            }
        }

        return $this->transformItem($request, $communityUser);
    }

    public function approve(BaseRequest $request, CommunityUser $communityUser)
    {
        \Gate::authorize("approve", $communityUser);

        $communityUser->approve($request->user()->id, $request->input("note"));
        $communityUser->load("approver");

        return $this->transformItem($request, $communityUser);
    }

    public function suspend(BaseRequest $request, CommunityUser $communityUser)
    {
        \Gate::authorize("suspend", $communityUser);

        $communityUser->suspended_at = Carbon::now();
        $communityUser->save();

        return $this->transformItem($request, $communityUser);
    }

    public function unsuspend(
        BaseRequest $request,
        CommunityUser $communityUser
    ) {
        \Gate::authorize("unsuspend", $communityUser);

        $communityUser->suspended_at = null;
        $communityUser->save();

        return $this->transformItem($request, $communityUser);
    }

    public function exportMailboxes(BaseRequest $request, int $communityId)
    {
        \Gate::authorize("exportMailboxes", [User::class, $communityId]);

        if ($request->headers->get("accept") == "text/csv") {
            $request->merge([
                "fields" => "user.id,user.name,user.last_name,user.email",
                "community_id" => $communityId,
            ]);
            return $this->respondWithCsv($request, $this->model);
        }

        $query = WebQueryBuilder::for($request, new CommunityUser());

        // Ensure order exists if not set
        if (empty($query->getQuery()->orders)) {
            $query->orderBy("id");
        }

        $communityUsers = $query
            ->where("community_id", "$communityId")
            ->with("user:id,name,last_name,email")
            ->select("user_id")
            ->get();

        return $communityUsers
            ->map(fn(CommunityUser $u) => $u->user->getMailbox())
            ->join(",\n");
    }
}
