<?php

namespace App\Http\Controllers;

use App\Reports\InsuranceQuarterlyReport;
use App\Reports\UserBalanceReport;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;

class ReportController
{
    public function generateInsuranceReport(Request $request)
    {
        \Gate::allowIf($request->user()->isAdmin());

        $tmpfilehandle = tmpfile();

        InsuranceQuarterlyReport::generate(
            new CarbonImmutable($request->string("from"), "America/Toronto"),
            new CarbonImmutable($request->string("to"), "America/Toronto"),
            "csv",
            $tmpfilehandle
        );

        rewind($tmpfilehandle);

        return response()->streamDownload(
            function () use ($tmpfilehandle) {
                echo stream_get_contents($tmpfilehandle);
            },
            "report.csv",
            ["content-type" => "text/csv"]
        );
    }

    public function generateBalanceReport(Request $request)
    {
        \Gate::allowIf($request->user()->isAdmin());

        $tmpFileHandle = tmpfile();

        UserBalanceReport::generate(
            userId: $request->has("user_id")
                ? $request->integer("user_id")
                : null,
            minUserId: $request->has("min_user_id")
                ? $request->integer("min_user_id")
                : null,
            at: $request->has("at")
                ? new CarbonImmutable($request->string("at"), "America/Toronto")
                : null,
            nonZeroOnly: $request->boolean("non_zero_only"),
            outputStream: $tmpFileHandle
        );

        rewind($tmpFileHandle);

        return response()->streamDownload(
            function () use ($tmpFileHandle) {
                echo stream_get_contents($tmpFileHandle);
            },
            "report.csv",
            ["content-type" => "text/csv"]
        );
    }
}
