<?php

namespace App\Http\Controllers;

use App\Models\BillItem;
use App\Repositories\InvoiceItemRepository;

class InvoiceItemController extends RestController
{
    public function __construct(
        InvoiceItemRepository $repository,
        BillItem $model
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }
}
