<?php

namespace App\Http\Controllers;

use App\Enums\IncidentNotificationLevel;
use App\Enums\LoanableUserRoles;
use App\Events\IncidentAssigneeChangedEvent;
use App\Events\IncidentCreatedEvent;
use App\Events\IncidentNoteAddedEvent;
use App\Events\IncidentReopenedEvent;
use App\Events\IncidentResolvedEvent;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Resources\IncidentNoteResource;
use App\Http\Resources\IncidentResource;
use App\Mail\Incidents\LoansBlocked;
use App\Mail\UserMail;
use App\Models\Incident;
use App\Models\IncidentNote;
use App\Models\IncidentNotification;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\MergedLoanableUserRole;
use App\Models\User;
use App\Repositories\IncidentRepository;
use Carbon\CarbonImmutable;
use Gate;

class IncidentController extends RestController
{
    public function __construct(IncidentRepository $repository, Incident $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function index(Request $request)
    {
        return IncidentResource::collection(
            self::getPaginatedItems($request, new Incident())
        );
    }

    public function getConcernedUserIds(
        Incident $incident,
        bool $includeCommunityAdmins = false
    ) {
        $concernedUserIds = $incident->loanable->mergedUserRoles
            ->filter(
                fn(MergedLoanableUserRole $userRole) => in_array(
                    $userRole->role,
                    [
                        LoanableUserRoles::Owner,
                        LoanableUserRoles::Manager,
                        LoanableUserRoles::Coowner,
                    ]
                )
            )
            ->map(fn(MergedLoanableUserRole $userRole) => $userRole->user_id);

        foreach (User::globalAdmins()->pluck("id") as $globalAdminId) {
            $concernedUserIds->add($globalAdminId);
        }

        if ($includeCommunityAdmins && $incident->loan) {
            foreach (
                User::adminOfCommunity($incident->loan->community_id)->pluck(
                    "id"
                )
                as $admin
            ) {
                $concernedUserIds->add($admin);
            }
        }

        if ($incident->loan) {
            $concernedUserIds->add($incident->loan->borrower_user_id);
        }

        if ($incident->assignee_id) {
            $concernedUserIds->add($incident->assignee_id);
        }

        if ($incident->resolved_by_user_id) {
            $concernedUserIds->add($incident->resolved_by_user_id);
        }

        return $concernedUserIds->toBase()->unique();
    }

    public function subscribeAllConcernedUsers(Incident $incident)
    {
        $concernedUserIds = $this->getConcernedUserIds($incident);

        // Avoid changing existing subscription for users
        $currentlySubscribedUserIds = $incident
            ->notifiedUsers()
            ->pluck("users.id");
        $concernedUserIds = $concernedUserIds->diff(
            $currentlySubscribedUserIds
        );

        $incident->notifiedUsers()->syncWithPivotValues(
            $concernedUserIds->all(),
            [
                "level" => IncidentNotificationLevel::All,
            ],
            detaching: false
        );
    }

    public function syncBlockedBorrowerNotifications(Incident $incident)
    {
        if (!$incident->blocking_until) {
            return;
        }

        $subscribedUsers = IncidentNotification::where(
            "incident_id",
            $incident->id
        )->pluck("user_id");

        $blockedLoans = $incident->blockedLoans;
        $usersToSubscribe = $blockedLoans
            ->map(fn(Loan $loan) => $loan->borrower_user_id)
            ->toBase()
            ->diff($subscribedUsers)
            ->unique()
            ->toArray();

        // Subscribe newly blocked borrowers on incident resolved
        $incident->notifiedUsers()->attach($usersToSubscribe, [
            "level" => IncidentNotificationLevel::ResolvedOnly,
        ]);

        // Notify newly blocked borrowers of their blocked loans
        $blockedLoansPerBorrower = $blockedLoans
            ->groupBy(fn(Loan $loan) => $loan->borrower_user_id)
            ->diffKeys($subscribedUsers->flip());
        foreach ($blockedLoansPerBorrower as $blockedLoansList) {
            $borrowerUser = $blockedLoansList->first()->borrowerUser;
            UserMail::queue(
                new LoansBlocked($borrowerUser, $blockedLoansList, $incident),
                $borrowerUser
            );
        }
    }

    /**
     * @param Incident $incident
     * @param bool $removeInvalidSubscriptions If true, this will fully remove notification
     * prefrences which are not 'None'. Otherwise, this will simply demote notifications prefrences
     * from 'All' to 'ResolvedOnly'.
     */
    public function ensureValidNotificationSubscriptions(
        Incident $incident,
        bool $removeInvalidSubscriptions = false
    ) {
        $validAllNotificationSubscriberIds = $this->getConcernedUserIds(
            $incident,
            includeCommunityAdmins: true
        );
        $blockedLoans = $incident->blockedLoans;

        if ($incident->show_details_to_blocked_borrowers) {
            $validAllNotificationSubscriberIds = $validAllNotificationSubscriberIds->merge(
                $blockedLoans->map(fn(Loan $loan) => $loan->borrower_user_id)
            );
        }

        if (!$removeInvalidSubscriptions) {
            IncidentNotification::where("incident_id", $incident->id)
                ->where("level", IncidentNotificationLevel::All)
                ->whereNotIn("user_id", $validAllNotificationSubscriberIds)
                ->update([
                    "level" => IncidentNotificationLevel::ResolvedOnly,
                ]);
        } else {
            IncidentNotification::where("incident_id", $incident->id)
                ->where("level", "!=", IncidentNotificationLevel::None)
                ->whereNotIn("user_id", $validAllNotificationSubscriberIds)
                ->delete();
        }
    }

    public function changeSubscriptionLevel(
        Request $request,
        Incident $incident
    ) {
        $validOptions = [
            IncidentNotificationLevel::All,
            IncidentNotificationLevel::ResolvedOnly,
            IncidentNotificationLevel::None,
        ];
        $request->validate([
            "level" => [
                "in:" .
                implode(
                    ",",
                    array_map(
                        fn(IncidentNotificationLevel $o) => $o->value,
                        $validOptions
                    )
                ),
            ],
        ]);

        if ($request->level === IncidentNotificationLevel::All->value) {
            Gate::authorize("viewIncidentDetails", $incident);
        }

        $incident
            ->notifiedUsers()
            ->syncWithPivotValues(
                $request->user(),
                ["level" => $request->level],
                detaching: false
            );
    }

    public function create(Request $request)
    {
        Gate::authorize("create", [
            Incident::class,
            Loan::find($request->loan_id),
            Loanable::find($request->loanable_id),
        ]);
        $incident = new Incident(
            $request->validate(Incident::getRules("create"))
        );
        $incident->reported_by_user_id = $request->user()->id;
        $incident->status = "in_process";

        if ($incident->incident_type === "accident") {
            $incident->blocking_until = CarbonImmutable::now()->addDay();
        }

        $incident->save();

        $incident->refresh();
        $this->subscribeAllConcernedUsers($incident);
        $this->syncBlockedBorrowerNotifications($incident);
        $incident->load("notifiedUsers");
        event(new IncidentCreatedEvent($incident));

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }
    public function update(Request $request, Incident $incident)
    {
        Gate::authorize("updateDescription", $incident);
        $incident->fill(
            $request->validate([
                "comments_on_incident" => ["required"],
            ])
        );
        $incident->save();

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }

    public function complete(Request $request, Incident $incident)
    {
        Gate::authorize("resolve", $incident);

        $incident->status = "completed";
        $incident->resolved_by_user_id = $request->user()->id;
        $incident->blocking_until = null;
        $incident->save();

        // Lower notification level for blocked borrowers which are no longer blocked
        $this->ensureValidNotificationSubscriptions($incident);

        event(new IncidentResolvedEvent($incident));

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }

    public function reopen(Request $request, Incident $incident)
    {
        Gate::authorize("reopen", $incident);

        $incident->status = "in_process";
        $incident->resolved_by_user_id = null;
        $incident->save();

        $this->subscribeAllConcernedUsers($incident);
        // We want to fully remove invalid subscriptions (no longer blocked borrowers) so they get
        // alerted if we re-block loans with this incident.
        $this->ensureValidNotificationSubscriptions(
            $incident,
            removeInvalidSubscriptions: true
        );

        event(new IncidentReopenedEvent($incident, $request->user()));

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }

    public function addNote(Request $request, Incident $incident)
    {
        Gate::authorize("addNote", $incident);

        $note = $incident->notes()->make($request->all());
        $note->author_id = $request->user()->id;
        $note->save();

        // Update the 'updated_at' column
        $incident->touch();

        event(new IncidentNoteAddedEvent($note));

        return new IncidentNoteResource($note->load("author.avatar"));
    }

    public function updateNote(
        Request $request,
        Incident $incident,
        IncidentNote $note
    ) {
        Gate::authorize("updateNote", $note);

        $note->fill($request->all());
        $note->save();

        return new IncidentNoteResource($note->load("author.avatar"));
    }

    public function deleteNote(Incident $incident, IncidentNote $note)
    {
        Gate::authorize("deleteNote", $note);

        $note->delete();
    }

    public function setAssignee(Request $request, Incident $incident)
    {
        Gate::authorize("changeAssignee", $incident);

        $request->validate([
            "assignee_id" => ["nullable"],
        ]);

        if ($request->assignee_id) {
            if (
                !User::assignableToIncident($incident->id)
                    ->where("id", $request->assignee_id)
                    ->exists()
            ) {
                abort(422, "This user cannot be assigned to this incident");
            }
        }

        $incident->assignee_id = $request->assignee_id;
        $incident->save();

        if ($incident->assignee) {
            // Subscribe assigned user to all notifications
            $incident->notifiedUsers()->syncWithPivotValues(
                [$incident->assignee_id],
                [
                    "level" => IncidentNotificationLevel::All,
                ],
                detaching: false
            );

            event(
                new IncidentAssigneeChangedEvent($incident, $request->user())
            );
        }

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }

    public function setBlockingUntil(Request $request, Incident $incident)
    {
        Gate::authorize("changeAssignee", $incident);

        $request->validate([
            "blocking_until" => ["nullable", "date"],
            "show_details_to_blocked_borrowers" => ["boolean"],
        ]);

        $incident->blocking_until = $request->blocking_until;
        if ($incident->blocking_until) {
            $incident->show_details_to_blocked_borrowers = $request->get(
                "show_details_to_blocked_borrowers",
                $incident->show_details_to_blocked_borrowers
            );
            $incident->save();
            $this->syncBlockedBorrowerNotifications($incident);
        } else {
            $incident->show_details_to_blocked_borrowers = false;
            $incident->save();
        }

        $this->ensureValidNotificationSubscriptions($incident);

        return new IncidentResource(
            $incident->load(
                "assignee.avatar",
                "resolvedByUser.avatar",
                "notes.author.avatar",
                "reportedByUser.avatar"
            )
        );
    }
}
