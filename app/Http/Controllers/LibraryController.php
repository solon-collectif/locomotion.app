<?php

namespace App\Http\Controllers;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Calendar\Interval;
use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Events\LoanableUserRoleAdded;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\CommunityOverviewResource;
use App\Http\Resources\DashboardLoanableResource;
use App\Http\Resources\LibraryResource;
use App\Http\Resources\LoanableUserRoleResource;
use App\Http\WebQueryBuilder;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class LibraryController
{
    use AuthorizesRequests;

    public function index(BaseRequest $request)
    {
        $this->authorize("viewAny", Library::class);

        return LibraryResource::collection(
            WebQueryBuilder::for($request, new Library())->paginate()
        );
    }

    public function create(Request $request)
    {
        Gate::authorize("create", Library::class);

        $data = $request->validate([
            "name" => ["required"],
            "phone_number" => ["nullable"],
            "owner_user_id" => ["required", "exists:users,id"],
            "loan_auto_validation_question" => ["nullable", "max:4048"],
            "loan_auto_validation_answer" => ["nullable", "max:100"],
        ]);
        $library = Library::create($data);

        if ($request->has("avatar")) {
            $library->setMorphOne("avatar", $request->input("avatar.id"));
        }

        $userRole = $library->userRoles()->make([
            "pays_loan_insurance" => false,
            "pays_loan_price" => false,
        ]);
        $userRole->user_id = $data["owner_user_id"];
        $userRole->role = LoanableUserRoles::Owner;
        $userRole->granted_by_user_id = $request->user()->id;
        $userRole->save();

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function show(Library $library)
    {
        Gate::authorize("view", $library);

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function events(BaseRequest $request, Library $library)
    {
        Gate::authorize("view", $library);
        $range = Interval::of($request->get("start"), $request->get("end"));

        $loanableId = $request->get("loanable_id");

        $events = [];

        $loanQuery = Loan::query()
            ->accessibleBy($request->user())
            ->forLibrary($library->id)
            ->where("status", "!=", LoanStatus::Canceled)
            ->where("status", "!=", LoanStatus::Rejected)
            ->intersects($range)
            ->orderBy("departure_at")
            ->with(["borrowerUser", "loanable"]);

        if ($loanableId) {
            $loanQuery->where("loanable_id", $loanableId);
        }

        foreach ($loanQuery->get() as $loan) {
            $events[] = [
                "type" => "loan",
                "start" => $loan->departure_at
                    ->tz($loan->loanable->timezone)
                    ->toDateTimeString(),
                "end" => $loan->actual_return_at
                    ->tz($loan->loanable->timezone)
                    ->toDateTimeString(),
                "uri" => "/loans/$loan->id",
                "data" => [
                    "status" => $loan->status,
                    "accepted" => $loan->is_accepted,
                    "loanable_name" => $loan->loanable->name,
                    "loan_id" => $loan->id,
                    "loanable_id" => $loan->loanable_id,
                    "borrower_name" => $loan->borrowerUser->name,
                    "action_required" => $loan->userActionRequired(
                        $request->user()
                    ),
                ],
            ];
        }

        return response($events, 200);
    }

    public function update(Request $request, Library $library)
    {
        Gate::authorize("update", $library);

        $data = $request->validate([
            "name" => ["required"],
            "phone_number" => ["nullable"],
            "loan_auto_validation_question" => ["nullable", "max:4048"],
            "loan_auto_validation_answer" => ["nullable", "max:100"],
        ]);

        $library->update($data);

        if ($request->has("avatar")) {
            $library->setMorphOne("avatar", $request->input("avatar.id"));
        }

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function destroy(Request $request, Library $library)
    {
        Gate::authorize("delete", $library);

        // Find the library owner
        $libraryOwner = $library
            ->userRoles()
            ->where("role", LoanableUserRoles::Owner)
            ->first();

        // Make sure ownership of each library loanables is transferred to the library owner
        if ($libraryOwner) {
            foreach ($library->loanables as $loanable) {
                // Assign the library owner as the loanable owner
                $userRole = $loanable->userRoles()->make([
                    "show_as_contact" => true,
                    "pays_loan_price" => $libraryOwner->pays_loan_price,
                    "pays_loan_insurance" => $libraryOwner->pays_loan_insurance,
                ]);

                $userRole->user_id = $libraryOwner->user_id;
                $userRole->role = LoanableUserRoles::Owner;
                $userRole->granted_by_user_id = $request->user()->id;
                $userRole->save();
            }
        }

        $library->userRoles()->delete();
        $library->delete();
        return response()->json();
    }
    public function viewLoanables(BaseRequest $request, Library $library)
    {
        Gate::authorize("viewLoanables", $library);

        $request->validate([
            "per_page" => ["nullable", "integer", "min:1", "max:100"],
            "page" => ["nullable", "integer", "min:1"],
            "with_loan_summary" => ["nullable", "boolean"],
        ]);

        $request->merge([
            "library_id" => $library->id,
        ]);

        $builder = WebQueryBuilder::for($request, new Loanable());
        if ($request->boolean("with_loan_summary")) {
            $builder->with(
                "loans",
                fn(HasMany $builder) => $builder
                    ->where(
                        "actual_return_at",
                        ">",
                        CarbonImmutable::now()->subHour()
                    )
                    ->where(
                        "departure_at",
                        "<",
                        CarbonImmutable::now()->addDays(7)
                    )
                    ->whereNotIn("status", [
                        LoanStatus::Canceled,
                        LoanStatus::Rejected,
                    ])
                    ->orderBy("departure_at")
                    ->limit(3)
                    ->with([
                        "community",
                        "borrowerUser.avatar",
                        "borrowerUser.borrower",
                        "borrowerUser.subscriptions.communityUser",
                        "loanable.userRoles.user.approvedCommunities.allowedLoanableTypes",
                        "loanable.userRoles.user.avatar",
                        "loanable.activeIncidents",
                        "loanable.image",
                    ])
            );
        }

        return DashboardLoanableResource::collection($builder->paginate());
    }

    public function addLoanable(
        Request $request,
        Library $library,
        Loanable $loanable
    ) {
        Gate::authorize("addLoanable", [$library, $loanable]);

        $loanable
            ->userRoles()
            ->where("role", LoanableUserRoles::Owner)
            ->delete();
        $library->loanables()->save($loanable);

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function removeLoanable(
        Request $request,
        Library $library,
        Loanable $loanable
    ) {
        Gate::authorize("removeLoanable", [$library, $loanable]);

        $loanable->library()->dissociate();
        $loanable->save();

        // Find the library owner
        $libraryOwner = $library
            ->userRoles()
            ->where("role", LoanableUserRoles::Owner)
            ->first();

        if ($libraryOwner) {
            // Assign the library owner as the loanable owner
            $userRole = $loanable->userRoles()->make([
                "show_as_contact" => true,
                "pays_loan_price" => $libraryOwner->pays_loan_price,
                "pays_loan_insurance" => $libraryOwner->pays_loan_insurance,
            ]);

            $userRole->user_id = $libraryOwner->user_id;
            $userRole->role = LoanableUserRoles::Owner;
            $userRole->granted_by_user_id = $request->user()->id;
            $userRole->save();
        }

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function addCommunity(
        Request $request,
        Library $library,
        Community $community
    ) {
        Gate::authorize("addToCommunity", [$library, $community]);

        $library->communities()->attach($community);

        LoanablesByTypeAndCommunity::forgetForCommunity($community->id);

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function removeCommunity(
        Request $request,
        Library $library,
        Community $community
    ) {
        Gate::authorize("removeFromCommunity", [$library, $community]);

        $library->communities()->detach($community);
        LoanablesByTypeAndCommunity::forgetForCommunity($community->id);

        return new LibraryResource(
            $library->load(["userRoles.user", "avatar"])
        );
    }

    public function viewCommunities(BaseRequest $request, Library $library)
    {
        Gate::authorize("viewCommunities", $library);

        $request->merge([
            "libraries.id" => $library->id,
        ]);

        return CommunityOverviewResource::collection(
            WebQueryBuilder::for($request, new Community())->paginate()
        );
    }

    public function createUserRole(Request $request, Library $library)
    {
        Gate::authorize("createUserRole", [$library, $request->all()]);

        $data = $request->validate([
            "user_id" => ["required", "exists:users,id"],
            "role" => ["required", "in:manager"],
            "title" => "nullable|string",
            "show_as_contact" => "boolean",
            "pays_loan_price" => "boolean",
            "pays_loan_insurance" => "boolean",
        ]);

        $userRole = $library->userRoles()->make($data);
        $userRole->user_id = $request->get("user_id");
        $userRole->role = $request->get("role");
        $userRole->granted_by_user_id = $request->user()->id;
        try {
            $userRole->save();
        } catch (QueryException $e) {
            // If due to unique key violation
            if ($e->getCode() == 23505) {
                abort(422, __("state.loanable.user_already_has_role"));
            }
        }

        $removeExistingUserRoles = $request->boolean(
            "remove_existing_coowner_roles"
        );
        if ($removeExistingUserRoles) {
            LoanableUserRole::where("user_id", $request->get("user_id"))
                ->where("ressource_type", "loanable")
                ->where("role", LoanableUserRoles::Coowner)
                ->whereExists(
                    fn($query) => $query
                        ->select(\DB::raw(1))
                        ->from("loanables")
                        ->whereColumn(
                            "loanables.id",
                            "loanable_user_roles.ressource_id"
                        )
                        ->where("loanables.library_id", $library->id)
                )
                ->delete();
        }

        event(new LoanableUserRoleAdded($request->user(), $library, $userRole));

        return new LoanableUserRoleResource(
            $userRole->load(["user.avatar", "grantedByUser"])
        );
    }

    public function viewUserRoles(BaseRequest $request, Library $library)
    {
        $this->authorize("viewUserRoles", $library);

        $request->merge([
            "ressource_type" => "library",
            "ressource_id" => $library->id,
        ]);

        return LoanableUserRoleResource::collection(
            WebQueryBuilder::for($request, new LoanableUserRole())->paginate()
        );
    }
}
