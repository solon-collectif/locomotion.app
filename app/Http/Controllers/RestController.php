<?php

namespace App\Http\Controllers;

use App\Exports\BaseExport;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Resources\ExportResource;
use App\Http\WebQueryBuilder;
use App\Models\AuthenticatableBaseModel;
use App\Models\BaseModel;
use App\Models\Pivots\BasePivot;
use App\Models\User;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RestController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $repo;
    protected $model;

    protected $canReturnCsv = true;

    public function indexCsv(Request $request)
    {
        if (!$request->user()->isAdmin()) {
            abort(403, "Only global admins can export data");
        }

        return $this->respondWithCsv($request, $this->model);
    }

    public function index(Request $request)
    {
        if (
            $this->canReturnCsv &&
            $request->headers->get("accept") == "text/csv"
        ) {
            return $this->indexCsv($request);
        }

        return $this->transformPaginatorItems(
            $request,
            self::getPaginatedItems($request, $this->model)
        );
    }

    public static function getPaginatedItems(
        $request,
        $model
    ): LengthAwarePaginator {
        $query = WebQueryBuilder::for($request, $model);

        try {
            return $query->paginate();
        } catch (RelationNotFoundException $e) {
            throw ValidationException::withMessages([
                "includes" => "relationship does not exist",
            ]);
        }
    }

    protected function validateAction(array $data, User $user, string $action)
    {
        $validator = Validator::make(
            $data,
            $this->model::getRules($action, $user),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    // Rules which expect a value to be present
    private const requiredLikeRules = ["required", "present", "accepted"];

    private static function isRequiredLikeRule($rule)
    {
        foreach (self::requiredLikeRules as $requiredLikeRule) {
            // This will match rules like "required_if"
            if (starts_with($rule, $requiredLikeRule)) {
                return true;
            }
        }
        return false;
    }

    // Validates input without the required rules for fields which may already be present on the model.
    protected function validateInputForUpdate(array $data, User $user)
    {
        $rules = $this->model::getRules("update", $user);
        $unRequiredRules = [];

        foreach ($rules as $field => $rule) {
            if (is_string($rule)) {
                $rule = explode("|", $rule);
            }
            $rule = array_filter(
                $rule,
                fn($r) => !is_string($r) || !self::isRequiredLikeRule($r)
            );
            if (!empty($rule)) {
                $unRequiredRules[$field] = $rule;
            }
        }

        $validator = Validator::make(
            $data,
            $unRequiredRules,
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    protected function retrieveWithRequestFields($request, $item)
    {
        $fieldsOnlyRequest = (new BaseRequest([
            "fields" => $request->get("fields"),
        ]))->setUserResolver(fn() => $request->user());

        return WebQueryBuilder::for($fieldsOnlyRequest, $item)->find(
            $item->getKey()
        );
    }

    protected function validateAndCreate(
        BaseRequest $request,
        $saveRelations = true
    ) {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "create");

        $item = $this->repo->create($input, $saveRelations);

        return $this->retrieveWithRequestFields($request, $item);
    }

    protected function respondWithItem($request, $item, $status = 200)
    {
        $result = $this->transformItem($request, $item);

        return response($result, $status);
    }

    /**
     * Validates, authorizes and updates the item with given id using the request data.
     */
    protected function validateAndUpdate(BaseRequest $request, $id)
    {
        $input = $request->json()->all();
        // Validatate update input
        $this->validateInputForUpdate($input, $request->user());

        $item = $this->model->findOrFail($id);

        \Gate::authorize("update", [$item, $input]);

        $item->fill($input);

        foreach (
            array_merge($item->items, array_keys($item->morphOnes))
            as $relation
        ) {
            if (isset($input[$relation])) {
                $item->{$relation} = $input[$relation];
            }
        }

        // Validate the model is correct, after the update.
        $this->validateAction($item->toArray(), $request->user(), "update");

        $item = $this->repo->update($request, $id, $input);

        return $this->retrieveWithRequestFields($request, $item);
    }

    protected function validateAndDestroy(BaseRequest $request, $id)
    {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "destroy");

        return $this->repo->destroy($request, $id);
    }

    protected function validateAndRestore($request, $id)
    {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "destroy");

        return $this->repo->restore($request, $id);
    }

    protected function formatRules($rules)
    {
        if (is_string($rules)) {
            $rules = explode("|", $rules);
        }

        $cleanRules = [];
        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }
            $cleanRules[] = $rule;
        }

        $flipRules = array_flip($cleanRules);

        foreach ($flipRules as $key => $rule) {
            if (str_starts_with($key, "in:")) {
                unset($flipRules[$key]);
                [$_, $values] = explode(":", $key);
                $flipRules["oneOf"] = explode(",", $values);
            } elseif (strpos($key, ":")) {
                unset($flipRules[$key]);

                [$rule, $values] = explode(":", $key);
                $flipRules[$rule] = explode(",", $values);
                if (count($flipRules[$rule]) === 1) {
                    $flipRules[$rule] = array_pop($flipRules[$rule]);
                }

                switch ($rule) {
                    case "exists":
                        unset($flipRules[$rule]);
                        break;
                    case "regex":
                        $flipRules[$rule] = substr($flipRules[$rule], 1, -1);
                        break;
                    case "digits":
                        $flipRules[$rule] = (int) $flipRules[$rule];
                        break;
                    case "size":
                        $flipRules["max"] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    case "max":
                        $flipRules["max_value"] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    default:
                        break;
                }
            } else {
                $flipRules[$key] = true;
            }
        }

        return $flipRules;
    }

    protected function transformPaginatorItems(
        $request,
        AbstractPaginator $paginator
    ) {
        return $paginator->setCollection(
            $paginator->transform(
                fn($item) => $this->transformItem($request, $item)
            )
        );
    }

    protected function transformCollection($request, Collection $items)
    {
        return $items->transform(
            fn($item) => $this->transformItem($request, $item)
        );
    }

    protected function transformItem(
        $request,
        BasePivot|BaseModel|AuthenticatableBaseModel $item
    ) {
        return $item->getTransformer()->transform([
            "fields" => $request->getFields(),
            "!fields" => $request->getNotFields(),
            "pivot" => isset($item->pivot) ? $item->pivot : null,
        ]);
    }

    protected function respondWithCsv($request, $model)
    {
        /** @var BaseExport $export */
        $export = $model::$export::fromFields(
            $request->input("fields", ""),
            $model,
            $request,
            $request->input("export_filename", "")
        );

        $export->dispatch();

        return new ExportResource($export->exportData);
    }
}
