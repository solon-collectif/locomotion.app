<?php

namespace App\Http\Controllers;

use App\Calendar\AvailabilityHelper;
use App\Calendar\DateIntervalHelper;
use App\Calendar\Interval;
use App\Helpers\Path;
use App\Http\Requests\BaseRequest;
use App\Http\WebQueryBuilder;
use App\Models\Bike;
use App\Models\Car;
use App\Models\Community;
use App\Models\GbfsDataset;
use App\Models\GbfsDatasetCommunity;
use App\Models\Loanable;
use App\Models\Trailer;
use App\Repositories\RestRepository;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Route;

class GbfsController extends RestController
{
    const latestVersion = "3.0";

    const supportedVersions = ["3.0"];

    const feeds = ["system_information", "vehicle_types", "vehicle_status"];

    public function __construct(GbfsDataset $model)
    {
        $this->model = $model;
        $this->repo = RestRepository::for($model);
    }

    public static function getDatasetNames()
    {
        return GbfsDataset::query()
            ->pluck("name")
            ->values()
            ->toArray();
    }

    public static function hasDataset(string $datasetName)
    {
        return GbfsDataset::query()
            ->where("name", $datasetName)
            ->exists();
    }

    public static function getDatasetCommunities(string $datasetName): array
    {
        return GbfsDatasetCommunity::query()
            ->where("gbfs_dataset_name", $datasetName)
            ->pluck("community_id")
            ->values()
            ->toArray();
    }

    public static function getRoutes()
    {
        Route::get("gbfs", [GbfsController::class, "manifest"]);

        Route::get("gbfs/manifest", [GbfsController::class, "manifest"]);

        Route::get("gbfs/{version}/{dataset}/gbfs", [
            GbfsController::class,
            "gbfs",
        ]);
        Route::get("gbfs/{version}/{dataset}/gbfs_versions", [
            GbfsController::class,
            "gbfs_versions",
        ]);
        foreach (self::feeds as $feed) {
            Route::get("gbfs/{version}/{dataset}/$feed", [
                GbfsController::class,
                $feed,
            ]);
        }
    }

    public function createGbfsResponse(
        array $data,
        string $version = null,
        int $ttl = 300
    ) {
        if (!$version) {
            $version = self::latestVersion;
        }

        return response([
            "last_updated" => Carbon::now()->toRfc3339String(),
            "ttl" => $ttl,
            "version" => $version,
            "data" => $data,
        ])->setMaxAge($ttl);
    }

    public function getFeedUrl(
        string $version,
        string $dataset,
        string $feedName
    ) {
        return Path::url("api/gbfs", $version, $dataset, $feedName);
    }

    public function manifest()
    {
        return self::createGbfsResponse([
            "datasets" => array_map(
                fn($dataset) => [
                    "system_id" => "locomotion_$dataset",
                    "versions" => array_map(
                        fn($version) => [
                            "version" => $version,
                            "url" => self::getFeedUrl(
                                $version,
                                $dataset,
                                "gbfs"
                            ),
                        ],
                        self::supportedVersions
                    ),
                ],
                self::getDatasetNames()
            ),
        ]);
    }

    public function gbfs(string $version, string $dataset)
    {
        if (!self::hasDataset($dataset)) {
            return response("Dataset not found", 404);
        }

        if (!in_array($version, self::supportedVersions)) {
            return response("Version not supported", 404);
        }

        return self::createGbfsResponse(
            [
                "feeds" => array_map(
                    fn(string $feed) => [
                        "name" => $feed,
                        "url" => self::getFeedUrl($version, $dataset, $feed),
                    ],
                    self::feeds
                ),
            ],
            $version
        );
    }

    public function gbfs_versions(string $version, string $dataset)
    {
        if (!self::hasDataset($dataset)) {
            return response("Dataset not found", 404);
        }

        if (!in_array($version, self::supportedVersions)) {
            return response("Version not supported", 404);
        }

        return self::createGbfsResponse(
            [
                "versions" => array_map(
                    fn($v) => [
                        "version" => $v,
                        "url" => self::getFeedUrl($v, $dataset, "gbfs"),
                    ],
                    self::supportedVersions
                ),
            ],
            $version
        );
    }

    public function localized(string $frenchString, string $englishString)
    {
        return [
            [
                "text" => $frenchString,
                "language" => "fr-CA",
            ],
            [
                "text" => $englishString,
                "language" => "en-CA",
            ],
        ];
    }

    public function system_information(string $version, string $dataset)
    {
        if (!self::hasDataset($dataset)) {
            return response("Dataset not found", 404);
        }

        if (!in_array($version, self::supportedVersions)) {
            return response("Version not supported", 404);
        }

        return self::createGbfsResponse(
            [
                "system_id" => "locomotion_$dataset",
                ...config("gbfs.system_information"),
            ],
            $version
        );
    }

    public function vehicle_types(string $version, string $dataset)
    {
        if (!self::hasDataset($dataset)) {
            return response("Dataset not found", 404);
        }

        if (!in_array($version, self::supportedVersions)) {
            return response("Version not supported", 404);
        }

        return self::createGbfsResponse(
            [
                "vehicle_types" => [
                    [
                        "vehicle_type_id" => "car_electric",
                        "form_factor" => "car",
                        "propulsion_type" => "electric",
                        "name" => $this->localized(
                            "auto électrique",
                            "electric car"
                        ),
                        "vehicle_assets" => [
                            "icon_url" => Path::url(
                                "/loanables/car_electric.svg"
                            ),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "car_fuel",
                        "form_factor" => "car",
                        "propulsion_type" => "combustion",
                        "name" => $this->localized("auto", "car"),
                        "vehicle_assets" => [
                            "icon_url" => Path::url("/loanables/car.svg"),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "car_diesel",
                        "form_factor" => "car",
                        "propulsion_type" => "combustion_diesel",
                        "name" => $this->localized("auto", "car"),
                        "vehicle_assets" => [
                            "icon_url" => Path::url("/loanables/car.svg"),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "car_hybrid",
                        "form_factor" => "car",
                        "propulsion_type" => "hybrid",
                        "name" => $this->localized("auto", "car"),
                        "vehicle_assets" => [
                            "icon_url" => Path::url("/loanables/car.svg"),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "trailer",
                        "form_factor" => "other",
                        "propulsion_type" => "human",
                        "name" => $this->localized(
                            "remorque à vélo",
                            "bike trailer"
                        ),
                        "vehicle_assets" => [
                            "icon_url" => Path::url("/loanables/trailer.svg"),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "bike_regular",
                        "form_factor" => "bicycle",
                        "propulsion_type" => "human",
                        "name" => $this->localized("vélo", "bike"),
                        "vehicle_assets" => [
                            "icon_url" => Path::url("/loanables/bike.svg"),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "bike_electric",
                        "form_factor" => "bicycle",
                        "propulsion_type" => "electric_assist",
                        "name" => $this->localized(
                            "vélo électrique",
                            "electric bike"
                        ),
                        "vehicle_assets" => [
                            "icon_url" => Path::url(
                                "/loanables/bike_electric.svg"
                            ),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "bike_cargo",
                        "form_factor" => "cargo_bicycle",
                        "propulsion_type" => "human",
                        "name" => $this->localized("vélo-cargo", "cargo bike"),
                        "vehicle_assets" => [
                            "icon_url" => Path::url(
                                "/loanables/bike_cargo.svg"
                            ),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                    [
                        "vehicle_type_id" => "bike_cargo_electric",
                        "form_factor" => "cargo_bicycle",
                        "propulsion_type" => "human",
                        "name" => $this->localized(
                            "vélo-cargo électrique",
                            "electric cargo bike"
                        ),
                        "vehicle_assets" => [
                            "icon_url" => Path::url(
                                "/loanables/bike_cargo_electric.svg"
                            ),
                            "icon_last_modified" => "2024-06-11",
                        ],
                    ],
                ],
            ],
            $version
        );
    }

    public function vehicle_status(string $version, string $dataset)
    {
        if (!self::hasDataset($dataset)) {
            return response("Dataset not found", 404);
        }

        if (!in_array($version, self::supportedVersions)) {
            return response("Version not supported", 404);
        }

        $loanableQuery = Loanable::query()
            ->with(["nextLoan"])
            ->sharedInSomeCommunity(self::getDatasetCommunities($dataset));

        $sharedLoanables = $loanableQuery
            ->where("shared_publicly", true)
            ->availabilityStatus("has_availabilities")
            ->whereDoesntHave(
                "loans",
                fn($loans) => $loans->isPeriodUnavailable(
                    new Interval(Carbon::now(), Carbon::now()->addMinutes(15))
                )
            )
            ->get();

        $weekWindow = new Interval(Carbon::now(), Carbon::now()->addWeek());

        $smallestLoanInterval = new Interval(
            Carbon::now(),
            Carbon::now()->addMinutes(15)
        );

        $loanableAvailabilities = (clone $sharedLoanables)->map(
            fn(Loanable $loanable) => AvailabilityHelper::getAvailability(
                [
                    "available" => "always" == $loanable->availability_mode,
                    "rules" => $loanable->getAvailabilityRules(),
                ],
                $weekWindow,
                $loanable->timezone
            )
        );
        $availableLoanables = $sharedLoanables
            ->zip($loanableAvailabilities)
            ->filter(
                fn($loanableAndAvailability) => DateIntervalHelper::cover(
                    $loanableAndAvailability[1],
                    $smallestLoanInterval
                )
            );

        return self::createGbfsResponse(
            [
                "vehicles" => $availableLoanables
                    ->map(
                        fn(
                            $loanableAndAvailability
                        ) => self::loanableToVehicleStatus(
                            $loanableAndAvailability[0],
                            $loanableAndAvailability[1],
                            $weekWindow->end
                        )
                    )
                    ->values()
                    ->toArray(),
            ],
            $version
        );
    }

    /**
     * @param Loanable $loanable
     * @param Interval[] $availability
     * @param CarbonInterface $endOfWindow
     * @return array
     */
    private static function loanableToVehicleStatus(
        Loanable $loanable,
        array $availability,
        CarbonInterface $endOfWindow
    ) {
        $loanableData = [
            "vehicle_id" => "$loanable->id",
            "lat" => $loanable->position->lat,
            "lon" => $loanable->position->long,
            "is_reserved" => false,
            "is_disabled" => false,
            "rental_uris" => [
                "web" => Path::url("loans/new?loanable_id=$loanable->id"),
            ],
            "vehicle_type_id" => self::mapToVehicleType($loanable),
        ];

        $nextLoan = $loanable->nextLoan;

        $end = $availability[0]->end;
        if ($nextLoan && $nextLoan->departure_at->isBefore($end)) {
            $end = $nextLoan->departure_at;
        }

        if (!$end->isSameMinute($endOfWindow)) {
            $loanableData["available_until"] = $end
                ->timezone("UTC")
                ->toIso8601String();
        }

        return $loanableData;
    }

    public static function mapToVehicleType(Loanable $loanable): string
    {
        return match (get_class($loanable->details)) {
            Car::class => match ($loanable->details->engine) {
                "diesel" => "car_diesel",
                "electric" => "car_electric",
                "hybrid" => "car_hybrid",
                default => "car_fuel",
            },
            Bike::class => match ($loanable->details->bike_type) {
                "electric" => "bike_electric",
                "cargo" => "bike_cargo",
                "cargo_electric" => "bike_cargo_electric",
                default => "bike_regular",
            },
            Trailer::class => "trailer",
        };
    }

    public function create(BaseRequest $request)
    {
        \Gate::authorize("create", GbfsDataset::class);
        $item = $this->validateAndCreate($request);
        return $this->respondWithItem($request, $item, 201);
    }

    public function index(BaseRequest $request)
    {
        \Gate::authorize("view", GbfsDataset::class);
        return parent::index($request);
    }

    public function destroy(BaseRequest $request, $id)
    {
        \Gate::authorize("delete", GbfsDataset::class);
        return self::validateAndDestroy($request, $id);
    }

    public function update(BaseRequest $request, GbfsDataset $gbfsDataset)
    {
        \Gate::authorize("update", GbfsDataset::class);
        $gbfsDataset->fill($request->all());
        $gbfsDataset->save();
        return $gbfsDataset;
    }

    public function view(BaseRequest $request, GbfsDataset $gbfsDataset)
    {
        \Gate::authorize("view", GbfsDataset::class);
        return $this->respondWithItem($request, $gbfsDataset);
    }

    public function addDatasetCommunity(BaseRequest $request)
    {
        \Gate::authorize("update", GbfsDataset::class);
        $communityId = $request->input("community_id");
        $gbfsDatasetName = $request->input("gbfs_dataset_name");

        if (
            !Community::query()
                ->where("id", $communityId)
                ->exists()
        ) {
            abort(404);
        }
        if (
            !GbfsDataset::query()
                ->where("name", $gbfsDatasetName)
                ->exists()
        ) {
            abort(404);
        }

        if (
            GbfsDatasetCommunity::query()
                ->where("community_id", $communityId)
                ->where("gbfs_dataset_name", $gbfsDatasetName)
                ->exists()
        ) {
            abort(
                422,
                __("validation.unique", [
                    "attribute" => "communauté et flux GBFS",
                ])
            );
        }

        $datasetCommunity = new GbfsDatasetCommunity();
        $datasetCommunity->gbfs_dataset_name = $gbfsDatasetName;
        $datasetCommunity->community_id = $communityId;
        $datasetCommunity->save();

        return $datasetCommunity;
    }

    public function listDatasetCommunities(BaseRequest $request)
    {
        \Gate::authorize("view", GbfsDataset::class);

        return $this->transformPaginatorItems(
            $request,
            WebQueryBuilder::for(
                $request,
                new GbfsDatasetCommunity()
            )->paginate()
        );
    }

    public function destroyDatasetCommunity(int $id)
    {
        \Gate::authorize("update", GbfsDataset::class);
        return GbfsDatasetCommunity::query()
            ->findOrFail($id)
            ->delete();
    }
}
