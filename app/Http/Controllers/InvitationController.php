<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\InvitationResource;
use App\Mail\Invitation\UserInvitedMail;
use App\Models\Invitation;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Repositories\RestRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class InvitationController extends RestController
{
    use AuthorizesRequests;

    public function __construct(Invitation $model)
    {
        $this->model = $model;
        $this->repo = RestRepository::for($model);
    }

    public function index(BaseRequest $request)
    {
        $this->authorize("viewAny", Invitation::class);

        return parent::index($request);
    }

    private function setCreateRequestDefaults(BaseRequest $request)
    {
        $request->merge([
            "inviter_user_id" => $request->user()->id,
        ]);

        if (!$request->input("expires_at")) {
            $request->merge([
                "expires_at" => Carbon::now()->addMonth(),
            ]);
        }
    }

    private function createInvitation(array $attributes): ?Invitation
    {
        if (
            CommunityUser::where("community_id", $attributes["community_id"])
                ->whereHas(
                    "user",
                    fn(Builder $user) => $user->where(
                        "email",
                        $attributes["email"]
                    )
                )
                ->exists()
        ) {
            // Do not create invitations for already existing member
            return null;
        }

        $invitation = Invitation::create($attributes);

        \Mail::queue(
            (new UserInvitedMail($invitation))->to($invitation->email)
        );

        return $invitation;
    }

    public function createMany(BaseRequest $request)
    {
        $invitationRulesExceptEmail = Invitation::$rules;
        unset($invitationRulesExceptEmail["email"]);

        $this->setCreateRequestDefaults($request);

        $request->validate([
            ...$invitationRulesExceptEmail,
            "emails" => [
                "required",
                "string",
                function ($attribute, $value, $fail) {
                    $emails = explode(",", $value);
                    $index = 0;
                    foreach ($emails as $email) {
                        $validator = \Validator::make(
                            [
                                "email" => $email,
                            ],
                            ["email" => Invitation::$rules["email"]]
                        );
                        if ($validator->fails()) {
                            $failedRule = array_keys($validator->failed())[0];
                            $fail(
                                __("validation.$failedRule", [
                                    "attribute" => "{$attribute}[$index]",
                                ])
                            );
                        }
                        $index++;
                    }
                },
            ],
        ]);

        $this->authorize("create", [
            Invitation::class,
            $request->input("community_id"),
        ]);

        $invitationParams = $request->input();
        $emails = explode(",", $invitationParams["emails"]);
        unset($invitationParams["emails"]);

        $createdInvitations = [];
        foreach ($emails as $email) {
            if (
                $createdInvitation = $this->createInvitation([
                    ...$invitationParams,
                    "email" => $email,
                ])
            ) {
                $createdInvitations[] = $createdInvitation;
            }
        }

        return InvitationResource::collection($createdInvitations);
    }

    public function create(BaseRequest $request)
    {
        $this->setCreateRequestDefaults($request);
        $request->validate(Invitation::$rules);

        $this->authorize("create", [
            Invitation::class,
            $request->input("community_id"),
        ]);

        $created = $this->createInvitation($request->input());

        if ($created) {
            return new InvitationResource($created);
        }

        return response()->noContent();
    }

    public function sendSample(BaseRequest $request)
    {
        $this->authorize("create", [
            Invitation::class,
            $request->input("community_id"),
        ]);
        $this->setCreateRequestDefaults($request);
        $request->merge([
            "email" => $request->user()->email,
        ]);

        $invitation = new Invitation($request->validate(Invitation::$rules));

        \Mail::send(
            (new UserInvitedMail($invitation))->to($request->user()->email)
        );
    }

    public function validateInvitation(BaseRequest $request, int $id)
    {
        $invitation = Invitation::findOrFail($id);

        if ($invitation->community->trashed()) {
            abort(
                400,
                "Cette invitation est expirée: la communauté a été archivée."
            );
        }

        $codeValid = $invitation->confirmation_code === $request->query("code");
        $emailValid = $invitation->email === $request->query("email");
        $data = [
            "is_active" => $invitation->status === "active",
            "email_valid" => $emailValid,
            "code_valid" => $codeValid,
        ];

        // We don't check for email here, since user might not yet have an account
        if ($codeValid && $invitation->status === "active") {
            $data["account_exists"] = User::where(
                "email",
                $invitation->email
            )->exists();
            $data["community_name"] = $invitation->community->name;
            // If personnalised message is present, we show the inviter name
            if (!empty($invitation->message) && $invitation->inviter) {
                $data["inviter_name"] = $invitation->inviter->name;
            }
        }

        return $data;
    }

    public function getSingleInvitationFromRequest(
        BaseRequest $request,
        int $id
    ) {
        $user = $request->user();
        if (!$user->email_verified_at) {
            $request->validate([
                "code" => ["required"],
            ]);
        }

        $invitation = Invitation::where("email", $user->email)->findOrFail($id);

        if ($invitation->status !== "active") {
            abort(403, "Cette invitation n'est plus active");
        }

        if (
            !$user->email_verified_at &&
            $invitation->confirmation_code !== $request->input("code")
        ) {
            abort(403, "Code invalide");
        }
        // we can confirm user email, since the code was only sent there.
        if (!$user->email_verified_at) {
            $user->email_verified_at = Carbon::now();
            $user->save();
        }

        return $invitation;
    }

    public function acceptForSingleCommunity(BaseRequest $request, int $id)
    {
        $validatedInvitation = $this->getSingleInvitationFromRequest(
            $request,
            $id
        );

        $allActiveInvitationsForCommunity = Invitation::where(
            "community_id",
            $validatedInvitation->community_id
        )
            ->status("active")
            ->where("email", $validatedInvitation->email)
            ->get();

        foreach ($allActiveInvitationsForCommunity as $invitation) {
            $invitation->accept($request->user());
        }

        return [
            "community_id" => $validatedInvitation->community_id,
        ];
    }

    public function refuseSingle(BaseRequest $request, int $id)
    {
        $this->getSingleInvitationFromRequest($request, $id)->refuse(
            $request->user()
        );
    }

    public function acceptOnRegistration(BaseRequest $request)
    {
        $user = $request->user();

        $invitation = Invitation::where("email", $request->user()->email)
            ->where("validated_on_registration", true)
            ->first();

        if (!$invitation) {
            return response()->noContent();
        }

        $allActiveInvitationsForCommunity = Invitation::where(
            "community_id",
            $invitation->community_id
        )
            ->status("active")
            ->where("email", $user->email)
            ->get();

        foreach ($allActiveInvitationsForCommunity as $invitation) {
            $invitation->accept($request->user());
        }

        return [
            "community_id" => $invitation->community_id,
        ];
    }

    public function destroy(int $invitationId)
    {
        $invitation = Invitation::findOrFail($invitationId);

        $this->authorize("delete", $invitation);

        $invitation->delete();

        return response()->json();
    }
}
