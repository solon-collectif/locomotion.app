<?php

namespace App\Http\Controllers;

use App\Enums\MailingListIntegrationStatus;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\MailingListIntegrationResource;
use App\Jobs\MailingLists\ImportAllMailingListContacts;
use App\Jobs\MailingLists\MarkMailingListSynchronized;
use App\Jobs\MailingLists\RemoveAllExtraMailingListContacts;
use App\Models\MailingListIntegration;
use App\Repositories\RestRepository;
use App\Services\MailingListService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class MailingListIntegrationController extends RestController
{
    use AuthorizesRequests;

    public function __construct(MailingListIntegration $model)
    {
        $this->repo = RestRepository::for($model);
        $this->model = $model;
    }

    public function index(BaseRequest $request)
    {
        $this->authorize("viewAny", MailingListIntegration::class);
        return parent::index($request);
    }

    public function create(Request $request)
    {
        $this->authorize("create", [MailingListIntegration::class, $request]);

        $data = $request->validate([
            "community_id" => ["required", "exists:communities,id"],
            "provider" => ["required", "in:mailchimp,brevo"],
            "api_key" => ["required"],
            "list_id" => ["required"],
            "community_editable" => ["boolean"],
            "tag" => ["required_if:provider,mailchimp"],
        ]);

        $integration = new MailingListIntegration($data);
        $integration->created_by_user_id = $request->user()->id;

        $mailingListService = $integration->getService();
        try {
            $listResult = $mailingListService->retryThrottled(
                fn() => $mailingListService->getList()
            );
        } catch (\Exception $e) {
            abort(422, "Erreur: {$e->getMessage()}");
        }

        if (isset($listResult["list"]["name"])) {
            $integration->list_name = $listResult["list"]["name"];
        }

        $integration->status = MailingListIntegrationStatus::Synchronizing;
        $integration->save();

        \Bus::chain([
            new RemoveAllExtraMailingListContacts($integration),
            new ImportAllMailingListContacts($integration),
            // With brevo, imports happen via a process on their side. We might not truly be fully
            // synchronized at this point, since we do not currently handle the notification
            // of the completion of the import processes. However, this feels like good enough for
            // now.
            new MarkMailingListSynchronized($integration),
        ])
            ->onQueue("low")
            ->dispatch();

        return new MailingListIntegrationResource($integration);
    }

    public function destroy(MailingListIntegration $mailingListIntegration)
    {
        $this->authorize("delete", $mailingListIntegration);

        $mailingListIntegration->delete();

        return response()->json();
    }

    public function getLists(Request $request): array
    {
        $data = $request->validate([
            "provider" => ["required", "in:mailchimp,brevo"],
            "api_key" => ["required"],
        ]);

        /** @var MailingListService $mailingListService */
        $mailingListService = \App::makeWith(MailingListService::class, $data);
        try {
            return $mailingListService->retryThrottled(
                fn() => $mailingListService->fetchLists(
                    $request->integer("page", 0),
                    $request->integer("per_page", 10)
                )
            );
        } catch (\Throwable $exception) {
            return ["error" => $exception->getMessage()];
        }
    }
}
