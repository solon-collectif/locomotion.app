<?php

namespace App\Http\Controllers;

use App\Enums\BillItemTypes;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\LoanNotificationLevel;
use App\Enums\LoanStatus;
use App\Events\Loan\CanceledEvent;
use App\Events\LoanAcceptedEvent;
use App\Events\LoanCommentAddedEvent;
use App\Events\LoanCreatedEvent;
use App\Events\LoanEndedEvent;
use App\Events\LoanExtensionAcceptedEvent;
use App\Events\LoanExtensionCanceledEvent;
use App\Events\LoanExtensionRejectedEvent;
use App\Events\LoanExtensionRequestedEvent;
use App\Events\LoanPaidEvent;
use App\Events\LoanPrepaidEvent;
use App\Events\LoanRejectedEvent;
use App\Events\LoanUpdatedEvent;
use App\Events\LoanValidatedEvent;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Loan\CreateRequest;
use App\Http\Resources\DashboardLoanResource;
use App\Http\Resources\ExtensionBlockingLoanResource;
use App\Http\Resources\InvoiceSummaryResource;
use App\Http\Resources\LoanCommentResource;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanComment;
use App\Models\User;
use App\Repositories\LoanRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class LoanController extends RestController
{
    public function __construct(LoanRepository $repository, Loan $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        $data = $request->safe()->except(["message_for_owner"]);

        $borrowerUser = User::with("borrower")->findOrFail(
            $request->input("borrower_user_id")
        );

        if (
            !$borrowerUser->is($request->user()) &&
            !$request->user()->isAdmin()
        ) {
            // Only admins can create loans for other users.
            abort(
                403,
                __("validation.custom.cannot_create_loan_for_other_user")
            );
        }

        $loanable = Loanable::query()
            ->accessibleBy($request->user())
            ->where("id", $data["loanable_id"])
            ->firstOrFail();

        Gate::authorize("create", [Loan::class, $loanable]);

        if (
            $loanable->type === LoanableTypes::Car &&
            !$borrowerUser->borrower->validated
        ) {
            abort(
                403,
                __("validation.custom.cannot_loan_car_borrower_not_approved")
            );
        }
        // Only use provided community_id if user is admin.
        if (!$request->user()->isAdmin() || !array_has($data, "community_id")) {
            $loanCommunity = $loanable->getCommunityForLoanBy($borrowerUser);
            if (!$loanCommunity) {
                abort(
                    403,
                    "Impossible de créer un emprunt dans cette communauté"
                );
            }
            $data["community_id"] = $loanCommunity->id;
        }

        $data["departure_at"] = (new CarbonImmutable(
            $data["departure_at"],
            $loanable->timezone
        ))
            ->tz(config("app.timezone"))
            ->startOfMinute();

        $this->checkLoanableAvailable(
            $loanable,
            departureAt: $data["departure_at"],
            durationInMinutes: $data["duration_in_minutes"]
        );

        $loan = Loan::create($data);

        $messageForOwner = $request->validated(
            "message_for_owner",
            default: null
        );
        if ($messageForOwner) {
            $comment = $loan->comments()->make([
                "text" => $messageForOwner,
            ]);
            $comment->author_id = $request->user()->id;
            $comment->save();
        }

        self::subscribeAllConcernedUsers($loan);

        event(new LoanCreatedEvent($request->user(), $loan, $messageForOwner));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public static function subscribeAllConcernedUsers(Loan $loan)
    {
        $allNotificationUsers = collect([]);
        $messagesOnlyNotificationUsers = collect([]);

        // Maybe allow co-owners configure their default notifications for all loans of a loanable
        if (!$loan->is_self_service) {
            $allNotificationUsers = $loan->loanable->mergedUserRoles
                ->filter(
                    fn($role) => $role->role === LoanableUserRoles::Coowner ||
                        $role->role === LoanableUserRoles::Owner
                )
                ->map(fn($role) => $role->user_id)
                ->toBase();
        } else {
            $messagesOnlyNotificationUsers = $loan->loanable->mergedUserRoles
                ->filter(
                    fn($role) => $role->role === LoanableUserRoles::Coowner ||
                        $role->role === LoanableUserRoles::Owner
                )
                ->map(fn($role) => $role->user_id)
                ->toBase();
        }

        $allNotificationUsers = $allNotificationUsers
            ->push($loan->borrower_user_id)
            ->values()
            ->unique();

        // Avoid changing existing subscription for users
        $currentlySubscribedUserIds = $loan->notifiedUsers()->pluck("users.id");
        $allNotificationUsers = $allNotificationUsers->diff(
            $currentlySubscribedUserIds
        );
        $messagesOnlyNotificationUsers = $messagesOnlyNotificationUsers->diff(
            $currentlySubscribedUserIds
        );

        $loan->notifiedUsers()->syncWithPivotValues(
            $allNotificationUsers->all(),
            [
                "level" => LoanNotificationLevel::All,
            ],
            detaching: false
        );
        $loan->notifiedUsers()->syncWithPivotValues(
            $messagesOnlyNotificationUsers->all(),
            [
                "level" => LoanNotificationLevel::MessagesOnly,
            ],
            detaching: false
        );

        $loan->load("notifiedUsers");
    }

    public function markNotificationsSeen(Request $request, Loan $loan)
    {
        Gate::authorize("view", $loan);

        $data = $request->validate([
            "seen_at" => ["date"],
        ]);

        $seenAt = new CarbonImmutable($data["seen_at"]);

        $userNotificationSettings = $loan->notifiedUsers
            ->where("id", $request->user()->id)
            ->first()?->pivot;

        if (
            $userNotificationSettings &&
            $userNotificationSettings->last_seen &&
            $seenAt->isBefore($userNotificationSettings->last_seen)
        ) {
            return;
        }

        $loan->notifiedUsers()->syncWithPivotValues(
            $request->user(),
            [
                "last_seen" => $seenAt,
                "level" =>
                    $userNotificationSettings->level ??
                    LoanNotificationLevel::None,
            ],
            detaching: false
        );
    }

    public function changeNotificationLevel(Request $request, Loan $loan)
    {
        $validOptions = [
            LoanNotificationLevel::All,
            LoanNotificationLevel::MessagesOnly,
            LoanNotificationLevel::None,
        ];
        $request->validate([
            "level" => [
                "in:" .
                implode(
                    ",",
                    array_map(
                        fn(LoanNotificationLevel $o) => $o->value,
                        $validOptions
                    )
                ),
            ],
        ]);

        if ($request->level !== LoanNotificationLevel::None->value) {
            Gate::authorize("view", $loan);
        }

        $loan
            ->notifiedUsers()
            ->syncWithPivotValues(
                $request->user(),
                ["level" => $request->level, "last_seen" => Carbon::now()],
                detaching: false
            );
    }

    public function accept(Request $request, Loan $loan)
    {
        Gate::authorize("accept", $loan);

        self::checkLoanableAvailable(
            $loan->loanable,
            $loan->id,
            $loan->departure_at,
            $loan->duration_in_minutes
        );

        $comment = $request->get("comment");

        if ($comment) {
            $loanComment = $loan->comments()->make([
                "text" => $comment,
            ]);
            $loanComment->author_id = $request->user()->id;
            $loanComment->save();
        }

        $loan->setLoanStatusAccepted();
        $loan->save();

        event(new LoanAcceptedEvent($loan, $request->user(), $comment));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function reject(Request $request, Loan $loan)
    {
        Gate::authorize("reject", $loan);

        $loan->setLoanStatusRejected();
        $loan->save();

        $commentText = $request->get("comment");

        if ($commentText) {
            $comment = $loan->comments()->make([
                "text" => $commentText,
            ]);
            $comment->author_id = $request->user()->id;
            $comment->save();
        }

        event(new LoanRejectedEvent($loan, $request->user(), $commentText));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function earlyReturn(Request $request, Loan $loan)
    {
        Gate::authorize("update", $loan);

        if ($loan->status !== LoanStatus::Ongoing) {
            abort(
                422,
                __("state.loan.must_have_status", [
                    "status" => __("state.loan.status.ongoing"),
                ])
            );
        }

        $loan->setLoanStatusEnded();
        $loan->save();
        event(new LoanEndedEvent($loan, $request->user()));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function checkLoanableAvailable(
        Loanable $loanable,
        $loanId = null,
        $departureAt = null,
        $durationInMinutes = null
    ) {
        if (
            !$loanable->isAvailable(
                new Carbon($departureAt, $loanable->timezone),
                $durationInMinutes,
                $loanId
            )
        ) {
            abort(422, "Le véhicule n'est pas disponible sur cette période.");
        }
    }

    private function loanDataForBeforeAfter(Loan $loan)
    {
        return array_intersect_key(
            $loan->append("actual_distance")->toArray(),
            array_flip([
                "departure_at",
                "actual_distance",
                "estimated_distance",
                "duration_in_minutes",
                "mileage_start",
                "mileage_end",
                "expenses_amount",
            ])
        );
    }

    public function updateTip(Request $request, Loan $loan)
    {
        Gate::authorize("updateTip", $loan);

        $data = $request->validate([
            "platform_tip" => ["nullable", "numeric", "min:0"],
        ]);

        $loan->fill($data);
        $loan->save();

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function updateFactors(Request $request, Loan $loan)
    {
        Gate::authorize("update", $loan);

        $initialLoan = $this->loanDataForBeforeAfter($loan);

        $mileageStart = $request->integer(
            "mileage_start",
            $initialLoan->mileage_start ?? 0
        );

        // Simple data
        $data = $request->validate([
            "estimated_distance" => ["integer"],
            "mileage_start" => ["integer", "nullable"],
            "mileage_end" => ["integer", "nullable", "gt:{$mileageStart}"],
        ]);

        $loan->fill($data);

        // Expenses: we need to validate it doesn't go over what's owed to owner
        if ($loan->can_add_expenses && $request->has("expenses_amount")) {
            $ownerTotal =
                $loan
                    ->getOwnerInvoice()
                    ?->getUserBalanceChangeForType(BillItemTypes::loanPrice) ??
                0;
            $purchasesAmountData = $request->validate(
                [
                    "expenses_amount" => [
                        "numeric",
                        "lte:$ownerTotal",
                        "nullable",
                    ],
                ],
                [
                    "expenses_amount.lte" => __(
                        "validation.custom.expenses_amount"
                    ),
                ]
            );
            $loan->fill($purchasesAmountData);
            $loan->clearInvoices(); // Since we update expenses, we need to regenerated invoices
        }

        // Set images
        $imageData = $request->validate([
            "mileage_start_image_id" => ["nullable"],
            "mileage_end_image_id" => ["nullable"],
            "expense_image_id" => ["nullable"],
        ]);
        if (array_key_exists("mileage_start_image_id", $imageData)) {
            $loan->setMorphOne(
                "mileageStartImage",
                $imageData["mileage_start_image_id"]
            );
        }
        if (array_key_exists("mileage_end_image_id", $imageData)) {
            $loan->setMorphOne(
                "mileageEndImage",
                $imageData["mileage_end_image_id"]
            );
        }
        if (array_key_exists("expense_image_id", $imageData)) {
            $loan->setMorphOne("expenseImage", $imageData["expense_image_id"]);
        }

        // Reset validation
        $loan->borrower_validated_at = null;
        $loan->owner_validated_at = null;
        if ($loan->has_required_info_to_validate) {
            if ($loan->borrower_user_id === $request->user()->id) {
                $loan->borrower_validated_at = CarbonImmutable::now();
            } elseif ($loan->loanable->hasOwnerOrCoowner($request->user())) {
                $loan->owner_validated_at = CarbonImmutable::now();
            }
        }
        if ($loan->status === LoanStatus::Validated) {
            $loan->setLoanStatusEnded();
        }
        if ($loan->status === LoanStatus::Ended) {
            // New loan data needs validation, we give some spare time to do so.
            $loan->attempt_autocomplete_at = CarbonImmutable::now()->addDays(2);
        }

        // If mileage has been changed, user may need to re-prepay
        if ($loan->status === LoanStatus::Confirmed) {
            $loan->setLoanStatusAccepted();
        }

        $loan->save();

        event(
            new LoanUpdatedEvent(
                $request->user(),
                $loan,
                /* approvalReset: */ false,
                $initialLoan
            )
        );

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function updateDates(Request $request, Loan $loan)
    {
        Gate::authorize("updateDates", $loan);
        $data = $request->validate([
            "departure_at" => ["date"],
            "duration_in_minutes" => ["integer", "min:15"],
        ]);

        $initialLoan = $this->loanDataForBeforeAfter($loan);

        // Make sure timezone is in vehicle timezone if request departure_at is zoneless.
        if (isset($data["departure_at"])) {
            $data["departure_at"] = (new CarbonImmutable(
                $data["departure_at"],
                $loan->loanable->timezone
            ))->tz(config("app.timezone"));
        }

        $this->checkLoanableAvailable(
            $loan->loanable,
            $loan->id,
            $data["departure_at"] ?? $loan->departure_at,
            $data["duration_in_minutes"] ?? $loan->duration_in_minutes
        );

        $loan->fill($data);

        // Reset prepayment if needed (maybe borrower cannot pay after changing dates)
        // setLoanStatusAccepted() will check if borrower can pay
        if ($loan->status === LoanStatus::Confirmed) {
            $loan->setLoanStatusAccepted();
        }

        // Reset loan approval if borrower updates dates
        $approvalReset = false;
        if (
            $loan->status === LoanStatus::Accepted ||
            $loan->status === LoanStatus::Confirmed
        ) {
            if (!$request->user()->can("accept", $loan)) {
                $loan->accepted_at = null;
            }
            $loan->setLoanStatusRequested();
            $approvalReset = $loan->status === LoanStatus::Requested;
        }

        $loan->save();

        event(
            new LoanUpdatedEvent(
                $request->user(),
                $loan,
                $approvalReset,
                $initialLoan
            )
        );

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function prepay(Request $request, Loan $loan)
    {
        $data = $request->validate([
            "platform_tip" => ["nullable", "numeric", "min:0"],
        ]);

        $loan->fill($data);

        Gate::authorize("prepay", $loan);

        $loan->prepay();
        $loan->save();

        event(new LoanPrepaidEvent($loan));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function pay(Request $request, Loan $loan)
    {
        $data = $request->validate([
            "platform_tip" => ["nullable", "numeric", "min:0"],
        ]);

        $loan->fill($data);

        Gate::authorize("pay", $loan);

        $loan->extension_duration_in_minutes = null;
        $loan->pay();
        $loan->save();

        event(new LoanPaidEvent($loan));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function comment(Request $request, Loan $loan)
    {
        Gate::authorize("comment", $loan);
        $data = $request->validate([
            "text" => ["required", "max:1024"],
        ]);

        $comment = $loan->comments()->make($data);
        $comment->author_id = $request->user()->id;
        $comment->save();

        event(new LoanCommentAddedEvent($comment));

        $userNotificationLevel = $loan->notifiedUsers
            ->where("id", $request->user()->id)
            ->first()?->pivot->level;

        if (
            !$userNotificationLevel ||
            $userNotificationLevel === LoanNotificationLevel::None
        ) {
            $userNotificationLevel = LoanNotificationLevel::MessagesOnly;
        }

        $loan->notifiedUsers()->syncWithPivotValues(
            $request->user(),
            [
                "level" => $userNotificationLevel,
                "last_seen" => Carbon::now(),
            ],
            detaching: false
        );

        return new LoanCommentResource($comment->load("author.avatar"));
    }
    public function deleteComment(
        Request $request,
        Loan $loan,
        LoanComment $comment
    ) {
        Gate::authorize("deleteLoanComment", [$loan, $comment]);

        $comment->delete();
        return new LoanCommentResource($comment->load("author.avatar"));
    }

    public function get(Request $request, Loan $loan)
    {
        Gate::authorize("view", $loan);

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function cancel(Request $request, Loan $loan)
    {
        Gate::authorize("cancel", $loan);

        $loan->setLoanStatusCanceled();
        $loan->save();

        event(new CanceledEvent($request->user(), $loan));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function requestExtension(Request $request, Loan $loan)
    {
        $minDurationForExtension = max(15, $loan->duration_in_minutes + 15);
        $data = $request->validate([
            "extension_duration_in_minutes" => [
                "integer",
                "min:$minDurationForExtension",
            ],
        ]);
        self::checkLoanableAvailable(
            $loan->loanable,
            $loan->id,
            $loan->departure_at,
            $data["extension_duration_in_minutes"]
        );

        Gate::authorize("requestExtension", $loan);

        if (
            $loan->is_self_service ||
            $request->user()->can("acceptExtension", $loan)
        ) {
            $initialLoan = self::loanDataForBeforeAfter($loan);
            $loan->duration_in_minutes = $data["extension_duration_in_minutes"];
            $loan->save();

            // If extension was created after loan ended, we might go back to ongoing state
            if (
                $loan->actual_return_at->isAfter(Carbon::now()) &&
                $loan->status !== LoanStatus::Ongoing
            ) {
                $loan->setLoanStatusOngoing();
                $loan->save();
            }

            event(
                new LoanUpdatedEvent(
                    $request->user(),
                    $loan,
                    approvalReset: false,
                    initialLoan: $initialLoan
                )
            );
        } else {
            $loan->extension_duration_in_minutes =
                $data["extension_duration_in_minutes"];
            $loan->save();
            event(new LoanExtensionRequestedEvent($loan));
        }

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function rejectExtension(Request $request, Loan $loan)
    {
        Gate::authorize("rejectExtension", $loan);
        if (!$loan->extension_duration_in_minutes) {
            abort(422, "No extension in process");
        }
        $loan->extension_duration_in_minutes = null;
        $loan->save();

        event(new LoanExtensionRejectedEvent($loan, $request->user()));

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function cancelExtension(Request $request, Loan $loan)
    {
        Gate::authorize("cancelExtension", $loan);
        if (!$loan->extension_duration_in_minutes) {
            abort(422, "No extension in process");
        }
        $loan->extension_duration_in_minutes = null;
        $loan->save();

        event(new LoanExtensionCanceledEvent($loan));

        return new LoanResource($loan->load("comments.author.avatar"));
    }
    public function acceptExtension(Request $request, Loan $loan)
    {
        Gate::authorize("acceptExtension", $loan);
        if (!$loan->extension_duration_in_minutes) {
            abort(422, "No extension in process");
        }

        self::checkLoanableAvailable(
            $loan->loanable,
            $loan->id,
            $loan->departure_at,
            $loan->extension_duration_in_minutes
        );

        $previousDuration = $loan->duration_in_minutes;
        $loan->acceptExtension();
        $loan->save();

        // If loan extension was accepted after the loan ended, we might need to change the state
        // back to ongoing.
        if (
            $loan->actual_return_at->isAfter(Carbon::now()) &&
            $loan->status !== LoanStatus::Ongoing
        ) {
            $loan->setLoanStatusOngoing();
            $loan->save();
        }

        event(
            new LoanExtensionAcceptedEvent(
                $loan,
                $request->user(),
                $previousDuration
            )
        );

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    /**
     * Estimates owner and borrowers invoices  given potential changes to the existing loan
     * parameters. This method is to be called with existing loans, which differs from
     * `LoanableController::test` which is meant for new loans only.
     */
    public function estimateChanges(Request $request, Loan $loan)
    {
        Gate::authorize("view", $loan);

        $data = $request->validate([
            "duration_in_minutes" => ["integer", "min:15"],
            "estimated_distance" => ["integer"],
            "mileage_start" => ["integer"],
            "mileage_end" => ["integer"],
            "expenses_amount" => ["numeric"],
            "platform_tip" => ["numeric"],
        ]);

        if (
            isset($data["mileage_end"]) &&
            (isset($data["mileage_start"]) || $loan->mileage_start) &&
            $data["mileage_end"] <
                ($data["mileage_start"] ?? $loan->mileage_start)
        ) {
            // unset invalid mileage end
            unset($data["mileage_end"]);
        }

        $loan->fill($data);
        if ($request->has("departure_at")) {
            $loan->departure_at = (new CarbonImmutable(
                $request->get("departure_at"),
                $loan->loanable->timezone
            ))->tz(config("app.timezone"));
        }

        $loan->actual_return_at = $loan->departure_at->addMinutes(
            $loan->duration_in_minutes
        );

        $borrowerInvoice = $loan->getBorrowerInvoice();
        $ownerInvoice = $loan->getOwnerInvoice();
        $isAvailable = $loan->loanable->isAvailable(
            $loan->departure_at,
            $loan->duration_in_minutes,
            $loan->id
        );

        $blockingLoan = null;

        // If estimating for extension for a self_service_loan, we get contact info of
        // blocking loans
        if (
            !$isAvailable &&
            $loan->is_self_service &&
            in_array($loan->status, [
                LoanStatus::Ongoing,
                LoanStatus::Ended,
                LoanStatus::Validated,
            ]) &&
            $request->has("duration_in_minutes")
        ) {
            $blockingLoan = $loan->loanable
                ->loans()
                ->accepted()
                ->where("loans.id", "!=", $loan->id)
                ->where("departure_at", ">=", $loan->departure_at)
                ->where("departure_at", "<", $loan->actual_return_at)
                ->orderBy("departure_at", "asc")
                ->first();
        }

        $response = [
            "available" => $isAvailable,
            "blocking_loan" => $blockingLoan
                ? new ExtensionBlockingLoanResource($blockingLoan)
                : null,
            "desired_contribution" => $loan->desired_contribution,
        ];

        if (
            $ownerInvoice &&
            $ownerInvoice->hasNonZeroItems() &&
            $request->user()->can("viewOwnerInvoice", $loan)
        ) {
            $response["owner_invoice"] = new InvoiceSummaryResource(
                $ownerInvoice
            );
        }
        if (
            $borrowerInvoice &&
            $borrowerInvoice->hasNonZeroItems() &&
            $request->user()->can("viewBorrowerInvoice", $loan)
        ) {
            $response["borrower_invoice"] = new InvoiceSummaryResource(
                $borrowerInvoice
            );
        }

        return $response;
    }
    /**
     * Marks the loan as being validated: it's information has been checked by
     * the loanable owner and payment can proceed.
     */
    public function validateInformation(
        Request $request,
        Loan $loan
    ): LoanResource {
        Gate::authorize("validateLoanInfo", $loan);

        $validatedAsBorrower = false;
        $validatedAsOwner = false;
        $validationDate = CarbonImmutable::now()->startOfSecond();

        if ($loan->borrowerUser->is($request->user())) {
            if (!$loan->borrower_validated_at) {
                $loan->borrower_validated_at = $validationDate;
            }
            $validatedAsBorrower = true;
        }

        if ($loan->loanable->hasOwnerOrCoowner($request->user())) {
            if (!$loan->owner_validated_at) {
                $loan->owner_validated_at = $validationDate;
            }
            $validatedAsOwner = true;
        }

        $loan->refreshStatus();
        $loan->save();

        if (
            ($validatedAsBorrower || $validatedAsOwner) &&
            ($loan->status === LoanStatus::Validated ||
                $loan->status === LoanStatus::Completed)
        ) {
            event(new LoanValidatedEvent($loan, $request->user()));
        }

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function autoValidate(Request $request, Loan $loan): LoanResource
    {
        Gate::authorize("validateLoanInfo", $loan);

        if (!$loan->loanable->library?->loan_auto_validation_question) {
            abort(422, __("state.loan.auto_validation_question_missing"));
        }

        $isSkipping = $request->boolean("skip");

        if ($isSkipping) {
            Gate::authorize("skipAutoValidation", $loan);
        } else {
            $expectedAnswer =
                $loan->loanable->library->loan_auto_validation_answer;
            if (
                !is_null($expectedAnswer) &&
                $request->get("answer") !== $expectedAnswer
            ) {
                abort(422, __("state.loan.auto_validation_answer_wrong"));
            }
        }
        $loan->auto_validated_at = CarbonImmutable::now();
        $loan->auto_validated_by_user_id = $request->user()->id;

        $loan->refreshStatus();
        $loan->save();

        if (
            $loan->status === LoanStatus::Validated ||
            $loan->status === LoanStatus::Completed
        ) {
            event(new LoanValidatedEvent($loan, $request->user()));
        }

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    public function resume(Request $request, Loan $loan): LoanResource
    {
        Gate::authorize("resume", $loan);

        if ($loan->actual_return_at->isBefore(CarbonImmutable::now())) {
            // Loans that are reset after their expected return time are automatically considered
            // 'approved' and 'prepaid', otherwise they will be automatically cancelled again.
            // (See MoveLoanForwards.php)
            $loan->setLoanStatusEnded();
        } else {
            $loan->setLoanStatusRequested();
        }
        $loan->save();

        return new LoanResource($loan->load("comments.author.avatar"));
    }

    /**
     * @return array{Collection, int}
     */
    private function limit(Builder $query, int $limit = 5)
    {
        $data = (clone $query)->limit($limit)->get();

        $count = $data->count();
        if ($count >= $limit) {
            // If there are more than the limit, we need to count with a query to the db;
            $count = $query->count();
        }
        return [$data, $count];
    }

    public function dashboard(Request $request)
    {
        $accessibleLoans = Loan::forUser($request->user()->id);

        $completedLoansQuery = (clone $accessibleLoans)
            ->where("status", "completed")
            ->orderBy("updated_at", "desc");

        $requestedLoans = (clone $accessibleLoans)->where(
            "status",
            LoanStatus::Requested
        );

        $userId = $request->user()->id;
        $requestedLoansAsBorrowerQuery = (clone $requestedLoans)->where(
            "borrower_user_id",
            $userId
        );
        $requestLoansAsOwnerQuery = $requestedLoans->hasOwnerAccess($userId);

        $ongoingLoansQuery = (clone $accessibleLoans)->whereIn("status", [
            LoanStatus::Ongoing,
            LoanStatus::Ended,
            LoanStatus::Validated,
        ]);

        $confirmedLoansQuery = (clone $accessibleLoans)->whereIn("status", [
            LoanStatus::Accepted,
            LoanStatus::Confirmed,
        ]);

        [$ongoingLoans, $ongoingLoansCount] = self::limit($ongoingLoansQuery);
        [$requestLoansAsBorrower, $requestedLoansAsBorrowerCount] = self::limit(
            $requestedLoansAsBorrowerQuery
        );
        [$requestedLoansAsOwner, $requestedLoansAsOwnerCount] = self::limit(
            $requestLoansAsOwnerQuery
        );
        [$confirmedLoans, $confirmedLoansCount] = self::limit(
            $confirmedLoansQuery
        );
        [$completedLoans, $completedLoansCount] = self::limit(
            $completedLoansQuery
        );

        // We eagerly load relations (about 20 in total) for all combined query results
        // This beats doing it per query (6 x 20) or lazily (max 6 queries * 5 items * 20 relations).
        (new Collection())
            ->concat($ongoingLoans)
            ->concat($requestLoansAsBorrower)
            ->concat($requestedLoansAsOwner)
            ->concat($confirmedLoans)
            ->concat($completedLoans)
            ->load([
                "community",
                "borrowerUser.avatar",
                "borrowerUser.borrower",
                "borrowerUser.subscriptions.communityUser",
                "loanable.userRoles.user.approvedCommunities.allowedLoanableTypes",
                "loanable.userRoles.user.avatar",
                "loanable.activeIncidents",
                "loanable.image",
            ]);

        return response(
            [
                "started" => [
                    "total" => $ongoingLoansCount,
                    "loans" => DashboardLoanResource::collection($ongoingLoans),
                ],
                "waiting" => [
                    "total" => $requestedLoansAsBorrowerCount,
                    "loans" => DashboardLoanResource::collection(
                        $requestLoansAsBorrower
                    ),
                ],
                "need_approval" => [
                    "total" => $requestedLoansAsOwnerCount,
                    "loans" => DashboardLoanResource::collection(
                        $requestedLoansAsOwner
                    ),
                ],
                "future" => [
                    "total" => $confirmedLoansCount,
                    "loans" => DashboardLoanResource::collection(
                        $confirmedLoans
                    ),
                ],
                "completed" => [
                    "total" => $completedLoansCount,
                    "loans" => DashboardLoanResource::collection(
                        $completedLoans
                    ),
                ],
            ],
            200
        );
    }
}
