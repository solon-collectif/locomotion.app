<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\PaymentMethod\CreateRequest;
use App\Models\PaymentMethod;
use App\Repositories\PaymentMethodRepository;
use Stripe;

class PaymentMethodController extends RestController
{
    protected $canReturnCsv = false;

    public function __construct(
        PaymentMethodRepository $repository,
        PaymentMethod $model
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        $user = $request->user();

        $customer = $user->getStripeCustomer();
        $card = Stripe::createCardBySourceId(
            $customer->id,
            $request->get("external_id")
        );

        $request->merge([
            "external_id" => $card->id,
            "country" => $card->country,
        ]);

        if (!$request->get("user_id")) {
            $request->merge(["user_id" => $request->user()->id]);
        }

        $item = parent::validateAndCreate($request);

        return $this->respondWithItem($request, $item, 201);
    }

    public function view(Request $request, PaymentMethod $paymentMethod)
    {
        \Gate::authorize("view", $paymentMethod);

        return $this->respondWithItem($request, $paymentMethod);
    }

    public function destroy(Request $request, PaymentMethod $paymentMethod)
    {
        \Gate::authorize("delete", $paymentMethod);
        $user = $request->user();
        $customer = $user->getStripeCustomer();
        Stripe::deleteSource($customer->id, $paymentMethod->external_id);

        $paymentMethod->delete();
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
            ],
            "form" => [
                "name" => [
                    "type" => "text",
                ],
                "external_id" => [
                    "type" => "text",
                ],
                "four_last_digits" => [
                    "type" => "number",
                    "disabled" => true,
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"][$field])) {
                continue;
            }
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }
}
