<?php

namespace App\Http\Controllers;

use App\Helpers\Path;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\ImageManager as ImageManager;

class ImageController extends FileController
{
    public function __construct(ImageRepository $image)
    {
        $this->repo = $image;
    }

    public function viewImage(Request $request, Image $image)
    {
        $size = $request->get("size", Image::$orginalSizeName);

        \Gate::authorize("view", $image);

        if (!$image->isValidSize($size)) {
            abort(
                422,
                "Request image size '$size' doesn't exist for this image"
            );
        }

        $path = $image->getImageSizePath($size);

        if (!\Storage::exists($path)) {
            if (!\Storage::exists($image->full_path)) {
                \Log::error(
                    "[ImageController] Original size for image $image->id is missing at $image->full_path"
                );
                abort(404);
            }
            // Attempt to create the desired size.
            $image->syncThumbnailFiles();
        }

        return \Storage::response($path);
    }

    protected function upload($file, $field, $userId)
    {
        $uniq = uniqid();
        $uri = "images/tmp/$userId/$uniq";

        $originalFilename = $file->getClientOriginalName();
        $filename =
            $field . "." . pathinfo($originalFilename, PATHINFO_EXTENSION);

        $manager = new ImageManager(["driver" => "imagick"]);
        try {
            $image = $manager->make($file)->orientate();
        } catch (NotReadableException) {
            abort(422, "Fichier illisible.");
        }

        try {
            $filepath = Path::join($uri, $filename);
            $saved = Image::store($filepath, $image);
            if (!$saved) {
                abort(500, "Fichier non sauvegardé: $filepath");
            }
        } catch (NotSupportedException) {
            abort(422, "Format non supporté.");
        }

        return [
            "path" => $uri,
            "original_filename" => $originalFilename,
            "filename" => $filename,
            "width" => $image->width(),
            "height" => $image->height(),
            "field" => $field,
            "filesize" => $image->filesize(),
        ];
    }
}
