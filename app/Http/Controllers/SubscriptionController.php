<?php

namespace App\Http\Controllers;

use App\Enums\BillItemTypes;
use App\Enums\PricingLoanableTypeValues;
use App\Events\NewYearlyContributionEvent;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\InvoiceResource;
use App\Http\Resources\PricingResource;
use App\Http\Resources\SubscriptionResource;
use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\Pivots\CommunityUser;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Pricings\LoanInvoiceHelper;
use Carbon\CarbonImmutable;
use Ds\Set;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

class SubscriptionController
{
    use AuthorizesRequests;

    public function myTargets(Request $request)
    {
        $myCommunities = $request->user()->approvedCommunities;

        $output = [];

        foreach ($myCommunities as $myCommunity) {
            // Get my current subscriptions
            $mySubscriptions = Subscription::where(
                "community_user_id",
                $myCommunity->pivot->id
            )
                ->active()
                ->get();
            $subscribedLoanableTypes = new Set();
            foreach ($mySubscriptions as $mySubscription) {
                $subscribedLoanableTypes->add(
                    ...$mySubscription->loanable_types
                );
            }

            $subscriptionInterval = Subscription::newSubscriptionInterval();

            $pricingsByLoanableType = [];
            foreach (
                PricingLoanableTypeValues::cases()
                as $pricingLoanableType
            ) {
                $pricings = Pricing::join(
                    "pricing_loanable_types",
                    "pricing_loanable_types.pricing_id",
                    "pricings.id"
                )
                    ->where(
                        "pricing_loanable_types.pricing_loanable_type",
                        $pricingLoanableType
                    )
                    ->where("pricings.pricing_type", "contribution")
                    ->whereNotNull("pricings.yearly_target_per_user")
                    ->forCommunity($myCommunity->id)
                    ->select("pricings.*")
                    ->intersects($subscriptionInterval)
                    ->distinct()
                    ->get();

                // We cannot subscribe for a loanble type that doesn't have a community contribution
                // pricing
                if (
                    $pricings->isEmpty() ||
                    !$pricings->contains(
                        fn(Pricing $pricing) => !$pricing->is_global
                    )
                ) {
                    continue;
                }
                $pricingsByLoanableType[
                    $pricingLoanableType->value
                ] = PricingResource::collection($pricings);
            }

            if (!empty($pricingsByLoanableType)) {
                $output[] = [
                    "community" => [
                        "id" => $myCommunity->id,
                        "name" => $myCommunity->name,
                    ],
                    "subscriptions" => SubscriptionResource::collection(
                        $mySubscriptions
                    ),
                    "pricings_by_loanable_types" => $pricingsByLoanableType,
                ];
            }
        }

        return $output;
    }

    private function generateInvoice(BaseRequest $request)
    {
        $data = $request->validate([
            "pricing_loanable_types" => [
                "required",
                "array",
                Rule::in(PricingLoanableTypeValues::possibleTypes),
            ],
            "community_id" => ["required", "exists:communities,id"],
        ]);
        $communityId = (int) $data["community_id"];
        $user = $request->user();

        $communityUser = CommunityUser::where("user_id", $user->id)
            ->where("community_id", $communityId)
            ->firstOrFail();

        $currentSubscription = Subscription::where(
            "community_user_id",
            $communityUser->id
        )
            ->where("type", "paid")
            ->active()
            ->first();

        if ($currentSubscription) {
            $missingLoanableTypes = new Set(
                array_map(
                    fn(
                        PricingLoanableTypeValues $loanableTypeValues
                    ) => $loanableTypeValues->value,
                    $currentSubscription->loanable_types
                )
            );
            $missingLoanableTypes->remove(...$data["pricing_loanable_types"]);

            if (!$missingLoanableTypes->isEmpty()) {
                abort(
                    422,
                    __("state.subscription.cannot_reduce", [
                        "pricing_loanable_types" => PricingLoanableTypeValues::translateList(
                            $missingLoanableTypes->toArray()
                        ),
                    ])
                );
            }
        }

        $subscriptionInterval = Subscription::newSubscriptionInterval();

        // Get total to be paid for the loanable types
        $pricings = Pricing::join(
            "pricing_loanable_types",
            "pricing_loanable_types.pricing_id",
            "pricings.id"
        )
            ->whereIn(
                "pricing_loanable_types.pricing_loanable_type",
                $data["pricing_loanable_types"]
            )
            ->whereNotNull("pricings.yearly_target_per_user")
            ->where("pricings.pricing_type", "contribution")
            ->forCommunity($communityId)
            ->intersects($subscriptionInterval)
            ->select("pricings.*")
            ->distinct()
            ->get();

        $communityPricings = $pricings->filter(
            fn(Pricing $p) => !$p->is_global
        );
        $communityPricingIds = $communityPricings->map(
            fn(Pricing $p) => $p->id
        );

        // Get all loanable types which are covered by the pricings paid for the request's loanable
        // types. (e.g. if two loanable types share the exact same pricings, but the request only
        // has one of the two, the subscription should still cover both since the same pricings will
        // be paid).
        $coveredLoanableTypes = Pricing::join(
            "pricing_loanable_types",
            "pricing_loanable_types.pricing_id",
            "pricings.id"
        )
            ->yearlyContribution()
            ->forCommunity($communityId)
            ->groupBy("pricing_loanable_type")
            // Keep loanable types which have the same of fewer community pricings
            ->whereIn("pricing_id", $communityPricingIds)
            ->havingRaw("count(*) <= {$communityPricingIds->count()}")
            // Exclude loanable types which have extra applicable pricings (community or global)
            ->whereNotExists(
                fn(Builder $q) => $q->fromSub(
                    Pricing::from("pricings as p2")
                        ->join(
                            "pricing_loanable_types as plt2",
                            "plt2.pricing_id",
                            "p2.id"
                        )
                        ->yearlyContribution()
                        ->whereNull("deleted_at")
                        ->forCommunity($communityId)
                        ->intersects($subscriptionInterval)
                        ->whereColumn(
                            "pricing_loanable_types.pricing_loanable_type",
                            "plt2.pricing_loanable_type"
                        )
                        ->whereNotIn(
                            "plt2.pricing_id",
                            $pricings->map(fn($p) => $p->id)
                        )
                        ->getQuery(),
                    "sub"
                )
            )
            ->pluck("pricing_loanable_type")
            ->toArray();

        if (empty($coveredLoanableTypes)) {
            abort(
                422,
                __("state.subscription.no_available_subscription_for_types", [
                    "pricing_loanable_types" => PricingLoanableTypeValues::translateList(
                        $data["pricing_loanable_types"]
                    ),
                ])
            );
        }

        $invoice = new Invoice([
            "period" => "Contribution annuelle",
        ]);
        $invoice->paid_at = CarbonImmutable::now();
        $invoice->user_id = $user->id;

        $billItems = new Collection();

        $pricings->map(
            fn(Pricing $p) => $billItems->add(
                new BillItem([
                    "item_type" => BillItemTypes::contributionYearly,
                    "contribution_community_id" => $p->community_id,
                    "label" => $p->community
                        ? "$p->name ({$p->community->name})"
                        : "$p->name (Réseau LocoMotion)",
                    "amount_type" => "corrected",
                    ...LoanInvoiceHelper::splitIntoAmountAndTaxes(
                        -$p->yearly_target_per_user
                    ),
                    "meta" => [
                        "pricing_id" => $p->id,
                        "pricing_name" => $p->name,
                    ],
                ])
            )
        );

        $invoice->setRelation("billItems", $billItems);

        if ($currentSubscription) {
            $reinbursementBillItems = collect(
                $currentSubscription->getRefundBillItems(
                    -$invoice->getUserBalanceChange()
                )
            );

            $finalBillItems = collect([]);

            // Simplify items by removing reinbursement bill items that exactly match new bill items.
            // This happens when a subscription is modified the same day it has been created.
            foreach ($billItems as $billItem) {
                $exactReimbursmentBillItems = $reinbursementBillItems->filter(
                    fn(BillItem $b) => abs($b->amount + $billItem->amount) <
                        0.005 &&
                        $b->meta["pricing_id"] == $billItem->meta["pricing_id"]
                );

                if ($exactReimbursmentBillItems->count() === 1) {
                    // Remove both the reinbursement and initial item
                    $reinbursementBillItems = $reinbursementBillItems->reject(
                        fn(BillItem $b) => abs($b->amount + $billItem->amount) <
                            0.005 &&
                            $b->meta["pricing_id"] ==
                                $billItem->meta["pricing_id"]
                    );
                    continue;
                }

                // Keep both
                $finalBillItems->add($billItem);
            }

            $finalBillItems = $finalBillItems
                ->merge($reinbursementBillItems)
                ->sortBy(fn(BillItem $b) => $b->meta["pricing_id"] ?? $b->id);

            $invoice->setRelation("billItems", $finalBillItems);
        }

        return [
            "invoice" => $invoice,
            "covered_loanable_types" => $coveredLoanableTypes,
            "current_subscription" => $currentSubscription,
            "community_user" => $communityUser,
        ];
    }

    public function estimate(BaseRequest $request)
    {
        ["invoice" => $invoice] = self::generateInvoice($request);
        return new InvoiceResource($invoice);
    }

    public function pay(BaseRequest $request)
    {
        [
            "invoice" => $invoice,
            "covered_loanable_types" => $coveredLoanableTypes,
            "current_subscription" => $currentSubscription,
            "community_user" => $communityUser,
        ] = self::generateInvoice($request);

        $user = $request->user();

        if (floatval($user->balance) < -$invoice->getUserBalanceChange()) {
            abort(422, __("state.subscription.cannot_pay"));
        }

        $invoice->save();
        $invoice->billItems()->saveMany($invoice->billItems);
        $user->balance += $invoice->getUserBalanceChange();
        $user->save();

        $subscription = Subscription::createForPayment(
            $communityUser,
            $user->id,
            $invoice
        );
        \DB::table("subscription_loanable_types")->insert(
            array_map(
                fn($pricingLoanableType) => [
                    "subscription_id" => $subscription->id,
                    "pricing_loanable_type" => $pricingLoanableType,
                ],
                $coveredLoanableTypes
            )
        );

        $currentSubscription?->delete();

        event(new NewYearlyContributionEvent($user, $invoice));

        return new SubscriptionResource($subscription->refresh());
    }

    public function grant(Request $request)
    {
        $data = $request->validate([
            "user_id" => ["required", "numeric", "exists:users,id"],
            "community_id" => ["required", "numeric", "exists:communities,id"],
            "duration_in_days" => ["nullable", "numeric"],
            "reason" => ["nullable", "string"],
        ]);

        \Gate::authorize("grant", [Subscription::class, $data]);

        $communityUser = CommunityUser::where("user_id", $data["user_id"])
            ->where("community_id", $data["community_id"])
            ->firstOrFail();

        $existingSubscription = Subscription::where(
            "community_user_id",
            $communityUser->id
        )
            ->active()
            ->where("type", "granted")
            ->first();

        if ($existingSubscription) {
            $existingSubscription->changeDuration($data["duration_in_days"]);
            $existingSubscription->reason = $data["reason"];
            $existingSubscription->granted_by_user_id = $request->user()->id;
            $existingSubscription->save();

            return new SubscriptionResource($existingSubscription->refresh());
        }

        $subscription = Subscription::createForGrant(
            $communityUser,
            $request->user()->id,
            $data["duration_in_days"] ?? null,
            $data["reason"] ?? null
        );

        return new SubscriptionResource($subscription->refresh());
    }

    public function destroy(Subscription $subscription)
    {
        $this->authorize("delete", $subscription);

        $subscription->delete();

        return response()->json();
    }

    public function restore(Subscription $subscription)
    {
        $this->authorize("restore", $subscription);

        $subscription->restore();

        return new SubscriptionResource($subscription);
    }
}
