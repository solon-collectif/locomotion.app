<?php

namespace App\Http\Controllers;

use App\Enums\BillItemTypes;
use App\Events\AddedToUserBalanceEvent;
use App\Events\BorrowerApprovedEvent;
use App\Events\BorrowerCompletedEvent;
use App\Events\PermanentlyDeletingUserDataEvent;
use App\Events\RegistrationSubmittedEvent;
use App\Events\UserClaimedBalanceEvent;
use App\Facades\Kiwili;
use App\Http\ErrorResponse;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\User\AddToBalanceRequest;
use App\Http\Requests\User\BorrowerStatusRequest;
use App\Http\Requests\User\BorrowerSubmitRequest;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateEmailRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\WebQueryBuilder;
use App\Mail\Registration\Approved;
use App\Mail\Registration\Rejected;
use App\Mail\Registration\Submitted;
use App\Mail\UserBalanceClaimReceived;
use App\Mail\UserDataDeletedMail;
use App\Mail\UserMail;
use App\Models\Borrower;
use App\Models\Invoice;
use App\Models\Payout;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Reports\YearlyIncomeReport;
use App\Repositories\CommunityRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\RestRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use PHPUnit\Exception;
use Stripe;
use Stripe\Refund;

class UserController extends RestController
{
    public function __construct(
        UserRepository $repository,
        User $model,
        protected CommunityRepository $communityRepo,
        protected InvoiceRepository $invoiceRepo
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        $item = parent::validateAndCreate($request);

        return $this->respondWithItem($request, $item, 201);
    }

    private static function isChangedProof(User $user, $data): bool
    {
        if (
            !isset($data["residency_proof"]) &&
            !isset($data["identity_proof"])
        ) {
            return false;
        }

        $newProofs = array_map(function ($p) {
            return $p["id"];
        }, array_merge(
            $data["residency_proof"] ?? [],
            $data["identity_proof"] ?? []
        ));

        $previousProofs = $user->residencyProof
            ->merge($user->identityProof)
            ->map(function ($p) {
                return $p->id;
            })
            ->toArray();

        // Proof has changed if the number of files, or the ids of the files
        // are different.
        return sizeof($newProofs) !== sizeof($previousProofs) ||
            array_sort($previousProofs) != array_sort($newProofs);
    }

    /**
     * Sends a registrationSubmittedEvent for every community where user has sufficient proofs.
     */
    private function submitUserProofs(User $user)
    {
        foreach ($user->communities as $community) {
            /** @var CommunityUser $communityUser */
            $communityUser = $community->pivot;
            if (
                !$communityUser->approved_at &&
                !$communityUser->suspended_at &&
                (!$community->requires_identity_proof ||
                    $user->identityProof()->exists() > 0)
            ) {
                event(new RegistrationSubmittedEvent($communityUser));
            }
        }
    }

    public function update(UpdateRequest $request, User $user)
    {
        Gate::authorize("update", $user);

        $proofChanged = static::isChangedProof($user, $request->json()->all());
        $savedUser = parent::validateAndUpdate($request, $user->id);

        if ($proofChanged) {
            $this->submitUserProofs($user);
        }

        return $this->respondWithItem($request, $savedUser);
    }

    public function retrieveSelf(Request $request)
    {
        return $this->respondWithItem(
            $request,
            $this->retrieveWithRequestFields($request, $request->user())
        );
    }

    public function view(Request $request, User $user)
    {
        Gate::authorize("view", $user);
        return $this->respondWithItem(
            $request,
            $this->retrieveWithRequestFields($request, $user)
        );
    }

    public function destroy(Request $request, User $user)
    {
        Gate::authorize("deactivate", $user);

        $loansAsBorrower = $user->loansAsBorrower()->blockingLoanableDeletion();

        $loansAsOwner = $user
            ->loanablesAsOwner()
            ->whereHas(
                "loans",
                fn(Builder $l) => $l->blockingLoanableDeletion()
            );

        if ($loansAsBorrower->exists() || $loansAsOwner->exists()) {
            throw ValidationException::withMessages([
                __("validation.custom.user_must_not_have_ongoing_loans"),
            ]);
        }

        if ($user->balance !== null && floatval($user->balance) !== 0.0) {
            throw ValidationException::withMessages([
                __("validation.custom.user_balance_not_zero"),
            ]);
        }

        if ($request->boolean("delete_data")) {
            $report = new YearlyIncomeReport(
                $user,
                CarbonImmutable::now()->year
            );
            $reportData = $report->hasData() ? $report->generate() : null;
            $userName = $user->name;
            $userEmail = $user->email;
            event(new PermanentlyDeletingUserDataEvent($user));
            $user->deleteData();
            // We do not queue here, since if we fail to send the report, we should also fail the
            // deletion of the user's data.
            \Mail::send(
                (new UserDataDeletedMail($userName, $reportData))->to(
                    $userEmail,
                    $userName
                )
            );
        } else {
            $user->delete();
        }

        return $user;
    }

    public function restore(Request $request, User $user)
    {
        Gate::authorize("reactivate", $user);

        /** @var User $user */
        $user = parent::validateAndRestore($request, $user->id);

        if ($request->boolean("restore_loanables")) {
            $user
                ->loanablesAsOwner()
                ->withTrashed()
                ->restore();

            if (!$request->boolean("restore_availability")) {
                // Delete all availabilities.
                $user->loanablesAsOwner()->update([
                    "availability_json" => [],
                    "availability_mode" => "never",
                ]);
            }
        }
    }

    public function updateEmail(UpdateEmailRequest $request, User $user)
    {
        // verify if the user who sent the request is not an admin. if so, we need to check for its current password.
        if (!$request->user()->isAdmin()) {
            $currentPassword = $request->get("password");

            // if the current password entered is invalid, return bad response
            if (!Hash::check($currentPassword, $user->password)) {
                return $this->respondWithItem($request, $user, 401);
            }
        }

        // change the email
        $user->email = $request->get("email");
        $user->save();
        return $this->respondWithItem($request, $user);
    }

    public function updatePassword(UpdatePasswordRequest $request, User $user)
    {
        $changingOwnPassword = $request->user()->id === $user->id;
        // Current password must be validated for non admins or if changing own password (even if admin)
        if ($changingOwnPassword) {
            $currentPassword = $request->get("current");

            // if the current password entered is invalid, return bad response
            if (!Hash::check($currentPassword, $user->password)) {
                return $this->respondWithItem($request, $user, 401);
            }
        }

        // change the password
        $user->password = Hash::make($request->get("new"));
        $user->save();

        $currentUserToken = \Auth::user()->token();
        // Remove all tokens except current
        foreach ($user->tokens as $token) {
            if (!$changingOwnPassword || $token->id != $currentUserToken->id) {
                $token->revoke();
            }
        }

        return $this->respondWithItem($request, $user);
    }

    public function addAllToCommunity(AdminRequest $request, $communityId)
    {
        foreach (
            WebQueryBuilder::for($request, new User())->cursor()
            as $user
        ) {
            $user->communities()->sync([$communityId]);
        }
    }

    private function trySendEmail(Mailable $mailable, User $user)
    {
        try {
            UserMail::send($mailable, $user);
            return [
                "id" => $user->id,
                "response" => [
                    "status" => "success",
                ],
            ];
        } catch (Exception $e) {
            return [
                "id" => $user->id,
                "response" => [
                    "status" => "error",
                    "message" => $e->getMessage(),
                ],
            ];
        }
    }

    public function sendEmail(AdminRequest $request, $type)
    {
        $users = WebQueryBuilder::for($request, new User())->cursor();

        $report = [];
        switch ($type) {
            case "password_reset":
                foreach ($users as $user) {
                    $authController = app(AuthController::class);
                    $passwordRequest = $request->redirectAs(
                        $user,
                        Request::class
                    );
                    $passwordRequest->merge([
                        "email" => $user->email,
                    ]);
                    $output = $authController->passwordRequest(
                        $passwordRequest
                    );
                    $report[] = [
                        "id" => $user->id,
                        "response" => array_merge($output->getData(), [
                            "status" => "success",
                        ]),
                    ];
                }
                break;
            case "registration_approved":
                foreach ($users as $user) {
                    foreach ($user->approvedCommunities as $approvedCommunity) {
                        $report[] = $this->trySendEmail(
                            new Approved($approvedCommunity->pivot),
                            $user
                        );
                    }
                }
                break;
            case "registration_submitted":
                foreach ($users as $user) {
                    if ($user->approvedCommunities->isEmpty()) {
                        $report[] = $this->trySendEmail(
                            new Submitted($user),
                            $user
                        );
                    }
                }
                break;
            case "registration_rejected":
                foreach ($users as $user) {
                    $report[] = $this->trySendEmail(new Rejected($user), $user);
                }
                break;
            default:
                abort(422);
        }

        return ["report" => $report];
    }

    public function submitBorrower(BorrowerSubmitRequest $request, User $user)
    {
        Gate::authorize("update", $user);
        $borrower = RestRepository::for($user->borrower)->update(
            $request,
            $user->borrower->id,
            $request->json()->all()
        );
        $borrower->approved_at = null;
        $borrower->submitted_at = Carbon::now();
        $borrower->save();

        event(new BorrowerCompletedEvent($user));

        return $this->respondWithItem($request, $borrower->refresh());
    }

    public function approveBorrower(BorrowerStatusRequest $request, User $user)
    {
        if (!$user->borrower->approved_at) {
            $user->borrower->approved_at = new Carbon();
            $user->borrower->save();

            event(new BorrowerApprovedEvent($user));
        }

        return $this->respondWithItem($request, $user->borrower);
    }

    public function resetBorrowerStatus(
        BorrowerStatusRequest $request,
        User $user
    ) {
        $user->borrower->suspended_at = null;
        $user->borrower->approved_at = null;
        $user->borrower->submitted_at = null;
        $user->borrower->save();

        return $this->respondWithItem($request, $user->borrower);
    }

    public function suspendBorrower(BorrowerStatusRequest $request, User $user)
    {
        if (!$user->borrower->suspended_at) {
            $user->borrower->suspended_at = new \DateTime();
            $user->borrower->save();
        }

        return $this->respondWithItem($request, $user->borrower);
    }

    public function unsuspendBorrower(
        BorrowerStatusRequest $request,
        User $user
    ) {
        if ($user->borrower->suspended_at) {
            $user->borrower->suspended_at = null;
            $user->borrower->save();
        }

        return $this->respondWithItem($request, $user->borrower);
    }

    public function getBalance(Request $request, User $user)
    {
        Gate::authorize("view", $user);

        return $user->balance;
    }

    public function addToBalance(AddToBalanceRequest $request, User $user)
    {
        Gate::authorize("add-to-balance", $user);
        // In case anything fails.
        $userBalanceBefore = $user->balance;

        $amount = $request->get("amount");
        $amountInCents = (int) ($amount * 100);
        $paymentMethodId = $request->get("payment_method_id");

        if ($paymentMethodId) {
            $paymentMethod = $user->paymentMethods
                ->where("id", $paymentMethodId)
                ->first();
        } else {
            $paymentMethod = $user->defaultPaymentMethod;
        }

        if (!$paymentMethod) {
            return ErrorResponse::withMessage("no_payment_method", 400);
        }

        $amountWithFeeInCents = Stripe::computeAmountWithFee(
            $amountInCents,
            $paymentMethod
        );
        if ($amountWithFeeInCents <= 0) {
            return ErrorResponse::withMessage(
                "amount_in_cents_is_nothing",
                400
            );
        }

        $feeInCents = $amountWithFeeInCents - $amountInCents;
        $fee = round($feeInCents / 100, 2);

        $amountForDisplay = Invoice::formatAmountForDisplay($amount);
        $feeForDisplay = Invoice::formatAmountForDisplay($fee);

        $charge = null;
        try {
            $customerId = $user->getStripeCustomer()->id;
            $charge = Stripe::createCharge(
                $amountWithFeeInCents,
                $customerId,
                "Ajout au compte LocoMotion: {$amountForDisplay}$ + {$feeForDisplay}$ (frais)",
                $paymentMethod->external_id
            );
        } catch (\Exception $e) {
            $message = match ($e->getStripeCode()) {
                52 => "Carte expirée!",
                default => $e->getMessage(),
            };
            return ErrorResponse::withError("Stripe: {$message}", 400);
        }

        try {
            $invoice = new Invoice();

            $invoice->period = "Approvisionnement du compte";
            $invoice->user()->associate($user);
            $invoice->save();

            $stripeMeta = [
                "sourceType" => "stripe",
                "stripeChargeId" => $charge->id,
            ];

            $invoice->billItems()->create([
                "item_type" => BillItemTypes::balanceProvision,
                "label" => "Ajout au compte LocoMotion",
                "amount" => $amount,
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "meta" => array_merge($stripeMeta, [
                    "destType" => "user",
                    "destUserId" => $user->id,
                    "destUserName" => $user->name,
                    "destUserEmail" => $user->email,
                ]),
            ]);
            $invoice->billItems()->create([
                "item_type" => BillItemTypes::feesStripe,
                "label" => "Frais de transaction Stripe",
                "amount" => $fee,
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "meta" => array_merge($stripeMeta, [
                    "destType" => "transactionFees",
                ]),
            ]);

            $user->addToBalance($invoice->getUserBalanceChange());

            $user->save();

            $invoice->payWith($paymentMethod);

            event(new AddedToUserBalanceEvent($user, $invoice));

            return response($user->balance, 200);
        } catch (\Exception $e) {
            if ($charge) {
                Refund::create([
                    "charge" => $charge->id,
                ]);
            }

            $user->balance = $userBalanceBefore;
            $user->save();

            return ErrorResponse::withMessage($e->getMessage(), 500);
        }
    }

    public function claimBalance(Request $request)
    {
        $user = $request->user();
        Gate::authorize("claim-balance", $user);

        if ($user->balance <= 0) {
            abort(422, __("state.user.balance_too_low"));
        }

        // Ensure users cannot claim balance a second time while the first is processed.
        // Since we use a transaction middleware which wraps the database updates, it is possible
        // to read that user does have a balance > 0 even though another request is currently
        // updating that balance to 0. This is even more likely since we wait for the kiwili
        // API which can take seconds.
        $lockName = "payout-$user->id";
        $lock = \Cache::lock($lockName, 30);
        if (!$lock->get()) {
            abort(429);
        }
        \DB::beginTransaction();
        $invoice = $user->invoices()->create([
            "period" => "Remboursement de solde",
        ]);

        $invoice->billItems()->create([
            "item_type" => BillItemTypes::balanceRefund,
            "label" => "Remboursement par virement bancaire",
            "amount" => -$user->balance,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $payout = new Payout([
            "invoice_id" => $invoice->id,
            "amount" => $user->balance,
            "user_id" => $user->id,
        ]);

        $user->balance = 0;
        $user->save();
        $payout->save();
        \DB::commit();

        try {
            $kiwiliExepense = Kiwili::createExpense($payout);
            $payout->kiwili_expense_id = $kiwiliExepense["Id"];
            $payout->save();
        } catch (\Exception $e) {
            if (config("services.kiwili.enabled")) {
                \Log::error("Failed saving kiwili expense", [
                    "exception" => $e,
                ]);
            }

            event(new UserClaimedBalanceEvent($payout));
        }

        UserMail::queue(new UserBalanceClaimReceived($payout), $user);

        $lock->release();

        return response()->noContent();
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
                "borrower" => new \stdClass(),
                "accept_conditions" => false,
            ],
            "form" => [
                "general" => [
                    "email" => [
                        "type" => "email",
                    ],
                    "name" => [
                        "type" => "text",
                    ],
                    "last_name" => [
                        "type" => "text",
                    ],
                    "avatar" => [
                        "type" => "image",
                        "aspect_ratio" => "1 / 1",
                    ],
                    "description" => [
                        "type" => "textarea",
                    ],
                    "date_of_birth" => [
                        "type" => "date",
                    ],
                    "address" => [
                        "type" => "text",
                    ],
                    "postal_code" => [
                        "type" => "text",
                    ],
                    "phone" => [
                        "type" => "text",
                    ],
                    "is_smart_phone" => [
                        "type" => "checkbox",
                    ],
                    "other_phone" => [
                        "type" => "text",
                    ],
                    "accept_conditions" => [
                        "type" => "checkbox",
                    ],
                    "bank_transit_number" => [
                        "type" => "text",
                    ],
                    "bank_institution_number" => [
                        "type" => "text",
                    ],
                    "bank_account_number" => [
                        "type" => "text",
                    ],
                ],
                "borrower" => [
                    "drivers_license_number" => [
                        "type" => "text",
                    ],
                    "has_not_been_sued_last_ten_years" => [
                        "type" => "checkbox",
                    ],
                    "gaa" => [
                        "type" => "files",
                    ],
                    "saaq" => [
                        "type" => "files",
                    ],
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"]["general"][$field])) {
                continue;
            }
            $template["form"]["general"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $borrowerRules = Borrower::getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"]["borrower"][$field])) {
                continue;
            }
            $template["form"]["borrower"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        return $template;
    }

    public function acceptConditions(Request $request)
    {
        $request->user()->acceptConditions();
        $request->user()->save();
    }
}
