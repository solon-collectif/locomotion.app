<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\PayoutResource;
use App\Models\Payout;
use App\Repositories\RestRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class PayoutController extends RestController
{
    use AuthorizesRequests;

    public function __construct(Payout $model)
    {
        $this->model = $model;
        $this->repo = RestRepository::for($model);
    }

    public function index(BaseRequest $request)
    {
        $this->authorize("viewAny", Payout::class);
        return parent::index($request);
    }

    public function update(BaseRequest $request, Payout $payout)
    {
        $this->authorize("update", $payout);

        $data = $request->validate([
            "kiwili_expense_id" => ["required"],
        ]);

        $payout->update($data);

        return new PayoutResource($payout);
    }
}
