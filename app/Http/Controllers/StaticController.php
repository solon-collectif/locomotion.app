<?php

namespace App\Http\Controllers;

use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use DB;
use Illuminate\Routing\Controller;

class StaticController extends Controller
{
    public function redirectToSolon()
    {
        return redirect("https://solon-collectif.org/locomotion/");
    }

    public function blank()
    {
        return response("", 204);
    }

    public function notFound()
    {
        return abort(404);
    }

    public function status()
    {
        return view("status", [
            "database" => DB::statement("SELECT 1") ? "OK" : "Erreur",
        ]);
    }

    public function stats()
    {
        $communityQuery = Community::whereIn("type", ["borough", "private"]);

        $communityQuery->select("*")->withCustomColumn("center");

        return response(
            [
                "communities" => $communityQuery->get()->map(function ($c) {
                    return [
                        "id" => $c->id,
                        "name" => $c->name,
                        "center" => $c->center,
                        "area" => $c->area,
                        "center_google" => $c->center_google,
                        "type" => $c->type,
                        "description" => $c->description,
                    ];
                }),
                // User is not super admin
                "users" => User::whereRole(null)
                    // User is approved in at least one community
                    ->whereHas("approvedCommunities")
                    ->count(),
                "loanables" => Loanable::count(),
            ],
            200
        );
    }

    public function app()
    {
        return view("app");
    }
}
