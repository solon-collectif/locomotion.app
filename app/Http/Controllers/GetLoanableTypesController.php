<?php

namespace App\Http\Controllers;

use App\Models\LoanableTypeDetails;
use Illuminate\Routing\Controller;

class GetLoanableTypesController extends Controller
{
    public function __invoke()
    {
        return \Response::make(LoanableTypeDetails::all())->setMaxAge(3600);
    }
}
