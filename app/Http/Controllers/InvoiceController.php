<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Http\Requests\Invoice\CreateRequest;
use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\User;
use App\Repositories\InvoiceRepository;

class InvoiceController extends RestController
{
    public function __construct(InvoiceRepository $repository, Invoice $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function view(BaseRequest $request, Invoice $invoice)
    {
        \Gate::authorize("view", $invoice);
        return $this->respondWithItem(
            $request,
            $this->retrieveWithRequestFields($request, $invoice)
        );
    }

    public function create(CreateRequest $request)
    {
        \Gate::authorize("create", Invoice::class);
        $item = parent::validateAndCreate($request);

        $user = $item->user;

        // addToBalance does save user.
        $user->addToBalance($item->getUserBalanceChange());

        $item->save();

        return $this->respondWithItem($request, $item, 201);
    }

    public function estimate(BaseRequest $request)
    {
        \Gate::authorize("create", Invoice::class);
        $user = User::find($request->input("user_id"));

        $invoice = new Invoice($request->input());
        $billItems = collect($request->input("bill_items"))->map(
            fn($b) => new BillItem($b)
        );

        $invoice->setRelation("billItems", $billItems);

        $change = $invoice->getUserBalanceChange();

        return [
            "before" => $user->balance,
            "change" => $change,
            "after" => $user->balance + $change,
        ];
    }

    public function template()
    {
        return [
            "item" => [
                "bill_items" => [],
                "period" => "",
            ],
            "rules" => [],
        ];
    }
}
