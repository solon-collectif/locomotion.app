<?php

namespace App\Http\Controllers;

use App\Enums\BillItemTypes;
use App\Enums\ReportStatus;
use App\Enums\ReportType;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\PublishableReportResource;
use App\Jobs\GenerateOwnerIncomeReport;
use App\Mail\ReportPublishedMail;
use App\Mail\UserMail;
use App\Models\Invoice;
use App\Models\PublishableReport;
use App\Models\User;
use App\Reports\YearlyIncomeReport;
use App\Repositories\RestRepository;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;

class PublishableReportController extends RestController
{
    public function __construct(PublishableReport $report)
    {
        $this->model = $report;
        $this->repo = RestRepository::for($report);
    }

    public function getYearlyIncomeReport(Request $request, $year)
    {
        \Gate::authorize("view", PublishableReport::class);

        return new PublishableReportResource(
            PublishableReport::where("type", "yearly-owner-income")
                ->where("report_key", $year)
                ->orderBy("id", "desc")
                ->firstOrFail()
        );
    }

    public function generateYearlyReport(
        BaseRequest $request,
        int $year,
        User $user
    ) {
        \Gate::authorize("create", PublishableReport::class);

        $pdf = new YearlyIncomeReport($user, $year);

        $output = $pdf->generate();

        return response()->make($output, 200, [
            "content-type" => "application/pdf",
        ]);
    }

    public function generateAllYearlyReports(Request $request, int $year)
    {
        \Gate::authorize("create", PublishableReport::class);

        $existingReport = PublishableReport::where(
            "type",
            "yearly-owner-income"
        )
            ->where("report_key", $year)
            ->orderBy("id", "desc")
            ->first();

        if ($existingReport?->status === ReportStatus::Published) {
            abort(403, "cannot regenerate published report");
        }

        $followingYear = $year + 1;
        $usersWithOwnerInvoices = Invoice::query()
            ->toBase()
            ->where(
                "invoices.created_at",
                ">=",
                (new CarbonImmutable(
                    "{$year}-01-01",
                    config("app.default_user_timezone")
                ))
                    ->startOfDay()
                    ->toISOString()
            )
            ->where(
                "invoices.created_at",
                "<",
                (new CarbonImmutable(
                    "{$followingYear}-01-01",
                    config("app.default_user_timezone")
                ))
                    ->startOfDay()
                    ->toISOString()
            )
            ->join("loans", "loans.owner_invoice_id", "=", "invoices.id")
            ->join("users", "invoices.user_id", "=", "users.id")
            ->join("bill_items", "invoices.id", "=", "bill_items.invoice_id")
            ->whereIn("bill_items.item_type", [
                BillItemTypes::loanExpenses,
                BillItemTypes::loanPrice,
            ])
            ->where("bill_items.amount", ">", 0)
            ->whereNull("users.data_deleted_at")
            ->selectRaw("distinct invoices.user_id")
            ->pluck("user_id");

        if ($usersWithOwnerInvoices->count() === 0) {
            return response()->json([
                "message" => __("state.reports.no_owners_with_income", [
                    "year" => $year,
                ]),
            ]);
        }

        if ($existingReport) {
            $existingReport->delete();
        }

        $report = PublishableReport::create([
            "type" => ReportType::YearlyOwnerIncome,
            "status" => ReportStatus::Generating,
            "report_key" => $year,
        ]);

        $report->users()->sync($usersWithOwnerInvoices);

        $jobs = $usersWithOwnerInvoices->map(
            (fn($userId) => new GenerateOwnerIncomeReport(
                $userId,
                $year,
                $report
            ))
        );

        $batch = \Bus::batch($jobs)
            ->then(function () use ($report) {
                $report->status = ReportStatus::Generated;
                $report->save();
            })
            ->catch(function () use ($report) {
                $report->status = ReportStatus::Failed;
                $report->save();
            })
            ->onQueue("reports")
            ->dispatch();

        $report->batch_id = $batch->id;
        $report->save();
        return new PublishableReportResource($report);
    }

    public function publish(BaseRequest $request, PublishableReport $report)
    {
        \Gate::authorize("publish", $report);
        $report->status = ReportStatus::Published;
        $report->save();

        foreach ($report->users as $reportUser) {
            UserMail::queue(
                new ReportPublishedMail($reportUser->pivot),
                $reportUser
            );
        }

        return new PublishableReportResource($report);
    }
}
