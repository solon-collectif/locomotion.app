<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\ExportResource;
use App\Http\WebQueryBuilder;
use App\Models\Export;
use App\Repositories\RestRepository;

class ExportController extends RestController
{
    protected $canReturnCsv = false;

    public function __construct(Export $model)
    {
        $this->repo = RestRepository::for($model);
        $this->model = $model;
    }

    public function view($id)
    {
        $export = Export::with("file")->findOrFail($id);
        \Gate::authorize("view", $export);

        return new ExportResource($export);
    }

    public function destroy($id)
    {
        \Gate::authorize("destroy", Export::class);
        return Export::findOrFail($id)->delete();
    }

    public function cancel($id)
    {
        $export = Export::findOrFail($id);
        \Gate::authorize("cancel", $export);
        $export->cancel();
    }

    public function destroyBatch(BaseRequest $request)
    {
        \Gate::authorize("destroy", Export::class);

        foreach (
            WebQueryBuilder::for($request, new Export())->cursor()
            as $item
        ) {
            $item->delete();
        }
    }

    public function cancelBatch(BaseRequest $request)
    {
        \Gate::authorize("cancelBatch", Export::class);

        foreach (
            WebQueryBuilder::for($request, new Export())->cursor()
            as $item
        ) {
            $item->cancel();
        }
    }
}
