<?php

namespace App\Http\Controllers;

use App\Enums\LoanableUserRoles;
use App\Events\LoanableUserRoleAdded;
use App\Events\LoanableUserRoleRemoved;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\LoanableUserRoleResource;
use App\Models\LoanableUserRole;
use Gate;

class LoanableUserRolesController
{
    public function delete(BaseRequest $request, LoanableUserRole $role)
    {
        Gate::authorize("delete", $role);
        $role->delete();

        event(new LoanableUserRoleRemoved($role, $request->user()));
    }

    public function changeOwner(BaseRequest $request, LoanableUserRole $role)
    {
        Gate::authorize("changeOwner", [$role, $request->all()]);
        $data = $request->validate([
            "user_id" => ["required", "exists:users,id"],
            "title" => "nullable|string",
            "show_as_contact" => "boolean",
            "pays_loan_price" => "boolean",
            "pays_loan_insurance" => "boolean",
        ]);

        $oldOwner = $role->ressource
            ->userRoles()
            ->where("role", LoanableUserRoles::Owner)
            ->first();

        $oldOwner?->delete();

        $newOwnerRole = $role->ressource->userRoles()->make($data);
        $newOwnerRole->user_id = $data["user_id"];
        $newOwnerRole->role = LoanableUserRoles::Owner;
        $newOwnerRole->granted_by_user_id = $request->user()->id;
        $newOwnerRole->save();

        event(new LoanableUserRoleRemoved($oldOwner, $request->user()));
        event(
            new LoanableUserRoleAdded(
                $request->user(),
                $role->ressource,
                $newOwnerRole
            )
        );

        return new LoanableUserRoleResource($newOwnerRole);
    }

    public function update(BaseRequest $request, LoanableUserRole $role)
    {
        Gate::authorize("update", [$role, $request->all()]);

        if (
            $request->has("user_id") &&
            $request->get("user_id") !== $role->user_id
        ) {
            return $this->changeOwner($request, $role);
        }

        if (
            ($request->has("pays_loan_price") &&
                $request->get("pays_loan_price") !== $role->pays_loan_price) ||
            ($request->has("pays_loan_insurance") &&
                $request->get("pays_loan_insurance") !==
                    $role->pays_loan_insurance)
        ) {
            Gate::authorize("updatePaidAmountTypes", $role);
        }

        $role->fill(
            $request->validate([
                "title" => "nullable|string",
                "show_as_contact" => "boolean",
                "pays_loan_price" => "boolean",
                "pays_loan_insurance" => "boolean",
            ])
        );
        $role->save();

        return new LoanableUserRoleResource($role->load("user.avatar"));
    }
}
