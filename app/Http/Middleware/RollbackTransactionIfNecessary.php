<?php

namespace App\Http\Middleware;
use DB;
use Laravel\Octane\Events\RequestTerminated;
use Log;
use StorageTransaction;

class RollbackTransactionIfNecessary
{
    public function handle(RequestTerminated $event): void
    {
        if (DB::transactionLevel() > 0) {
            Log::warning(
                "DB Transaction in process on octane request end. Rolling back..."
            );
            DB::rollBack(0);
        }

        if (StorageTransaction::isInTransaction()) {
            Log::warning(
                "Storage in transaction on octane request end. Rolling back..."
            );
            StorageTransaction::rollback();
        }
    }
}
