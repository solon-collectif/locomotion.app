<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddClientVersionHeaderMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if (config("app.client_version")) {
            $response->headers->set(
                "X-Client-Version",
                config("app.client_version")
            );
        }
        return $response;
    }
}
