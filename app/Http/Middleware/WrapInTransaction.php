<?php

namespace App\Http\Middleware;

use App\Facades\StorageTransaction;
use Closure;
use Illuminate\Support\Facades\DB;

class WrapInTransaction
{
    public function handle($request, Closure $next, $guard = null)
    {
        DB::beginTransaction();
        StorageTransaction::beginTransaction();

        $response = $next($request);

        // Error response
        $status = (int) $response->getStatusCode();
        if (in_array($status, [400, 500])) {
            DB::rollBack();
            StorageTransaction::rollback();
            return $response;
        }

        DB::commit();
        StorageTransaction::commit();

        return $response;
    }
}
