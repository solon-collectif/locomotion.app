<?php

namespace App\Http\Resources;

use App\Models\Trailer;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Trailer */
class TrailerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "maximum_charge" => $this->maximum_charge,
            "dimensions" => $this->dimensions,
        ];
    }
}
