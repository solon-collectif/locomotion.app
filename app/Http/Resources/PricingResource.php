<?php

namespace App\Http\Resources;

use App\Models\Pricing;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Pricing */
class PricingResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "rule" => $this->rule,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "pricing_type" => $this->pricing_type,
            "loanable_ownership_type" => $this->loanable_ownership_type,
            "yearly_target_per_user" => $this->yearly_target_per_user,
            "is_mandatory" => $this->is_mandatory,
            "description" => $this->description,
            "is_global" => $this->is_global,
            "pricing_loanable_types" => $this->pricing_loanable_types,
        ];
    }
}
