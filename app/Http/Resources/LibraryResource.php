<?php

namespace App\Http\Resources;

use App\Models\Library;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Library */
class LibraryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "phone_number" => $this->phone_number,
            "loan_auto_validation_question" =>
                $this->loan_auto_validation_question,
            "loan_auto_validation_answer" => $this->when(
                $request->user()->can("see-validation-answer", $this->resource),
                $this->loan_auto_validation_answer
            ),
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "communities_count" => $this->communities_count,
            "loanables_count" => $this->loanables_count,
            "owner_user" => new UserBriefResource($this->owner_user),
            "user_roles" => LoanableUserRoleResource::collection(
                $this->whenLoaded("userRoles")
            ),
            "loanables" => LoanableResource::collection(
                $this->whenLoaded("loanables")
            ),
            "avatar" => new ImageResource($this->whenLoaded("avatar")),
        ];
    }
}
