<?php

namespace App\Http\Resources;

use App\Models\Loan;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loan */
class ExtensionBlockingLoanResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "departure_at" => $this->departure_at,
            "borrower_user" => new UserBriefResource(
                $this->borrowerUser,
                showUserPhone: true
            ),
        ];
    }
}
