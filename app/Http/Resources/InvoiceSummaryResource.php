<?php

namespace App\Http\Resources;

use App\Models\Invoice;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Invoice */
class InvoiceSummaryResource extends JsonResource
{
    public function toArray($request)
    {
        $itemSummaries = $this->billItems->map(function ($item, $key) {
            return [
                "item_type" => $item->item_type,
                "amount" => $item->amount,
                "taxes_tps" => $item->taxes_tps,
                "taxes_tvq" => $item->taxes_tvq,
                "total" => $item->total,
                "contribution_community_id" => $item->contribution_community_id,
                "meta" => [
                    "pricing_description" =>
                        $item->meta["pricing_description"] ?? null,
                    "pricing_name" => $item->meta["pricing_name"] ?? null,
                    "pricing_id" => $item->meta["pricing_id"] ?? null,
                ],
            ];
        });

        return [
            "items" => $itemSummaries,
            "user_balance_change" => $this->getUserBalanceChange(),
        ];
    }
}
