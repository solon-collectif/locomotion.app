<?php

namespace App\Http\Resources;

use App\Enums\LoanStatus;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loan */
class DashboardLoanResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "actual_return_at" => $this->actual_return_at,
            "borrower_validated_at" => $this->borrower_validated_at,
            "community" => [
                "id" => $this->community_id,
            ],
            "departure_at" => $this->departure_at,
            "duration_in_minutes" => $this->duration_in_minutes,
            "extension_duration_in_minutes" =>
                $this->extension_duration_in_minutes,
            "needs_validation" => $this->needs_validation,
            "owner_validated_at" => $this->owner_validated_at,
            "status" => $this->status,

            // The following three attributes require computing invoices
            // (which means calculating the pricing if not present)
            $this->mergeWhen(
                $this->status === LoanStatus::Completed,
                fn() => [
                    "borrower_total" => $this->borrower_total,
                    "owner_total" => $this->owner_total,
                ]
            ),

            "borrower_must_pay_compensation" =>
                $this->borrower_must_pay_compensation,
            "borrower_may_contribute" => $this->borrower_may_contribute,
            "borrower_must_pay_insurance" => $this->borrower_must_pay_insurance,

            "owner_action_required" => $this->owner_action_required,
            "borrower_action_required" => $this->borrower_action_required,
            "is_self_service" => $this->is_self_service,

            "borrower_user" => new UserBriefResource($this->borrowerUser),

            "loanable" => new DashboardLoanableResource(
                $this->whenLoaded("loanable")
            ),
        ];
    }
}
