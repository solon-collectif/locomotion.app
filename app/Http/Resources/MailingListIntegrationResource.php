<?php

namespace App\Http\Resources;

use App\Models\MailingListIntegration;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin MailingListIntegration */
class MailingListIntegrationResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "provider" => $this->provider,
            "api_key" =>
                substr($this->api_key, 0, 3) .
                str_pad(substr($this->api_key, -3), 7, "*", STR_PAD_LEFT),
            "list_id" => $this->list_id,
            "tag" => $this->tag,
            "list_name" => $this->list_name,
            "status" => $this->status,
            "consecutive_error_count" => $this->consecutive_error_count,
            "last_error" => $this->last_error,
            "community_editable" => $this->community_editable,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,

            "community_id" => $this->community_id,
            "created_by_user_id" => $this->created_by_user_id,
            "createdByUser" => new UserBriefResource(
                $this->whenLoaded("createdByUser")
            ),
            "community" => new CommunityOverviewResource(
                $this->whenLoaded("community")
            ),
        ];
    }
}
