<?php

namespace App\Http\Resources;

use App\Models\Community;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Community */
class ArchivedCommunityResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => "(Archivé) " . $this->name,
            "deleted_at" => $this->deleted_at,
            "type" => $this->type,
        ];
    }
}
