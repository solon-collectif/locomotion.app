<?php

namespace App\Http\Resources;

use App\Models\Invoice;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Invoice */
class InvoiceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->period,
            "user_id" => $this->user_id,
            "payment_method_id" => $this->payment_method_id,
            "paid_at" => $this->paid_at,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "deleted_at" => $this->deleted_at,
            "user_balance_change" => $this->getUserBalanceChange(),

            "items" => InvoiceItemResource::collection($this->billItems),
        ];
    }
}
