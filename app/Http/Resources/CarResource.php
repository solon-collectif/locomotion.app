<?php

namespace App\Http\Resources;

use App\Models\Car;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Car */
class CarResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "brand" => $this->brand,
            "model" => $this->model,
            "year_of_circulation" => $this->year_of_circulation,
            "transmission_mode" => $this->transmission_mode,
            "engine" => $this->engine,
            "plate_number" => $this->plate_number,
            "papers_location" => $this->papers_location,
            "insurer" => $this->insurer,
            "has_informed_insurer" => $this->has_informed_insurer,
            "pricing_category" => $this->pricing_category->value,
            "value_category" => $this->value_category,
            "has_onboard_notebook" => $this->has_onboard_notebook,
            "has_report_in_notebook" => $this->has_report_in_notebook,

            "report" => new FileResource($this->report),
        ];
    }
}
