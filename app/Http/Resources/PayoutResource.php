<?php

namespace App\Http\Resources;

use App\Models\Payout;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Payout */
class PayoutResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "status" => $this->status,
            "kiwili_expense_id" => $this->kiwili_expense_id,
            "amount" => $this->amount,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,

            "user_id" => $this->user_id,
            "invoice_id" => $this->invoice_id,

            "user" => new UserBriefResource($this->whenLoaded("user")),
            "invoice" => new InvoiceResource($this->whenLoaded("invoice")),
        ];
    }
}
