<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin User */
class UserBriefResource extends JsonResource
{
    public function __construct(
        mixed $user,
        private readonly bool $showUserPhone = false,
        private readonly bool $showUserEmail = false
    ) {
        parent::__construct($user);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->trashed()) {
            return (new ArchivedUserResource($this))->toArray($request);
        }

        return [
            "id" => $this->id,
            "description" => $this->description,
            "name" => $this->name,
            "last_name" => $this->last_name,
            "full_name" => $this->full_name,
            "avatar" => new ImageResource($this->whenLoaded("avatar")),
            "phone" => $this->when($this->showUserPhone, $this->phone),
            "email" => $this->when($this->showUserEmail, $this->email),
        ];
    }
}
