<?php

namespace App\Http\Resources;

use App\Models\Invitation;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Invitation */
class InvitationResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "email" => $this->email,
            "reason" => $this->reason,
            "auto_approve" => $this->auto_approve,
            "expires_at" => $this->expires_at,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,

            "inviter_user" => new UserBriefResource($this->inviter),
            "community" => [
                "id" => $this->community->id,
                "name" => $this->community->name,
            ],
        ];
    }
}
