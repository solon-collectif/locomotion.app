<?php

namespace App\Http\Resources;

use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Loan;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class LoanLoanableResource extends JsonResource
{
    public function __construct(Loanable $loanable, private readonly Loan $loan)
    {
        parent::__construct($loanable);
    }

    public function toArray($request)
    {
        if ($this->trashed()) {
            return (new ArchivedLoanableResource($this))->toArray($request);
        }

        if ($this->type === LoanableTypes::Car) {
            $this->details->load("report");
        }

        return [
            "id" => $this->id,
            "type" => $this->type,
            "pricing_loanable_type" => PricingLoanableTypeValues::forLoanable(
                $this->resource
            ),
            "position_google" => $this->position_google,
            "position" => $this->position,
            "comments" => $this->comments,
            "instructions" => $this->when(
                $request->user()->can("viewInstructions", $this->loan),
                $this->instructions
            ),
            "return_instructions" => $this->when(
                $request->user()->can("viewInstructions", $this->loan),
                $this->return_instructions
            ),
            "trusted_borrower_instructions" => $this->when(
                $request
                    ->user()
                    ->can("viewTrustedBorrowerInstructions", $this->loan),
                $this->trusted_borrower_instructions
            ),
            "sharing_mode" => $this->sharing_mode,
            "location_description" => $this->location_description,
            "name" => $this->name,
            "timezone" => $this->timezone,
            "max_loan_duration_in_minutes" =>
                $this->max_loan_duration_in_minutes,
            "merged_user_roles" => LoanableUserRoleResource::collection(
                $this->getFilteredLoanableRoles()
            ),
            "library" => new LibraryResource($this->library),

            "image" => new ImageResource($this->image),
            "images" => ImageResource::collection($this->images),
            "details" => $this->getLoanableDetailsResource($this->details),
            "active_incidents" => IncidentResource::collection(
                $this->activeIncidents
            ),
        ];
    }

    private function getFilteredLoanableRoles()
    {
        if (\Gate::allows("viewUserRoles", $this->resource)) {
            return $this->mergedUserRoles
                ->filter(
                    fn($userRole) => in_array($userRole->role, [
                        LoanableUserRoles::Owner,
                        LoanableUserRoles::Coowner,
                        LoanableUserRoles::Manager,
                    ])
                )
                ->load("user.avatar");
        }

        return $this->mergedUserRoles
            ->filter(
                // Owners should always be returned, but Coowners and Managers only if they're contacts
                fn($userRole) => $userRole->role === LoanableUserRoles::Owner ||
                    (in_array($userRole->role, [
                        LoanableUserRoles::Coowner,
                        LoanableUserRoles::Manager,
                    ]) &&
                        $userRole->show_as_contact)
            )
            ->load("user.avatar");
    }

    private function getLoanableDetailsResource(
        $data
    ): TrailerResource|BikeResource|CarResource {
        return match ($this->type) {
            LoanableTypes::Bike => new BikeResource($data),
            LoanableTypes::Trailer => new TrailerResource($data),
            LoanableTypes::Car => new CarResource($data),
        };
    }
}
