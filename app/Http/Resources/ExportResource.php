<?php

namespace App\Http\Resources;

use App\Models\Export;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Export */
class ExportResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "model" => $this->model,
            "status" => $this->status,
            "progress" => $this->progress,

            "file" => new FileResource($this->whenLoaded("file")),
        ];
    }
}
