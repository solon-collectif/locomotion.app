<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin User */
class OwnerUserResource extends JsonResource
{
    public function __construct(
        User $user,
        private readonly bool $showUserPhone = false
    ) {
        parent::__construct($user);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => new UserBriefResource($this, $this->showUserPhone),
        ];
    }
}
