<?php

namespace App\Http\Resources;

use App\Models\IncidentNote;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin IncidentNote */
class IncidentNoteResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "text" => $this->text,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "author" => new UserBriefResource($this->whenLoaded("author")),
        ];
    }
}
