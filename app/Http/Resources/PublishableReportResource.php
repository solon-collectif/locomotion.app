<?php

namespace App\Http\Resources;

use App\Models\PublishableReport;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin PublishableReport */
class PublishableReportResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "status" => $this->status,
            "type" => $this->type,
            "progress" => $this->progress,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "report_key" => $this->report_key,
        ];
    }
}
