<?php

namespace App\Http\Resources;

use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class CommunityOverviewLoanableResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "sharing_mode" => $this->sharing_mode,
            "type" => $this->type,
            "pricing_loanable_type" => PricingLoanableTypeValues::forLoanable(
                $this->resource
            ),
            "name" => $this->name,

            // Only send owner role.
            "user_roles" => LoanableUserRoleResource::collection(
                $this->mergedUserRoles->filter(function ($userRole, $key) {
                    return $userRole->role === LoanableUserRoles::Owner;
                })
            ),

            "availability_status" => $this->availability_status,
            "details" => $this->getLoanableDetailsResource($this->details),
            "community_ids" => $this->community_ids,
            "position_google" => $this->position_google,
        ];
    }

    private function getLoanableDetailsResource($data): JsonResource
    {
        return match ($this->type) {
            LoanableTypes::Bike => new BikeResource($data),
            LoanableTypes::Trailer => new TrailerResource($data),
            LoanableTypes::Car => new CarResource($data),
        };
    }
}
