<?php

namespace App\Http\Resources;

use App\Models\LoanComment;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin LoanComment */
class LoanCommentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "deleted_at" => $this->deleted_at,
            "id" => $this->id,
            "text" => $this->when(
                !$this->resource->trashed() || $request->user()->isAdmin(),
                $this->text
            ),

            "loan_id" => $this->loan_id,
            "author_id" => $this->author_id,

            "author" => new UserBriefResource($this->whenLoaded("author")),
        ];
    }
}
