<?php

namespace App\Http\Resources;

use App\Models\Bike;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Bike */
class BikeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "model" => $this->model,
            "size" => $this->size,
            "bike_type" => $this->bike_type,
        ];
    }
}
