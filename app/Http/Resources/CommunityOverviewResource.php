<?php

namespace App\Http\Resources;

use App\Models\Community;
use App\Models\LoanableTypeDetails;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Community */
class CommunityOverviewResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "area" => $this->area,
            "type" => $this->type,
            "description" => $this->description,
            "chat_group_url" => $this->chat_group_url,
            "contact_email" => $this->contact_email,
            "starting_guide_url" => $this->starting_guide_url,
            "requires_residency_proof" => $this->requires_residency_proof,
            "requires_identity_proof" => $this->requires_identity_proof,
            "requires_custom_proof" => $this->requires_custom_proof,
            "approved_users_count" => $this->approved_users_count,
            "loanables_count_by_type" => $this->loanables_count_by_type,
            "allowed_loanable_types" => $this->allowedLoanableTypes
                ->map(fn(LoanableTypeDetails $t) => $t->name->value)
                ->toArray(),
        ];
    }
}
