<?php

namespace App\Http\Resources;

use App\Models\LoanableUserRole;
use App\Models\MergedLoanableUserRole;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin LoanableUserRole
 * @mixin MergedLoanableUserRole
 */
class LoanableUserRoleResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "ressource_type" => $this->ressource_type,
            "ressource_id" => $this->ressource_id,
            "role" => $this->role,
            "user_id" => $this->user_id,
            "title" => $this->title,
            "show_as_contact" => $this->show_as_contact,
            "pays_loan_price" => $this->pays_loan_price,
            "pays_loan_insurance" => $this->pays_loan_insurance,
            "loanable_id" => $this->loanable_id,

            "user" => new LoanableRoleUserRessource(
                $this->user,
                role: $this->role,
                showUserPhone: !!$this->show_as_contact
            ),
            "granted_by_user" => new UserBriefResource(
                $this->whenLoaded("grantedByUser")
            ),
        ];
    }
}
