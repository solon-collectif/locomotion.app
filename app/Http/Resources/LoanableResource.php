<?php

namespace App\Http\Resources;

use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * This public LoanableResource should only have information visible to anyone
 * who is registered on the app.
 *
 * @mixin Loanable
 */
class LoanableResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "pricing_loanable_type" => PricingLoanableTypeValues::forLoanable(
                $this->resource
            ),
            "active_incidents" => IncidentResource::collection(
                $this->whenLoaded("activeIncidents")
            ),
            "position_google" => $this->position_google,
            "position" => $this->position,
            "comments" => $this->comments,
            "shared_publicly" => $this->shared_publicly,
            "sharing_mode" => $this->sharing_mode,
            "location_description" => $this->location_description,
            "name" => $this->name,
            "community_ids" => $this->community_ids,
            "published" => $this->published,
            "availability_mode" => $this->availability_mode,
            "availability_json" => $this->availability_json,
            "availability_status" => $this->availability_status,
            "timezone" => $this->timezone,
            "instructions" => $this->when(
                \Gate::allows("viewInstructions", $this->resource),
                $this->instructions
            ),
            "return_instructions" => $this->when(
                \Gate::allows("viewInstructions", $this->resource),
                $this->return_instructions
            ),
            "max_loan_duration_in_minutes" =>
                $this->max_loan_duration_in_minutes,
            "trusted_borrower_instructions" => $this->when(
                \Gate::allows(
                    "viewTrustedBorrowerInstructions",
                    $this->resource
                ),
                $this->trusted_borrower_instructions
            ),
            "deleted_at" => $this->deleted_at,

            "merged_user_roles" => LoanableUserRoleResource::collection(
                $this->getFilteredLoanableRoles()
            ),

            "image" => new ImageResource($this->image),
            "images" => ImageResource::collection($this->images),
            "details" => $this->getLoanableDetailsResource($this->details),
            "library" => new LibraryResource($this->library?->load("avatar")),
        ];
    }

    private function getFilteredLoanableRoles()
    {
        if (\Gate::allows("viewUserRoles", $this->resource)) {
            return $this->mergedUserRoles
                ->filter(
                    fn($userRole) => in_array($userRole->role, [
                        LoanableUserRoles::Owner,
                        LoanableUserRoles::Coowner,
                        LoanableUserRoles::Manager,
                    ])
                )
                ->load("user.avatar");
        }

        return $this->mergedUserRoles
            ->filter(
                // Always show owner, but only coowners and managers if they are contacts
                fn($userRole) => $userRole->role === LoanableUserRoles::Owner ||
                    (in_array($userRole->role, [
                        LoanableUserRoles::Coowner,
                        LoanableUserRoles::Manager,
                    ]) &&
                        $userRole->show_as_contact)
            )
            ->load("user.avatar");
    }

    private function getLoanableDetailsResource($data): JsonResource
    {
        return match ($this->type) {
            LoanableTypes::Bike => new BikeResource($data),
            LoanableTypes::Trailer => new TrailerResource($data),
            LoanableTypes::Car => new CarResource($data->load("report")),
        };
    }
}
