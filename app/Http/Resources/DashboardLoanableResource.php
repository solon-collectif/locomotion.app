<?php

namespace App\Http\Resources;

use App\Enums\LoanableUserRoles;
use App\Models\Loanable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class DashboardLoanableResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->trashed()) {
            return (new ArchivedLoanableResource($this))->toArray($request);
        }

        return [
            "id" => $this->id,
            "image" => new ImageResource($this->image),
            "sharing_mode" => $this->sharing_mode,
            "name" => $this->name,
            "type" => $this->type,

            // Only send owner and coowner roles.
            "merged_user_roles" => LoanableUserRoleResource::collection(
                $this->mergedUserRoles->filter(function ($userRole, $key) {
                    return in_array($userRole->role, [
                        LoanableUserRoles::Owner,
                        LoanableUserRoles::Coowner,
                        LoanableUserRoles::Manager,
                    ]);
                })
            ),

            "availability_status" => $this->availability_status,
            "timezone" => $this->timezone,
            "active_incidents" => PublicIncidentResource::collection(
                $this->activeIncidents
            ),
            "library" => new LibraryResource($this->library),
            "loans" => DashboardLoanResource::collection(
                $this->whenLoaded("loans")
            ),
        ];
    }
}
