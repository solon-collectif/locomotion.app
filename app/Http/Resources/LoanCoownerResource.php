<?php

namespace App\Http\Resources;

use App\Models\LoanableUserRole;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin LoanableUserRole */
class LoanCoownerResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => new UserBriefResource(
                $this->user,
                $this->show_as_contact
            ),
            "title" => $this->title,
            "show_as_contact" => $this->show_as_contact,
            "pays_loan_price" => $this->pays_loan_price,
            "pays_loan_insurance" => $this->pays_loan_insurance,
        ];
    }
}
