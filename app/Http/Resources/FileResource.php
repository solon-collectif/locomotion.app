<?php

namespace App\Http\Resources;

use App\Models\File;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin File */
class FileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "original_filename" => $this->original_filename,
        ];
    }
}
