<?php

namespace App\Http\Resources;

use App\Enums\LoanableUserRoles;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class ArchivedLoanableResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "name" => "(Archivé) $this->name",
            "deleted_at" => $this->deleted_at,

            "coowners" => [],

            // Only send owner role.
            "user_roles" => LoanableUserRoleResource::collection(
                $this->mergedUserRoles->filter(function ($userRole, $key) {
                    return $userRole->role === LoanableUserRoles::Owner;
                })
            ),
        ];
    }
}
