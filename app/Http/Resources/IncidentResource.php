<?php

namespace App\Http\Resources;

use App\Enums\IncidentNotificationLevel;
use App\Models\Incident;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Incident */
class IncidentResource extends JsonResource
{
    public function toArray($request)
    {
        $userCanSeeIncidentDetails = \Gate::allows(
            "viewIncidentDetails",
            $this->resource
        );

        return [
            "id" => $this->id,
            "created_at" => $this->created_at,
            "executed_at" => $this->executed_at,
            "updated_at" => $this->updated_at,
            "status" => $this->status,
            "incident_type" => $this->incident_type,
            "blocking_until" => $this->blocking_until,
            "is_blocking" => $this->is_blocking,
            "start_at" => $this->start_at,
            "loan_id" => $this->loan_id,
            "loan_community_id" => $this->loan?->community_id,
            "loanable_id" => $this->loanable_id,
            // Check for comment access (is either loan borrower or blocked by it?)
            "details_hidden" => $this->when(!$userCanSeeIncidentDetails, true),
            $this->mergeWhen($userCanSeeIncidentDetails, [
                "show_details_to_blocked_borrowers" =>
                    $this->show_details_to_blocked_borrowers,
                "comments_on_incident" => $this->comments_on_incident,
                "assignee_id" => $this->assignee_id,
                "assignee" => new UserBriefResource(
                    $this->whenLoaded("assignee")
                ),
                "reported_by_user" => new UserBriefResource(
                    $this->whenLoaded("reportedByUser")
                ),
                "resolved_by_user" => new UserBriefResource(
                    $this->whenLoaded("resolvedByUser")
                ),
                "notes" => IncidentNoteResource::collection(
                    $this->whenLoaded("notes")
                ),
                "loanable" => new LoanableResource(
                    $this->whenLoaded("loanable")
                ),
                "notification_subscription" =>
                    $this->notifiedUsers()
                        ->where("user_id", $request->user()->id)
                        ->first()?->pivot->level ??
                    IncidentNotificationLevel::None,
            ]),
        ];
    }
}
