<?php

namespace App\Http\Resources;

use App\Models\Bike;
use App\Models\Car;
use App\Models\Loanable;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class LoanablePricingResource extends JsonResource
{
    public function toArray($request): array
    {
        if (is_a($this->details, Car::class)) {
            return [
                "type" => $this->type->value,
                "year_of_circulation" => $this->details->year_of_circulation,
                "transmission_mode" => $this->details->transmission_mode,
                "engine" => $this->details->engine,
                "pricing_category" => $this->details->pricing_category->value,
                "value_category" => $this->details->value_category,
                "daily_premium_from_value_category" =>
                    $this->details->daily_premium_from_value_category,
                "daily_premium_factor_from_value_category" =>
                    $this->details->daily_premium_factor_from_value_category,
            ];
        }

        if (is_a($this->details, Bike::class)) {
            return [
                "type" => $this->type->value,
                "size" => $this->details->size,
                "bike_type" => $this->details->bike_type,
            ];
        }

        return ["type" => $this->type->value];
    }
}
