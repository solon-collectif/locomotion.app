<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin User */
class ArchivedUserResource extends JsonResource
{
    public function toArray($request)
    {
        $label = $this->data_deleted_at ? "Supprimé" : "Désactivé";

        if (
            !$this->data_deleted_at &&
            $request->user()?->can("seeArchivedDetails", $this->getModel())
        ) {
            return [
                "id" => $this->id,
                "name" => "($label) $this->name",
                "last_name" => $this->last_name,
                "full_name" => "($label) $this->full_name",
                "deleted_at" => $this->deleted_at,
                "email" => $this->email,
                "data_deleted_at" => $this->data_deleted_at,
            ];
        }

        return [
            "id" => $this->id,
            "name" => "$label",
            "last_name" => "$label",
            "full_name" => "Compte $label",
            "deleted_at" => $this->deleted_at,
            "data_deleted_at" => $this->data_deleted_at,
        ];
    }
}
