<?php

namespace App\Http\Resources;

use App\Enums\LoanNotificationLevel;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loan */
class LoanResource extends JsonResource
{
    public static $wrap = false;
    public function toArray($request)
    {
        $userNotificationSettings = $this->notifiedUsers()
            ->where("user_id", $request->user()->id)
            ->first();

        return [
            "id" => $this->id,
            "created_at" => $this->created_at,
            "departure_at" => $this->departure_at,
            "accepted_at" => $this->accepted_at,
            "prepaid_at" => $this->prepaid_at,
            "duration_in_minutes" => $this->duration_in_minutes,
            "extension_duration_in_minutes" =>
                $this->extension_duration_in_minutes,
            "expenses_amount" => $this->expenses_amount,
            "estimated_distance" => $this->estimated_distance,
            "alternative_to" => $this->alternative_to,
            "alternative_to_other" => $this->alternative_to_other,
            "platform_tip" => $this->platform_tip,
            "canceled_at" => $this->canceled_at,
            "actual_expenses" => $this->actual_expenses,
            "status" => $this->status,
            "actual_return_at" => $this->actual_return_at,
            "owner_validated_at" => $this->owner_validated_at,
            "borrower_validated_at" => $this->borrower_validated_at,
            "auto_validated_at" => $this->auto_validated_at,
            "auto_validated_by_user" => new UserBriefResource(
                $this->autoValidatedByUser
            ),
            "actual_distance" => $this->actual_distance,
            "calendar_days" => $this->calendar_days,
            "is_free" => $this->is_free,

            "can_add_expenses" => $this->can_add_expenses,
            "requires_mileage" => $this->requires_mileage,
            "requires_detailed_mileage" => $this->requires_detailed_mileage,
            "owner_action_required" => $this->owner_action_required,
            "borrower_action_required" => $this->borrower_action_required,

            "borrower_must_pay_compensation" =>
                $this->borrower_must_pay_compensation,
            "borrower_may_contribute" => $this->borrower_may_contribute,
            "borrower_must_pay_insurance" => $this->borrower_must_pay_insurance,

            "is_self_service" => $this->is_self_service,
            "is_borrower_trusted" => $this->is_borrower_trusted,
            "applicable_amount_types" => $this->applicable_amount_types,
            "desired_contribution" => $this->desired_contribution,
            "subscription_available" => $this->subscription_available,
            "needs_validation" => $this->needs_validation,
            "borrower_total" => $this->borrower_total,
            "owner_total" => $this->owner_total,

            "mileage_start" => $this->mileage_start,
            "mileage_end" => $this->mileage_end,
            "community" => new LoanCommunityResource($this->community),
            "comments" => LoanCommentResource::collection($this->comments),

            "mileage_start_image" => new ImageResource(
                $this->mileageStartImage
            ),
            "mileage_end_image" => new ImageResource($this->mileageEndImage),
            "expense_image" => new ImageResource($this->expenseImage),

            "borrower_user" => new UserBriefResource(
                $this->borrowerUser->load("avatar"),
                $this->canSeeLoanParticipantInfo($request->user()),
                showUserEmail: true
            ),

            "loanable" => new LoanLoanableResource(
                $this->loanable->load([
                    "activeIncidents.assignee.avatar",
                    "activeIncidents.resolvedByUser.avatar",
                    "activeIncidents.notes.author.avatar",
                    "activeIncidents.reportedByUser.avatar",
                    "library.avatar",
                    "library.userRoles",
                ]),
                $this->resource
            ),

            "borrower_invoice" => $this->when(
                $request->user()->can("viewBorrowerInvoice", $this->resource),
                fn() => new InvoiceResource($this->getBorrowerInvoice())
            ),

            "owner_invoice" => $this->when(
                $request->user()->can("viewOwnerInvoice", $this->resource),
                fn() => new InvoiceResource($this->getOwnerInvoice())
            ),

            "notification_subscription" =>
                $userNotificationSettings?->pivot->level ??
                LoanNotificationLevel::None,

            "notification_last_seen" =>
                $userNotificationSettings?->pivot->last_seen ?? null,

            "attempt_autocomplete_at" => $this->attempt_autocomplete_at,
        ];
    }

    private function canSeeLoanParticipantInfo(User $user)
    {
        return $user->isAdmin() ||
            $user->isAdminOfCommunity($this->community_id) ||
            $user->id === $this->borrower_user_id ||
            $this->loanable->hasOwnerOrCoowner($user);
    }
}
