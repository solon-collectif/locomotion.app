<?php

namespace App\Http\Resources;

use App\Models\Subscription;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Subscription */
class SubscriptionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "id" => $this->id,
            "start_date" => $this->start_date,
            "end_date" => $this->end_date,
            "type" => $this->type,
            "reason" => $this->reason,
            "community_id" => $this->communityUser->community_id,
            "user_id" => $this->communityUser->user_id,

            "loanable_types" => $this->loanable_types,
            "granted_by_user" => new UserBriefResource($this->grantedByUser),
        ];
    }
}
