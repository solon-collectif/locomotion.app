<?php

namespace App\Http\Resources;

use App\Models\BillItem;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin BillItem */
class InvoiceItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "item_type" => $this->item_type,
            "label" => $this->label,
            "amount" => $this->amount,
            "taxes_tps" => $this->taxes_tps,
            "taxes_tvq" => $this->taxes_tvq,
            "total" => $this->total,
            "contribution_community_id" => $this->contribution_community_id,
            "meta" => $this->meta,
        ];
    }
}
