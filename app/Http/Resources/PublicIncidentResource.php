<?php

namespace App\Http\Resources;

use App\Models\Incident;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Incident */
class PublicIncidentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "status" => $this->status,
            "incident_type" => $this->incident_type,
            "blocking_until" => $this->blocking_until,
            "is_blocking" => $this->is_blocking,
            "start_at" => $this->start_at,
            "loan_id" => $this->loan_id,
            "loanable_id" => $this->loanable_id,
        ];
    }
}
