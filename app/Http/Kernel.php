<?php

namespace App\Http;

use App\Http\Middleware\AddClientVersionHeaderMiddleware;
use App\Http\Middleware\AuthenticateWithQueryParam;
use App\Http\Middleware\CheckForMaintenanceMode;
use App\Http\Middleware\Cors;
use App\Http\Middleware\EncryptCookies;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\TrimStrings;
use App\Http\Middleware\TrustProxies;
use App\Http\Middleware\WrapInTransaction;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Middleware\ValidateSignature;

class Kernel extends HttpKernel
{
    protected $middleware = [
        TrustProxies::class,
        CheckForMaintenanceMode::class,
        ValidatePostSize::class,
        TrimStrings::class,
        Cors::class,
    ];

    protected $middlewareGroups = [
        "web" => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            SubstituteBindings::class,
        ],

        "api" => [
            "throttle:120,1",
            "bindings",
            AddClientVersionHeaderMiddleware::class,
        ],
    ];

    protected $middlewareAliases = [
        "auth" => Authenticate::class,
        "auth.basic" => AuthenticateWithBasicAuth::class,
        "auth.query" => AuthenticateWithQueryParam::class,
        "bindings" => SubstituteBindings::class,
        "cache.headers" => SetCacheHeaders::class,
        "can" => Authorize::class,
        "guest" => RedirectIfAuthenticated::class,
        "signed" => ValidateSignature::class,
        "throttle" => ThrottleRequests::class,
        "transaction" => WrapInTransaction::class,
        "verified" => EnsureEmailIsVerified::class,
    ];

    protected $middlewarePriority = [
        Authenticate::class,
        ThrottleRequests::class,
        SubstituteBindings::class,
        Authorize::class,
    ];
}
