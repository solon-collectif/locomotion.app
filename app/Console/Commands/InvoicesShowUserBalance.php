<?php

namespace App\Console\Commands;

use App\Reports\UserBalanceReport;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class InvoicesShowUserBalance extends Command
{
    protected $signature = "invoices:show:userbalance {--user-geq=} {--user=} {--at=} {--mismatch-only} {--non-zero-only} {--format=}";

    public function handle(): void
    {
        $options = $this->options();

        UserBalanceReport::generate(
            userId: $options["user"] > 0 ? (int) $options["user"] : null,
            minUserId: $options["user-geq"] > 0
                ? (int) $options["user-geq"]
                : null,
            at: $options["at"]
                ? new CarbonImmutable($options["at"], "America/Toronto")
                : null,
            nonZeroOnly: $options["non-zero-only"],
            mismatchOnly: $options["mismatch-only"]
        );
    }
}
