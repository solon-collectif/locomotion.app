<?php

namespace App\Console\Commands;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanPrePaymentMissingMail;
use App\Mail\UserMail;
use App\Models\Loan;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class EmailLoanPrePaymentMissing extends Command
{
    protected $signature = "email:loan:pre_payment_missing";

    protected $description = "Send emails when pre-payment is still missing in 24 hours";

    public function handle(): void
    {
        $now = CarbonImmutable::now();
        $loans = Loan::where("status", "=", LoanStatus::Accepted)
            ->where("departure_at", "<=", $now->addDay())
            ->where("departure_at", ">", $now)
            ->where("loans.created_at", "<=", $now->subHours(3))
            ->where("meta->sent_loan_pre_payment_missing_email", null)
            ->get();

        foreach ($loans as $loan) {
            if ($loan->borrowerCanPay()) {
                $loan->setLoanStatusConfirmed();
            } else {
                UserMail::queueToLoanBorrower(
                    new LoanPrePaymentMissingMail($loan->borrowerUser, $loan),
                    $loan
                );

                $meta = $loan->meta;
                $meta["sent_loan_pre_payment_missing_email"] = true;
                $loan->meta = $meta;
            }

            $loan->save();
        }
    }
}
