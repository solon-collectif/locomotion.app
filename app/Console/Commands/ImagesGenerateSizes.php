<?php

namespace App\Console\Commands;

use App\Jobs\GenerateImageSizes;
use App\Models\Image;
use Illuminate\Console\Command;

class ImagesGenerateSizes extends Command
{
    protected $signature = "images:generate:sizes
                            {--regenerate=}";

    protected $description = "Generates missing thumbnails for every image";

    public function handle(): void
    {
        $sizesToRegenerate = $this->option("regenerate")
            ? explode(",", $this->option("regenerate"))
            : [];

        $totalImages = Image::count();
        $countPerJob = 50;
        $jobs = [];
        for ($i = 0; $i < $totalImages; $i += $countPerJob) {
            $jobs[] = new GenerateImageSizes(
                $i,
                $countPerJob,
                $sizesToRegenerate
            );
        }

        \Bus::batch($jobs)
            ->onQueue("low")
            ->dispatch();
    }
}
