<?php

namespace App\Console\Commands;

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Community;
use App\Models\Loan;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class InvoicesGenerateContributionReport extends Command
{
    protected $signature = "invoices:generate:contributionreport {--from=} {--to=} {--format=}";

    public function handle()
    {
        $options = $this->options();

        $options["from"] = new CarbonImmutable(
            $options["from"],
            "America/Toronto"
        );
        $options["to"] = new CarbonImmutable($options["to"], "America/Toronto");

        $fieldTitles = [
            "id" => "ID item",
            "invoice.id" => "ID facture",
            "invoice.date" => "Date facture",
            "user.id" => "ID Utilisateur",
            "loan.id" => "ID emprunt",
            "loanable.id" => "ID Véhicule",

            "community.name" => "Communauté",

            "amount" => "Montant",
            "taxes_tps" => "TPS",
            "taxes_tvq" => "TVQ",
            "total" => "Total",
        ];

        // Print titles for csv format.
        $outputStream = null;
        if ($options["format"] === "csv") {
            $outputStream = STDOUT;
            fputcsv($outputStream, $fieldTitles);
        }

        // Get contribution invoice items for invoices that were issued in the
        // given interval.
        $invoiceItems = BillItem::whereIn("item_type", [
            BillItemTypes::donationLoan,
            BillItemTypes::donationBalance,
            BillItemTypes::loanContribution,
            BillItemTypes::contributionYearly,
        ])
            ->whereHas("invoice", function ($q) use ($options) {
                $q->where("created_at", ">=", $options["from"])->where(
                    "created_at",
                    "<",
                    $options["to"]
                );
            })
            ->cursor();

        $communitiesById = Community::pluck("name", "id")->all();

        $contributionSummary = [
            null => [
                "amount" => 0,
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "total" => 0,
            ],
        ];

        foreach ($invoiceItems as $item) {
            $itemMeta = $item->meta;
            $loan = isset($itemMeta["entityId"])
                ? Loan::find($itemMeta["entityId"])
                : null;

            $communityId = $item->contribution_community_id;

            $invoice = $item->invoice;

            $fieldValues = [
                "id" => $item->id,
                "invoice.id" => $item->invoice_id,
                "invoice.date" => $invoice->created_at,
                "user.id" => $invoice->user_id,
                "loan.id" => $loan ? $loan->id : "",
                "loanable.id" => $loan ? $loan->loanable_id : "",

                "community.name" => $communityId
                    ? $communitiesById[$communityId]
                    : "",

                "amount" => $item->amount,
                "taxes_tps" => $item->taxes_tps,
                "taxes_tvq" => $item->taxes_tvq,
                "total" => $item->total,
            ];

            if ($options["format"] === "csv") {
                fputcsv($outputStream, self::formatFields($fieldValues));
            }

            // Add to summary
            if (array_key_exists($communityId, $contributionSummary)) {
                $contributionSummary[$communityId]["amount"] += $item->amount;
                $contributionSummary[$communityId]["taxes_tps"] +=
                    $item->taxes_tps;
                $contributionSummary[$communityId]["taxes_tvq"] +=
                    $item->taxes_tvq;
                $contributionSummary[$communityId]["total"] += $item->total;
            } else {
                $contributionSummary[$communityId] = [
                    "amount" => $item->amount,
                    "taxes_tps" => $item->taxes_tps,
                    "taxes_tvq" => $item->taxes_tvq,
                    "total" => $item->total,
                ];
            }
        }

        $summaryTotal = [
            "amount" => 0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "total" => 0,
        ];

        foreach ($contributionSummary as $communitySummary) {
            foreach ($communitySummary as $key => $amount) {
                $summaryTotal[$key] += $amount;
            }
        }
        $contributionSummary["total"] = $summaryTotal;

        $fieldValues = [
            "id" => "",
            "invoice.id" => "",
            "invoice.date" => "",
            "user.id" => "",
            "loan.id" => "",
            "loanable.id" => "",

            "community.name" => "Communauté",

            "amount" => "Montant",
            "taxes_tps" => "TPS",
            "taxes_tvq" => "TVQ",
            "total" => "Total",
        ];
        if ($options["format"] === "csv") {
            // Add blank line before sumary.
            fputcsv($outputStream, []);
            fputcsv($outputStream, $fieldValues);
        }

        foreach ($contributionSummary as $communityId => $summary) {
            $fieldValues["community.name"] = match ($communityId) {
                "" => "Réseau",
                "total" => "total",
                default => $communitiesById[$communityId],
            };

            $fieldValues["amount"] = $summary["amount"];
            $fieldValues["taxes_tps"] = $summary["taxes_tps"];
            $fieldValues["taxes_tvq"] = $summary["taxes_tvq"];
            $fieldValues["total"] = $summary["total"];

            if ($options["format"] === "csv") {
                fputcsv($outputStream, self::formatFields($fieldValues));
            }
        }
    }

    protected static function formatFields($fieldValues)
    {
        $fieldValues["amount"] = $fieldValues["amount"]
            ? self::formatCurrency($fieldValues["amount"])
            : "";
        $fieldValues["taxes_tps"] = $fieldValues["taxes_tps"]
            ? self::formatCurrency($fieldValues["taxes_tps"])
            : "";
        $fieldValues["taxes_tvq"] = $fieldValues["taxes_tvq"]
            ? self::formatCurrency($fieldValues["taxes_tvq"])
            : "";
        $fieldValues["total"] = $fieldValues["total"]
            ? self::formatCurrency($fieldValues["total"])
            : "";

        return $fieldValues;
    }

    protected static function formatCurrency($amount)
    {
        return str_replace(".", ",", (string) round($amount, 2));
    }
}
