<?php

namespace App\Console\Commands;

use App\Enums\LoanStatus;
use App\Events\LoanEndedEvent;
use App\Events\LoanPaidEvent;
use App\Events\LoanValidatedEvent;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\Loan\LoanNeedsPaymentMail;
use App\Mail\Loan\LoanStalledMail;
use App\Mail\UserMail;
use App\Models\Loan;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class MoveLoanForward extends Command
{
    protected $signature = "loans:forward";

    protected $description = "Move loans forward";

    /**
     * @return Builder<Loan>
     */
    public function expiredLoanQuery(): Builder
    {
        return Loan::query()
            ->where(
                "actual_return_at",
                "<=",
                CarbonImmutable::now()->subDays(2)
            )
            ->where(
                fn($q) => $q
                    ->whereNull("attempt_autocomplete_at")
                    ->orWhere(
                        "attempt_autocomplete_at",
                        "<=",
                        CarbonImmutable::now()
                    )
            );
    }

    public function handle(): void
    {
        $now = CarbonImmutable::now();

        // Loans that were never accepted or prepaid: never had access to instructions, so we
        // assume they did not happen.
        $unconfirmedLoans = Loan::whereIn("status", [
            LoanStatus::Requested,
            LoanStatus::Accepted,
        ])
            ->where("actual_return_at", "<=", $now)
            ->get();
        foreach ($unconfirmedLoans as $loan) {
            // Cancel expired loans silently, without sending notification
            $loan->setLoanStatusCanceled();
            $loan->save();
        }

        $newlyEndedLoans = Loan::whereIn("status", [
            LoanStatus::Confirmed,
            LoanStatus::Ongoing,
        ])
            ->where("actual_return_at", "<=", $now)
            ->get();
        foreach ($newlyEndedLoans as $loan) {
            $loan->setLoanStatusEnded();
            $loan->save();
            event(new LoanEndedEvent($loan));
        }

        $newlyStartedLoans = Loan::where("status", LoanStatus::Confirmed)
            ->where("departure_at", "<=", $now)
            ->get();
        foreach ($newlyStartedLoans as $loan) {
            $loan->setLoanStatusOngoing();
            $loan->save();
        }

        $gracePeriodLoans = Loan::where("status", LoanStatus::Validated)
            // if attempt_autocomplete_at is set, this means loan was already checked.
            ->whereNull("attempt_autocomplete_at")
            ->where(
                "actual_return_at",
                "<=",
                $now->subMinutes(Loan::$GRACE_PERIOD_MINUTES)
            )
            ->get();

        foreach ($gracePeriodLoans as $loan) {
            // End grace period by setting the validation state which will attempt to complete
            // or pay the loan, if possible.
            $loan->setLoanStatusValidated();
            if ($loan->status === LoanStatus::Completed) {
                $loan->save();
                UserMail::queueToLoanNotifiedUsers(
                    fn(User $user) => new LoanCompletedMail($user, $loan),
                    $loan
                );
            } else {
                $loan->attempt_autocomplete_at = $loan->actual_return_at->addDays(
                    2
                );
                $loan->save();
            }
        }

        $endedLoans = $this->expiredLoanQuery()
            ->where("status", LoanStatus::Ended)
            ->get();
        foreach ($endedLoans as $loan) {
            if (!$loan->has_required_info_to_validate) {
                // We explicitly sent to borrower, no matter their notification settings.
                UserMail::queue(
                    new LoanStalledMail($loan, $loan->borrowerUser),
                    $loan->borrowerUser
                );
                UserMail::queueToLoanNotifiedUsers(
                    fn(User $user) => new LoanStalledMail($loan, $user),
                    $loan,
                    $loan->borrowerUser // Do not send it twice to borrower
                );
            } else {
                $loan->setLoanStatusValidated();
                $loan->save();
                event(new LoanValidatedEvent($loan));
            }

            $loan->attempt_autocomplete_at = $now->addDays(2);
            $loan->save();
        }

        $validatedLoans = $this->expiredLoanQuery()
            ->where("status", LoanStatus::Validated)
            ->get();
        foreach ($validatedLoans as $loan) {
            if (
                !$loan->borrowerCanPay() &&
                $loan->borrowerCanPayMandatoryTotal()
            ) {
                $loan->platform_tip = 0;
                $loan->clearInvoices();
            }

            if ($loan->borrowerCanPay()) {
                $loan->pay();
                $loan->save();
                event(new LoanPaidEvent($loan));
            } else {
                // Do not use `UserMail::queueToLoanBorrower`: we want to bypass user notification
                // settings. Loan must be paid!
                UserMail::queue(
                    new LoanNeedsPaymentMail($loan),
                    $loan->borrowerUser
                );
            }

            $loan->attempt_autocomplete_at = $now->addDays(2);
            $loan->save();
        }
    }
}
