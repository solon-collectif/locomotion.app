<?php

namespace App\Console\Commands;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanUpcomingMail;
use App\Mail\UserMail;
use App\Models\Loan;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class EmailLoanUpcoming extends Command
{
    protected $signature = "email:loan:upcoming";

    protected $description = "Send loan upcoming emails (in three hours)";

    public function handle(): void
    {
        $now = CarbonImmutable::now();
        $loans = Loan::whereIn("status", [
            LoanStatus::Confirmed,
            LoanStatus::Accepted,
        ])
            ->where("departure_at", "<=", $now->addHours(3))
            ->where("departure_at", ">", $now)
            ->where("loans.created_at", "<=", $now->subHours(3))
            ->where("meta->sent_loan_upcoming_email", null)
            ->cursor();

        foreach ($loans as $loan) {
            UserMail::queueToLoanNotifiedUsers(
                fn(User $user) => new LoanUpcomingMail($user, $loan),
                $loan
            );

            $meta = $loan->meta;
            $meta["sent_loan_upcoming_email"] = true;
            $loan->meta = $meta;

            $loan->save();
        }
    }
}
