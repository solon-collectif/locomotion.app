<?php
namespace App\Console\Commands;
use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Loan;
use App\Pricings\LoanInvoiceHelper;
use Illuminate\Console\Command;
use Log;

class SplitExcessContributionBillItems extends Command
{
    protected $signature = "contributions:split";

    public function handle(): void
    {
        $loans = Loan::where("status", "completed")
            ->where("platform_tip", ">", 0)
            ->whereHas(
                "borrowerInvoice.billItems",
                fn($query) => $query
                    ->where("item_type", BillItemTypes::loanContribution)
                    ->whereIn("meta->pricing_id", [191, 190])
            )
            ->with(["community", "borrowerInvoice.billItems"])
            ->get();

        foreach ($loans as $loan) {
            $contributionBillItems = $loan->borrowerInvoice->billItems->where(
                "item_type",
                BillItemTypes::loanContribution
            );

            if ($contributionBillItems->count() !== 1) {
                continue;
            }

            /** @var BillItem $contributionBillItem */
            $contributionBillItem = $contributionBillItems->first();
            if ($contributionBillItem->contribution_community_id) {
                Log::error(
                    "Unexpected: only community contribution for loan $loan->id"
                );
                continue;
            }

            if (!isset($contributionBillItem->meta["pricing_id"])) {
                Log::error(
                    "Unexpected: no pricing id meta for loan $loan->id, bill item $contributionBillItem->id"
                );
                continue;
            }
            $pricingId = $contributionBillItem->meta["pricing_id"];

            $excessAmount = -1;
            $newTotal = null;
            if ($pricingId === 191) {
                $excessAmount = -$contributionBillItem->total - 8;
                $newTotal = -8;
            } elseif ($pricingId === 190) {
                $excessAmount = -$contributionBillItem->total - 2;
                $newTotal = -2;
            }

            if ($excessAmount <= 0) {
                // no excess
                continue;
            }

            $contributionBillItem
                ->fill(LoanInvoiceHelper::splitIntoAmountAndTaxes($newTotal))
                ->save();

            $newBillItem = BillItem::create([
                "item_type" => BillItemTypes::loanContribution,
                "invoice_id" => $contributionBillItem->invoice_id,
                ...LoanInvoiceHelper::splitIntoAmountAndTaxes(-$excessAmount),
                "contribution_community_id" => $loan->community_id,
                "label" => "Contribution à la communauté {$loan->community->name}",
                "meta" => [
                    "pricing_name" =>
                        "Contribution excédentaire à la communauté",
                    ...LoanInvoiceHelper::getLoanMeta($loan),
                ],
            ]);
        }
    }
}
