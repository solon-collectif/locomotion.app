<?php

namespace App\Console\Commands;

use App\Reports\InsuranceQuarterlyReport;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class InvoicesGenerateInsuranceReport extends Command
{
    protected $signature = "invoices:generate:insurancereport {--from=} {--to=} {--format=}";

    public function handle()
    {
        $options = $this->options();

        InsuranceQuarterlyReport::generate(
            new CarbonImmutable($options["from"], "America/Toronto"),
            new CarbonImmutable($options["to"], "America/Toronto"),
            $options["format"]
        );
    }
}
