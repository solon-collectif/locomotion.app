<?php

namespace App\Console\Commands;

use App\Enums\BillItemTypes;
use App\Models\Community;
use App\Models\Invoice;
use App\Models\Loan;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class InvoicesGenerateAccountingReport extends Command
{
    protected $signature = "invoices:generate:accountingreport {--from=} {--to=} {--format=} {--group-items} {--provision-only}";

    public function handle()
    {
        $options = $this->options();

        $options["from"] = new CarbonImmutable(
            $options["from"],
            "America/Toronto"
        );
        $options["to"] = new CarbonImmutable($options["to"], "America/Toronto");

        $communitiesById = Community::pluck("name", "id")->all();

        $fieldTitles = [
            "id" => "ID item",
            "invoice.id" => "ID facture",
            "invoice.date" => "Date facture",
            "user.id" => "ID Utilisateur",
            "item_type" => "Type d'item",

            "loan.id" => "ID emprunt",
            "loan.departure_at" => "Départ",
            "loan.actual_return_at" => "Retour",
            "loanable.id" => "ID Véhicule",

            "donation.community" => "Contribution - Communauté",
            "donation.amount" => "Contribution - Montant",
            "donation.taxes_tps" => "Contribution - TPS",
            "donation.taxes_tvq" => "Contribution - TVQ",
            "donation.total" => "Contribution - Total",

            "balance.provision" => "Approvisionnement",
            "fees.stripe" => "Frais Stripe",

            "balance.refund" => "Remboursements",

            "insurance.amount" => "Coût assurance",

            "item.label" => "Description",
        ];

        $summary = [
            "id" => "",
            "invoice.id" => "",
            "invoice.date" => "",
            "user.id" => "",
            "item_type" => "",

            "loan.id" => "",
            "loan.departure_at" => "",
            "loan.actual_return_at" => "",
            "loanable.id" => "",

            "contribution.community" => "",
            "donation.amount" => 0,
            "donation.taxes_tps" => 0,
            "donation.taxes_tvq" => 0,
            "donation.total" => 0,

            "balance.provision" => 0,
            "fees.stripe" => 0,

            "balance.refund" => 0,

            "insurance.amount" => 0,

            "item.label" => "",
        ];

        // Print titles for csv format.
        $outputStream = null;
        if ($options["format"] === "csv") {
            $outputStream = STDOUT;
            fputcsv($outputStream, $fieldTitles);
        }

        $invoices = Invoice::where("created_at", ">=", $options["from"])
            ->where("created_at", "<", $options["to"])
            ->orderBy("created_at", "ASC")
            ->cursor();

        // Generate Stripe data only.
        if ($options["provision-only"]) {
            $includeItemTypes = [
                BillItemTypes::balanceProvision,
                BillItemTypes::feesStripe,
                BillItemTypes::balanceRefund,
            ];
        }

        $skipItemTypes = [
            BillItemTypes::loanPrice,
            BillItemTypes::loanExpenses,
        ];

        foreach ($invoices as $invoice) {
            $invoiceItems = $invoice->billItems;

            $user = $invoice->user;

            $invoiceHasItems = false;

            $fieldValues = [
                "id" => "",
                "invoice.id" => $invoice->id,
                "invoice.date" => $invoice->created_at,
                "user.id" => $user ? $user->id : "",
                "item_type" => "",

                "loan.id" => "",
                "loan.departure_at" => "",
                "loan.actual_return_at" => "",
                "loanable.id" => "",

                "community.name" => "",
                "donation.amount" => 0,
                "donation.taxes_tps" => 0,
                "donation.taxes_tvq" => 0,
                "donation.total" => 0,

                "balance.provision" => 0,
                "fees.stripe" => 0,
                "balance.refund" => 0,

                "insurance.amount" => 0,
                "item.label" => "",
            ];

            foreach ($invoiceItems as $item) {
                if (
                    (!empty($includeItemTypes) &&
                        !in_array($item->item_type, $includeItemTypes)) ||
                    in_array($item->item_type, $skipItemTypes)
                ) {
                    continue;
                }

                $invoiceHasItems = true;

                $itemMeta = $item->meta;

                $communityId = $item->contribution_community_id;

                $loan = isset($itemMeta["entityId"])
                    ? Loan::find($itemMeta["entityId"])
                    : null;

                $isDonation =
                    $item->item_type === BillItemTypes::donationLoan ||
                    $item->item_type === BillItemTypes::donationBalance ||
                    $item->item_type === BillItemTypes::loanContribution ||
                    $item->item_type === BillItemTypes::contributionYearly;

                $isInsurance =
                    $item->item_type === BillItemTypes::loanInsurance;

                if ($options["group-items"]) {
                    // Only expect one loan for a given invoice.
                    $fieldValues["loan.id"] = $loan ? $loan->id : "";
                    $fieldValues["loan.departure_at"] = $loan
                        ? $loan->departure_at
                        : "";
                    $fieldValues["loan.actual_return_at"] = $loan
                        ? $loan->actual_return_at
                        : "";
                    $fieldValues["loanable.id"] = $loan
                        ? $loan->loanable_id
                        : "";

                    $fieldValues["community.name"] = $communityId
                        ? $communitiesById[$communityId]
                        : "";

                    $fieldValues["donation.amount"] += $isDonation
                        ? $item->amount
                        : 0;
                    $fieldValues["donation.taxes_tps"] += $isDonation
                        ? $item->taxes_tps
                        : 0;
                    $fieldValues["donation.taxes_tvq"] += $isDonation
                        ? $item->taxes_tvq
                        : 0;
                    $fieldValues["donation.total"] += $isDonation
                        ? $item->total
                        : 0;

                    $fieldValues["balance.provision"] +=
                        $item->item_type === BillItemTypes::balanceProvision
                            ? $item->total
                            : 0;
                    $fieldValues["fees.stripe"] +=
                        $item->item_type === BillItemTypes::feesStripe
                            ? $item->total
                            : 0;
                    $fieldValues["balance.refund"] +=
                        $item->item_type === BillItemTypes::balanceRefund
                            ? $item->total
                            : 0;
                    $fieldValues["insurance.amount"] += $isInsurance
                        ? $item->total
                        : 0;
                } else {
                    $fieldValues = [
                        "id" => $item->id,
                        "invoice.id" => $item->invoice_id,
                        "invoice.date" => $invoice->created_at,
                        "user.id" => $user ? $user->id : "",
                        "item_type" => $item->item_type->value,

                        "loan.id" => $loan ? $loan->id : "",
                        "loan.departure_at" => $loan ? $loan->departure_at : "",
                        "loan.actual_return_at" => $loan
                            ? $loan->actual_return_at
                            : "",
                        "loanable.id" => $loan ? $loan->loanable_id : "",

                        "community.name" => $communityId
                            ? $communitiesById[$communityId]
                            : "",

                        "donation.amount" => $isDonation ? $item->amount : 0,
                        "donation.taxes_tps" => $isDonation
                            ? $item->taxes_tps
                            : 0,
                        "donation.taxes_tvq" => $isDonation
                            ? $item->taxes_tvq
                            : 0,
                        "donation.total" => $isDonation ? $item->total : 0,

                        "balance.provision" =>
                            $item->item_type === BillItemTypes::balanceProvision
                                ? $item->total
                                : 0,
                        "fees.stripe" =>
                            $item->item_type === BillItemTypes::feesStripe
                                ? $item->total
                                : 0,
                        "balance.refund" =>
                            $item->item_type === BillItemTypes::balanceRefund
                                ? $item->total
                                : 0,

                        "insurance.amount" => $isInsurance ? $item->total : 0,

                        "item.label" => $item->label,
                    ];

                    if ($options["format"] === "csv") {
                        fputcsv(
                            $outputStream,
                            self::formatFields($fieldValues)
                        );
                    }
                }

                if ($isDonation) {
                    $summary["donation.amount"] += $item->amount;
                    $summary["donation.taxes_tps"] += $item->taxes_tps;
                    $summary["donation.taxes_tvq"] += $item->taxes_tvq;
                    $summary["donation.total"] += $item->total;
                } elseif ($isInsurance) {
                    $summary["insurance.amount"] += $item->total;
                } elseif (
                    $item->item_type === BillItemTypes::balanceProvision
                ) {
                    $summary["balance.provision"] += $item->total;
                } elseif ($item->item_type === BillItemTypes::feesStripe) {
                    $summary["fees.stripe"] += $item->total;
                } elseif ($item->item_type === BillItemTypes::balanceRefund) {
                    $summary["balance.refund"] += $item->total;
                }
            }

            if ($options["group-items"] && $invoiceHasItems) {
                if ($options["format"] === "csv") {
                    fputcsv($outputStream, self::formatFields($fieldValues));
                }
            }
        }

        if ($options["format"] === "csv") {
            $summary["donation.amount"] = self::formatCurrency(
                $summary["donation.amount"]
            );
            $summary["donation.taxes_tps"] = self::formatCurrency(
                $summary["donation.taxes_tps"]
            );
            $summary["donation.taxes_tvq"] = self::formatCurrency(
                $summary["donation.taxes_tvq"]
            );
            $summary["donation.total"] = self::formatCurrency(
                $summary["donation.total"]
            );
            $summary["insurance.amount"] = self::formatCurrency(
                $summary["insurance.amount"]
            );
            $summary["balance.provision"] = self::formatCurrency(
                $summary["balance.provision"]
            );
            $summary["fees.stripe"] = self::formatCurrency(
                $summary["fees.stripe"]
            );
            $summary["balance.refund"] = self::formatCurrency(
                $summary["balance.refund"]
            );

            fputcsv($outputStream, $summary);
        }
    }

    protected static function formatFields($fieldValues)
    {
        $fieldValues["donation.amount"] = $fieldValues["donation.amount"]
            ? self::formatCurrency($fieldValues["donation.amount"])
            : "";
        $fieldValues["donation.taxes_tps"] = $fieldValues["donation.taxes_tps"]
            ? self::formatCurrency($fieldValues["donation.taxes_tps"])
            : "";
        $fieldValues["donation.taxes_tvq"] = $fieldValues["donation.taxes_tvq"]
            ? self::formatCurrency($fieldValues["donation.taxes_tvq"])
            : "";
        $fieldValues["donation.total"] = $fieldValues["donation.total"]
            ? self::formatCurrency($fieldValues["donation.total"])
            : "";

        $fieldValues["balance.provision"] = $fieldValues["balance.provision"]
            ? self::formatCurrency($fieldValues["balance.provision"])
            : "";
        $fieldValues["fees.stripe"] = $fieldValues["fees.stripe"]
            ? self::formatCurrency($fieldValues["fees.stripe"])
            : "";
        $fieldValues["balance.refund"] = $fieldValues["balance.refund"]
            ? self::formatCurrency($fieldValues["balance.refund"])
            : "";

        $fieldValues["insurance.amount"] = $fieldValues["insurance.amount"]
            ? self::formatCurrency($fieldValues["insurance.amount"])
            : "";

        return $fieldValues;
    }

    protected static function formatCurrency($amount)
    {
        return str_replace(".", ",", (string) round($amount, 2));
    }
}
