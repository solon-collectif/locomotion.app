<?php

namespace App\Console\Commands;

use App\Models\Invoice;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;

class InvoicesShowUserHistory extends Command
{
    protected $signature = "invoices:show:userhistory {--user=} {--non-zero-only} {--show-items} {--item-types=} {--from=} {--to=} {--format=}";

    public function handle()
    {
        $options = $this->options();

        $user = User::where("id", $options["user"])->first();

        $invoices = Invoice::query();

        if ($options["user"] > 0) {
            $invoices = $invoices->where("user_id", (int) $options["user"]);
        }

        // Convert item-types option to an array.
        if ($options["item-types"]) {
            $options["show-items"] = true;
            $options["item-types"] = explode(",", $options["item-types"]);
        }

        if ($options["from"]) {
            $options["from"] = new CarbonImmutable(
                $options["from"],
                "America/Toronto"
            );
            $invoices = $invoices->where("created_at", ">=", $options["from"]);
        }

        if ($options["to"]) {
            $options["to"] = new CarbonImmutable(
                $options["to"],
                "America/Toronto"
            );
            $invoices = $invoices->where("created_at", "<", $options["to"]);
        }

        $invoices = $invoices->orderBy("created_at", "asc")->cursor();

        if ($options["show-items"]) {
            $fieldTitles = [
                "invoice.id" => "ID facture",
                "invoice.date" => "Date facture",
                "item.id" => "ID item",
                "item.item_type" => "Type d'item",
                "balance_change" => "Variation au solde",
                "item.description" => "Description",
            ];
        } else {
            $fieldTitles = [
                "invoice.id" => "ID facture",
                "invoice.date" => "Date facture",
                "balance_change" => "Variation au solde",
                "invoice.title" => "Titre",
            ];
        }

        $outputStream = null;
        if ($options["format"] === "csv") {
            $outputStream = STDOUT;

            if ($options["user"] > 0) {
                fputcsv($outputStream, [
                    "Utilisateur",
                    $user->id,
                    $user->full_name,
                ]);
            }

            if ($options["from"]) {
                fputcsv($outputStream, ["À partir de", $options["from"]]);
            }

            if ($options["to"]) {
                fputcsv($outputStream, ["Jusqu'à", $options["to"]]);
            }

            fputcsv($outputStream, $fieldTitles);
        }

        $totalBalanceChange = 0;
        $userManualInvoices = 0;
        foreach ($invoices as $invoice) {
            $isManualInvoice = !$invoice->paid_at;

            $userManualInvoices += $isManualInvoice;

            $invoiceBalanceChange = $invoice->getUserBalanceChange();

            if ($options["show-items"]) {
                $invoiceItems = $invoice->billItems;

                foreach ($invoiceItems as $item) {
                    $itemBalanceChange = $item->getUserBalanceChange();

                    if (
                        ($options["non-zero-only"] &&
                            $itemBalanceChange < 0.005) ||
                        ($options["item-types"] &&
                            !in_array(
                                $item->item_type->value,
                                $options["item-types"]
                            ))
                    ) {
                        continue;
                    }

                    $fieldValues = [
                        "invoice.id" => $invoice->id,
                        "invoice.date" => $invoice->created_at,
                        "item.id" => $item->id,
                        "item.item_type" => $item->item_type->value,
                        "balance_change" => self::formatCurrency(
                            $itemBalanceChange
                        ),
                        "item.description" => $invoice->period,
                    ];

                    if ($options["format"] === "csv") {
                        fputcsv($outputStream, $fieldValues);
                    }
                }
            } else {
                if (
                    $options["non-zero-only"] &&
                    $invoiceBalanceChange < 0.005
                ) {
                    continue;
                }

                $fieldValues = [
                    "invoice.id" => $invoice->id,
                    "invoice.date" => $invoice->created_at,
                    "balance_change" => self::formatCurrency(
                        $invoiceBalanceChange
                    ),
                    "invoice.title" => $invoice->period,
                ];

                if ($options["format"] === "csv") {
                    fputcsv($outputStream, $fieldValues);
                }
            }

            $totalBalanceChange += $invoiceBalanceChange;
        }

        if ($options["user"] > 0) {
            if ($options["format"] === "csv") {
                if ($options["show-items"]) {
                    fputcsv($outputStream, [
                        "",
                        "",
                        "",
                        "Total",
                        self::formatCurrency($totalBalanceChange),
                    ]);

                    fputcsv($outputStream, [
                        "",
                        "",
                        "",
                        "Solde utilisateur",
                        self::formatCurrency($user->balance),
                    ]);
                } else {
                    fputcsv($outputStream, [
                        "",
                        "Total",
                        self::formatCurrency($totalBalanceChange),
                    ]);

                    fputcsv($outputStream, [
                        "",
                        "Solde utilisateur",
                        self::formatCurrency($user->balance),
                    ]);
                }
            }
        }
    }

    protected static function formatCurrency($amount)
    {
        return str_replace(".", ",", (string) round($amount, 2));
    }
}
