<?php

namespace App\Console\Commands;

use App\Helpers\Path;
use App\Models\File;
use App\Models\Image;
use DB;
use Ds\Set;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use League\Flysystem\StorageAttributes;
use Storage;

class FilesCleanStorage extends Command
{
    protected $signature = "files:clean:storage
                            {--delete-missing : delete DB entries when missing related file}
                            {--pretend : Do not actually delete anything}";

    protected $description = "Delete stored files not referenced in any db entry";

    private bool $pretend = false;
    private bool $verbose;
    private bool $deleteMissing;

    public function handle(): void
    {
        $this->pretend = $this->option("pretend");
        $this->deleteMissing = (bool) $this->option("delete-missing");
        $this->verbose = (bool) $this->option("verbose");

        $pretendString = $this->pretend ? "true" : "false";

        \Log::info(
            "[FilesCleanStorage] Start cleaning filesystem. Pretend: $pretendString"
        );

        $this->cleanFiles();
        $this->cleanImages();
    }

    public function cleanFiles()
    {
        $dbFiles = new Set(
            DB::table("files")
                ->select("path", "filename")
                ->get()
                ->map(
                    fn($f) => Path::asAbsolute(
                        Path::join($f->path, $f->filename)
                    )
                )
                ->values()
        );
        $storageFiles = new Set(
            array_map(
                Path::asAbsolute(...),
                // Use ::getDriver rather than ::files() to avoid costly sorting
                Storage::getDriver()
                    ->listContents("files", true)
                    ->filter(fn(StorageAttributes $d) => $d->isFile())
                    ->map(fn(StorageAttributes $f) => $f->path())
                    ->toArray()
            )
        );

        $missingFiles = $dbFiles->diff($storageFiles);
        $this->processMissingItems(
            $missingFiles,
            File::query(),
            "file",
            DB::raw("concat(path, '/', filename)")
        );

        $unreferencedFiles = $storageFiles->diff($dbFiles);
        $this->processUnreferencedItems(
            $unreferencedFiles,
            Storage::delete(...),
            "file"
        );
    }

    public function cleanImages()
    {
        $dbImageDirs = new Set(
            DB::table("images")
                ->select("path")
                ->get()
                ->map(fn($f) => Path::asAbsolute($f->path))
                ->values()
        );
        $storageImageDirs = new Set(
            // We use listContents here rather than Storage::allDirectories(),
            // since that command doesn't actually return all directories with minio
            Storage::getDriver()
                ->listContents("images", true)
                ->filter(fn(StorageAttributes $d) => $d->isFile())
                ->map(
                    fn(StorageAttributes $f) => Path::asAbsolute(
                        dirname($f->path())
                    )
                )
                ->toArray()
        );

        $missingImages = $dbImageDirs->diff($storageImageDirs);
        $this->processMissingItems(
            $missingImages,
            Image::query(),
            "image directory",
            "path"
        );

        $unreferencedImages = $storageImageDirs->diff($dbImageDirs);
        $this->processUnreferencedItems(
            $unreferencedImages,
            Storage::deleteDirectory(...),
            "image directory"
        );
    }

    private function processMissingItems(
        Set $missingFiles,
        Builder $query,
        string $itemType,
        $column
    ): void {
        if ($missingFiles->count() <= 0) {
            \Log::info("[FilesCleanStorage] No missing $itemType.");
            return;
        }

        \Log::info(
            "[FilesCleanStorage] Found {$missingFiles->count()} missing $itemType. e.g. {$missingFiles->get(
                0
            )}"
        );

        if (!$this->deleteMissing) {
            return;
        }

        $missingFilePaths = $missingFiles->toArray();
        array_push(
            $missingFilePaths,
            ...array_map(fn($path) => ltrim($path, "/"), $missingFilePaths)
        );

        $query = $query->whereIn($column, $missingFilePaths);

        if ($this->verbose) {
            $ids = implode(", ", (clone $query)->pluck("id")->toArray());
            \Log::info("[FilesCleanStorage] Deleting DB $itemType: $ids");
        }

        if (!$this->pretend) {
            foreach ($query->cursor() as $file) {
                $file->delete();
            }
        }
    }

    private function processUnreferencedItems(
        Set $unreferencedItems,
        \Closure $deleteAction,
        $itemType
    ) {
        if ($unreferencedItems->count() <= 0) {
            \Log::info("[FilesCleanStorage] No unused $itemType.");
            return;
        }

        \Log::info(
            "[FilesCleanStorage] Deleting {$unreferencedItems->count()} unused $itemType."
        );

        if ($this->verbose) {
            $itemString = implode(", ", $unreferencedItems->toArray());
            \Log::info("[FilesCleanStorage] Deleting: $itemString");
        }
        if (!$this->pretend) {
            foreach ($unreferencedItems as $item) {
                $deleteAction($item);
            }
        }
    }
}
