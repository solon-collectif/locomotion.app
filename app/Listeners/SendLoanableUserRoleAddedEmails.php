<?php

namespace App\Listeners;

use App\Enums\LoanableUserRoles;
use App\Events\LoanableUserRoleAdded;
use App\Mail\LibraryUserRoleAddedMail;
use App\Mail\LoanableUserRoleAddedMail;
use App\Mail\TrustedBorrowerAddedMail;
use App\Mail\UserMail;
use App\Models\Loanable;

class SendLoanableUserRoleAddedEmails
{
    public function handle(LoanableUserRoleAdded $event): void
    {
        if (
            in_array($event->loanableUserRole->role, [
                LoanableUserRoles::Coowner,
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ])
        ) {
            if ($event->ressource instanceof Loanable) {
                UserMail::queue(
                    new LoanableUserRoleAddedMail(
                        $event->adder,
                        $event->loanableUserRole->user,
                        $event->ressource
                    ),
                    $event->loanableUserRole->user
                );
            } else {
                UserMail::queue(
                    new LibraryUserRoleAddedMail(
                        $event->adder,
                        $event->loanableUserRole->user,
                        $event->ressource
                    ),
                    $event->loanableUserRole->user
                );
            }

            return;
        }
        if (
            $event->loanableUserRole->role ===
                LoanableUserRoles::TrustedBorrower &&
            $event->ressource instanceof Loanable
        ) {
            UserMail::queue(
                new TrustedBorrowerAddedMail(
                    $event->adder,
                    $event->loanableUserRole->user,
                    $event->ressource
                ),
                $event->loanableUserRole->user
            );
        }
    }
}
