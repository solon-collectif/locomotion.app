<?php

namespace App\Listeners;

use App\Enums\IncidentNotificationLevel;
use App\Events\IncidentReopenedEvent;
use App\Mail\Incidents\IncidentReopened;
use App\Mail\UserMail;

class SendIncidentReopenedEmails
{
    public function __construct()
    {
    }

    public function handle(IncidentReopenedEvent $event): void
    {
        foreach ($event->incident->notifiedUsers as $user) {
            if ($user->pivot->level === IncidentNotificationLevel::None) {
                continue;
            }

            // Do not notify reopener
            if ($user->id === $event->reopener->id) {
                continue;
            }

            UserMail::queue(
                new IncidentReopened($event->incident, $user, $event->reopener),
                $user
            );
        }
    }
}
