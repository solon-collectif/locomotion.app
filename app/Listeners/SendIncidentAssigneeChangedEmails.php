<?php

namespace App\Listeners;

use App\Events\IncidentAssigneeChangedEvent;
use App\Mail\Incidents\IncidentAssigneeChanged;
use App\Mail\UserMail;

class SendIncidentAssigneeChangedEmails
{
    public function __construct()
    {
    }

    public function handle(IncidentAssigneeChangedEvent $event): void
    {
        if ($event->assignedBy->id === $event->incident->assignee_id) {
            return;
        }

        UserMail::queue(
            new IncidentAssigneeChanged(
                $event->incident,
                $event->incident->assignee,
                $event->assignedBy
            ),
            $event->incident->assignee
        );
    }
}
