<?php

namespace App\Listeners;

use App\Enums\IncidentNotificationLevel;
use App\Events\IncidentResolvedEvent;
use App\Mail\Incidents\IncidentResolved;
use App\Mail\UserMail;

class SendIncidentResolvedEmails
{
    /*
       Send incident-resolved notification to owner and borrower.
       Only send one copy if owner is borrower.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(IncidentResolvedEvent $event)
    {
        foreach ($event->incident->notifiedUsers as $user) {
            if ($user->pivot->level === IncidentNotificationLevel::None) {
                continue;
            }

            // Do not notify resolver
            if ($user->id === $event->incident->resolved_by_user_id) {
                continue;
            }

            UserMail::queue(
                new IncidentResolved($event->incident, $user),
                $user
            );
        }
    }
}
