<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanAcceptedEvent;
use App\Mail\Loan\LoanAcceptedMail;
use App\Mail\Loan\LoanConfirmedMail;
use App\Mail\UserMail;

class SendLoanAcceptedEmails
{
    public function handle(LoanAcceptedEvent $event): void
    {
        $loan = $event->loan;

        if ($loan->status === LoanStatus::Accepted) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanAcceptedMail(
                    $loan,
                    $user,
                    $event->accepter,
                    $event->comment
                ),
                $loan,
                $event->accepter //Do not send email to accepter, they know what they did
            );
            return;
        }

        if ($loan->status === LoanStatus::Confirmed) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanConfirmedMail(
                    $user,
                    $loan,
                    $event->accepter,
                    $event->comment
                ),
                $loan
                // Do send an email to the accepter: they'll want to have the calendar invite
            );
        }
    }
}
