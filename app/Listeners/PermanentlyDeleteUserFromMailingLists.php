<?php

namespace App\Listeners;

use App\Events\PermanentlyDeletingUserDataEvent;
use App\Jobs\MailingLists\PermanentlyDeleteUserFromMailingList;

class PermanentlyDeleteUserFromMailingLists
{
    public function handle(PermanentlyDeletingUserDataEvent $event)
    {
        foreach (
            $event->user
                ->communities()
                ->withoutGlobalScope("active-user")
                ->get()
            as $community
        ) {
            foreach ($community->mailingLists as $mailingList) {
                \Queue::pushOn(
                    "low",
                    new PermanentlyDeleteUserFromMailingList(
                        $mailingList,
                        $event->user->email
                    )
                );
            }
        }
    }
}
