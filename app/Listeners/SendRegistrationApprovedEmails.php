<?php

namespace App\Listeners;

use App\Events\CommunityUserApprovedEvent;
use App\Mail\Registration\Approved as RegistrationApproved;
use App\Mail\Registration\ApprovedCustom as RegistrationApprovedCustom;
use App\Mail\UserMail;

class SendRegistrationApprovedEmails
{
    public function handle(CommunityUserApprovedEvent $event)
    {
        $communityUser = $event->communityUser;
        $user = $communityUser->user;

        if (
            !empty(
                $communityUser->community->custom_registration_approved_email
            )
        ) {
            if (
                isset($communityUser->meta["sent_registration_approved_email"])
            ) {
                return;
            }

            UserMail::queue(
                new RegistrationApprovedCustom(
                    $communityUser->community->custom_registration_approved_email
                ),
                $user
            );

            // Save Meta
            $meta = $communityUser->meta;
            $meta["sent_registration_approved_email"] = true;
            $communityUser->meta = $meta;
            $communityUser->save();

            return;
        }

        if (!isset($user->meta["sent_registration_approved_email"])) {
            UserMail::queue(new RegistrationApproved($communityUser), $user);

            // Save Meta
            $meta = $user->meta;
            $meta["sent_registration_approved_email"] = true;
            $user->meta = $meta;
            $user->save();
        }
    }
}
