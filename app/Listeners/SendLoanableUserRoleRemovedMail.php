<?php

namespace App\Listeners;

use App\Enums\LoanableUserRoles;
use App\Events\LoanableUserRoleRemoved;
use App\Mail\LibraryUserRoleRemovedMail;
use App\Mail\LoanableUserRoleRemovedMail;
use App\Mail\TrustedBorrowerRemovedMail;
use App\Mail\UserMail;
use App\Models\Loanable;

class SendLoanableUserRoleRemovedMail
{
    public function handle(LoanableUserRoleRemoved $event)
    {
        if ($event->loanableUserRole->user->is($event->remover)) {
            return;
        }
        if (
            in_array($event->loanableUserRole->role, [
                LoanableUserRoles::Coowner,
                LoanableUserRoles::Owner,
                LoanableUserRoles::Manager,
            ])
        ) {
            if ($event->loanableUserRole->ressource instanceof Loanable) {
                UserMail::queue(
                    new LoanableUserRoleRemovedMail(
                        $event->loanableUserRole->user,
                        $event->loanableUserRole->ressource,
                        $event->remover
                    ),
                    $event->loanableUserRole->user
                );
            } else {
                UserMail::queue(
                    new LibraryUserRoleRemovedMail(
                        $event->loanableUserRole->user,
                        $event->loanableUserRole->ressource,
                        $event->remover
                    ),
                    $event->loanableUserRole->user
                );
            }
            return;
        }
        if (
            $event->loanableUserRole->role ===
                LoanableUserRoles::TrustedBorrower &&
            $event->loanableUserRole->ressource instanceof Loanable
        ) {
            UserMail::queue(
                new TrustedBorrowerRemovedMail(
                    $event->loanableUserRole->user,
                    $event->loanableUserRole->ressource,
                    $event->remover
                ),
                $event->loanableUserRole->user
            );
        }
    }
}
