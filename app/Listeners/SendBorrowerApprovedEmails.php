<?php

namespace App\Listeners;

use App\Events\BorrowerApprovedEvent;
use App\Mail\Borrower\Approved as BorrowerApproved;
use App\Mail\Borrower\ApprovedToAdmin as BorrowerApprovedToAdmin;
use App\Mail\UserMail;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

/*
  This listener will:
    - Send a confirmation email to the user
      - As borrower approved if registration is approved
      - As borrower pending if registration is not yet approved
    - Send an email to all admins where user can loan cars
*/

class SendBorrowerApprovedEmails
{
    public function handle(BorrowerApprovedEvent $event)
    {
        $user = $event->user;

        if (!isset($user->meta["sent_borrower_approved_email"])) {
            if ($user->approvedCommunities->count() > 0) {
                // Registration is approved, borrower approved
                UserMail::queue(new BorrowerApproved($user), $user);
            }

            // Send emails to admins in community
            $communityAdmins = User::whereHas(
                "approvedCommunities",
                fn(Builder $q) => $q
                    ->where("role", "admin")
                    ->whereHas(
                        "approvedUsers",
                        fn(Builder $users) => $users->where(
                            "user_id",
                            "=",
                            $user->id
                        )
                    )
            )->get();

            foreach ($communityAdmins as $communityAdmin) {
                UserMail::queue(
                    new BorrowerApprovedToAdmin($user),
                    $communityAdmin
                );
            }

            // Mark the user email approved as borrower
            $meta = $user->meta;
            $meta["sent_borrower_approved_email"] = true;
            $user->meta = $meta;

            $user->save();
        }
    }
}
