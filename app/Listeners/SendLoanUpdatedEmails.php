<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanUpdatedEvent;
use App\Mail\Loan\LoanUpdatedMail;
use App\Mail\UserMail;

class SendLoanUpdatedEmails
{
    public function handle(LoanUpdatedEvent $event): void
    {
        $loan = $event->loan;

        $hasUpdatedDuration =
            $event->initialLoan["duration_in_minutes"] !==
            $loan->duration_in_minutes;
        $hasUpdatedDeparture = !$loan->departure_at->isSameMinute(
            $event->initialLoan["departure_at"]
        );

        // Only notify users when this results in a change in calendar event (start, duration)
        // Or there's an action required (status = Requested, Ended)
        // This way, we avoid notifying participants when loan is ongoing and mileage/expenses are
        // changed: participants will be notified anyway when loan is ended and validation is required.
        // See LoanEndedEvent / SendLoanEndedEmails
        if (
            $hasUpdatedDeparture ||
            $hasUpdatedDuration ||
            $loan->status === LoanStatus::Requested ||
            $loan->status === LoanStatus::Ended
        ) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanUpdatedMail(
                    $event->updater,
                    $user,
                    $loan,
                    $event->approvalReset,
                    $event->initialLoan
                ),
                $loan
            );
        }
    }
}
