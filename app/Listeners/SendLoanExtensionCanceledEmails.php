<?php

namespace App\Listeners;

use App\Events\LoanExtensionCanceledEvent;
use App\Mail\Loan\LoanExtensionCanceledMail;
use App\Mail\UserMail;

class SendLoanExtensionCanceledEmails
{
    public function handle(LoanExtensionCanceledEvent $event): void
    {
        UserMail::queueToLoanOwners(
            new LoanExtensionCanceledMail($event->loan),
            $event->loan
        );
    }
}
