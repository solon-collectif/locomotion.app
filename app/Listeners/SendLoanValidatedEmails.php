<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanValidatedEvent;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\Loan\LoanValidatedMail;
use App\Mail\UserMail;

class SendLoanValidatedEmails
{
    public function handle(LoanValidatedEvent $event): void
    {
        if ($event->loan->status === LoanStatus::Validated) {
            if ($event->validator?->id === $event->loan->borrower_user_id) {
                // Do not notify borrower if they performed the action.
                return;
            }
            UserMail::queueToLoanBorrower(
                new LoanValidatedMail($event->loan, $event->validator),
                $event->loan
            );

            return;
        }

        if ($event->loan->status === LoanStatus::Completed) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanCompletedMail($user, $event->loan),
                $event->loan
            );
        }
    }
}
