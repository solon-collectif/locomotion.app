<?php

namespace App\Listeners;

use App\Events\Loan\CanceledEvent;
use App\Mail\Loan\LoanCanceledMail;
use App\Mail\UserMail;

class SendLoanCanceledEmails
{
    public function handle(CanceledEvent $event): void
    {
        UserMail::queueToLoanNotifiedUsers(
            fn($user) => new LoanCanceledMail(
                $event->canceler,
                $user,
                $event->loan
            ),
            $event->loan
        );
    }
}
