<?php

namespace App\Listeners;

use App\Events\LoanExtensionAcceptedEvent;
use App\Mail\Loan\LoanExtensionAcceptedMail;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanExtensionAcceptedEmails
{
    public function handle(LoanExtensionAcceptedEvent $event): void
    {
        UserMail::queueToLoanNotifiedUsers(
            fn(User $user) => new LoanExtensionAcceptedMail(
                $event->loan,
                $user,
                $event->accepter,
                $event->previousDuration
            ),
            $event->loan
            // we still send an email to the $event->accepter, so they get an updated
            // calendar invitation.
        );
    }
}
