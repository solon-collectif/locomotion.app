<?php

namespace App\Listeners;

use App\Enums\IncidentNotificationLevel;
use App\Events\IncidentCreatedEvent;
use App\Mail\Incidents\IncidentCreated;
use App\Mail\UserMail;

class SendIncidentCreatedEmails
{
    public function handle(IncidentCreatedEvent $event)
    {
        foreach ($event->incident->notifiedUsers as $user) {
            if ($user->pivot->level !== IncidentNotificationLevel::All) {
                continue;
            }

            // Do not notify reporter
            if ($user->id === $event->incident->reported_by_user_id) {
                continue;
            }

            UserMail::queue(
                new IncidentCreated($event->incident, $user),
                $user
            );
        }
    }
}
