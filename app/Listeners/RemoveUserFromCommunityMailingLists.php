<?php

namespace App\Listeners;

use App\Events\CommunityUserUnapprovedEvent;
use App\Jobs\MailingLists\RemoveUserFromMailingList;
use App\Models\MailingListIntegration;

class RemoveUserFromCommunityMailingLists
{
    public function handle(CommunityUserUnapprovedEvent $event): void
    {
        /** @var MailingListIntegration $mailingList */
        foreach (
            $event->communityUser->community->mailingLists
            as $mailingList
        ) {
            \Queue::pushOn(
                "low",
                new RemoveUserFromMailingList(
                    $mailingList,
                    $event->communityUser->user
                )
            );
        }
    }
}
