<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanPaidEvent;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\UserMail;

class SendLoanPaidEmails
{
    public function handle(LoanPaidEvent $event): void
    {
        $loan = $event->loan;

        if ($loan->status !== LoanStatus::Completed) {
            return;
        }

        UserMail::queueToLoanNotifiedUsers(
            fn($user) => new LoanCompletedMail($user, $event->loan),
            $event->loan
        );
    }
}
