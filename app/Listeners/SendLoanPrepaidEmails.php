<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanPrepaidEvent;
use App\Mail\Loan\LoanConfirmedMail;
use App\Mail\UserMail;

class SendLoanPrepaidEmails
{
    public function handle(LoanPrepaidEvent $event): void
    {
        if ($event->loan->status !== LoanStatus::Confirmed) {
            return;
        }
        UserMail::queueToLoanNotifiedUsers(
            fn($user) => new LoanConfirmedMail($user, $event->loan),
            $event->loan
        );
    }
}
