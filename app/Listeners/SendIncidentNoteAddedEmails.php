<?php

namespace App\Listeners;

use App\Enums\IncidentNotificationLevel;
use App\Events\IncidentNoteAddedEvent;
use App\Mail\Incidents\NoteAdded;
use App\Mail\UserMail;

class SendIncidentNoteAddedEmails
{
    public function __construct()
    {
    }

    public function handle(IncidentNoteAddedEvent $event): void
    {
        $usersToNotify = $event->incidentNote->incident->notifiedUsers;

        foreach ($usersToNotify as $user) {
            if ($user->pivot->level !== IncidentNotificationLevel::All) {
                continue;
            }

            // Do not notify author
            if ($user->id === $event->incidentNote->author_id) {
                continue;
            }

            UserMail::queue(new NoteAdded($event->incidentNote, $user), $user);
        }
    }
}
