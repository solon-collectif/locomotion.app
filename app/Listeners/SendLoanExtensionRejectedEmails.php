<?php

namespace App\Listeners;

use App\Events\LoanExtensionRejectedEvent;
use App\Mail\Loan\LoanExtensionRejectedMail;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanExtensionRejectedEmails
{
    public function handle(LoanExtensionRejectedEvent $event)
    {
        UserMail::queueToLoanNotifiedUsers(
            fn(User $user) => new LoanExtensionRejectedMail(
                $event->loan,
                $user,
                $event->refuser
            ),
            $event->loan,
            $event->refuser // We do not send an email to the refuser, since they know what they did
        );
    }
}
