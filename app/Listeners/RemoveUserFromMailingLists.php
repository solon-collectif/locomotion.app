<?php

namespace App\Listeners;

use App\Events\UserDeletedEvent;
use App\Jobs\MailingLists\RemoveUserFromMailingList;
use App\Models\MailingListIntegration;

class RemoveUserFromMailingLists
{
    public function handle(UserDeletedEvent $event)
    {
        // Deleting user suspends them from communities, so we check on all communities where user
        // was added, no matter if they were approved or not
        foreach (
            $event->user
                ->communities()
                ->withoutGlobalScope("active-user")
                ->get()
            as $community
        ) {
            /** @var MailingListIntegration $mailingList */
            foreach ($community->mailingLists as $mailingList) {
                \Queue::pushOn(
                    "low",
                    new RemoveUserFromMailingList($mailingList, $event->user)
                );
            }
        }
    }
}
