<?php

namespace App\Listeners;

class EnsureLoanableTypesExist
{
    // We want these loanable type details to exist whenever migrations are run, namely for
    // tests, even if seeders are not executed.
    public function handle(): void
    {
        \Log::info("Inserting default loanable types");
        \DB::table("loanable_type_details")->insertOrIgnore([
            ["id" => 1, "name" => "car"],
            ["id" => 2, "name" => "trailer"],
            ["id" => 3, "name" => "bike"],
        ]);
    }
}
