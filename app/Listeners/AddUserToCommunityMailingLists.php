<?php

namespace App\Listeners;

use App\Events\CommunityUserApprovedEvent;
use App\Jobs\MailingLists\AddOrUpdateUserInMailingList;
use App\Models\MailingListIntegration;

class AddUserToCommunityMailingLists
{
    public function handle(CommunityUserApprovedEvent $event): void
    {
        /** @var MailingListIntegration $mailingList */
        foreach (
            $event->communityUser->community->mailingLists
            as $mailingList
        ) {
            \Queue::pushOn(
                "low",
                new AddOrUpdateUserInMailingList(
                    $mailingList,
                    $event->communityUser
                )
            );
        }
    }
}
