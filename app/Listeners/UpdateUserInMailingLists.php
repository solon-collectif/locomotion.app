<?php

namespace App\Listeners;

use App\Events\UserEmailUpdatedEvent;
use App\Events\UserNameUpdatedEvent;
use App\Jobs\MailingLists\AddOrUpdateUserInMailingList;
use App\Jobs\MailingLists\UpdateUserEmailInMailingList;
use App\Models\MailingListIntegration;

class UpdateUserInMailingLists
{
    public function handle(UserEmailUpdatedEvent|UserNameUpdatedEvent $event)
    {
        $user = $event->user;
        foreach ($user->approvedCommunities as $community) {
            /** @var MailingListIntegration $mailingList */
            foreach ($community->mailingLists as $mailingList) {
                if ($event instanceof UserEmailUpdatedEvent) {
                    \Queue::pushOn(
                        "low",
                        new UpdateUserEmailInMailingList(
                            $mailingList,
                            $community->pivot,
                            $event->previousEmail
                        )
                    );
                } else {
                    \Queue::pushOn(
                        "low",
                        new AddOrUpdateUserInMailingList(
                            $mailingList,
                            $community->pivot
                        )
                    );
                }
            }
        }
    }
}
