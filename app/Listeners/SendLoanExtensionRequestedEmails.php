<?php

namespace App\Listeners;

use App\Events\LoanExtensionRequestedEvent;
use App\Mail\Loan\LoanExtensionRequestedMail;
use App\Mail\UserMail;

class SendLoanExtensionRequestedEmails
{
    public function handle(LoanExtensionRequestedEvent $event)
    {
        $loan = $event->loan;

        UserMail::queueToLoanOwners(
            new LoanExtensionRequestedMail($loan, $loan->borrowerUser),
            $loan
        );
    }
}
