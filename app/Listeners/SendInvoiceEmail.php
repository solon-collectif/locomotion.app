<?php

namespace App\Listeners;

use App\Mail\InvoicePaidMail;
use App\Mail\UserMail;

class SendInvoiceEmail
{
    public function handle($event)
    {
        UserMail::queue(
            new InvoicePaidMail(
                $event->user,
                $event->invoice,
                $event->title,
                $event->text
            ),
            $event->user
        );
    }
}
