<?php

namespace App\Listeners;

use App\Events\RegistrationSubmittedEvent;

class ResetProofEvaluation
{
    public function __construct()
    {
    }

    public function handle(RegistrationSubmittedEvent $event): void
    {
        $event->communityUser->resetProofEvaluation();
        $event->communityUser->save();
    }
}
