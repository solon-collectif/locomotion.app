<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanEndedEvent;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\Loan\LoanEndedMail;
use App\Mail\Loan\LoanValidatedMail;
use App\Mail\UserMail;

class SendLoanEndedEmails
{
    public function handle(LoanEndedEvent $event): void
    {
        $loan = $event->loan;

        if ($loan->status === LoanStatus::Ended) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanEndedMail($loan, $user),
                $loan,
                $event->ender
            );
            return;
        }

        if (
            (!$event->ender || $event->ender->id !== $loan->borrower_user_id) &&
            $loan->status === LoanStatus::Validated &&
            !$loan->isInGracePeriod()
        ) {
            UserMail::queueToLoanBorrower(new LoanValidatedMail($loan), $loan);
            return;
        }

        if ($loan->status === LoanStatus::Completed) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanCompletedMail($user, $loan),
                $loan
            );
        }
    }
}
