<?php

namespace App\Listeners;

use App\Events\LoanablePublishedEvent;
use App\Mail\LoanablePublished;
use App\Mail\LoanableReviewable;
use App\Mail\UserMail;

class SendLoanablePublishedEmails
{
    public function handle(LoanablePublishedEvent $event)
    {
        UserMail::queue(
            new LoanablePublished($event->user, $event->loanable),
            $event->user
        );
        foreach ($event->user->communities as $community) {
            foreach ($community->admins() as $admin) {
                UserMail::queue(
                    new LoanableReviewable(
                        $event->user,
                        $community,
                        $event->loanable
                    ),
                    $admin
                );
            }
        }
    }
}
