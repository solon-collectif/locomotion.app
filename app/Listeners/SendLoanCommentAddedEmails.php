<?php

namespace App\Listeners;

use App\Enums\LoanNotificationLevel;
use App\Events\LoanCommentAddedEvent;
use App\Mail\Loan\LoanCommentAddedMail;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanCommentAddedEmails
{
    public function handle(LoanCommentAddedEvent $event): void
    {
        UserMail::queueToLoanNotifiedUsers(
            fn(User $user) => new LoanCommentAddedMail($event->comment, $user),
            $event->comment->loan,
            exception: $event->comment->author, // do not notify author
            acceptedLevels: [
                LoanNotificationLevel::All,
                LoanNotificationLevel::MessagesOnly,
            ]
        );
    }
}
