<?php

namespace App\Listeners;

use App\Events\LoanRejectedEvent;
use App\Mail\Loan\LoanRejectedMail;
use App\Mail\UserMail;

class SendLoanRejectedEmails
{
    public function handle(LoanRejectedEvent $event): void
    {
        if ($event->rejecter->id !== $event->loan->borrower_user_id) {
            UserMail::queueToLoanBorrower(
                new LoanRejectedMail(
                    $event->loan,
                    $event->rejecter,
                    $event->comment
                ),
                $event->loan
            );
        }
    }
}
