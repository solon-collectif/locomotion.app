<?php

namespace App\Listeners;

use App\Events\CommunityUserUnapprovedEvent;

class RemoveUserLoanablesFromRemovedCommunity
{
    public function __construct()
    {
    }

    public function handle(CommunityUserUnapprovedEvent $event): void
    {
        $user = $event->communityUser->user;
        $community = $event->communityUser->community;

        \DB::table("community_library")
            ->join(
                "libraries",
                "libraries.id",
                "=",
                "community_library.library_id"
            )
            ->join(
                "loanable_user_roles",
                "loanable_user_roles.ressource_id",
                "=",
                "libraries.id"
            )
            ->where("loanable_user_roles.ressource_type", "library")
            ->where("community_library.community_id", $community->id)
            ->where("loanable_user_roles.user_id", $user->id)
            ->where("loanable_user_roles.role", "owner")
            ->delete();
    }
}
