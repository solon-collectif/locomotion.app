<?php

namespace App\Listeners;

use App\Events\UserClaimedBalanceEvent;
use App\Mail\UserClaimedBalance;
use App\Mail\UserMail;
use App\Models\User;

class SendUserClaimedBalanceEmails
{
    public function handle(UserClaimedBalanceEvent $event)
    {
        $admins = User::query()
            ->globalAdmins()
            ->get();

        foreach ($admins as $admin) {
            UserMail::queue(new UserClaimedBalance($event->payout), $admin);
        }
    }
}
