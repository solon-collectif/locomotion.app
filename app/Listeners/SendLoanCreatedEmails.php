<?php

namespace App\Listeners;

use App\Enums\LoanStatus;
use App\Events\LoanCreatedEvent;
use App\Mail\Loan\LoanAcceptedMail;
use App\Mail\Loan\LoanConfirmedMail;
use App\Mail\Loan\LoanRequestedMail;
use App\Mail\UserMail;

class SendLoanCreatedEmails
{
    public function handle(LoanCreatedEvent $event): void
    {
        $loan = $event->loan;

        if ($loan->status === LoanStatus::Requested) {
            UserMail::queueToLoanOwners(
                (new LoanRequestedMail(
                    $loan,
                    $event->messageForOwner
                ))->replyTo(
                    $loan->borrowerUser->email,
                    $loan->borrowerUser->full_name
                ),
                $loan
            );
            return;
        }

        if ($loan->status === LoanStatus::Confirmed) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanConfirmedMail($user, $loan),
                $loan
            );
            return;
        }

        if ($loan->status === LoanStatus::Accepted) {
            UserMail::queueToLoanNotifiedUsers(
                fn($user) => new LoanAcceptedMail($loan, $user),
                $loan,
                exception: $event->creator
            );
        }
    }
}
