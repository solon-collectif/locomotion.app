<?php

namespace App\Pricings;

use App\Calendar\CalendarHelper;
use App\Calendar\Interval;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;

/**
 *  Pricing applied to a loan over a given time interval. This class evaluates the pricing over
 *  the given interval and produces BillItems.
 */
abstract class InvoicePricingItems extends InvoiceItems
{
    public function __construct(
        protected readonly Pricing $pricing,
        Loan $loan,
        Interval $interval = null
    ) {
        parent::__construct($loan, $interval);
    }

    public function getLoanable(): Loanable
    {
        return $this->loan->loanable;
    }

    public function getDistance()
    {
        return $this->loan->actual_distance;
    }

    public function getDurationInMinutes()
    {
        // Ensure duration is always positive.
        return max(
            (int) $this->interval->start->diffInMinutes($this->interval->end),
            0
        );
    }

    public function getCalendarDays(): int
    {
        return CalendarHelper::getCalendarDays(
            $this->interval->start,
            $this->interval->end,
            config("app.default_user_timezone")
        );
    }

    public function getLoanableAgeForInsurance()
    {
        return Loan::getLoanableAgeForInsurance(
            $this->getLoanable(),
            $this->interval->start
        );
    }

    private int|null|float $cost = null;
    public function getCost(): float|int|null
    {
        if (is_null($this->cost)) {
            $this->cost = $this->pricing->evaluateRule(
                $this->getDistance(),
                $this->getDurationInMinutes(),
                $this->getLoanable(),
                [
                    "days" => $this->getCalendarDays(),
                    "loanable_age_for_insurance" => $this->getLoanableAgeForInsurance(),
                    "start" => $this->interval->start,
                    "end" => $this->interval->end,
                ]
            );
        }
        return $this->cost;
    }

    public static function forPricing(
        Pricing $pricing,
        Loan $loan,
        Interval $interval = null
    ): InvoicePricingItems {
        if ($pricing->pricing_type === "price") {
            return new InvoiceCompensation($pricing, $loan, $interval);
        }
        if ($pricing->pricing_type === "insurance") {
            return new InvoiceInsurance($pricing, $loan, $interval);
        }

        throw new \Exception("Pricing type not supported");
    }
}
