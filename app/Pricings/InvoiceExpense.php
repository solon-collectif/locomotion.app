<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Models\BillItem;

class InvoiceExpense extends InvoiceItems
{
    public function getBorrowerBillItems($meta = []): array
    {
        if ($this->loan->actual_expenses <= 0) {
            return [];
        }

        $borrowerInvoiceItemData = [
            "item_type" => BillItemTypes::loanExpenses,
            "label" => "Dépenses",
            "amount" => $this->loan->actual_expenses,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "meta" => $meta,
        ];

        return [new BillItem($borrowerInvoiceItemData)];
    }

    public function getOwnerBillItems($meta = []): array
    {
        if ($this->loan->actual_expenses <= 0) {
            return [];
        }

        $borrowerInvoiceItemData = [
            "item_type" => BillItemTypes::loanExpenses,
            "label" => "Dépenses",
            "amount" => -$this->loan->actual_expenses,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "meta" => $meta,
        ];

        return [new BillItem($borrowerInvoiceItemData)];
    }
}
