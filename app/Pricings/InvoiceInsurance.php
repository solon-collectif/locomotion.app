<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Car;

class InvoiceInsurance extends InvoicePricingItems
{
    public function getBorrowerBillItems($meta = []): array
    {
        // Add pricing data to meta information.
        $meta["pricing_id"] = $this->pricing->id;
        $meta["pricing_name"] = $this->pricing->name;

        $days = $this->getCalendarDays();
        $start = $this->interval->start->format("Y-m-d");
        $end = $this->interval->end->subMinute()->format("Y-m-d");

        if ($days === 1) {
            $labelDates = "le $start";
        } else {
            $labelDates = "$days jours, du $start au $end";
        }

        $loanableMeta = [];
        $loanableDetails = $this->getLoanable()->details;
        if ($loanableDetails::class === Car::class) {
            $loanableMeta = [
                "loanableValueCategory" => $loanableDetails->value_category,
                "loanablePricingCategory" => $loanableDetails->pricing_category,
                "loanableAgeForInsurance" => $this->getLoanableAgeForInsurance(),
            ];
        }

        $borrowerInvoiceItemData = [
            "label" => "Coût de l'assurance ($labelDates)",
            "amount" => -$this->getCost(),
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "item_type" => BillItemTypes::loanInsurance,
            "meta" => [
                ...$meta,
                "calendarDays" => $this->getCalendarDays(),
                ...$loanableMeta,
            ],
        ];

        return [new BillItem($borrowerInvoiceItemData)];
    }
}
