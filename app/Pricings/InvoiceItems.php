<?php

namespace App\Pricings;

use App\Calendar\Interval;
use App\Models\Loan;

/**
 * Descendants of this class represent types of invoice items which may apply to borrower, owners
 * or both in the context of a loan.
 *
 * TODO: Decide whether InvoiceItems should map to a single or multiple BillItems
 */
abstract class InvoiceItems
{
    protected readonly Interval $interval;
    public function __construct(
        protected readonly Loan $loan,
        Interval $interval = null
    ) {
        if (!$interval) {
            $interval = new Interval(
                $this->loan->departure_at,
                $this->loan->actual_return_at
            );
        }

        // Ensure interval is in the appropriate timezone for calendar days and loanable age
        $this->interval = $interval->atTimezone($loan->loanable->timezone);
    }

    public function getBorrowerBillItems($meta = []): array
    {
        return [];
    }

    public function getOwnerBillItems($meta = []): array
    {
        return [];
    }
}
