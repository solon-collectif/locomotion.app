<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Enums\Requirement;

class BorrowerApplicableAmountTypes implements \JsonSerializable
{
    public function __construct(
        public readonly Requirement $price = Requirement::required,
        public readonly Requirement $insurance = Requirement::required,
        public readonly Requirement $expenses = Requirement::required,
        public readonly Requirement $contributions = Requirement::optional
    ) {
    }

    public function jsonSerialize(): mixed
    {
        return [
            "price" => $this->price,
            "insurance" => $this->insurance,
            "expenses" => $this->expenses,
            "contributions" => $this->contributions,
        ];
    }

    public function isBillItemTypeRequired(BillItemTypes $type): bool
    {
        return match ($type) {
            BillItemTypes::loanPrice => $this->price === Requirement::required,
            BillItemTypes::loanExpenses => $this->expenses ===
                Requirement::required,
            BillItemTypes::loanInsurance => $this->insurance ===
                Requirement::required,
            BillItemTypes::loanContribution => $this->contributions ===
                Requirement::required,
            default => false,
        };
    }
}
