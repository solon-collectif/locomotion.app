<?php

namespace App\Pricings;

use App\Models\Loan;
use App\Models\Pricing;

class InvoiceSyntheticCommunityContribution extends InvoiceContribution
{
    public function __construct(Loan $loan)
    {
        parent::__construct(
            new Pricing([
                "name" => "Contribution excédentaire à la communauté",
                "rule" => 0,
                "community_id" => $loan->community_id,
            ]),
            $loan
        );
    }
}
