<?php

namespace App\Pricings;

use App\Models\Invoice;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Support\Collection;

class LoanInvoiceHelper
{
    public static function splitIntoAmountAndTaxes(float $total): array
    {
        $taxesTps = round(($total / 1.14975) * 0.05, 2);
        $taxesTvq = round(($total / 1.14975) * 0.09975, 2);

        $tipWithoutTaxes = round($total - $taxesTps - $taxesTvq, 2);
        return [
            "amount" => $tipWithoutTaxes,
            "taxes_tps" => $taxesTps,
            "taxes_tvq" => $taxesTvq,
        ];
    }

    public static function getLoanMeta(Loan $loan): array
    {
        $meta = [
            "entityType" => "loan",
            "entityId" => 0,
        ];

        // Account for undefined loan id when estimating.
        if ($loan->id) {
            $meta["entityId"] = $loan->id;
            $meta["entityUrl"] = route("loans.get", [
                "loan" => $loan,
            ]);
        }

        return $meta;
    }

    public static function getBorrowerInvoice(Loan $loan): Invoice
    {
        $loanMeta = self::getLoanMeta($loan);

        $pricings = $loan->getPricings();
        $borrowerInvoiceItems = $pricings->getBorrowerInvoiceItems($loanMeta);

        return self::createInvoiceWithBillItems(
            $loan,
            $borrowerInvoiceItems,
            $loan->borrowerUser
        );
    }

    public static function getOwnerInvoice(Loan $loan): Invoice
    {
        $loanMeta = self::getLoanMeta($loan);
        $pricings = $loan->getPricings();
        $ownerInvoiceItems = $pricings->getOwnerInvoiceItems($loanMeta);

        return self::createInvoiceWithBillItems(
            $loan,
            $ownerInvoiceItems,
            $loan->loanable->owner_user
        );
    }

    private static function createInvoiceWithBillItems(
        Loan $loan,
        Collection $invoiceItems,
        User $user
    ): Invoice {
        $loanableName = $loan->loanable->name;
        $localDate = $loan->departure_at->timezone($loan->loanable->timezone);
        $prettyDate =
            $localDate->translatedFormat("j F Y \à H:i") .
            $localDate->format(" T"); // Append timezone

        $invoice = new Invoice([
            "period" => "Emprunt de $loanableName le $prettyDate",
        ]);
        $invoice->user()->associate($user);

        $invoice->setRelation("billItems", $invoiceItems);

        return $invoice;
    }
}
