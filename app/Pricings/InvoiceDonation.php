<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Loan;

class InvoiceDonation extends InvoiceItems
{
    public function __construct(public float $donationAmount, Loan $loan)
    {
        parent::__construct($loan);
    }

    public function getBorrowerBillItems($meta = []): array
    {
        $label = "Contribution volontaire";

        // Default values before checking each case specifically.
        $borrowerInvoiceItemData = [
            "label" => $label,
            "item_type" => BillItemTypes::donationLoan,
            ...LoanInvoiceHelper::splitIntoAmountAndTaxes(
                -$this->donationAmount
            ),
            "meta" => $meta,
        ];

        return [new BillItem($borrowerInvoiceItemData)];
    }
}
