<?php

namespace App\Pricings;

use App\Calendar\Interval;
use App\Enums\Requirement;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use App\Models\Pricing;
use Carbon\CarbonImmutable;
use Illuminate\Support\Collection;

/**
 * Computes the pricings applied to a Loan and exposes an interface to get all BillItems related
 * to pricings for the given Loan.
 *
 * This class also handles the allocation of the users tip into the different contributions,
 * if necessary. If no contribution pricings are defined, we fall back to generating a simple
 * InvoiceDonationSegement instead.
 */
class LoanPricingsHelper
{
    private ?Collection $invoiceItems = null;

    private Collection $contributionPricings;
    private ?Pricing $pricePricing;
    private Collection $insurancePricings;

    public function __construct(private readonly Loan|LoanResource $loan)
    {
        $allPricings = Pricing::query()
            ->forCommunity($loan->community_id)
            ->forLoanable($loan->loanable)
            ->get();

        // For contributions and price, we consider pricings which are active on the loan creation
        // date
        $this->contributionPricings = $allPricings->filter(
            fn(Pricing $p) => $p->pricing_type === "contribution" &&
                $p->activeOn($loan->created_at)
        );

        // We keep only the local contribution pricing, if present, or the global contribution pricing otherwise.
        $pricePricings = $allPricings->filter(
            fn(Pricing $p) => $p->pricing_type === "price" &&
                $p->activeOn($loan->created_at)
        );
        $communityPricePricings = $pricePricings->filter(
            fn($p) => !is_null($p->community_id)
        );
        $this->pricePricing =
            $communityPricePricings->first() ?? $pricePricings->first();

        // We keep all insurance pricings that applied on the actual dates of the loan.
        $this->insurancePricings = $allPricings->filter(
            fn(Pricing $p) => $p->pricing_type === "insurance" &&
                $p->intersects(
                    new Interval($loan->departure_at, $loan->actual_return_at)
                )
        );
    }

    /**
     * @return Collection<InvoiceItems>
     */
    public function getInvoiceItems(): Collection
    {
        if (!$this->invoiceItems) {
            $this->invoiceItems = self::getPricingPriceItems()
                ->merge(self::getPricingInsuranceItems())
                ->merge(self::getPricingContributionItems())
                ->merge(self::getExpenseItems());
        }

        return $this->invoiceItems;
    }

    public function getBorrowerInvoiceItems(array $loanMeta): Collection
    {
        return self::getInvoiceItems()->flatMap(
            fn(InvoiceItems $segment) => $segment->getBorrowerBillItems(
                $loanMeta
            )
        );
    }

    public function getOwnerInvoiceItems(array $loanMeta): Collection
    {
        return self::getInvoiceItems()->flatMap(
            fn(InvoiceItems $segment) => $segment->getOwnerBillItems($loanMeta)
        );
    }

    public function isContributionRequired(): bool
    {
        $communityContributionPricing = $this->contributionPricings
            ->filter(fn($p) => !$p->is_global)
            ->first();

        return $communityContributionPricing &&
            $communityContributionPricing->is_mandatory;
    }

    public function getDesiredContribution(): float
    {
        $communityContributionPricing = $this->contributionPricings
            ->filter(fn($p) => !$p->is_global)
            ->first();

        $total = 0;

        if ($communityContributionPricing) {
            $total += (new InvoiceContribution(
                $communityContributionPricing,
                $this->loan
            ))->getCost();
        }

        $globalContributionPricing = $this->contributionPricings
            ->filter(fn($p) => $p->is_global)
            ->first();

        if ($globalContributionPricing) {
            $total += (new InvoiceContribution(
                $globalContributionPricing,
                $this->loan
            ))->getCost();
        }

        return $total;
    }

    public function requiresMileage(): bool
    {
        return ($this->loan->applicable_amount_types->price ===
            Requirement::required &&
            $this->pricePricing &&
            str_contains($this->pricePricing->rule, '$KM')) ||
            ($this->loan->applicable_amount_types->insurance ===
                Requirement::required &&
                $this->insurancePricings
                    ->filter(fn($p) => str_contains($p->rule, '$KM'))
                    ->count() > 0) ||
            ($this->loan->applicable_amount_types->contributions ===
                Requirement::required &&
                $this->contributionPricings
                    ->filter(fn($p) => str_contains($p->rule, '$KM'))
                    ->count() > 0);
    }

    public function isSubscriptionPossible(): bool
    {
        // Subscription possible only if there is a community contribution with yearly target.
        // Keep this in sync with logic in SubscriptionController::myTargets
        return $this->contributionPricings
            ->filter(
                fn(Pricing $p) => !$p->is_global &&
                    !is_null($p->yearly_target_per_user)
            )
            ->isNotEmpty();
    }

    public function getPricingPriceItems(): Collection
    {
        if (
            !$this->pricePricing ||
            $this->loan->applicable_amount_types->price ===
                Requirement::notApplicable
        ) {
            return Collection::empty();
        }
        return collect([
            InvoicePricingItems::forPricing($this->pricePricing, $this->loan),
        ]);
    }

    private function firstApplicableInsurancePricing(CarbonImmutable $date)
    {
        $applicablePricings = $this->insurancePricings->filter(
            fn(Pricing $p) => $date->greaterThanOrEqualTo(
                $p->start_date_carbon
            ) &&
                (is_null($p->end_date) || $date->lessThan($p->end_date_carbon))
        );

        return $applicablePricings->filter(fn($p) => !$p->is_global)->first() ??
            $applicablePricings->first();
    }

    public function getPricingInsuranceItems()
    {
        if (
            $this->loan->applicable_amount_types->insurance ===
            Requirement::notApplicable
        ) {
            return Collection::empty();
        }

        // flatten list of insurance pricings according to their intervals
        $currentDay = $this->loan->departure_at
            ->tz(config("app.default_user_timezone"))
            ->toImmutable();
        $currentIntervalStart = $currentDay->copy();
        $lastDay = $this->loan->actual_return_at
            ->tz(config("app.default_user_timezone"))
            ->toImmutable();
        $currentPricing = $this->firstApplicableInsurancePricing($currentDay);

        $insurancePricingItems = [];
        while ($currentDay->isBefore($lastDay)) {
            // Intervals limits are set at the start of the day in the timezone used for pricings,
            // or at the start or end of the loan for partial days.
            $nextDay = $currentDay->addDay()->startOfDay();
            $nextPricing = $this->firstApplicableInsurancePricing($nextDay);

            if ($currentPricing?->id !== $nextPricing?->id) {
                if ($currentPricing) {
                    $insurancePricingItems[] = InvoicePricingItems::forPricing(
                        $currentPricing,
                        $this->loan,
                        new Interval($currentIntervalStart, $nextDay)
                    );
                }
                $currentIntervalStart = $nextDay->copy();
                $currentPricing = $nextPricing;
            }

            $currentDay = $nextDay;
        }

        if ($currentPricing && $currentIntervalStart !== $currentDay) {
            $insurancePricingItems[] = InvoicePricingItems::forPricing(
                $currentPricing,
                $this->loan,
                new Interval($currentIntervalStart, $lastDay)
            );
        }

        return collect($insurancePricingItems);
    }

    public function getPricingContributionItems(): Collection
    {
        if (
            $this->loan->applicable_amount_types->contributions ===
            Requirement::notApplicable
        ) {
            return Collection::empty();
        }

        $globalContribution = $this->contributionPricings
            ->filter(fn($p) => $p->is_global)
            ->first();
        $communityContribution = $this->contributionPricings
            ->filter(fn($p) => !$p->is_global)
            ->first();
        $amountToSplit = $this->loan->platform_tip;

        if (!$globalContribution && !$communityContribution) {
            if ($amountToSplit < 0.005) {
                return collect([]);
            }

            // No contribution defined, we fall back to the generic donation.loan bill items
            $segment = new InvoiceDonation($amountToSplit, $this->loan);
            return collect([$segment]);
        }

        $isContributionRequired = false;

        if ($communityContribution) {
            $isContributionRequired = $communityContribution->is_mandatory;
        }

        if (!$globalContribution) {
            $communitySegment = new InvoiceContribution(
                $communityContribution,
                $this->loan
            );

            // If no global contribution is defined, everything goes to community.
            $communityAmount = $isContributionRequired
                ? max($amountToSplit, $communitySegment->getCost())
                : $amountToSplit;

            if ($communityAmount < 0.005) {
                return collect([]);
            }

            $communitySegment->setAmount($communityAmount);
            return collect([$communitySegment]);
        }

        $globalSegment = new InvoiceContribution(
            $globalContribution,
            $this->loan
        );

        // Current allocation strategy: fill global and anything extra goes to community
        $globalAllocation = min($amountToSplit, $globalSegment->getCost());
        $communityAllocation = $amountToSplit - $globalAllocation;

        if (!$communityContribution) {
            $communitySegment = new InvoiceSyntheticCommunityContribution(
                $this->loan
            );
        } else {
            $communitySegment = new InvoiceContribution(
                $communityContribution,
                $this->loan
            );

            if ($isContributionRequired) {
                $globalAllocation = max(
                    $globalAllocation,
                    $globalSegment->getCost()
                );
                $communityAllocation = max(
                    $communityAllocation,
                    $communitySegment->getCost()
                );
            }
        }

        $nonZeroSegments = [];
        if ($globalAllocation > 0.005) {
            $globalSegment->setAmount($globalAllocation);
            $nonZeroSegments[] = $globalSegment;
        }

        if ($communityAllocation > 0.005) {
            $communitySegment->setAmount($communityAllocation);
            $nonZeroSegments[] = $communitySegment;
        }

        return collect($nonZeroSegments);
    }

    private function getExpenseItems(): Collection
    {
        if (
            $this->loan->applicable_amount_types->expenses ===
            Requirement::notApplicable
        ) {
            return Collection::empty();
        }
        return collect([new InvoiceExpense($this->loan)]);
    }
}
