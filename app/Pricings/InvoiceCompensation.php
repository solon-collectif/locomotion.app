<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Models\BillItem;

class InvoiceCompensation extends InvoicePricingItems
{
    public function getBorrowerBillItems($meta = []): array
    {
        // Add pricing data to meta information.
        $meta["pricing_id"] = $this->pricing->id;
        $meta["pricing_name"] = $this->pricing->name;

        return [
            new BillItem([
                "label" => "Coût de l'emprunt",
                "item_type" => BillItemTypes::loanPrice,
                "amount" => $this->loan->borrowedByOwner()
                    ? 0
                    : -$this->getCost(),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "meta" => $meta,
            ]),
        ];
    }

    public function getOwnerBillItems($meta = []): array
    {
        // Add pricing data to meta information.
        $meta["pricing_id"] = $this->pricing->id;
        $meta["pricing_name"] = $this->pricing->name;
        return [
            new BillItem([
                "label" => "Coût de l'emprunt",
                "item_type" => BillItemTypes::loanPrice,
                "amount" => $this->getCost(),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "meta" => $meta,
            ]),
        ];
    }
}
