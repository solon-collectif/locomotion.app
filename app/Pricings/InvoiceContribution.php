<?php

namespace App\Pricings;

use App\Enums\BillItemTypes;
use App\Models\BillItem;

class InvoiceContribution extends InvoicePricingItems
{
    protected ?float $contributionAmount = null;

    public function setAmount(?float $contributionAmount = null): void
    {
        $this->contributionAmount = $contributionAmount;
    }

    public function getBorrowerBillItems($meta = []): array
    {
        // Add pricing data to meta information.
        $meta["pricing_id"] = $this->pricing->id;
        $meta["pricing_name"] = $this->pricing->name;
        $meta["pricing_description"] = $this->pricing->description;
        $label = "Contribution au Réseau LocoMotion";
        if ($this->pricing->community_id) {
            $label = "Contribution à la communauté {$this->pricing->community->name}";
        }

        $total = $this->contributionAmount ?? $this->getCost();

        // Default values before checking each case specifically.
        $borrowerInvoiceItemData = [
            "label" => $label,
            "item_type" => BillItemTypes::loanContribution,
            "contribution_community_id" => $this->pricing->community_id,
            ...LoanInvoiceHelper::splitIntoAmountAndTaxes(-$total),
            "meta" => $meta,
        ];

        return [new BillItem($borrowerInvoiceItemData)];
    }

    public function getOwnerBillItems($meta = []): array
    {
        return [];
    }
}
