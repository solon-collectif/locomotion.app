<?php

namespace Tests;

use Illuminate\Support\Testing\Fakes\QueueFake;
use PHPUnit\Framework\Assert as PHPUnit;
use Queue;

/**
 * Extends QueueFake for testing queues while handling exceptions.
 *
 * This allows to more closely fake an async queue whose exceptions will not interrupt
 * the request where the jobs are dispatched, which is what normally happens with the sync queue
 * used in testing.
 */
class ErrorableQueueFake extends QueueFake
{
    private array $exceptions = [];

    public static function fake(): void
    {
        Queue::swap(
            new ErrorableQueueFake(app(), null, Queue::getFacadeRoot())
        );
    }

    public function shouldFakeJob($job)
    {
        return false;
    }

    public function push($job, $data = "", $queue = null)
    {
        try {
            parent::push($job, $data, $queue);
        } catch (\Exception $e) {
            $this->exceptions[is_object($job) ? get_class($job) : $job][] = $e;
        }
    }

    public function threwException($job, $callback)
    {
        if (!isset($this->exceptions[$job]) && empty($this->exceptions[$job])) {
            return collect();
        }
        $callback = $callback ?: fn() => true;
        return collect($this->exceptions[$job])->filter(
            fn($exception) => $callback($exception)
        );
    }

    public function assertThrewException($job, $callback = null)
    {
        PHPUnit::assertTrue(
            $this->threwException($job, $callback)->count() > 0,
            "The expected exception was not thrown by {$job}."
        );
    }
}
