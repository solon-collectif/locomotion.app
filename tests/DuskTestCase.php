<?php

namespace Tests;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Support\Collection;
use Laravel\Dusk\TestCase as BaseTestCase;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    public $seed = true;

    public array $exceptTables = ["loanable_type_details", "oauth_clients"];

    protected function tearDown(): void
    {
        // Close all browsers between tests to avoid leaking localStorage state
        $this->closeAll();
    }

    /**
     * Create the RemoteWebDriver instance.
     */
    protected function driver(): RemoteWebDriver
    {
        $options = (new ChromeOptions())->addArguments(
            collect([
                $this->shouldStartMaximized()
                    ? "--start-maximized"
                    : "--window-size=1920,1080",
                "--disable-search-engine-choice-screen",
                "--no-sandbox",
                "--disable-dev-shm-usage",
                "--disable-extensions",
            ])
                ->unless($this->hasHeadlessDisabled(), function (
                    Collection $items
                ) {
                    return $items->merge(["--disable-gpu", "--headless=new"]);
                })
                ->all()
        );

        return RemoteWebDriver::create(
            env("DUSK_DRIVER_URL") ?? "http://localhost:9515",
            DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY,
                $options
            )
        );
    }
}
