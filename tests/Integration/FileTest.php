<?php

namespace Tests\Integration;

use App\Http\Middleware\WrapInTransaction;
use App\Models\Community;
use App\Models\File;
use App\Models\Loanable;
use App\Models\User;
use Exception;
use Illuminate\Http\UploadedFile;
use Mockery;
use Storage;
use Tests\TestCase;

class FileTest extends TestCase
{
    public function testUploadFile()
    {
        Storage::fake();

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $response->assertJsonStructure([
            "path",
            "original_filename",
            "filename",
            "field",
            "filesize",
            "updated_at",
            "created_at",
            "id",
        ]);

        $response->assertJson([
            "field" => "residency_proof",
            "original_filename" => "foo.png",
            "path" => "files/tmp/{$this->user->id}",
        ]);

        // New filename is some temporary string
        $filename = $response->json("filename");
        self::assertNotEquals("foo.png", $filename);
        self::assertStringEndsWith(".png", $filename);
        Storage::assertExists("files/tmp/{$this->user->id}/" . $filename);
    }

    public function testUploadFile_cleansUpNewFileIfTransactionFails()
    {
        Storage::fake();

        File::saving(fn() => throw new Exception());

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);
        $response->assertStatus(500);
        Storage::assertDirectoryEmpty("files/tmp/{$this->user->id}");

        // Without the middleware, we would have a dangling file
        $this->withoutMiddleware(WrapInTransaction::class);
        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);
        $response->assertStatus(500);
        self::assertCount(1, Storage::files("files/tmp/{$this->user->id}"));
    }

    public function testUploadFile_failsIfNotStored()
    {
        Storage::shouldReceive("exists")->andReturn(false);
        Storage::shouldReceive("putFileAs")
            ->withArgs([
                "files/tmp/{$this->user->id}",
                Mockery::any(),
                Mockery::any(),
            ])
            ->once()
            ->andReturn(false);

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $response->assertStatus(500);
    }

    public function testGetFile_doesntExist()
    {
        $this->get("/api/v1/images/123")->assertStatus(404);

        // Create db file without related file in storage
        File::withoutEvents(
            fn() => File::factory()->create([
                "id" => 123,
            ])
        );

        $this->assertModelExists(File::find(123));
        $this->get("/api/v1/files/123")->assertStatus(404);
    }

    public function testGetTempFile()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $file = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/$user->id",
            ]);

        Storage::assertExists($file->full_path);
        $fileData = Storage::get($file->full_path);

        $this->actAs($user);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
    }

    public function testGetUserFile()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $unapprovedCommunity = Community::factory()->create();
        $user->communities()->attach($unapprovedCommunity);

        $file = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->id,
                "fileable_type" => "user",
                "field" => "residency_proof",
            ]);

        Storage::assertExists($file->full_path);
        $fileData = Storage::get($file->full_path);

        $this->actAs($user);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $adminOfUnapprovedCommunity = User::factory()
            ->adminOfCommunity($unapprovedCommunity)
            ->create();
        $this->actAs($adminOfUnapprovedCommunity);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
    }

    public function testGetLoanableFile()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $unapprovedCommunity = Community::factory()->create();
        $user->communities()->attach($unapprovedCommunity);

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withCar()
            ->create();

        $file = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $loanable->id,
                "fileable_type" => "car",
                "field" => "report",
            ]);

        Storage::assertExists($file->full_path);
        $fileData = Storage::get($file->full_path);

        $this->actAs($user);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);

        $adminOfUnapprovedCommunity = User::factory()
            ->adminOfCommunity($unapprovedCommunity)
            ->create();
        $this->actAs($adminOfUnapprovedCommunity);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
    }

    public function testGetBorrowerFile()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $unapprovedCommunity = Community::factory()->create();
        $user->communities()->attach($unapprovedCommunity);

        $file = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->borrower->id,
                "fileable_type" => "borrower",
                "field" => "saaq",
            ]);

        Storage::assertExists($file->full_path);
        $fileData = Storage::get($file->full_path);

        $this->actAs($user);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/files/$file->id")
            ->assertStatus(200)
            ->assertStreamedContent($fileData);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $adminOfUnapprovedCommunity = User::factory()
            ->adminOfCommunity($unapprovedCommunity)
            ->create();
        $this->actAs($adminOfUnapprovedCommunity);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/files/$file->id")->assertStatus(403);
    }

    public function testAssociateFileToEntity_movesFile()
    {
        Storage::fake();

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $filename = $response->json("filename");
        $tmpPath = "files/tmp/{$this->user->id}/" . $filename;
        Storage::assertExists($tmpPath);

        $user = User::factory()->create();
        $fileId = $response->json("id");
        $this->json("put", "/api/v1/users/$user->id", [
            "residency_proof" => [
                [
                    "id" => $fileId,
                ],
            ],
        ])->assertStatus(200);

        Storage::assertMissing($tmpPath);
        self::assertCount(
            1,
            Storage::files("files/user/$user->id/residency_proof/")
        );
        self::assertEquals(
            "files/user/$user->id/residency_proof",
            File::find($fileId)->path
        );
    }

    public function testAssociateFileToEntity_keepsTmpFileOnError()
    {
        Storage::fake();

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $filename = $response->json("filename");
        $tmpPath = "files/tmp/{$this->user->id}/" . $filename;
        Storage::assertExists($tmpPath);

        $user = User::factory()->create();
        File::saved(fn() => throw new Exception());
        $fileId = $response->json("id");
        $this->json("put", "/api/v1/users/$user->id", [
            "residency_proof" => [
                [
                    "id" => $fileId,
                ],
            ],
        ])->assertStatus(500);
        self::assertEquals($tmpPath, File::find($fileId)->full_path);
        Storage::assertExists($tmpPath);
        storage::assertDirectoryEmpty("files/user/$user->id/residency_proof/");
    }
}
