<?php

namespace Tests\Integration;

use App\Helpers\Path;
use App\Http\Middleware\WrapInTransaction;
use App\Models\Community;
use App\Models\Image;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Exception;
use Illuminate\Http\UploadedFile;
use Storage;
use Tests\TestCase;

class ImageTest extends TestCase
{
    public function testUploadImage()
    {
        Storage::fake();

        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);

        $response->assertJsonStructure([
            "path",
            "original_filename",
            "filename",
            "field",
            "filesize",
            "updated_at",
            "created_at",
            "id",
            "height",
            "width",
            "sizes" => ["api", "blur"],
        ]);

        $response->assertJson([
            "field" => "avatar",
            "original_filename" => "foo.png",
            "filename" => "avatar.png",
            "sizes" => ["api" => ["original", "thumbnail"]],
        ]);

        // New path starts ends with uniquely named folder
        $path = $response->json("path");
        self::assertStringStartsWith("images/tmp/{$this->user->id}/", $path);
        self::assertNotEquals("images/tmp/{$this->user->id}/", $path);
    }

    public function testUploadImage_failsIfNotStored()
    {
        Storage::shouldReceive("exists")->andReturn(false);
        Storage::shouldReceive("put")
            ->once()
            ->andReturn(false);

        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);

        $response->assertStatus(500);
    }

    public function testUploadImage_cleansUpNewImageIfTransactionFails()
    {
        Storage::fake();

        Image::saving(fn() => throw new Exception());

        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);
        $response->assertStatus(500);
        Storage::assertDirectoryEmpty("images/tmp/{$this->user->id}");

        // Without the middleware, we would have a dangling file
        $this->withoutMiddleware(WrapInTransaction::class);
        $response = $this->post("/api/v1/images", [
            "field" => "residency_proof",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);
        $response->assertStatus(500);
        self::assertCount(
            1,
            Storage::directories("images/tmp/{$this->user->id}")
        );
    }

    public function testAssociateImageToEntity_movesImage()
    {
        Storage::fake();

        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ])->assertStatus(201);

        $filename = $response->json("filename");
        $path = $response->json("path");
        self::assertStringStartsWith("images/tmp/{$this->user->id}", $path);
        $tmpPath = Path::join($path, $filename);
        Storage::assertExists($tmpPath);

        $user = User::factory()->create();
        $imageId = $response->json("id");
        $this->json("put", "/api/v1/users/$user->id", [
            "avatar" => [
                "id" => $imageId,
            ],
        ])->assertStatus(200);

        Storage::assertMissing($tmpPath);
        $avatarDirs = Storage::directories("images/user/$user->id/avatar/");
        self::assertCount(1, $avatarDirs);

        self::assertEquals($avatarDirs[0], Image::find($imageId)->path);
    }

    public function testGetImage_doesntExist()
    {
        $this->get("/api/v1/images/123")->assertStatus(404);

        // Create image without related file in storage
        Image::withoutEvents(
            fn() => Image::factory()->create([
                "id" => 123,
            ])
        );

        $this->assertModelExists(Image::find(123));
        $this->get("/api/v1/images/123")->assertStatus(404);
    }

    public function testGetTempImage()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $fakeImage = UploadedFile::fake()->image("foo.png");

        Storage::put(
            "images/tmp/$user->id/fooUniquePath/avatar.png",
            $fakeImage->getContent()
        );

        $image = Image::factory()->create([
            "field" => "avatar",
            "filename" => "avatar.png",
            "original_filename" => "foo.png",
            "path" => "images/tmp/{$user->id}/fooUniquePath",
        ]);

        Storage::assertExists($image->full_path);

        // Only the creator and global admins have access to temp files
        $this->actAs($user);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($fakeImage->getContent());
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($fakeImage->getContent());

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
    }

    public function testGetUserImage()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $unapprovedCommunity = Community::factory()->create();
        $user->communities()->attach($unapprovedCommunity);

        $image = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        Storage::assertExists($image->full_path);
        $imageContent = Storage::get($image->full_path);

        // Only the creator and global admins have access to temp files
        $this->actAs($user);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $adminOfUnapprovedCommunity = User::factory()
            ->adminOfCommunity($unapprovedCommunity)
            ->create();
        $this->actAs($adminOfUnapprovedCommunity);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);

        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);

        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
    }

    public function testGetLoanableImage()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $unapprovedCommunity = Community::factory()->create();
        $ownerUser->communities()->attach($unapprovedCommunity);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $image = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loanable->id,
                "imageable_type" => "loanable",
                "field" => "image",
            ]);

        Storage::assertExists($image->full_path);
        $imageContent = Storage::get($image->full_path);

        $this->actAs($ownerUser);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->actAs($admin);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);
        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/images/$image->id")
            ->assertStatus(200)
            ->assertStreamedContent($imageContent);

        $adminOfUnapprovedCommunity = User::factory()
            ->adminOfCommunity($unapprovedCommunity)
            ->create();
        $this->actAs($adminOfUnapprovedCommunity);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/images/$image->id")->assertStatus(403);
    }

    public function testGetLoanActionImage()
    {
        Storage::fake();

        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $coowner = User::factory()->create();
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner($coowner)
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "community_id" => $community,
                "borrower_user_id" => $borrowerUser,
                "loanable_id" => $loanable,
            ]);

        $handoverImage = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loan->id,
                "imageable_type" => "loan",
                "field" => "milage_end",
            ]);
        $handoverImageContent = Storage::get($handoverImage->full_path);
        $handoverExpenseImage = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loan->id,
                "imageable_type" => "loan",
                "field" => "expense",
            ]);
        $handoverExpenseImageContent = Storage::get(
            $handoverExpenseImage->full_path
        );
        $takeoverImage = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loan->id,
                "imageable_type" => "loan",
                "field" => "mileage_start",
            ]);
        $takeoverImageContent = Storage::get($takeoverImage->full_path);

        $this->actAs($borrowerUser);
        $this->get("/api/v1/images/$handoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverImageContent);
        $this->get("/api/v1/images/$handoverExpenseImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverExpenseImageContent);
        $this->get("/api/v1/images/$takeoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($takeoverImageContent);

        $admin = User::factory()->create([
            "role" => "admin",
        ]);

        $this->actAs($admin);
        $this->get("/api/v1/images/$handoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverImageContent);
        $this->get("/api/v1/images/$handoverExpenseImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverExpenseImageContent);
        $this->get("/api/v1/images/$takeoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($takeoverImageContent);

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $this->actAs($communityAdmin);
        $this->get("/api/v1/images/$handoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverImageContent);
        $this->get("/api/v1/images/$handoverExpenseImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverExpenseImageContent);
        $this->get("/api/v1/images/$takeoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($takeoverImageContent);

        $this->actAs($loanable->owner_user);
        $this->get("/api/v1/images/$handoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverImageContent);
        $this->get("/api/v1/images/$handoverExpenseImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverExpenseImageContent);
        $this->get("/api/v1/images/$takeoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($takeoverImageContent);

        $this->actAs($coowner);
        $this->get("/api/v1/images/$handoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverImageContent);
        $this->get("/api/v1/images/$handoverExpenseImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($handoverExpenseImageContent);
        $this->get("/api/v1/images/$takeoverImage->id")
            ->assertStatus(200)
            ->assertStreamedContent($takeoverImageContent);

        $communityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $this->actAs($communityMember);
        $this->get("/api/v1/images/$handoverImage->id")->assertStatus(403);
        $this->get("/api/v1/images/$handoverExpenseImage->id")->assertStatus(
            403
        );
        $this->get("/api/v1/images/$takeoverImage->id")->assertStatus(403);

        $randomUser = User::factory()->create();
        $this->actAs($randomUser);
        $this->get("/api/v1/images/$handoverImage->id")->assertStatus(403);
        $this->get("/api/v1/images/$handoverExpenseImage->id")->assertStatus(
            403
        );
        $this->get("/api/v1/images/$takeoverImage->id")->assertStatus(403);
    }

    public function testGetInvalidSize()
    {
        Storage::fake();
        $user = User::factory()->create();

        $image = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        $this->get("/api/v1/images/$image->id?size=invalidsize")->assertStatus(
            422
        );
    }

    public function testGetThumbnail()
    {
        Storage::fake();
        $user = User::factory()->create();

        $image = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        $imageContent = Storage::get($image->full_path);
        $thumbnailPath = $image->getImageSizePath("thumbnail");
        Storage::assertExists($thumbnailPath);
        $thumbnailContent = Storage::get($thumbnailPath);
        self::assertNotEquals($imageContent, $thumbnailContent);

        $this->get("/api/v1/images/$image->id?size=thumbnail")
            ->assertStatus(200)
            ->assertStreamedContent($thumbnailContent);
    }

    public function testGetThumbnail_generatesSizesIfMissing()
    {
        Storage::fake();
        $user = User::factory()->create();

        $image = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        $thumbnailPath = $image->getImageSizePath("thumbnail");
        Storage::assertExists($thumbnailPath);
        $thumbnailContent = Storage::get($thumbnailPath);
        Storage::delete($thumbnailPath);

        $this->get("/api/v1/images/$image->id?size=thumbnail")
            ->assertStatus(200)
            ->assertStreamedContent($thumbnailContent);

        Storage::assertExists($thumbnailPath);
    }
}
