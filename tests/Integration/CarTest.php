<?php

namespace Tests\Integration;

use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Tests\TestCase;

class CarTest extends TestCase
{
    private static $carResponseStructure = [
        "type",
        "comments",
        "instructions",
        "location_description",
        "name",
        "position",
        "details" => [
            "brand",
            "engine",
            "model",
            "has_informed_insurer",
            "insurer",
            "value_category",
            "papers_location",
            "plate_number",
            "transmission_mode",
            "year_of_circulation",
        ],
    ];

    public function testCarCreationValidation()
    {
        $ownerUser = User::factory()->create();

        $validData = [
            "type" => "car",
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "location_description" => $this->faker->sentence,

            "name" => $this->faker->name,
            "owner_user" => ["id" => $ownerUser->id],
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "details" => [
                "engine" => $this->faker->randomElement([
                    "fuel",
                    "diesel",
                    "electric",
                    "hybrid",
                ]),
                "brand" => $this->faker->word,
                "has_informed_insurer" => true,
                "insurer" => $this->faker->word,
                "value_category" => $this->faker->randomElement([
                    "lte50k",
                    "lte70k",
                    "lte100k",
                ]),
                "model" => $this->faker->sentence,
                "papers_location" => $this->faker->randomElement([
                    "in_the_car",
                    "to_request_with_car",
                ]),
                "plate_number" => $this->faker->shuffle("9F29J2"),
                "pricing_category" => "large",
                "transmission_mode" => $this->faker->randomElement([
                    "automatic",
                    "manual",
                ]),
                "year_of_circulation" => $this->faker->year($max = "now"),
            ],
        ];
        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        $wrongEngineData = $validData;
        $wrongEngineData["details"]["engine"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $wrongEngineData);
        $response->assertStatus(422);

        $invalidPapersLocation = $validData;
        $invalidPapersLocation["details"]["papers_location"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidPapersLocation
        );
        $response->assertStatus(422);

        $invalidPricingCategory = $validData;
        $invalidPricingCategory["details"]["pricing_category"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidPricingCategory
        );
        $response->assertStatus(422);

        $invalidTransmissionMode = $validData;
        $invalidTransmissionMode["details"]["transmission_mode"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidTransmissionMode
        );
        $response->assertStatus(422);

        $invalidYearOfCirculation = $validData;
        $invalidYearOfCirculation["details"]["year_of_circulation"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidYearOfCirculation
        );
        $response->assertStatus(422);
    }

    public function testCreateCars()
    {
        $ownerUser = User::factory()->create();

        $data = [
            "type" => "car",
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "location_description" => $this->faker->sentence,

            "name" => $this->faker->name,
            "owner_user" => ["id" => $ownerUser->id],
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "details" => [
                "engine" => $this->faker->randomElement([
                    "fuel",
                    "diesel",
                    "electric",
                    "hybrid",
                ]),
                "brand" => $this->faker->word,
                "has_informed_insurer" => true,
                "insurer" => $this->faker->word,
                "value_category" => $this->faker->randomElement([
                    "lte50k",
                    "lte70k",
                    "lte100k",
                ]),
                "model" => $this->faker->sentence,
                "papers_location" => $this->faker->randomElement([
                    "in_the_car",
                    "to_request_with_car",
                ]),
                "plate_number" => $this->faker->shuffle("9F29J2"),
                "pricing_category" => "large",
                "transmission_mode" => $this->faker->randomElement([
                    "automatic",
                    "manual",
                ]),
                "year_of_circulation" => $this->faker->year($max = "now"),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$carResponseStructure)
            ->assertJsonPath("details.pricing_category", "large");

        // Check that loanable has $ownerUser as owner
        $loanable = Loanable::find($response->json()["id"]);
        $this->assertEquals($ownerUser->id, $loanable->owner_user->id);
    }

    public function testShowCars()
    {
        $car = Loanable::factory()
            ->withOwner($this->user)
            ->withCar()
            ->create();

        $response = $this->json("GET", "/api/v1/loanables/$car->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$carResponseStructure);
    }

    public function testUpdateCars()
    {
        $car = Loanable::factory()
            ->withOwner($this->user)
            ->withCar()
            ->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$car->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteCars()
    {
        $car = Loanable::factory()
            ->withOwner($this->user)
            ->withCar()
            ->create();

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$car->id");
        $response->assertStatus(404);
    }

    public function testDeleteCarsWithActiveLoan()
    {
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);
        $car = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$car->id");
        $response->assertStatus(404);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);
        $car = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);
        $loan = Loan::factory()
            ->paid()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);
        $car = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);
    }

    public function testListCars()
    {
        Loanable::factory(2)
            ->withOwner($this->user)
            ->withCar()
            ->create();

        $response = $this->json("GET", route("loanables.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(static::$carResponseStructure)
            );
    }
}
