<?php

namespace Tests\Integration;

use App\Enums\IncidentNotificationLevel;
use App\Enums\LoanableUserRoles;
use App\Mail\Incidents\IncidentCreated;
use App\Mail\Incidents\IncidentResolved;
use App\Mail\Incidents\LoansBlocked;
use App\Mail\Incidents\NoteAdded;
use App\Models\Community;
use App\Models\IncidentNotification;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class IncidentTest extends TestCase
{
    public function testIncidentCreation_SendsEmailsToConcernedUsers()
    {
        \Mail::fake();

        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()->create([
            "borrower_user_id" => $borrowerUser,
            "loanable_id" => Loanable::factory()
                ->withOwner($owner)
                ->withCoowner($coowner)
                ->create(),
        ]);

        $communityAdmin = User::factory()
            ->adminOfCommunity($ourCommunity)
            ->create();
        $globalAdmin = User::factory()->create(["role" => "admin"]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "general",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        $response->assertStatus(201);

        \Mail::assertQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($owner->email)
        );
        \Mail::assertQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($coowner->email)
        );
        \Mail::assertQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($globalAdmin->email)
        );
        // Person reporting the incident is not notified
        \Mail::assertNotQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($this->user->email)
        );
        \Mail::assertNotQueued(
            IncidentCreated::class,
            fn(IncidentCreated $mail) => $mail->hasTo($communityAdmin->email)
        );

        $notifications = IncidentNotification::where(
            "incident_id",
            $response->json("id")
        )->get();

        // $owner, $borrowerUser, $coowner, $globalAdmin, $this->user
        self::assertCount(5, $notifications);
        self::assertCount(
            5,
            $notifications
                ->toBase()
                ->filter(
                    fn(IncidentNotification $n) => $n->level ===
                        IncidentNotificationLevel::All
                )
        );
    }

    public function testAccidentCreation_blocksLoansForOneDay()
    {
        $loan = Loan::factory()->create();

        $now = CarbonImmutable::now();
        self::setTestNow($now);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        $response->assertJson([
            "blocking_until" => $now
                ->addDay()
                ->startOfSecond()
                ->toISOString(),
        ]);
    }

    public function testIncidentBlocking_notifiesBlockedBorrowers()
    {
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        \Mail::fake();

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        \Mail::assertQueued(
            LoansBlocked::class,
            fn(LoansBlocked $mail) => $mail->hasTo(
                $blockedLoan->borrowerUser->email
            )
        );

        $blockedBorrowerNotification = IncidentNotification::where(
            "incident_id",
            $response->json("id")
        )
            ->where("user_id", $blockedLoan->borrower_user_id)
            ->first();

        self::assertNotNull($blockedBorrowerNotification);
        self::assertEquals(
            IncidentNotificationLevel::ResolvedOnly,
            $blockedBorrowerNotification->level
        );
    }

    public function testIncidentBlockingForExtraTime_doesntNotifyPreviouslyBlockedBorrowers()
    {
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        $newBlockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addDays(2),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        \Mail::fake();

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        \Mail::assertQueued(
            LoansBlocked::class,
            fn(LoansBlocked $mail) => $mail->hasTo(
                $blockedLoan->borrowerUser->email
            )
        );

        \Mail::assertNotQueued(
            LoansBlocked::class,
            fn(LoansBlocked $mail) => $mail->hasTo(
                $newBlockedLoan->borrowerUser->email
            )
        );
        \Mail::fake();

        $this->json("PUT", "/api/v1/incidents/{$response->json("id")}/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
        ]);

        \Mail::assertNotQueued(
            LoansBlocked::class,
            fn(LoansBlocked $mail) => $mail->hasTo(
                $blockedLoan->borrowerUser->email
            )
        );

        \Mail::assertQueued(
            LoansBlocked::class,
            fn(LoansBlocked $mail) => $mail->hasTo(
                $newBlockedLoan->borrowerUser->email
            )
        );
    }

    public function testIncidentNoteAdded_notifiesSubscribedUsers()
    {
        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser,
                "loanable_id" => Loanable::factory()
                    ->withOwner($owner)
                    ->withCoowner($coowner)
                    ->create(),
            ]);

        $communityAdmin = User::factory()
            ->adminOfCommunity($ourCommunity)
            ->create();
        $globalAdmin = User::factory()->create(["role" => "admin"]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        \Mail::fake();

        $incidentId = $response->json("id");
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ]);

        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($owner->email)
        );
        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($coowner->email)
        );
        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($globalAdmin->email)
        );
        // Person reporting the incident is not notified
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($this->user->email)
        );
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($communityAdmin->email)
        );
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo(
                $blockedLoan->borrowerUser->email
            )
        );

        $this->actAs($blockedLoan->borrowerUser);

        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(200);

        $this->actAs($coowner);

        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "resolved_only",
        ])->assertStatus(200);
        $this->actAs($globalAdmin);

        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "none",
        ])->assertStatus(200);

        $this->actAs($owner);

        \Mail::fake();
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "bar",
        ])->assertStatus(201);

        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($this->user->email)
        );
        // Blocked borrower subscribed to all notifications
        \Mail::assertQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo(
                $blockedLoan->borrowerUser->email
            )
        );
        // Person adding note is not notified
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($owner->email)
        );
        // Resolved_only doesn't include notes
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($coowner->email)
        );
        // None receives no notifications
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($globalAdmin->email)
        );
        \Mail::assertNotQueued(
            NoteAdded::class,
            fn(NoteAdded $mail) => $mail->hasTo($communityAdmin->email)
        );
    }

    public function testIncidentDetailsVisibility()
    {
        $community = Community::factory()->create();
        $blockedBorrower = User::factory([
            "role" => null,
        ])
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
            "loanable_id" => Loanable::factory()
                ->withOwner($ownerUser)
                ->create(),
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
                "borrower_user_id" => $blockedBorrower,
            ]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        $incidentId = $response->json("id");
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ]);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ]);

        $this->actAs($blockedBorrower);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/$loan->loanable_id?fields=active_incidents"
        );

        $incident = $response->json("active_incidents")[0];
        self::assertTrue($incident["details_hidden"]);
        self::assertFalse(isset($incident["comments_on_incident"]));
        self::assertFalse(isset($incident["assignee"]));
        self::assertFalse(isset($incident["reported_by_user"]));
        self::assertFalse(isset($incident["notes"]));
        self::assertFalse(isset($incident["loanable"]));

        $response = $this->json(
            "GET",
            "/api/v1/loanables/$loan->loanable_id?ressource=true"
        );

        $incident = $response->json("active_incidents")[0];
        self::assertTrue($incident["details_hidden"]);
        self::assertFalse(isset($incident["comments_on_incident"]));
        self::assertFalse(isset($incident["assignee"]));
        self::assertFalse(isset($incident["reported_by_user"]));
        self::assertFalse(isset($incident["notes"]));
        self::assertFalse(isset($incident["loanable"]));

        $this->actAs($this->user);
        $this->json("PUT", "/api/v1/incidents/{$incident["id"]}/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);

        $this->actAs($blockedBorrower);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/$loan->loanable_id?fields=active_incidents.assignee,active_incidents.reported_by_user,active_incidents.notes"
        );

        $incident = $response->json("active_incidents")[0];
        self::assertFalse(isset($incident["details_hidden"]));
        self::assertTrue(isset($incident["comments_on_incident"]));
        self::assertTrue(isset($incident["assignee"]));
        self::assertTrue(isset($incident["reported_by_user"]));
        self::assertTrue(isset($incident["notes"]));
        self::assertTrue(isset($incident["loanable"]));

        $response = $this->json(
            "GET",
            "/api/v1/loanables/$loan->loanable_id?ressource=true"
        );

        $incident = $response->json("active_incidents")[0];
        self::assertFalse(isset($incident["details_hidden"]));
        self::assertTrue(isset($incident["comments_on_incident"]));
        self::assertTrue(isset($incident["assignee"]));
        self::assertTrue(isset($incident["reported_by_user"]));
        self::assertTrue(isset($incident["notes"]));
        self::assertTrue(isset($incident["loanable"]));
    }

    public function testIncidentNotificationSubscriptionPermissions()
    {
        // Blocked borrowers can only subscribe to all if show_details option is enabled
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        $incidentId = $response->json("id");

        $this->actAs($blockedLoan->borrowerUser);

        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(403);

        $this->actAs($this->user);

        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);

        $this->actAs($blockedLoan->borrowerUser);

        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(200);
    }

    public function testIncidentUpdatePermissions()
    {
        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()->create([
            "borrower_user_id" => $borrowerUser,
            "loanable_id" => Loanable::factory()
                ->withOwner($owner)
                ->withCoowner($coowner)
                ->create(),
            "departure_at" => CarbonImmutable::now(),
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        $communityAdmin = User::factory()
            ->adminOfCommunity($ourCommunity)
            ->create();
        $globalAdmin = User::factory()->create(["role" => "admin"]);

        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);
        $incidentId = $response->json("id");
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);

        $this->actAs($owner);
        $response = $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ]);
        $noteId = $response->json("id");

        // Global admin can do everything
        $this->actAs($globalAdmin);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(200);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(200);

        // Community admin cannot update description or notes if they are not the author
        $this->actAs($communityAdmin);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(403);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(403);

        // Coowners cannot update description or notes if they are not the author
        $this->actAs($coowner);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(403);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(403);

        // Owners cannot update description or notes if they are not the author
        $this->actAs($owner);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(403);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(200);

        // Borrowers cannot update description or notes if they are not the author and cannot change
        // assignee
        $this->actAs($borrowerUser);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(200);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(403);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(403);

        // Blocked borrowers and others cannot do anything
        $this->actAs($blockedLoan->borrowerUser);
        $this->json("PUT", "/api/v1/incidents/$incidentId", [
            "comments_on_incident" => "foo",
        ])->assertStatus(403);
        $this->json("POST", "/api/v1/incidents/$incidentId/note", [
            "text" => "foo",
        ])->assertStatus(403);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(403);
        $this->json("PUT", "/api/v1/incidents/$incidentId/note/$noteId", [
            "text" => "foo",
        ])->assertStatus(403);
    }
    public function testIncidentChangeAssignee_limitsToConcernedUsers()
    {
        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()->create([
            "borrower_user_id" => $borrowerUser,
            "loanable_id" => Loanable::factory()
                ->withOwner($owner)
                ->withCoowner($coowner)
                ->create(),
            "departure_at" => CarbonImmutable::now(),
        ]);

        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);

        $communityAdmin = User::factory()
            ->adminOfCommunity($ourCommunity)
            ->create();
        $globalAdmin = User::factory()->create(["role" => "admin"]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);

        $incidentId = $response->json("id");

        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $this->user->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $globalAdmin->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $borrowerUser->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $owner->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $coowner->id,
        ])->assertStatus(200);
        $this->json("PUT", "/api/v1/incidents/$incidentId/assignee", [
            "assignee_id" => $blockedLoan->borrowerUser->id,
        ])->assertStatus(422);
    }

    public function testIncidentDetailsHidden_changesNotificationForBlockedBorrowers()
    {
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);
        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);
        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);
        $incidentId = $response->json("id");
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);

        $this->actAs($blockedLoan->borrowerUser);
        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(200);

        // Blocked borrower is subscribed to all
        self::assertEquals(
            IncidentNotificationLevel::All,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );

        // Global admin disables showing details to blocked borrowers
        $this->actAs($this->user);
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => false,
        ]);

        // Blocked borrower no longer notified for all
        self::assertEquals(
            IncidentNotificationLevel::ResolvedOnly,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );

        // Reset
        $this->actAs($this->user);
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);
        $this->actAs($blockedLoan->borrowerUser);
        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(200);
        // Blocked borrower is subscribed to all
        self::assertEquals(
            IncidentNotificationLevel::All,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );

        // Global admin shortens the blocked period such that blocked borrower is no longer blocked
        $this->actAs($this->user);
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addMinute(),
            "show_details_to_blocked_borrowers" => true,
        ]);
        // Blocked borrower no longer notified for all
        self::assertEquals(
            IncidentNotificationLevel::ResolvedOnly,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );
    }

    public function testIncidentReopened_changesNotificationSettingsForBlockedBorrowers()
    {
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);
        $blockedLoan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => CarbonImmutable::now()->addHour(),
                "duration_in_minutes" => 30,
                "loanable_id" => $loan->loanable_id,
            ]);
        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);
        $incidentId = $response->json("id");
        $this->json("PUT", "/api/v1/incidents/$incidentId/block", [
            "blocking_until" => CarbonImmutable::now()->addWeek(),
            "show_details_to_blocked_borrowers" => true,
        ]);

        $this->actAs($blockedLoan->borrowerUser);
        $this->json("PUT", "/api/v1/incidents/$incidentId/subscription", [
            "level" => "all",
        ])->assertStatus(200);

        // Blocked borrower is subscribed to all
        self::assertEquals(
            IncidentNotificationLevel::All,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );

        $this->actAs($this->user);
        $this->json(
            "put",
            "/api/v1/incidents/$incidentId/complete"
        )->assertStatus(200);

        // Blocked borrower no longer subscribed to all, as they are no longer blocked
        self::assertEquals(
            IncidentNotificationLevel::ResolvedOnly,
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->pluck("level")
                ->first()
        );

        $response = $this->json(
            "put",
            "/api/v1/incidents/$incidentId/reopen"
        )->assertStatus(200);

        // Blocked_until has been cleared
        $response->assertJson([
            "blocking_until" => null,
        ]);

        // Blocked borrower no longer subscribed at all, so they can be re-subscribed if this ever
        // gets blocked again.
        self::assertFalse(
            IncidentNotification::where("incident_id", $incidentId)
                ->where("user_id", $blockedLoan->borrower_user_id)
                ->exists()
        );
    }

    public function testIncidentReopened_subscribesNewConcernedUsers()
    {
        $loan = Loan::factory()->create([
            "departure_at" => CarbonImmutable::now(),
            "duration_in_minutes" => 30,
        ]);
        $response = $this->json("POST", "/api/v1/incidents", [
            "type" => "incident",
            "incident_type" => "accident",
            "loan_id" => $loan->id,
            "loanable_id" => $loan->loanable_id,
            "comments_on_incident" => "Un gros soucis",
        ]);
        $incidentId = $response->json("id");

        // $this->user, loan borrower, loanable owner
        self::assertDatabaseCount("incident_notifications", 3);

        $coowner = LoanableUserRole::factory()->create([
            "user_id" => User::factory(),
            "ressource_id" => $loan->loanable_id,
            "ressource_type" => "loanable",
            "role" => LoanableUserRoles::Coowner,
        ]);
        $this->json(
            "put",
            "/api/v1/incidents/$incidentId/complete"
        )->assertStatus(200);
        $this->json(
            "put",
            "/api/v1/incidents/$incidentId/reopen"
        )->assertStatus(200);

        self::assertDatabaseCount("incident_notifications", 4);
        self::assertTrue(
            IncidentNotification::where("user_id", $coowner->user_id)
                ->where("incident_id", $incidentId)
                ->exists()
        );
    }

    public function testIncidentResolved_SendsEmailsToSubscribedUsers()
    {
        \Mail::fake();

        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()->create([
            "borrower_user_id" => User::factory()
                ->withCommunity($ourCommunity)
                ->withBorrower(),
            "loanable_id" => Loanable::factory()
                ->withOwner($owner)
                ->withCoowner($coowner)
                ->create(),
        ]);

        $response = $this->json("POST", "/api/v1/incidents", [
            "loan_id" => $loan->id,
            "incident_type" => "accident",
            "comments_on_incident" => "Un gros soucis",
            "loanable_id" => $loan->loanable_id,
        ]);

        $response->assertStatus(201);

        $response = $this->json(
            "put",
            "/api/v1/incidents/{$response->json("id")}/complete"
        );
        $response->assertStatus(200);

        \Mail::assertQueued(
            IncidentResolved::class,
            fn(IncidentResolved $mail) => $mail->hasTo($owner->email)
        );
        \Mail::assertQueued(
            IncidentResolved::class,
            fn(IncidentResolved $mail) => $mail->hasTo($coowner->email)
        );
        \Mail::assertQueued(
            IncidentResolved::class,
            fn(IncidentResolved $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }
}
