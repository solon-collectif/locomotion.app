<?php

namespace Tests\Integration;

use App\Enums\LoanableUserRoles;
use App\Mail\LibraryUserRoleAddedMail;
use App\Mail\LibraryUserRoleRemovedMail;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class LibraryTest extends TestCase
{
    public function testIndexReturnsPaginatedLibraries()
    {
        Library::factory()
            ->count(15)
            ->create();

        $response = $this->getJson("/api/v1/libraries?per_page=10");

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [["id", "name", "phone_number"]],
            "meta", // pagination information
            "links",
        ]);

        $response->assertJsonCount(10, "data");
    }

    public function testStoreCreatesNewLibrary()
    {
        $user = User::factory()->create();

        $data = [
            "name" => "Test Library",
            "phone_number" => "123456789",
            "owner_user_id" => $user->id,
        ];

        $response = $this->postJson("/api/v1/libraries", $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure(["id", "name", "phone_number"]);

        $this->assertDatabaseHas("libraries", ["name" => "Test Library"]);
        $this->assertDatabaseHas("loanable_user_roles", [
            "user_id" => $user->id,
            "role" => "owner",
        ]);
    }

    public function testChangeOwnerUpdatesLibraryOwner()
    {
        $oldOwner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($oldOwner)
            ->create();
        $newOwner = User::factory()->create();

        $currentOwnerRole = $library->userRoles()->first();

        \Mail::fake();
        $response = $this->putJson(
            "/api/v1/loanables/roles/{$currentOwnerRole->id}",
            [
                "user_id" => $newOwner->id,
                "role" => "owner",
            ]
        );

        $response->assertStatus(201);

        $this->assertDatabaseMissing("loanable_user_roles", [
            "role" => "owner",
            "user_id" => $oldOwner->id,
        ]);
        $this->assertDatabaseHas("loanable_user_roles", [
            "role" => "owner",
            "user_id" => $newOwner->id,
        ]);
        \Mail::assertQueued(
            LibraryUserRoleAddedMail::class,
            fn(LibraryUserRoleAddedMail $mail) => $mail->hasTo($newOwner->email)
        );
        \Mail::assertQueued(
            LibraryUserRoleRemovedMail::class,
            fn(LibraryUserRoleRemovedMail $mail) => $mail->hasTo(
                $oldOwner->email
            )
        );
    }

    public function testAddLoanableToLibraryAsOwner()
    {
        $user = User::factory()->create();
        $library = Library::factory()
            ->withOwner($user)
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->create();

        $this->actAs($user);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/loanables/{$loanable->id}"
        );

        $response->assertStatus(200);

        $this->assertDatabaseHas("loanables", [
            "library_id" => $library->id,
        ]);
    }

    public function testRemoveLoanableFromLibraryAsOwner()
    {
        $user = User::factory()->create();
        $library = Library::factory()
            ->withOwner($user)
            ->create();
        $loanable = Loanable::factory()
            ->for($library)
            ->create();

        $this->actAs($user);

        $response = $this->deleteJson(
            "/api/v1/libraries/{$library->id}/loanables/{$loanable->id}"
        );

        $response->assertStatus(200);

        $this->assertDatabaseMissing("loanables", [
            "library_id" => $library->id,
        ]);
    }

    public function testShowReturnsLibraryDetails()
    {
        $library = Library::factory()->create();

        $response = $this->getJson("/api/v1/libraries/{$library->id}");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(["id", "name", "phone_number"]);
    }

    public function testUpdateEditsLibraryDetails()
    {
        $library = Library::factory()->create();

        $data = [
            "name" => "Updated Library Name",
            "phone_number" => "987654321",
        ];

        $response = $this->putJson("/api/v1/libraries/{$library->id}", $data);

        $response->assertStatus(200)->assertJsonFragment([
            "name" => "Updated Library Name",
            "phone_number" => "987654321",
        ]);

        $this->assertDatabaseHas("libraries", [
            "name" => "Updated Library Name",
            "phone_number" => "987654321",
        ]);
    }

    public function testDestroyDeletesLibraryAndMovesLoanableToOwner()
    {
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        $loanableA = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);
        $loanableB = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);

        $response = $this->deleteJson("/api/v1/libraries/{$library->id}");

        $response->assertStatus(200);

        $this->assertDatabaseMissing("libraries", ["id" => $library->id]);
        $this->assertDatabaseMissing("loanable_user_roles", [
            "ressource_id" => $library->id,
            "ressource_type" => "library",
        ]);
        $loanableA->refresh();
        $loanableB->refresh();
        self::assertNull($loanableA->library_id);
        self::assertNull($loanableB->library_id);
        self::assertEquals($owner->id, $loanableA->owner_user->id);
        self::assertEquals($owner->id, $loanableB->owner_user->id);
    }

    public function testCreateManagerRoleAsOwner()
    {
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        $user = User::factory()->create();

        $data = [
            "user_id" => $user->id,
            "role" => "manager",
            "pays_loan_insurance" => true,
            "pays_loan_price" => false,
        ];

        $this->actingAs($owner);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/roles",
            $data
        );

        $response
            ->assertStatus(201)
            ->assertJsonStructure(["id", "user", "role"]);

        $this->assertDatabaseHas("loanable_user_roles", [
            "user_id" => $user->id,
            "role" => "manager",
        ]);
    }

    public function testCreateManagerRoleAsOwner_failsIfRoleAlreadyAssigned()
    {
        $owner = User::factory()->create();
        $manager = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        LoanableUserRole::factory()->create([
            "ressource_id" => $library->id,
            "ressource_type" => "library",
            "user_id" => $manager->id,
            "role" => "manager",
        ]);

        $data = [
            "user_id" => $manager->id,
            "role" => "manager",
            "pays_loan_insurance" => true,
            "pays_loan_price" => false,
        ];

        $this->actingAs($owner);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/roles",
            $data
        )
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le-a membre a déjà ce rôle pour ce véhicule.",
            ]);
    }

    public function testCreateManagerRoleAsOwner_canRemoveManagerCoownerRoles()
    {
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        $user = User::factory()->create();
        $loanable = Loanable::factory()
            ->withCoowner($user)
            ->create([
                "library_id" => $library->id,
            ]);

        $this->assertDatabaseHas("loanable_user_roles", [
            "user_id" => $user->id,
            "role" => "coowner",
        ]);

        $data = [
            "user_id" => $user->id,
            "role" => "manager",
            "pays_loan_insurance" => true,
            "pays_loan_price" => false,
            "remove_existing_coowner_roles" => true,
        ];

        $this->actingAs($owner);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/roles",
            $data
        );

        $response
            ->assertStatus(201)
            ->assertJsonStructure(["id", "user", "role"]);

        $this->assertDatabaseHas("loanable_user_roles", [
            "user_id" => $user->id,
            "role" => "manager",
        ]);
        $this->assertDatabaseMissing("loanable_user_roles", [
            "user_id" => $user->id,
            "role" => "coowner",
        ]);
    }
    public function testUpdatingLibraryUserRoleProtectedAttributesHasNoEffect()
    {
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        $user = User::factory()->create();

        // Create a library user role instance
        $role = LoanableUserRole::factory()->create([
            "ressource_id" => $library->id,
            "ressource_type" => "library",
            "user_id" => $user->id,
            "role" => LoanableUserRoles::Manager,
        ]);

        $otherLibrary = Library::factory()->create();
        $otherUser = User::factory()->create();

        $this->actAs($owner);

        // Attempt to update attributes
        $data = [
            "ressource_id" => $otherLibrary->id,
            "role" => LoanableUserRoles::Owner,
        ];

        $response = $this->putJson(
            "/api/v1/loanables/roles/{$role->id}",
            $data
        );

        $response->assertStatus(200);

        // Assert the role remains unchanged in the database
        $this->assertDatabaseHas("loanable_user_roles", [
            "id" => $role->id,
            "ressource_id" => $library->id,
            "user_id" => $user->id,
            "role" => LoanableUserRoles::Manager,
        ]);
    }

    public function testLibraryManagerCannotAddLoanables()
    {
        $manager = User::factory()->create();
        $library = Library::factory()
            ->withManager($manager)
            ->create();
        $loanable = Loanable::factory()->create();

        $this->actAs($manager);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/loanables/{$loanable->id}"
        );

        $response->assertStatus(403);

        $this->assertDatabaseMissing("loanables", [
            "library_id" => $library->id,
        ]);
    }

    public function testLibraryManagerCannotRemoveLoanables()
    {
        $manager = User::factory()->create();
        $library = Library::factory()
            ->withManager($manager)
            ->create();
        $loanable = Loanable::factory()
            ->for($library)
            ->create();

        $this->actAs($manager);

        $response = $this->deleteJson(
            "/api/v1/libraries/{$library->id}/loanables/{$loanable->id}"
        );

        $response->assertStatus(403);

        $this->assertDatabaseHas("loanables", [
            "library_id" => $library->id,
        ]);
    }

    public function testLibraryManagerCannotChangeOwner()
    {
        $manager = User::factory()->create();
        $library = Library::factory()
            ->withOwner(User::factory()->create())
            ->withManager($manager)
            ->create();
        $newOwner = User::factory()->create();

        $this->actAs($manager);

        $currentOwnerRole = LoanableUserRole::first();

        $response = $this->putJson(
            "/api/v1/loanables/roles/{$currentOwnerRole->id}",
            [
                "user_id" => $newOwner->id,
                "role" => "owner",
            ]
        );

        $response->assertStatus(403);

        $this->assertDatabaseMissing("loanable_user_roles", [
            "role" => "owner",
            "user_id" => $newOwner->id,
        ]);
    }

    public function testLibraryManagerCannotAddUserRoles()
    {
        $manager = User::factory()->create();
        $library = Library::factory()
            ->withOwner(User::factory()->create())
            ->withManager($manager)
            ->create();
        $newUser = User::factory()->create();

        $this->actAs($manager);

        $data = [
            "user_id" => $newUser->id,
            "role" => "manager",
        ];

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/roles",
            $data
        );

        $response->assertStatus(403);

        $this->assertDatabaseMissing("loanable_user_roles", [
            "user_id" => $newUser->id,
            "role" => "manager",
        ]);
    }

    public function testLibraryManagerCannotRemoveUserRoles()
    {
        $manager = User::factory()->create();
        $otherManager = User::factory()->create();
        $library = Library::factory()
            ->withManager($manager)
            ->create();

        $otherManagerRole = $library->userRoles()->make([]);
        $otherManagerRole->user_id = $otherManager->id;
        $otherManagerRole->role = LoanableUserRoles::Manager;
        $otherManagerRole->granted_by_user_id = $this->user->id;
        $otherManagerRole->save();

        \Log::error($otherManagerRole->id);

        $this->actAs($manager);

        $response = $this->deleteJson(
            "/api/v1/loanables/roles/{$otherManagerRole->id}"
        );

        $response->assertStatus(403);

        $this->assertDatabaseHas("loanable_user_roles", [
            "user_id" => $otherManager->id,
            "role" => "manager",
        ]);
    }
    public function testLibraryOwnerCannotChangeOwner()
    {
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();
        $newOwner = User::factory()->create();

        $this->actAs($owner);

        $currentOwnerRole = LoanableUserRole::first();

        $response = $this->putJson(
            "/api/v1/loanables/roles/{$currentOwnerRole->id}",
            [
                "user_id" => $newOwner->id,
                "role" => "owner",
            ]
        );

        $response->assertStatus(403);

        $this->assertDatabaseMissing("loanable_user_roles", [
            "role" => "owner",
            "user_id" => $newOwner->id,
        ]);
    }

    public function testLibraryOwnerCanAddLibraryToApprovedCommunity()
    {
        $community = Community::factory()->create();
        $owner = User::factory()
            ->withCommunity($community)
            ->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();

        $this->actAs($owner);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/communities/{$community->id}"
        );

        $response->assertStatus(200);

        $this->assertDatabaseHas("community_library", [
            "community_id" => $community->id,
            "library_id" => $library->id,
        ]);
    }

    public function testLibraryOwnerCannotAddLibraryToNonApprovedCommunity()
    {
        $community = Community::factory()->create(); // owner not approved
        $owner = User::factory()->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();

        $this->actAs($owner);

        $response = $this->postJson(
            "/api/v1/libraries/{$library->id}/communities/{$community->id}"
        );

        $response->assertStatus(403);

        $this->assertDatabaseMissing("community_library", [
            "community_id" => $community->id,
            "library_id" => $library->id,
        ]);
    }

    public function testLibraryOwnerCanRemoveLibraryFromApprovedCommunity()
    {
        $community = Community::factory()->create();
        $owner = User::factory()
            ->withCommunity($community)
            ->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->create();

        // Add the library to the community
        $library->communities()->attach($community);

        $this->actAs($owner);

        $response = $this->deleteJson(
            "/api/v1/libraries/{$library->id}/communities/{$community->id}"
        );

        $response->assertStatus(200);

        $this->assertDatabaseMissing("community_library", [
            "community_id" => $community->id,
            "library_id" => $library->id,
        ]);
    }

    public function testRemoveLibraryOwnerFromCommunity_removesLibraryFromCommunity()
    {
        $community = Community::factory()->create();
        $owner = User::factory()
            ->withCommunity($community)
            ->create();
        $library = Library::factory()
            ->withOwner($owner)
            ->withCommunity($community)
            ->create();

        self::assertJson(1, $library->communities()->count());
        $communityUser = $owner->communities->first()->pivot;

        $this->putJson(
            "/api/v1/communityUsers/{$communityUser->id}/suspend"
        )->assertStatus(200);

        $library->refresh();
        self::assertJson(0, $library->communities()->count());
    }

    public function testViewLoanables_filtersLoanables()
    {
        $nonLibraryLoanable = Loanable::factory()->create();
        $library = Library::factory()->create();
        $libraryLoanable = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);

        $response = $this->getJson("/api/v1/libraries/$library->id/loanables");
        $response->assertStatus(200);
        $response
            ->assertJsonFragment([
                "id" => $libraryLoanable->id,
            ])
            ->assertJsonMissing([
                "id" => $nonLibraryLoanable->id,
            ])
            ->assertJsonMissing([
                "loans" => [],
            ]);
    }

    public function testViewLoanables_withLoanSummary()
    {
        $library = Library::factory()
            ->withOwner()
            ->create();
        $libraryLoanable = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);
        $recentlyCompletedLoan = Loan::factory()
            ->paid()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->subMinutes(90)
                    ->toDateTimeString(),
                "duration_in_minutes" => 60,
                "loanable_id" => $libraryLoanable,
            ]);
        $oldLoan = Loan::factory()
            ->paid()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->subDay()
                    ->toDateTimeString(),
                "duration_in_minutes" => 60,
                "loanable_id" => $libraryLoanable,
            ]);
        $futureLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addHour()
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);
        $canceledLoan = Loan::factory()
            ->canceled()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addMinutes(15)
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);
        $wayFutureLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addWeeks(2)
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);

        $response = $this->getJson(
            "/api/v1/libraries/$library->id/loanables?with_loan_summary=1"
        );
        $response->assertStatus(200);
        $response
            ->assertJson([
                "data" => [
                    [
                        "id" => $libraryLoanable->id,
                        "loans" => [
                            [
                                "id" => $recentlyCompletedLoan->id,
                            ],
                            [
                                "id" => $futureLoan->id,
                            ],
                        ],
                    ],
                ],
            ])
            ->assertJsonCount(2, "data.0.loans");
    }

    public function testViewCommunities_filtersLibraryCommunities()
    {
        $otherCommunity = Community::factory()->create();
        $libraryCommunity = Community::factory()->create();
        $library = Library::factory()
            ->withCommunity($libraryCommunity)
            ->create();

        $response = $this->getJson(
            "/api/v1/libraries/$library->id/communities"
        );
        $response->assertStatus(200);
        $response->assertJson([
            "data" => [
                [
                    "id" => $libraryCommunity->id,
                ],
            ],
        ]);
        $response->assertJsonCount(1, "data");
    }

    public function testViewUserRoles_filtersForLibrary()
    {
        $library = Library::factory()->create();
        $otherLibrary = Library::factory()->create();

        $libraryUserRole = LoanableUserRole::factory()->create([
            "ressource_id" => $library->id,
            "ressource_type" => "library",
        ]);

        LoanableUserRole::factory()->create([
            "ressource_id" => $library->id,
            "ressource_type" => "loanable",
        ]);
        LoanableUserRole::factory()->create([
            "ressource_id" => $otherLibrary->id,
            "ressource_type" => "library",
        ]);

        $response = $this->getJson("/api/v1/libraries/$library->id/roles");
        $response->assertStatus(200);
        $response->assertJson([
            "data" => [
                [
                    "id" => $libraryUserRole->id,
                ],
            ],
        ]);
        $response->assertJsonCount(1, "data");
    }

    public function testGetLibraryEvents()
    {
        $library = Library::factory()
            ->withOwner()
            ->create();
        $libraryLoanable = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);
        $otherLibraryLoanable = Loanable::factory()->create([
            "library_id" => $library->id,
        ]);
        $nonLibraryLoanable = Loanable::factory()
            ->withOwner()
            ->create([]);
        $recentlyCompletedLoan = Loan::factory()
            ->paid()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->subMinutes(90)
                    ->toDateTimeString(),
                "duration_in_minutes" => 60,
                "loanable_id" => $libraryLoanable,
            ]);
        $oldLoan = Loan::factory()
            ->paid()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->subDays(4)
                    ->toDateTimeString(),
                "duration_in_minutes" => 60,
                "loanable_id" => $libraryLoanable,
            ]);
        $futureLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addHour()
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);
        $canceledLoan = Loan::factory()
            ->canceled()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addMinutes(15)
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);
        $wayFutureLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addWeeks(2)
                    ->toDateTimeString(),
                "loanable_id" => $libraryLoanable,
            ]);
        $otherLibraryLoanableLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addMinutes(90)
                    ->toDateTimeString(),
                "loanable_id" => $otherLibraryLoanable,
            ]);
        $nonLibraryLoanableLoan = Loan::factory()
            ->accepted()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addMinutes(75)
                    ->toDateTimeString(),
                "loanable_id" => $nonLibraryLoanable,
            ]);

        $start = CarbonImmutable::now()
            ->subDays(2)
            ->toDateTimeString();
        $end = CarbonImmutable::now()
            ->addDays(2)
            ->toDateTimeString();
        $response = $this->getJson(
            "/api/v1/libraries/$library->id/events?start=$start&end=$end"
        );

        $response->assertStatus(200)->assertJson([
            [
                "data" => ["loan_id" => $recentlyCompletedLoan->id],
            ],
            [
                "data" => ["loan_id" => $futureLoan->id],
            ],
            [
                "data" => ["loan_id" => $otherLibraryLoanableLoan->id],
            ],
        ]);
        $response->assertJsonCount(3);

        $response = $this->getJson(
            "/api/v1/libraries/$library->id/events?start=$start&end=$end&loanable_id=$otherLibraryLoanable->id"
        );

        $response->assertStatus(200)->assertJson([
            [
                "data" => ["loan_id" => $otherLibraryLoanableLoan->id],
            ],
        ]);
        $response->assertJsonCount(1);
    }
}
