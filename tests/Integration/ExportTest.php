<?php

namespace Tests\Integration;

use App\Exports\BaseExport;
use App\Exports\LoanableExport;
use App\Exports\tests\SmallBatchExport;
use App\Http\Middleware\WrapInTransaction;
use App\Jobs\AppendExportToFile;
use App\Jobs\NotifyUserExportCompleted;
use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use File;
use Mockery;
use Queue;
use Storage;
use Tests\TestCase;

class ExportTest extends TestCase
{
    public function testExport_defaultsToOrderById()
    {
        Storage::fake();
        $userId = User::max("id");
        $userData = [
            [
                "id" => $userId + 1,
                "name" => "foo",
            ],
            [
                "id" => $userId + 2,
                "name" => "bar",
            ],
            [
                "id" => $userId + 3,
                "name" => "baz",
            ],
            [
                "id" => $userId + 4,
                "name" => "quux",
            ],
            [
                "id" => $userId + 5,
                "name" => "quuz",
            ],
        ];

        $shuffledUserData = $userData;
        shuffle($shuffledUserData);

        User::factory()
            ->sequence(...$shuffledUserData)
            ->count(5)
            ->create();

        $minUserId = $userId + 1;
        $maxUserId = $userId + 5;

        $response = $this->get(
            "/api/v1/users?fields=id,name&id=$minUserId:$maxUserId",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            $userData
        );
    }

    public function testExport_respectsOrder()
    {
        Storage::fake();
        $userId = User::max("id");
        $userData = [
            [
                "id" => $userId + 5,
                "name" => "a",
            ],
            [
                "id" => $userId + 4,
                "name" => "b",
            ],
            [
                "id" => $userId + 3,
                "name" => "c",
            ],
            [
                "id" => $userId + 2,
                "name" => "d",
            ],
            [
                "id" => $userId + 1,
                "name" => "e",
            ],
        ];

        $minUserId = $userId + 1;
        $maxUserId = $userId + 5;

        $shuffledUserData = $userData;
        shuffle($shuffledUserData);

        User::factory()
            ->sequence(...$shuffledUserData)
            ->count(5)
            ->create();

        $response = $this->get(
            "/api/v1/users?fields=id,name&id=$minUserId:$maxUserId&order=name",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            $userData
        );
    }

    public function testExport_serializesObjectValues()
    {
        Storage::fake();
        $communityId = Community::max("id") + 1;
        $community = Community::factory()->create([
            "id" => $communityId,
            "area" => [
                "type" => "MultiPolygon",
                "coordinates" => [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
            ],
        ]);

        self::assertIsObject($community->area);

        $response = $this->get(
            "/api/v1/communities?fields=area&id=$communityId",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["area"],
            [
                [
                    '{"type":"MultiPolygon","coordinates":[[[[1,2],[3,4],[5,6],[1,2]]]]}',
                ],
            ]
        );
    }

    public function testExport_serializesArrayValues()
    {
        Storage::fake();
        $loanableId = Loanable::max("id") + 1;
        $loanable = Loanable::factory()->create([
            "id" => $loanableId,
            "position" => [2, 3],
        ]);

        Loanable::$export = BaseExport::class;

        $response = $this->get(
            "/api/v1/loanables?fields=position&id=$loanableId",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["position"],
            [["[2,3]"]]
        );
    }

    public function testExport_enumeratesRelationColumns()
    {
        Storage::fake();
        $userId = User::max("id") + 1;
        $user = User::factory()->create([
            "id" => $userId,
        ]);
        $communityA = Community::factory()->create(["name" => "CommunityA"]);
        $communityB = Community::factory()->create(["name" => "CommunityB"]);
        $user->communities()->attach($communityA);
        $user->communities()->attach($communityB);

        $secondUserId = $userId + 1;
        User::factory()
            ->create([
                "id" => $userId + 1,
            ])
            ->communities()
            ->attach($communityB);

        $response = $this->get(
            "/api/v1/users?fields=communities.name&id=$userId,$secondUserId",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["communities.0.name", "communities.1.name"],
            [["CommunityA", "CommunityB"], ["CommunityB", ""]]
        );
    }

    public function testExport_appendsPivotFields()
    {
        Storage::fake();
        $id = User::max("id") + 1;
        $user = User::factory()->create([
            "id" => $id,
        ]);
        $communityA = Community::factory()->create(["name" => "CommunityA"]);
        $user->communities()->attach($communityA, [
            "approved_at" => "2023-05-05T12:12:12.000000Z",
        ]);

        $response = $this->get(
            // Accessing the community name from the pivot user->communityUser->community relation,
            // for testing purposes.
            "/api/v1/users?fields=communities.community.name,communities.approved_at&id=$id",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(201)->assertJson([
            "status" => "completed",
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["communities.0.community.name", "communities.0.approved_at"],
            [["CommunityA", "2023-05-05T12:12:12.000000Z"]]
        );
    }

    public function testExport_dispatchesToQueueForLargeExports()
    {
        Storage::fake();
        User::factory()
            ->count(10)
            ->create();

        // SmallBatchExport creates batches with 6 items
        User::$export = SmallBatchExport::class;
        Queue::fake();
        $response = $this->get("/api/v1/users?fields=id", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(201)->assertJson([
            "status" => "in_process",
        ]);

        Queue::assertPushedWithChain(
            AppendExportToFile::class,
            [AppendExportToFile::class, NotifyUserExportCompleted::class],
            fn(AppendExportToFile $export) => $export->getJobNumber() === 0
        );
    }

    public function testQueuedExport_hasExpectedRows()
    {
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();

        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;
        $response = $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(201)->assertJson([
            "status" => "in_process",
        ]);

        // The queue is sync in tests, so it should have all run
        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            [
                [0, "c_0"],
                [1, "c_1"],
                [2, "c_2"],
                [3, "c_3"],
                [4, "c_4"],
                [5, "c_5"],
                [6, "c_6"],
                [7, "c_7"],
                [8, "c_8"],
                [9, "c_9"],
            ]
        );
    }

    public function testExport_cleansUpTemporaryFile()
    {
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();

        $filename = "";
        $assertion = File::partialMock()
            ->shouldReceive("delete")
            ->twice()
            ->with(Mockery::capture($filename))
            ->passthru();

        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;
        $response = $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(201)->assertJson([
            "status" => "in_process",
        ]);

        self::assertStringStartsWith(
            storage_path("framework/exports/"),
            $filename
        );
        $assertion->verify();
        self::assertFileDoesNotExist($filename);
    }

    public function testExport_cleansUpTemporaryFileOnError()
    {
        $this->withoutMiddleware(WrapInTransaction::class);
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();
        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;

        $filename = "";
        $assertion = File::partialMock()
            ->shouldReceive("delete")
            ->once()
            ->with(Mockery::capture($filename))
            ->passthru();

        // Initial empty file created on cloud
        Storage::shouldReceive("put")->andReturn(true);
        // File::updateFilePath on save
        Storage::shouldReceive("exists")->andReturn(true);
        Storage::shouldReceive("delete")->andReturn(true);

        // Failing to update the cloud file
        Storage::shouldReceive("putFileAs")->andReturn(false);

        $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ])->assertStatus(500);

        self::assertStringStartsWith(
            storage_path("framework/exports/"),
            $filename
        );
        $assertion->verify();
        self::assertFileDoesNotExist($filename);
    }

    private static function assertCsvMatches(
        array $fileData,
        array $headers,
        array $expectedValues
    ) {
        $filePath = \App\Models\File::find($fileData["id"])->full_path;
        Storage::assertExists($filePath);
        $data = Storage::get($filePath);

        // reading line by line
        $stream = fopen("php://memory", "r+");
        fwrite($stream, $data);
        rewind($stream);
        self::assertCsvLine($stream, $headers);
        foreach ($expectedValues as $expectedValue) {
            self::assertCsvLine($stream, array_values($expectedValue));
        }
        self::assertEndOfCsv($stream);

        fclose($stream);
    }

    private static function assertCsvLine($stream, array $values): void
    {
        $csvLine = fgetcsv($stream);
        self::assertNotFalse($csvLine);
        self::assertEquals($values, $csvLine);
    }

    private static function assertEndOfCsv($stream): void
    {
        self::assertFalse(fgetcsv($stream));
    }
}
