<?php

namespace Tests\Integration;

use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Tests\TestCase;

class GbfsTest extends TestCase
{
    public function testCreateGbfsDataset()
    {
        $this->assertDatabaseCount("gbfs_datasets", 0);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "newStream",
        ]);

        $this->assertDatabaseCount("gbfs_datasets", 1);
    }

    public function testUpdateGbfsDataset()
    {
        $this->assertDatabaseCount("gbfs_datasets", 0);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);

        $this->putJson("/api/v1/gbfs_datasets/foo", [
            "name" => "bar",
        ]);

        $this->assertDatabaseCount("gbfs_datasets", 1);
        $this->assertEquals("bar", DB::table("gbfs_datasets")->first()->name);
    }

    public function testDestroyGbfsDataset()
    {
        $this->assertDatabaseCount("gbfs_datasets", 0);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->assertDatabaseCount("gbfs_datasets", 1);

        $this->deleteJson("/api/v1/gbfs_datasets/foo");

        $this->assertDatabaseCount("gbfs_datasets", 0);
    }

    public function testAddGbfsDatasetCommunity()
    {
        $this->assertDatabaseCount("gbfs_dataset_communities", 0);

        $community = Community::factory()->create();
        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);

        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);

        $this->assertDatabaseCount("gbfs_dataset_communities", 1);
    }
    public function testRemoveGbfsDatasetCommunity()
    {
        $this->assertDatabaseCount("gbfs_dataset_communities", 0);
        $community = Community::factory()->create();
        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $id = $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ])->json("id");
        $this->assertDatabaseCount("gbfs_dataset_communities", 1);

        $this->deleteJson("/api/v1/gbfs_dataset_communities/$id");

        $this->assertDatabaseCount("gbfs_dataset_communities", 1);
    }

    public function testLoanableVisibleInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => true,
                "availability_mode" => "always",
                "position" => [50, 12],
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(1, $vehicles["data"]["vehicles"]);
        self::assertEquals(50, $vehicles["data"]["vehicles"][0]["lat"]);
        self::assertEquals(12, $vehicles["data"]["vehicles"][0]["lon"]);
    }

    public function testUnpublishedLoanableNotVisibleInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => false,
                "shared_publicly" => true,
                "availability_mode" => "always",
                "position" => [50, 12],
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(0, $vehicles["data"]["vehicles"]);
    }

    public function testUnavailableLoanableNotVisibleInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => true,
                "availability_mode" => "never",
                "position" => [50, 12],
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(0, $vehicles["data"]["vehicles"]);
    }

    public function testNotSharedLoanableNotVisibleInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => false,
                "availability_mode" => "always",
                "position" => [50, 12],
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(0, $vehicles["data"]["vehicles"]);
    }

    public function testLoanedLoanableNotVisibleInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => true,
                "availability_mode" => "always",
                "position" => [50, 12],
            ]);

        Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->subMinutes(5),
                "duration_in_minutes" => 15,
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(0, $vehicles["data"]["vehicles"]);
    }

    public function testLoanablesAvailableUntilNextLoanInGbfsStream()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => true,
                "availability_mode" => "always",
                "position" => [50, 12],
            ]);

        $nextLoanStart = Carbon::now()->addHour();

        Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => $nextLoanStart,
                "duration_in_minutes" => 15,
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(1, $vehicles["data"]["vehicles"]);
        self::assertEquals(
            $nextLoanStart->toIso8601String(),
            $vehicles["data"]["vehicles"][0]["available_until"]
        );
    }

    public function testLoanablesNextAvailablityInGbfsStream()
    {
        self::setTestNow(new Carbon("2024-04-15T13:00", "America/Toronto"));
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar([
                "engine" => "fuel",
            ])
            ->create([
                "published" => true,
                "shared_publicly" => true,
                "availability_mode" => "always",
                "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO"],
    "period": "17:00-22:00"
  }
]
JSON
                ,
                "position" => [50, 12],
                "timezone" => "America/Toronto",
            ]);

        $this->putJson("/api/v1/gbfs_datasets", [
            "name" => "foo",
        ]);
        $this->putJson("/api/v1/gbfs_dataset_communities", [
            "gbfs_dataset_name" => "foo",
            "community_id" => $community->id,
        ]);
        $link = $this->getJson("/api/v1/gbfs_datasets/foo")->json("link");

        $vehicles = $this->getJson($link . "/vehicle_status")->json();

        self::assertCount(1, $vehicles["data"]["vehicles"]);
        self::assertEquals(
            "2024-04-15T21:00:00+00:00", //  17:00 in America/Toronto
            $vehicles["data"]["vehicles"][0]["available_until"]
        );
    }
}
