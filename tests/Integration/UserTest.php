<?php

namespace Tests\Integration;

use App\Events\RegistrationSubmittedEvent;
use App\Facades\Kiwili;
use App\Mail\UserBalanceClaimReceived;
use App\Mail\UserClaimedBalance;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\File;
use App\Models\Image;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\PaymentMethod;
use App\Models\Payout;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Testing\Assert;
use Mockery;
use Stripe;
use Tests\TestCase;
use Tests\TraversableIsSimilar;

class UserTest extends TestCase
{
    private static $userResponseStructure = [
        "id",
        "name",
        "email",
        "email_verified_at",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
    ];

    private static $getCommunityResponseStructure = [
        "id",
        "name",
        "description",
        "area",
        "created_at",
        "updated_at",
        "deleted_at",
        "type",
        "center",
    ];

    public function testOrderUsersById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByFullName()
    {
        $data = [
            "order" => "full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByEmail()
    {
        $data = [
            "order" => "email",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "id" => "3",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCreatedAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "full_name" => "Ariane",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByIsDeleted()
    {
        // Zero integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => 0,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $notDeactivatedResponseContent = $response->baseResponse->getContent();

        // Positive integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => 1,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $deactivatedResponseContent = $response->baseResponse->getContent();

        // Boolean false
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => false,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $notDeactivatedResponseContent,
            $response->baseResponse->getContent()
        );

        // Boolean true
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => true,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $deactivatedResponseContent,
            $response->baseResponse->getContent()
        );
    }

    public function testFilterUsersByCommunityId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.id" => "3",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCommunityName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.name" => "Patrie",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testSearchUsers()
    {
        User::factory()->create([
            "name" => "First name",
        ]);
        User::factory()->create([
            "name" => "Other name",
        ]);

        $data = [
            "q" => "rst NA",
        ];
        $response = $this->json(
            "GET",
            route("users.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testCreateUsers()
    {
        $data = [
            "accept_conditions" => true,
            "name" => $this->faker->name,
            "last_name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" =>
                '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            "description" => null,
            "date_of_birth" => null,
            "address" => "",
            "postal_code" => "",
            "phone" => "",
            "is_smart_phone" => false,
            "other_phone" => "",
            "remember_token" => Str::random(10),
        ];

        $response = $this->json("POST", "/api/v1/users", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "accept_conditions",
                "created_at",
                "updated_at",
            ]);
    }

    public function testCreateUsersWithSimilarEmails()
    {
        $user = User::factory()
            ->make()
            ->toArray();
        $user["password"] = "12354123124";

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(201);

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);

        // Case insensitivity
        $user["email"] =
            strtoupper($user["email"][0]) . substr($user["email"], 1);
        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);
    }

    public function testShowUsers()
    {
        $user = User::factory()->create();
        $data = [];

        $response = $this->json("GET", "/api/v1/users/$user->id", $data);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "email_verified_at",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "remember_token",
                "created_at",
                "updated_at",
                "role",
                "full_name",
            ]);
    }

    public function testUpdateUsers()
    {
        $user = User::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $this->actAs($user);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testUpdateUsersWithNonexistentId()
    {
        $user = User::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/users/208084082084", $data);

        $response->assertStatus(404);
    }

    public function testDeleteUser_AuthorizedAsGlobalAdmin()
    {
        $userToDeactivate = User::factory()->create();

        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );
        $response->assertStatus(200);
    }

    public function testDeleteUser_AuthorizedAsSelf()
    {
        $userToDeactivate = User::factory()->create();

        $this->actAs($userToDeactivate);

        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );
        $response->assertStatus(200);
    }

    public function testDeleteUser_UnauthorizedAsRegularUser()
    {
        $normalUser = User::factory()->create();
        $this->actAs($normalUser);

        $userToDeactivate = User::factory()->create();
        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );

        $response->assertStatus(403);
    }

    public function testDeleteUser_UnauthorizedAsAdminOfCommunity()
    {
        $community = Community::factory()->create();

        $communityAdmin = User::factory()->create();
        $communityAdmin->communities()->attach($community->id, [
            "role" => "admin",
        ]);
        $this->actAs($communityAdmin);

        $userToDeactivate = User::factory()->create();
        $userToDeactivate->communities()->attach($community->id, [
            "role" => "regular",
        ]);
        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );

        $response->assertStatus(403);
    }

    public function testDeleteUsers_failsWithNonexistentId()
    {
        $response = $this->json("DELETE", "/api/v1/users/0280398420384");

        $response->assertStatus(404);
    }

    public function testDeleteUsers_mustNotHaveOngoingLoansAsOwner()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                [
                    "L'utilisateur ne doit pas avoir d'emprunts en cours en tant que propriétaire ou emprunteur.",
                ],
            ],
        ]);

        $loan->setLoanStatusCanceled();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(200);
    }

    public function testDeleteUsers_mustNotHaveOngoingLoansAsBorrower()
    {
        $user = User::factory()
            ->withBorrower()
            ->withCommunity()
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $user->id,
            ]);

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                [
                    "L'utilisateur ne doit pas avoir d'emprunts en cours en tant que propriétaire ou emprunteur.",
                ],
            ],
        ]);

        $loan->setLoanStatusCanceled();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(200);
    }

    public function testDeleteUsers_mustNotHaveBalance()
    {
        $user = User::factory([
            "balance" => 10.0,
        ])->create();

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["L'utilisateur ne doit pas avoir un solde dans son compte."],
            ],
        ]);
    }

    public function testDeleteUserData()
    {
        $user = User::factory()
            ->withCommunity()
            ->create([
                "name" => "Foo",
                "last_name" => "Bar",
                "email" => "foo@bar.com",
                "description" => "foo bar foo bar",
                "date_of_birth" => "2023-03-12",
                "address" => "123 foo",
                "postal_code" => "F0O8A3",
                "phone" => "123245544",
                "other_phone" => "9876543211",
                "role" => "admin",
                "meta" => [
                    "foo" => "bar",
                ],
                "bank_account_number" => 1231,
                "bank_institution_number" => 12312,
                "bank_transit_number" => 1232121,
                "address_position" => [45.537573, -73.601417],
            ]);

        $avatar = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        self::assertTrue(Storage::exists($avatar->full_path));

        $residencyProof = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->id,
                "fileable_type" => "user",
                "field" => "residency_proof",
            ]);

        self::assertTrue(Storage::exists($residencyProof->full_path));

        $identityProof = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->id,
                "fileable_type" => "user",
                "field" => "identity_proof",
            ]);

        self::assertTrue(Storage::exists($identityProof->full_path));

        $communityUser = $user->main_community->pivot;

        $customProof = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $communityUser->id,
                "fileable_type" => "communityUser",
                "field" => "custom_proof",
            ]);

        self::assertTrue(Storage::exists($customProof->full_path));

        $borrower = Borrower::factory()->create([
            "user_id" => $user,
            "drivers_license_number" => 999999999,
        ]);

        $insurance = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $borrower->id,
                "fileable_type" => "borrower",
                "field" => "insurance",
            ]);

        self::assertTrue(Storage::exists($insurance->full_path));

        $saaq = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $borrower->id,
                "fileable_type" => "borrower",
                "field" => "saaq",
            ]);

        self::assertTrue(Storage::exists($saaq->full_path));

        $gaa = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $borrower->id,
                "fileable_type" => "borrower",
                "field" => "gaa",
            ]);

        self::assertTrue(Storage::exists($gaa->full_path));

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->create([
                "comments" => "foo bar",
                "instructions" => "foo bar",
                "position" => [45.537573, -73.601417],
                "location_description" => "foo bar",
                "name" => "foo bar",
            ]);

        $loanableImage = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loanable->id,
                "imageable_type" => "loanable",
                "field" => "image",
            ]);

        self::assertTrue(Storage::exists($loanableImage->full_path));

        $now = Carbon::now();
        self::setTestNow($now);

        $this->json("DELETE", "/api/v1/users/$user->id", [
            "delete_data" => true,
        ])->assertStatus(200);

        $expectedUserData = [
            "name" => "",
            "last_name" => "",
            "email" => "deleted-email-$user->id",
            "description" => "",
            "date_of_birth" => null,
            "address" => "",
            "postal_code" => "",
            "phone" => "",
            "other_phone" => "",
            "role" => null,
            "meta" => "[]",
            "bank_account_number" => null,
            "bank_institution_number" => null,
            "bank_transit_number" => null,
            "address_position" => null,
            "data_deleted_at" => $now->toDateTimeString(),
        ];

        $expectedBorrowerData = [
            "drivers_license_number" => null,
            "deleted_at" => $now->toDateTimeString(),
        ];

        $expectedLoanableData = [
            "comments" => "",
            "instructions" => "",
            "position" => null,
            "location_description" => "",
            "name" => "",
        ];

        $user->refresh();
        $loanable->refresh();
        $borrower->refresh();
        self::assertThat(
            $user->getAttributes(),
            new TraversableIsSimilar($expectedUserData)
        );
        self::assertThat(
            $loanable->getAttributes(),
            new TraversableIsSimilar($expectedLoanableData)
        );
        self::assertThat(
            $borrower->getAttributes(),
            new TraversableIsSimilar($expectedBorrowerData)
        );
        self::assertDatabaseCount("files", 0);
        self::assertDatabaseCount("images", 0);
        self::assertFalse(Storage::exists($avatar->full_path));
        self::assertFalse(Storage::exists($residencyProof->full_path));
        self::assertFalse(Storage::exists($identityProof->full_path));
        self::assertFalse(Storage::exists($insurance->full_path));
        self::assertFalse(Storage::exists($saaq->full_path));
        self::assertFalse(Storage::exists($gaa->full_path));
        self::assertFalse(Storage::exists($loanableImage->full_path));
    }

    public function testDeleteUserData_possibleOnDeletedUser()
    {
        $user = User::factory()->create([
            "name" => "Foo",
            "last_name" => "Bar",
            "email" => "foo@bar.com",
            "description" => "foo bar foo bar",
            "date_of_birth" => "2023-03-12",
            "address" => "123 foo",
            "postal_code" => "F0O8A3",
            "phone" => "123245544",
            "other_phone" => "9876543211",
            "role" => "admin",
            "meta" => [
                "foo" => "bar",
            ],
            "bank_account_number" => 1231,
            "bank_institution_number" => 12312,
            "bank_transit_number" => 1232121,
            "address_position" => [45.537573, -73.601417],
        ]);

        $user->delete();

        $avatar = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $user->id,
                "imageable_type" => "user",
                "field" => "avatar",
            ]);

        self::assertTrue(Storage::exists($avatar->full_path));

        $residencyProof = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->id,
                "fileable_type" => "user",
                "field" => "residency_proof",
            ]);

        self::assertTrue(Storage::exists($residencyProof->full_path));

        $identityProof = File::factory()
            ->withStoredFile()
            ->create([
                "fileable_id" => $user->id,
                "fileable_type" => "user",
                "field" => "identity_proof",
            ]);

        self::assertTrue(Storage::exists($identityProof->full_path));

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->create([
                "comments" => "foo bar",
                "instructions" => "foo bar",
                "position" => [45.537573, -73.601417],
                "location_description" => "foo bar",
                "name" => "foo bar",
            ]);

        $loanable->delete();

        $loanableImage = Image::factory()
            ->withStoredImage()
            ->create([
                "imageable_id" => $loanable->id,
                "imageable_type" => "loanable",
                "field" => "image",
            ]);

        self::assertTrue(Storage::exists($loanableImage->full_path));

        $now = Carbon::now();
        self::setTestNow($now);

        $this->json("DELETE", "/api/v1/users/$user->id", [
            "delete_data" => true,
        ])->assertStatus(200);

        $expectedUserData = [
            "name" => "",
            "last_name" => "",
            "email" => "deleted-email-$user->id",
            "description" => "",
            "date_of_birth" => null,
            "address" => "",
            "postal_code" => "",
            "phone" => "",
            "other_phone" => "",
            "role" => null,
            "meta" => "[]",
            "bank_account_number" => null,
            "bank_institution_number" => null,
            "bank_transit_number" => null,
            "address_position" => null,
            "data_deleted_at" => $now->toDateTimeString(),
        ];

        $expectedLoanableData = [
            "comments" => "",
            "instructions" => "",
            "position" => null,
            "location_description" => "",
            "name" => "",
        ];

        $user->refresh();
        $loanable->refresh();
        self::assertThat(
            $user->getAttributes(),
            new TraversableIsSimilar($expectedUserData)
        );
        self::assertThat(
            $loanable->getAttributes(),
            new TraversableIsSimilar($expectedLoanableData)
        );
        self::assertDatabaseCount("files", 0);
        self::assertDatabaseCount("images", 0);
        self::assertFalse(Storage::exists($avatar->full_path));
        self::assertFalse(Storage::exists($residencyProof->full_path));
        self::assertFalse(Storage::exists($identityProof->full_path));
        self::assertFalse(Storage::exists($loanableImage->full_path));
    }

    public function testRestoreUser_forbiddenIfDataDeleted()
    {
        $user = User::factory()->create();
        $user->deleteData();

        $response = $this->json("PUT", "/api/v1/users/{$user->id}/restore");

        $response->assertStatus(403);
    }

    public function testRestoreUser_doesntRestoreLoanablesByDefault()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loanable->owner_user->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$loanable->owner_user->id}/restore"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNotNull($loanable->deleted_at);
    }

    public function testRestoreUser_removesLoanableAvailabilities()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                "availability_mode" => "always",
            ]);

        $loanable->owner_user->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$loanable->owner_user->id}/restore?restore_loanables=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNull($loanable->deleted_at);
        self::assertEquals("never", $loanable->availability_mode);
        self::assertEquals("[]", $loanable->availability_json);
    }

    public function testRestoreUser_canRestoreLoanableAvailabilities()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                "availability_mode" => "always",
            ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore?restore_loanables=true&restore_availability=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNull($loanable->deleted_at);
        self::assertEquals("always", $loanable->availability_mode);
        self::assertEquals(
            <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            $loanable->availability_json
        );
    }

    public function testListUsers()
    {
        $users = User::factory(2)
            ->create()
            ->map(function ($user) {
                return $user->only(static::$userResponseStructure);
            });

        $response = $this->json("GET", route("users.index"));

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->buildCollectionStructure(static::$userResponseStructure)
            );
    }

    public function testUpdateUserWithCommunity()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
    }

    public function testUpdateUserWithResidencyProof_triggersRegistrationSubmitted()
    {
        \Storage::fake();
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "field" => "residency_proof",
            ]);

        $data = [
            "residency_proof" => [["id" => $proof->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithProofs_onlyTriggersRegistrationSubmittedWhenAllProofsPresent()
    {
        \Storage::fake();
        $user = User::factory()->create();
        $community = Community::factory()->create([
            "requires_identity_proof" => true,
        ]);
        $user->communities()->save($community);

        $residencyProof = File::factory()
            ->withStoredFile()
            ->create([
                "field" => "residency_proof",
            ]);

        $data = [
            "residency_proof" => [["id" => $residencyProof->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);

        $identityProof = File::factory()
            ->withStoredFile()
            ->create([
                "field" => "identity_proof",
            ]);
        $data = [
            "residency_proof" => [["id" => $residencyProof->id]],
            "identity_proof" => [["id" => $identityProof->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithResidencyProof_resetsCommunityProofEvaluation()
    {
        \Storage::fake();
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community, [
            "proof_evaluation" => "invalid",
        ]);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "field" => "residency_proof",
            ]);

        $data = [
            "residency_proof" => [["id" => $proof->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        $user->refresh();
        self::assertEquals(
            "unevaluated",
            $user->main_community->pivot->proof_evaluation
        );
    }

    public function testUpdateUserWithSameResidencyProof_doesntTriggerRegistrationSubmitted()
    {
        Storage::fake();
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "field" => "residency_proof",
            ]);
        $user->residencyProof()->save($proof);

        $data = ["residency_proof" => [["id" => $proof->id]]];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithoutResidencyProof_doesntTriggerRegistrationSubmitted()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testListUsersCommunities()
    {
        $user = User::factory()->create();
        $communities = Community::factory(2)->create();

        $data = [
            "communities" => [["id" => $communities[0]->id]],
        ];
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $data = [
            "users.id" => $user->id,
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJson(["total" => 1])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getCommunityResponseStructure
                )
            );
    }

    public function testAddToBalanceEndpoint()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
            "external_id" => "stripe source id",
        ]);

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->withArgs(function ($arg) {
                return $arg->id === $this->user->id;
            })
            ->andReturn((object) ["id" => "cus_test"]);

        Stripe::shouldReceive("computeAmountWithFee")
            ->once()
            ->with(1000, Mockery::any())
            ->andReturn(1050);

        Stripe::shouldReceive("createCharge")
            ->once()
            ->with(
                1050,
                "cus_test",
                "Ajout au compte LocoMotion: 10,00$ + 0,50$ (frais)",
                "stripe source id"
            )
            ->andReturn(
                (object) [
                    "id" => "ch_3N1fAGMh7B5SwAr3Q2hQpeQH",
                    "object" => "charge",
                    "amount" => 1050,
                ]
            );

        $response = $this->json("PUT", "/api/v1/auth/user/balance", [
            "amount" => 10,
            "payment_method_id" => $paymentMethod->id,
        ]);
        $response->assertStatus(200)->assertSee("10");
    }

    public function testAddToBalanceEndpoint_forbiddenIfNotApproved()
    {
        $user = User::factory()->create([]);

        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $user->id,
            "external_id" => "stripe source id",
        ]);

        $this->actAs($user);

        $response = $this->json("PUT", "/api/v1/auth/user/balance", [
            "amount" => 10,
            "payment_method_id" => $paymentMethod->id,
        ]);
        $response->assertStatus(403);
    }

    public function testClaimBalance_testClaimBalance_shouldLinkWithKiwili()
    {
        $this->user->balance = 10;
        $this->user->save();
        Kiwili::shouldReceive("createExpense")->andReturn([
            "Id" => "foo kiwili id",
        ]);
        \Mail::fake();
        $this->json("PUT", "/api/v1/auth/user/claim")->assertStatus(204);

        $this->user->refresh();
        self::assertEquals(0, $this->user->balance);
        self::assertCount(1, $this->user->invoices);
        self::assertEquals(
            -10,
            $this->user->invoices[0]->getUserBalanceChange()
        );
        $payouts = Payout::all();
        self::assertCount(1, $payouts);
        self::assertEquals("foo kiwili id", $payouts[0]->kiwili_expense_id);
        self::assertEquals($this->user->id, $payouts[0]->user_id);
        self::assertEquals(
            $this->user->invoices[0]->id,
            $payouts[0]->invoice_id
        );
        \Mail::assertQueued(
            UserBalanceClaimReceived::class,
            fn(UserBalanceClaimReceived $mail) => $mail->hasTo(
                $this->user->email
            )
        );
        \Mail::assertNotQueued(UserClaimedBalance::class);
    }

    public function testClaimBalance_testClaimBalance_canContinueWithoutLinking()
    {
        $this->user->balance = 10;
        $this->user->save();
        Kiwili::shouldReceive("createExpense")->andThrow(new \Exception());
        \Mail::fake();
        $this->json("PUT", "/api/v1/auth/user/claim")->assertStatus(204);

        $this->user->refresh();
        self::assertEquals(0, $this->user->balance);
        self::assertCount(1, $this->user->invoices);
        self::assertEquals(
            -10,
            $this->user->invoices[0]->getUserBalanceChange()
        );
        $payouts = Payout::all();
        self::assertCount(1, $payouts);
        self::assertEquals(null, $payouts[0]->kiwili_expense_id);
        self::assertEquals($this->user->id, $payouts[0]->user_id);
        self::assertEquals(
            $this->user->invoices[0]->id,
            $payouts[0]->invoice_id
        );
        \Mail::assertQueued(
            UserBalanceClaimReceived::class,
            fn(UserBalanceClaimReceived $mail) => $mail->hasTo(
                $this->user->email
            )
        );
        \Mail::assertQueued(
            UserClaimedBalance::class,
            fn(UserClaimedBalance $mail) => $mail->hasTo(
                $this->user->email // As admin
            )
        );
    }

    public function testClaimBalance_preventsSimultaneousRequests()
    {
        $this->user->balance = 10;
        $this->user->save();
        // It is impossible to tests concurrent requests with laravel, so we settle for 'simulating'
        // another requests being ongoing by acquiring the lock.
        \Cache::lock("payout-{$this->user->id}")->get();
        $this->json("PUT", "/api/v1/auth/user/claim")->assertStatus(429);
    }

    public function testPhoneNotVisibleToOtherUser()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $otherUser = User::factory()
            ->withCommunity($user->main_community)
            ->create();

        $this->actAs($user);
        $response = $this->json("GET", "api/v1/users?id={$otherUser->id}");

        $response->assertStatus(200)->assertJsonMissing(["phone"]);
    }

    public function testPhoneVisibleToAdmin()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();

        $response = $this->json("GET", "api/v1/users/{$user->id}");

        $response->assertStatus(200)->assertJsonStructure(["phone"]);
    }

    public function testLoanableOwnerPhoneNotVisibleToRandomUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();
        $otherUser = User::factory()->create();

        $this->actAs($otherUser);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,user_roles.role,user_roles.user.phone"
        );

        $response->assertStatus(403);
    }

    public function testLoanableOwnerPhoneVisibleToCoowner()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();

        $this->actAs($loanable->coowners->first()->user);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,user_roles.role,user_roles.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "merged_user_roles" => [
                [
                    "user" => ["phone"],
                ],
            ],
        ]);

        self::assertJsonUnordered($response->json(), [
            "merged_user_roles" => [["role" => "coowner"], ["role" => "owner"]],
        ]);
    }

    public function testLoanableCoOwnerPhoneVisibleToOwner()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();

        $this->actAs($loanable->owner_user);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,user_roles.role,user_roles.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "merged_user_roles" => [
                [
                    "user" => ["phone"],
                ],
            ],
        ]);

        self::assertJsonUnordered($response->json(), [
            "merged_user_roles" => [["role" => "coowner"], ["role" => "owner"]],
        ]);
    }

    public function testBorrowerPhoneVisibleToLoanableOwners()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner()
                ->withCoowner(),
        ])->create();

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,borrower_user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "borrower_user" => ["phone"],
        ]);

        $this->actAs($loan->loanable->coowners->first()->user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,borrower_user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "borrower_user" => ["phone"],
        ]);
    }

    public function testLoanableOwnersPhoneVisibleToBorrower()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory([])
                ->withOwner()
                ->withCoowner(),
        ])->create();

        $this->actAs($loan->borrowerUser);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,loanable.merged_user_roles.role,loanable.merged_user_roles.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "loanable" => [
                "merged_user_roles" => [
                    [
                        "user" => ["phone"],
                    ],
                ],
            ],
        ]);

        self::assertJsonUnordered($response->json(), [
            "loanable" => [
                "merged_user_roles" => [
                    ["role" => "coowner"],
                    ["role" => "owner"],
                ],
            ],
        ]);
    }

    public function testLoanableOwnersPhoneNotVisibleToBorrower_ifDesired()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner(null, ["show_as_contact" => false])
                ->withCoowner(null, ["show_as_contact" => false]),
        ])->create();

        $this->actAs($loan->borrowerUser);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,loanable.merged_user_roles.role,loanable.merged_user_roles.user.phone"
        );

        $response
            ->assertStatus(200)
            ->assertJsonPath("loanable.merged_user_roles.0.role", "owner")
            ->assertJsonMissingPath("loanable.merged_user_roles.0.user.phone")
            ->assertJsonMissingPath("loanable.merged_user_roles.1");
    }
}
