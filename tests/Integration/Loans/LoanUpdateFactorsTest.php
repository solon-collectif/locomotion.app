<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanUpdatedMail;
use App\Models\Image;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class LoanUpdateFactorsTest extends TestCase
{
    public function testSaveReturnInformation_validatesLoanForUpdater_resetsStatusToEnded()
    {
        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        self::setTestNow($executedAtDate);

        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "mileage_start" => 5,
            ]);
        $this->actAs($loan->borrowerUser);

        \Mail::fake();

        $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 10,
        ])->assertStatus(200);
        $loan = $loan->refresh();

        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertEquals(5, $loan->mileage_start);
        self::assertEquals(10, $loan->mileage_end);
        self::assertEquals($executedAtDate, $loan->borrower_validated_at);
        self::assertNull($loan->owner_validated_at);

        $this->actAs($loan->loanable->owner_user);
        $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 12,
        ])->assertStatus(200);

        \Mail::assertQueued(
            LoanUpdatedMail::class,
            fn(LoanUpdatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        \Mail::assertQueued(
            LoanUpdatedMail::class,
            fn(LoanUpdatedMail $mail) => $mail->hasTo(
                $loan->loanable->owner_user->email
            )
        );

        $loan->refresh();
        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertEquals(5, $loan->mileage_start);
        self::assertEquals(12, $loan->mileage_end);
        self::assertEquals($executedAtDate, $loan->owner_validated_at);
        self::assertNull($loan->borrower_validated_at);

        $this->actAs($this->user);
        $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 15,
        ])->assertStatus(200);

        $loan->refresh();
        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertEquals(5, $loan->mileage_start);
        self::assertEquals(15, $loan->mileage_end);
        self::assertNull($loan->owner_validated_at);
        self::assertNull($loan->borrower_validated_at);
    }

    public function testSaveReturnInformation_expiredLoan_givesTwoDaysForValidation()
    {
        $executedAtDate = CarbonImmutable::now()->format("Y-m-d h:m:s");
        $twoDaysAgo = CarbonImmutable::now()
            ->subDays(2)
            ->subMinute();
        self::setTestNow($twoDaysAgo);

        $loan = Loan::factory()
            ->needsValidation()
            ->ended()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => $twoDaysAgo,
                "mileage_start" => 5,
            ]);
        $this->actAs($loan->borrowerUser);

        self::setTestNow($executedAtDate);

        $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 10,
        ])->assertStatus(200);
        $loan = $loan->refresh();

        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertEquals(5, $loan->mileage_start);
        self::assertEquals(10, $loan->mileage_end);
        self::assertEquals($executedAtDate, $loan->borrower_validated_at);
        self::assertNull($loan->owner_validated_at);
        self::assertEquals(
            CarbonImmutable::now()->addDays(2),
            $loan->validationLimit()
        );
    }

    public function testSaveExpenses_cannotExceedPrice()
    {
        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        self::setTestNow($executedAtDate);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "estimated_distance" => 5,
                "mileage_start" => 0,
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);
        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "price",
                "community_id" => $loan->community_id,
                "rule" => "1 * \$KM",
            ]);

        $this->actAs($loan->borrowerUser);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 10,
            "expenses_amount" => 7,
        ]);
        $response->assertStatus(200);

        $loan = $loan->refresh();

        self::assertEquals(LoanStatus::Ongoing, $loan->status);
        self::assertEquals(7, $loan->expenses_amount);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 10,
            "expenses_amount" => 11,
        ]);
        $response->assertStatus(422)->assertJson([
            "message" =>
                "Le montant des dépenses ne peut pas excéder la compensation au propriétaire (10 $) pour cet emprunt. Discutez avec la personne propriétaire de l'auto pour obtenir le remboursement complet.",
        ]);
    }

    public function testSaveExpenses_cannotBeAddedIfFree()
    {
        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        self::setTestNow($executedAtDate);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "estimated_distance" => 5,
                "mileage_start" => 0,
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);
        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "price",
                "community_id" => $loan->community_id,
                "rule" => "0",
            ]);

        $this->actAs($loan->borrowerUser);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_end" => 10,
            "expenses_amount" => 1,
        ]);
        $response->assertStatus(422)->assertJson([
            "message" =>
                "Le montant des dépenses ne peut pas excéder la compensation au propriétaire (0 $) pour cet emprunt. Discutez avec la personne propriétaire de l'auto pour obtenir le remboursement complet.",
        ]);
    }

    public function testSaveDepartureInformation()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_start" => 10,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(10, $loan->mileage_start);
    }

    public function testSaveDepartureInformation_resetsValidation()
    {
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_start" => 10,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertEquals(null, $loan->borrower_validated_at);
        self::assertEquals(null, $loan->owner_validated_at);
        self::assertEquals(10, $loan->mileage_start);
    }

    public function testSaveDepartureInformation_forbiddenWhenLoanCompleted()
    {
        $loan = Loan::factory()
            ->paid()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_start" => 10,
        ]);
        $response->assertStatus(403);
    }

    function testUpdateMileage_resetsValidationForOtherParticipant()
    {
        $loan = Loan::factory()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "borrower_validated_at" => Carbon::now()->subHour(),
                "owner_validated_at" => Carbon::now()->subHour(),
                "mileage_start" => 5,
                "mileage_end" => 12,
            ]);

        $now = Carbon::now()->startOfSecond();
        self::setTestNow($now);

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/factors", [
            "mileage_end" => 15,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(15, $loan->mileage_end);
        self::assertNull($loan->owner_validated_at);
        self::assertEquals($now, $loan->borrower_validated_at);
    }

    function testUpdateExpenses_resetsValidation()
    {
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
                "borrower_validated_at" => new Carbon(),
                "owner_validated_at" => new Carbon(),
                "expenses_amount" => 8,
                "mileage_start" => 5,
                "mileage_end" => 15,
            ]);

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);

        $now = Carbon::now()->startOfSecond();
        self::setTestNow($now);

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/factors", [
            "expenses_amount" => 9,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(9, $loan->expenses_amount);
        self::assertNull($loan->borrower_validated_at);
        self::assertEquals($now, $loan->owner_validated_at);
    }

    function testUpdateLoanInfoAsAdmin_resetsValidationForAll()
    {
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
                "borrower_validated_at" => new Carbon(),
                "owner_validated_at" => new Carbon(),
                "expenses_amount" => 7,
            ]);

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);

        $this->actAs($this->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/factors", [
            "expenses_amount" => 9,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(9, $loan->expenses_amount);
        self::assertNull($loan->borrower_validated_at);
        self::assertNull($loan->owner_validated_at);
    }

    public function testUpdateLoanTipOrMileage_doesNotResetApproval()
    {
        $loan = Loan::factory([
            "platform_tip" => 5,
            "mileage_start" => 3,
            "mileage_end" => 4,
        ])
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        $this->actAs($loan->borrowerUser);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/tip", [
            "platform_tip" => 12,
        ]);

        $response->assertStatus(200)->assertJson([
            "platform_tip" => 12,
        ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_start" => 7,
            "mileage_end" => 13,
        ]);

        $response->assertStatus(200)->assertJson([
            "mileage_start" => 7,
            "mileage_end" => 13,
        ]);
        $loan->refresh();
        self::assertEquals(LoanStatus::Confirmed, $loan->status);
    }

    function testUpdateLoan_setsImages()
    {
        $loan = Loan::factory()->create([
            "loanable_id" => Loanable::factory()->withOwner(),
        ]);

        $imageA = Image::factory()->create([
            "imageable_id" => $loan->id,
            "imageable_type" => "loan",
            "field" => "mileage_start_image",
        ]);
        $imageB = Image::factory()->create([
            "imageable_id" => $loan->id,
            "imageable_type" => "loan",
            "field" => "mileage_end_image",
        ]);
        $imageC = Image::factory()->create([
            "imageable_id" => $loan->id,
            "imageable_type" => "loan",
            "field" => "expense_image",
        ]);

        $loan->refresh();

        self::assertEquals($imageA->id, $loan->mileageStartImage->id);
        self::assertEquals($imageB->id, $loan->mileageEndImage->id);
        self::assertEquals($imageC->id, $loan->expenseImage->id);

        $imageD = Image::factory()->create([
            "field" => "mileage_start_image",
        ]);
        $imageE = Image::factory()->create([
            "field" => "mileage_end_image",
        ]);
        $imageF = Image::factory()->create([
            "field" => "expense_image",
        ]);

        $this->actAs($loan->borrowerUser);
        $this->json("PUT", "/api/v1/loans/$loan->id/factors", [
            "mileage_start" => 7,
            "mileage_end" => 13,
            "expenses_amount" => 3,
            "mileage_start_image_id" => $imageD->id,
            "mileage_end_image_id" => $imageE->id,
            "expense_image_id" => $imageF->id,
        ])->assertStatus(200);
        $loan->refresh();

        self::assertEquals($imageD->id, $loan->mileageStartImage->id);
        self::assertEquals($imageE->id, $loan->mileageEndImage->id);
        self::assertEquals($imageF->id, $loan->expenseImage->id);
        self::assertDatabaseMissing("images", ["id" => $imageA->id]);
        self::assertDatabaseMissing("images", ["id" => $imageB->id]);
        self::assertDatabaseMissing("images", ["id" => $imageC->id]);
    }
}
