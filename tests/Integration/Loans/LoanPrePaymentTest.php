<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanConfirmedMail;
use App\Models\Loan;
use App\Models\User;
use Carbon\Carbon;
use Mail;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class LoanPrePaymentTest extends TestCase
{
    public function testPrepayingLoan_makesItConfirmed()
    {
        Mail::fake();
        $loan = Loan::factory()
            ->withMileageBasedCompensation()
            ->accepted()
            ->create([
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        // User balance must be set afterwards, otherwise we skip prepayment on loan creation
        // (since borrower could pay).
        $loan->borrowerUser->balance = 20;
        $loan->borrowerUser->save();
        $prepaidAt = Carbon::now()->format("Y-m-d h:m:s");
        self::setTestNow($prepaidAt);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/prepay", [
            "platform_tip" => 5,
        ]);
        $response->assertStatus(200);

        $loan->refresh();
        assertEquals(LoanStatus::Confirmed, $loan->status);
        assertEquals($prepaidAt, $loan->prepaid_at);
        assertEquals(5, $loan->platform_tip);

        Mail::assertQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        Mail::assertQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo(
                $loan->loanable->owner_user->email
            )
        );
    }

    public function testPrepayingLoan_cannotPay_fails()
    {
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/prepay", [
            "platform_tip" => 5,
        ]);
        $response->assertStatus(403);
    }
}
