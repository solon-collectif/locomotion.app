<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Mail\Loan\LoanExtensionAcceptedMail;
use App\Mail\Loan\LoanExtensionCanceledMail;
use App\Mail\Loan\LoanExtensionRejectedMail;
use App\Mail\Loan\LoanUpdatedMail;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class LoanExtensionTest extends TestCase
{
    protected Loan $loan;
    protected Loanable $loanable;
    protected User $borrowerUser;
    protected User $ownerUser;
    protected CarbonImmutable $departure;

    public function setUp(): void
    {
        parent::setUp();
        $this->borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $this->ownerUser = User::factory()
            ->withCommunity()
            ->create();

        $this->loanable = Loanable::factory()
            ->withOwner($this->ownerUser)
            ->create();
        $this->departure = CarbonImmutable::now()
            ->subMinutes(10)
            ->startOfSecond();
        $this->loan = Loan::factory()
            ->needsValidation()
            ->ongoing()
            ->create([
                "loanable_id" => $this->loanable,
                "borrower_user_id" => $this->borrowerUser,
                "duration_in_minutes" => 15,
                "departure_at" => $this->departure,
            ]);
    }

    public function testCreateExtensions()
    {
        $this->actAs($this->borrowerUser);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $response->assertStatus(200)->assertJson([
            "extension_duration_in_minutes" => 30,
        ]);
    }

    public function testCreateExtensions_AutoExtendsAsNonBorrowerOwner()
    {
        $this->actAs($this->ownerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $response->assertStatus(200);
        $this->loan->refresh();
        self::assertEquals(30, $this->loan->duration_in_minutes);
    }

    public function testCreateExtensions_AutoExtendsAsNonBorrowerCoowner()
    {
        $coowner = User::factory()->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $this->loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coowner->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $this->loanable->userRoles()->save($userRole);

        $this->actAs($coowner);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $response->assertStatus(200);
        $this->loan->refresh();
        self::assertEquals(30, $this->loan->duration_in_minutes);
    }

    public function testCannotCreateExtension_ifUnavailable()
    {
        Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $this->loanable->id,
                "duration_in_minutes" => 15,
                "departure_at" => $this->departure->addHour(),
            ]);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 76, // 15 initial + 60 next loan departure + 1 for overlap
            ]
        );

        $response->assertStatus(422)->assertJson([
            "message" => "Le véhicule n'est pas disponible sur cette période.",
        ]);
    }

    public function testCreateExtension_failsIfTooShort()
    {
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 10,
            ]
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "extension_duration_in_minutes" => [],
            ],
        ]);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 25, // Only 10 minutes in the future
            ]
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "extension_duration_in_minutes" => [],
            ],
        ]);
    }

    public function testCreateExtensionsForSelfServiceLoan()
    {
        $this->loan->is_self_service = true;
        $this->loan->save();

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $response->assertStatus(200)->assertJson([
            "duration_in_minutes" => 30,
        ]);

        $this->loan->refresh();
        self::assertEquals(
            $this->departure->copy()->addMinutes(30),
            $this->loan->actual_return_at
        );

        \Mail::assertQueued(
            LoanUpdatedMail::class,
            fn(LoanUpdatedMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
    }
    public function testCreateExtensions_selfServiceLoanEnded_resetsToOngoing()
    {
        CarbonImmutable::setTestNow(CarbonImmutable::now()->addMinutes(15));
        $this->loan->is_self_service = true;
        $this->loan->setLoanStatusEnded();
        $this->loan->save();

        self::assertEquals(LoanStatus::Ended, $this->loan->status);

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $response->assertStatus(200)->assertJson([
            "duration_in_minutes" => 30,
            "status" => "ongoing",
        ]);

        $this->loan->refresh();
        self::assertEquals(
            $this->departure->copy()->addMinutes(30),
            $this->loan->actual_return_at
        );
        self::assertEquals(LoanStatus::Ongoing, $this->loan->status);
    }

    public function testAcceptExtension_succeeds()
    {
        $this->actAs($this->borrowerUser);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $this->actAs($this->ownerUser);

        \Mail::fake();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 30,
                "extension_duration_in_minutes" => null,
            ]);

        \Mail::assertQueued(
            LoanExtensionAcceptedMail::class,
            fn(LoanExtensionAcceptedMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
        // Queued to owner even if they performed the action, to update their calendar invites
        \Mail::assertQueued(
            LoanExtensionAcceptedMail::class,
            fn(LoanExtensionAcceptedMail $mail) => $mail->hasTo(
                $this->ownerUser->email
            )
        );
    }

    public function testAcceptExtension_succeedsAsCoowner()
    {
        $this->actAs($this->borrowerUser);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $coowner = User::factory()->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $this->loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coowner->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $this->loanable->userRoles()->save($userRole);

        $this->actAs($coowner);

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 30,
                "extension_duration_in_minutes" => null,
            ]);
        \Mail::assertQueued(
            LoanExtensionAcceptedMail::class,
            fn(LoanExtensionAcceptedMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
        \Mail::assertQueued(
            LoanExtensionAcceptedMail::class,
            fn(LoanExtensionAcceptedMail $mail) => $mail->hasTo(
                $this->ownerUser->email
            )
        );
    }

    public function testAcceptExtension_forbiddenAsBorrower()
    {
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )->assertStatus(403);
    }

    public function testAcceptExtensions_loanEnded_resetsToOngoing()
    {
        CarbonImmutable::setTestNow(CarbonImmutable::now()->addMinutes(15));
        $this->loan->setLoanStatusEnded();
        $this->loan->save();

        self::assertEquals(LoanStatus::Ended, $this->loan->status);
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );
        $this->loan->refresh();
        self::assertEquals(LoanStatus::Ended, $this->loan->status);

        $this->actAs($this->ownerUser);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 30,
                "extension_duration_in_minutes" => null,
                "status" => "ongoing",
            ]);
        $this->loan->refresh();
        self::assertEquals(
            $this->departure->copy()->addMinutes(30),
            $this->loan->actual_return_at
        );
        self::assertEquals(LoanStatus::Ongoing, $this->loan->status);
    }

    public function testAcceptExtension_failsWhenNoExtension()
    {
        $this->actAs($this->ownerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )->assertStatus(422);
    }

    public function testAcceptExtension_failsIfNoLongerAvailable()
    {
        $this->actAs($this->borrowerUser);
        $this->json("PUT", "/api/v1/loans/{$this->loan->id}/extension", [
            "extension_duration_in_minutes" => 30,
        ])->assertStatus(200);

        Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $this->loanable,
                "borrower_user_id" => $this->borrowerUser,
                "duration_in_minutes" => 15,
                "departure_at" => $this->departure->addMinutes(15),
            ]);

        $this->actAs($this->ownerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/accept"
        )
            ->assertStatus(422)
            ->assertJson([
                "message" =>
                    "Le véhicule n'est pas disponible sur cette période.",
            ]);
    }

    public function testRejectExtension_succeeds()
    {
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $this->actAs($this->ownerUser);

        \Mail::fake();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/reject"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 15,
                "extension_duration_in_minutes" => null,
            ]);

        \Mail::assertQueued(
            LoanExtensionRejectedMail::class,
            fn(LoanExtensionRejectedMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
        // Not queued to owner, since they performed the action
        \Mail::assertNotQueued(
            LoanExtensionRejectedMail::class,
            fn(LoanExtensionRejectedMail $mail) => $mail->hasTo(
                $this->ownerUser->email
            )
        );
    }

    public function testRejectExtension_succeedsAsCoowner()
    {
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $coowner = User::factory()->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $this->loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coowner->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $this->loanable->userRoles()->save($userRole);

        $this->actAs($coowner);

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/reject"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 15,
                "extension_duration_in_minutes" => null,
            ]);
        \Mail::assertQueued(
            LoanExtensionRejectedMail::class,
            fn(LoanExtensionRejectedMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
        \Mail::assertQueued(
            LoanExtensionRejectedMail::class,
            fn(LoanExtensionRejectedMail $mail) => $mail->hasTo(
                $this->ownerUser->email
            )
        );
    }

    public function testRejectExtension_forbiddenAsBorrower()
    {
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/reject"
        )->assertStatus(403);
    }

    public function testRejectExtension_failsWhenNoExtension()
    {
        $this->actAs($this->ownerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/reject"
        )->assertStatus(422);
    }

    public function testCancelExtension_succeeds()
    {
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $this->actAs($this->borrowerUser);

        \Mail::fake();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/cancel"
        )
            ->assertStatus(200)
            ->assertJson([
                "duration_in_minutes" => 15,
                "extension_duration_in_minutes" => null,
            ]);

        // Not queued to borrower, since they performed the action
        \Mail::assertNotQueued(
            LoanExtensionCanceledMail::class,
            fn(LoanExtensionCanceledMail $mail) => $mail->hasTo(
                $this->borrowerUser->email
            )
        );
        \Mail::assertQueued(
            LoanExtensionCanceledMail::class,
            fn(LoanExtensionCanceledMail $mail) => $mail->hasTo(
                $this->ownerUser->email
            )
        );
    }

    public function testCancelExtension_forbiddenAsCoowner()
    {
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );

        $coowner = User::factory()->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $this->loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coowner->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $this->loanable->userRoles()->save($userRole);

        $this->actAs($coowner);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/cancel"
        )->assertStatus(403);
    }

    public function testCancelExtension_forbiddenAsNonBorrowerOwner()
    {
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension",
            [
                "extension_duration_in_minutes" => 30,
            ]
        );
        $this->actAs($this->ownerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/cancel"
        )->assertStatus(403);
    }

    public function testCancelExtension_failsWhenNoExtension()
    {
        $this->actAs($this->borrowerUser);
        $response = $this->json(
            "PUT",
            "/api/v1/loans/{$this->loan->id}/extension/cancel"
        )->assertStatus(422);
    }
}
