<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanAcceptedMail;
use App\Mail\Loan\LoanConfirmedMail;
use App\Mail\Loan\LoanRejectedMail;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use Mail;
use Tests\TestCase;

class LoanRequestTest extends TestCase
{
    public function testAcceptLoan_makesItAcceptedIfBorrowerCannotPay()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);

        $acceptedAt = Carbon::now()->format("Y-m-d h:m:s");
        self::setTestNow($acceptedAt);

        Mail::fake();

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/accept");
        $response->assertStatus(200);
        Mail::assertQueued(
            LoanAcceptedMail::class,
            fn(LoanAcceptedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        Mail::assertNotQueued(
            LoanAcceptedMail::class,
            fn(LoanAcceptedMail $mail) => $mail->hasTo(
                $loan->loanable->owner_user->email
            )
        );
        Mail::assertNotQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );

        $loan->refresh();
        self::assertEquals(LoanStatus::Accepted, $loan->status);
        self::assertEquals($acceptedAt, $loan->accepted_at);
    }

    public function testAcceptLoan_makesItConfirmedIfNoPrepaymentNecessary()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "community_id" => Community::factory()->withDefaultFreePricing(),
                "platform_tip" => 0,
            ]);

        Mail::fake();

        $this->json("PUT", "/api/v1/loans/$loan->id/accept")->assertStatus(200);
        Mail::assertNotQueued(
            LoanAcceptedMail::class,
            fn(LoanAcceptedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        Mail::assertQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        Mail::assertQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo(
                $loan->loanable->owner_user->email
            )
        );
        $loan->refresh();

        self::assertEquals(LoanStatus::Confirmed, $loan->status);
    }

    public function testAcceptLoan_makesItOngoingIfStartedInThePastAndBorrowerCanPay()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => Carbon::now()
                    ->subMinute()
                    ->toISOString(),
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
                "borrower_user_id" => User::factory([
                    "balance" => 10,
                ]),
                "platform_tip" => 0,
            ]);

        $this->json("PUT", "/api/v1/loans/$loan->id/accept")->assertStatus(200);
        $loan->refresh();

        self::assertEquals(LoanStatus::Ongoing, $loan->status);
    }

    public function testAcceptLoan_addsComment()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/accept", [
            "comment" => "foo",
        ]);
        $response->assertStatus(200)->assertJson([
            "comments" => [
                [
                    "text" => "foo",
                    "author" => [
                        "id" => $this->user->id,
                    ],
                ],
            ],
        ]);
    }

    public function testRejectLoan_addsComment()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
            ]);

        Mail::fake();
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/reject", [
            "comment" => "foo",
        ]);
        $response->assertStatus(200)->assertJson([
            "status" => "rejected",
            "comments" => [
                [
                    "text" => "foo",
                    "author" => [
                        "id" => $this->user->id,
                    ],
                ],
            ],
        ]);
        Mail::assertQueued(
            LoanRejectedMail::class,
            fn(LoanRejectedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );

        $loan->refresh();
        self::assertEquals(LoanStatus::Rejected, $loan->status);
    }
}
