<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanableUserRoles;
use App\Enums\LoanNotificationLevel;
use App\Enums\SharingModes;
use App\Mail\Loan\LoanCommentAddedMail;
use App\Mail\Loan\LoanRequestedMail;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanComment;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class LoanNotificationTest extends TestCase
{
    public function testSubcribesAllConcernedUsers()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coownerUser = User::factory()->create();
        $trustedBorrower = User::factory()->create();
        $stranger = User::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner($coownerUser)
            ->withUserRole($trustedBorrower, [
                "role" => LoanableUserRoles::TrustedBorrower,
            ])
            ->create();

        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/loans", [
            "departure_at" => CarbonImmutable::now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "loanable_id" => $loanable->id,
            "borrower_user_id" => $borrowerUser->id,
            "alternative_to" => "car",
        ])->assertStatus(201);

        $loan = Loan::find($response->json("id"));

        self::assertCount(3, $loan->notifiedUsers);
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $borrowerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $ownerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $coownerUser->id)->first()->pivot
                ->level
        );
    }

    public function testSubcribesOwnersAsMessageOnlyForNewSelfServiceLoans()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coownerUser = User::factory()->create();
        $trustedBorrower = User::factory()->create();
        $stranger = User::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner($coownerUser)
            ->withUserRole($trustedBorrower, [
                "role" => LoanableUserRoles::TrustedBorrower,
            ])
            ->create([
                "sharing_mode" => SharingModes::SelfService,
            ]);

        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/loans", [
            "departure_at" => CarbonImmutable::now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "loanable_id" => $loanable->id,
            "borrower_user_id" => $borrowerUser->id,
            "alternative_to" => "car",
        ])->assertStatus(201);

        $loan = Loan::find($response->json("id"));

        self::assertCount(3, $loan->notifiedUsers);
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $borrowerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::MessagesOnly,
            $loan->notifiedUsers->where("id", $ownerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::MessagesOnly,
            $loan->notifiedUsers->where("id", $coownerUser->id)->first()->pivot
                ->level
        );
    }

    public function testSubscribeToLoan()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coownerUser = User::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $stranger = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner($coownerUser)
            ->create();

        $loan = Loan::factory()->create([
            "loanable_id" => $loanable,
            "borrower_user_id" => $borrowerUser,
        ]);

        $this->actAs($communityAdmin);
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications", [
            "level" => "all",
        ])->assertStatus(200);

        $this->actAs($borrowerUser);
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications", [
            "level" => "none",
        ])->assertStatus(200);

        $loan->refresh();
        self::assertCount(4, $loan->notifiedUsers);
        self::assertEquals(
            LoanNotificationLevel::None,
            $loan->notifiedUsers->where("id", $borrowerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $ownerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $coownerUser->id)->first()->pivot
                ->level
        );
        self::assertEquals(
            LoanNotificationLevel::All,
            $loan->notifiedUsers->where("id", $communityAdmin->id)->first()
                ->pivot->level
        );

        $this->actAs($coownerUser);

        \Mail::fake();
        $this->json("POST", "/api/v1/loans/{$loan->id}/comment", [
            "text" => "foo",
        ]);

        \Mail::assertNotQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        // Coowner not notified since they sent the comment themselves
        \Mail::assertNotQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo($coownerUser->email)
        );
        \Mail::assertQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo(
                $communityAdmin->email
            )
        );
        \Mail::assertQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo($ownerUser->email)
        );
    }

    public function testSubscribeToLoanForbiddenForStranger()
    {
        $community = Community::factory()->create();
        $otherCommunityMember = User::factory()
            ->withCommunity($community)
            ->create();
        $stranger = User::factory()->create();

        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        $this->actAs($otherCommunityMember);
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications", [
            "level" => "all",
        ])->assertStatus(403);

        $this->actAs($stranger);
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications", [
            "level" => "all",
        ])->assertStatus(403);
    }

    public function testNotificationSeenAt()
    {
        $loan = Loan::factory()->create();

        $this->actAs($loan->borrowerUser);
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications/seen", [
            "seen_at" => "2024-03-12T12:00:00Z",
        ])->assertStatus(200);

        $loan->refresh();
        self::assertEquals(
            new CarbonImmutable("2024-03-12T12:00:00Z"),
            $loan->notifiedUsers->where("id", $loan->borrowerUser->id)->first()
                ->pivot->last_seen
        );

        // Earlier doesn't change actual last_seen
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications/seen", [
            "seen_at" => "2024-03-10T12:00:00Z",
        ])->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            new CarbonImmutable("2024-03-12T12:00:00Z"),
            $loan->notifiedUsers->where("id", $loan->borrowerUser->id)->first()
                ->pivot->last_seen
        );

        // Later overwrites
        $this->json("put", "/api/v1/loans/{$loan->id}/notifications/seen", [
            "seen_at" => "2024-04-12T12:00:00Z",
        ])->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            new CarbonImmutable("2024-04-12T12:00:00Z"),
            $loan->notifiedUsers->where("id", $loan->borrowerUser->id)->first()
                ->pivot->last_seen
        );
    }

    public function testDeleteComment()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coownerUser = User::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $stranger = User::factory()->create();
        $loan = Loan::factory()->create([
            "community_id" => $community,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => Loanable::factory()
                ->withOwner($ownerUser)
                ->create(),
        ]);

        $comment = LoanComment::factory([
            "loan_id" => $loan,
            "author_id" => $borrowerUser->id,
        ])->create();

        $this->actAs($ownerUser);
        $this->json(
            "DELETE",
            "/api/v1/loans/{$loan->id}/comment/{$comment->id}"
        )->assertStatus(403);

        $this->actAs($coownerUser);
        $this->json(
            "DELETE",
            "/api/v1/loans/{$loan->id}/comment/{$comment->id}"
        )->assertStatus(403);

        $this->actAs($stranger);
        $this->json(
            "DELETE",
            "/api/v1/loans/{$loan->id}/comment/{$comment->id}"
        )->assertStatus(403);

        $this->actAs($communityAdmin);
        $this->json(
            "DELETE",
            "/api/v1/loans/{$loan->id}/comment/{$comment->id}"
        )->assertStatus(403);

        $this->actAs($borrowerUser);
        $this->json(
            "DELETE",
            "/api/v1/loans/{$loan->id}/comment/{$comment->id}"
        )->assertStatus(200);
    }

    public function testDoesntNotify_coOwnersForNewSelfServiceLoan()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coowner = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner($coowner)
            ->create([
                "sharing_mode" => SharingModes::SelfService,
            ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "New loan hello!",
            "alternative_to" => "car",
        ];

        \Mail::fake();

        $this->actAs($borrowerUser);
        $this->json("POST", "/api/v1/loans", $data)->assertStatus(201);

        \Mail::assertNotQueued(
            LoanRequestedMail::class,
            fn(LoanRequestedMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertNotQueued(
            LoanRequestedMail::class,
            fn(LoanRequestedMail $mail) => $mail->hasTo($coowner->email)
        );
    }
    public function testDoesNotify_coOwnersForMessageInSelfServiceLoan()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $coowner = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner($coowner)
            ->create([
                "sharing_mode" => SharingModes::SelfService,
            ]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
                "is_self_service" => true,
            ]);

        \Mail::fake();
        $this->actAs($borrowerUser);
        $this->json("POST", "/api/v1/loans/{$loan->id}/comment", [
            "text" => "foo",
        ]);

        \Mail::assertQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertQueued(
            LoanCommentAddedMail::class,
            fn(LoanCommentAddedMail $mail) => $mail->hasTo($coowner->email)
        );
    }
}
