<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Enums\SharingModes;
use App\Mail\Loan\LoanAcceptedMail;
use App\Mail\Loan\LoanCanceledMail;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\Loan\LoanConfirmedMail;
use App\Mail\Loan\LoanRequestedMail;
use App\Mail\Loan\LoanValidatedMail;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Database\Events\QueryExecuted;
use Mail;
use Tests\TestCase;

class LoanTest extends TestCase
{
    private static array $getLoanResponseStructure = [
        "id",
        "departure_at",
        "duration_in_minutes",
        "estimated_distance",
        "alternative_to",
        "platform_tip",
        "canceled_at",
        "actual_expenses",
        "status",
        "actual_return_at",
        "owner_validated_at",
        "borrower_validated_at",
        "calendar_days",
        "is_free",
        "needs_validation",
        "borrower_total",
        "owner_total",
    ];

    public function testOrderLoansById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower_user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByDepartureAt()
    {
        $data = [
            "order" => "departure_at",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower_user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByBorrowerFullName()
    {
        $data = [
            "order" => "borrowerUser.full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower_user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByCommunityName()
    {
        $data = [
            "order" => "community.name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower_user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoanByDepartureAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "@",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCalendarDays()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "calendar_days" => "3",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCalendarDaysWithTimezone()
    {
        $ESTLoanable = Loanable::factory()
            ->withOwner()
            ->create([
                "timezone" => "America/Toronto",
            ]);

        $twoDayLoan = Loan::factory()->create([
            "departure_at" => (new Carbon(
                "2024-03-01T23:15",
                "America/Toronto"
            ))->toISOString(),
            "duration_in_minutes" => 60,
            "loanable_id" => $ESTLoanable,
        ]);

        $singleDayLoan = Loan::factory()->create([
            "departure_at" => (new Carbon(
                "2024-03-01T00:00",
                "America/Toronto"
            ))->toISOString(),
            "duration_in_minutes" => 360,
            "loanable_id" => $ESTLoanable,
        ]);

        $otherSingleDayLoan = Loan::factory()->create([
            "departure_at" => (new Carbon(
                "2024-03-01T18:00",
                "America/Toronto"
            ))->toISOString(),
            "duration_in_minutes" => 360,
            "loanable_id" => $ESTLoanable,
        ]);

        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "calendar_days" => "2:",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonCount(1, "data")
            ->assertJsonPath("data.0.id", $twoDayLoan->id);
    }

    public function testFilterLoansByLoanableType()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.type" => "bike",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByOwnerUserId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.owner_user_id" => 4,
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByBorrowerFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "borrowerUser.full_name" => "Georges",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByIncidentStatus()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "incidents.status" => "completed",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanStatus()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "status" => "completed",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCommunityId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "community.id" => "9",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCommunityName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "community.name" => "Patrie",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanableId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.id" => "1",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanableName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.name" => "Vélo",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testCreateLoan()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "on_demand",
            ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "New loan hello!",
            "alternative_to" => "car",
        ];

        Mail::fake();

        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/loans", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure)
            ->assertJson([
                "community" => [
                    "id" => $community->id,
                ],
                "comments" => [
                    [
                        "text" => "New loan hello!",
                        "author" => [
                            "id" => $borrowerUser->id,
                        ],
                    ],
                ],
            ]);

        Mail::assertQueued(
            LoanRequestedMail::class,
            fn(LoanRequestedMail $mail) => $mail->hasTo($ownerUser->email)
        );
    }

    public function testCreateLoan_assignsCommunityAutomatically()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "alternative_to" => "car",
        ];

        $response = $this->json("POST", "/api/v1/loans", $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure);
    }

    public function testCreateLoan_doesntSaveLoanable()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "loanable" => $loanable->toArray(),
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        Loanable::saving(fn() => throw new \Exception("Tried saving loanable"));
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure);
    }

    public function testCreateLoan_CannotCreateZeroMinuteLoan()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => 0,
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "duration_in_minutes" => [
                    "La durée de l'emprunt doit être supérieure ou égale à 15 minutes.",
                ],
            ],
        ]);
    }

    public function testCreateLoan_CannotCreateLoanInPast()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $now = CarbonImmutable::now();
        self::setTestNow($now);

        $data = [
            "departure_at" => $now->subMinutes(),
            "duration_in_minutes" => 15,
            "estimated_distance" => 5,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);
    }

    public function testCreateLoanImpossibleIfFleetNotSharedInCommunity()
    {
        $this->setTestNow(new Carbon("2023-05-05"));
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();
        $loanable = Loanable::factory()->create([
            "library_id" => $library,
        ]);

        $this->actAs($borrowerUser);

        $baseData = [
            "duration_in_minutes" => 60,
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "departure_at" => "2024-07-02T11:00",
            "alternative_to" => "car",
        ];

        $this->json("POST", "/api/v1/loans", $baseData)
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le champ loanable id est invalide.",
            ]);

        $library->communities()->attach($community->id);

        $this->json("POST", "/api/v1/loans", $baseData)->assertStatus(201);
    }

    public function testCreateLoanOnApprovedCommunityOnly()
    {
        $approvedCommunity = Community::factory()->create();
        $suspendedCommunity = Community::factory()->create();
        $justRegisteredCommunity = Community::factory()->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($approvedCommunity->id, [
            "approved_at" => CarbonImmutable::now(),
        ]);
        $borrowerUser->communities()->attach($suspendedCommunity->id, [
            "approved_at" => CarbonImmutable::now(),
            "suspended_at" => CarbonImmutable::now(),
        ]);
        $borrowerUser->communities()->attach($justRegisteredCommunity->id);

        $this->actAs($borrowerUser);

        $this->user->communities()->attach($approvedCommunity->id, [
            "approved_at" => CarbonImmutable::now(),
        ]);

        $loanable = Loanable::factory()
            ->withOwner($this->user)
            ->create();

        $departure = new Carbon();
        $baseData = [
            "duration_in_minutes" => 60,
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,

            "alternative_to" => "car",
        ];

        // Try creating a loan on...
        // 1. an approved community
        // 2. a suspended community
        // 3. a new community
        $approvedData = array_merge($baseData, [
            "community_id" => $approvedCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);
        $suspendedData = array_merge($baseData, [
            "community_id" => $suspendedCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);
        $justRegisteredData = array_merge($baseData, [
            "community_id" => $justRegisteredCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);

        $this->json("POST", "/api/v1/loans", $approvedData)->assertStatus(201);
        $this->json("POST", "/api/v1/loans", $suspendedData)->assertStatus(422);
        $this->json("POST", "/api/v1/loans", $justRegisteredData)->assertStatus(
            422
        );

        // Approve previously suspended or not approved communities
        $borrowerUser
            ->communities()
            ->updateExistingPivot($suspendedCommunity->id, [
                "approved_at" => CarbonImmutable::now(),
                "suspended_at" => null,
            ]);
        $borrowerUser
            ->communities()
            ->updateExistingPivot($justRegisteredCommunity->id, [
                "approved_at" => CarbonImmutable::now(),
            ]);

        $this->json("POST", "/api/v1/loans", $suspendedData)->assertStatus(201);
        $this->json("POST", "/api/v1/loans", $justRegisteredData)->assertStatus(
            201
        );
    }

    public function testShowLoans()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $loan = Loan::factory()->create([
            "borrower_user_id" => $borrowerUser->id,
        ]);

        $response = $this->json("GET", "/api/v1/loans/$loan->id");

        $response
            ->assertStatus(200)
            ->assertJson(["id" => $loan->id])
            ->assertJsonStructure(static::$getLoanResponseStructure);
    }

    public function testListLoans()
    {
        Loan::factory(2)
            ->create([
                "borrower_user_id" => User::factory()
                    ->withBorrower()
                    ->create(),
            ])
            ->map(fn($loan) => $loan->only(static::$getLoanResponseStructure));

        $response = $this->json("GET", route("loans.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getLoanResponseStructure
                )
            );
    }

    public function testCannotCreateLoanForOtherUser()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $car = Loanable::factory()
            ->withOwner($ownerUser)
            ->withBike()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower(
                Borrower::factory([
                    "approved_at" => null,
                ])
            )
            ->withCommunity($community)
            ->create();

        // Owner cannot create loan for borrower
        $this->actAs($ownerUser);

        $this->json("POST", "/api/v1/loans", [
            "departure_at" => Carbon::now(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $car->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,

            "alternative_to" => "car",
        ])
            ->assertStatus(403)
            ->assertJson([
                "message" =>
                    "Vous ne pouvez par créer un emprunt pour un autre utilisateur.",
            ]);
    }
    public function testCannotCreateCarLoanWhenBorrowerNotApproved()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $car = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower(
                Borrower::factory([
                    "approved_at" => null,
                ])
            )
            ->withCommunity($community)
            ->create();

        $this->actAs($borrowerUser);

        $this->json("POST", "/api/v1/loans", [
            "departure_at" => Carbon::now(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $car->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,

            "alternative_to" => "car",
        ])
            ->assertStatus(403)
            ->assertJson([
                "message" =>
                    "Le dossier de conduite doit être approuvé et non suspendu pour l'emprunt d'autos.",
            ]);
    }

    public function testCannotCreateCarLoanWhenBorrowerSuspended()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $car = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower(
                Borrower::factory([
                    "approved_at" => Carbon::now(),
                    "suspended_at" => Carbon::now(),
                ])
            )
            ->withCommunity($community)
            ->create();

        $this->actAs($borrowerUser);

        $this->json("POST", "/api/v1/loans", [
            "departure_at" => Carbon::now(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $car->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,

            "alternative_to" => "car",
        ])
            ->assertStatus(403)
            ->assertJson([
                "message" =>
                    "Le dossier de conduite doit être approuvé et non suspendu pour l'emprunt d'autos.",
            ]);
    }

    public function testCannotCreateConcurrentLoans()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $departure = new Carbon();
        $departure->setSeconds(0);
        $departure->setMilliseconds(0);

        $data = [
            "departure_at" => $departure->toDateTimeString(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        // First loan
        $response = $this->json("POST", "/api/v1/loans?fields=*", $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure);

        $loanId = $response->json()["id"];

        // Confirm intention on first loan
        $response = $this->json("PUT", "/api/v1/loans/$loanId/accept");
        $response->assertStatus(200);

        // Exactly the same time: overlap
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);

        // 1 hour from 30 minutes later: overlap
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->add(30, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);

        // 1 hour from 1 hour later: OK
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->add(60, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(201);

        // 1 hour from 30 minutes earlier: overlap
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->subtract(30, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);

        // 30 minutes from 30 minutes earlier: OK
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->subtract(30, "minutes")
                    ->toDateTimeString(),
                "duration_in_minutes" => 30,
            ])
        );

        $response->assertStatus(201);
    }

    public function testCannotCreateLoanOverMaxDuration()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "max_loan_duration_in_minutes" => 30,
            ]);

        $departure = new Carbon();
        $departure->setSeconds(0);
        $departure->setMilliseconds(0);

        $data = [
            "departure_at" => $departure->toDateTimeString(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        $response = $this->json("POST", "/api/v1/loans?fields=*", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);
    }
    public function testCreateWithSelfServiceLoanableIsAutomaticallyAccepted()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create([
                "balance" => 0,
            ]);
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3, true),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        Mail::fake();
        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(201)->assertJson([
            "status" => LoanStatus::Accepted->value,
        ]);

        // No 'accepted mail' necessary since borrower created the loan and they need to
        // do the next action (prepay).
        Mail::assertNotQueued(
            LoanAcceptedMail::class,
            fn(LoanAcceptedMail $mail) => $mail->hasTo($borrowerUser->email)
        );
    }

    public function testCreateLoanAsAdmin_notifiesPariticipants()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create([
                "balance" => 0,
            ]);
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3, true),
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        Mail::fake();
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(201)->assertJson([
            "status" => LoanStatus::Accepted->value,
        ]);

        Mail::assertQueued(
            LoanAcceptedMail::class,
            fn(LoanAcceptedMail $mail) => $mail->hasTo($borrowerUser->email)
        );
    }
    public function testCreateWithSelfServiceLoanableIsAutomaticallyConfirmed_ifBalanceIsEnough()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create([
                "balance" => 11,
            ]);
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => 50,
            "estimated_distance" => 10,
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,

            "alternative_to" => "car",
            "community_id" => $community->id,
        ];

        Mail::fake();
        $this->actAs($borrowerUser);
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(201)->assertJson([
            "status" => LoanStatus::Confirmed->value,
        ]);
        Mail::assertQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        // Not queued to owner, since self service
        Mail::assertNotQueued(
            LoanConfirmedMail::class,
            fn(LoanConfirmedMail $mail) => $mail->hasTo($ownerUser->email)
        );
    }

    public function testRetrieveApprovedLoan_showsInstructions()
    {
        $community = Community::factory()->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "instructions" => "test",
            ]);

        $this->actAs($borrowerUser);
        $response = $this->json("GET", route("loans.index"));

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);

        $loan = Loan::factory()->create([
            "borrower_user_id" => $borrowerUser->id,
            "loanable_id" => $loanable->id,
        ]);
        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id?fields=*,loanable.*"
        );

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);

        $this->actAs($ownerUser);
        $this->json("PUT", "/api/v1/loans/$loan->id/accept")->assertStatus(200);

        $this->actAs($borrowerUser);
        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id?fields=*,loanable.*"
        );

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testLoanDashboard()
    {
        $community = Community::factory()->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();

        Subscription::factory()
            ->forCommunityUser($community, $borrowerUser)
            ->create([
                "type" => "granted",
            ]);

        // Need of approval from $owner
        $loanable = Loanable::factory()
            ->withOwner($borrowerUser)
            ->create();
        Loan::factory(2)
            ->requested()
            ->create([
                "community_id" => $community,
                "loanable_id" => $loanable->id,
            ]);

        $otherOwnerUser = User::factory()
            ->withCommunity()
            ->create();

        $otherLoanable = Loanable::factory()
            ->withOwner($otherOwnerUser)
            ->create();
        Loan::factory(1)
            ->requested()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $otherLoanable->id,
            ]);
        // Starting in the future
        Loan::factory(2)
            ->confirmed()
            ->create([
                "departure_at" => Carbon::now()->addHour(),
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
            ]);
        // Starts in the future, but status = ongoing. This should only happen if we allowed
        // starting loans before their departure date, or if it was manually set.
        // These will be considered as 'started' on the dashboard.
        Loan::factory(3)
            ->ongoing()
            ->create([
                "departure_at" => Carbon::now()->addHour(),
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
            ]);
        // Started
        Loan::factory(4)
            ->ongoing()
            ->create([
                "departure_at" => Carbon::now()->subMinutes(5),
                "duration_in_minutes" => 60,
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
            ]);
        // recently_completed
        Loan::factory(7)
            ->paid()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
            ]);

        Pricing::factory()->create([
            "community_id" => null,
            "pricing_type" => "price",
            "rule" => 3,
        ]);

        Pricing::factory()->create([
            "community_id" => $community->id,
            "pricing_type" => "insurance",
            "rule" => 3,
        ]);

        $this->actAs($borrowerUser);

        $queryCount = 0;
        \DB::listen(function (QueryExecuted $q) use (&$queryCount) {
            $queryCount++;
        });
        $response = $this->json("get", "/api/v1/loans/dashboard");

        $response->assertJsonCount(5, "started.loans");
        $response->assertJsonPath("started.total", 7);
        $response->assertJsonCount(1, "waiting.loans");
        $response->assertJsonPath("waiting.total", 1);
        $response->assertJsonCount(2, "need_approval.loans");
        $response->assertJsonPath("need_approval.total", 2);
        $response->assertJsonCount(2, "future.loans");
        $response->assertJsonPath("future.total", 2);
        $response->assertJsonCount(5, "completed.loans");
        $response->assertJsonPath("completed.total", 7);

        // Request count
        self::assertEquals(51, $queryCount);
    }

    public function testLoanDashboard_adminSeesOnlyAsBorrowerOrOwner()
    {
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $ownerUser = User::factory()
            ->withCommunity()
            ->create();

        $admin = User::factory()
            ->withCommunity()
            ->create(["role" => "admin"]);

        $adminAsBorrower = Borrower::factory()->create([
            "user_id" => $admin->id,
        ]);

        $borrowedLoanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();
        $ownedLoanable = Loanable::factory()
            ->withOwner($admin)
            ->create();
        $otherLoanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $hiddenLoan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $otherLoanable->id,
            ]);
        $loanAsBorrower = Loan::factory()
            ->requested()
            ->create([
                "borrower_user_id" => $admin->id,
                "loanable_id" => $borrowedLoanable->id,
            ]);
        $loanAsOwner = Loan::factory()
            ->requested()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $ownedLoanable->id,
            ]);

        $this->actAs($admin);
        $response = $this->json("get", "/api/v1/loans/dashboard");

        $response->assertJson([
            "waiting" => [
                "loans" => [["id" => $loanAsBorrower->id]],
                "total" => 1,
            ],
            "need_approval" => [
                "loans" => [["id" => $loanAsOwner->id]],
                "total" => 1,
            ],
        ]);
        $response->assertJsonCount(0, "started.loans");
        $response->assertJsonCount(0, "future.loans");
        $response->assertJsonCount(0, "completed.loans");
    }

    public function testCompleteLoan_sendsAndMails()
    {
        // Bike with owner
        $bike = Loanable::factory(["sharing_mode" => "on_demand"])
            ->withOwner()
            ->withCoowner()
            ->create();

        $loan = Loan::factory()
            ->validated()
            ->create([
                "loanable_id" => $bike->id,
                "platform_tip" => 10,
            ]);

        // Borrower total is negative when loan cost is positive.
        $loan->borrowerUser->balance = -$loan->borrower_total + 100;
        $loan->borrowerUser->save();

        Mail::fake();

        $response = $this->json("put", "/api/v1/loans/$loan->id/pay");

        $response->assertStatus(200);
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->borrowerUser->email)
        );
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner_user->email)
        );
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo(
                $loan->loanable->coowners->first()->user->email
            )
        );
    }

    public function testCompleteSelfServiceLoan_sendsMailOnlyToBorrower()
    {
        // Bike without owner
        $bike = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create(["sharing_mode" => "self_service"]);

        $loan = Loan::factory()
            ->validated()
            ->create([
                "loanable_id" => $bike->id,
            ]);

        // Borrower total is negative when loan cost is positive.
        $loan->borrowerUser->balance = -$loan->borrower_total + 100;
        $loan->borrowerUser->save();
        $loan->refresh();

        Mail::fake();

        $response = $this->json("put", "/api/v1/loans/$loan->id/pay");

        $response->assertStatus(200);
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->borrowerUser->email)
        );
        Mail::assertNotQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner_user->email)
        );
        Mail::assertNotQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo(
                $loan->loanable->coowners->first()->user->email
            )
        );
    }

    function testCancelLoan_failsWhenDisallowed()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
            ]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(403)->assertJson([
            "message" => "L'emprunt payant ne doit pas être débuté.",
        ]);
    }

    function testCancelLoan_allowedForOwnerWhenOngoing()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
            ]);

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");

        $response->assertStatus(200);
        $loan->refresh();
        self::assertEquals(LoanStatus::Canceled, $loan->status);
    }

    function testCancelLoan_succeedsAsAdmin()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
            ]);

        // $this->user is admin
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testCancelLoan_succeedsWhenInTheFuture()
    {
        Mail::fake();

        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => Community::factory()->withDefault10DollarsPricing(),
            ]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
        Mail::assertQueued(
            LoanCanceledMail::class,
            fn(LoanCanceledMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
        Mail::assertQueued(
            LoanCanceledMail::class,
            fn(LoanCanceledMail $mail) => $mail->hasTo(
                $loan->loanable->owner_user->email
            )
        );
    }

    function testCancelLoan_succeedsWhenFree()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => Community::factory()->withDefaultFreePricing(),
            ]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testResumeExpiredLoan_skipsApprovalAndPrepayment()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->requested()
            ->needsValidation()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => CarbonImmutable::now()
                    ->subDay()
                    ->toISOString(),
                "duration_in_minutes" => 60,
            ]);
        $loan->setLoanStatusCanceled();
        $loan->save();

        $response = $this->json("PUT", "api/v1/loans/$loan->id/resume");
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(LoanStatus::Ended, $loan->status);
    }

    function testResumeRequestedLoan_resetsPreviousState()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => CarbonImmutable::now()
                    ->subDay()
                    ->toISOString(),
                "duration_in_minutes" => 60 * 24 * 3,
            ]);
        $loan->setLoanStatusCanceled();
        $loan->save();

        $response = $this->json("PUT", "api/v1/loans/$loan->id/resume");
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(LoanStatus::Requested, $loan->status);
    }

    function testResumeAcceptedLoan_resetsPreviousState()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => CarbonImmutable::now()
                    ->subDay()
                    ->toISOString(),
                "duration_in_minutes" => 60 * 24 * 3,
            ]);
        $loan->setLoanStatusCanceled();
        $loan->save();

        $response = $this->json("PUT", "api/v1/loans/$loan->id/resume");
        $response->assertStatus(200);

        $loan->refresh();
        self::assertEquals(LoanStatus::Accepted, $loan->status);
    }

    function testValidateLoan_succeedsForOwner()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create(["loanable_id" => $loanable]);

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->owner_validated_at);
    }

    function testValidateLoan_doesntOverwriteBorrowerInitialValidation()
    {
        $twentyMinutesAgo = Carbon::now(0)
            ->subMinutes(20)
            ->milli(0);

        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable->id,
                "borrower_validated_at" => $twentyMinutesAgo,
            ]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            $twentyMinutesAgo,
            new Carbon($loan->borrower_validated_at)
        );
    }

    function testValidateLoan_doesntOverwriteOwnerInitialValidation()
    {
        $twentyMinutesAgo = Carbon::now(0)
            ->subMinutes(20)
            ->milli(0);

        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable->id,
                "owner_validated_at" => $twentyMinutesAgo,
            ]);

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            $twentyMinutesAgo,
            new Carbon($loan->owner_validated_at)
        );
    }

    function testValidateLoan_succeedsForBorrower()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create(["loanable_id" => $loanable]);

        $this->actAs($loan->borrowerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->borrower_validated_at);
    }

    function testValidateLoan_allowsCoownerBorrowerToFullyValidate()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create(["loanable_id" => $loanable]);

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $loan->borrower_user_id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loan->loanable->userRoles()->save($userRole);

        $this->actAs($loan->borrowerUser);
        $loan->refresh();
        self::assertFalse($loan->needs_validation);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);
    }

    function testValidateLoan_allowsOwnerBorrowerToFullyValidate()
    {
        $ownerUser = User::factory()
            ->withBorrower()
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
            ]);

        // Set borrower after otherwise the loan will be validated after creation.
        $loan->borrower_user_id = $ownerUser->id;
        $loan->save();
        $loan->refresh();

        $this->actAs($ownerUser);
        self::assertFalse($loan->needs_validation);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);
        self::assertEquals(LoanStatus::Validated, $loan->status);
    }

    function testValidateLoan_doesntChangeFromEndedStateIfAutoValidationRequired()
    {
        $ownerUser = User::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
                "owner_validated_at" => CarbonImmutable::now(),
            ]);

        $this->actAs($borrowerUser);
        self::assertTrue($loan->needs_validation);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);
        self::assertTrue($loan->isFullyValidated());
        self::assertFalse($loan->has_required_info_to_validate);
        self::assertEquals(LoanStatus::Ended, $loan->status);
    }

    public function testAutoValidation_doesntChangeOngoingState()
    {
        $ownerUser = User::factory()
            ->withCommunity()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
                "loan_auto_validation_answer" => "bar",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
            ]);

        $this->actAs($borrowerUser);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "answer" => "bar",
        ]);
        $response->assertStatus(200);
        $loan->refresh();

        self::assertEquals(LoanStatus::Ongoing, $loan->status);
        self::assertNotNull($loan->auto_validated_at);
        self::assertEquals($borrowerUser->id, $loan->autoValidatedByUser->id);
    }

    public function testAutoValidation_doesntChangeEndedStateIfValidationRequired()
    {
        $ownerUser = User::factory()
            ->withCommunity()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
                "loan_auto_validation_answer" => "bar",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
            ]);

        $this->actAs($borrowerUser);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "answer" => "bar",
        ]);
        $response->assertStatus(200);
        $loan->refresh();

        self::assertEquals(LoanStatus::Ended, $loan->status);
        self::assertNotNull($loan->auto_validated_at);
        self::assertEquals($borrowerUser->id, $loan->autoValidatedByUser->id);
    }

    public function testAutoValidation_makesLoanValidatedAndSendsMailIfNoMoreValidationRequired()
    {
        $ownerUser = User::factory()
            ->withCommunity()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 0,
            ]);

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
                "loan_auto_validation_answer" => "bar",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
                "borrower_validated_at" => CarbonImmutable::now(),
                "owner_validated_at" => CarbonImmutable::now(),
            ]);

        $this->actAs($ownerUser);
        Mail::fake();

        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "answer" => "bar",
        ]);
        $response->assertStatus(200);
        $loan->refresh();

        self::assertEquals(LoanStatus::Validated, $loan->status);
        self::assertNotNull($loan->auto_validated_at);
        self::assertEquals($ownerUser->id, $loan->autoValidatedByUser->id);
        Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo($borrowerUser->email)
        );
    }

    public function testAutoValidation_requiresCorrectAnswer()
    {
        $ownerUser = User::factory()
            ->withCommunity()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 0,
            ]);

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
                "loan_auto_validation_answer" => "bar",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
            ]);

        $this->actAs($borrowerUser);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "answer" => "baz",
        ]);
        $response->assertStatus(422);
    }

    public function testAutoValidation_canBeSkippedButNotByBorrower()
    {
        $ownerUser = User::factory()
            ->withCommunity()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 0,
            ]);

        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo",
                "loan_auto_validation_answer" => "bar",
            ]);

        $loanable = Loanable::factory([
            "library_id" => $library->id,
        ])
            ->withCar()
            ->create();
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => $loanable,
                "borrower_user_id" => $borrowerUser,
            ]);

        $this->actAs($borrowerUser);

        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "skip" => true,
        ]);
        $response->assertStatus(403);

        $loan->refresh();
        self::assertNull($loan->auto_validated_at);

        $this->actAs($ownerUser);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/autovalidate", [
            "skip" => true,
        ])->assertStatus(200);

        $loan->refresh();
        self::assertNotNull($loan->auto_validated_at);
    }

    function testEstimateLoan_availabilityWithOtherLoan()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "sharing_mode" => SharingModes::OnDemand,
            ]);
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now(),
                "duration_in_minutes" => 15,
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 50,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => true,
            ]);

        Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now()->addMinutes(30),
                "duration_in_minutes" => 15,
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 30,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => true,
            ]);
        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 45,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => false,
            ])
            ->assertJsonMissing(["blocking_loan"]);
    }

    function testEstimateLoan_availabilityWithMaxDuration()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "sharing_mode" => SharingModes::OnDemand,
                "max_loan_duration_in_minutes" => 30,
            ]);
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now(),
                "duration_in_minutes" => 15,
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 31,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => false,
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 30,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => true,
            ]);
    }

    function testEstimateLoan_returnsBlockingLoanForOngoingSelfServiceLoans()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "sharing_mode" => SharingModes::SelfService,
            ]);
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now(),
                "duration_in_minutes" => 15,
            ]);

        $blockingLoan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now()->addMinutes(30),
                "duration_in_minutes" => 15,
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 45,
        ])
            ->assertStatus(200)
            ->assertJson([
                "available" => false,
                "blocking_loan" => [
                    "id" => $blockingLoan->id,
                ],
            ]);
    }

    function testEstimateLoan_computesPricingWithNewDates()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "sharing_mode" => SharingModes::OnDemand,
                "timezone" => "America/Toronto",
            ]);
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => (new CarbonImmutable(
                    "2024-07-04T23:30:00",
                    "America/Toronto"
                ))->tz("utc"),
                "duration_in_minutes" => 15,
            ]);

        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "rule" => "\$EMPRUNT.days + \$MINUTES",
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 30,
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -31, // 30 minutes & 1 calendar day
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 30,
            "departure_at" => "2024-04-07T23:45:00",
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -32, // 30 minutes & 2 calendar days
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "duration_in_minutes" => 45,
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -47, // 45 minutes & 2 calendar days
                ],
            ]);
    }

    function testEstimateLoan_computesPricingWithNewMileage()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCar()
            ->create([
                "sharing_mode" => SharingModes::OnDemand,
                "timezone" => "America/Toronto",
            ]);
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => CarbonImmutable::now(),
                "duration_in_minutes" => 15,
                "estimated_distance" => 7,
                "mileage_start" => 13,
            ]);

        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "rule" => "\$KM",
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "estimated_distance" => 11,
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -11,
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -7, // no mileage, we consider only estimated_distance
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
            "mileage_end" => 12,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -7, // Invalid mileage, we consider only estimated_distance
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
            "mileage_start" => 15,
            "mileage_end" => 12,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -7, // Invalid mileage, we consider only estimated_distance
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
            "mileage_start" => 15,
            "mileage_end" => 37,
            "estimated_distance" => 11,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -22, // 37 - 15
                ],
            ]);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
            "mileage_end" => 37,
            "estimated_distance" => 11,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "user_balance_change" => -24, // 37 - 13
                ],
            ]);
    }

    function testEstimateLoan_hidesOwnerInvoiceToBorrower()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->withMileageBasedCompensation()
            ->create([]);

        $this->actAs($loan->borrowerUser);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 2,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [],
            ])
            ->assertJsonMissing(["owner_invoice"]);
    }
    function testEstimateLoan_hidesBorrowerInvoiceToOwner()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->withMileageBasedCompensation()
            ->create([]);

        $this->actAs($loan->loanable->owner_user);

        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
        ])
            ->assertStatus(200)
            ->assertJsonMissing([
                "borrower_invoice" => [],
            ])
            ->assertJson([
                "owner_invoice" => [],
            ]);
    }

    function testEstimateLoan_forbiddenToStranger()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create();

        $stanger = User::factory()->create();

        $this->actAs($stanger);
        $this->json("GET", "api/v1/loans/$loan->id/estimate", [
            "platform_tip" => 0,
        ])->assertStatus(403);
    }
}
