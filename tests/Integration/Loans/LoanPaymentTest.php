<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanStatus;
use App\Enums\PricingLoanableTypeValues;
use App\Mail\InvoicePaidMail;
use App\Mail\Loan\LoanCompletedMail;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class LoanPaymentTest extends TestCase
{
    public function testCompletePayments()
    {
        $departureAt = new CarbonImmutable(
            "2024-03-12T23:00",
            "America/Toronto"
        );
        // Reset test time now.
        self::setTestNow($departureAt->addMinutes(100));
        $community = Community::factory()->create();

        // Expected total for time: 7 +  100 = 107,
        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "pricing_type" => "price",
                "rule" => "\$KM + \$MINUTES",
            ]);

        // Expected total for insurance: 2 (2 days in America/Toronto, but 1 day in UTC)
        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "pricing_type" => "insurance",
                "rule" => "\$EMPRUNT.days",
            ]);

        $loan = Loan::factory()
            ->validated()
            ->create([
                "mileage_start" => 5,
                "mileage_end" => 12,
                "expenses_amount" => 4,
                "platform_tip" => 1,
                "community_id" => $community,
                "departure_at" => $departureAt->toISOString(),
                "duration_in_minutes" => 100,
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);

        // Add user balance after loan creation, to avoid it being automatically paid.
        $loan->borrowerUser->balance = 200;
        $loan->borrowerUser->save();

        $executedAt = Carbon::now()->addMinutes(15);
        self::setTestNow($executedAt);

        Mail::fake();

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");
        $response->assertStatus(200);

        $loan->refresh();
        assertEquals(LoanStatus::Completed, $loan->status);
        assertEquals(
            $executedAt->toDateTimeString(),
            $loan->paid_at->toDateTimeString()
        );

        // 200 - 107 - 2 + 4 - 1
        assertEquals(94, $loan->borrowerUser->balance);
        // 107 - 4
        assertEquals(103, $loan->loanable->owner_user->balance);

        $this->json("PUT", "/api/v1/loans/$loan->id/pay", [
            "type" => "payment",
        ])->assertStatus(403);

        // Assert that items are saved in the database.
        $this->assertDatabaseHas("invoices", [
            "id" => $loan->borrower_invoice_id,
        ]);
        $this->assertDatabaseHas("bill_items", [
            "invoice_id" => $loan->borrower_invoice_id,
        ]);

        $this->assertDatabaseHas("invoices", [
            "id" => $loan->owner_invoice_id,
        ]);

        $this->assertDatabaseHas("bill_items", [
            "invoice_id" => $loan->owner_invoice_id,
        ]);

        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->borrowerUser->email)
        );
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner_user->email)
        );
    }

    public function testCompletePayments_failsIfBeforeLoanDeparture()
    {
        $loan = Loan::factory()
            ->validated()
            ->create([
                "departure_at" => CarbonImmutable::now()
                    ->addHour()
                    ->toISOString(),
            ]);

        $loan->borrowerUser->balance = 100;
        $loan->borrowerUser->save();

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");
        $response->assertStatus(403);
        $response->assertJson([
            "message" =>
                "L'étape payment ne peut pas être complétée avant l'heure du départ.",
        ]);

        $this->assertEquals(LoanStatus::Validated, $loan->status);
        $this->assertNull($loan->paid_at);
    }

    public function testCompletePayments_failsIfNotEnoughMoney()
    {
        $loan = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create();

        $loan->borrowerUser->balance = 0;
        $loan->borrowerUser->save();

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");

        $response->assertStatus(403)->assertJson([
            "message" =>
                "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
        ]);
    }

    public function testCompletePayments_failsIfNotValidated()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCar()
            ->create();

        $loan = Loan::factory()
            ->needsValidation()
            ->ended(10, 100) // not validated
            ->create([
                "loanable_id" => $loanable,
            ]);

        $loan->borrowerUser->balance = 100;
        $loan->borrowerUser->save();

        $this->actAs($loan->loanable->owner_user);
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");
        $response->assertStatus(403)->assertJson([
            "message" => "L'emprunt doit être validé avant d'être payé.",
        ]);
    }

    public function testCompletePayments_whenBorrowerIsOwner()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create(["balance" => 0]);

        $loanable = Loanable::factory()
            ->withOwner($borrowerUser)
            ->withCar()
            ->create([
                "availability_mode" => "always",
            ]);

        $loan = Loan::factory()
            ->validated()
            ->create([
                "borrower_user_id" => $borrowerUser,
                "loanable_id" => $loanable,
                "platform_tip" => 1,
                "expenses_amount" => 4,
            ]);

        $borrowerUser->balance = 20;
        $borrowerUser->save();

        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "price",
                "rule" => "5",
            ]);
        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "insurance",
                "rule" => "7",
            ]);

        $executedAt = Carbon::now()->addMinutes(15);
        self::setTestNow($executedAt);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");
        $response->assertStatus(200);

        $loan->refresh();

        assertEquals(LoanStatus::Completed, $loan->status);
        assertEquals(
            $executedAt->toDateTimeString(),
            $loan->paid_at->toDateTimeString()
        );

        // Expect the insurance cost not to be taken off the borrower's balance.
        // 20 - 1 (platform tip) = 19 (price and expenses are ignored)
        assertEquals(19, $loan->borrowerUser->balance);

        // Assert that both invoices are the same.
        $this->assertEquals(
            $loan->owner_invoice_id,
            $loan->borrower_invoice_id
        );

        // Assert that items are saved in the database.
        $this->assertDatabaseHas("invoices", [
            "id" => $loan->borrower_invoice_id,
        ]);
        $this->assertDatabaseHas("bill_items", [
            "invoice_id" => $loan->borrower_invoice_id,
        ]);
    }

    public function testCompletePayment_failsIfNotEnoughMoneyForMandatoryContribution()
    {
        $community = Community::factory()->create();
        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "pricing_type" => "contribution",
                "is_mandatory" => true,
                "rule" => 5,
            ]);
        $loan = Loan::factory()
            ->validated()
            ->create([
                "community_id" => $community,
                "platform_tip" => 0,
            ]);

        $loan->refresh();
        self::assertEquals(5, -$loan->borrower_total);
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay", [
            "type" => "payment",
        ]);

        $response->assertStatus(403)->assertJson([
            "message" =>
                "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
        ]);
    }

    public function testCompletePayment_noInvoiceIfNoInvoiceItems()
    {
        $community = Community::factory()->create();
        $borrower = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "pricing_type" => "contribution",
                "is_mandatory" => true,
                "community_id" => $community,
                "rule" => 5,
            ]);
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "pricing_type" => "price",
                "is_mandatory" => true,
                "community_id" => $community,
                "rule" => 2,
            ]);
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "pricing_type" => "insurance",
                "is_mandatory" => true,
                "community_id" => $community,
                "rule" => 2,
            ]);

        $loan = Loan::factory()
            ->validated()
            ->create([
                "community_id" => $community,
                "platform_tip" => 0,
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
                "borrower_user_id" => $borrower,
            ]);

        $this->json("PUT", "/api/v1/loans/$loan->id/pay")
            ->assertStatus(403)
            ->assertJson([
                "message" =>
                    "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
            ]);

        // User fully exempted from pricings
        LoanableUserRole::factory()->create([
            "user_id" => $loan->borrowerUser,
            "ressource_id" => $loan->loanable->id,
            "ressource_type" => "loanable",
            "pays_loan_insurance" => false,
            "pays_loan_price" => false,
        ]);
        Subscription::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->forCommunityUser($community, $borrower)
            ->create();

        Mail::fake();

        $this->json("PUT", "/api/v1/loans/$loan->id/pay")->assertStatus(200);

        $this->assertDatabaseEmpty("invoices");
        $loan->refresh();
        self::assertEquals(LoanStatus::Completed, $loan->status);
        self::assertNull($loan->owner_invoice_id);
        self::assertNull($loan->borrower_invoice_id);

        Mail::assertNotQueued(
            InvoicePaidMail::class,
            fn($mail) => $mail->hasTo($loan->borrowerUser->email)
        );
        Mail::assertNotQueued(
            InvoicePaidMail::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner_user->email)
        );
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->borrowerUser->email)
        );
        Mail::assertQueued(
            LoanCompletedMail::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner_user->email)
        );
    }

    public function testCompletePayment_paidToLibraryOwner()
    {
        $libraryOwner = User::factory()->create(["balance" => 0]);
        $community = Community::factory()->create();
        $library = Library::factory()
            ->withOwner($libraryOwner)
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create(["balance" => 50]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "library_id" => $library,
            ]);

        $loan = Loan::factory()
            ->validated()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "platform_tip" => 0,
                "expenses_amount" => 0,
                "community_id" => $community,
            ]);

        // Apply pricing rules
        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "price",
                "rule" => "20",
            ]);

        $executedAt = Carbon::now()->addMinutes(15);
        self::setTestNow($executedAt);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/pay");
        $response->assertStatus(200);

        $loan->refresh();

        // Assert loan status and timestamps
        $this->assertEquals(LoanStatus::Completed, $loan->status);
        $this->assertEquals(
            $executedAt->toDateTimeString(),
            $loan->paid_at->toDateTimeString()
        );

        $libraryEarnings = 20;
        $this->assertEquals($libraryEarnings, $libraryOwner->fresh()->balance);

        $this->assertNotNull($loan->owner_invoice_id);
        $this->assertEquals($loan->ownerInvoice->user_id, $libraryOwner->id);
    }
}
