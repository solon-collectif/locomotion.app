<?php

namespace Tests\Integration\Loans;

use App\Enums\LoanStatus;
use App\Mail\Loan\LoanUpdatedMail;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Mail;
use Tests\TestCase;

class LoanUpdateDatesTest extends TestCase
{
    public function testUpdateLoanDates_updatesInLoanableTz()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "timezone" => "America/Toronto",
            ]);
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 45,
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", [
            "departure_at" => "2022-03-09T23:00",
        ])
            ->assertStatus(200)
            ->assertJson([
                "departure_at" => "2022-03-10T04:00:00.000000Z",
            ]);
    }

    public function testUpdateLoan_isForbiddenIfUnavailable()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 45,
            ]);

        $otherLoan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()
                    ->addWeek()
                    ->addHour(),
            ]);

        $data = [
            "duration_in_minutes" => 360, // 6 hours, which would overlap the other loan
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);
    }

    public function testUpdateLoan_isForbiddenIfOverMaxDuration()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "max_loan_duration_in_minutes" => 60,
            ]);
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 45,
            ]);

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", [
            "duration_in_minutes" => 61,
        ]);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule n'est pas disponible sur cette période."],
            ],
        ]);
    }

    public function testUpdateLoan_isForbiddenIfAfterLoanDeparture()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->subHour(),
                "duration_in_minutes" => 120,
            ]);

        $data = [
            "duration_in_minutes" => 360, // 6 hours, which would overlap the other loan
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", $data);

        $response->assertStatus(403)->assertJson([
            "message" =>
                "L'heure du début de la réservation doit être dans le futur.",
        ]);
    }

    public function testUpdateSelfServiceLoan_isApprovedAutomaticallyIfAvailable()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "sharing_mode" => "self_service",
            ]);
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
                "platform_tip" => 0,
            ]);
        $data = [
            "duration_in_minutes" => $this->faker->randomDigitNotZero() * 15,
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals(LoanStatus::Confirmed, $loan->status);
    }

    public function testUpdateLoan_resetsPrepaymentIfNotFreeForBorrower()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $this->user
            ->communities()
            ->attach($community->id, ["approved_at" => CarbonImmutable::now()]);
        $loanable = Loanable::factory()
            ->withOwner($this->user)
            ->create([
                "sharing_mode" => "self_service",
            ]);
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 60,
                "borrower_user_id" => User::factory([
                    "balance" => 5,
                ]),
            ]);
        $data = [
            "duration_in_minutes" => 120,
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals(LoanStatus::Accepted, $loan->status);
        self::assertNull($loan->takeover);
    }

    public function testUpdateLoanTime_resetsApproval()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
            ]);
        $data = [
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
        ];

        $this->actAs($loan->borrowerUser);
        Mail::fake();
        $response = $this->json("PUT", "/api/v1/loans/$loan->id/dates", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals(LoanStatus::Requested, $loan->status);
        Mail::assertQueued(
            LoanUpdatedMail::class,
            fn(LoanUpdatedMail $mail) => $mail->hasTo(
                $loanable->owner_user->email
            )
        );
        Mail::assertQueued(
            LoanUpdatedMail::class,
            fn(LoanUpdatedMail $mail) => $mail->hasTo(
                $loanable->coowners->first()->user->email
            )
        );
    }
}
