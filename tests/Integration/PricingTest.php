<?php

namespace Tests\Integration;

use App\Enums\BillItemTypes;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class PricingTest extends TestCase
{
    public function testEvaluatePricing()
    {
        $community = Community::factory()->create();

        $syncCommunities = [];
        $syncCommunities[$community->id] = [
            "approved_at" => new \DateTime(),
            "role" => "admin",
        ];
        $this->user->communities()->sync($syncCommunities);

        $car = Loanable::factory()
            ->withOwner($this->user)
            ->withCar([
                "year_of_circulation" => 1_000_000,
            ])
            ->create();

        $pricing = Pricing::factory()->create([
            "community_id" => $community->id,
            "rule" =>
                '$KM * 1 + $MINUTES * 1000 + $OBJET.year_of_circulation ' .
                '+ $EMPRUNT.days * 0.1',
        ]);

        $data = [
            "km" => 1,
            "minutes" => 1,
            "loanable" => $car->load("details")->toArray(),
            "loan" => [
                "days" => 2,
            ],
        ];

        $response = $this->json(
            "PUT",
            route("pricings.evaluate", $pricing),
            $data
        );

        $response->assertStatus(200)->assertContent("1001001.2");
    }

    public function testPricingBillItemGeneration()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner()
                ->withTrailer(),
            "platform_tip" => 3,
        ])
            ->confirmed()
            ->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "price",
                "rule" => "7",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "5",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();
        // 3 = since insurance, price and donation
        self::assertCount(3, $borrowerInvoice->billItems);
        self::assertEquals(
            -5,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanInsurance
            )
        );
        self::assertEquals(
            -7,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanPrice
            )
        );
        self::assertEquals(
            -3,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::donationLoan
            )
        );
    }

    public function testPricingBillItemGeneration_firstContributionDollarsToGlobal()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner()
                ->withTrailer(),
            "platform_tip" => 10,
        ])
            ->confirmed()
            ->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "rule" => "7",
            ]);

        $localPricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $loan->community_id,
                "pricing_type" => "contribution",
                "rule" => "5",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();
        self::assertCount(2, $borrowerInvoice->billItems);
        $globalContribution = $borrowerInvoice->billItems
            ->whereNull("contribution_community_id")
            ->first();
        $localContribution = $borrowerInvoice->billItems
            ->whereNotNull("contribution_community_id")
            ->first();

        self::assertEquals(-3, $localContribution->total);
        self::assertEquals(
            $loan->community_id,
            $localContribution->contribution_community_id
        );
        self::assertEquals(
            $localPricing->id,
            $localContribution->meta["pricing_id"]
        );
        self::assertEquals(-7, $globalContribution->total);
    }

    public function testPricingBillItemGeneration_communityReceivesContributionEvenWithoutPricing()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner()
                ->withTrailer(),
            "platform_tip" => 10,
        ])
            ->ongoing()
            ->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "rule" => "7",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();
        self::assertCount(2, $borrowerInvoice->billItems);
        $globalContribution = $borrowerInvoice->billItems
            ->whereNull("contribution_community_id")
            ->first();
        $localContribution = $borrowerInvoice->billItems
            ->whereNotNull("contribution_community_id")
            ->first();

        self::assertEquals(-3, $localContribution->total);
        self::assertEquals(
            $loan->community_id,
            $localContribution->contribution_community_id
        );
        self::assertNull($localContribution->meta["pricing_id"]);
        self::assertEquals(-7, $globalContribution->total);
    }

    public function testPricingBillItemsGeneration_communityPriceInsuranceOverridesGlobal()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()
                ->withOwner()
                ->withTrailer(),
            "platform_tip" => 3,
        ])
            ->confirmed()
            ->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "price",
                "rule" => "7",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $loan->community_id,
                "pricing_type" => "price",
                "rule" => "13",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $loan->community_id,
                "pricing_type" => "insurance",
                "rule" => "17",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();
        // 3 = since insurance, price and donation
        self::assertCount(3, $borrowerInvoice->billItems);
        self::assertEquals(
            -17,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanInsurance
            )
        );
        self::assertEquals(
            -13,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanPrice
            )
        );
    }

    public function testCreatePricing_possibleForAdminsOfCommunity()
    {
        $community = Community::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);
        $this->json("put", route("pricings.create"), [
            "name" => "foo",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(201);
    }

    public function testCreatePricing_mustStartInTheFuture()
    {
        $community = Community::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $today = Carbon::today(config("app.default_user_timezone"))->format(
            "Y-m-d"
        );

        $this->actAs($communityAdmin);
        $this->json("put", route("pricings.create"), [
            "name" => "foo",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()->format("Y-m-d"),
        ])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le champ date de début doit être une date postérieure au $today.",
            ]);

        $this->json("put", route("pricings.create"), [
            "name" => "foo",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->subDay()
                ->format("Y-m-d"),
        ])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le champ date de début doit être une date postérieure au $today.",
            ]);
    }

    public function testCreatePricing_endMustBeAfterStart()
    {
        $community = Community::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);
        $this->json("put", route("pricings.create"), [
            "name" => "foo",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDays(5)
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addDays(2)
                ->format("Y-m-d"),
        ])
            ->assertStatus(422)
            ->assertJson([
                "message" =>
                    "Le champ date de fin doit être une date postérieure au date de début.",
            ]);
    }

    public function testCreatePricing_deniedForRandomUser()
    {
        $community = Community::factory()->create();
        $randomUser = User::factory()->create();

        $this->actAs($randomUser);
        $this->json("put", route("pricings.create"), [
            "name" => "foo",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
    }

    public function testUpdatePricing_possibleForAdminsOfCommunity()
    {
        $community = Community::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "name" => "foo",
                "pricing_type" => "price",
                "rule" => "10",
                "loanable_ownership_type" => "all",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);

        $this->actAs($communityAdmin);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $pricing->refresh();
        self::assertEquals(
            [PricingLoanableTypeValues::CarSmall],
            $pricing->pricingLoanableTypes
        );
    }

    public function testUpdatePricing_deniedForRandomUser()
    {
        $community = Community::factory()->create();
        $randomUser = User::factory()->create();
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "name" => "foo",
                "pricing_type" => "price",
                "rule" => "10",
                "loanable_ownership_type" => "all",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);

        $this->actAs($randomUser);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
    }

    public function testUpdatePricing_cannotUpdateCommunityId()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "name" => "foo",
                "pricing_type" => "price",
                "rule" => "10",
                "loanable_ownership_type" => "all",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);

        $this->actAs($communityAdmin);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
            "pricing_type" => "price",
            "rule" => "10",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "all",
            "community_id" => $otherCommunity->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);
    }

    public function testUpdatePricing_preventsOverlaps()
    {
        $community = Community::factory()->create();
        $overlappedPricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "price",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "pricing_type" => "insurance",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            // Exact overlap
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.update", $pricing), [
            // Overlaps since all includes fleet
            "loanable_ownership_type" => "all",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.update", $pricing), [
            // Doesn't overlap (non_fleet vs fleet)
            "loanable_ownership_type" => "non_fleet",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $this->json("put", route("pricings.update", $pricing), [
            // Doesn't overlap (car_large vs car_small)
            "pricing_loanable_types" => ["car_large"],
            "loanable_ownership_type" => "fleet",
            "pricing_type" => "price",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $this->json("put", route("pricings.update", $pricing), [
            // Doesn't overlap (price vs insurance)
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $overlappedPricing->delete();
        $overlappedPricing = Pricing::factory()
            ->forAllTypes()
            ->create([
                "pricing_type" => "price",
                "loanable_ownership_type" => "all",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addMonth()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addYear()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            // Partial overlap forbidden
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.update", $pricing), [
            // Partial overlap forbidden
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "non_fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.update", $pricing), [
            // Doesn't overlap (insurance vs price)
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "non_fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        // partial date overlap
        $this->json("put", route("pricings.update", $pricing), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addMonths(2)
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.update", $pricing), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addMonths(2)
                ->format("Y-m-d"),
        ])->assertStatus(422);

        // No date overlap
        $this->json("put", route("pricings.update", $pricing), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addDays(4)
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $this->json("put", route("pricings.update", $pricing), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addYear()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addYears(2)
                ->format("Y-m-d"),
        ])->assertStatus(200);
    }

    public function testCreatePricing_preventsOverlaps()
    {
        $community = Community::factory()->create();
        $overlappedPricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "price",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addYear()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.create"), [
            // Exact overlap
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.create"), [
            // Overlaps since all includes fleet
            "loanable_ownership_type" => "all",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $overlappedPricing->delete();
        $overlappedPricing = Pricing::factory()
            ->forAllTypes()
            ->create([
                "pricing_type" => "price",
                "loanable_ownership_type" => "all",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addMonth()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addYear()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.create"), [
            // Partial overlap forbidden
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.create"), [
            // Partial overlap forbidden
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "non_fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.create"), [
            // Doesn't overlap (insurance vs price)
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "non_fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(201);

        // partial date overlap
        $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addMonths(2)
                ->format("Y-m-d"),
        ])->assertStatus(422);

        $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addMonths(2)
                ->format("Y-m-d"),
        ])->assertStatus(422);

        // No date overlap
        $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addDays(4)
                ->format("Y-m-d"),
        ])->assertStatus(201);

        $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addYear()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addYears(2)
                ->format("Y-m-d"),
        ])->assertStatus(201);
    }

    public function testPricingTypeslimitedForPublicCommunityAdmin()
    {
        $community = Community::factory()->create([
            "type" => "borough",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $response = $this->json("put", route("pricings.create"), [
            "pricing_type" => "contribution",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "foo",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(201);
        $pricingId = $response->json("id");
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "contribution",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        // Cannot change type to insurance or price
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);

        $this->json(
            "delete",
            route("pricings.destroy", $pricingId)
        )->assertStatus(200);

        // price type is forbidden for community admins in public community
        $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "price",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
        ])->assertStatus(403);
        $this->json(
            "delete",
            route("pricings.destroy", $pricing)
        )->assertStatus(403);

        // insurance type is forbidden for community admins in public community
        $this->json("put", route("pricings.create"), [
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "insurance",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
        ])->assertStatus(403);
        $this->json(
            "delete",
            route("pricings.destroy", $pricing)
        )->assertStatus(403);
    }

    public function testPricingTypeslimitedForPrivateCommunityAdmin()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $response = $this->json("put", route("pricings.create"), [
            "pricing_type" => "contribution",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "foo",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(201);
        $pricingId = $response->json("id");
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "contribution",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);
        // Can change type to price in private community
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);
        // Cannot change type to insurance
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);

        $this->json(
            "delete",
            route("pricings.destroy", $pricingId)
        )->assertStatus(200);

        // price type is allowed  for community admins in private community
        $response = $this->json("put", route("pricings.create"), [
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "foo",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(201);
        $pricingId = $response->json("id");
        $this->json("put", route("pricings.update", $pricingId), [
            "name" => "bar",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(200);
        $this->json(
            "delete",
            route("pricings.destroy", $pricingId)
        )->assertStatus(200);

        // insurance type is forbidden for community admins in public community
        $this->json("put", route("pricings.create"), [
            "pricing_type" => "insurance",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "name" => "bar",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])->assertStatus(403);
        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "insurance",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d"),
            ]);
        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
        ])->assertStatus(403);
        $this->json(
            "delete",
            route("pricings.destroy", $pricing)
        )->assertStatus(403);
    }

    public function testDelete_forbiddenForStartedPricings()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "contribution",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->subDay()
                    ->format("Y-m-d"),
            ]);

        $today = Carbon::today(config("app.default_user_timezone"))->format(
            "Y-m-d"
        );

        $this->json("delete", route("pricings.destroy", $pricing))
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le champ date de début doit être une date postérieure au $today.",
            ]);
    }

    public function testUpdate_forbiddenForPastPricings()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "contribution",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->subWeek()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->subDay()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            "name" => "bar",
            "pricing_type" => "price",
            "pricing_loanable_types" => ["car_small"],
            "loanable_ownership_type" => "fleet",
            "rule" => "10",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addDay()
                ->format("Y-m-d"),
        ])
            ->assertStatus(503)
            ->assertJson([
                "message" => "Cannot change pricing that has expired",
            ]);
    }

    public function testUpdate_limitedForStartedPricings()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "name" => "foo",
                "description" => "bar",
                "rule" => "10",
                "is_mandatory" => false,
                "yearly_target_per_user" => 13,
                "pricing_type" => "contribution",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->subWeek()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addWeek()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            "name" => "foo",
            "description" => "bar",
            "rule" => "100",
            "is_mandatory" => true,
            "yearly_target_per_user" => 15,
            "pricing_loanable_types" => ["car_large"],
            "pricing_type" => "contribution",
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->subWeeks(2)
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addWeek()
                ->format("Y-m-d"),
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "rule" => ["Le champ règle ne peut être modifié."],
                    "loanable_ownership_type" => [
                        "Le champ type de partage du véhicule ne peut être modifié.",
                    ],
                    "yearly_target_per_user" => [
                        "Le champ cible de contribution annuelle ne peut être modifié.",
                    ],
                    "is_mandatory" => [
                        "Le champ contribution obligatoire ne peut être modifié.",
                    ],
                    "pricing_loanable_types" => [
                        "Le champ types de véhicule ne peut être modifié.",
                    ],
                    "start_date" => [
                        "Le champ date de début ne peut être modifié.",
                    ],
                ],
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            "name" => "baz",
            "description" => "quux",
            "rule" => "10",
            "is_mandatory" => false,
            "yearly_target_per_user" => 13,
            "pricing_loanable_types" => ["car_small"],
            "pricing_type" => "contribution",
            "loanable_ownership_type" => "fleet",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->subWeek()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addWeeks(2)
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $pricing->refresh();
        self::assertEquals("baz", $pricing->name);
        self::assertEquals("quux", $pricing->description);
        self::assertEquals(
            Carbon::now()
                ->addWeeks(2)
                ->format("Y-m-d"),
            $pricing->end_date
        );
    }

    public function testUpdate_canChangeStartDateOfFuturePricing()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "name" => "foo",
                "description" => "bar",
                "rule" => "10",
                "is_mandatory" => false,
                "yearly_target_per_user" => 13,
                "pricing_type" => "contribution",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addWeek()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addMonth()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            "name" => "foo",
            "description" => "bar",
            "rule" => "100",
            "is_mandatory" => true,
            "yearly_target_per_user" => 15,
            "pricing_loanable_types" => ["car_large"],
            "pricing_type" => "contribution",
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->addWeeks(2)
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addMonth()
                ->format("Y-m-d"),
        ])->assertStatus(200);

        $pricing->refresh();
        self::assertEquals(
            Carbon::now()
                ->addWeeks(2)
                ->format("Y-m-d"),
            $pricing->start_date
        );
    }
    public function testUpdate_cannotMakeStartDateOfFuturePricingInThePast()
    {
        $community = Community::factory()->create([
            "type" => "private",
        ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);

        $pricing = Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "name" => "foo",
                "description" => "bar",
                "rule" => "10",
                "is_mandatory" => false,
                "yearly_target_per_user" => 13,
                "pricing_type" => "contribution",
                "loanable_ownership_type" => "fleet",
                "community_id" => $community->id,
                "start_date" => Carbon::now()
                    ->addWeek()
                    ->format("Y-m-d"),
                "end_date" => Carbon::now()
                    ->addMonth()
                    ->format("Y-m-d"),
            ]);

        $this->json("put", route("pricings.update", $pricing), [
            "name" => "foo",
            "description" => "bar",
            "rule" => "100",
            "is_mandatory" => true,
            "yearly_target_per_user" => 15,
            "pricing_loanable_types" => ["car_large"],
            "pricing_type" => "contribution",
            "loanable_ownership_type" => "all",
            "community_id" => $community->id,
            "start_date" => Carbon::now()
                ->subWeek()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->addMonth()
                ->format("Y-m-d"),
        ])->assertStatus(422);
    }
    public function testFleetPricingApplyForLoanLoanablesInLibraries()
    {
        self::setTestNow("2024-07-01");

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory([
                    "library_id" => Library::factory()->withOwner(),
                ])->withTrailer(),
                "platform_tip" => 0,
                "departure_at" => (new Carbon(
                    "2024-07-04 13:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 3 * 24 * 60 + 11 * 60, // ends on midnight on the 8th, so only 4 days
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "price",
                "rule" => "5 * \$EMPRUNT.days",
                "start_date" => "2024-06-05",
                "end_date" => "2024-07-12",
                "loanable_ownership_type" => "fleet",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();

        self::assertCount(1, $borrowerInvoice->billItems);
        self::assertEquals(
            -20,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanPrice
            )
        );

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                // no library
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
                "platform_tip" => 0,
                "departure_at" => (new Carbon(
                    "2024-07-04 13:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 3 * 24 * 60 + 11 * 60, // ends on midnight on the 8th, so only 4 days
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();
        self::assertCount(0, $borrowerInvoice->billItems);
    }

    public function testMultipleInsurancePricingsForSameLoan()
    {
        self::setTestNow("2024-07-01");

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
                "platform_tip" => 0,
                "departure_at" => (new Carbon(
                    "2024-07-04 13:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 3 * 24 * 60 + 11 * 60, // ends on midnight on the 8th, so only 4 days
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "5 * \$EMPRUNT.days",
                "start_date" => "2024-07-02",
                "end_date" => "2024-07-05",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "2 * \$EMPRUNT.days",
                "start_date" => "2024-07-05",
                "end_date" => "2024-07-07",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "12 * \$EMPRUNT.days",
                "start_date" => "2024-07-07",
                "end_date" => null,
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();

        self::assertCount(3, $borrowerInvoice->billItems);
        self::assertEquals(
            -21,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanInsurance
            )
        );
    }

    public function testMultipleInsurancePricingsForSameLoan_prefersCommunityInsuranceWhenAvailable()
    {
        self::setTestNow("2024-07-01");

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
                "platform_tip" => 0,
                "departure_at" => (new Carbon(
                    "2024-07-04 00:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 4 * 24 * 60,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => null,
                "pricing_type" => "insurance",
                "rule" => "5 * \$EMPRUNT.days",
                "start_date" => "2024-07-02",
                "end_date" => null,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $loan->community_id,
                "pricing_type" => "insurance",
                "rule" => "2 * \$EMPRUNT.days",
                "start_date" => "2024-07-05",
                "end_date" => "2024-07-07",
            ]);

        $borrowerInvoice = $loan->getBorrowerInvoice();

        self::assertCount(3, $borrowerInvoice->billItems);
        // 1 day at 5$, 2 days at 2$, last day falls back to 5$
        self::assertEquals(
            -14,
            $borrowerInvoice->getUserBalanceChangeForType(
                BillItemTypes::loanInsurance
            )
        );
    }
}
