<?php

namespace Tests\Integration;

use App\Models\Community;
use App\Models\Invitation;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class InvitationTest extends TestCase
{
    public function testCreateInvitation_successAsGlobalAdmin()
    {
        $admin = User::factory()->create([
            "role" => "admin",
        ]);

        $community = Community::factory()->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(201);
        self::assertEquals(1, Invitation::count());
        self::assertEquals(
            [
                "email" => "some@fake.email",
                "community_id" => $community->id,
                "reason" => "Added because I trust them",
                "expires_at" => $inTwoWeeks->toDateString(),
            ],
            Invitation::select([
                "email",
                "community_id",
                "reason",
                "expires_at",
            ])
                ->first()
                ->toArray()
        );
    }

    public function testCreateInvitation_doesntCreateIfUserAlreadyInCommunity()
    {
        $community = Community::factory()->create();
        User::factory()
            ->withCommunity($community)
            ->create(["email" => "some@email.com"]);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "email" => "some@email.com",
        ]);

        $response->assertStatus(204);
        self::assertEquals(0, Invitation::count());
    }

    public function testCreateInvitationBatch_onlyCreatesForNotYetAddedUsers()
    {
        $community = Community::factory()->create();
        User::factory()
            ->withCommunity($community)
            ->create(["email" => "some@email.com"]);
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "emails" => "some@email.com,someother@email.com",
        ]);

        $response->assertStatus(200);
        self::assertEquals(1, Invitation::count());
        self::assertEquals(
            "someother@email.com",
            Invitation::pluck("email")[0]
        );
    }

    public function testCreateInvitation_requiresEmail()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.email.0",
            "Le champ adresse email est obligatoire."
        );
    }

    public function testCreateInvitation_emailMustBeValid()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "email" => "invalid email",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.email.0",
            "Le champ adresse email doit être une adresse email valide."
        );
    }

    public function testCreateInvitation_requiresCommunityId()
    {
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "reason" => "Added because I trust them",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.community_id.0",
            "Le champ community id est obligatoire."
        );
    }

    public function testCreateInvitation_requiresNoteWhenAutoApprove()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "auto_approve" => true,
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.reason.0",
            "Le champ note pour le comité est obligatoire quand la valeur de pré-approbation est acceptée."
        );
    }

    public function testCreateInvitationBatch_requiresEmail()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.emails.0",
            "Le champ emails est obligatoire."
        );
    }

    public function testCreateInvitationBatch_invalidEmail()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "emails" => "validemail@email.fake,invalidemail",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.emails.0",
            "Le champ emails[1] doit être une adresse email valide."
        );
    }

    public function testCreateInvitationBatch_requiresCommunityId()
    {
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email",
            "reason" => "Added because I trust them",
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.community_id.0",
            "Le champ community id est obligatoire."
        );
    }

    public function testCreateInvitationBatch_requiresNote()
    {
        $community = Community::factory()->create();
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email",
            "community_id" => $community->id,
            "auto_approve" => true,
        ]);

        $response->assertStatus(422);
        $response->assertJsonPath(
            "errors.reason.0",
            "Le champ note pour le comité est obligatoire quand la valeur de pré-approbation est acceptée."
        );
    }

    public function testCreateInvitation_successAsCommunityAdminInOwnCommunity()
    {
        $community = Community::factory()->create();
        $admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(201);
    }

    public function testCreateInvitation_forbiddenAsCommunityAdminInDifferentCommunity()
    {
        $community = Community::factory()->create();
        $admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $otherCommunity = Community::factory()->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $otherCommunity->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(403);
    }

    public function testCreateInvitation_forbiddenAsRegularUser()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $this->actAs($user);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(403);
    }

    public function testCreateInvitationBatch_successAsGlobalAdmin()
    {
        $admin = User::factory()->create([
            "role" => "admin",
        ]);

        $community = Community::factory()->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()->addWeeks(2);
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email,someotheremail@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(200);
        self::assertEquals(2, Invitation::count());
        $this->assertJsonUnordered(
            Invitation::select([
                "email",
                "community_id",
                "reason",
                "expires_at",
            ])
                ->get()
                ->toArray(),
            [
                [
                    "email" => "some@fake.email",
                    "community_id" => $community->id,
                    "reason" => "Added because I trust them",
                    "expires_at" => $inTwoWeeks->toDateString(),
                ],
                [
                    "email" => "someotheremail@fake.email",
                    "community_id" => $community->id,
                    "reason" => "Added because I trust them",
                    "expires_at" => $inTwoWeeks->toDateString(),
                ],
            ]
        );
    }

    public function testCreateInvitationBatch_successAsCommunityAdminInOwnCommunity()
    {
        $community = Community::factory()->create();
        $admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email,someotheremail@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(200);
    }

    public function testCreateInvitationBatch_forbiddenAsCommunityAdminInDifferentCommunity()
    {
        $community = Community::factory()->create();
        $admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $otherCommunity = Community::factory()->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email,someotheremail@fake.email",
            "community_id" => $otherCommunity->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(403);
    }

    public function testCreateInvitationBatch_forbiddenAsRegularUser()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $this->actAs($user);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations/batch", [
            "emails" => "some@fake.email,someotheremail@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(403);
    }

    public function testCreateInvitation_generatesCode()
    {
        $admin = User::factory()->create([
            "role" => "admin",
        ]);

        $community = Community::factory()->create();

        $this->actAs($admin);
        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
        ]);

        $response->assertStatus(201);
        self::assertEquals(
            32,
            strlen(Invitation::pluck("confirmation_code")[0])
        );
    }

    public function testCreateInvitation_defaultsTo1MonthExpiration()
    {
        $admin = User::factory()->create([
            "role" => "admin",
        ]);

        $community = Community::factory()->create();

        $this->actAs($admin);
        $now = Carbon::now();
        self::setTestNow($now);
        $response = $this->json("PUT", "/api/v1/invitations", [
            "email" => "some@fake.email",
            "community_id" => $community->id,
            "reason" => "Added because I trust them",
        ]);

        $response->assertStatus(201);
        self::assertEquals(
            $now->addMonth()->startOfDay(),
            Invitation::first()->expires_at
        );
    }

    public function testListInvitations_successAsGlobalAdmin()
    {
        $admin = User::factory()->create([
            "role" => "admin",
        ]);
        Invitation::factory()->create();
        Invitation::factory()->create();

        $this->actAs($admin);
        $response = $this->json("GET", "/api/v1/invitations");

        $response->assertStatus(200);
        $response->assertJsonCount(2, "data");
    }

    public function testListInvitations_showsOnlyAccessibleInvitationsAsAdminOfCommunity()
    {
        $community = Community::factory()->create();
        $admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        Invitation::factory([
            "community_id" => $community,
        ])
            ->count(2)
            ->create();
        Invitation::factory()->create();

        $this->actAs($admin);
        $response = $this->json("GET", "/api/v1/invitations");

        $response->assertStatus(200);
        $response->assertJsonCount(2, "data");
    }

    public function testListInvitations_forbiddenAsRegularUser()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        Invitation::factory([
            "community_id" => $community,
        ])
            ->count(2)
            ->create();
        Invitation::factory()->create();

        $this->actAs($user);
        $response = $this->json(
            "GET",
            "/api/v1/invitations?community_id=$community->id"
        );

        $response->assertStatus(403);
    }

    public function testValidateInvitation_returns404ForMissingInvitation()
    {
        $this->json("GET", "/api/v1/invitations/validate/0")->assertStatus(404);
    }

    public function testValidateInvitation_returns429whenOver30RequestsPerMinute()
    {
        $invitation = Invitation::factory()->create();
        // To avoid tripping other tests, we set the time in the future
        self::setTestNow(Carbon::now()->addYear());
        for ($i = 0; $i < 30; $i++) {
            $this->json("GET", "/api/v1/invitations/validate/$invitation->id");
        }

        $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        )->assertStatus(429);
    }

    public function testValidateInvitation_validatesCode()
    {
        $invitation = Invitation::factory()->create();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?code=$invitation->confirmation_code"
        );
        $response->assertStatus(200)->assertJson([
            "code_valid" => true,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?code=wrongCode"
        );
        $response->assertStatus(200)->assertJson([
            "code_valid" => false,
        ]);
    }

    public function testValidateInvitation_validatesEmail()
    {
        $invitation = Invitation::factory()->create();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?email=$invitation->email"
        );
        $response->assertStatus(200)->assertJson([
            "email_valid" => true,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?email=wrongEmail"
        );
        $response->assertStatus(200)->assertJson([
            "email_valid" => false,
        ]);
    }

    public function testValidateInvitation_validatesStatus()
    {
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        );
        $response->assertStatus(200)->assertJson([
            "is_active" => true,
        ]);

        $invitation->accepted_at = Carbon::now();
        $invitation->save();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        );
        $response->assertStatus(200)->assertJson([
            "is_active" => false,
        ]);

        $invitation->accepted_at = null;
        $invitation->refused_at = Carbon::now();
        $invitation->save();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        );
        $response->assertStatus(200)->assertJson([
            "is_active" => false,
        ]);

        $invitation->refused_at = null;
        $invitation->deleted_at = Carbon::now();
        $invitation->save();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        );
        $response->assertStatus(404);

        $invitation->deleted_at = null;
        $invitation->expires_at = Carbon::now()->subDay();
        $invitation->save();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id"
        );
        $response->assertStatus(200)->assertJson([
            "is_active" => false,
        ]);
    }

    public function testValidateInvitation_showsWhetherAccountExistsOnlyWhenCodeValidAndActive()
    {
        $invitation = Invitation::factory()->create();

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?email=$invitation->email"
        );
        $response->assertStatus(200)->assertJsonMissing([
            "account_exists" => false,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?code=$invitation->confirmation_code"
        );
        $response->assertStatus(200)->assertJson([
            "account_exists" => false,
        ]);

        User::factory()->create([
            "email" => $invitation->email,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/invitations/validate/$invitation->id?code=$invitation->confirmation_code"
        );
        $response->assertStatus(200)->assertJson([
            "account_exists" => true,
        ]);
    }

    public function testAcceptSingle_returns404ifMissing()
    {
        $this->json("POST", "/api/v1/invitations/0/accept", [
            "code" => "someCode",
        ])->assertStatus(404);
    }

    public function testAcceptSingle_returns403IfNotActive()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->subDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(403);
    }

    public function testAcceptSingle_returns422IfCodeMissing()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->subDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept")
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "code" => ["Le champ code est obligatoire."],
                ],
            ]);
    }

    public function testAcceptSingle_returns403IfWrongCode()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept", [
            "code" => "wrongCode",
        ])->assertStatus(403);
    }

    public function testAcceptSingle_addsUserToCommunity()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(200);

        $user->refresh();

        self::assertEquals(1, $user->communities->count());
    }

    public function testAcceptSingle_acceptsAllInvitationForTheSameCommunity()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
            "auto_approve" => false,
        ]);

        Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
            "auto_approve" => true,
        ]);

        Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $otherCommunity,
            "auto_approve" => false,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(200);

        $user->refresh();

        // User isn't added to $otherCommunity.
        self::assertEquals(1, $user->communities->count());
        // User is accepted in $community even if the accepted invite (id and code) didn't have
        // auto-approve, since another invitation did.
        self::assertEquals(1, $user->approvedCommunities->count());
    }

    public function testAcceptSingle_worksWithoutCodeIsAlreadyVerifiedEmail()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "email_verified_at" => Carbon::now(),
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
        ]);

        $this->actAs($user);

        $this->json(
            "POST",
            "/api/v1/invitations/$invitation->id/accept"
        )->assertStatus(200);
    }

    public function testAcceptSingle_marksEmailAsValidated()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json("POST", "/api/v1/invitations/$invitation->id/accept", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(200);

        $user->refresh();

        self::assertNotNull($user->email_verified_at);
    }

    public function testRefuseSingle_returns404ifMissing()
    {
        $this->json("POST", "/api/v1/invitations/0/refuse", [
            "code" => "someCode",
        ])->assertStatus(404);
    }

    public function testRefuseSingle_returns403IfNotActive()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->subDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/refuse", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(403);
    }

    public function testRefuseSingle_returns422IfCodeMissing()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->subDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/refuse")
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "code" => ["Le champ code est obligatoire."],
                ],
            ]);
    }

    public function testRefuseSingle_returns403IfWrongCode()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        $this->json("POST", "/api/v1/invitations/$invitation->id/refuse", [
            "code" => "wrongCode",
        ])->assertStatus(403);
    }

    public function testRefuseSingle_marksInvitationAsRefused()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json("POST", "/api/v1/invitations/$invitation->id/refuse", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(200);

        $user->refresh();
        self::assertEquals(0, $user->communities->count());

        $invitation->refresh();
        self::assertNotNull($invitation->refused_at);
    }

    public function testRefuseSingle_worksWithoutCodeIfEmailValidated()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "email_verified_at" => Carbon::now(),
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
            "community_id" => $community,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json(
            "POST",
            "/api/v1/invitations/$invitation->id/refuse"
        )->assertStatus(200);

        $user->refresh();
        self::assertEquals(0, $user->communities->count());

        $invitation->refresh();
        self::assertNotNull($invitation->refused_at);
    }

    public function testRefuseSingle_marksEmailAsValidated()
    {
        $user = User::factory()->create([
            "email_verified_at" => null,
        ]);
        $invitation = Invitation::factory()->create([
            "expires_at" => Carbon::now()->addDay(),
            "email" => $user->email,
        ]);

        $this->actAs($user);

        self::assertEquals(0, $user->communities->count());

        $this->json("POST", "/api/v1/invitations/$invitation->id/refuse", [
            "code" => $invitation->confirmation_code,
        ])->assertStatus(200);

        $user->refresh();

        self::assertNotNull($user->email_verified_at);
    }

    public function testAcceptAll_returns204IfNoInvitationsToAccept()
    {
        $this->json(
            "POST",
            "/api/v1/invitations/accept/registration"
        )->assertStatus(204);
    }

    public function testAcceptOnRegistration_acceptsCorrectInvitations()
    {
        $communityA = Community::factory()->create();
        $communityB = Community::factory()->create();

        $admin = User::factory()
            ->adminOfCommunity($communityA)
            ->adminOfCommunity($communityB)
            ->create();

        $email = "foobar@example.com";

        $this->actAs($admin);

        $inTwoWeeks = Carbon::now()
            ->addWeeks(2)
            ->micros(0);

        $this->json("PUT", "/api/v1/invitations", [
            "email" => $email,
            "community_id" => $communityA->id,
            "reason" => "Added because I trust them",
            "expires_at" => $inTwoWeeks,
            "auto_approve" => true,
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/invitations", [
            "email" => $email,
            "community_id" => $communityB->id,
            "auto_approve" => false,
            "expires_at" => $inTwoWeeks,
        ])->assertStatus(201);
        $this->json("PUT", "/api/v1/invitations", [
            "email" => $email,
            "community_id" => $communityB->id,
            "reason" => "Actually I want them auto approved",
            "expires_at" => $inTwoWeeks,
            "auto_approve" => true,
        ])->assertStatus(201);

        $firstCommunityBInvitation = Invitation::where("email", $email)
            ->where("community_id", $communityB->id)
            ->where("auto_approve", false)
            ->first();

        $this->actAs(null);

        $response = $this->json("POST", "/api/v1/auth/register", [
            "email" => $email,
            "password" => "abcd1234",
            "code" => $firstCommunityBInvitation->confirmation_code,
        ])
            ->assertStatus(200)
            ->assertJsonStructure(["access_token"]);

        $firstCommunityBInvitation->refresh();
        self::assertTrue($firstCommunityBInvitation->validated_on_registration);

        $userResponse = $this->json(
            "GET",
            "/api/v1/auth/user",
            [],
            [
                "Authorization" => "Bearer {$response->json("access_token")}",
            ]
        );

        $user = User::find($userResponse->json("id"));
        self::assertCount(0, $user->communities);

        $this->actAs($user);

        $this->json(
            "POST",
            "/api/v1/invitations/accept/registration"
        )->assertStatus(200);

        $user->refresh();

        self::assertCount(1, $user->communities);
        self::assertCount(1, $user->approvedCommunities);
        self::assertEquals($communityB->id, $user->approvedCommunities[0]->id);
    }

    public function testAcceptInvitationOnRegistration_isStillPossibleWhenExpiresNull()
    {
        $communityA = Community::factory()->create();
        $communityB = Community::factory()->create();

        Invitation::factory()->create([
            "email" => "foo@bar.com",
            "community_id" => $communityA->id,
            "reason" => "this is great",
            "auto_approve" => true,
            // User has used the code during registration, so it is pre-approved
            "validated_on_registration" => true,
            "expires_at" => null,
        ]);
        $invitationToB = Invitation::factory()->create([
            "email" => "foo@bar.com",
            "community_id" => $communityB->id,
            "auto_approve" => false,
            "expires_at" => Carbon::now()->addWeek(),
            // User has used the code during registration, so it is pre-approved
        ]);

        $user = User::factory()->create([
            "email" => "foo@bar.com",
        ]);

        $this->actAs($user);
        self::setTestNow(Carbon::now()->addMonth());

        $this->json(
            "POST",
            "/api/v1/invitations/accept/registration"
        )->assertStatus(200);

        $this->json("POST", "/api/v1/invitations/$invitationToB->id/accept", [
            "code" => $invitationToB->confirmation_code,
        ])->assertStatus(403);

        $user->refresh();

        self::assertCount(1, $user->approvedCommunities);
        self::assertEquals($communityA->id, $user->approvedCommunities[0]->id);
    }
}
