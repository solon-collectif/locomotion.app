<?php

namespace Tests\Integration;

use App\Enums\BillItemTypes;
use App\Enums\PricingLoanableTypeValues;
use App\Models\BillItem;
use App\Models\Community;
use App\Models\Invoice;
use App\Models\Pivots\CommunityUser;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    public function testPaySubscription()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 15,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        self::assertEquals(0, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEquals(
            [PricingLoanableTypeValues::CarSmall],
            $user->subscriptions->first()->loanable_types
        );
    }

    public function testPaySubscription_forMultiplePricings()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 30,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "7",
            ]);

        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small", "car_large"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        // 30 - 5 - 7 - 10 = 8
        self::assertEquals(8, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::CarLarge,
            ],
            $user->subscriptions->first()->loanable_types
        );
    }

    public function testPaySubscription_forMultipleLoanables()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 30,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
                "loanable_ownership_type" => "fleet",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "6",
                "loanable_ownership_type" => "non_fleet",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "7",
                "loanable_ownership_type" => "non_fleet",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::BikeRegular)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "9",
                "loanable_ownership_type" => "all",
            ]);

        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            // Car small should pay for car_large as well since they share the exact same pricings
            "pricing_loanable_types" => ["car_small"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        // 30 - 5 - 7 - 10 = 8
        self::assertEquals(8, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::CarLarge,
            ],
            $user->subscriptions->first()->loanable_types
        );
    }

    public function testPaySubscription_notEnoughMoney()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 14,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(422)->assertJson([
            "message" =>
                "Votre solde n'est pas assez élevé pour payer la contribution annuelle.",
        ]);
    }

    public function testPaySubscription_withNoCommunityPricing_createsNoSubscription()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 30,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::BikeRegular)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small", "car_large"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(422)->assertJson([
            "message" =>
                "Aucune contribution annuelle disponible pour petite auto, grosse auto.",
        ]);
    }

    public function testPaySubscription_doesntReplaceActiveGrantedSubscription()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "balance" => 20,
        ]);
        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->subMonth(),
                "end_date" => Carbon::now()->addMonth(),
                "type" => "granted",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        self::assertCount(2, $user->subscriptions);
        $paymentSubscription = $user->subscriptions
            ->where("type", "paid")
            ->first();
        self::assertEquals(
            $community->id,
            $paymentSubscription->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [PricingLoanableTypeValues::CarSmall],
            $paymentSubscription->loanable_types
        );
    }

    public function testPaySubscription_cannotReduceExistingSubscription()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create();

        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->subMonth(),
                "end_date" => Carbon::now()->addMonth(),
                "type" => "paid",
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small"],
            "community_id" => $community->id,
        ]);

        $response->assertStatus(422)->assertJson([
            "message" =>
                "Impossible de réduire la contribution actuelle. Incluez les types suivants: grosse auto.",
        ]);
    }

    public function testPaySubscription_subtractsExistingSubscriptionCost()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "balance" => 100,
        ]);
        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "7",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "3",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->forType(PricingLoanableTypeValues::BikeElectric)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        $now = CarbonImmutable::now();
        self::setTestNow($now);

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_user_id" => $communityUser,
                "type" => "paid",

                // Elapsed days: 200
                "start_date" => Carbon::now(config("app.default_user_timezone"))
                    ->subDays(200)
                    ->startOfDay()
                    ->toISOString(),

                // Remaining days: today + next 99 = 100
                "end_date" => Carbon::now(config("app.default_user_timezone"))
                    ->addDays(100)
                    ->startOfDay()
                    ->toISOString(),
                // Invoice for a total of 15$ paid as contributions
                "invoice_id" => Invoice::factory()
                    ->has(
                        BillItem::factory([
                            "item_type" => BillItemTypes::contributionYearly,
                            "amount" => -3,
                            "taxes_tvq" => -1,
                            "taxes_tps" => -1,
                            "contribution_community_id" => $community,
                        ])
                    )
                    ->has(
                        BillItem::factory([
                            "item_type" => BillItemTypes::contributionYearly,
                            "amount" => -8,
                            "taxes_tvq" => -1,
                            "taxes_tps" => -1,
                            "contribution_community_id" => null,
                        ])
                    ),
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small", "car_large"],
            "community_id" => $community->id,
        ]);
        $user->refresh();

        // Total price (with taxes):
        // new subscription: 10 + 7 + 5 = 22
        //  - partial reinbursement for previous subscription: 1 + 0.33 + 0.33 + 2.67 + 0.33 + 0.33 = 4.99
        // Total: 17.01
        self::assertEquals(82.99, $user->balance); // There might be a few cents difference due to rounding
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::CarLarge,
            ],
            $user->subscriptions->first()->loanable_types
        );
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => [
                "car_small",
                "car_large",
                "car_small_electric",
                "car_large_electric",
            ],
            "community_id" => $community->id,
        ]);

        $user->refresh();

        // Have only added car_*_electric, for the exact same duration as previous subscription, which
        // should be reinbursed in full
        self::assertEquals(/* 82.99 - 3 = */ 79.99, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::CarLarge,
                PricingLoanableTypeValues::CarSmallElectric,
                PricingLoanableTypeValues::CarLargeElectric,
            ],
            $user->subscriptions->first()->loanable_types
        );
    }

    public function testPaySubscription_shouldNotBeNegative()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "balance" => 20,
        ]);
        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "5",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => $community->id,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "7",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_id" => null,
                "pricing_type" => "contribution",
                "yearly_target_per_user" => "10",
            ]);

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_user_id" => $communityUser,
                "type" => "paid",
                "start_date" => Carbon::now(config("app.default_user_timezone"))
                    ->subDays(200)
                    ->startOfDay()
                    ->toISOString(),
                "end_date" => Carbon::now(config("app.default_user_timezone"))
                    ->addDays(100)
                    ->startOfDay()
                    ->toISOString(),
                "invoice_id" => Invoice::factory()->has(
                    BillItem::factory([
                        "item_type" => BillItemTypes::contributionYearly,
                        "amount" => -250,
                        "taxes_tvq" => -30,
                        "taxes_tps" => -20,
                        "contribution_community_id" => $community,
                    ])
                ),
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/pay", [
            "pricing_loanable_types" => ["car_small", "car_large"],
            "community_id" => $community->id,
        ]);

        $user->refresh();
        self::assertEquals(20, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::CarLarge,
            ],
            $user->subscriptions->first()->loanable_types
        );
    }

    public function testGrantSubscription_succeedsAsCommunityAdmin()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 15,
            ]);
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        $this->actAs($communityAdmin);
        $response = $this->json("put", "/api/v1/subscriptions/grant", [
            "community_id" => $community->id,
            "user_id" => $user->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        self::assertEquals(15, $user->balance);
        self::assertCount(1, $user->subscriptions);
        self::assertEquals(
            $community->id,
            $user->subscriptions->first()->communityUser->community_id
        );
        self::assertEquals("granted", $user->subscriptions->first()->type);
    }

    public function testGrantSubscription_forbiddenIfNotAdmin()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "balance" => 15,
            ]);

        $this->actAs($user);
        $response = $this->json("put", "/api/v1/subscriptions/grant", [
            "community_id" => $community->id,
            "user_id" => $user->id,
        ])->assertStatus(403);
    }

    public function testGrantSubscription_doesntReplacePaidSubscription()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create([
            "balance" => 15,
        ]);
        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();
        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->subMonth(),
                "end_date" => Carbon::now()->addMonth(),
                "type" => "paid",
            ]);

        $this->actAs($communityAdmin);
        $response = $this->json("put", "/api/v1/subscriptions/grant", [
            "community_id" => $community->id,
            "user_id" => $user->id,
        ]);

        $response->assertStatus(201);
        $user->refresh();
        self::assertCount(2, $user->subscriptions);
        $grantedSubscription = $user->subscriptions
            ->where("type", "granted")
            ->first();
        self::assertEquals(
            $community->id,
            $grantedSubscription->communityUser->community_id
        );
    }
}
