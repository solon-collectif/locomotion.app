<?php

namespace Tests\Integration;

use App\Enums\LoanableUserRoles;
use App\Mail\LoanableUserRoleAddedMail;
use App\Mail\LoanableUserRoleRemovedMail;
use App\Models\Loanable;
use App\Models\User;
use Tests\TestCase;

class LoanableUserRoleTest extends TestCase
{
    public function testAdminCanUpdateOwnerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $ownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerUserRole);

        $adminUser = User::factory()->create(["role" => "admin"]);
        $this->actAs($adminUser);

        $newOwnerUser = User::factory()->create();

        $data = [
            "user_id" => $newOwnerUser->id,
            "role" => LoanableUserRoles::Owner->value,
        ];
        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$ownerUserRole->id}",
            $data
        );
        $response->assertStatus(201);

        $loanable->refresh();

        $this->assertEquals($newOwnerUser->id, $loanable->owner_user->id);
        \Mail::assertQueued(
            LoanableUserRoleAddedMail::class,
            fn(LoanableUserRoleAddedMail $mail) => $mail->hasTo(
                $newOwnerUser->email
            )
        );
        \Mail::assertQueued(
            LoanableUserRoleRemovedMail::class,
            fn(LoanableUserRoleRemovedMail $mail) => $mail->hasTo(
                $ownerUserRole->user->email
            )
        );
    }

    public function testOwnerCannotUpdateOwnerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $ownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerUserRole);

        $this->actAs($ownerUserRole->user);

        $newOwnerUser = User::factory()->create();

        $data = [
            "user_id" => $newOwnerUser->id,
        ];
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$ownerUserRole->id}",
            $data
        );
        $response->assertStatus(403);
    }

    public function testCoownerCannotUpdateOwnerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();

        $ownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerUserRole);

        $coownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->first();

        $this->actAs($coownerUserRole->user);

        $newOwnerUser = User::factory()->create();

        $data = [
            "user_id" => $newOwnerUser->id,
        ];
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$ownerUserRole->id}",
            $data
        );
        $response->assertStatus(403);
    }

    public function testCannotUpdateCoownerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();

        $coownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->first();
        $this->assertNotNull($coownerUserRole);

        $newCoownerUser = User::factory()->create();

        $data = [
            "user_id" => $newCoownerUser->id,
            "role" => LoanableUserRoles::Coowner->value,
        ];
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$coownerUserRole->id}",
            $data
        );
        $response->assertStatus(403);
    }

    public function testOwnerCannotUpdateCoownerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->create();

        $ownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerUserRole);

        $coownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->first();
        $this->assertNotNull($coownerUserRole);

        $this->actAs($ownerUserRole->user);

        $newCoownerUser = User::factory()->create();

        $data = [
            "user_id" => $newCoownerUser->id,
        ];
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$coownerUserRole->id}",
            $data
        );
        $response->assertStatus(403);
    }

    public function testCoownerCannotUpdateCoownerUser()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCoowner()
            ->withCoowner()
            ->create();

        $ownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerUserRole);

        $coownerUserRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->first();
        $this->assertNotNull($coownerUserRole);

        $coownerToActAs = $loanable->userRoles
            ->where("role", LoanableUserRoles::Coowner)
            ->last();
        $this->actAs($coownerToActAs->user);

        $newCoownerUser = User::factory()->create();

        $data = [
            "user_id" => $newCoownerUser->id,
        ];
        $response = $this->json(
            "PUT",
            "/api/v1/loanables/roles/{$coownerUserRole->id}",
            $data
        );
        $response->assertStatus(403);
    }
}
