<?php

namespace Tests\Integration;

use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Tests\TestCase;

class TrailerTest extends TestCase
{
    private static $getTrailerResponseStructure = [
        "id",
        "type",
        "name",
        "comments",
        "instructions",
        "location_description",
        "position",
        "details" => ["maximum_charge", "dimensions"],
    ];

    public function testCreateTrailers()
    {
        $ownerUser = User::factory()->create();

        $data = [
            "name" => $this->faker->name,
            "owner_user" => ["id" => $ownerUser->id],
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "type" => "trailer",
            "details" => [
                "maximum_charge" => $this->faker->numberBetween(1000, 9000),
                "dimensions" => $this->faker->sentence(2),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getTrailerResponseStructure);

        // Check that loanable has $ownerUser as owner
        $loanable = Loanable::find($response->json()["id"]);
        $this->assertEquals($ownerUser->id, $loanable->owner_user->id);
    }

    public function testShowTrailers()
    {
        $trailer = Loanable::factory()
            ->withTrailer()
            ->withOwner($this->user)
            ->create();

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getTrailerResponseStructure);
    }

    public function testUpdateTrailers()
    {
        $trailer = Loanable::factory()
            ->withOwner($this->user)
            ->withTrailer()
            ->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$trailer->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteTrailers()
    {
        $trailer = Loanable::factory()
            ->withOwner($this->user)
            ->withTrailer()
            ->create();

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(404);
    }

    public function testDeleteTrailersWithActiveLoan()
    {
        // No active loan
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
            ]);
        $trailer = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(404);

        // Prepaid (active) loan
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
            ]);
        $trailer = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        // Only completed loan
        $loan = Loan::factory()
            ->paid()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withTrailer(),
            ]);
        $trailer = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);
    }

    public function testListTrailers()
    {
        Loanable::factory(2)
            ->withOwner($this->user)
            ->withTrailer()
            ->create();

        $response = $this->json("GET", "/api/v1/loanables");

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getTrailerResponseStructure
                )
            );
    }
}
