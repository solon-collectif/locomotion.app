<?php

namespace Tests\Integration;

use App\Caches\LoanablesByTypeAndCommunity;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\PricingLoanableTypeValues;
use App\Mail\LoanableUserRoleAddedMail;
use App\Mail\LoanableUserRoleRemovedMail;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Image;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Mail\Mailable;
use Mail;
use Tests\TestCase;

class LoanableTest extends TestCase
{
    private static $getLoanablesResponseStructure = [
        "current_page",
        "data",
        "first_page_url",
        "from",
        "last_page",
        "last_page_url",
        "next_page_url",
        "path",
        "per_page",
        "prev_page_url",
        "to",
        "total",
    ];

    public function testLoanableCreationValidation()
    {
        $ownerUser = User::factory()->create();

        $validData = [
            "name" => $this->faker->name,
            "owner_user" => ["id" => $ownerUser->id],
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "type" => "trailer",
            "details" => [
                "dimensions" => "",
                "maximum_charge" => "",
            ],
        ];
        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        // Check that loanable has $ownerUser as owner
        $loanable = Loanable::find($response->json()["id"]);
        $this->assertEquals($ownerUser->id, $loanable->owner_user->id);

        $missingType = $validData;
        unset($missingType["type"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingType);
        $response->assertStatus(422);

        $wrongType = $validData;
        $wrongType["type"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $wrongType);
        $response->assertStatus(422);

        $missingOwner = $validData;
        unset($missingOwner["owner_user"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingOwner);
        $response->assertStatus(422);

        $missingPosition = $validData;
        unset($missingPosition["position"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingPosition);
        $response->assertStatus(201);

        $missingName = $validData;
        unset($missingName["name"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingName);
        $response->assertStatus(422);

        // valid with details missing (until publishing)
        $missingDetails = $validData;
        unset($missingDetails["details"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingDetails);
        $response->assertStatus(201);
    }

    public function testAvailability_AvailabilityModeNever_ResponseModeAvailable()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-12 13:15:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 8.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "available",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "data" => ["available" => true],
            ],
            // Loan from 13:15 to 21:45
            [
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 13:15:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-12 21:45:00",
                "end" => "2022-10-12 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "data" => ["available" => true],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeAlways_ResponseModeAvailable()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
                "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        // Loan that overlaps availability rules.
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-12 09:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 13.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "available",
            ]
        );

        $response->assertStatus(200)->assertExactJson([
            [
                "start" => "2022-10-10 00:00:00",
                "end" => "2022-10-10 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 12:00:00",
                "end" => "2022-10-10 13:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 22:00:00",
                "end" => "2022-10-11 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 12:00:00",
                "end" => "2022-10-11 17:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 22:00:00",
                "end" => "2022-10-12 09:00:00",
                "data" => ["available" => true],
            ],
            // Loan from 09:00 to 22:30
            [
                "start" => "2022-10-12 22:30:00",
                "end" => "2022-10-13 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 12:00:00",
                "end" => "2022-10-13 17:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 22:00:00",
                "end" => "2022-10-14 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 12:00:00",
                "end" => "2022-10-14 13:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 22:00:00",
                "end" => "2022-10-15 00:00:00",
                "data" => ["available" => true],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeNever_ResponseModeUnavailable()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-12 13:15:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 8.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "unavailable",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-10 00:00:00",
                "end" => "2022-10-10 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 12:00:00",
                "end" => "2022-10-10 13:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 22:00:00",
                "end" => "2022-10-11 10:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-11 12:00:00",
                "end" => "2022-10-11 17:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-11 22:00:00",
                "end" => "2022-10-12 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 12:00:00",
                "end" => "2022-10-12 13:00:00",
                "data" => ["available" => false],
            ],
            // Loan
            [
                "start" => "2022-10-12 13:15:00",
                "end" => "2022-10-12 21:45:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 22:00:00",
                "end" => "2022-10-13 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 12:00:00",
                "end" => "2022-10-13 17:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 22:00:00",
                "end" => "2022-10-14 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 12:00:00",
                "end" => "2022-10-14 13:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 22:00:00",
                "end" => "2022-10-15 00:00:00",
                "data" => ["available" => false],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeAlways_ResponseModeUnavailable()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
                "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-11 12:15:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 4.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "unavailable",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "data" => ["available" => false],
            ],
            // Loan
            [
                "start" => "2022-10-11 12:15:00",
                "end" => "2022-10-11 16:45:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 22:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "data" => ["available" => false],
            ],
        ]);
    }

    public function testEvents_AvailabilityModeNever()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);
        $loanLessThanOneDay = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-11 11:15:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 10 * 60,
            ]);

        $loanMoreThanOneDay = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-12 22:45:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 3 * 24 * 60,
            ]);

        $loanMoreThanOneMonth = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-09-30 00:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 44 * 24 * 60,
            ]);

        $loanNotConfirmed = Loan::factory()
            ->requested()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-09-30 00:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 44 * 24 * 60,
            ]);

        $this->actAs($ownerUser);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/events",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                // Set order to get the results in a predictable order.
                "order" => "type,start,end",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            // Loans
            [
                "type" => "loan",
                // Loan is 44 days in minutes, but crosses over 11-03 which is EDT->EST
                // when 1am->2am is repeated. This is why this ends on 23pm rather than at midnight
                "start" => "2022-09-30 00:00:00",
                "end" => "2022-11-12 23:00:00",
                "uri" => "/loans/{$loanMoreThanOneMonth->id}",
                "data" => [
                    "status" => "ongoing",
                    "accepted" => true,
                    "borrower_name" =>
                        $loanMoreThanOneMonth->borrowerUser->name,
                    "action_required" => false,
                    "loan_id" => $loanMoreThanOneMonth->id,
                ],
            ],
            [
                "type" => "loan",
                // Loan is 44 days in minutes, but crosses over 11-03 which is EDT->EST
                // when 1am->2am is repeated. This is why this ends on 23pm rather than at midnight
                "start" => "2022-09-30 00:00:00",
                "end" => "2022-11-12 23:00:00",
                "uri" => "/loans/{$loanNotConfirmed->id}",
                "data" => [
                    "status" => "requested",
                    "accepted" => false,
                    "borrower_name" => $loanNotConfirmed->borrowerUser->name,
                    "action_required" => true,
                    "loan_id" => $loanNotConfirmed->id,
                ],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-11 11:15:00",
                "end" => "2022-10-11 21:15:00",
                "uri" => "/loans/{$loanLessThanOneDay->id}",
                "data" => [
                    "status" => "ongoing",
                    "accepted" => true,
                    "borrower_name" => $loanLessThanOneDay->borrowerUser->name,
                    "action_required" => false,
                    "loan_id" => $loanLessThanOneDay->id,
                ],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-12 22:45:00",
                "end" => "2022-10-15 22:45:00",
                "uri" => "/loans/{$loanMoreThanOneDay->id}",
                "data" => [
                    "status" => "ongoing",
                    "accepted" => true,
                    "borrower_name" => $loanMoreThanOneDay->borrowerUser->name,
                    "action_required" => false,
                    "loan_id" => $loanMoreThanOneDay->id,
                ],
            ],
        ]);
    }

    public function testEvents_AvailabilityModeAlways()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
                "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loanLessThanOneDay = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-11 11:15:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 10 * 60,
            ]);

        $loanMoreThanOneDay = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-10-12 22:45:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 3 * 24 * 60,
            ]);

        $loanMoreThanOneMonth = Loan::factory()
            ->ongoing()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon(
                    "2022-09-30 00:00:00",
                    "America/Toronto"
                ))->toISOString(),
                "duration_in_minutes" => 44 * 24 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/events",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                // Set order to get the results in a predictable order.
                "order" => "type,start,end",
            ]
        );

        $response->assertStatus(200)->assertJson([
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            // Loans
            [
                "type" => "loan",
                "start" => "2022-09-30 00:00:00",
                // Loan is 44 days in minutes, but crosses over 11-03 which is EDT->EST
                // when 1am->2am is repeated. This is why this ends on 23pm rather than at midnight
                "end" => "2022-11-12 23:00:00",
                "uri" => "/loans/{$loanMoreThanOneMonth->id}",
                "data" => ["status" => "ongoing"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-11 11:15:00",
                "end" => "2022-10-11 21:15:00",
                "uri" => "/loans/{$loanLessThanOneDay->id}",
                "data" => ["status" => "ongoing"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-12 22:45:00",
                "end" => "2022-10-15 22:45:00",
                "uri" => "/loans/{$loanMoreThanOneDay->id}",
                "data" => ["status" => "ongoing"],
            ],
        ]);
    }

    public function testListLoanablesValidation()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        // Complete valid request
        $this->json("GET", route("loanables.list"))->assertStatus(200);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car",
            ])
        )->assertStatus(200);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,bike,trailer",
            ])
        )->assertStatus(200);
        $this->json("GET", route("loanables.list", ["types" => "couch"]))
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "types" => [
                        "Les types demandés sont invalides. Options possibles: bike,trailer,car.",
                    ],
                ],
            ]);
    }

    public function testListLoanables_doesntReturnUnaccessibleLoanables()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();

        $borrowerUser = User::factory()->create();
        $borrowerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $otherCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withBike()
            ->create([
                "availability_mode" => "never",
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "never",
            ]);

        $this->actAs($borrowerUser);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,trailer,bike",
            ])
        )
            ->assertStatus(200)
            ->assertExactJson([
                "car" => [],
                "bike" => [],
                "trailer" => [],
            ]);
    }

    public function testListLoanables_returnsCoownedCarsEvenIfOwner()
    {
        $user = User::factory()->create();
        $loanable = Loanable::factory()
            ->withOwner()
            ->withCar()
            ->withCoowner($user)
            ->create([
                "availability_mode" => "always",
            ]);

        $this->actAs($user);

        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car",
            ])
        )
            ->assertStatus(200)
            ->assertJson([
                "car" => [
                    [
                        "id" => $loanable->id,
                    ],
                ],
            ]);
    }

    public function testListLoanables_returnsCachedLoanables()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $searcher = User::factory()
            ->withCommunity($community)
            ->withBorrower(Borrower::factory()->approved())
            ->create();

        $this->actAs($searcher);

        $car = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "availability_mode" => "always",
            ]);
        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->withBike()
            ->create([
                "availability_mode" => "always",
            ]);
        $trailer = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
            ]);

        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,trailer,bike",
            ])
        )
            ->assertStatus(200)
            ->assertJson([
                "car" => [
                    [
                        "id" => $car->id,
                    ],
                ],
                "bike" => [
                    [
                        "id" => $bike->id,
                    ],
                ],
                "trailer" => [
                    [
                        "id" => $trailer->id,
                    ],
                ],
            ]);

        $cachedCars = \Cache::get("loanables_car_$community->id");
        self::assertCount(1, $cachedCars);
        self::assertArrayHasKey($car->id, $cachedCars);
        $cachedBikes = \Cache::get("loanables_bike_$community->id");
        self::assertCount(1, $cachedBikes);
        self::assertArrayHasKey($bike->id, $cachedBikes);
        $cachedTrailers = \Cache::get("loanables_trailer_$community->id");
        self::assertCount(1, $cachedTrailers);
        self::assertArrayHasKey($trailer->id, $cachedTrailers);

        // We wipe the DB without invalidating the cache to make sure the cache returns
        // the data in the next test
        \DB::table("cars")->delete($car->id);
        \DB::table("bikes")->delete($bike->id);
        \DB::table("trailers")->delete($trailer->id);
        \DB::table("loanables")->delete();
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,trailer,bike",
            ])
        )
            ->assertStatus(200)
            ->assertJson([
                "car" => [
                    [
                        "id" => $car->id,
                    ],
                ],
                "bike" => [
                    [
                        "id" => $bike->id,
                    ],
                ],
                "trailer" => [
                    [
                        "id" => $trailer->id,
                    ],
                ],
            ]);
        // once cache is cleared, we return from DB
        \Cache::clear();
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,trailer,bike",
            ])
        )
            ->assertStatus(200)
            ->assertExactJson([
                "car" => [],
                "bike" => [],
                "trailer" => [],
            ]);
    }

    public function testUpdateMainImage_clearsLoanableCache()
    {
        $user = User::factory()
            ->withCommunity()
            ->create([
                "name" => "first",
                "last_name" => "last",
            ]);
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create([
                "availability_mode" => "always",
            ]);
        $fooImage = Image::factory()->create([
            "imageable_type" => "loanable",
            "imageable_id" => $loanable->id,
            "field" => "image",
            "original_filename" => "foo.jpg",
            "order" => 0,
        ]);
        $barImage = Image::factory()->create([
            "imageable_type" => "loanable",
            "imageable_id" => $loanable->id,
            "field" => "image",
            "original_filename" => "bar.jpg",
            "order" => 1,
        ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertEquals(
            "foo.jpg",
            $cachedLoanables[$loanable->id]["image"]["original_filename"]
        );
        self::assertCount(2, $cachedLoanables[$loanable->id]["images"]);

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id?fields=*,images.*",
            [
                "images" => [
                    [
                        "id" => $barImage->id,
                        "order" => 0,
                    ],
                    [
                        "id" => $fooImage->id,
                        "order" => 1,
                    ],
                ],
            ]
        )->assertStatus(200);

        $response->assertJson([
            "images" => [
                [
                    "id" => $barImage->id,
                    "order" => 0,
                ],
                [
                    "id" => $fooImage->id,
                    "order" => 1,
                ],
            ],
        ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertEquals(
            "bar.jpg",
            $cachedLoanables[$loanable->id]["image"]["original_filename"]
        );
        self::assertCount(2, $cachedLoanables[$loanable->id]["images"]);
    }

    public function testUpdateOwner_clearsLoanableCache()
    {
        $user = User::factory()
            ->withCommunity()
            ->create([
                "name" => "first",
                "last_name" => "last",
            ]);
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create([
                "availability_mode" => "always",
            ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "full_name" => "first last",
                        ],
                    ],
                ],
            ],
        ]);

        $this->json("PUT", "/api/v1/users/$user->id", [
            "name" => "foo",
            "last_name" => "bar",
        ])->assertStatus(200);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "full_name" => "foo bar",
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testUpdateOwnerAvatar_clearsLoanableCache()
    {
        $user = User::factory()
            ->withCommunity()
            ->create([
                "name" => "first",
                "last_name" => "last",
            ]);
        Image::factory()->create([
            "imageable_type" => "user",
            "imageable_id" => $user->id,
            "field" => "avatar",
            "original_filename" => "bar.jpg",
        ]);
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create([
                "availability_mode" => "always",
            ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "avatar" => [
                                "original_filename" => "bar.jpg",
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $user->avatar->update([
            "original_filename" => "foo.jpg",
        ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "avatar" => [
                                "original_filename" => "foo.jpg",
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testUpdateCoowner_clearsLoanableCache()
    {
        $user = User::factory()->create([
            "name" => "first",
            "last_name" => "last",
        ]);
        $loanable = Loanable::factory()
            ->withOwner()
            ->withBike()
            ->withCoowner($user)
            ->create([
                "availability_mode" => "always",
            ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $loanable->owner_user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "full_name" => "first last",
                        ],
                    ],
                ],
            ],
        ]);

        $this->json("PUT", "/api/v1/users/$user->id", [
            "name" => "foo",
            "last_name" => "bar",
        ])->assertStatus(200);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $loanable->owner_user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "full_name" => "foo bar",
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testUpdateCoownerAvatar_clearsLoanableCache()
    {
        $user = User::factory()->create([
            "name" => "first",
            "last_name" => "last",
        ]);
        $loanable = Loanable::factory()
            ->withOwner()
            ->withBike()
            ->withCoowner($user)
            ->create([
                "availability_mode" => "always",
            ]);

        Image::factory()->create([
            "imageable_type" => "user",
            "imageable_id" => $user->id,
            "field" => "avatar",
            "original_filename" => "bar.jpg",
        ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $loanable->owner_user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "avatar" => [
                                "original_filename" => "bar.jpg",
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $user->avatar->update([
            "original_filename" => "foo.jpg",
        ]);

        $cachedLoanables = LoanablesByTypeAndCommunity::get(
            $loanable->owner_user->communities[0]->id,
            LoanableTypes::Bike
        );
        self::assertCount(1, $cachedLoanables);
        self::assertArrayHasKey($loanable->id, $cachedLoanables);
        self::assertJsonUnordered($cachedLoanables, [
            $loanable->id => [
                "merged_user_roles" => [
                    [
                        "user" => [
                            "avatar" => [
                                "original_filename" => "foo.jpg",
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testCreateLoanable_createsOwnerRole()
    {
        $data = [
            "name" => $this->faker->name,
            "owner_user" => ["id" => $this->user->id],
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "type" => "trailer",
            "details" => [
                "dimensions" => "",
                "maximum_charge" => "",
            ],
        ];
        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response->assertStatus(201);

        $loanable = Loanable::find($response["id"]);

        $ownerRole = $loanable->userRoles
            ->where("role", LoanableUserRoles::Owner)
            ->first();
        $this->assertNotNull($ownerRole);
    }

    public function testListLoanables_returnsDetailedCars()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "availability_mode" => "always",
            ]);

        $response = $this->json(
            "GET",
            route("loanables.list", ["types" => "car"])
        )->assertStatus(200);

        $response
            ->assertJsonStructure([
                "car" => [
                    [
                        "id",
                        "type",
                        "name",
                        "position_google",
                        "sharing_mode",
                        "comments",
                        "merged_user_roles" => [["role", "user"]],
                        "image" => [],
                        "details" => [
                            "brand",
                            "engine",
                            "transmission_mode",
                            "year_of_circulation",
                            "papers_location",
                            "model",
                        ],
                    ],
                ],
            ])
            ->assertJsonCount(1, "car");
    }

    public function testListLoanables_returnsDetailedBikes()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);

        $this->json("GET", route("loanables.list", ["types" => "bike"]))
            ->assertStatus(200)
            ->assertJsonStructure([
                "bike" => [
                    [
                        "id",
                        "type",
                        "name",
                        "position_google",
                        "sharing_mode",
                        "comments",
                        "merged_user_roles" => [["role", "user"]],
                        "image" => [],
                        "details" => ["bike_type", "model", "size"],
                    ],
                ],
            ])
            ->assertJsonCount(1, "bike");
    }

    public function testListLoanables_returnsDetailedTrailers()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
            ]);

        $this->json("GET", route("loanables.list", ["types" => "trailer"]))
            ->assertStatus(200)
            ->assertJsonStructure([
                "trailer" => [
                    [
                        "id",
                        "type",
                        "name",
                        "position_google",
                        "sharing_mode",
                        "comments",
                        "merged_user_roles" => [["role", "user"]],
                        "image" => [],
                        "details" => ["maximum_charge", "dimensions"],
                    ],
                ],
            ])
            ->assertJsonCount(1, "trailer");
    }

    public function testListLoanables_hidesUnavailableLoanables()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "never",
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
            ]);
        Loanable::factory()
            ->withOwner()
            ->withBike()
            ->create([
                "availability_mode" => "never",
            ]);

        $this->json("GET", route("loanables.list"))
            ->assertStatus(200)
            ->assertExactJson([
                "car" => [],
                "bike" => [],
                "trailer" => [],
            ]);
    }

    public function testListLoanable_hidesForbiddenLoanableTypes()
    {
        $bikeOnlyCommunity = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $allLoanableTypesCommunity = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $bikeOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Bike->getTypeDetails());

        // Requesting user can see all types of loanables, since they're approved
        // in allLoanableTypesCommunity
        $this->user->communities()->sync([
            $bikeOnlyCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
            $allLoanableTypesCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        // The car and trailer should still not be visible since their owner is only approved
        // in the bike only community
        $ownerUser->communities()->sync([
            $bikeOnlyCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "availability_mode" => "always",
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withBike()
            ->create([
                "availability_mode" => "always",
            ]);

        $this->json("GET", route("loanables.list"))
            ->assertStatus(200)
            ->assertJsonFragment([
                "car" => [],
                "trailer" => [],
            ])
            ->assertJsonCount(1, "bike");
    }

    public function testListLoanables_showsPartiallyAvailableLoanables()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "never",
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "availability_mode" => "never",
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
            ]);
        Loanable::factory()
            ->withOwner($ownerUser)
            ->withBike()
            ->create([
                "availability_mode" => "never",
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
            ]);

        $response = $this->json("GET", route("loanables.list"))
            ->assertStatus(200)
            ->assertJsonStructure([
                "car" => [],
                "trailer" => [],
                "bike" => [],
            ]);

        $response->assertJsonCount(1, "car");
        $response->assertJsonCount(1, "bike");
        $response->assertJsonCount(1, "trailer");
    }

    public function testSearchLoanablesValidation()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])->assertStatus(200);

        // Departure missing
        $this->json("GET", route("loanables.search"), [
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "departure_at" => ["validation.required"],
                ],
            ]);

        // Duration 0
        $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "duration_in_minutes" => ["validation.min.numeric"],
                ],
            ]);
    }

    public function testSearchLoanables_findsLoanable()
    {
        self::setTestNow("2022-09-01");
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $carToFind = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        // Non-overlapping loan after
        $loanAfter = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $carToFind->id,
                "departure_at" => Carbon::now()
                    ->addDay()
                    ->format("Y-m-d H:i:s"),
            ]);
        // Non-overlapping loan before
        $loanBefore = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $carToFind->id,
                "departure_at" => Carbon::now()
                    ->subDay()
                    ->format("Y-m-d H:i:s"),
                "duration_in_minutes" => 20,
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertJson([$carToFind->id]);
    }

    public function testSearchLoanables_ignoresLoanableFromOtherCommunity()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $otherCommunity = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $otherCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_ignoresLoanableWithOverlappingLoans()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $carToIgnore = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => $carToIgnore->id,
                "departure_at" => Carbon::now()
                    ->subMinutes(5)
                    ->format("Y-m-d H:i:s"),
                "duration_in_minutes" => 20,
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_ignoresLoanableWhenUnavailable()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "never",
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_ignoresLoanableWithLesserMaxLoanDuratin()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
                "max_loan_duration_in_minutes" => 60,
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 120,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 30,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([$loanable->id]);
    }

    public function testSearchLoanables_estimatesWithPricing()
    {
        self::setTestNow("2022-09-01");

        $community = Community::factory()->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create();
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($borrowerUser);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $carToFind = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "availability_mode" => "always",
            ]);

        $bikeToFind = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "availability_mode" => "always",
            ]);

        $trailerToFind = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_id" => $community->id,
                "rule" => "[10,3]",
                "name" => "car pricing name",
            ]);
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::BikeCargoElectric)
            ->forType(PricingLoanableTypeValues::BikeElectric)
            ->forType(PricingLoanableTypeValues::BikeRegular)
            ->forType(PricingLoanableTypeValues::BikeCargoRegular)
            ->create([
                "community_id" => $community->id,
                "rule" => "7",
                "name" => "bike pricing name",
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $this->assertJsonUnordered($response->json(), [
            $carToFind->id,
            $bikeToFind->id,
            $trailerToFind->id,
        ]);
    }

    public function testSearchLoanables_ignoresTimezonesInRequest()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);

        $now = new CarbonImmutable("2024-02-12 04:00:00");
        self::setTestNow($now);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
                "availability_json" => <<<JSON
[
    {
        "available":  false,
        "type":  "dates",
        "scope":  ["2024-02-12"],
        "period": "8:00-10:15"
    }
]
JSON
            ,
            ]);

        $availableLoanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            // 11:00 America/Toronto, which would mean the first loanable would also be available,
            // but we ignore this, since we want to test only local times for each vehicle.
            "departure_at" => "2024-02-12 10:00:00 America/Chicago",
            "duration_in_minutes" => 20,
            "estimated_distance" => 10,
        ]);

        $response->assertStatus(200)->assertJson([$availableLoanable->id]);
    }

    public function testSearchLoanables_searchesInLoanableLocalTime()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);

        $now = new CarbonImmutable("2024-02-12 04:00:00");
        self::setTestNow($now);

        $unavailableLoanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
                "timezone" => "America/Chicago",
                "availability_json" => <<<JSON
[
    {
        "available":  false,
        "type":  "dates",
        "scope":  ["2024-02-12"],
        "period": "10:00-14:00"
    }
]
JSON
            ,
            ]);

        $loanedLoanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
                "timezone" => "America/St_Johns",
            ]);

        Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanedLoanable,
                "departure_at" => (new Carbon(
                    "2024-02-12 10:00:00",
                    "America/St_Johns"
                ))->toISOString(),
                "duration_in_minutes" => 240, // until 14:00 in America/St_Johns
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            // Search without timezone information
            "departure_at" => "2024-02-12 10:00:00",
            "duration_in_minutes" => 20,
            "estimated_distance" => 10,
        ]);

        $response->assertStatus(200)->assertExactJson([]);

        $response = $this->json("GET", route("loanables.search"), [
            // Search without timezone information
            "departure_at" => "2024-02-12 14:00:00",
            "duration_in_minutes" => 20,
            "estimated_distance" => 10,
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson([$unavailableLoanable->id, $loanedLoanable->id]);
    }

    public function testSearchLoanables_handlesDepartureAtAfterNow()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $ownerUser = User::factory()
            ->withCommunity($community)
            ->create();
        $borrowerUser = User::factory()
            ->withCommunity($community)
            ->withBorrower()
            ->create();

        $this->actAs($borrowerUser);

        $now = new CarbonImmutable("2024-02-12 10:05:00 America/Toronto");
        self::setTestNow($now);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
                "timezone" => "America/Toronto",
            ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => "2024-02-12 10:00:00",
            "duration_in_minutes" => 20,
            "estimated_distance" => 10,
        ]);

        $response->assertStatus(200)->assertExactJson([]);
    }

    public function testOrderLoanablesById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testOrderLoanablesByName()
    {
        $data = [
            "order" => "name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testOrderLoanablesByType()
    {
        $data = [
            "order" => "type",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "id" => "4",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "name" => "Vélo",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByType()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "type" => "car",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByDeletedAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "@",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testSearchLoanables()
    {
        Loanable::factory()
            ->withOwner()
            ->create([
                "name" => "Loanable name",
            ]);
        Loanable::factory()
            ->withOwner()
            ->create([
                "name" => "Other name",
            ]);

        $data = [
            "q" => "able NA",
        ];
        $response = $this->json(
            "GET",
            route("loanables.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testRetrieveLoanableForOwner_showsInstructions()
    {
        $ownerUser = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "instructions" => "test",
            ]);

        $this->actAs($ownerUser);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testRetrieveLoanableForAdmin_showsInstructions()
    {
        $ownerUser = User::factory()->create();
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "instructions" => "test",
            ]);

        $admin = User::factory()->create(["role" => "admin"]);

        $this->actAs($admin);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testRetrieveLoanable_hidesInstructions()
    {
        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "instructions" => "test",
            ]);

        $otherUser = User::factory()->create();

        // Other user has access to loanable but not to instructions
        $otherUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($otherUser);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);
    }

    public function testLoanableTestEndpointValidation()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])->assertStatus(200);

        // Departure missing
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "departure_at" => ["validation.required"],
                ],
            ]);

        // Community missing: OK
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])->assertStatus(200);

        // Loanable missing
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "loanable_id" => ["validation.required"],
                ],
            ]);

        // Duration 0
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "duration_in_minutes" => ["validation.min.numeric"],
                ],
            ]);

        // Estimated distance negative
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => -1,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "estimated_distance" => ["validation.min.numeric"],
                ],
            ]);
    }

    public function testLoanableTestEndpointWithPricing()
    {
        $community = Community::factory()
            //          ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Pricing::factory()
            ->forAllTypes()
            ->create([
                "rule" => "3 * \$KM + \$MINUTES",
                "pricing_type" => "price",
                "community_id" => $community->id,
            ]);

        Pricing::factory()
            ->forAllTypes()
            ->create([
                "rule" => "2 * \$KM + \$MINUTES ",
                "pricing_type" => "insurance",
                "community_id" => $community->id,
            ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $this->setTestLocale();

        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 2,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])
            ->assertStatus(200)
            ->assertJson([
                "borrower_invoice" => [
                    "items" => [
                        [
                            "item_type" => "loan.price",
                            "amount" => -26,
                            "taxes_tps" => 0,
                            "taxes_tvq" => 0,
                            "total" => -26,
                        ],
                        [
                            "item_type" => "loan.insurance",
                            "amount" => -24,
                            "taxes_tps" => 0,
                            "taxes_tvq" => 0,
                            "total" => -24,
                        ],
                    ],
                ],
            ]);

        // Check that no loan, invoice or invoice item was saved.
        $this->assertDatabaseCount("loans", 0);
        $this->assertDatabaseCount("invoices", 0);
        $this->assertDatabaseCount("bill_items", 0);
    }

    public function testLoanableTestEndpointValidation_WithNullPricing()
    {
        $community = Community::factory()->create();
        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])->assertStatus(200);
    }

    public function testLoanableTestAvailability()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($loanable->owner_user->main_community)
            ->create();

        $this->actAs($borrowerUser);

        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now("America/Toronto")
                ->addHour()
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => true]);
    }

    public function testLoanableTestAvailability_withGreaterLoanTimeThanMax()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "max_loan_duration_in_minutes" => 60,
            ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($loanable->owner_user->main_community)
            ->create();

        $this->actAs($borrowerUser);

        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now("America/Toronto")
                ->addHour()
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 120,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => false]);
    }

    public function testLoanableTestAvailability_whenUnavailable()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($loanable->owner_user->main_community)
            ->create();

        Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now(),
                "duration_in_minutes" => 20,
            ]);

        $this->actAs($borrowerUser);

        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now("America/Toronto")
                ->addMinutes(15)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => false]);
    }

    public function testUpdateOwner_allowedForGlobalAdmin()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $admin = User::factory()->create(["role" => "admin"]);
        $otherOwnerUser = User::factory()->create();

        $this->actAs($admin);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_user_id" => $otherOwnerUser->id,
        ])->assertStatus(200);
    }

    public function testUpdateOwner_allowedForCommunityAdmin()
    {
        $community = Community::factory()->create();
        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => Carbon::now()]);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();
        $communityAdmin = User::factory()->create();
        $communityAdmin->communities()->attach($community->id, [
            "role" => "admin",
            "approved_at" => Carbon::now(),
        ]);
        $otherOwnerUser = User::factory()->create();
        $otherOwnerUser->communities()->attach($community->id, [
            "role" => "admin",
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($communityAdmin);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_user_id" => $otherOwnerUser->id,
        ])->assertStatus(200);
    }

    public function testAddCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $this->actAs($ownerUser);
        Mail::fake();

        $this->json("POST", "/api/v1/loanables/{$loanable->id}/roles", [
            "role" => "coowner",
            "user_id" => $coOwnerUser->id,
            "title" => "Co-owner",
            "show_as_contact" => true,
            "pays_loan_price" => true,
            "pays_loan_insurance" => false,
        ])->assertStatus(201);
        $loanable->refresh();

        Mail::assertQueued(LoanableUserRoleAddedMail::class, function (
            $mail
        ) use ($coOwnerUser) {
            return $mail->hasTo($coOwnerUser->email);
        });

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "user_roles.*",
        ]);

        self::assertJsonUnordered($response->json(), [
            "merged_user_roles" => [["role" => "owner"], ["role" => "coowner"]],
        ]);
    }

    public function testAddCoowner_failsWithNoSharedCommunities()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $this->actAs($ownerUser);

        $this->json("POST", "/api/v1/loanables/{$loanable->id}/roles", [
            "role" => "coowner",
            "user_id" => $coOwnerUser->id,
        ])->assertStatus(403);
    }

    public function testAddCoowner_failsForSelf()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $this->actAs($coOwnerUser);

        $this->json("POST", "/api/v1/loanables/{$loanable->id}/roles", [
            "role" => "coowner",
            "user_id" => $coOwnerUser->id,
        ])->assertStatus(403);
        $loanable->refresh();
    }

    public function testRemoveCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $coowner = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coOwnerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($coowner);

        Mail::fake();
        $this->actAs($ownerUser);
        $this->json(
            "DELETE",
            "/api/v1/loanables/roles/{$coowner->id}"
        )->assertStatus(200);
        $loanable->refresh();

        Mail::assertQueued(LoanableUserRoleRemovedMail::class, function (
            Mailable $mailable
        ) use ($coowner) {
            return $mailable->hasTo($coowner->user->email);
        });

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "user_roles.role",
        ]);

        $response
            ->assertJsonCount(1, "merged_user_roles")
            ->assertJsonPath("merged_user_roles.0.role", "owner");
    }

    public function testRemoveCoowner_succeedsForSelf()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $coowner = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coOwnerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($coowner);

        Mail::fake();
        $this->actAs($coOwnerUser);
        $this->json(
            "DELETE",
            "/api/v1/loanables/roles/{$coowner->id}"
        )->assertStatus(200);
        $loanable->refresh();

        // Doesn't send emails for self
        Mail::assertNothingSent();

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "user_roles.role",
        ]);

        $response
            ->assertJsonCount(1, "merged_user_roles")
            ->assertJsonPath("merged_user_roles.0.role", "owner");
    }

    public function testRemoveCoowner_failsForDifferentCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $otherCoOwnerUser = User::factory()->create();
        $otherCoOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $coowner = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $coOwnerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($coowner);

        $otherCoowner = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $otherCoOwnerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($otherCoowner);

        $this->actAs($coOwnerUser);
        $this->json(
            "DELETE",
            "/api/v1/loanables/roles/{$otherCoowner->id}"
        )->assertStatus(403);
    }

    public function testRemoveCoowner_failsForNonCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $randomUser = User::factory()->create();
        $randomUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->create();

        $this->actAs($ownerUser);
        $this->json(
            "DELETE",
            "/api/v1/loanables/roles/{$ownerUser->id}"
        )->assertStatus(404);
    }

    public function testUpdateCar_cannotUpdatePublishedFields()
    {
        $user = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withCar()
            ->create([
                "published" => true,
            ]);

        $this->actAs($user);

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "details" => [
                "plate_number" => "foo",
                "papers_location" => "in_car",
                "brand" => "foo",
                "model" => "foo",
                "pricing_category" => "electric",
                "year_of_circulation" => 2000,
                "transmission_mode" => "manual",
                "engine" => "electric",
                "value_category" => "lte70k",
            ],
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "pricing_category" => [
                        "Le champ pricing category ne peut être mis à jour.",
                    ],
                    "year_of_circulation" => [
                        "Le champ year of circulation ne peut être mis à jour.",
                    ],
                    "transmission_mode" => [
                        "Le champ transmission mode ne peut être mis à jour.",
                    ],
                    "engine" => ["Le champ engine ne peut être mis à jour."],
                    "value_category" => [
                        "Le champ value category ne peut être mis à jour.",
                    ],
                    "brand" => ["Le champ brand ne peut être mis à jour."],
                    "model" => ["Le champ model ne peut être mis à jour."],
                ],
            ])
            ->assertJsonMissing([
                "errors" => [
                    "plate_number" => [
                        "Le champ plate number ne peut être mis à jour.",
                    ],
                    "papers_location" => [
                        "Le champ papers location ne peut être mis à jour.",
                    ],
                ],
            ]);
    }

    public function testPublishLoanable_requiresPosition()
    {
        $user = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withTrailer()
            ->create([
                "position" => null,
            ]);

        $this->actAs($user);

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/{$loanable->id}/publish"
        );
        $response->assertStatus(422)->assertJson([
            "errors" => [
                "position" => ["Le champ position est obligatoire."],
            ],
        ]);
    }

    public function testPublishCar_requiresFields()
    {
        $user = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withCar([
                "plate_number" => "",
                "brand" => "",
                "model" => "",
                "pricing_category" => null,
                "year_of_circulation" => null,
                "transmission_mode" => null,
                "engine" => null,
                "value_category" => null,
                "papers_location" => null,
            ])
            ->create();

        $this->actAs($user);

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/{$loanable->id}/publish"
        );
        $response->assertStatus(422)->assertJson([
            "errors" => [
                "pricing_category" => [
                    "Le champ pricing category est obligatoire.",
                ],
                "year_of_circulation" => [
                    "Le champ year of circulation est obligatoire.",
                ],
                "transmission_mode" => [
                    "Le champ transmission mode est obligatoire.",
                ],
                "engine" => ["Le champ engine est obligatoire."],
                "plate_number" => ["Le champ plate number est obligatoire."],
                "value_category" => [
                    "Le champ value category est obligatoire.",
                ],
                "brand" => ["Le champ brand est obligatoire."],
                "model" => ["Le champ model est obligatoire."],
                "papers_location" => [
                    "Le champ papers location est obligatoire.",
                ],
            ],
        ]);
    }

    public function testDashboard()
    {
        $ownerUser = User::factory()->create();

        $trailer = Loanable::factory()
            ->withOwner($ownerUser)
            ->withTrailer()
            ->withImage()
            ->create();
        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();
        $car = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $coOwnedTrailer = Loanable::factory()
            ->withOwner()
            ->withTrailer()
            ->withImage()
            ->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $coOwnedTrailer->id,
            "ressource_type" => "loanable",
            "user_id" => $ownerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $coOwnedTrailer->userRoles()->save($userRole);

        $this->actAs($ownerUser);
        $response = $this->json("GET", "/api/v1/loanables/dashboard");

        $response->assertStatus(200);
        $this->assertJsonUnordered($response->json(), [
            "loanables" => [
                "owned" => [
                    "data" => [
                        [
                            "id" => $trailer->id,
                            "image" => [
                                "id" => $trailer->image->id,
                            ],
                            "merged_user_roles" => [
                                [
                                    "role" => "owner",
                                    "user" => ["id" => $ownerUser->id],
                                ],
                            ],
                        ],
                        [
                            "id" => $bike->id,
                            "merged_user_roles" => [
                                [
                                    "role" => "owner",
                                    "user" => ["id" => $ownerUser->id],
                                ],
                            ],
                        ],
                        [
                            "id" => $car->id,
                            "merged_user_roles" => [
                                [
                                    "role" => "owner",
                                    "user" => ["id" => $ownerUser->id],
                                ],
                            ],
                        ],
                    ],
                    "total" => 3,
                ],
                "managed" => [
                    "data" => [
                        [
                            "id" => $coOwnedTrailer->id,
                            "image" => [
                                "id" => $coOwnedTrailer->image->id,
                            ],
                            "merged_user_roles" => [
                                [
                                    "role" => "coowner",
                                    "user" => ["id" => $ownerUser->id],
                                ],
                            ],
                        ],
                    ],
                    "total" => 1,
                ],
            ],
        ]);
    }

    public function testGetUnavailableLoans_onlyReturnsUnavailableLoans()
    {
        $now = CarbonImmutable::now();
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        // Available loan for loanable
        Loan::factory()->create([
            "departure_at" => $now->addMinute(),
            "duration_in_minutes" => 30,
            "loanable_id" => $loanable->id,
        ]);
        // unavailable loan for other loanable
        Loan::factory()->create([
            "departure_at" => $now->addDay(),
            "duration_in_minutes" => 30,
        ]);
        $unavailableLoan = Loan::factory()->create([
            "departure_at" => $now->addDay(),
            "duration_in_minutes" => 30,
            "loanable_id" => $loanable->id,
        ]);

        // With a 'only avaialble today' rule
        $response = $this->json(
            "GET",
            "api/v1/loanables/$loanable->id/loans/unavailable",
            [
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
    {
        "available":  true,
        "type":  "dates",
        "scope":  ["{$now->format("Y-m-d")}"],
        "period": "00:00-24:00"
    }
]
JSON
            ,
            ]
        );

        $response->assertJsonCount(1);
        $response->assertJson([
            [
                "id" => $unavailableLoan->id,
            ],
        ]);
    }

    public function testDelete_mustNotHaveOngoingLoansAsOwner()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $response = $this->json("DELETE", "/api/v1/loanables/$loanable->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        $loan->setLoanStatusCanceled();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/loanables/$loanable->id");

        $response->assertStatus(200);
    }

    public function testRestore_removesAvailabilities()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                "availability_mode" => "always",
            ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertEquals("never", $loanable->availability_mode);
        self::assertEquals("[]", $loanable->availability_json);
    }

    public function testRestore_canRestoreAvailabilities()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create([
                "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                "availability_mode" => "always",
            ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore?restore_availability=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertEquals("always", $loanable->availability_mode);
        self::assertEquals(
            <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            $loanable->availability_json
        );
    }

    public function testGetCommunityForLoanByUser()
    {
        $ownerUser = User::factory()->create();
        $ownerUserInNoCommunity = User::factory()->create();
        $borrowerUserInPrivate = User::factory()->create();
        $borrowerUserInOwnerGeo = User::factory()->create();
        $borrowerUserWithOwnerInGeo = User::factory()->create();
        $borrowerUserInPublic = User::factory()->create();
        $borrowerUserNotInShared = User::factory()->create();
        $borrowerUserInNoCommunity = User::factory()->create();

        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->create();

        $firstPrivateCommunity = Community::factory([
            "type" => "private",
        ])->create();
        $ownerUser->communities()->attach($firstPrivateCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        $borrowerUserInPrivate->communities()->attach($firstPrivateCommunity, [
            "approved_at" => (new CarbonImmutable())->addHours(3),
            "join_method" => "manual",
        ]);

        $secondPrivateCommunity = Community::factory([
            "type" => "private",
        ])->create();
        $ownerUser->communities()->attach($secondPrivateCommunity, [
            "approved_at" => (new CarbonImmutable())->addHours(2),
            "join_method" => "manual",
        ]);
        $borrowerUserInPrivate->communities()->attach($secondPrivateCommunity, [
            "approved_at" => (new CarbonImmutable())->subHours(3),
            "join_method" => "manual",
        ]);

        $ownerGeoCommunity = Community::factory([
            "type" => "borough",
        ])->create();
        $ownerUser->communities()->attach($ownerGeoCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "geo",
        ]);
        $borrowerUserInOwnerGeo->communities()->attach($ownerGeoCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        // Put users in this community where it should not have precendence.
        $borrowerUserInPrivate->communities()->attach($ownerGeoCommunity, [
            "approved_at" => (new CarbonImmutable())->addHours(3),
            "join_method" => "manual",
        ]);

        $borrowerGeoCommunity = Community::factory([
            "type" => "borough",
        ])->create();
        $ownerUser->communities()->attach($borrowerGeoCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        $borrowerUserWithOwnerInGeo
            ->communities()
            ->attach($borrowerGeoCommunity, [
                "approved_at" => new CarbonImmutable(),
                "join_method" => "geo",
            ]);
        // Put users in this community where it should not have precendence.
        $borrowerUserInPrivate->communities()->attach($borrowerGeoCommunity, [
            "approved_at" => (new CarbonImmutable())->addHours(3),
            "join_method" => "manual",
        ]);
        $borrowerUserInOwnerGeo->communities()->attach($borrowerGeoCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);

        $firstPublicCommunity = Community::factory([
            "type" => "borough",
        ])->create();
        $ownerUser->communities()->attach($firstPublicCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        $borrowerUserInPublic->communities()->attach($firstPublicCommunity, [
            "approved_at" => (new CarbonImmutable())->subHours(2),
            "join_method" => "manual",
        ]);
        // Put users in this community where it should not have precendence.
        $borrowerUserInPrivate->communities()->attach($firstPublicCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        $borrowerUserInOwnerGeo->communities()->attach($firstPublicCommunity, [
            "approved_at" => new CarbonImmutable(),
            "join_method" => "manual",
        ]);
        $borrowerUserWithOwnerInGeo
            ->communities()
            ->attach($firstPublicCommunity, [
                "approved_at" => new CarbonImmutable(),
                "join_method" => "manual",
            ]);

        $secondPublicCommunity = Community::factory([
            "type" => "borough",
        ])->create();
        $ownerUser->communities()->attach($secondPublicCommunity, [
            "approved_at" => (new CarbonImmutable())->subHours(1),
            "join_method" => "manual",
        ]);
        $borrowerUserInPublic->communities()->attach($secondPublicCommunity, [
            "approved_at" => (new CarbonImmutable())->addHours(2),
            "join_method" => "manual",
        ]);

        $borrowerOnlyCommunity = Community::factory([
            "type" => "borough",
        ])->create();
        $borrowerUserNotInShared
            ->communities()
            ->attach($borrowerOnlyCommunity, [
                "approved_at" => new CarbonImmutable(),
                "join_method" => "manual",
            ]);

        // 1. Shared private community.
        $this->assertEquals(
            $firstPrivateCommunity->id,
            $loanable->getCommunityForLoanBy($borrowerUserInPrivate)->id
        );

        // 2. Owner's geographic community if borrower is approved.
        $this->assertEquals(
            $ownerGeoCommunity->id,
            $loanable->getCommunityForLoanBy($borrowerUserInOwnerGeo)->id
        );

        // 3. Borrower's geographic community if owner is approved.
        $this->assertEquals(
            $borrowerGeoCommunity->id,
            $loanable->getCommunityForLoanBy($borrowerUserWithOwnerInGeo)->id
        );

        // 4. Any other shared community, in the correct order.
        $this->assertEquals(
            $secondPublicCommunity->id,
            $loanable->getCommunityForLoanBy($borrowerUserInPublic)->id
        );

        // 5. If no shared community, use owner's geographic community.
        $this->assertEquals(
            $ownerGeoCommunity->id,
            $loanable->getCommunityForLoanBy($borrowerUserNotInShared)->id
        );

        // Ensure it works if users are in no communities.
        $loanable = Loanable::factory()
            ->withOwner($ownerUserInNoCommunity)
            ->create();

        $this->assertNull(
            $loanable->getCommunityForLoanBy($borrowerUserInNoCommunity)
        );
    }
}
