<?php

namespace Tests\Integration;

use App\Mail\Registration\Approved;
use App\Mail\Registration\ApprovedCustom;
use App\Events\RegistrationSubmittedEvent;
use App\Models\Community;
use App\Models\File;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class CommunityUserTest extends TestCase
{
    public function testApprovingCommunityUser_setsProofEvaluation()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        self::assertEquals("unevaluated", $communityUser->proof_evaluation);

        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => Carbon::now(),
            ]
        );

        $response->assertStatus(200)->assertJson([
            "proof_evaluation" => "valid",
        ]);
    }

    public function testApprovingCommunityUser_sendEmail()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => Carbon::now(),
            ]
        );
        \Mail::assertQueued(
            Approved::class,
            fn(Approved $mail) => $mail->hasTo($user->email)
        );

        $communityUser->approved_at = null;
        $communityUser->save();

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => Carbon::now(),
            ]
        );
        // Mail only sent once, when first approved
        \Mail::assertNothingQueued();
    }

    public function testApprovingCommunityUser_sendsCustomEmailIfExists()
    {
        $community = Community::factory()->create([
            "custom_registration_approved_email" => "## Custom Email!",
        ]);
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        \Mail::fake();

        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => Carbon::now(),
            ]
        );
        \Mail::assertQueued(
            ApprovedCustom::class,
            fn(ApprovedCustom $mail) => $mail->hasTo($user->email)
        );

        $communityUser->approved_at = null;
        $communityUser->save();

        \Mail::fake();
        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => Carbon::now(),
            ]
        );
        // Mail only sent once, when first approved
        \Mail::assertNothingQueued();
    }

    public function testUpdateProofEvaluation()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        self::assertEquals("unevaluated", $communityUser->proof_evaluation);

        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "proof_evaluation" => "invalid",
            ]
        );

        $response->assertStatus(200)->assertJson([
            "proof_evaluation" => "invalid",
        ]);
        $communityUser->refresh();
        self::assertEquals("invalid", $communityUser->proof_evaluation);
    }

    public function testUpdateProofEvaluation_onlyAcceptsValidValues()
    {
        $community = Community::factory()->create();
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        $this->json("PUT", "/api/v1/communityUsers/$communityUser->id", [
            "proof_evaluation" => "someWrongString",
        ])->assertStatus(422);
    }

    public function testExportMailboxes()
    {
        $community = Community::factory()->create();

        User::factory()
            ->withCommunity($community)
            ->sequence(
                [
                    "name" => "foo",
                    "last_name" => "bar",
                    "email" => "foo@bar.com",
                ],
                [
                    "name" => "Joe",
                    "last_name" => "Smith",
                    "email" => "joesmith@test.com",
                ],
                [
                    "name" => "Quokka",
                    "last_name" => "Lemur",
                    "email" => "lemur@quokka.ca",
                ]
            )
            ->count(3)
            ->create();

        $this->get("api/v1/communities/$community->id/users/mailboxes")
            ->assertStatus(200)
            ->assertContent(
                "foo bar <foo@bar.com>,\n" .
                    "Joe Smith <joesmith@test.com>,\n" .
                    "Quokka Lemur <lemur@quokka.ca>"
            );
    }

    public function testExportMailboxes_keepsFilters()
    {
        $community = Community::factory()->create();

        User::factory()
            ->withCommunity($community)
            ->sequence(
                [
                    "name" => "foo",
                    "last_name" => "bar",
                    "email" => "foo@bar.com",
                ],
                [
                    "name" => "Joe",
                    "last_name" => "Smith",
                    "email" => "joesmith@test.com",
                ],
                [
                    "name" => "Joe",
                    "last_name" => "Lemur",
                    "email" => "lemur@quokka.ca",
                ]
            )
            ->count(3)
            ->create();

        $this->json(
            "get",
            "api/v1/communities/$community->id/users/mailboxes",
            ["user.name" => "Joe"]
        )
            ->assertStatus(200)
            ->assertContent(
                "Joe Smith <joesmith@test.com>,\nJoe Lemur <lemur@quokka.ca>"
            );
    }

    public function testExportMailboxes_ignoresPerPage()
    {
        $community = Community::factory()->create();

        User::factory()
            ->count(10)
            ->withCommunity($community)
            ->create();

        $content = $this->json(
            "get",
            "api/v1/communities/$community->id/users/mailboxes",
            [
                "per_page" => 2,
            ]
        )
            ->assertStatus(200)
            ->content();

        self::assertCount(10, explode(",", $content));
    }

    public function testExportMailboxes_forbiddenForRegularUser()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $this->actAs($user);

        $this->json(
            "get",
            "api/v1/communities/$community->id/users/mailboxes"
        )->assertStatus(403);
    }

    public function testUpdateRole_onlyAllowedForGlobalAdmin()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->adminOfCommunity($community)
            ->create();

        self::assertDatabaseCount("community_user", 1);
        $communityUser = CommunityUser::first();

        $this->actAs($user);

        $this->json("put", "api/v1/communityUsers/$communityUser->id", [
            "role" => "admin",
        ])
            ->assertStatus(422)
            ->assertJson([
                "message" => "Le champ role doit être absent.",
            ]);

        $this->actAs($this->user);

        $this->json("put", "api/v1/communityUsers/$communityUser->id", [
            "role" => "admin",
        ])->assertStatus(200);
    }

    public function testUpdateProofs_onlyAllowedForSelfAndAdmin()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $communityUser = CommunityUser::first();

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        self::assertDatabaseCount("community_user", 2);

        /* Check that user can update own proofs */
        $this->actAs($user);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/$communityUser->id",
                "fileable_type" => "communityUser",
                "field" => "custom_proof",
            ]);

        $data = ["custom_proof" => [$proof]];

        $this->json(
            "put",
            "api/v1/communityUsers/$communityUser->id/proofs",
            $data
        )->assertStatus(200);

        /* Check that admin can update proofs */
        $this->actAs($this->user);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/$communityUser->id",
                "fileable_type" => "user",
                "field" => "custom_proof",
            ]);

        $data = ["custom_proof" => [$proof]];

        $this->json(
            "put",
            "api/v1/communityUsers/$communityUser->id/proofs",
            $data
        )->assertStatus(200);

        /* Check that community admin cannot update proofs */
        $this->actAs($communityAdmin);

        $proof = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/$communityUser->id",
                "fileable_type" => "communityUser",
                "field" => "custom_proof",
            ]);

        $data = ["custom_proof" => [$proof]];

        $this->json(
            "put",
            "api/v1/communityUsers/$communityUser->id/proofs",
            $data
        )->assertStatus(403);
    }

    public function testCommunityUserUpdateProofs_onlyTriggersRegistrationSubmittedWhenAllProofsPresent()
    {
        \Storage::fake();

        $community = Community::factory()->create([
            "requires_residency_proof" => true,
            "requires_identity_proof" => false,
            "requires_custom_proof" => true,
        ]);
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $communityUser = CommunityUser::first();

        $residencyProof = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/{$communityUser->user->id}",
                "fileable_type" => "user",
                "field" => "residency_proof",
            ]);

        $data = [
            "residency_proof" => [$residencyProof],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json(
            "PUT",
            "api/v1/communityUsers/$communityUser->id/proofs",
            $data
        );
        $response->assertStatus(200);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);

        $customProof = File::factory()
            ->withStoredFile()
            ->create([
                "path" => "files/tmp/{$communityUser->id}",
                "fileable_type" => "communityUser",
                "field" => "custom_proof",
            ]);
        $data = [
            "residency_proof" => [$residencyProof],
            "custom_proof" => [$customProof],
        ];

        $response = $this->json(
            "PUT",
            "api/v1/communityUsers/$communityUser->id/proofs",
            $data
        );
        $response->assertStatus(200);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }
}
