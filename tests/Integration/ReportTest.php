<?php

use App\Enums\BillItemTypes;
use App\Enums\ReportStatus;
use App\Enums\ReportType;
use App\Jobs\GenerateOwnerIncomeReport;
use App\Mail\ReportPublishedMail;
use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\PublishableReport;
use App\Models\User;
use App\Models\UserPublishableReport;
use App\Reports\YearlyIncomeReport;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class ReportTest extends TestCase
{
    public function testGenerateIncomeReport_returnsPdf()
    {
        $response = $this->json(
            "GET",
            "/api/v1/reports/yearly/2023/income/{$this->user->id}/generate"
        );

        $response->assertStatus(200);
        $response->assertHeader("Content-Type", "application/pdf");
        $this->assertNotEmpty($response->getContent());
    }

    public function testGenerateAllYearlyReports_returnsEarlyIfNoOwnersForYear()
    {
        $this->json("POST", "/api/v1/reports/yearly/2023/income/generate")
            ->assertStatus(200)
            ->assertJson([
                "message" => "Aucun propriétaire avec revenu pour l'année 2023",
            ]);
    }
    public function testGenerateAllYearlyReports_dispatchesOneJobPerOwner()
    {
        Queue::fake();

        $this->setTestNow(new CarbonImmutable("2024-10-10"));

        $owner1 = User::factory()->create();
        $loanable1 = Loanable::factory()
            ->withOwner($owner1)
            ->create();

        $owner2 = User::factory()->create();
        $loanable2 = Loanable::factory()
            ->withOwner($owner2)
            ->create([]);

        $unpaidOwner = User::factory()->create();
        $unpaidLoanable = Loanable::factory()
            ->withOwner($unpaidOwner)
            ->create();
        $wrongYearOwner = User::factory()->create();
        $wrongYearLoanable = Loanable::factory()
            ->withOwner($wrongYearOwner)
            ->create([]);

        $borrower = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 10000,
            ]);

        $loan1 = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $loanable1->id,
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 10,
                "mileage_end" => 20,
            ]);
        $loan2 = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $loanable2->id,
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 10,
                "mileage_end" => 20,
            ]);

        Loan::factory()
            ->withMileageBasedCompensation()
            ->ongoing()
            ->create([
                "loanable_id" => $unpaidLoanable->id,
                "borrower_user_id" => $borrower->id,
            ]);

        $this->setTestNow(new CarbonImmutable("2022-10-10"));
        $loanInWrongYear = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $wrongYearLoanable->id,
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 10,
                "mileage_end" => 20,
            ]);

        // Paying the loans
        $this->actAs($borrower);

        $this->setTestNow(new CarbonImmutable("2024-10-10"));
        $this->json("PUT", "/api/v1/loans/$loan1->id/pay")->assertStatus(200);
        $this->json("PUT", "/api/v1/loans/$loan2->id/pay")->assertStatus(200);

        $this->setTestNow(new CarbonImmutable("2022-10-10"));
        $this->json(
            "PUT",
            "/api/v1/loans/$loanInWrongYear->id/pay"
        )->assertStatus(200);

        // Generating the imports
        $this->actAs($this->user);
        $this->setTestNow(new CarbonImmutable("2024-10-10"));
        $this->json(
            "POST",
            "/api/v1/reports/yearly/2024/income/generate"
        )->assertStatus(201);

        Queue::assertPushed(
            GenerateOwnerIncomeReport::class,
            fn(GenerateOwnerIncomeReport $job) => $job->userId ===
                $owner1->id && $job->year === 2024
        );
        Queue::assertPushed(
            GenerateOwnerIncomeReport::class,
            fn(GenerateOwnerIncomeReport $job) => $job->userId ===
                $owner2->id && $job->year === 2024
        );
        Queue::assertNotPushed(
            GenerateOwnerIncomeReport::class,
            fn(GenerateOwnerIncomeReport $job) => $job->userId ===
                $unpaidOwner->id
        );
        Queue::assertNotPushed(
            GenerateOwnerIncomeReport::class,
            fn(GenerateOwnerIncomeReport $job) => $job->userId ===
                $wrongYearOwner->id
        );
    }

    public function testPublishYearlyReports_possibleOnlyWhenReportIsFullyGenerated()
    {
        $report = PublishableReport::factory()->create([
            "status" => ReportStatus::Generating,
        ]);

        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $userReport1 = UserPublishableReport::factory()->create([
            "publishable_report_id" => $report->id,
            "user_id" => $user1->id,
            "path" => "reports/user1/dummy1.pdf",
        ]);
        $userReport2 = UserPublishableReport::factory()->create([
            "publishable_report_id" => $report->id,
            "user_id" => $user2->id,
            "path" => "reports/user2/dummy2.pdf",
        ]);

        $this->json(
            "PUT",
            "/api/v1/reports/{$report->id}/publish"
        )->assertStatus(403);

        $report->status = ReportStatus::Generated;
        $report->save();

        Mail::fake();

        $this->json(
            "PUT",
            "/api/v1/reports/{$report->id}/publish"
        )->assertStatus(200);

        Mail::assertQueued(
            ReportPublishedMail::class,
            fn($mail) => $mail->hasTo($user1->email)
        );

        Mail::assertQueued(
            ReportPublishedMail::class,
            fn($mail) => $mail->hasTo($user2->email)
        );
    }

    public function testRegenerateReport_replacesExistingReport()
    {
        // Set up owner which would need a report
        $this->setTestNow(new CarbonImmutable("2023-10-10"));
        $owner = User::factory()->create();
        $loanable = Loanable::factory()
            ->withOwner($owner)
            ->create();
        $borrower = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 10000,
            ]);
        $loan = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $loanable->id,
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 10,
                "mileage_end" => 20,
            ]);
        $this->json("PUT", "/api/v1/loans/$loan->id/pay")->assertStatus(200);

        // Setup initial report
        $initialReport = PublishableReport::factory()->create([
            "report_key" => "2023",
            "type" => ReportType::YearlyOwnerIncome,
        ]);
        $userReport1 = UserPublishableReport::factory()->create([
            "publishable_report_id" => $initialReport->id,
            "path" => "reports/initial/dummy1.pdf",
        ]);
        $userReport2 = UserPublishableReport::factory()->create([
            "publishable_report_id" => $initialReport->id,
            "path" => "reports/initial/dummy2.pdf",
        ]);
        StorageTransaction::partialMock();

        // Validate that the initial report's file paths will be deleted
        StorageTransaction::shouldReceive("delete")
            ->with(["reports/initial/dummy1.pdf", "reports/initial/dummy2.pdf"])
            ->once()
            ->andReturn(true);

        // Mock the saving of the new report
        StorageTransaction::shouldReceive("put")
            ->with(
                "files/users/$owner->id/reports/income/2023.pdf",
                Mockery::any()
            )
            ->once()
            ->andReturn(true);

        $this->json(
            "POST",
            "/api/v1/reports/yearly/2023/income/generate"
        )->assertStatus(201);

        // Check that the initial report has been deleted
        $this->assertDatabaseMissing("publishable_reports", [
            "id" => $initialReport->id,
        ]);
        $this->assertDatabaseMissing("user_publishable_reports", [
            "id" => $userReport1->id,
        ]);
        $this->assertDatabaseMissing("user_publishable_reports", [
            "id" => $userReport2->id,
        ]);
    }

    public function testRegenerateReport_impossibleWhenReportIsPublished()
    {
        PublishableReport::factory()->create([
            "report_key" => "2023",
            "type" => ReportType::YearlyOwnerIncome,
            "status" => ReportStatus::Published,
        ]);

        $this->json(
            "POST",
            "/api/v1/reports/yearly/2023/income/generate"
        )->assertStatus(403);
    }

    public function testAccessUserReportFailsWhenReportIsNotPublished()
    {
        $user = User::factory()->create();

        $report = PublishableReport::factory()->create([
            "status" => ReportStatus::Generating,
            "type" => ReportType::YearlyOwnerIncome,
        ]);

        $userReport = UserPublishableReport::factory()->create([
            "publishable_report_id" => $report->id,
            "user_id" => $user->id,
            "path" => "reports/initial/dummy1.pdf",
        ]);

        $this->actAs($user);

        $response = $this->json(
            "GET",
            "api/v1/user_reports/$userReport->id/download"
        );

        $response->assertStatus(403);

        $report->status = ReportStatus::Published;
        $report->save();

        Storage::shouldReceive("response")->once();
        $response = $this->json(
            "GET",
            "api/v1/user_reports/$userReport->id/download"
        )->assertStatus(200);
    }

    public function testYearlyIncomeReportGetData()
    {
        // Create a user and owner
        $owner = User::factory()->create();

        // Create loanables for the owner
        $loanable1 = Loanable::factory()
            ->withOwner($owner)
            ->withCar()
            ->create();
        $loanable2 = Loanable::factory()
            ->withOwner($owner)
            ->withCar()
            ->create();

        // Create loans for each loanable in the given year
        $borrower = User::factory()
            ->withBorrower()
            ->create(["balance" => 10000]);

        $loan1 = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $loanable1->id,
                "borrower_user_id" => $borrower->id,
                "platform_tip" => 0,
                "departure_at" => "2024-02-12T10:00",
                "duration_in_minutes" => 24 * 60 * 10, // 10 full days, covering 11 calendar days
                "mileage_start" => 10,
                "mileage_end" => 50,
                "expenses_amount" => 15, // total cost = 40 - 15 = 25
            ]);

        $loan2 = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "loanable_id" => $loanable2->id,
                "platform_tip" => 0,
                "departure_at" => "2024-01-21T10:00",
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 20,
                "mileage_end" => 70, // total cost = 50
            ]);

        $loan3 = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "platform_tip" => 0,
                "loanable_id" => $loanable1->id,
                "borrower_user_id" => $borrower->id,
                "departure_at" => "2024-02-21T10:00", //   on the same day loan1 ends
                "duration_in_minutes" => 60,
                "mileage_start" => 50,
                "mileage_end" => 120, // total cost = 70
            ]);

        $unpaidLoan = Loan::factory()
            ->withMileageBasedCompensation()
            ->ongoing()
            ->create([
                "platform_tip" => 0,
                "loanable_id" => $loanable1->id,
                "departure_at" => "2024-05-21T10:00",
                "borrower_user_id" => $borrower->id,
                "mileage_start" => 0,
                "mileage_end" => 1000,
            ]);

        $wrongYearLoan = Loan::factory()
            ->withMileageBasedCompensation()
            ->validated()
            ->create([
                "platform_tip" => 0,
                "loanable_id" => $loanable2->id,
                "borrower_user_id" => $borrower->id,
                "departure_at" => "2023-05-01T10:00",
                "mileage_start" => 0,
                "mileage_end" => 1000,
            ]);
        $manualInvoice = Invoice::factory()
            ->has(
                BillItem::factory([
                    "item_type" => BillItemTypes::loanExpenses,
                    "amount" => -27,
                    "taxes_tvq" => 0,
                    "taxes_tps" => 0,
                ])
            )
            ->has(
                BillItem::factory([
                    "item_type" => BillItemTypes::loanPrice,
                    "amount" => 39,
                    "taxes_tvq" => 0,
                    "taxes_tps" => 0,
                ])
            )
            ->create([
                "user_id" => $owner->id,
                "created_at" => "2024-05-23T10:00",
            ]);

        $this->setTestNow(new CarbonImmutable("2024-12-05"));
        // Pay the loans
        $this->json("PUT", "/api/v1/loans/{$loan1->id}/pay")->assertStatus(200);
        $this->json("PUT", "/api/v1/loans/{$loan2->id}/pay")->assertStatus(200);
        $this->json("PUT", "/api/v1/loans/{$loan3->id}/pay")->assertStatus(200);

        $this->setTestNow(new CarbonImmutable("2023-05-05"));
        $this->json(
            "PUT",
            "/api/v1/loans/{$wrongYearLoan->id}/pay"
        )->assertStatus(200);

        $owner->refresh();

        // Get the report data
        [
            $linesPerLoanable,
            $sumsPerLoanable,
            $extraInvoices,
            $globalSums,
        ] = (new YearlyIncomeReport($owner, 2024))->getData();

        $expectedLinesPerLoanable = [
            $loanable1->id => [
                [
                    "loan_id" => $loan1->id,
                    "loanable_id" => $loanable1->id,
                    "payment_date" => "2024-12-05",
                    "expenses" => -15.0,
                    "price" => 40.0,
                    "net" => 25.0,
                    "distance" => 40,
                ],
                [
                    "loan_id" => $loan3->id,
                    "loanable_id" => $loanable1->id,
                    "payment_date" => "2024-12-05",
                    "expenses" => 0,
                    "price" => 70.0,
                    "net" => 70.0,
                    "distance" => 70,
                ],
            ],
            $loanable2->id => [
                [
                    "loan_id" => $loan2->id,
                    "loanable_id" => $loanable2->id,
                    "payment_date" => "2024-12-05",
                    "price" => 50.0,
                    "expenses" => 0,
                    "net" => 50.0,
                    "distance" => 50,
                ],
            ],
        ];

        $this->assertJsonUnordered(
            $expectedLinesPerLoanable,
            $linesPerLoanable
        );

        $expectedSumsPerLoanable = [
            $loanable1->id => [
                "price" => 110.0,
                "expenses" => -15.0,
                "net" => 95.0,
                "distance" => 110,
                "covered_days" => 11,
                "name" => $loanable1->name,
            ],
            $loanable2->id => [
                "price" => 50.0,
                "expenses" => 0,
                "net" => 50.0,
                "distance" => 50,
                "covered_days" => 1,
                "name" => $loanable2->name,
            ],
        ];
        $expecteExtraInvoices = [
            [
                "invoice_id" => $manualInvoice->id,
                "price" => 39.0,
                "net" => 12.0,
                "expenses" => -27.0,
            ],
        ];

        $expectedGlobalSums = [
            "distance" => 160,
            "price" => 199.0,
            "net" => 157.0,
            "expenses" => -42.0,
        ];

        $this->assertJsonUnordered($sumsPerLoanable, $expectedSumsPerLoanable);
        $this->assertJsonUnordered($extraInvoices, $expecteExtraInvoices);
        $this->assertJsonUnordered($globalSums, $expectedGlobalSums);
    }

    public function testUserReportsEndpointReturnsOnlyPublishedReportsForUser()
    {
        $ownUser = User::factory()->create();
        $otherUser = User::factory()->create();

        $publishedReport = PublishableReport::factory()->create([
            "status" => ReportStatus::Published,
        ]);

        $unpublishedReport = PublishableReport::factory()->create([
            "status" => ReportStatus::Generating,
        ]);

        // Create reports for the authenticated user
        $userPublishedReport = UserPublishableReport::factory()->create([
            "user_id" => $ownUser->id,
            "publishable_report_id" => $publishedReport->id,
        ]);
        $userUnpublishedReport = UserPublishableReport::factory()->create([
            "user_id" => $ownUser->id,
            "publishable_report_id" => $unpublishedReport->id,
        ]);
        // Create a report for another user
        $otherUserPublishedReport = UserPublishableReport::factory()->create([
            "user_id" => $otherUser->id,
            "publishable_report_id" => $publishedReport->id,
        ]);

        $this->actAs($ownUser);

        $response = $this->json("GET", "/api/v1/user_reports")->assertStatus(
            200
        );

        $response
            ->assertJsonCount(1, "data")
            ->assertJsonFragment([
                "id" => $userPublishedReport->id,
            ])
            ->assertJsonMissing([
                "id" => $userUnpublishedReport->id,
            ])
            ->assertJsonMissing([
                "id" => $otherUserPublishedReport->id,
            ]);

        $this->actAs($this->user); // admin

        $response = $this->json("GET", "/api/v1/user_reports")->assertStatus(
            200
        );

        $response
            ->assertJsonCount(3, "data")
            ->assertJsonFragment([
                "id" => $userPublishedReport->id,
            ])
            ->assertJsonFragment([
                "id" => $userUnpublishedReport->id,
            ])
            ->assertJsonFragment([
                "id" => $otherUserPublishedReport->id,
            ]);
    }
}
