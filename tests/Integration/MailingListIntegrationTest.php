<?php

namespace Tests\Integration;

use App\Enums\MailingListIntegrationStatus;
use App\Jobs\MailingLists\ImportMailingListContactsBatch;
use App\Jobs\MailingLists\RemoveMailingListContactsBatch;
use App\Models\Community;
use App\Models\Invitation;
use App\Models\MailingListIntegration;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Services\GeocoderService;
use App\Services\MailingListException;
use App\Services\MailingListService;
use Carbon\CarbonImmutable;
use Exception;
use Geocoder\Model\Address;
use Geocoder\Model\AdminLevelCollection;
use Geocoder\Model\Coordinates;
use Mockery;
use Mockery\MockInterface;
use Queue;
use Tests\ErrorableQueueFake;
use Tests\TestCase;

class MailingListIntegrationTest extends TestCase
{
    public MockInterface $mailListServiceMock;

    public function setUp(): void
    {
        parent::setUp();

        // We have to explicityly bind like this, since `$this->mock` and `$this->instance` are
        // setting an instance in the container, which is ignored by our `makeWith()` calls for the
        // service.
        $this->mailListServiceMock = Mockery::mock(MailingListService::class);
        $this->app->bind(
            MailingListService::class,
            fn() => $this->mailListServiceMock
        );

        $this->mailListServiceMock
            ->shouldReceive("throttle")
            ->andReturnUsing(fn(callable $callble) => $callble())
            ->zeroOrMoreTimes();

        $this->mailListServiceMock
            ->shouldReceive("retryThrottled")
            ->andReturnUsing(fn(callable $callble) => $callble())
            ->zeroOrMoreTimes();
    }

    public function testCreateMailingList_syncsContacts()
    {
        $importedContacts = [];

        $this->mailListServiceMock
            ->shouldReceive("getList")
            ->once()
            ->andReturn([
                "list" => [
                    "id" => 123,
                    "name" => "foo",
                ],
            ])
            ->once();
        $this->mailListServiceMock
            ->shouldReceive("getRemoveBatchSize")
            ->once()
            ->andReturn(3);
        $this->mailListServiceMock
            ->shouldReceive("getAllContactsEmailsThrottled")
            ->once()
            ->andReturn([
                "1@1.com",
                "2@2.com",
                "3@3.com",
                "4@4.com",
                "5@5.com",
            ]);

        $this->mailListServiceMock
            ->shouldReceive("removeContactBatch")
            ->with(["1@1.com", "2@2.com", "3@3.com"])
            ->andReturn(["success" => true]);

        $this->mailListServiceMock
            ->shouldReceive("removeContactBatch")
            ->with(["4@4.com", "5@5.com"])
            ->andReturn(["success" => true]);

        $this->mailListServiceMock
            ->shouldReceive("getImportBatchSize")
            ->once()
            ->andReturn(2);
        $this->mailListServiceMock
            ->shouldReceive("importContactBatch")
            ->with(Mockery::capture($importedContacts))
            ->andReturn(["success" => true]);
        $this->mailListServiceMock
            ->shouldReceive("importContactBatch")
            ->withArgs(
                fn($users) => count($users) === 1 && $users["name"] === "quux"
            )
            ->andReturn(["success" => true]);

        $community = Community::factory()->create();
        User::factory()
            ->withCommunity($community)
            ->createMany([
                [
                    "name" => "bar",
                    "last_name" => "raq",
                    "email" => "test@bar.com",
                ],
                [
                    "name" => "baz",
                    "last_name" => "zaq",
                    "email" => "test@baz.com",
                ],
                [
                    "name" => "quux",
                    "last_name" => "xuub",
                    "email" => "test@quux.com",
                ],
            ]);

        $response = $this->json("POST", "/api/v1/mailinglists", [
            "community_id" => $community->id,
            "provider" => "mailchimp",
            "api_key" => "fake_key",
            "endpoint" => "endpoint",
            "list_id" => 123,
            "community_editable" => false,
            "tag" => "some tag",
        ]);

        $response->assertStatus(201)->assertJson([
            "list_name" => "foo",
        ]);

        self::assertJsonUnordered($importedContacts, [
            [
                "name" => "bar",
                "last_name" => "raq",
                "email" => "test@bar.com",
            ],
            [
                "name" => "baz",
                "last_name" => "zaq",
                "email" => "test@baz.com",
            ],
        ]);
    }

    public function testCreateMailingList_failsWhenCannotGetListFromProvider()
    {
        $this->mailListServiceMock
            ->shouldReceive("getList")
            ->once()
            ->andThrow(
                new MailingListException(
                    new Exception("some error"),
                    "foo action"
                )
            )
            ->once();

        $community = Community::factory()->create();

        $response = $this->json("POST", "/api/v1/mailinglists", [
            "community_id" => $community->id,
            "provider" => "mailchimp",
            "api_key" => "fake_key",
            "endpoint" => "endpoint",
            "list_id" => 123,
            "community_editable" => false,
            "tag" => "some tag",
        ]);

        $response->assertStatus(422)->assertJson([
            "message" => "Erreur: some error",
        ]);
    }

    public function testCreateMailingList_suspendedWhenFailsToRemoveContacts()
    {
        ErrorableQueueFake::fake();
        $this->mailListServiceMock
            ->shouldReceive("getList")
            ->once()
            ->andReturn([
                "list" => [
                    "id" => 123,
                    "name" => "foo",
                ],
            ])
            ->once();
        $this->mailListServiceMock
            ->shouldReceive("getRemoveBatchSize")
            ->once()
            ->andReturn(3);
        $this->mailListServiceMock
            ->shouldReceive("getAllContactsEmailsThrottled")
            ->once()
            ->andReturn([
                "1@1.com",
                "2@2.com",
                "3@3.com",
                "4@4.com",
                "5@5.com",
            ]);

        $this->mailListServiceMock
            ->shouldReceive("removeContactBatch")
            ->andThrow(
                new MailingListException(
                    new Exception("bar api error message"),
                    "foo action"
                )
            )
            ->once();

        $community = Community::factory()->create();

        $response = $this->json("POST", "/api/v1/mailinglists", [
            "community_id" => $community->id,
            "provider" => "mailchimp",
            "api_key" => "fake_key",
            "endpoint" => "endpoint",
            "list_id" => 123,
            "community_editable" => false,
            "tag" => "some tag",
        ]);

        $response->assertStatus(201)->assertJson([
            "list_name" => "foo",
        ]);
        $integration = MailingListIntegration::find($response->json("id"));

        self::assertEquals(
            MailingListIntegrationStatus::Suspended,
            $integration->status
        );
        self::assertEquals(
            "[MailingList] [mailchimp] Failed foo action: bar api error message",
            $integration->last_error
        );
        Queue::assertThrewException(RemoveMailingListContactsBatch::class);
    }

    public function testCreateMailingList_suspendedWhenFailsToImportContacts()
    {
        ErrorableQueueFake::fake();
        $this->mailListServiceMock
            ->shouldReceive("getList")
            ->once()
            ->andReturn([
                "list" => [
                    "id" => 123,
                    "name" => "foo",
                ],
            ])
            ->once();
        $this->mailListServiceMock
            ->shouldReceive("getAllContactsEmailsThrottled")
            ->once()
            ->andReturn([]);

        $this->mailListServiceMock
            ->shouldReceive("getImportBatchSize")
            ->once()
            ->andReturn(2);
        $this->mailListServiceMock
            ->shouldReceive("importContactBatch")
            ->andThrow(
                new MailingListException(
                    new Exception("bar api error message"),
                    "foo action"
                )
            )
            ->once();

        $community = Community::factory()->create();
        User::factory()
            ->withCommunity($community)
            ->createMany([
                [
                    "name" => "bar",
                    "last_name" => "raq",
                    "email" => "test@bar.com",
                ],
                [
                    "name" => "baz",
                    "last_name" => "zaq",
                    "email" => "test@baz.com",
                ],
                [
                    "name" => "quux",
                    "last_name" => "xuub",
                    "email" => "test@quux.com",
                ],
            ]);

        $response = $this->json("POST", "/api/v1/mailinglists", [
            "community_id" => $community->id,
            "provider" => "mailchimp",
            "api_key" => "fake_key",
            "endpoint" => "endpoint",
            "list_id" => 123,
            "community_editable" => false,
            "tag" => "some tag",
        ]);

        $response->assertStatus(201)->assertJson([
            "list_name" => "foo",
        ]);
        $integration = MailingListIntegration::find($response->json("id"));
        self::assertEquals(
            MailingListIntegrationStatus::Suspended,
            $integration->status
        );
        self::assertEquals(
            "[MailingList] [mailchimp] Failed foo action: bar api error message",
            $integration->last_error
        );
        Queue::assertThrewException(ImportMailingListContactsBatch::class);
    }

    public function testUserApproved_addsContactForMailingListInCommunity()
    {
        $community = Community::factory()->create([
            "name" => "newly approved community",
        ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $community,
            ]);
        // $user not yet approved
        $user = User::factory()
            ->hasAttached($community)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        $communityUser = $user->refresh()->communities->first()->pivot;

        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(fn(CommunityUser $cu) => $cu->is($communityUser))
            ->andReturn()
            ->times(2); // Once per integration

        $this->json(
            "PUT",
            "/api/v1/communityUsers/{$communityUser->id}/approve"
        )->assertStatus(200);
    }

    public function testUserApproved_addsContactOnInvitationAccepted()
    {
        $community = Community::factory()->create([
            "name" => "newly approved community",
        ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $community,
            ]);
        $invitation = Invitation::factory()->create([
            "email" => "foo@bar.com",
            "community_id" => $community,
            "reason" => "Added because I trust them",
            "auto_approve" => true,
        ]);

        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(
                fn(CommunityUser $cu) => $cu->community_id === $community->id &&
                    $cu->user->email === "foo@bar.com"
            )
            ->andReturn()
            ->times(2); // Once per integration

        $response = $this->json("POST", "/api/v1/auth/register", [
            "email" => "foo@bar.com",
            "password" => "abcd1234",
            "code" => $invitation->confirmation_code,
        ])
            ->assertStatus(200)
            ->assertJsonStructure(["access_token"]);

        $this->actAs(null);
        $userResponse = $this->json(
            "GET",
            "/api/v1/auth/user",
            [],
            [
                "Authorization" => "Bearer {$response->json("access_token")}",
            ]
        )
            ->assertStatus(200)
            ->assertJson(["email" => "foo@bar.com"]);

        $user = User::find($userResponse->json("id"));

        $this->actAs($user);
        $this->json(
            "POST",
            "/api/v1/invitations/accept/registration"
        )->assertStatus(200);
        $user->refresh();

        self::assertCount(1, $user->approvedCommunities);
    }

    public function testUserSuspended_removesContactForMailingListInCommunity()
    {
        $community = Community::factory()->create([
            "name" => "newly approved community",
        ]);
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $community,
            ]);
        $communityUser = $user->refresh()->communities->first()->pivot;

        $this->mailListServiceMock
            ->shouldReceive("removeContact")
            ->withArgs(fn(User $u) => $u->is($user))
            ->andReturn()
            ->times(2); // Once per integration

        $this->json(
            "PUT",
            "/api/v1/communityUsers/{$communityUser->id}/suspend"
        )->assertStatus(200);
    }

    public function testUserUnsuspended_addsContactForMailingListInCommunity()
    {
        $community = Community::factory()->create([
            "name" => "newly approved community",
        ]);
        $user = User::factory()
            ->hasAttached($community, [
                "suspended_at" => CarbonImmutable::now(),
                "approved_at" => CarbonImmutable::now(),
            ])
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $community,
            ]);
        $communityUser = $user->refresh()->communities->first()->pivot;

        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(fn(CommunityUser $cu) => $cu->is($communityUser))
            ->andReturn()
            ->times(2); // Once per integration

        $this->json(
            "PUT",
            "/api/v1/communityUsers/{$communityUser->id}/unsuspend"
        )->assertStatus(200);
    }

    public function testUserRemovedFromCommunity_removesContactForMailingListInCommunity()
    {
        $community = Community::factory()->create([
            "name" => "newly approved community",
        ]);
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $community,
            ]);

        $this->mailListServiceMock
            ->shouldReceive("removeContact")
            ->withArgs(fn(User $u) => $u->is($user))
            ->andReturn()
            ->times(2); // Once per integration

        $this->json(
            "delete",
            "/api/v1/communities/{$community->id}/users/{$user->id}"
        )->assertStatus(200);
    }

    public function testUserChangingGeoCommunity_removesContactForMailingListInOriginalCommunity()
    {
        $originalCommunity = Community::factory()->create([
            "name" => "original community",
        ]);
        $newCommunity = Community::factory()->create([
            "name" => "original community",
        ]);

        $coordinates = new Coordinates(1, 2);
        $location = new Address(
            providedBy: "me",
            adminLevels: new AdminLevelCollection(),
            coordinates: $coordinates,
            postalCode: "h0h0h0"
        );
        GeocoderService::shouldReceive("geocode")
            ->with("fake address")
            ->andReturn($location)
            ->once();
        GeocoderService::shouldReceive("formatAddressToText")
            ->with($location)
            ->andReturn("fake formatted address")
            ->once();
        GeocoderService::shouldReceive("findCommunityFromCoordinates")
            ->with(1, 2)
            ->andReturn($newCommunity)
            ->once();

        $user = User::factory()
            ->hasAttached($originalCommunity, [
                "approved_at" => CarbonImmutable::now(),
                "join_method" => "geo",
            ])
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $originalCommunity,
            ]);
        MailingListIntegration::factory()
            ->count(1)
            ->create([
                "community_id" => $newCommunity,
            ]);

        $this->mailListServiceMock
            ->shouldReceive("removeContact")
            ->withArgs(fn(User $u) => $u->is($user))
            ->andReturn()
            ->times(2); // Once per integration in original community

        $this->actAs($user);
        $this->json("put", "/api/v1/users/$user->id", [
            "address" => "fake address",
        ])->assertStatus(200);

        $newCommunityUser = $user->refresh()->communities->first()->pivot;

        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(fn(CommunityUser $u) => $u->is($newCommunityUser))
            ->andReturn()
            ->once();
        $this->actAs($this->user); //admin
        $this->json(
            "put",
            "/api/v1/communityUsers/$newCommunityUser->id/approve"
        )->assertStatus(200);
    }

    public function testDeleteUser_removesContactForMailingListInAllCommunitiess()
    {
        $communityA = Community::factory()->create();
        $communityB = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($communityA)
            ->withCommunity($communityB)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $communityA,
            ]);
        MailingListIntegration::factory()
            ->count(1)
            ->create([
                "community_id" => $communityB,
            ]);

        $this->mailListServiceMock
            ->shouldReceive("removeContact")
            ->withArgs(fn(User $u) => $u->is($user))
            ->andReturn()
            ->times(3); // Once per integration

        $this->json("delete", "/api/v1/users/{$user->id}")->assertStatus(200);

        $this->mailListServiceMock
            ->shouldReceive("permanentlyDeleteContact")
            ->withArgs(fn($email) => $email === $user->email)
            ->andReturn()
            ->times(3); // Once per integration

        $this->json("delete", "/api/v1/users/{$user->id}", [
            "delete_data" => true,
        ])->assertStatus(200);
    }

    public function testUpdateUserName_updatesContactForMailingListInAllCommunitiess()
    {
        $communityA = Community::factory()->create();
        $communityB = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($communityA)
            ->withCommunity($communityB)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $communityA,
            ]);
        MailingListIntegration::factory()
            ->count(1)
            ->create([
                "community_id" => $communityB,
            ]);

        $communityUserA = CommunityUser::where("community_id", $communityA->id)
            ->where("user_id", $user->id)
            ->first();
        $communityUserB = CommunityUser::where("community_id", $communityB->id)
            ->where("user_id", $user->id)
            ->first();

        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(
                fn(CommunityUser $u) => $u->is($communityUserA) &&
                    $u->user->name === "baz"
            )
            ->andReturn()
            ->times(2);
        $this->mailListServiceMock
            ->shouldReceive("addOrUpdateContact")
            ->withArgs(
                fn(CommunityUser $u) => $u->is($communityUserB) &&
                    $u->user->name === "baz"
            )
            ->andReturn()
            ->times(1);

        $this->json("put", "/api/v1/users/{$user->id}", [
            "name" => "baz",
        ])->assertStatus(200);
    }

    public function testUpdateUserEmail_updatesContactForMailingListInAllCommunitiess()
    {
        $communityA = Community::factory()->create();
        $communityB = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($communityA)
            ->withCommunity($communityB)
            ->create([
                "name" => "foo",
                "last_name" => "bar",
                "email" => "test@bar.com",
            ]);
        MailingListIntegration::factory()
            ->count(2)
            ->create([
                "community_id" => $communityA,
            ]);
        MailingListIntegration::factory()
            ->count(1)
            ->create([
                "community_id" => $communityB,
            ]);

        $communityUserA = CommunityUser::where("community_id", $communityA->id)
            ->where("user_id", $user->id)
            ->first();
        $communityUserB = CommunityUser::where("community_id", $communityB->id)
            ->where("user_id", $user->id)
            ->first();

        $this->mailListServiceMock
            ->shouldReceive("updateEmail")
            ->withArgs(
                fn(CommunityUser $u, $previousEmail) => $u->is(
                    $communityUserA
                ) &&
                    $u->user->email === "test@baz.com" &&
                    $previousEmail === "test@bar.com"
            )
            ->andReturn()
            ->times(2);
        $this->mailListServiceMock
            ->shouldReceive("updateEmail")
            ->withArgs(
                fn(CommunityUser $u, $previousEmail) => $u->is(
                    $communityUserB
                ) &&
                    $u->user->email === "test@baz.com" &&
                    $previousEmail === "test@bar.com"
            )
            ->andReturn()
            ->times(1);

        $this->json("put", "/api/v1/users/{$user->id}", [
            "email" => "test@baz.com",
        ])->assertStatus(200);
    }

    public function testGetLists()
    {
        $this->mailListServiceMock
            ->shouldReceive("fetchLists")
            ->andReturn()
            ->times(1);

        $this->json("get", "/api/v1/mailinglists/lists", [
            "provider" => "mailchimp",
            "api_key" => "foo",
            "endpoint" => "bar",
            "list_id" => "baz",
        ])->assertStatus(200);

        $this->mailListServiceMock
            ->shouldReceive("fetchLists")
            ->with(2, 20)
            ->andReturn()
            ->times(1);

        $this->json("get", "/api/v1/mailinglists/lists", [
            "provider" => "mailchimp",
            "api_key" => "foo",
            "endpoint" => "bar",
            "list_id" => "baz",
            "page" => 2,
            "per_page" => 20,
        ])->assertStatus(200);
    }
}
