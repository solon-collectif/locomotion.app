<?php

namespace Tests\Integration;

use App\Events\BorrowerApprovedEvent;
use App\Listeners\SendBorrowerApprovedEmails;
use App\Mail\Borrower\Approved as BorrowerApproved;
use App\Mail\Borrower\ApprovedToAdmin as ApprovedToAdminAlias;
use App\Mail\Borrower\Completed as BorrowerCompleted;
use App\Mail\Borrower\Reviewable as BorrowerReviewable;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\File;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Storage;
use Tests\TestCase;

class BorrowerTest extends TestCase
{
    private static $getBorrowerResponseStructure = [
        "id",
        "drivers_license_number",
        "has_not_been_sued_last_ten_years",
        "submitted_at",
        "approved_at",
    ];

    public function testCreateBorrowers()
    {
        $data = [
            "drivers_license_number" => $this->faker->numberBetween(
                $min = 1111111111,
                $max = 999999999
            ),
            "has_not_been_sued_last_ten_years" => $this->faker->boolean,
            "submitted_at" => $this->faker->date(
                $format = "Y-m-d",
                $max = "now"
            ),
            "approved_at" => null,
            "user_id" => $this->user->id,
        ];

        $response = $this->json("POST", "/api/v1/borrowers", $data);

        $response->assertStatus(405);
    }

    public function testUpdateBorrowers()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $data = [
            "drivers_license_number" => $this->faker->numberBetween(
                $min = 1_111_111_111,
                $max = 999_999_999
            ),
        ];

        $response = $this->json(
            "PUT",
            "/api/v1/borrowers/$borrower->id",
            $data
        );

        $response->assertStatus(405);
    }

    public function testDeleteBorrowers()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $response = $this->json("DELETE", "/api/v1/borrowers/$borrower->id");
        $response->assertStatus(405);
    }

    public function testSubmitBorrowers()
    {
        Mail::fake();
        Storage::fake();

        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $user->id,
            "submitted_at" => null,
            "approved_at" => Carbon::now(),
        ]);
        $saaq = File::factory()
            ->withStoredFile()
            ->create(["field" => "saaq"]);
        $gaa = File::factory()
            ->withStoredFile()
            ->create(["field" => "gaa"]);

        $now = Carbon::now()->format("Y-m-d H:i:s");
        self::setTestNow($now);

        $this->actAs($user);
        $response = $this->json(
            "PUT",
            "/api/v1/users/{$user->id}/borrower/submit",
            [
                "user_id" => $user->id,
                "drivers_license_number" => "abcabc",
                "has_not_been_sued_last_ten_years" => true,
                "saaq" => [["id" => $saaq->id]],
                "gaa" => [["id" => $gaa->id]],
            ]
        );

        $response->assertStatus(200);
        $response->assertJson(["approved_at" => null]);
        self::assertEquals(
            $now,
            Carbon::parse($response->json("submitted_at"))
        );
        $borrower->refresh();
        self::assertEquals($now, Carbon::parse($borrower->submitted_at));
        Mail::assertQueued(
            BorrowerCompleted::class,
            fn($mail) => $mail->hasTo($user->email)
        );
        Mail::assertQueued(
            BorrowerReviewable::class,
            fn($mail) => $mail->hasTo($this->user->email)
        );
    }

    public function testCannotSubmitBorrowersForAnotherUser()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $anotherUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $user->id,
            "submitted_at" => null,
        ]);
        $saaq = File::factory()->create();
        $gaa = File::factory()->create();

        $now = Carbon::now()->format("Y-m-d H:i:s");
        self::setTestNow($now);

        $this->actAs($anotherUser);
        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "drivers_license_number" => "abcabc",
            "has_not_been_sued_last_ten_years" => true,
            "saaq" => [["id" => $saaq->id]],
            "gaa" => [["id" => $gaa->id]],
        ])->assertStatus(403);
    }

    public function testCannotSubmitIncompleteBorrowers()
    {
        $community = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $user->id,
            "submitted_at" => null,
        ]);
        $saaq = File::factory()->create();
        $gaa = File::factory()->create();

        $now = Carbon::now()->format("Y-m-d H:i:s");
        self::setTestNow($now);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "drivers_license_number" => "abcabc",
            "has_not_been_sued_last_ten_years" => true,
            "saaq" => [["id" => $saaq->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.gaa", ["Le champ gaa est obligatoire."]);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "drivers_license_number" => "abcabc",
            "has_not_been_sued_last_ten_years" => true,
            "gaa" => [["id" => $gaa->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.saaq", ["Le champ saaq est obligatoire."]);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "drivers_license_number" => "abcabc",
            "saaq" => [["id" => $saaq->id]],
            "gaa" => [["id" => $gaa->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.has_not_been_sued_last_ten_years", [
                "Le champ has not been sued last ten years est obligatoire.",
            ]);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "has_not_been_sued_last_ten_years" => true,
            "saaq" => [["id" => $saaq->id]],
            "gaa" => [["id" => $gaa->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.drivers_license_number", [
                "Le champ drivers license number est obligatoire.",
            ]);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "drivers_license_number" => "abcabc",
            "has_not_been_sued_last_ten_years" => true,
            "saaq" => [["id" => $saaq->id]],
            "gaa" => [["id" => $gaa->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.user_id", [
                "Le champ user id est obligatoire.",
            ]);

        $this->json("PUT", "/api/v1/users/{$user->id}/borrower/submit", [
            "user_id" => $user->id,
            "drivers_license_number" => "abcabc",
            "has_not_been_sued_last_ten_years" => false,
            "saaq" => [["id" => $saaq->id]],
            "gaa" => [["id" => $gaa->id]],
        ])
            ->assertStatus(422)
            ->assertJsonPath("errors.has_not_been_sued_last_ten_years", [
                "Le champ has not been sued last ten years doit être accepté.",
            ]);
    }

    public function testApproveBorrowers()
    {
        $meta = [];
        $meta["sent_registration_approved_email"] = true;

        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();

        $communityAdmin = User::factory()
            ->adminOfCommunity($community)
            ->create();
        $otherCommunityAdmin = User::factory()
            ->adminOfCommunity($otherCommunity)
            ->create();

        // Fake user with registration approved
        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "meta" => $meta,
            ]);

        $borrower = Borrower::factory()->create([
            "user_id" => $user->id,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/users/$user->id?fields=borrower.*"
        );
        $response
            ->assertStatus(200)
            ->assertJsonPath("borrower.approved_at", null);

        $approvedAtDate = Carbon::now()
            ->startOfSecond()
            ->toISOString();
        self::setTestNow($approvedAtDate);

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$user->id}/borrower/approve"
        );
        $response->assertStatus(200);

        $response = $this->json(
            "GET",
            "/api/v1/users/$user->id?fields=borrower.*"
        );
        $response
            ->assertStatus(200)
            ->assertJsonPath("borrower.approved_at", $approvedAtDate);

        $event = new BorrowerApprovedEvent($user);

        Mail::fake();

        // Don't trigger event. Only test listener.
        $listener = app()->make(SendBorrowerApprovedEmails::class);
        $listener->handle($event);

        // Mail to borrower.
        Mail::assertQueued(
            BorrowerApproved::class,
            fn($mail) => $mail->hasTo($user->email)
        );

        // Mail to community admin of user
        Mail::assertQueued(
            ApprovedToAdminAlias::class,
            fn($mail) => $mail->hasTo($communityAdmin->email)
        );

        // No mail to community admin of other community
        Mail::assertNotQueued(
            ApprovedToAdminAlias::class,
            fn($mail) => $mail->hasTo($otherCommunityAdmin->email)
        );

        // No mail to global admin
        Mail::assertNotQueued(
            ApprovedToAdminAlias::class,
            fn($mail) => $mail->hasTo($this->user->email)
        );
    }

    public function testPendingBorrowers()
    {
        // Fake user without registration approved
        $user = $this->user;
        $borrower = Borrower::factory()->create([
            "user_id" => $user->id,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/users/$user->id?fields=borrower.*"
        );
        $response
            ->assertStatus(200)
            ->assertJson(["borrower" => ["approved_at" => null]]);

        $approvedAtDate = Carbon::now()
            ->startOfSecond()
            ->toISOString();
        self::setTestNow($approvedAtDate);

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$user->id}/borrower/approve"
        );
        $response->assertStatus(200);

        $response = $this->json(
            "GET",
            "/api/v1/users/$user->id?fields=borrower.*"
        );
        $response
            ->assertStatus(200)
            ->assertJson(["borrower" => ["approved_at" => $approvedAtDate]]);

        $event = new BorrowerApprovedEvent($user);

        Mail::fake();

        // Don't trigger event. Only test listener.
        $listener = app()->make(SendBorrowerApprovedEmails::class);
        $listener->handle($event);

        // Mail to borrower.
        Mail::assertNotQueued(BorrowerApproved::class, function ($mail) use (
            $user
        ) {
            return $mail->hasTo($user->email);
        });
    }
}
