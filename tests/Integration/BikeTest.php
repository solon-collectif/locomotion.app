<?php

namespace Tests\Integration;

use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Tests\TestCase;

class BikeTest extends TestCase
{
    private static $getBikeResponseStructure = [
        "id",
        "name",
        "details" => ["bike_type", "model", "size"],
        "position",
        "location_description",
        "instructions",
        "comments",
    ];

    public function testBikeCreationValidation()
    {
        $ownerUser = User::factory()->create();

        $validData = [
            "name" => $this->faker->name,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "owner_user" => ["id" => $ownerUser->id],
            "type" => "bike",
            "details" => [
                "model" => $this->faker->sentence,
                "bike_type" => $this->faker->randomElement([
                    "regular",
                    "electric",
                    "fixed_wheel",
                ]),
                "size" => $this->faker->randomElement([
                    "big",
                    "medium",
                    "small",
                    "kid",
                ]),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        $missingModelData = $validData;
        unset($missingModelData["details"]["model"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingModelData);
        $response->assertStatus(201);

        $missingType = $validData;
        unset($missingType["details"]["bike_type"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingType);
        $response->assertStatus(201);

        $invalidBikeTypeData = $validData;
        $invalidBikeTypeData["details"]["bike_type"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidBikeTypeData
        );
        $response->assertStatus(422);

        $missingSize = $validData;
        unset($missingSize["details"]["size"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingSize);
        $response->assertStatus(201);

        $invalidSizeData = $validData;
        $invalidSizeData["details"]["size"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $invalidSizeData);
        $response->assertStatus(422);
    }

    public function testCreateBikes()
    {
        $ownerUser = User::factory()->create();

        $data = [
            "name" => $this->faker->name,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "owner_user" => ["id" => $ownerUser->id],
            "details" => [
                "model" => $this->faker->sentence,
                "bike_type" => $this->faker->randomElement([
                    "regular",
                    "electric",
                    "fixed_wheel",
                ]),
                "size" => $this->faker->randomElement([
                    "big",
                    "medium",
                    "small",
                    "kid",
                ]),
            ],
            "type" => "bike",
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getBikeResponseStructure);

        // Check that loanable has $ownerUser as owner
        $loanable = Loanable::find($response->json()["id"]);
        $this->assertEquals($ownerUser->id, $loanable->owner_user->id);
    }

    public function testShowBikes()
    {
        $bike = Loanable::factory()
            ->withOwner($this->user)
            ->withBike()
            ->create();

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getBikeResponseStructure);
    }

    public function testUpdateBikes()
    {
        $bike = Loanable::factory()
            ->withOwner($this->user)
            ->withBike()
            ->create();

        $data = [
            "name" => $this->faker->name,
            "details" => [
                "model" => "TERN",
            ],
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$bike->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteBikes()
    {
        $bike = Loanable::factory()
            ->withOwner($this->user)
            ->withBike()
            ->create();

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");
        $response->assertStatus(404);
    }

    public function testDeleteBikesWithActiveLoan()
    {
        // No active loan
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withBike(),
            ]);
        $bike = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");
        $response->assertStatus(404);

        // Prepaid (active) loan
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withBike(),
            ]);
        $bike = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        // Only completed loan
        $loan = Loan::factory()
            ->paid()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withBike(),
            ]);
        $bike = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);
    }

    public function testListBikes()
    {
        $bikes = Loanable::factory(2)
            ->withOwner($this->user)
            ->withBike()
            ->create();

        $response = $this->json("GET", route("loanables.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getBikeResponseStructure
                )
            );
    }
}
