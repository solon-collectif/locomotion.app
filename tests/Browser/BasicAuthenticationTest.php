<?php

namespace Tests\Browser;

use App\Models\Community;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Database\Seeders\CommunitiesTableSeeder;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class BasicAuthenticationTest extends DuskTestCase
{
    use DatabaseTruncation;
    /**
     * A basic browser test example.
     */
    public function testRegister(): void
    {
        $this->assertDatabaseCount("users", 0);
        $community = Community::factory()->create([
            "id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
            "name" => "Quux",
        ]);

        $this->browse(function (Browser $browser) {
            $browser
                ->visit("/")
                ->waitFor(".login-box__form")
                ->assertPathIs("/login")
                ->click(".navbar .nav-link:first-child")
                ->waitFor(".register-form")
                ->assertPathIs("/register/1")
                ->type("email", "foo@bar.com")
                ->type("password", "foobarbaz")
                ->type("password_repeat", "foobarbaz")
                ->press("button[type='submit']")
                ->waitFor(".profile-form")
                ->assertPathIs("/register/2");
        });

        $this->assertDatabaseCount("users", 1);

        // On reconnect, we start at /register/2
        $this->browse(function (Browser $browser) {
            $browser
                ->visit("/")
                ->waitFor(".profile-form")
                ->assertPathIs("/register/2");
        });

        $this->browse(function (Browser $browser) {
            $registerPage = $browser
                ->visit("/register/2?dev=1")
                ->waitFor(".profile-form")
                ->type("name", "Foo")
                ->type("last_name", "BarBaz")
                ->type("phone", "6666666666")
                ->type("description", "some description")
                ->type("address", "6450 CC");

            $checkbox = $registerPage
                ->element("#accept_conditions")
                ->getCoordinates()
                ->inViewPort();

            $registerPage
                ->clickAtPoint($checkbox->getX(), $checkbox->getY())
                ->attach("avatar", storage_path("seeds/user.png"))
                ->waitFor("figure.preview")
                ->click(".mx-icon-calendar")
                ->waitFor(".mx-datepicker-popup")
                ->click(".mx-date-row .cell")
                ->waitUntilMissing(".mx-datepicker-popup")
                ->press("button[type='submit']")
                ->waitFor(".register-step__community")
                ->assertPathIs("/register/3");
        });

        // For multiple communities, proof inputs are tagged with community user id.
        $communityUser = $community->users->first()->pivot;

        $this->browse(function (Browser $browser) use ($communityUser) {
            $registerPage = $browser
                ->visit("/register/3")
                ->waitFor(".user-proof-form__form")

                ->attach(
                    "residency_proof_{$communityUser->id}",
                    storage_path("seeds/user2.jpg")
                )
                ->waitForText("user2.jpg")
                ->waitFor('button[type="submit"]:not(.disabled)')
                ->press("button[type='submit']")

                ->waitFor(".register-step__reasons-why")
                ->assertPathIs("/register/4")
                ->clickLink("On avance!")
                ->waitFor(".page.dashboard")
                ->assertPathIs("/app")
                ->assertSee("Solde")
                ->assertSeeIn(".dashboard-balance__balance", '0,00 $');
        });
    }

    public function testLogin(): void
    {
        User::factory()->create([
            "email" => "foo@bar.com",
            "password" => Hash::make("abcabcabc"),
            "description" => "foo",
            "date_of_birth" => "1990-01-01",
            "balance" => 12.13,
        ]);

        $this->browse(function (Browser $browser) {
            $browser
                ->visit("/login")
                ->waitFor(".login-box__form")
                ->assertPathIs("/login")
                ->type("email", "foo@bar.com")
                ->type("password", "abcabcabc")
                ->press("button[type='submit']")
                ->waitFor(".page.dashboard")
                ->assertPathIs("/app")
                ->assertSee("Solde")
                ->assertSeeIn(".dashboard-balance__balance", '12,13 $');
        });
    }
}
