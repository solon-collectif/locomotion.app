<?php

namespace Tests\Browser;

use App\Models\Borrower;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\CarbonImmutable;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoanTest extends DuskTestCase
{
    use DatabaseTruncation;

    public function testCreateLoanFromUrl(): void
    {
        $community = Community::factory()
            ->withKMDollarsPricing()
            ->create([
                "name" => "some community",
            ]);
        $fleetOwner = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "FooFleetOwner",
                "last_name" => "OwnerLastName",
            ]);

        $fleetManager = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "BarFleetManager",
                "last_name" => "ManagerLastName",
            ]);

        $fleet = Library::factory()
            ->withOwner($fleetOwner, ["show_as_contact" => true])
            ->withManager($fleetManager, ["show_as_contact" => true])
            ->withCommunity($community)
            ->create([
                "name" => "QuuxFleet",
            ]);

        $fleetLoanable = Loanable::factory()
            ->withCar()
            ->create([
                "name" => "CarFoo",
                "library_id" => $fleet->id,
                "comments" => "Car Foo Comments",
                "location_description" =>
                    "location description: Near the other car",
            ]);

        $borrower = User::factory()
            ->withCommunity($community)
            ->withBorrower(
                Borrower::factory([
                    "approved_at" => CarbonImmutable::now()->subDay(),
                ])
            )
            ->create([
                "name" => "BazBorrower",
                "last_name" => "BorrowerLastName",
                "balance" => 40,
            ]);

        $this->assertDatabaseCount("loans", 0);

        $this->browse(function (Browser $browser) use (
            $fleetLoanable,
            $borrower
        ) {
            $browser->as($borrower);
            $browser->go("/loans/new?loanable_id=$fleetLoanable->id");
            $browser->waitFor(".loan-page .loan-context");
            $browser->assertSee("CarFoo");
            $browser->assertSee("QuuxFleet");
            $browser->assertSee("FooFleetOwner OwnerLastName");
            $browser->assertSee("BarFleetManager ManagerLastName");

            $browser->click(".loan-page .nav .nav-item:nth-child(2) a");
            $browser->waitFor(".loan-loanable-map");
            $browser->assertSee("Car Foo Comments");
            $browser->assertSee("location description: Near the other car");

            $browser->click(".loan-page .nav .nav-item:first-child a");
            $browser->waitFor(".loan-page .loan-card");

            $browser->select("alternative_to", "car");
            $browser->assertSeeIn(".price-summary", "Gratuit");

            $browser->type("estimated_distance", 50);
            // Estimation load when distance changes
            $browser->click("#message_for_owner");
            $browser->assertPresent(".price-summary .spinner");
            $browser->waitUntilMissing(".price-summary .spinner");
            $browser->assertSeeIn(".price-summary", "50,00 $");

            $browser->type("message_for_owner", "Thank you!");
            $browser->click(".loan-card .btn-success");
            $browser->waitFor(".loan-status-requested");

            $browser->assertPresent(".loan-page .nav .nav-item:nth-child(4)");
            $browser->assertSeeIn(
                ".loan-action-buttons .btn-outline-danger",
                "Annuler"
            );
            $browser->assertSeeIn(
                ".loan-action-buttons .btn-outline-warning",
                "Signaler un incident"
            );
        });

        $this->assertDatabaseCount("loans", 1);
        $loanId = Loan::query()->firstOrFail()->id;

        $this->browse(function (Browser $browser) use ($loanId, $fleetManager) {
            $browser->as($fleetManager);
            $browser->go("/loans/$loanId");
            $browser
                ->waitFor(".loan-page .loan-context")
                ->assertSee("CarFoo")
                ->assertSee("BazBorrower BorrowerLastName")
                ->assertPresent(".loan-status-action-required")
                ->assertSee("Nouvelle demande")
                ->assertSeeIn(
                    ".loan-page .nav .nav-item:nth-child(2) .badge",
                    "1"
                );

            $browser
                ->type("message_for_borrower", "No, thank you!")
                ->click(".loan-status-action-required .btn-success")
                ->waitFor(".loan-status-accepted");

            $browser
                ->click(".loan-page .nav .nav-item:nth-child(2) a")
                ->waitFor(".loan-messages-card")
                ->assertSee("Thank you!")
                ->assertSee("No, thank you!");
        });

        $this->browse(function (Browser $browser) use ($loanId, $borrower) {
            $browser->as($borrower);

            $browser->go("/loans/$loanId");
            $browser
                ->waitFor(".loan-page .loan-context")
                ->assertPresent(".loan-status-action-required");

            $browser
                ->click(".loan-status-action-required .btn")
                ->waitFor(".loan-section-contribution")
                ->assertSee("Par et pour la communauté");

            $browser
                ->element(".donation__amount input")
                ->clear()
                ->sendKeys("3.50")
                ->sendKeys(WebDriverKeys::TAB);
            $browser->waitFor(".price-summary .spinner");
            $browser->waitUntilMissing(".price-summary .spinner");
            $browser->assertSeeIn(".price-summary", "53,50 $");
            $browser->click(".loan-section-contribution .btn-primary");
            $browser->waitFor(".provision-summary");
            $browser->assertSee("Minimum (13,50 $)");
            $browser->assertSee("Ce trajet (53,50 $)");
        });

        $borrower->balance = 60;
        $borrower->save();

        $this->browse(function (Browser $browser) use ($loanId, $borrower) {
            $browser->as($borrower);
            $browser->go("/loans/$loanId");

            $browser
                ->waitFor(".loan-status-action-required")
                ->click(".loan-status-action-required .btn")
                ->waitFor(".loan-section-contribution")
                ->assertSee("Par et pour la communauté");

            $browser
                ->element(".donation__amount input")
                ->clear()
                ->sendKeys("3.50")
                ->sendKeys(WebDriverKeys::TAB);
            $browser->waitFor(".price-summary .spinner");
            $browser->waitUntilMissing(".price-summary .spinner");
            $browser->assertSeeIn(".price-summary", "53,50 $");
            $browser->click(".loan-section-contribution .btn-primary");
            $browser->waitFor(".loan-status-confirmed");
            $browser->assertPresent(".loan-section-info .loan-box.is-editing");
        });
    }

    public function testCompleteOngoingLoan(): void
    {
        $community = Community::factory()
            ->withKMDollarsPricing()
            ->create([
                "name" => "some community",
            ]);
        $fleetOwner = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "FooFleetOwner",
                "last_name" => "OwnerLastName",
            ]);

        $fleet = Library::factory()
            ->withOwner($fleetOwner, ["show_as_contact" => true])
            ->withCommunity($community)
            ->create([
                "name" => "QuuxFleet",
            ]);

        $fleetLoanable = Loanable::factory()
            ->withCar()
            ->create([
                "name" => "CarFoo",
                "library_id" => $fleet->id,
                "comments" => "Car Foo Comments",
                "location_description" =>
                    "location description: Near the other car",
            ]);

        $borrower = User::factory()
            ->withCommunity($community)
            ->withBorrower(
                Borrower::factory([
                    "approved_at" => CarbonImmutable::now()->subDay(),
                ])
            )
            ->create([
                "name" => "BazBorrower",
                "last_name" => "BorrowerLastName",
                "balance" => 40,
            ]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => $fleetLoanable->id,
                "borrower_user_id" => $borrower->id,
                "estimated_distance" => 50,
                "platform_tip" => 3.5,
            ]);

        $loanId = $loan->id;

        $this->browse(function (Browser $browser) use ($loanId, $borrower) {
            $browser->as($borrower);
            $browser->go("/loans/$loanId");

            $browser->waitFor(".loan-status-ongoing");
            $browser->assertPresent(".loan-section-info .loan-box.is-editing");

            // Early return
            $browser->click(".loan-status-ongoing .btn-ghost-secondary.btn-sm");
            $browser->waitFor(".modal-content");
            // Confirm
            $browser->click(".modal-footer .btn-outline-success");
            $browser->waitFor(".loan-status-action-required");
            $browser->assertSeeIn(".loan-status-action-required", "Validation");

            $browser->clear("mileage_start");
            $browser->type("mileage_start", 105);
            $browser->attach(
                "mileage_start_image",
                storage_path("seeds/user.png")
            );
            $browser->type("mileage_end", 145);
            $browser->type("expenses_amount", 10);
            $browser->element("#expenses_amount")->sendKeys(WebDriverKeys::TAB);
            $browser->waitFor(".price-summary .spinner");
            $browser->waitUntilMissing(".price-summary .spinner");
            // 40 (mileage) - 10 expenses + 3.5 tip
            $browser->assertSeeIn(".price-summary", "33,50 $");
            $browser->waitFor(".forms-image-uploader .bi-file-earmark-image"); // image has been uploaded

            $browser->click(".loan-section-info .btn-success");

            $browser->waitUntilMissing(
                ".loan-section-info .loan-box.is-editing"
            );
            $browser->pause(200); // give time for the animation
            $browser->assertPresent(".loan-status-ended");
            $browser->assertPresent(".loan-section-info .loan-box");
            $browser->assertSeeIn(".loan-section-info .loan-box", "105 KM");
            $browser->assertSeeIn(".loan-section-info .loan-box", "145 KM");
            $browser->assertSeeIn(".loan-section-info .loan-box", "40 KM");
            $browser->assertSeeIn(".loan-section-info .loan-box", '10,00 $');

            // Pricing tab
            $browser->click(".price-summary-card a");
            $browser->waitFor(".loan-section-contribution");
            $browser->click(".invoice-group-header");
            $browser->pause(200);
            $browser->assertSeeIn(".tiny-invoice", "40,00 $");
            $browser->assertSeeIn(".tiny-invoice", "-10,00 $");
            $browser->assertSeeIn(".tiny-invoice", "3,50 $");
            $browser->assertSeeIn(".tiny-invoice", "33,50 $");
        });

        $this->browse(function (Browser $browser) use ($fleetOwner, $loanId) {
            $browser->as($fleetOwner);
            $browser->go("/loans/$loanId");

            $browser->waitFor(".loan-status-action-required");
            $browser->assertSeeIn(".price-summary", "30,00 $"); // compensation is only 30
            $browser->assertPresent(".loan-section-info .loan-box");
            // Validate information
            $browser->click(".loan-section-info .btn-success");

            $browser->waitFor(".loan-status-validated");
        });

        $this->browse(function (Browser $browser) use ($borrower, $loanId) {
            $browser->as($borrower);
            $browser->go("/loans/$loanId");

            $browser->waitFor(".loan-status-action-required");
            $browser->assertSeeIn(".price-summary", "33,50 $"); // compensation is only 30
            // Pricing tab
            $browser->click(".price-summary-card a");
            $browser->waitFor(".loan-section-contribution");
            // Pay
            $browser->click(".loan-section-contribution .btn-primary");

            $browser->waitFor(".loan-status-completed");
            $browser->assertSee("Merci pour votre contribution au projet");
            // invoice
            $browser->click(".loan-status-completed .btn-ghost-secondary");
            $browser->waitFor(".invoice-view");
            $browser->assertSeeIn(".invoice-view", "-40,00 $"); // compensation
            $browser->assertSeeIn(".invoice-view", "10,00 $"); // expenses
            $browser->assertSeeIn(".invoice-view", "-3,05 $"); // contribution
            $browser->assertSeeIn(".invoice-view", "-33,05 $"); // sub-total
            $browser->assertSeeIn(".invoice-view", "-0,15 $"); // tps
            $browser->assertSeeIn(".invoice-view", "-0,30 $"); // tvq
            $browser->assertSeeIn(".invoice-view", "-33,50 $"); // total
        });
    }
}
