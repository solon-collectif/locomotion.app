<?php

namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class RelationInput extends BaseComponent
{
    public function __construct(private string $selector)
    {
    }

    /**
     * Get the root selector for the component.
     */
    public function selector(): string
    {
        return "";
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array<string, string>
     */
    public function elements(): array
    {
        return [];
    }

    /**
     * Select filtered option.
     */
    public function selectFilteredOption(
        Browser $browser,
        string $filterText
    ): void {
        $browser->element("$this->selector input")->sendKeys($filterText);
        $browser->waitFor(".vs__dropdown-menu .vs__dropdown-option");
        $browser->click(".vs__dropdown-menu .vs__dropdown-option");
    }
}
