<?php

namespace Tests\Browser;

use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\RelationInput;
use Tests\DuskTestCase;

class FleetManagementTest extends DuskTestCase
{
    use DatabaseTruncation;

    /**
     * A basic browser test example.
     */
    public function testCreateFleet(): void
    {
        $community = Community::factory()->create([
            "name" => "some community",
        ]);
        $globalAdmin = User::factory()->create([
            "role" => "admin",
        ]);

        $fleetOwner = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "SomeFirstName",
                "last_name" => "SomeLastName",
            ]);

        $fleetManager = User::factory()
            ->withCommunity($community)
            ->create([
                "name" => "Manager",
            ]);

        $fleetLoanable = Loanable::factory()
            ->withOwner($fleetOwner)
            ->create([
                "name" => "CarFoo",
            ]);

        $this->browse(function (Browser $browser) use ($globalAdmin) {
            $browser->as($globalAdmin);
            $browser->go("/admin/libraries");
            $browser->assertSee("Flottes");
            $browser->clickAndWait("@add-fleet-button");

            $browser->assertSee("Nouvelle flotte");
            $browser->type("name", "FooBar Fleet");
            $browser->type("phone_number", 6656464646);
            $browser->within(new RelationInput("#owner_user_id"), function (
                Browser $browser
            ) {
                $browser->selectFilteredOption("SomeF");
            });
            $browser->attach("avatar", storage_path("seeds/user.png"));
            $browser->waitFor("figure.preview");
            $browser->click("@save-fleet-button");
            $browser->waitUntilMissing("@save-fleet-button .b-spinner");
            $browser->waitForTextIn("h1", "FooBar Fleet");
        });

        $this->browse(function (Browser $browser) use (
            $fleetOwner,
            $fleetManager
        ) {
            $browser->as($fleetOwner);
            $browser->go("/");
            $browser->waitForText("CarFoo");
            $browser->assertSee("Flottes gérées (1)");
            $browser->click(
                ".dashboard__vehicles .nav-item:last-child .nav-link"
            );
            $browser->pause(200);
            $browser->clickAndWait(".library-info-box");
            $browser->waitForText("FooBar Fleet");
            $browser->clickAndWait("@edit-library-button");
            $browser->waitForText("FooBar Fleet");
            $browser->within(
                new RelationInput("@add-community-input"),
                fn(Browser $browser) => $browser->selectFilteredOption("some")
            );
            $browser->pause(500);
            $browser->waitUntilMissing(".table[aria-busy=\"true\"]");
            $browser->assertSeeIn("@communities-table", "some community");
            $browser->within(
                new RelationInput("@add-loanable-input"),
                fn(Browser $browser) => $browser->selectFilteredOption("Car")
            );
            $browser->pause(500);
            $browser->waitUntilMissing(".table[aria-busy=\"true\"]");
            $browser->assertSeeIn("@loanables-table", "CarFoo");

            $browser->click("@add-role-button");
            $browser->pause(150);
            $browser->within(
                new RelationInput("#user_id_null"),
                fn($browser) => $browser->selectFilteredOption("Mana")
            );

            $browser->type("title_null", "Some title");
            $browser->click("@save-user-role-button");
            $browser->pause(500);
            $browser->waitUntilMissing(".table[aria-busy=\"true\"]");
            $browser->assertSeeIn("@roles-table", $fleetManager->full_name);
        });

        $this->browse(function (Browser $browser) use (
            $fleetLoanable,
            $fleetManager
        ) {
            $browser->as($fleetManager);
            $browser->go("/");
            $browser->waitForText("CarFoo");
            $browser->assertSee("Flottes gérées (1)");
            $browser->click(
                ".dashboard__vehicles .nav-item:last-child .nav-link"
            );
            $browser->pause(200);
            $browser->clickAndWait(".library-info-box");
            $browser->waitForText("FooBar Fleet");
            $browser->clickAndWait("@edit-library-button");
            $browser->waitForText("FooBar Fleet");
            $browser->scrollIntoView("@loanables-table");
            $browser->pause(400);
            $browser->assertNotPresent("@add-role-button");
            $browser->assertNotPresent("@add-loanable-input");
            $browser->assertNotPresent("@add-community-input");
            $browser->clickAndWait("@edit-loanable-button");
            $browser->waitForText("CarFoo");
            $browser->assertPathIs("/profile/loanables/{$fleetLoanable->id}");
        });
    }
}
