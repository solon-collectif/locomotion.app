<?php

namespace Tests\Unit\Calendar;

use App\Calendar\DateIntervalHelper;
use App\Calendar\Interval;
use Tests\TestCase;

class DateIntervalHelperTest extends TestCase
{
    use AssertsIntervals;

    public function testIsEmpty()
    {
        // [a, a) is empty
        $interval = Interval::of("2021-10-10 12:34:56", "2021-10-10 12:34:56");
        $this->assertTrue($interval->isEmpty());

        // [a, <a) is empty
        $interval = Interval::of("2021-10-10 12:34:56", "2021-10-10 12:34:55");
        $this->assertTrue($interval->isEmpty());

        // [a, >a) is not empty
        $interval = Interval::of("2021-10-10 12:34:56", "2021-10-10 12:34:57");
        $this->assertFalse($interval->isEmpty());
    }

    public function testIntersection()
    {
        // 1. Interval starts before
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-10 12:34:56");

        $intersection = $fromInterval->intersection($interval);
        $this->assertNull($intersection, "Interval starts before");

        // 2. Interval intersects at the beginning
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-13 12:34:56");

        $intersection = $fromInterval->intersection($interval);

        $expected = Interval::of("2021-10-10 12:34:56", "2021-10-13 12:34:56");
        $this->assertEquals(
            $expected,
            $intersection,
            "Interval intersects at the beginning"
        );

        // 3. Interval is included.
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56");

        $intersection = $fromInterval->intersection($interval);

        $expected = Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56");
        $this->assertEquals($expected, $intersection, "Interval is included");

        // 4. Interval intersects at the end
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-17 23:45:01", "2021-10-31 12:34:56");

        $intersection = $fromInterval->intersection($interval);

        $expected = Interval::of("2021-10-17 23:45:01", "2021-10-19 23:45:01");
        $this->assertEquals(
            $expected,
            $intersection,
            "Interval intersects at the end"
        );

        // 5. Interval ends after
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-19 23:45:01", "2021-10-31 12:34:56");

        $intersection = $fromInterval->intersection($interval);
        $this->assertNull($intersection, "Interval ends after");

        // 6. Interval includes from interval
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-08 23:45:01", "2021-10-22 12:34:56");

        $intersection = $fromInterval->intersection($interval);

        $expected = Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01");
        $this->assertEquals($expected, $intersection, "Interval includes");

        // 7. Intersect with empty interval (start = end)
        $fromInterval = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01");

        $intersection = $fromInterval->intersection($interval);

        $this->assertNull($intersection, "Empty interval (start = end)");

        // 8. From intervals is empty (empty, not empty)
        $fromInterval = Interval::of(
            "2021-10-15 23:45:01",
            "2021-10-15 23:45:01"
        );
        $interval = Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56");

        $intersection = $fromInterval->intersection($interval);

        $this->assertNull(
            $intersection,
            "From intervals empty interval (empty, not empty)"
        );

        // 8. From intervals is empty (empty, same empty)
        $fromInterval = Interval::of(
            "2021-10-15 23:45:01",
            "2021-10-15 23:45:01"
        );
        $interval = Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01");

        $intersection = $fromInterval->intersection($interval);

        $this->assertNull(
            $intersection,
            "From intervals empty interval (empty, same empty)"
        );
    }

    public function testHasIntersection()
    {
        $fromIntervals = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-10 12:34:56");

        $hasIntersection = $fromIntervals->intersects($interval);
        $this->assertFalse($hasIntersection, "Interval starts before");

        $fromIntervals = Interval::of(
            "2021-10-10 12:34:56",
            "2021-10-19 23:45:01"
        );
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-13 12:34:56");

        $hasIntersection = $fromIntervals->intersects($interval);

        $this->assertTrue(
            $hasIntersection,
            "Interval intersects at the beginning"
        );
    }

    public function testUnion_NewIntervalStartsBefore()
    {
        $fromIntervals = [
            Interval::of("2021-10-11 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-10 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-01 23:45:01", "2021-10-10 12:34:56"),
            Interval::of("2021-10-11 12:34:56", "2021-10-19 23:45:01"),
        ];

        $this->assertSameIntervals($expected, $union, "Interval starts before");
    }

    public function testUnion_NewIntervalTouchesAtTheBeginning()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-10 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-01 23:45:01", "2021-10-19 23:45:01"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "Interval intersects at the beginning"
        );
    }

    public function testUnion_NewIntervalIntersectsAtTheBeginning()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-01 23:45:01", "2021-10-13 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-01 23:45:01", "2021-10-19 23:45:01"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "Interval intersects at the beginning"
        );
    }

    public function testUnion_NewIntervalIsIncluded()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];

        $this->assertSameIntervals($expected, $union, "Interval is included");
    }

    public function testUnion_NewIntervalIntersectsAtTheEnd()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-17 23:45:01", "2021-10-31 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-31 12:34:56"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "Interval intersects at the end"
        );
    }

    public function testUnion_NewIntervalTouchesAtTheEnd()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-19 23:45:01", "2021-10-31 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-31 12:34:56"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "Interval intersects at the end"
        );
    }

    public function testUnion_NewIntervalStartsAfter()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-20 23:45:01", "2021-10-31 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
            Interval::of("2021-10-20 23:45:01", "2021-10-31 12:34:56"),
        ];

        $this->assertSameIntervals($expected, $union, "Interval ends after");
    }

    public function testUnion_NewIntervalIncludes()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-08 23:45:01", "2021-10-22 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-08 23:45:01", "2021-10-22 12:34:56"),
        ];

        $this->assertSameIntervals($expected, $union, "Interval includes");
    }

    public function testUnion_NewIntervalIsEmpty()
    {
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $interval = Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "Empty interval (start = end)"
        );
    }

    public function testUnion_StartWithArrayWithEmpty_NewIntervalNotEmpty()
    {
        $fromIntervals = [
            Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01"),
        ];
        $interval = Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [
            Interval::of("2021-10-13 23:45:01", "2021-10-17 12:34:56"),
        ];

        $this->assertSameIntervals(
            $expected,
            $union,
            "From intervals empty interval (empty, not empty)"
        );
    }

    public function testUnion_StartWithArrayWithEmbty_NewIntervalIsSame()
    {
        // 8. From intervals is empty (empty, same empty)
        $fromIntervals = [
            Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01"),
        ];
        $interval = Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01");

        $union = DateIntervalHelper::union($fromIntervals, $interval);

        $expected = [];

        $this->assertSameIntervals(
            $expected,
            $union,
            "From intervals empty interval (empty, same empty)"
        );
    }

    public function testRemoveInterval()
    {
        // 1. Interval to remove starts before
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-01 23:45:01",
            "2021-10-10 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );
        $this->assertSameIntervals(
            $fromIntervals,
            $intervals,
            "Interval starts before"
        );

        // 2. Interval intersects at the beginning
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-01 23:45:01",
            "2021-10-13 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [
            Interval::of("2021-10-13 12:34:56", "2021-10-19 23:45:01"),
        ];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "Interval intersects at the beginning"
        );

        // 3. Interval to remove is included. Expect two intervals
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-13 23:45:01",
            "2021-10-17 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-13 23:45:01"),
            Interval::of("2021-10-17 12:34:56", "2021-10-19 23:45:01"),
        ];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "Interval is included"
        );

        // 4. Interval intersects at the end
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-17 23:45:01",
            "2021-10-31 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-17 23:45:01"),
        ];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "Interval intersects at the end"
        );

        // 5. Interval to remove ends after
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-19 23:45:01",
            "2021-10-31 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );
        $this->assertSameIntervals(
            $fromIntervals,
            $intervals,
            "Interval ends after"
        );

        // 6. Interval to remove includes from interval
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-08 23:45:01",
            "2021-10-22 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [];
        $this->assertSameIntervals($expected, $intervals, "Interval includes");

        // 7. Remove empty interval (start = end)
        $fromIntervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-15 23:45:01",
            "2021-10-15 23:45:01"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [
            Interval::of("2021-10-10 12:34:56", "2021-10-19 23:45:01"),
        ];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "Empty interval (start = end)"
        );

        // 8. From intervals is empty (empty, not empty)
        $fromIntervals = [
            Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-13 23:45:01",
            "2021-10-17 12:34:56"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "From intervals empty interval (empty, not empty)"
        );

        // 8. From intervals is empty (empty, same empty)
        $fromIntervals = [
            Interval::of("2021-10-15 23:45:01", "2021-10-15 23:45:01"),
        ];
        $intervalToSubtract = Interval::of(
            "2021-10-15 23:45:01",
            "2021-10-15 23:45:01"
        );

        $intervals = DateIntervalHelper::subtraction(
            $fromIntervals,
            $intervalToSubtract
        );

        $expected = [];
        $this->assertSameIntervals(
            $expected,
            $intervals,
            "From intervals empty interval (empty, same empty)"
        );
    }

    public function testCover()
    {
        $intervals = [
            Interval::of("2021-10-10 12:34:56", "2021-10-15 18:43:01"),
            Interval::of("2021-10-15 18:43:01", "2021-10-19 23:45:01"),
        ];

        $toCover = Interval::of("2021-10-12 12:34:56", "2021-10-18 18:43:01");
        $this->assertTrue(DateIntervalHelper::cover($intervals, $toCover));

        $toCover = Interval::of("2021-10-17 12:34:56", "2021-10-20 18:43:01");
        $this->assertFalse(DateIntervalHelper::cover($intervals, $toCover));
    }

    public function testCover_returnsFalseForSmallInterval()
    {
        $intervals = [
            Interval::of("2021-10-10 10:00:00", "2021-10-15 23:59:59"),
            Interval::of("2021-10-16 00:00:00", "2021-10-19 23:45:01"),
        ];

        $toCover = Interval::of("2021-10-15 23:59:58", "2021-10-16 00:00:01");
        $this->assertFalse(DateIntervalHelper::cover($intervals, $toCover));
    }
}
