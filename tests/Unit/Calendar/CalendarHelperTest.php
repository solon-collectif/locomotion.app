<?php

namespace Tests\Unit\Calendar;

use App\Calendar\CalendarHelper;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class CalendarHelperTest extends TestCase
{
    public function testGetCalendarDays()
    {
        // Interval starts before midnight
        $this->assertEquals(
            5,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-16 23:45:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Interval starts at midnight
        $this->assertEquals(
            4,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-17 00:00:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Interval starts after midnight
        $this->assertEquals(
            4,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-17 00:15:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Interval ends before midnight
        $this->assertEquals(
            5,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:30:00"),
                new CarbonImmutable("2022-04-20 23:45:00")
            )
        );

        // Interval ends at midnight
        $this->assertEquals(
            5,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:00:00"),
                new CarbonImmutable("2022-04-21 00:00:00")
            )
        );

        // Interval ends after midnight
        $this->assertEquals(
            6,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:15:00"),
                new CarbonImmutable("2022-04-21 00:15:00")
            )
        );

        // Interval spans some years.
        $this->assertEquals(
            4024,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2014-04-16 12:15:00"),
                new CarbonImmutable("2025-04-21 22:15:00")
            )
        );

        // Interval starts and ends at the same time.
        $this->assertEquals(
            0,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:15:00"),
                new CarbonImmutable("2022-04-16 12:15:00")
            )
        );

        // Interval ends before it starts.
        $this->assertEquals(
            0,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-20 12:30:00"),
                new CarbonImmutable("2022-04-16 23:45:00")
            )
        );
    }

    public function testGetCalendarDaysInTz()
    {
        $this->assertEquals(
            2,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-20 0:45:00"),
                new CarbonImmutable("2022-04-20 12:30:00"),
                "America/Toronto"
            )
        );

        $this->assertEquals(
            2,
            CalendarHelper::getCalendarDays(
                new CarbonImmutable("2022-04-19 23:45:00", "America/Toronto"),
                new CarbonImmutable("2022-04-20 12:30:00"),
                "America/Toronto"
            )
        );

        $this->assertEquals(
            1,
            CalendarHelper::getCalendarDays(
                (new CarbonImmutable(
                    "2022-04-20 12:30:00",
                    "America/Toronto"
                ))->startOfDay(),
                (new CarbonImmutable(
                    "2022-04-21 12:30:00",
                    "America/Toronto"
                ))->startOfDay(),
                "America/Toronto"
            )
        );
    }
}
