<?php

namespace Tests\Unit\Calendar;

trait AssertsIntervals
{
    public function assertSameIntervals(
        array $expected,
        array $actual,
        $message = ""
    ) {
        $expectedStr = [];
        foreach ($expected as $interval) {
            $expectedStr[] = [
                $interval->start->toISOString(),
                $interval->end->toISOString(),
            ];
        }

        $actualStr = [];
        foreach ($actual as $interval) {
            $actualStr[] = [
                $interval->start->toISOString(),
                $interval->end->toISOString(),
            ];
        }

        $this->assertEqualsCanonicalizing($expectedStr, $actualStr, $message);
    }
}
