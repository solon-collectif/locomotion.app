<?php

namespace Tests\Unit\Console\Commands;

use App\Enums\LoanStatus;
use App\Enums\PricingLoanableTypeValues;
use App\Mail\Loan\LoanCompletedMail;
use App\Mail\Loan\LoanEndedMail;
use App\Mail\Loan\LoanNeedsPaymentMail;
use App\Mail\Loan\LoanStalledMail;
use App\Mail\Loan\LoanValidatedMail;
use App\Models\Community;
use App\Models\Library;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class MoveLoansForwardTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // Act as random user without privileges
        $this->actAs(User::factory()->create());
    }

    public function testAutocompleteRequestedLoan_HasNoEffect_LoanNotExpired()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->subMinutes(60),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Requested, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Requested, $loan->status);
    }

    public function testAutocompleteRequestedLoan_Cancels_ExpiredLoan()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->subMinutes(80),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Requested, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Canceled, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteAcceptedLoan_Cancels_ExpiredLoan()
    {
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->subMinutes(80),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Accepted, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Canceled, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteAcceptedLoan_HasNoEffect_LoanNotExpired()
    {
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->subMinutes(60),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Accepted, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Accepted, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteConfirmedLoan_HasNoEffect_LoanNotExpired()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(10),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Confirmed, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteConfirmedLoan_MakesLoanOngoing_AfterDeparture()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(10));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                // 5 minutes in the past actually
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 70,
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ongoing, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteConfirmedLoan_MakesLoanEnded_AfterReturnAt()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->needsValidation()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);
        \Mail::assertQueued(
            LoanEndedMail::class,
            fn(LoanEndedMail $mail) => $mail->hasTo($loan->borrowerUser->email)
        );
    }

    public function testAutocompleteConfirmedLoan_MakesLoanValidated_AfterReturnAtInGracePeriodIfNoValidationNecessary()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);
        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }
    public function testAutocompleteConfirmedLoan_MakesLoanValidated_noNotificationsIfInGracePeriodAndCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteConfirmedLoan_MakesLoanCompleted_afterGracePeriodIfCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);
        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteConfirmedLoan_MakesLoanValidated_afterGracePeriodIfCannotPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);
        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteConfirmedLoan_MakesLoanValidated_afterGracePeriodIfPlatformTipNull()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => null,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);
        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteConfirmedLoan_MakesLoanCompleted_afterGracePeriodIfPlatformTipNullButExempt()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $community = Community::factory()->create();
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => null,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ])->withCommunity($community),
                "community_id" => $community,
            ]);
        self::setTestNow();

        Subscription::factory()
            ->forCommunityUser($loan->community, $loan->borrowerUser)
            ->create([
                "type" => "granted",
            ]);

        $this->assertEquals(LoanStatus::Confirmed, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);
        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteOngoingLoan_HasNoEffect_LoanNotExpired()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->subMinutes(10),
                "duration_in_minutes" => 70,
            ]);

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ongoing, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompleteOngoingLoan_MakesLoanEnded_AfterReturnAt()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->needsValidation()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);
        \Mail::assertQueued(
            LoanEndedMail::class,
            fn(LoanEndedMail $mail) => $mail->hasTo($loan->borrowerUser->email)
        );
    }

    public function testAutocompleteOngoingLoan_MakesLoanValidated_AfterReturnAtInGracePeriodIfNoValidationNecessary()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteOngoingLoan_MakesLoanValidated_noNotificationsIfInGracePeriodAndCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(50));
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 40,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);
        \Mail::assertNothingQueued();
    }

    public function testAutocompletOngoingLoan_MakesLoanCompleted_afterGracePeriodIfCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteOngoingLoan_MakesLoanValidated_afterGracePeriodIfCannotPay()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteOngoingLoan_MakesLoanValidated_afterGracePeriodIfPlatformTipNull()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => null,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteOngoingLoan_MakesLoanCompleted_afterGracePeriodIfPlatformTipNullButExempt()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $community = Community::factory()->create();
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => null,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ])->withCommunity($community),
                "community_id" => $community,
            ]);
        self::setTestNow();

        Subscription::factory()
            ->forCommunityUser($loan->community, $loan->borrowerUser)
            ->create([
                "type" => "granted",
            ]);

        $this->assertEquals(LoanStatus::Ongoing, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteEndedLoan_SendsEmail_LoanExpired_PaidOnDemand()
    {
        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");
        $now = CarbonImmutable::now()->startOfSecond();
        $ownerUser = User::factory()->create();
        $coownerUser = User::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);
        $loanable = Loanable::factory()
            ->withCar()
            ->withOwner($ownerUser)
            ->withCoowner($coownerUser)
            ->create([
                "sharing_mode" => "on_demand",
            ]);

        self::setTestNow($moreThanTwoDaysAgo);
        $loan = Loan::factory()
            ->ended()
            ->withMileageBasedCompensation()
            ->create([
                "loanable_id" => $loanable->id,
                "borrower_user_id" => $borrowerUser->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "mileage_end" => null, // data missing
            ]);
        // Setup is finished, set back test time to now.
        self::setTestNow($now);

        // Validate preconditions
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        // Run the command
        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        $this->assertEquals($now->addDays(2), $loan->attempt_autocomplete_at);

        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($coownerUser->email)
        );

        // Test this will not nag the following day
        $now = $now->addDay();
        self::setTestNow($now);
        \Mail::fake();

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);
        \Mail::assertNothingQueued();

        // Test that another email will be sent two days later
        $now = $now->addDay()->addHour();
        self::setTestNow($now);
        \Mail::fake();

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        $this->assertEquals($now->addDays(2), $loan->attempt_autocomplete_at);
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($coownerUser->email)
        );
    }

    public function testAutocompleteEndedLoan_sendsStalledEmails_LoanExpired_AutoCompleteMissing()
    {
        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");
        $now = CarbonImmutable::now()->startOfSecond();
        $ownerUser = User::factory()->create();
        $coownerUser = User::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);
        $library = Library::factory()
            ->withOwner($ownerUser)
            ->create([
                "loan_auto_validation_question" => "foo?",
            ]);
        $loanable = Loanable::factory()
            ->withCar()
            ->withCoowner($coownerUser)
            ->create([
                "sharing_mode" => "on_demand",
                "library_id" => $library->id,
            ]);

        self::setTestNow($moreThanTwoDaysAgo);
        $loan = Loan::factory()
            ->ended()
            ->withMileageBasedCompensation()
            ->create([
                "loanable_id" => $loanable->id,
                "borrower_user_id" => $borrowerUser->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "mileage_end" => null, // data missing
                "borrower_validated_at" => $now,
                "owner_validated_at" => $now,
            ]);
        // Setup is finished, set back test time to now.
        self::setTestNow($now);

        // Validate preconditions
        $this->assertEquals(LoanStatus::Ended, $loan->status);
        $this->assertTrue($loan->isFullyValidated());

        // Run the command
        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        $this->assertEquals($now->addDays(2), $loan->attempt_autocomplete_at);

        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($coownerUser->email)
        );

        // Test this will not nag the following day
        $now = $now->addDay();
        self::setTestNow($now);
        \Mail::fake();

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);
        \Mail::assertNothingQueued();

        // Test that another email will be sent two days later
        $now = $now->addDay()->addHour();
        self::setTestNow($now);
        \Mail::fake();

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        $this->assertEquals($now->addDays(2), $loan->attempt_autocomplete_at);
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($ownerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($borrowerUser->email)
        );
        \Mail::assertQueued(
            LoanStalledMail::class,
            fn(LoanStalledMail $mail) => $mail->hasTo($coownerUser->email)
        );
    }

    public function testAutocompleteEndedLoan_hasNoEffect_ifNotExpired()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->ended()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ended, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Ended, $loan->status);

        \Mail::assertNothingQueued();
    }

    public function testAutocompleteEndedLoan_CompletesLoan_ifExpiredAndCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subDays(3));
        $loan = Loan::factory()
            ->ended(0, 10)
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 12,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ended, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteEndedLoan_MakesLoanValidated_ifExpiredAndCannotPay()
    {
        self::setTestNow(CarbonImmutable::now()->subDays(3));
        $loan = Loan::factory()
            ->ended(0, 10)
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Ended, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanValidatedMail::class,
            fn(LoanValidatedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteValidatedLoan_hasNoEffect_ifNotExpired()
    {
        self::setTestNow(CarbonImmutable::now()->subMinutes(130));
        $loan = Loan::factory()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 2,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertNothingQueued();
    }

    public function testAutocompleteValidatedLoan_CompletesLoan_ifExpiredAndCanPay()
    {
        self::setTestNow(CarbonImmutable::now()->subDays(3));
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 12,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteValidatedLoan_CompletesLoanWithNoContribution_ifExpiredAndCanPayWithoutContribution()
    {
        self::setTestNow(CarbonImmutable::now()->subDays(3));
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 10,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);
        $this->assertEquals(0, $loan->platform_tip);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteValidatedLoan_hasNoEffect_ifExpiredAndCannotPay()
    {
        self::setTestNow(CarbonImmutable::now()->subDays(3));
        $loan = Loan::factory()
            ->needsValidation()
            ->validated()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "departure_at" => CarbonImmutable::now()->addMinutes(5),
                "duration_in_minutes" => 60,
                "platform_tip" => 2,
                "borrower_user_id" => User::factory([
                    "balance" => 0,
                ]),
            ]);
        self::setTestNow();

        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanNeedsPaymentMail::class,
            fn(LoanNeedsPaymentMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }

    public function testAutocompleteValidatedLoan_HasNoEffect_LoanExpired_BalanceNotSufficientForMandatoryContribution()
    {
        $now = CarbonImmutable::now()->startOfSecond();
        $twoDaysAgo = $now->sub(48, "hours");
        $moreThanTwoDaysAgo = $now->sub(54, "hours");
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create([
                "balance" => 0,
            ]);
        $loanable = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCar()
            ->create([
                "sharing_mode" => "on_demand",
            ]);
        self::setTestNow($moreThanTwoDaysAgo);
        $loan = Loan::factory()
            ->validated()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $community,
                "borrower_user_id" => $borrowerUser->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => 0,
            ]);

        Pricing::factory()
            ->forLoan($loan)
            ->create([
                "pricing_type" => "contribution",
                "rule" => 16,
                "is_mandatory" => true,
            ]);
        $borrowerUser->balance = 15;
        $borrowerUser->save();
        self::setTestNow($now);

        $this->assertEquals(LoanStatus::Validated, $loan->status);

        // Run the command
        \Mail::fake();
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Validated, $loan->status);

        \Mail::assertQueued(
            LoanNeedsPaymentMail::class,
            fn(LoanNeedsPaymentMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );

        \Mail::fake();

        // However if user has subscription, then mandatory contribution is not required anymore
        Subscription::factory()
            ->forCommunityUser($loan->community, $borrowerUser)
            ->forType(PricingLoanableTypeValues::forLoanable($loan->loanable))
            ->create([
                "start_date" => $twoDaysAgo->subDay(), // Before the loan started
            ]);

        self::assertEquals($now->addDays(2), $loan->attempt_autocomplete_at);
        self::setTestNow($now->addDays(3));
        // Run the command
        $this->artisan("loans:forward")->assertExitCode(0);

        $loan->refresh();
        $this->assertEquals(LoanStatus::Completed, $loan->status);

        \Mail::assertQueued(
            LoanCompletedMail::class,
            fn(LoanCompletedMail $mail) => $mail->hasTo(
                $loan->borrowerUser->email
            )
        );
    }
}
