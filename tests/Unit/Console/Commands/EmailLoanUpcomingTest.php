<?php

namespace Tests\Unit\Console\Commands;

use App\Mail\Loan\LoanUpcomingMail;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use Mail;
use Tests\TestCase;

class EmailLoanUpcomingTest extends TestCase
{
    public function testUpcomingLoanNotSelfService()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner()
            ->create([
                "sharing_mode" => "on_demand",
            ]);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertQueued(
            LoanUpcomingMail::class,
            fn(LoanUpcomingMail $upcoming) => $upcoming->recipient->is(
                $borrowerUser
            )
        );
        Mail::assertQueued(
            LoanUpcomingMail::class,
            fn(LoanUpcomingMail $upcoming) => $upcoming->recipient->is(
                $ownerUser
            )
        );
        Mail::assertQueued(
            LoanUpcomingMail::class,
            fn(LoanUpcomingMail $upcoming) => $upcoming->recipient->is(
                $bike->coowners->first()->user
            )
        );

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals(["sent_loan_upcoming_email" => true], $loan->meta);
    }

    public function testUpcomingLoanNotSelfService_canceled()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->withCoowner()
            ->create([
                "sharing_mode" => "on_demand",
            ]);

        $loan = Loan::factory()
            ->requested()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testUpcomingLoanSelfService()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);

        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertQueued(
            LoanUpcomingMail::class,
            fn(LoanUpcomingMail $upcoming) => $upcoming->recipient->is(
                $borrowerUser
            )
        );
        Mail::assertNotQueued(
            LoanUpcomingMail::class,
            fn(LoanUpcomingMail $upcoming) => $upcoming->recipient->is(
                $ownerUser
            )
        );

        // Reload from database.
        $loan->refresh();
        // Check that the email is marked as sent.
        $this->assertEquals(["sent_loan_upcoming_email" => true], $loan->meta);
    }

    public function testLoanNotYetUpcomingSelfService()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan starting in more than 3 hours.
                "departure_at" => Carbon::now()->add(185, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testLoanAlreadyDepartedSelfService()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created more than 3 hours ago.
                "created_at" => Carbon::now()->subtract(185, "minutes"),
                // Loan departed already.
                "departure_at" => Carbon::now()->subtract(5, "minutes"),
                "duration_in_minutes" => 600,
            ]);

        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }

    public function testLoanCreatedLessThanThreeHoursAgoSelfService()
    {
        $ownerUser = User::factory()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $bike = Loanable::factory()
            ->withOwner($ownerUser)
            ->create([
                "sharing_mode" => "self_service",
            ]);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $borrowerUser->id,
                "loanable_id" => $bike->id,
                "community_id" => $bike->community_id,
                // Loan created less than 3 hours ago.
                "created_at" => Carbon::now()->subtract(175, "minutes"),
                // Loan starting in less than 3 hours, but later than now.
                "departure_at" => Carbon::now()->add(175, "minutes"),
                "duration_in_minutes" => 600,
            ]);
        Mail::fake();

        $this->artisan("email:loan:upcoming")->assertExitCode(0);

        Mail::assertNothingQueued();
        // Reload from database.
        $loan->refresh();
        // Check that the email is not marked as sent.
        $this->assertEquals([], $loan->meta);
    }
}
