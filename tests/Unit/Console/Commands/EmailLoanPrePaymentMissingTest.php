<?php

namespace Tests\Unit\Console\Commands;

use App\Mail\Loan\LoanPrePaymentMissingMail;
use App\Models\Loan;
use App\Models\Loanable;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class EmailLoanPrePaymentMissingTest extends TestCase
{
    public function testLoanPrePaymentMissing()
    {
        \Mail::fake();

        $loan = Loan::factory()
            ->accepted()
            ->create([
                // Loan created more than 3 hours ago.
                "created_at" => CarbonImmutable::now()->subtract(
                    185,
                    "minutes"
                ),
                // Loan starting in less than 24 hours, but later than now.
                "departure_at" => CarbonImmutable::now()->add(240, "minutes"),
                "loanable_id" => Loanable::factory()
                    ->withOwner()
                    ->withCar(),
            ]);

        $this->artisan("email:loan:pre_payment_missing")->assertExitCode(0);

        \Mail::assertQueued(LoanPrePaymentMissingMail::class);

        $loan->refresh();
        $this->assertEquals(
            ["sent_loan_pre_payment_missing_email" => true],
            $loan->meta
        );
    }
}
