<?php

namespace Tests\Unit\Services;

use App\Models\Community;
use App\Services\GeocoderService;
use Tests\TestCase;

class LocoMotionGeocoderServiceTest extends TestCase
{
    public function testFindCommunityFromCoordinates()
    {
        $community = Community::factory()->create([
            "name" => "FooBar",
            "area" =>
                '{"type":"MultiPolygon","coordinates":[[[[0,0],[1,0],[1,1],[0,1],[0,0]]]]}',
            "type" => "borough",
        ]);

        $geolocatedCommunity = GeocoderService::findCommunityFromCoordinates(
            0.5,
            0.5
        );
        self::assertNotNull($geolocatedCommunity);
        self::assertEquals($community->id, $geolocatedCommunity->id);

        $geolocatedCommunity = GeocoderService::findCommunityFromCoordinates(
            2,
            2
        );
        self::assertNull($geolocatedCommunity);

        $geolocatedCommunity = GeocoderService::findCommunityFromCoordinates(
            -1,
            -1
        );
        self::assertNull($geolocatedCommunity);
    }
}
