<?php

namespace Tests\Unit\Filesystem;

use App\Facades\StorageTransaction;
use Illuminate\Http\UploadedFile;
use Storage;
use Tests\TestCase;

class StorageTransactionTest extends TestCase
{
    public function testTransaction_commit_deletesFiles()
    {
        Storage::fake();

        Storage::put("foo.txt", "test content");
        StorageTransaction::beginTransaction();
        StorageTransaction::delete("foo.txt");
        Storage::assertExists("foo.txt");

        StorageTransaction::commit();

        Storage::assertMissing("foo.txt");

        Storage::put("foo.txt", "test content");
        Storage::put("bar.txt", "test content");
        StorageTransaction::beginTransaction();
        StorageTransaction::delete("foo.txt", "bar.txt");
        Storage::assertExists("foo.txt");
        Storage::assertExists("bar.txt");

        StorageTransaction::commit();

        Storage::assertMissing("foo.txt");
        Storage::assertMissing("bar.txt");

        Storage::put("foo.txt", "test content");
        Storage::put("bar.txt", "test content");
        StorageTransaction::beginTransaction();
        StorageTransaction::delete(["foo.txt", "bar.txt"]);
        Storage::assertExists("foo.txt");
        Storage::assertExists("bar.txt");

        StorageTransaction::commit();

        Storage::assertMissing("foo.txt");
        Storage::assertMissing("bar.txt");
    }

    public function testTransaction_commit_deletesDirectories()
    {
        Storage::fake();

        Storage::put("test/foo.txt", "test content");
        StorageTransaction::beginTransaction();
        StorageTransaction::deleteDirectory("test");
        Storage::assertExists("test/foo.txt");

        StorageTransaction::commit();

        Storage::assertMissing("test/foo.txt");
        Storage::assertMissing("test/");
    }

    public function testWithoutTransaction_immediatelyDeletes()
    {
        Storage::fake();
        Storage::put("foo.txt", "test content");
        Storage::put("test/foo.txt", "test content");

        // NO StorageTransaction::beginTransaction();
        StorageTransaction::delete("foo.txt");
        StorageTransaction::deleteDirectory("test");

        Storage::assertMissing("foo.txt");
        Storage::assertMissing("test/foo.txt");
        Storage::assertMissing("test/");
    }

    public function testTransaction_rollback_deletesNewFiles()
    {
        Storage::fake();
        Storage::put("toBeCopied.txt", "test content");

        StorageTransaction::beginTransaction();
        StorageTransaction::put("foo.txt", "test content");
        Storage::assertExists("foo.txt");
        StorageTransaction::copy("toBeCopied.txt", "bar.txt");
        Storage::assertExists("bar.txt");
        StorageTransaction::putFileAs(
            "",
            UploadedFile::fake()->create("baz.txt"),
            "baz.txt"
        );
        Storage::assertExists("baz.txt");

        StorageTransaction::rollback();

        Storage::assertMissing("foo.txt");
        Storage::assertMissing("bar.txt");
        Storage::assertMissing("baz.txt");
    }

    public function testWithoutTransaction_rollback_cannotDeleteNewFiles()
    {
        Storage::fake();
        Storage::put("toBeCopied.txt", "test content");

        // NO StorageTransaction::beginTransaction();
        StorageTransaction::put("foo.txt", "test content");
        Storage::assertExists("foo.txt");
        StorageTransaction::copy("toBeCopied.txt", "bar.txt");
        Storage::assertExists("bar.txt");
        StorageTransaction::putFileAs(
            "",
            UploadedFile::fake()->create("baz.txt"),
            "baz.txt"
        );
        Storage::assertExists("baz.txt");

        StorageTransaction::rollback();

        Storage::assertExists("foo.txt");
        Storage::assertExists("bar.txt");
        Storage::assertExists("baz.txt");
    }

    public function testTransaction_rollback_revertsOverridenFiles()
    {
        Storage::fake();
        Storage::put("foo.txt", "original foo content");
        Storage::put("bar.txt", "original bar content");
        Storage::put("baz.txt", "original baz content");

        Storage::put("toBeCopied.txt", "test content");

        StorageTransaction::beginTransaction();
        StorageTransaction::put("foo.txt", "test content");
        Storage::assertExists("foo.txt");
        StorageTransaction::copy("toBeCopied.txt", "bar.txt");
        Storage::assertExists("bar.txt");
        StorageTransaction::putFileAs(
            "",
            UploadedFile::fake()->create("baz.txt"),
            "baz.txt"
        );
        Storage::assertExists("baz.txt");

        StorageTransaction::rollback();

        Storage::assertExists("foo.txt");
        self::assertEquals("original foo content", Storage::get("foo.txt"));
        Storage::assertExists("bar.txt");
        self::assertEquals("original bar content", Storage::get("bar.txt"));
        Storage::assertExists("baz.txt");
        self::assertEquals("original baz content", Storage::get("baz.txt"));
    }

    public function testWithoutTransaction_cannotRollbackOverridenFiles()
    {
        Storage::fake();
        Storage::put("foo.txt", "original foo content");
        Storage::put("bar.txt", "original bar content");
        Storage::put("baz.txt", "original baz content");

        Storage::put("toBeCopied.txt", "test content");

        // NO StorageTransaction::beginTransaction();
        StorageTransaction::put("foo.txt", "test content");
        Storage::assertExists("foo.txt");
        StorageTransaction::copy("toBeCopied.txt", "bar.txt");
        Storage::assertExists("bar.txt");
        StorageTransaction::putFileAs(
            "",
            UploadedFile::fake()->createWithContent("baz.txt", "test content"),
            "baz.txt"
        );
        Storage::assertExists("baz.txt");

        // Not rolling back the overriden files
        StorageTransaction::rollback();

        Storage::assertExists("foo.txt");
        self::assertEquals("test content", Storage::get("foo.txt"));
        Storage::assertExists("bar.txt");
        self::assertEquals("test content", Storage::get("bar.txt"));
        Storage::assertExists("baz.txt");
        self::assertEquals("test content", Storage::get("baz.txt"));
    }
}
