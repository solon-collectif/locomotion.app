<?php

namespace Tests\Unit\Models;

use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\Trailer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class LoanableTest extends TestCase
{
    public $borough;
    public $community;
    public $otherCommunity;
    public $memberOfCommunity;
    public $otherMemberOfCommunity;
    public $memberOfOtherCommunity;

    public $boroughLoanable;

    public function setUp(): void
    {
        parent::setUp();

        \Mail::fake();
        $this->community = Community::factory()->create();
        $this->otherCommunity = Community::factory()->create();

        $this->memberOfCommunity = User::factory()->create([
            "name" => "memberOfCommunity",
        ]);
        $this->community->users()->attach($this->memberOfCommunity, [
            "approved_at" => new \DateTime(),
        ]);

        $this->otherMemberOfCommunity = User::factory()->create([
            "name" => "otherMemberOfCommunity",
        ]);
        $this->community->users()->attach($this->otherMemberOfCommunity);

        $this->memberOfOtherCommunity = User::factory()->create([
            "name" => "memberOfOtherCommunity",
        ]);
        $this->otherCommunity->users()->attach($this->memberOfOtherCommunity, [
            "approved_at" => new \DateTime(),
        ]);

        foreach (
            [
                $this->memberOfCommunity,
                $this->otherMemberOfCommunity,
                $this->memberOfOtherCommunity,
            ]
            as $member
        ) {
            Loanable::factory()
                ->withOwner($member)
                ->withTrailer()
                ->create([
                    "name" => "$member->name trailer",
                ]);
        }
    }

    public function testLoanableNotAccessibleAccrossCommunitiesByDefault()
    {
        foreach (
            [
                $this->memberOfCommunity,
                $this->otherMemberOfCommunity,
                $this->memberOfOtherCommunity,
            ]
            as $member
        ) {
            $loanables = Loanable::query()
                ->accessibleBy($member)
                ->pluck("name");
            $loanableIds = Loanable::query()
                ->accessibleBy($member)
                ->pluck("id");
            $this->assertEquals(
                1,
                $loanables->count(),
                "too many loanables accessible to $member->name"
            );
            $this->assertEquals(
                $member->loanablesAsOwner()->first()->id,
                $loanableIds->first()
            );
        }
    }

    public function testLoanableBecomesAccessibleIfCommunityMembershipIsApproved()
    {
        Log::debug("Community");
        Log::debug($this->community);

        Log::debug("Community users");
        Log::debug($this->community->users);

        // Other member is not approved.

        $loanables = Loanable::query()
            ->accessibleBy($this->memberOfCommunity)
            ->pluck("id");
        $this->assertEquals(1, $loanables->count());
        $this->assertEquals(
            $this->memberOfCommunity->loanablesAsOwner()->first()->id,
            $loanables->first()
        );

        // On ajoute other member of community
        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $loanables = Loanable::query()
            ->accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->get();

        $this->assertEquals(2, $loanables->count());
        $this->assertEquals(
            $this->memberOfCommunity->loanablesAsOwner()->first()->id,
            $loanables[0]->id
        );
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanablesAsOwner()->first()->id,
            $loanables[1]->id
        );
    }

    public function testCarAccessibleEvenWhenBorrowerNotApproved()
    {
        $car = Loanable::factory()
            ->withOwner($this->memberOfCommunity)
            ->withCar()
            ->create();

        $loanables = Loanable::query()
            ->accessibleBy($this->memberOfCommunity)
            ->pluck("id");
        $this->assertEquals(2, $loanables->count());

        $loanables = Loanable::query()
            ->accessibleBy($this->otherMemberOfCommunity)
            ->pluck("id");
        $this->assertEquals(1, $loanables->count());
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanablesAsOwner()->first()->id,
            $loanables[0]
        );

        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $loanables = Loanable::query()
            ->accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(3, $loanables->count());
    }

    public function testLoanableAccessibleThroughInheritedClasses()
    {
        Loanable::factory()
            ->withOwner($this->memberOfCommunity)
            ->withCar()
            ->create();

        $bikes = Bike::query()
            ->accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $bikes->count());

        $trailers = Trailer::query()
            ->accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $trailers->count());

        $cars = Car::query()
            ->accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $cars->count());

        $cars = Car::query()
            ->accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $cars->count());

        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $cars = Car::query()
            ->accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $cars->count());

        $borrower = new Borrower();
        $borrower->user()->associate($this->otherMemberOfCommunity);
        $borrower->approved_at = new \DateTime();
        $borrower->save();

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $cars = Car::query()
            ->accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $cars->count());
    }

    public function testIsAvailableEventIfRequestedLoanExists()
    {
        $bike = Loanable::factory()
            ->withOwner($this->memberOfCommunity)
            ->create();

        $user = User::factory()
            ->withBorrower()
            ->create();
        Loan::factory()
            ->requested()
            ->create([
                "borrower_user_id" => $user->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-10 10:10:00",
                "duration_in_minutes" => 60,
            ]);

        Loan::factory()
            ->accepted()
            ->canceled()
            ->create([
                "borrower_user_id" => $user->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-11 10:10:00",
                "duration_in_minutes" => 60,
            ]);

        Loan::factory()
            ->confirmed()
            ->create([
                "borrower_user_id" => $user->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-12 10:10:00",
                "duration_in_minutes" => 60,
            ])
            ->refresh();

        $this->assertEquals(
            true,
            $bike->isAvailable(new Carbon("3000-10-10 10:20:00"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(new Carbon("3000-10-10 11:20:00"), 60)
        );

        $this->assertEquals(
            true,
            $bike->isAvailable(new Carbon("3000-10-11 10:20:00"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(new Carbon("3000-10-11 11:20:00"), 60)
        );

        $this->assertEquals(
            false,
            $bike->isAvailable(new Carbon("3000-10-12 10:20:00"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(new Carbon("3000-10-12 11:20:00"), 60)
        );
    }

    public function testIsAvailableEarlyIfPaidBeforeDurationInMinutes()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => Carbon::now(),
                "duration_in_minutes" => 60,
            ]);

        $bike = $loan->loanable;

        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(61, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(59, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(31, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(29, "minutes"), 60)
        );

        // The loan was completed earlier
        $loan->setLoanStatusPaid();
        $loan->paid_at = Carbon::now()->add(30, "minutes");
        $loan->save();

        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(61, "minutes"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(59, "minutes"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(31, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(29, "minutes"), 60)
        );
    }

    public function testLoanableAvailabilityRulesAreEmptyWhenInvalid()
    {
        $trailer = Loanable::factory()
            ->withOwner()
            ->withTrailer()
            ->create([
                "availability_json" => "[{",
            ]);

        Log::shouldReceive("error")
            ->once()
            ->withArgs(function ($message) use ($trailer) {
                return strpos($message, "\"[{\"") !== false &&
                    strpos($message, (string) $trailer->id) !== false;
            });

        $rules = $trailer->getAvailabilityRules();

        $this->assertEquals([], $rules);
    }

    public function testLoanableAvailabilityRules()
    {
        $trailer = Loanable::factory()
            ->withOwner()
            ->withTrailer()
            ->create([
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["MO","TU","WE","TH","FR"],"period":"00:00-24:00"}]',
            ]);

        $rules = $trailer->getAvailabilityRules();

        $expected = [
            [
                "available" => true,
                "type" => "weekdays",
                "scope" => ["MO", "TU", "WE", "TH", "FR"],
                "period" => "00:00-24:00",
            ],
        ];

        $this->assertEquals($expected, $rules);
    }

    public function testAddCoowner()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $user = User::factory()->create();

        $initialCoownerCount = $loanable->coowners->count();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $user->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($userRole);
        $loanable->refresh();

        self::assertCount($initialCoownerCount + 1, $loanable->coowners);
        self::assertContains(
            $user->id,
            $loanable->coowners->map(fn($coowner) => $coowner->user->id)
        );
    }

    public function testRemoveCoowner()
    {
        /** @var Loanable $loanable */
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $user = User::factory()->create();

        $initialCoownerCount = $loanable->coowners->count();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $user->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $loanable->userRoles()->save($userRole);
        $loanable->refresh();

        $loanable
            ->userRoles()
            ->where("user_id", $user->id)
            ->where("role", LoanableUserRoles::Coowner)
            ->delete();
        $loanable->refresh();

        self::assertCount($initialCoownerCount, $loanable->coowners);
        self::assertNotContains(
            $user->id,
            $loanable->coowners->map(fn($coowner) => $coowner->user->id)
        );
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        $someLoanable = Loanable::factory()
            ->withOwner()
            ->create();
        $otherLoanable = Loanable::factory()
            ->withOwner()
            ->create();

        // 2 here + 3 in setup
        self::assertCount(5, Loanable::for("admin", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someLoanable = Loanable::factory()
            ->withOwner()
            ->create();
        $otherLoanable = Loanable::factory()
            ->withOwner()
            ->create();

        $communityAdmin = User::factory()
            ->hasAttached($someLoanable->owner_user->main_community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        self::assertCount(1, Loanable::for("admin", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someLoanable = Loanable::factory()
            ->withOwner()
            ->create();
        $otherLoanable = Loanable::factory()
            ->withOwner()
            ->create();

        $regularUser = User::factory()
            ->hasAttached($someLoanable->owner_user->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherLoanable->owner_user->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, Loanable::for("admin", $regularUser)->get());
    }

    public function testDelete_cancelsOngoingLoans()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $loanable->delete();
        $loan->refresh();

        self::assertEquals(LoanStatus::Canceled, $loan->status);
    }

    public function testDelete_invalidatesLoanableCache()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create();

        $assertion = \Cache::shouldReceive("forget")
            ->with("loanables_bike_$community->id")
            ->andReturn(true)
            ->once();

        \Cache::shouldReceive("forget")
            ->with("loanables_bike_$otherCommunity->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_car_$community->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_trailer_$community->id")
            ->never();

        $loanable->delete();

        $assertion->verify();
    }

    public function testRestored_invalidatesLoanableCache()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create([
                "deleted_at" => Carbon::now(),
            ]);

        $assertion = \Cache::shouldReceive("forget")
            ->with("loanables_bike_$community->id")
            ->andReturn(true)
            ->once();

        \Cache::shouldReceive("forget")
            ->with("loanables_bike_$otherCommunity->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_car_$community->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_trailer_$community->id")
            ->never();

        $loanable->restore();

        $assertion->verify();
    }

    public function testSave_invalidatesLoanableCache()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create([
                "availability_mode" => "never",
            ]);

        $assertion = \Cache::shouldReceive("forget")
            ->with("loanables_bike_$community->id")
            ->andReturn(true)
            ->once();

        \Cache::shouldReceive("forget")
            ->with("loanables_bike_$otherCommunity->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_car_$community->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_trailer_$community->id")
            ->never();

        $loanable->availability_mode = "always";
        $loanable->save();

        $assertion->verify();
    }
}
