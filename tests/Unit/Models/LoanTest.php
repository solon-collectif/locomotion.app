<?php

namespace Tests\Unit\Models;

use App\Enums\BillItemTypes;
use App\Enums\LoanableUserRoles;
use App\Enums\LoanStatus;
use App\Enums\PricingLoanableTypeValues;
use App\Enums\Requirement;
use App\Models\BillItem;
use App\Models\Community;
use App\Models\Invoice;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\Pricing;
use App\Models\Subscription;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class LoanTest extends TestCase
{
    public function testCancel_Now()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->setLoanStatusCanceled();

        $this->assertTrue($loan->isCanceled());

        $this->assertNotNull($loan->canceled_at);
        $this->assertEquals(LoanStatus::Canceled, $loan->status);
    }

    public function testCancel_At()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->setLoanStatusCanceled(
            new CarbonImmutable("2022-04-16 12:34:56")
        );

        $this->assertTrue($loan->isCanceled());

        $this->assertNotNull($loan->canceled_at);
        $this->assertEquals(LoanStatus::Canceled, $loan->status);
        $this->assertEquals("2022-04-16 12:34:56", $loan->canceled_at);
    }

    public function testLoanTimesAndDurations_EarlyPayment()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = CarbonImmutable::now("America/Toronto")
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->paid($departureAt->addMinutes(60))
            ->create([
                "departure_at" => $departureAt->toISOString(),
                "duration_in_minutes" => 75,
            ]);

        $loan->refresh();

        // Assert that the loan return time accounts for early payment.
        $this->assertEquals(
            $departureAt->addMinutes(60),
            $loan->actual_return_at
        );

        // The loan initial duration didn't change
        $this->assertEquals(75, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_EarlyPaymentWithAcceptedExtension()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = CarbonImmutable::now("America/Toronto")
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->paid($departureAt->addMinutes(105))
            ->create([
                "departure_at" => $departureAt->toISOString(),
                "duration_in_minutes" => 75,
                "extension_duration_in_minutes" => 120,
            ]);

        $loan->acceptExtension();
        $loan->save();

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertEquals(
            $departureAt->addMinutes(105),
            $loan->actual_return_at
        );

        // Check the loan's duration
        $this->assertEquals(120, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ShortSpanOverTwoCalendarDays()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = CarbonImmutable::now("America/Toronto")
            ->setHours(23)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => $departureAt->toISOString(),
                "duration_in_minutes" => 60,
            ]);

        $loan->refresh();

        // Validate loan return time.
        $this->assertTrue(
            $departureAt->addMinutes(60)->equalTo($loan->actual_return_at)
        );

        // Check the loan's actual duration.
        $this->assertEquals(60, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(2, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_oneDayInLocalTimezoneButTwoInUTC()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = new CarbonImmutable(
            "2024-02-02T18:30",
            "America/Toronto"
        );

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => $departureAt->toISOString(),
                "duration_in_minutes" => 120,
            ]);

        $loan->refresh();

        // Validate loan return time.
        $this->assertTrue(
            $departureAt->addMinutes(120)->equalTo($loan->actual_return_at)
        );

        // Check the loan's actual duration.
        $this->assertEquals(120, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDays()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = new CarbonImmutable(
            "2024-10-20T04:30:00",
            "America/Toronto"
        );

        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "departure_at" => $departureAt->toISOString(),
                // Arbitrarily more than 3 days. Loan then spans onto 4 calendar days.
                "duration_in_minutes" => 3 * 24 * 60 + 415,
            ]);

        $loan->refresh();

        // Validate loan return time.
        $this->assertTrue(
            $departureAt->addMinutes(4735)->equalTo($loan->actual_return_at)
        );

        // Check the loan's actual duration.
        $this->assertEquals(4735, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(4, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDaysAndEarlyPayment()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only. Loanable timezone is America/Toronto by default.
        $departureAt = CarbonImmutable::now("America/Toronto")
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->paid($departureAt->addMinutes(2 * 24 * 60 + 675))
            ->create([
                "departure_at" => $departureAt->toISOString(),
                // Arbitrarily more than 3 days. Loan then spans onto 4 calendar days.
                "duration_in_minutes" => 3 * 24 * 60 + 415,
            ]);

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertEquals(
            $departureAt->addMinutes(2 * 24 * 60 + 675),
            $loan->actual_return_at
        );

        // Check the loan's  duration.
        $this->assertEquals(3 * 24 * 60 + 415, $loan->duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(3, $loan->calendar_days);
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        Loan::factory()->create();
        Loan::factory()->create();

        self::assertCount(2, Loanable::for("admin", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someLoan = Loan::factory()->create();
        Loan::factory()->create();

        $communityAdmin = User::factory()
            ->hasAttached($someLoan->community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        self::assertCount(1, Loanable::for("admin", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someLoan = Loan::factory()->create();
        $otherLoan = Loan::factory()->create();

        $regularUser = User::factory()
            ->hasAttached($someLoan->community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherLoan->community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, Loanable::for("admin", $regularUser)->get());
    }

    public function testIsFree_ifNoPricings()
    {
        $community = Community::factory()->create();
        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        self::assertTrue($loan->is_free);
    }

    public function testIsNotFree_ifPricingExist()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        self::assertFalse($loan->is_free);
    }

    public function testIsFree_ifCompleteWithoutBorrowerInvoice()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $loan = Loan::factory()
            ->paid()
            ->create([
                "community_id" => $community,
                "status" => "completed",
            ]);

        self::assertTrue($loan->is_free);
    }

    public function testIsNotFree_ifCompleteWithBorrowerInvoice()
    {
        $invoice = Invoice::factory()->create();

        BillItem::factory()->create([
            "invoice_id" => $invoice,
            "amount" => 5,
            "item_type" => BillItemTypes::loanPrice,
        ]);

        $loan = Loan::factory()
            ->paid()
            ->create([
                "borrower_invoice_id" => $invoice,
            ]);

        $loan->refresh();

        self::assertFalse($loan->is_free);
    }

    public function testIsFree_ifPricingExistButBorrowerIsOwner()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $user = User::factory()
            ->withBorrower()
            ->create();

        $loan = Loan::factory()->create([
            "community_id" => $community,
            "borrower_user_id" => $user,
            "loanable_id" => Loanable::factory()
                ->withOwner($user)
                ->create(),
        ]);

        self::assertTrue($loan->is_free);
    }

    public function testIsFree_ifPricingExistButBorrowerIsExemptCoowner()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $user = User::factory()
            ->withBorrower()
            ->create();
        $loanable = Loanable::factory()
            ->withOwner()
            ->create();
        $coowner = LoanableUserRole::factory()->create([
            "user_id" => $user,
            "ressource_id" => $loanable,
            "ressource_type" => "loanable",
            "role" => LoanableUserRoles::Coowner,
            "pays_loan_price" => true,
        ]);

        $loan = Loan::factory()->create([
            "community_id" => $community,
            "borrower_user_id" => $user,
            "loanable_id" => $loanable,
        ]);

        self::assertFalse($loan->is_free);

        $coowner->pays_loan_price = false;
        $coowner->save();

        $loan->refresh();
        self::assertTrue($loan->is_free);
    }

    public function testIsNotFree_ifContributionMandatory()
    {
        $community = Community::factory()->create();
        $pricing = Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "rule" => 10,
                "is_mandatory" => false,
                "pricing_type" => "contribution",
            ]);

        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        self::assertTrue($loan->is_free);

        $pricing->is_mandatory = true;
        $pricing->save();
        $loan->refresh();

        self::assertFalse($loan->is_free);
    }

    public function testIsFree_ifContributionMandatoryButUserHasPaidSubscription()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $community,
                "rule" => 10,
                "is_mandatory" => true,
                "pricing_type" => "contribution",
            ]);

        $loanable = Loanable::factory()
            ->withOwner()
            ->withTrailer()
            ->create();

        $loan = Loan::factory()->create([
            "community_id" => $community,
            "loanable_id" => $loanable,
            "borrower_user_id" => $borrowerUser,
        ]);

        self::assertFalse($loan->is_free);

        Subscription::factory()
            ->forCommunityUser($community, $loan->borrowerUser)
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "type" => "paid",
            ]);

        $loan->refresh();

        self::assertTrue($loan->is_free);
    }

    public function testIsFree_ifContributionMandatoryButUserHasGrantSubscription()
    {
        $community = Community::factory()->create();
        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::Trailer)
            ->create([
                "community_id" => $community,
                "rule" => 10,
                "is_mandatory" => true,
                "pricing_type" => "contribution",
            ]);

        $loanable = Loanable::factory()
            ->withOWner()
            ->withTrailer()
            ->create();

        $loan = Loan::factory()->create([
            "community_id" => $community,
            "loanable_id" => $loanable,
            "borrower_user_id" => $borrowerUser,
        ]);

        self::assertFalse($loan->is_free);

        Subscription::factory()
            ->forCommunityUser($community, $loan->borrowerUser)
            ->create([
                "type" => "granted",
            ]);

        $loan->refresh();

        self::assertTrue($loan->is_free);
    }

    public function testIsFree_ifContributionMandatoryButCommunityExempted()
    {
        $community = Community::factory()->create();
        Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "rule" => 10,
                "is_mandatory" => true,
                "pricing_type" => "contribution",
            ]);

        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        self::assertFalse($loan->is_free);

        $community->exempt_from_contributions = true;
        $community->save();
        $loan->refresh();

        self::assertTrue($loan->is_free);
    }

    public function testApplicableAmountTypes_takesIntoAccountMandatoryContributions()
    {
        $community = Community::factory()->create();
        $pricing = Pricing::factory()
            ->forAllTypes()
            ->create([
                "community_id" => $community,
                "rule" => 10,
                "is_mandatory" => false,
                "pricing_type" => "contribution",
            ]);

        $loan = Loan::factory()->create([
            "community_id" => $community,
        ]);

        self::assertEquals(
            Requirement::optional,
            $loan->applicable_amount_types->contributions
        );

        $pricing->is_mandatory = true;
        $pricing->save();
        $loan->refresh();
        self::assertEquals(
            Requirement::required,
            $loan->applicable_amount_types->contributions
        );
    }

    public function testActualDistance_includesTakeoverForSelfServiceCar()
    {
        $loanable = Loanable::factory()
            ->withOWner()
            ->withCar()
            ->create([
                "sharing_mode" => "self_service",
            ]);
        $loan = Loan::factory()
            ->ended(1000, 1500)
            ->withMileageBasedCompensation()
            ->create([
                "estimated_distance" => 100,
                "loanable_id" => $loanable,
            ]);

        self::assertEquals(500, $loan->actual_distance);
    }

    public function testActualDistance_isEstimatedDistanceForNonCars()
    {
        $loanable = Loanable::factory()
            ->withOwner()
            ->withTrailer()
            ->create([
                "sharing_mode" => "on_demand",
            ]);
        $loan = Loan::factory()
            ->ended(1000, 1500)
            ->create([
                "estimated_distance" => 100,
                "loanable_id" => $loanable,
            ]);

        self::assertEquals(100, $loan->actual_distance);
        $loanable = Loanable::factory()
            ->withOwner()
            ->withBike()
            ->create([
                "sharing_mode" => "on_demand",
            ]);
        $loan = Loan::factory()
            ->ended(1000, 1500)
            ->create([
                "estimated_distance" => 100,
                "loanable_id" => $loanable,
            ]);

        self::assertEquals(100, $loan->actual_distance);
    }
}
