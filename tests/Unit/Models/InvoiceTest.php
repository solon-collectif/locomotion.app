<?php

namespace Tests\Unit\Models;

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Invoice;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    public function testAmountFormatting()
    {
        $this->assertEquals(Invoice::formatAmountForDisplay(10.124), "10,12");
        $this->assertEquals(Invoice::formatAmountForDisplay(10.125), "10,13");
        $this->assertEquals(
            Invoice::formatAmountForDisplay(10000.126),
            "10 000,13"
        );
    }

    public function testGetUserBalanceChange_ManualInvoice()
    {
        // For manual invoices, before introducing amount types (debit/credit), positive
        // amounts were added to balance, and negative amounts were
        // subtracted balance.
        $invoice = Invoice::factory()->create([
            "paid_at" => null,
        ]);

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Item for manual invoice",
            "item_type" => null,
            "amount" => 5.0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "item_type" => BillItemTypes::balanceProvision,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(5.0, $invoice->getUserBalanceChange());
    }

    public function testGetUserBalanceChange_ProvisionWithFees()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Ajout au compte LocoMotion : 5,00$",
            "item_type" => BillItemTypes::balanceProvision,
            "amount" => 5.0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);
        $invoiceItems[] = new BillItem([
            "label" => "Frais de transaction Stripe : 0,41$",
            "item_type" => BillItemTypes::feesStripe,
            "amount" => 0.41,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(5.0, $invoice->getUserBalanceChange());
    }

    public function testGetUserBalanceChange_BorrowerLoan()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Coût de l'emprunt de l'auto le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::loanPrice,
            "amount" => -103.59,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoiceItems[] = new BillItem([
            "label" =>
                "Coût de l'assurance pour l'emprunt de l'auto le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::loanInsurance,
            "amount" => -8.0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoiceItems[] = new BillItem([
            "label" =>
                "Dépenses pour l'emprunt de l'auto le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::loanExpenses,
            "amount" => 58.95,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoiceItems[] = new BillItem([
            "label" =>
                "Contribution volontaire pour l'emprunt de Kia Sorento le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::donationLoan,
            "amount" => -4.35,
            "taxes_tps" => -0.22,
            "taxes_tvq" => -0.43,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(-57.64, $invoice->getUserBalanceChange());
    }

    public function testGetUserBalanceChange_OwnerLoan()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Coût de l'emprunt de l'auto le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::loanPrice,
            "amount" => 103.59,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoiceItems[] = new BillItem([
            "label" =>
                "Dépenses pour l'emprunt de l'auto le 6 juillet 2023 09:30",
            "item_type" => BillItemTypes::loanExpenses,
            "amount" => -58.95,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(44.64, $invoice->getUserBalanceChange());
    }

    public function testGetUserBalanceChange_BalanceRefund()
    {
        // Balance refunds are manual invoices.
        $invoice = Invoice::factory()->create([
            "paid_at" => null,
        ]);

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Remboursement du solde",
            "item_type" => BillItemTypes::balanceRefund,
            "amount" => -103.59,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(-103.59, $invoice->getUserBalanceChange());
    }

    public function testGetUserBalanceChange_BalanceDonation()
    {
        // Balance donations are manual invoices for the moment.
        $invoice = Invoice::factory()->create([
            "paid_at" => null,
        ]);

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Don de solde pour LocoMotion",
            "item_type" => BillItemTypes::donationBalance,
            "amount" => -71.23,
            "taxes_tps" => -3.56,
            "taxes_tvq" => -7.1,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertEquals(-81.89, $invoice->getUserBalanceChange());
    }

    public function testHasNonZeroItems_withNonZeroItems()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Coût de l'emprunt",
            "item_type" => BillItemTypes::loanPrice,
            "amount" => -103.59,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoiceItems[] = new BillItem([
            "label" => "Contribution volontaire",
            "item_type" => BillItemTypes::donationLoan,
            "amount" => 0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertTrue($invoice->hasNonZeroItems());
    }

    public function testHasNonZeroItems_withoutNonZeroItems()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Contribution volontaire",
            "item_type" => BillItemTypes::donationLoan,
            "amount" => 0,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertFalse($invoice->hasNonZeroItems());
    }

    public function testHasNonZeroItems_edgeCase()
    {
        $invoice = Invoice::factory()->create();

        $invoiceItems = [];
        $invoiceItems[] = new BillItem([
            "label" => "Contribution volontaire",
            "item_type" => BillItemTypes::donationLoan,
            "amount" => -0.0049,
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
        ]);

        $invoice->billItems()->saveMany($invoiceItems);

        $this->assertFalse($invoice->hasNonZeroItems());
    }
}
