<?php

namespace Tests\Unit\Models\Pivots;

use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class CommunityUserTest extends TestCase
{
    public function testDelete_invalidatesLoanableCache()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create();

        $assertion = \Cache::shouldReceive("forget")
            ->with("loanables_bike_$community->id")
            ->andReturn(true)
            ->once();

        \Cache::shouldReceive("forget")
            ->with("loanables_bike_$otherCommunity->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_car_$community->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_trailer_$community->id")
            ->never();

        $user->communities()->sync([]);

        $assertion->verify();
    }

    public function testSave_invalidatesLoanableCache()
    {
        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        $loanable = Loanable::factory()
            ->withOwner($user)
            ->withBike()
            ->create();

        $assertion = \Cache::shouldReceive("forget")
            ->with("loanables_bike_$community->id")
            ->andReturn(true)
            ->once();

        \Cache::shouldReceive("forget")
            ->with("loanables_bike_$otherCommunity->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_car_$community->id")
            ->never();
        \Cache::shouldReceive("forget")
            ->with("loanables_trailer_$community->id")
            ->never();

        $user->main_community->pivot->suspended_at = Carbon::now();
        $user->main_community->pivot->save();

        $assertion->verify();
    }
}
