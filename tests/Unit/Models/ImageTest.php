<?php

namespace Tests\Unit\Models;

use App\Enums\FileMoveResult;
use App\Models\Image;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Log;
use Mockery;
use Storage;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class ImageTest extends TestCase
{
    public function testUpdateLocation()
    {
        $user = User::factory()->create();

        $fakeFile = UploadedFile::fake()->image("filename.png");
        Storage::fake();
        Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.png",
            $fakeFile->getContent()
        );
        Storage::put(
            "storage/some/wrong/initial/path/thumbnail_wrongfilename.jpg",
            $fakeFile->getContent()
        );
        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.png",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
        ]);

        $image->updateFilePath();

        $uniqueDirName = $image->generateUniqueFolderName();
        self::assertEquals(
            "images/user/$user->id/avatar/$uniqueDirName",
            $image->path
        );
        self::assertEquals("avatar.png", $image->filename);

        self::assertTrue(
            Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/avatar.png"
            )
        );
        self::assertTrue(
            Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/avatar_avatar.jpg"
            )
        );
        self::assertTrue(
            Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/thumbnail_avatar.jpg"
            )
        );
        self::assertFalse(Storage::exists("storage/some/wrong/initial/path"));

        assertEquals(FileMoveResult::unchanged, $image->updateFilePath());
    }

    public function testDelete_deletesWholeFolder()
    {
        $user = User::factory()->create();

        Storage::fake();
        $fakeFile = UploadedFile::fake()->image("filename.png");
        Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.png",
            $fakeFile->getContent()
        );
        Storage::put(
            "storage/some/wrong/initial/path/thumbnail_wrongfilename.png",
            $fakeFile->getContent()
        );
        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.jpg",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
        ]);

        $uniqueDirName = $image->generateUniqueFolderName();
        $image->delete();

        self::assertFalse(
            Storage::exists("images/user/$user->id/avatar/$uniqueDirName")
        );
        self::assertFalse(Storage::exists("storage/some/wrong/initial/path"));
    }

    public function testDelete_logsIfFails()
    {
        $user = User::factory()->create();

        Storage::fake();

        $image = Image::withoutEvents(
            fn() => Image::factory()->create([
                "path" => "some/wrong/initial/path",
                "filename" => "wrongfilename.jpg",
                "field" => "avatar",
                "imageable_type" => "user",
                "imageable_id" => $user->id,
            ])
        );

        Storage::shouldReceive("deleteDirectory")
            ->once()
            ->andReturn(false);

        $assertion = Log::shouldReceive("warning")
            ->with(
                Mockery::on(
                    fn($log) => str_starts_with(
                        $log,
                        "[Image.php] Failed to delete directory"
                    )
                )
            )
            ->once();

        $image->delete();

        $assertion->verify();
    }

    public function testSave_createsABlurImage()
    {
        $user = User::factory()->create();

        $fakeFile = UploadedFile::fake()->image(
            "filename.png",
            /*width=*/ 40,
            /*height=*/ 50
        );
        Storage::fake();
        Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.png",
            $fakeFile->getContent()
        );

        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.png",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
        ]);

        $image->updateFilePath();

        $uniqueDirName = $image->generateUniqueFolderName();
        self::assertEquals(
            "images/user/$user->id/avatar/$uniqueDirName",
            $image->path
        );

        self::assertFalse(
            Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/blur_avatar.png"
            )
        );

        self::assertNotNull($image->blur);

        $manager = new ImageManager(["driver" => "imagick"]);
        $imagickImage = $manager->make($image->blur);
        self::assertEquals("WEBP", $imagickImage->getCore()->getImageFormat());
        self::assertEquals(16, $imagickImage->getWidth()); // resized to 16 width
        self::assertEquals(20, $imagickImage->getHeight()); // kept proportions 4:5
    }

    public function testSave_doesntCreateANewBlurImageIfItExists()
    {
        $user = User::factory()->create();

        $fakeFile = UploadedFile::fake()->image("filename.png");
        Storage::fake();
        Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.png",
            $fakeFile->getContent()
        );

        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.png",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
            "blur" => "some blurred image data",
        ]);

        $image->save();
        $image->refresh();

        self::assertEquals("some blurred image data", $image->blur);
    }
}
