<?php

namespace Tests\Unit\Models;

use App\Enums\LoanableTypes;
use App\Enums\LoanStatus;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\Subscription;
use App\Models\User;
use App\Services\GeocoderService;
use Carbon\Carbon;
use Geocoder\Location;
use Geocoder\Model\Address;
use Geocoder\Model\AdminLevelCollection;
use Geocoder\Model\Coordinates;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Stripe;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class UserTest extends TestCase
{
    private const FAKE_POSTAL_CODE = "H0H0H0";
    private const FAKE_ADDRESS = "1 place du pole nord, Canada H0H0H0";
    private const FAKE_FORMATTED_ADDRESS = "1 pl. du pole nord, Canada H0H0H0";
    private readonly Coordinates $fake_coordinates;
    private readonly Location $fake_location;

    public function setUp(): void
    {
        parent::setUp();

        $this->fake_coordinates = new Coordinates(1, 2);
        $this->fake_location = new Address(
            providedBy: "me",
            adminLevels: new AdminLevelCollection(),
            coordinates: $this->fake_coordinates,
            postalCode: self::FAKE_POSTAL_CODE
        );
    }

    public function testaddToBalance()
    {
        $user = User::factory()->create();

        $this->assertEquals(0, $user->balance);

        $user->addToBalance(10.1);
        $this->assertEquals(10.1, $user->balance);

        $user->addToBalance(-2.8);
        $this->assertEquals(7.3, $user->balance);
    }

    public function testAddToBalanceBelowZero()
    {
        $user = User::factory()->create([
            "balance" => 1,
        ]);

        $this->assertEquals(1, $user->balance);
        $user->addToBalance(-1);

        $this->assertEquals(0, $user->balance);

        $user->balance = 1;
        $user->save();

        $this->assertEquals(1, $user->balance);

        // If the balance is not sufficient, abort
        $this->expectException(HttpException::class);
        $user->addToBalance(-1.01);
        $this->assertEquals(0, 1); // Raised above
    }

    public function testUserStripeCustomerMethod()
    {
        $user = User::factory()->create();

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->with($user);

        $user->getStripeCustomer();
    }

    public function testUpdateEmailSuccess()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);

        $data = [
            "email" => "test_changed@locomotion.app",
            "password" => "locomotion",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );
        $json = $response->json();

        $response->assertStatus(200);
        $this->assertEquals(
            "test_changed@locomotion.app",
            Arr::get($json, "email")
        );
    }

    public function testUpdateEmailError()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);

        $data = [
            "email" => "test_changed@locomotion.app",
            "password" => "wrongpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );
        $json = $response->json();

        $response->assertStatus(401);
        $this->assertEquals("test@locomotion.app", array_get($json, "email"));
    }

    public function testUpdateEmailExistingEmail()
    {
        $newUser = $this->createTestUser();

        $otherUser = User::factory()->create([
            "email" => "used@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => null,
        ]);

        $this->actingAs($newUser);

        $this->assertEquals("test@locomotion.app", $newUser->email);
        $this->assertEquals("used@locomotion.app", $otherUser->email);

        $data = [
            "email" => "used@locomotion.app",
            "password" => "locomotion",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/email",
            $data
        );

        $response->assertStatus(422);
    }

    public function testUpdatePasswordSuccess()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "locomotion",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    public function testUpdatePasswordError()
    {
        $newUser = $this->createTestUser();
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "wrongpassword",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(401);
        $this->assertTrue(Hash::check("locomotion", $password));
        $this->assertFalse(Hash::check("newpassword", $password));
    }

    public function testUserAvailableLoanableTypes()
    {
        $carOnlyCommunity = Community::factory()->create();
        $carOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Car->getTypeDetails());

        $bikeOnlyCommunity = Community::factory()->create();
        $bikeOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Bike->getTypeDetails());

        $user = User::factory()->create();
        $user->communities()->attach($carOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($bikeOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(
            ["bike", "car"],
            array_sort_recursive($user->available_loanable_types)
        );
    }

    public function testUserAvailableLoanableTypes_doesntIncludeUnapprovedCommunities()
    {
        $carOnlyCommunity = Community::factory()->create();
        $carOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Car->getTypeDetails());

        $bikeOnlyCommunity = Community::factory()->create();
        $bikeOnlyCommunity
            ->allowedLoanableTypes()
            ->sync(LoanableTypes::Bike->getTypeDetails());

        $user = User::factory()->create();
        // Not approved in carCommunity
        $user->communities()->attach($carOnlyCommunity);
        $user->communities()->attach($bikeOnlyCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(["bike"], $user->available_loanable_types);
    }

    public function testUserAvailableLoanableTypes_doesntDuplicateTypes()
    {
        $carBikeCommunity = Community::factory()->create();
        $carBikeCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Car->getTypeDetails()->id,
                LoanableTypes::Bike->getTypeDetails()->id,
            ]);

        $carTrailerCommunity = Community::factory()->create();
        $carTrailerCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Car->getTypeDetails()->id,
                LoanableTypes::Trailer->getTypeDetails()->id,
            ]);

        $bikeTrailerCommunity = Community::factory()->create();
        $bikeTrailerCommunity
            ->allowedLoanableTypes()
            ->sync([
                LoanableTypes::Bike->getTypeDetails()->id,
                LoanableTypes::Trailer->getTypeDetails()->id,
            ]);

        $user = User::factory()->create();
        $user->communities()->attach($carBikeCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($carTrailerCommunity, [
            "approved_at" => Carbon::now(),
        ]);
        $user->communities()->attach($bikeTrailerCommunity, [
            "approved_at" => Carbon::now(),
        ]);

        $user->refresh();

        self::assertEquals(
            ["bike", "car", "trailer"],
            array_sort_recursive($user->available_loanable_types)
        );
    }

    public function testUpdatePassword_AdminCanChangeOtherUsersPassword()
    {
        $newUser = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => "admin",
        ]);
        // Don't act as user.

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    public function testUpdatePassword_AdminMustEnterOwnPassword()
    {
        $newUser = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => "admin",
        ]);
        // Do act as user.
        $this->actingAs($newUser);

        $this->assertTrue(Hash::check("locomotion", $newUser->password));

        $data = [
            "current" => "wrongpassword",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(401);
        $this->assertTrue(Hash::check("locomotion", $password));
        $this->assertFalse(Hash::check("newpassword", $password));

        $data = [
            "current" => "locomotion",
            "new" => "newpassword",
        ];

        $response = $this->json(
            "POST",
            "/api/v1/users/$newUser->id/password",
            $data
        );
        $password = User::find($newUser->id)->password;

        $response->assertStatus(200);
        $this->assertTrue(Hash::check("newpassword", $password));
        $this->assertFalse(Hash::check("locomotion", $password));
    }

    private function createTestUser()
    {
        $user = User::factory()->create([
            "email" => "test@locomotion.app",
            "password" => Hash::make("locomotion"),
            "role" => null,
        ]);

        return $user;
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        User::factory()
            ->withCommunity()
            ->create();
        User::factory()
            ->withCommunity()
            ->create();

        // 3, including $this->user
        self::assertCount(3, User::for("admin", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someUser = User::factory()
            ->withCommunity()
            ->create();
        User::factory()
            ->withCommunity()
            ->create();

        $communityAdmin = User::factory()
            ->hasAttached($someUser->main_community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        // 2, including $communityAdmin
        self::assertCount(2, User::for("admin", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someUser = User::factory()
            ->withCommunity()
            ->create();
        $otherUser = User::factory()
            ->withCommunity()
            ->create();

        $regularUser = User::factory()
            ->hasAttached($someUser->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherUser->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, User::for("admin", $regularUser)->get());
    }

    public function testDeleteUser_removesConnectionTokens()
    {
        $user = User::factory()->create([
            "email" => "soutien@locomotion.app",
            "password" => Hash::make("locomotion"),
        ]);

        $data = [
            "email" => "soutien@locomotion.app",
            "password" => "locomotion",
        ];
        $response = $this->json("POST", "/api/v1/auth/login", $data);
        $user->refresh();
        self::assertEquals(1, $user->tokens->count());
        self::assertFalse($user->tokens[0]->revoked);

        $user->delete();
        $user->refresh();
        self::assertTrue($user->tokens[0]->revoked);

        $this->artisan("passport:purge");
        $user->refresh();
        self::assertEmpty($user->tokens);
    }

    public function testDeleteUser_deletesLoanables()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $loanable = Loanable::factory()
            ->withOwner($user)
            ->create();

        self::assertFalse($loanable->trashed());

        $user->delete();
        $loanable->refresh();

        self::assertTrue($loanable->trashed());
    }

    public function testDeleteUser_cancelsLoans()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();

        $borrower = Borrower::factory()->create(["user_id" => $user]);

        $loan = Loan::factory()
            ->requested()
            ->create([
                "loanable_id" => Loanable::factory()->withOwner(),
                "borrower_user_id" => $user->id,
            ]);

        self::assertEquals(LoanStatus::Requested, $loan->status);

        $user->delete();
        $loan->refresh();

        self::assertEquals(LoanStatus::Canceled, $loan->status);
    }

    public function testDeleteUsers_removesPrivileges()
    {
        $user = User::factory()
            ->adminOfCommunity()
            ->create([
                "role" => "admin",
            ]);

        self::assertEquals("admin", $user->role);
        self::assertEquals("admin", $user->communities[0]->pivot->role);

        $user->delete();
        $user->refresh();

        self::assertNull($user->role);
        self::assertNull($user->communities[0]->pivot->role);
    }

    public function testDeleteUsers_suspendsFormActiveCommunities()
    {
        $approvedCommunity = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $unapprovedCommunity = Community::factory()->create([
            "name" => "unapproved_community",
        ]);
        $suspendedCommunity = Community::factory()->create([
            "name" => "suspended_community",
        ]);
        $suspensionTime = Carbon::now()->subHour();
        $user = User::factory()
            ->withCommunity($approvedCommunity)
            ->hasAttached($unapprovedCommunity)
            ->hasAttached($suspendedCommunity, [
                "approved_at" => Carbon::now()->subDay(),
                "suspended_at" => $suspensionTime,
            ])
            ->create();

        $user->delete();
        $user->refresh();

        self::assertCount(3, $user->communities);

        $userApprovedCommunity = $user->communities->first(
            fn($c) => $c->name === "approved_community"
        );
        self::assertNotNull($userApprovedCommunity->pivot->suspended_at);

        $userUnapprovedCommunity = $user->communities->first(
            fn($c) => $c->name === "unapproved_community"
        );
        self::assertNull($userUnapprovedCommunity->pivot->suspended_at);
        self::assertNull($userUnapprovedCommunity->pivot->suspended_at);

        $userSuspendedCommunity = $user->communities->first(
            fn($c) => $c->name === "suspended_community"
        );
        self::assertEquals(
            $suspensionTime->format("Y-m-d H:i:s"),
            (new Carbon($userSuspendedCommunity->pivot->suspended_at))->format(
                "Y-m-d H:i:s"
            )
        );
    }

    public function testUpdateAddressAndRelocateCommunity_whenAddressNotFound_thenAbort(): void
    {
        GeocoderService::shouldReceive("geocode")
            ->with(self::FAKE_ADDRESS)
            ->andReturn(null)
            ->once();
        $user = User::factory()->create([
            "address" => self::FAKE_ADDRESS,
        ]);

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage(
            "The provided user address was not found."
        );

        $user->updateAddressAndRelocateCommunity();
    }

    private function setupFakeGeocoderWithCommunity(?Community $community): void
    {
        GeocoderService::shouldReceive("geocode")
            ->with(self::FAKE_ADDRESS)
            ->andReturn($this->fake_location)
            ->once();
        GeocoderService::shouldReceive("formatAddressToText")
            ->with($this->fake_location)
            ->andReturn(self::FAKE_FORMATTED_ADDRESS)
            ->once();
        GeocoderService::shouldReceive("findCommunityFromCoordinates")
            ->with(1, 2)
            ->andReturn($community)
            ->once();
    }

    public function testUpdateAddressAndRelocateCommunity_whenStayingNotCovered_thenDoNotAssignCommunity(): void
    {
        $this->setupFakeGeocoderWithCommunity(null);
        $user = User::factory()->create([
            "address" => self::FAKE_ADDRESS,
        ]);

        $user->updateAddressAndRelocateCommunity();

        self::assertEquals($this->fake_coordinates, $user->address_position);
        self::assertEquals(self::FAKE_FORMATTED_ADDRESS, $user->address);
        self::assertEquals(self::FAKE_POSTAL_CODE, $user->postal_code);

        $user->refresh();
        self::assertNull($user->getMainCommunityAttribute());
    }

    public function testUpdateAddressAndRelocateCommunity_whenBecomingCovered_thenAssignCommunity(): void
    {
        $approvedCommunity = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $this->setupFakeGeocoderWithCommunity($approvedCommunity);
        $user = User::factory()->create([
            "address" => self::FAKE_ADDRESS,
        ]);

        $user->updateAddressAndRelocateCommunity();

        self::assertEquals($this->fake_coordinates, $user->address_position);
        self::assertEquals(self::FAKE_FORMATTED_ADDRESS, $user->address);
        self::assertEquals(self::FAKE_POSTAL_CODE, $user->postal_code);

        $user->refresh();
        self::assertEquals($approvedCommunity->id, $user->main_community->id);
    }

    public function testUpdateAddressAndRelocateCommunity_whenBecomingUncovered_thenDeAssignCommunity(): void
    {
        $approvedCommunity = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $this->setupFakeGeocoderWithCommunity(null);
        $user = User::factory()
            ->hasAttached($approvedCommunity, [
                "approved_at" => Carbon::now(),
                "join_method" => "geo",
            ])
            ->create([
                "address" => self::FAKE_ADDRESS,
            ]);

        $user->updateAddressAndRelocateCommunity();

        self::assertEquals($this->fake_coordinates, $user->address_position);
        self::assertEquals(self::FAKE_FORMATTED_ADDRESS, $user->address);
        self::assertEquals(self::FAKE_POSTAL_CODE, $user->postal_code);

        $user->refresh();
        self::assertNull($user->getMainCommunityAttribute());
    }

    public function testUpdateAddressAndRelocateCommunity_whenStayingCovered_thenSwitchCommunity(): void
    {
        $approvedCommunity1 = Community::factory()->create([
            "name" => "approved_community1",
        ]);
        $approvedCommunity2 = Community::factory()->create([
            "name" => "approved_community2",
        ]);
        $this->setupFakeGeocoderWithCommunity($approvedCommunity2);
        $user = User::factory()
            ->hasAttached($approvedCommunity1, [
                "approved_at" => Carbon::now(),
                "join_method" => "geo",
            ])
            ->create([
                "address" => self::FAKE_ADDRESS,
            ]);

        $user->updateAddressAndRelocateCommunity();

        self::assertEquals($this->fake_coordinates, $user->address_position);
        self::assertEquals(self::FAKE_FORMATTED_ADDRESS, $user->address);
        self::assertEquals(self::FAKE_POSTAL_CODE, $user->postal_code);
        $user->refresh();
        self::assertEquals($approvedCommunity2->id, $user->main_community->id);
    }

    public function testUpdateAddressAndRelocateCommunity_whenStayingInSameCommunity_thenOnlyAddressIsRefreshed(): void
    {
        $approvedCommunity = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $this->setupFakeGeocoderWithCommunity($approvedCommunity);
        $user = User::factory()
            ->withCommunity($approvedCommunity)
            ->create([
                "address" => self::FAKE_ADDRESS,
            ]);

        $user->updateAddressAndRelocateCommunity();

        self::assertEquals($this->fake_coordinates, $user->address_position);
        self::assertEquals(self::FAKE_FORMATTED_ADDRESS, $user->address);
        self::assertEquals(self::FAKE_POSTAL_CODE, $user->postal_code);
        $user->refresh();
        self::assertEquals($approvedCommunity->id, $user->main_community->id);
    }

    public function testUpdateAddressAndRelocateCommunity_setsJoinMethodToGeo(): void
    {
        $community = Community::factory()->create([
            "name" => "approved_community",
        ]);
        $this->setupFakeGeocoderWithCommunity($community);
        $user = User::factory()->create([
            "address" => self::FAKE_ADDRESS,
        ]);

        $user->updateAddressAndRelocateCommunity();
        $user->refresh();

        self::assertEquals($community->id, $user->main_community->id);
        self::assertEquals("geo", $user->main_community->pivot->join_method);
    }

    public function testUserHasSubscriptionsFor()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $communityUser = CommunityUser::factory([
            "user_id" => $user,
            "community_id" => $community,
            "approved_at" => Carbon::now(),
        ])->create();

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->subMonth(),
                "end_date" => Carbon::now()->addMonth(),
                "type" => "paid",
            ]);
        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->subMonth(),
                "end_date" => Carbon::now()->subDay(),
                "type" => "paid",
            ]);
        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->create([
                "community_user_id" => $communityUser,
                "start_date" => Carbon::now()->addDay(),
                "end_date" => Carbon::now()->addMonth(),
                "type" => "paid",
            ]);

        $user->load("subscriptions");

        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::CarSmall,
                $community->id
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::CarLargeElectric,
                $community->id
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::CarSmallElectric,
                $community->id
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::CarLarge,
                $community->id
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::Trailer,
                $community->id
            )
        );

        Subscription::factory()->create([
            "community_user_id" => $communityUser,
            "start_date" => Carbon::now()->subDay(),
            "end_date" => Carbon::now()->addMonth(),
            "type" => "granted",
        ]);

        $user->load("subscriptions");

        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::Trailer,
                $community->id
            )
        );
        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeRegular,
                $community->id
            )
        );
        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeCargoElectric,
                $community->id
            )
        );
        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeElectric,
                $community->id
            )
        );
        self::assertTrue(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeCargoRegular,
                $community->id
            )
        );

        $inAMonth = Carbon::now()
            ->addMonth()
            ->addMinute();

        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::Trailer,
                $community->id,
                $inAMonth
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeRegular,
                $community->id,
                $inAMonth
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeCargoElectric,
                $community->id,
                $inAMonth
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeElectric,
                $community->id,
                $inAMonth
            )
        );
        self::assertFalse(
            $user->hasActiveSubscriptionFor(
                PricingLoanableTypeValues::BikeCargoRegular,
                $community->id,
                $inAMonth
            )
        );
    }
}
