<?php

namespace Tests\Unit\Models\Policies;

use App\Enums\LoanableUserRoles;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Tests\TestCase;

class LoanPolicyTest extends TestCase
{
    private Community $community;
    private User $communityAdmin;
    private User $borrowerUser;
    private User $ownerUser;
    private User $coownerUser;
    private Loanable $loanable;
    private User $admin;
    private User $stranger;

    public function setUp(): void
    {
        parent::setUp();
        // Reduce mail log spam
        \Mail::fake();

        $this->community = Community::factory()
            ->withKMDollarsPricing()
            ->create();
        $this->communityAdmin = User::factory()->create();
        $this->communityAdmin->communities()->attach($this->community->id, [
            "approved_at" => new DateTime(),
            "role" => "admin",
        ]);

        $this->borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 10,
            ]);
        $this->borrowerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);

        $this->ownerUser = User::factory()->create();
        $this->ownerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);

        $this->coownerUser = User::factory()->create();
        $this->coownerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);

        $this->loanable = Loanable::factory()
            ->withOwner($this->ownerUser)
            ->withCar()
            ->create();

        $userRole = (new LoanableUserRole())->fill([
            "ressource_id" => $this->loanable->id,
            "ressource_type" => "loanable",
            "user_id" => $this->coownerUser->id,
            "role" => LoanableUserRoles::Coowner,
            "granted_by_user_id" => $this->user,
        ]);
        $this->loanable->userRoles()->save($userRole);

        $this->admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->stranger = User::factory()->create();
    }

    public function testCanView()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
                "borrower_user_id" => $this->borrowerUser->id,
            ]);

        self::assertTrue($this->borrowerUser->can("view", $loan));
        self::assertTrue($this->ownerUser->can("view", $loan));
        self::assertTrue($this->coownerUser->can("view", $loan));
        self::assertTrue($this->admin->can("view", $loan));
        self::assertTrue($this->communityAdmin->can("view", $loan));

        self::assertFalse($this->stranger->can("view", $loan));
    }

    public function testCanViewInstructions()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
                "borrower_user_id" => $this->borrowerUser->id,
            ]);

        self::assertTrue($this->ownerUser->can("viewInstructions", $loan));
        self::assertTrue($this->coownerUser->can("viewInstructions", $loan));
        self::assertTrue($this->admin->can("viewInstructions", $loan));
        self::assertTrue($this->communityAdmin->can("viewInstructions", $loan));
        self::assertTrue($this->borrowerUser->can("viewInstructions", $loan));

        self::assertFalse($this->stranger->can("viewInstructions", $loan));
    }

    public function testCanAccept()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->ownerUser->can("accept", $loan));
        self::assertTrue($this->coownerUser->can("accept", $loan));
        self::assertTrue($this->admin->can("accept", $loan));
        self::assertTrue($this->communityAdmin->can("accept", $loan));

        self::assertFalse($this->borrowerUser->can("accept", $loan));
        self::assertFalse($this->stranger->can("accept", $loan));
    }

    public function testCannotAcceptIfAccepted()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->ownerUser->can("accept", $loan));
        self::assertFalse($this->coownerUser->can("accept", $loan));
        self::assertFalse($this->admin->can("accept", $loan));
    }

    public function testCanReject()
    {
        $loan = Loan::factory()
            ->requested()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->ownerUser->can("reject", $loan));
        self::assertTrue($this->coownerUser->can("reject", $loan));
        self::assertTrue($this->admin->can("reject", $loan));
        self::assertTrue($this->communityAdmin->can("reject", $loan));

        self::assertFalse($this->borrowerUser->can("reject", $loan));
        self::assertFalse($this->stranger->can("reject", $loan));
    }

    public function testCannotRejectIfAccepted()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->ownerUser->can("reject", $loan));
        self::assertFalse($this->coownerUser->can("reject", $loan));
        self::assertFalse($this->admin->can("reject", $loan));
    }

    public function testCannotCancelIfCanceled()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create();

        $loan->setLoanStatusCanceled();

        self::assertFalse($this->admin->can("cancel", $loan));
    }

    public function testCannotCancelIfPaid()
    {
        $loan = Loan::factory()
            ->paid()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertFalse($this->admin->can("cancel", $loan));
    }

    public function testCannotCancelIfOngoingAndNotFree()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertFalse($this->borrowerUser->can("cancel", $loan));
    }

    public function testCannotCancelIfStranger()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create();

        self::assertFalse($this->stranger->can("cancel", $loan));
    }

    public function testCanCancelIfFree()
    {
        $this->community
            ->pricings()
            ->where("rule", "!=", 0)
            ->update(["rule" => 0]);

        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelInFuture()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->addMinutes(30),
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelWhenNotOngoing()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelIfAdmin()
    {
        $loan = Loan::factory()
            ->ongoing()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertTrue($this->admin->can("cancel", $loan));
        self::assertTrue($this->communityAdmin->can("cancel", $loan));
    }

    public function testCanResumeIfAdmin()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();

        self::assertTrue($this->admin->can("resume", $loan));
        self::assertTrue($this->communityAdmin->can("resume", $loan));

        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfNotCanceled()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfOwnerArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();
        $loan->loanable->ownerUser->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfBorrowerArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();
        $loan->borrowerUser->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfLoanableArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();
        $loan->loanable->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCanDeclareExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("requestExtension", $loan));
        self::assertTrue($this->communityAdmin->can("requestExtension", $loan));
        self::assertTrue($this->borrowerUser->can("requestExtension", $loan));
        self::assertTrue($this->ownerUser->can("requestExtension", $loan));
        self::assertTrue($this->coownerUser->can("requestExtension", $loan));

        self::assertFalse($this->stranger->can("requestExtension", $loan));
    }

    public function testCannotDeclareExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();

        self::assertFalse($this->admin->can("requestExtension", $loan));
        self::assertFalse(
            $this->communityAdmin->can("requestExtension", $loan)
        );
        self::assertFalse($this->borrowerUser->can("requestExtension", $loan));
        self::assertFalse($this->ownerUser->can("requestExtension", $loan));
        self::assertFalse($this->coownerUser->can("requestExtension", $loan));
        self::assertFalse($this->stranger->can("requestExtension", $loan));
    }

    public function testCanAcceptExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("acceptExtension", $loan));
        self::assertTrue($this->ownerUser->can("acceptExtension", $loan));
        self::assertTrue($this->coownerUser->can("acceptExtension", $loan));
        self::assertTrue($this->communityAdmin->can("acceptExtension", $loan));

        self::assertFalse($this->borrowerUser->can("acceptExtension", $loan));
        self::assertFalse($this->stranger->can("acceptExtension", $loan));
    }

    public function testCannotAcceptExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();

        self::assertFalse($this->admin->can("acceptExtension", $loan));
        self::assertFalse($this->communityAdmin->can("acceptExtension", $loan));
        self::assertFalse($this->borrowerUser->can("acceptExtension", $loan));
        self::assertFalse($this->ownerUser->can("acceptExtension", $loan));
        self::assertFalse($this->coownerUser->can("acceptExtension", $loan));
        self::assertFalse($this->stranger->can("acceptExtension", $loan));
    }

    public function testCanRejectExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("rejectExtension", $loan));
        self::assertTrue($this->ownerUser->can("rejectExtension", $loan));
        self::assertTrue($this->coownerUser->can("rejectExtension", $loan));
        self::assertTrue($this->communityAdmin->can("rejectExtension", $loan));

        self::assertFalse($this->borrowerUser->can("rejectExtension", $loan));
        self::assertFalse($this->stranger->can("rejectExtension", $loan));
    }

    public function testCannotRejectExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->setLoanStatusCanceled();

        self::assertFalse($this->admin->can("rejectExtension", $loan));
        self::assertFalse($this->communityAdmin->can("rejectExtension", $loan));
        self::assertFalse($this->borrowerUser->can("rejectExtension", $loan));
        self::assertFalse($this->ownerUser->can("rejectExtension", $loan));
        self::assertFalse($this->coownerUser->can("rejectExtension", $loan));
        self::assertFalse($this->stranger->can("rejectExtension", $loan));
    }

    public function testCanCancelExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("cancelExtension", $loan));
        self::assertTrue($this->communityAdmin->can("cancelExtension", $loan));
        self::assertTrue($this->borrowerUser->can("cancelExtension", $loan));

        self::assertFalse($this->ownerUser->can("cancelExtension", $loan));
        self::assertFalse($this->coownerUser->can("cancelExtension", $loan));
        self::assertFalse($this->stranger->can("cancelExtension", $loan));
    }

    public function testCanUpdateLoanInfo()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("updateLoanInfo", $loan));
        self::assertTrue($this->communityAdmin->can("updateLoanInfo", $loan));
        self::assertTrue($this->ownerUser->can("updateLoanInfo", $loan));
        self::assertTrue($this->coownerUser->can("updateLoanInfo", $loan));
        self::assertTrue($this->borrowerUser->can("updateLoanInfo", $loan));

        self::assertFalse($this->stranger->can("updateLoanInfo", $loan));
    }

    public function testCannotUpdateLoanInfoIfLoanNotOngoing()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $loan->setLoanStatusCanceled();

        self::assertFalse($this->admin->can("updateLoanInfo", $loan));
        self::assertFalse($this->communityAdmin->can("updateLoanInfo", $loan));
        self::assertFalse($this->ownerUser->can("updateLoanInfo", $loan));
        self::assertFalse($this->coownerUser->can("updateLoanInfo", $loan));
        self::assertFalse($this->borrowerUser->can("updateLoanInfo", $loan));
        self::assertFalse($this->stranger->can("updateLoanInfo", $loan));
    }

    public function testCanValidateLoanInfo()
    {
        $loan = Loan::factory()
            ->ended()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("validateLoanInfo", $loan));
        self::assertTrue($this->ownerUser->can("validateLoanInfo", $loan));
        self::assertTrue($this->coownerUser->can("validateLoanInfo", $loan));
        self::assertTrue($this->borrowerUser->can("validateLoanInfo", $loan));

        self::assertFalse(
            $this->communityAdmin->can("validateLoanInfo", $loan)
        );
        self::assertFalse($this->stranger->can("validateLoanInfo", $loan));
    }

    public function testCanPrepay()
    {
        $loan = Loan::factory()
            ->accepted()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $this->borrowerUser->balance = 100000;
        $this->borrowerUser->save();
        $loan->refresh();

        self::assertTrue($this->admin->can("prepay", $loan));
        self::assertTrue($this->communityAdmin->can("prepay", $loan));
        self::assertTrue($this->borrowerUser->can("prepay", $loan));

        self::assertFalse($this->ownerUser->can("prepay", $loan));
        self::assertFalse($this->coownerUser->can("prepay", $loan));
        self::assertFalse($this->stranger->can("prepay", $loan));
    }

    public function testCannotPrepayIfNotOngoing()
    {
        $loan = Loan::factory()
            ->confirmed()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("prepay", $loan));
        self::assertFalse($this->communityAdmin->can("prepay", $loan));
        self::assertFalse($this->borrowerUser->can("prepay", $loan));
        self::assertFalse($this->ownerUser->can("prepay", $loan));
        self::assertFalse($this->coownerUser->can("prepay", $loan));
        self::assertFalse($this->stranger->can("prepay", $loan));
    }

    public function testCanPay()
    {
        $loan = Loan::factory()
            ->validated()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "owner_validated_at" => Carbon::now(),
                "borrower_validated_at" => Carbon::now(),
            ]);

        $this->borrowerUser->balance = 100000;
        $this->borrowerUser->save();
        $loan->refresh();

        self::assertTrue($this->admin->can("pay", $loan));
        self::assertTrue($this->communityAdmin->can("pay", $loan));
        self::assertTrue($this->borrowerUser->can("pay", $loan));
        self::assertTrue($this->ownerUser->can("pay", $loan));
        self::assertTrue($this->coownerUser->can("pay", $loan));

        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_user_id" => $this->borrowerUser->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertFalse($this->admin->can("pay", $loan));
        self::assertFalse($this->communityAdmin->can("pay", $loan));
        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotValidated()
    {
        $loan = Loan::factory()
            ->ended()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $this->borrowerUser->balance = 100000;
        $this->borrowerUser->save();
        $loan->refresh();

        self::assertTrue($this->admin->can("pay", $loan));
        self::assertTrue($this->communityAdmin->can("pay", $loan));

        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotEnoughFunds()
    {
        $loan = Loan::factory()
            ->validated()
            ->create([
                "community_id" => $this->community->id,
                "borrower_user_id" => $this->borrowerUser->id,
                "loanable_id" => $this->loanable->id,
                "owner_validated_at" => Carbon::now(),
                "borrower_validated_at" => Carbon::now(),
            ]);

        $this->borrowerUser->balance = 0;
        $this->borrowerUser->save();

        $loan->refresh();

        self::assertFalse($this->admin->can("pay", $loan));
        self::assertFalse($this->communityAdmin->can("pay", $loan));
        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }
}
