<?php

namespace Tests\Unit\Models;

use App\Calendar\Interval;
use App\Enums\CarPricingCategories;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Tests\TestCase;

class PricingTest extends TestCase
{
    private function loanToData(Loan $loan)
    {
        return [
            "start" => $loan->departure_at,
            "end" => $loan->actual_return_at,
            "loanable_age_for_insurance" => $loan->loanable_age_for_insurance,
            "days" => $loan->calendar_days,
        ];
    }

    public function testRuleEvaluationWithMandatoryVariables()
    {
        $pricing = new Pricing();

        $pricing->rule = '$KM * 10 + $MINUTES * 2';

        $this->assertEquals(16, $pricing->evaluateRule(1, 3));
        $this->assertEquals(0, $pricing->evaluateRule(0, 0));
        $this->assertEquals(10, $pricing->evaluateRule(1, 0));
    }

    public function testRuleEvaluationSkipsSyntaxErrors()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
\$KM * 10 + * + \$MINUTES * 2
12345
RULE;

        $this->assertEquals(12345, $pricing->evaluateRule(1, 3));
    }

    public function testRuleEvaluationReturnsNullIfNoAnswerBecauseOfSyntaxError()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
\$KM * 10 + * + \$MINUTES * 2
RULE;

        $this->assertEquals(null, $pricing->evaluateRule(1, 3));
    }

    public function testRuleEvaluationReturnsNullIfNoMatch()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
SI \$KM > 10 ALORS \$KM * 10 + \$MINUTES * 2
RULE;

        $this->assertEquals(null, $pricing->evaluateRule(1, 3));
    }

    public function testRuleEvaluationWithConditions()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
SI \$KM > 20 ALORS 1
SI \$KM > 10 ALORS 2
3
RULE;

        $this->assertEquals(1, $pricing->evaluateRule(21, 0));
        $this->assertEquals(2, $pricing->evaluateRule(11, 0));
        $this->assertEquals(3, $pricing->evaluateRule(1, 0));
    }

    public function testRuleEvaluationConditionsOrder()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
SI \$KM > 20 ALORS 1
SI \$KM > 10 ALORS 2
SI \$MINUTES > 20 ALORS 3
SI \$MINUTES > 10 ALORS 4
5
RULE;

        $this->assertEquals(1, $pricing->evaluateRule(21, 0));
        $this->assertEquals(2, $pricing->evaluateRule(11, 0));
        $this->assertEquals(3, $pricing->evaluateRule(0, 21));
        $this->assertEquals(4, $pricing->evaluateRule(0, 11));
        $this->assertEquals(5, $pricing->evaluateRule(0, 0));

        $this->assertEquals(2, $pricing->evaluateRule(11, 21));
    }

    public function testRuleEvaluationOnObjectVariable()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
SI \$OBJET.pricing_category == 'small' ALORS 10
\$OBJET.year_of_circulation
RULE;

        $car = Loanable::factory()
            ->withCar([
                "year_of_circulation" => 1235,
                "pricing_category" => CarPricingCategories::Small,
            ])
            ->create();
        $this->assertEquals(10, $pricing->evaluateRule(0, 0, $car));

        $car->details->pricing_category = CarPricingCategories::Large;
        $car->details->save();
        $this->assertEquals(1235, $pricing->evaluateRule(0, 0, $car));
    }

    public function testRuleEvalationOnLoanVariables()
    {
        $loan = Loan::factory()->create([
            "duration_in_minutes" => 12 * 60,
            "departure_at" => (new Carbon(
                "2020-01-01 16:00:00",
                "America/Toronto"
            ))->toISOString(),
        ]);

        $car = Loanable::factory()
            ->withCar([
                "pricing_category" => CarPricingCategories::Small,
            ])
            ->create();

        $pricing = new Pricing();
        $pricing->rule = <<<RULE
SI \$OBJET.pricing_category == 'small' && \$EMPRUNT.days > 1 ALORS 1
SI \$OBJET.pricing_category == 'small' ALORS 2
SI \$OBJET.pricing_category == 'large' && \$EMPRUNT.days > 1 ALORS 3
SI \$OBJET.pricing_category == 'large' ALORS 4
4321
RULE;

        $this->assertEquals(
            1,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $car->details->pricing_category = CarPricingCategories::Large;
        $car->details->save();
        $this->assertEquals(
            3,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $loan->duration_in_minutes = 60;
        $loan->save();

        $this->assertEquals(
            4,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $car->details->pricing_category = CarPricingCategories::Small;
        $car->details->save();
        $this->assertEquals(
            2,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );
    }

    public function testRuleEvaluationMethods()
    {
        $pricing = new Pricing();

        // MIN
        $pricing->rule = 'MIN($KM, 15)';
        $this->assertEquals(14, $pricing->evaluateRule(14, 0));
        $this->assertEquals(15, $pricing->evaluateRule(16, 0));

        $this->assertNull($pricing->evaluateRule(null, 0));
        $this->assertNull($pricing->evaluateRule("test", 0));

        // MAX
        $pricing->rule = 'MAX($KM, 15)';
        $this->assertEquals(15, $pricing->evaluateRule(14, 0));
        $this->assertEquals(16, $pricing->evaluateRule(16, 0));

        $this->assertNull($pricing->evaluateRule(null, 0));
        $this->assertNull($pricing->evaluateRule("test", 0));

        // PLANCHER
        $pricing->rule = 'PLANCHER($KM)';
        $this->assertEquals(1, $pricing->evaluateRule(1.589, 0));
        $this->assertEquals(2, $pricing->evaluateRule(2.122, 0));

        // PLAFOND
        $pricing->rule = 'PLAFOND($KM)';
        $this->assertEquals(2, $pricing->evaluateRule(1.589, 0));
        $this->assertEquals(3, $pricing->evaluateRule(2.122, 0));

        // ARRONDI
        $pricing->rule = 'ARRONDI($KM)';
        $this->assertEquals(2, $pricing->evaluateRule(1.589, 0));
        $this->assertEquals(2, $pricing->evaluateRule(2.122, 0));

        // DOLLARS
        $pricing->rule = 'DOLLARS($KM)';
        $this->assertEquals(1.59, $pricing->evaluateRule(1.589, 0));
        $this->assertEquals(2.12, $pricing->evaluateRule(2.122, 0));
    }

    public function testRuleEvaluationWithLogicalOperators()
    {
        $pricing = new Pricing();

        // ET
        $pricing->rule = 'SI $KM == 1 ET $MINUTES == 2 ALORS 1234';
        $this->assertEquals(1234, $pricing->evaluateRule(1, 2));
        $this->assertEquals(null, $pricing->evaluateRule(2, 2));

        // OU
        $pricing->rule = 'SI $KM == 1 OU $MINUTES == 2 ALORS 1234';
        $this->assertEquals(1234, $pricing->evaluateRule(1, 1));
        $this->assertEquals(1234, $pricing->evaluateRule(2, 2));
        $this->assertEquals(null, $pricing->evaluateRule(3, 3));
    }

    public function testRuleEvaluationWithArrayOperators()
    {
        $pricing = new Pricing();

        // DANS
        $pricing->rule = 'SI $KM DANS [1] ALORS 1234';
        $this->assertEquals(1234, $pricing->evaluateRule(1, 0));
        $this->assertEquals(null, $pricing->evaluateRule(2, 0));

        // PAS DANS
        $pricing->rule = 'SI $KM PAS DANS [1] ALORS 1234';
        $this->assertEquals(null, $pricing->evaluateRule(1, 0));
        $this->assertEquals(1234, $pricing->evaluateRule(2, 0));
    }

    public function testRuleEvaluationRanges()
    {
        $pricing = new Pricing();

        $pricing->rule = 'SI $KM DANS 1..3 ALORS 1234';
        $this->assertEquals(null, $pricing->evaluateRule(0, 0));
        $this->assertEquals(1234, $pricing->evaluateRule(1, 0));
        $this->assertEquals(1234, $pricing->evaluateRule(2, 0));
        $this->assertEquals(1234, $pricing->evaluateRule(3, 0));
        $this->assertEquals(null, $pricing->evaluateRule(4, 0));
    }

    public function testRuleEvaluationDateProperties()
    {
        $loan = new Loan();
        $date = new Carbon("2020-01-01 16:00:00");
        $loan->duration_in_minutes = 12 * 60;
        $loan->departure_at = $date;
        $loan->actual_return_at = new Carbon("2020-01-02 04:00:00");
        $loan->loanable = new Loanable();
        $loan->loanable->timezone = "UTC";

        $pricing = new Pricing();

        foreach (
            ["year", "month", "day", "hour", "minute", "day_of_year"]
            as $key
        ) {
            $pricing->rule = "\$EMPRUNT.start.$key";
            $camelKey = Str::camel($key);
            $this->assertEquals(
                $date->{$camelKey},
                $pricing->evaluateRule(0, 0, null, self::loanToData($loan))
            );
        }
    }

    public function testRuleEvaluationSkipsComments()
    {
        $pricing = new Pricing();

        $pricing->rule = <<<RULE
# This is a comment
12345
RULE;
        $this->assertEquals(12345, $pricing->evaluateRule(0, 0));

        $pricing->rule = <<<RULE
  # Some whitespace may preced comment.
12345
RULE;
        $this->assertEquals(12345, $pricing->evaluateRule(0, 0));
    }

    public function testRuleEvaluationSurcoutAssurance()
    {
        $loan = new Loan();
        $loan->duration_in_minutes = 4 * 60;

        $bike = Loanable::factory()
            ->withBike()
            ->create();
        $car = Loanable::factory()
            ->withCar()
            ->create();

        $loan->loanable = $car;

        $pricing = new Pricing();

        // Car 2015 and departure on 2021-08-31
        $car->details->year_of_circulation = 2015;
        $car->details->save();
        $loan->departure_at = new Carbon("2021-08-31 16:00:00");
        $loan->actual_return_at = new Carbon("2020-01-01 20:00:00");

        $pricing->rule = '$EMPRUNT.loanable_age_for_insurance';

        $this->assertEquals(
            5,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $pricing->rule = 'SI $SURCOUT_ASSURANCE ALORS 1';
        $this->assertEquals(
            1,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        // Car 2015 and departure on 2021-09-01
        $car->details->year_of_circulation = 2015;
        $car->details->save();
        $loan->departure_at = new Carbon("2021-09-01 16:00:00");

        $pricing->rule = '$EMPRUNT.loanable_age_for_insurance';
        $this->assertEquals(
            6,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $pricing->rule = 'SI $SURCOUT_ASSURANCE ALORS 1';
        $this->assertEquals(
            null,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        // Car 2018 and departure on 2020-03-01
        $car->details->year_of_circulation = 2018;
        $car->details->save();
        $loan->departure_at = new Carbon("2020-03-01 16:00:00");

        $pricing->rule = '$EMPRUNT.loanable_age_for_insurance';
        $this->assertEquals(
            1,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        $pricing->rule = 'SI $SURCOUT_ASSURANCE ALORS 1';
        $this->assertEquals(
            1,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        // Car 2010 and same departure
        $car->details->year_of_circulation = 2010;
        $car->details->save();

        $pricing->rule = 'SI $SURCOUT_ASSURANCE ALORS 1';
        $this->assertEquals(
            null,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );

        // Bike: not applicable
        $loan->loanable = $bike;

        $pricing->rule = 'SI NON $SURCOUT_ASSURANCE ALORS 2';
        $this->assertEquals(
            2,
            $pricing->evaluateRule(0, 0, $bike, self::loanToData($loan))
        );
    }

    public function testRuleEvaluation_DailyPremiumFromValueCategory()
    {
        $loan = new Loan();
        $loan->duration_in_minutes = 4 * 60;
        $loan->departure_at = new Carbon("2021-08-31 16:00:00");
        $loan->actual_return_at = new Carbon("2020-01-01 20:00:00");

        $car = Loanable::factory()
            ->withCar([
                "value_category" => "lte70k",
            ])
            ->create();

        $loan->loanable = $car;

        $pricing = new Pricing();

        $pricing->rule = '$PRIME_PAR_JOUR';
        $this->assertEquals(
            5,
            $pricing->evaluateRule(0, 0, $car, self::loanToData($loan))
        );
    }

    public function testScopeActiveOn()
    {
        $yestedayPricing = Pricing::factory()->create([
            "start_date" => Carbon::now()
                ->subDay()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()->format("Y-m-d"),
        ]);
        Pricing::factory()->create([
            "start_date" => Carbon::now()
                ->subWeek()
                ->format("Y-m-d"),
            "end_date" => Carbon::now()
                ->subDay()
                ->format("Y-m-d"),
        ]);
        $currentPricing = Pricing::factory()->create([
            "start_date" => Carbon::now()->format("Y-m-d"),
            "end_date" => null,
        ]);

        $pricings = Pricing::query()
            ->activeOn(Carbon::now()->subDay())
            ->get();
        self::assertCount(1, $pricings);
        self::assertEquals($yestedayPricing->id, $pricings->first()->id);

        $pricings = Pricing::query()
            ->activeOn(Carbon::now())
            ->get();
        self::assertCount(1, $pricings);
        self::assertEquals($currentPricing->id, $pricings->first()->id);
    }

    public function testScopeIntersects()
    {
        $firstPricing = Pricing::factory()->create([
            "start_date" => "2024-07-04",
            "end_date" => "2024-07-06",
        ]);
        $secondPricing = Pricing::factory()->create([
            "start_date" => "2024-07-06",
            "end_date" => "2024-07-08",
        ]);
        $thirdPricing = Pricing::factory()->create([
            "start_date" => "2024-07-08",
            "end_date" => null,
        ]);

        $pricings = Pricing::query()
            ->intersects(
                new Interval(
                    new Carbon(
                        "2024-07-03 12:30",
                        config("app.default_user_timezone")
                    ),
                    new Carbon(
                        "2024-07-05 12:30",
                        config("app.default_user_timezone")
                    )
                )
            )
            ->get();
        self::assertCount(1, $pricings);
        self::assertEquals($firstPricing->id, $pricings->first()->id);

        $pricings = Pricing::query()
            ->intersects(
                new Interval(
                    new Carbon(
                        "2024-07-03 12:30",
                        config("app.default_user_timezone")
                    ),
                    new Carbon(
                        "2024-07-08 12:30",
                        config("app.default_user_timezone")
                    )
                )
            )
            ->get();
        self::assertCount(3, $pricings);

        $pricings = Pricing::query()
            ->intersects(
                new Interval(
                    new Carbon(
                        "2024-07-03 12:30",
                        config("app.default_user_timezone")
                    ),
                    new Carbon(
                        "2024-07-06 00:00:00",
                        config("app.default_user_timezone")
                    )
                )
            )
            ->get();
        self::assertCount(1, $pricings);
        self::assertEquals($firstPricing->id, $pricings->first()->id);

        $pricings = Pricing::query()
            ->intersects(
                new Interval(
                    new Carbon(
                        "2024-07-03 12:30",
                        config("app.default_user_timezone")
                    ),
                    new Carbon(
                        "2024-07-06 00:30:00",
                        config("app.default_user_timezone")
                    )
                )
            )
            ->get();
        self::assertCount(2, $pricings);

        $pricings = Pricing::query()
            ->intersects(
                new Interval(
                    new Carbon(
                        "2024-07-06 00:00:00",
                        config("app.default_user_timezone")
                    ),
                    new Carbon(
                        "2024-07-06 12:30:00",
                        config("app.default_user_timezone")
                    )
                )
            )
            ->get();
        self::assertCount(1, $pricings);
        self::assertEquals($secondPricing->id, $pricings->first()->id);
    }
}
