<?php

namespace Tests\Unit\Models;

use App\Models\Payout;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class PayoutTest extends TestCase
{
    public function testBankingInfoUpdated_noPreviousPayouts()
    {
        $user = User::factory()->create([
            "bank_info_updated_at" => null,
        ]);
        $payout = Payout::factory()->create([
            "user_id" => $user,
        ]);

        self::assertFalse($payout->bank_info_updated_since_last_payout);

        $user->bank_account_number = 123;
        $user->save();
        $payout->refresh();

        self::assertTrue($payout->bank_info_updated_since_last_payout);
    }

    public function testBankingInfoUpdated_withPreviousPayouts()
    {
        $user = User::factory()->create([
            "bank_info_updated_at" => null,
        ]);
        Payout::factory()->create([
            "user_id" => $user,
            "created_at" => CarbonImmutable::now()->subWeek(),
        ]);
        $payout = Payout::factory()->create([
            "user_id" => $user,
            "created_at" => CarbonImmutable::now(),
        ]);

        self::assertFalse($payout->bank_info_updated_since_last_payout);

        self::setTestNow(CarbonImmutable::now()->subWeeks(2));
        $user->bank_account_number = 123;
        $user->save();
        self::setTestNow();

        $payout->refresh();
        self::assertFalse($payout->bank_info_updated_since_last_payout);

        self::setTestNow(CarbonImmutable::now()->subDay());
        $user->bank_transit_number = 456;
        $user->save();
        self::setTestNow();

        $payout->refresh();
        self::assertTrue($payout->bank_info_updated_since_last_payout);
    }
}
