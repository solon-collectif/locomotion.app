<?php

namespace Tests\Unit\Models;

use App\Enums\PricingLoanableTypeValues;
use App\Models\Subscription;
use Carbon\Carbon;
use Tests\TestCase;

class SubscriptionTest extends TestCase
{
    public function testActiveSubscriptions()
    {
        $ended = Subscription::factory()->create([
            "start_date" => Carbon::now()->subYear(),
            "end_date" => Carbon::now()->subDay(),
        ]);
        $ending = Subscription::factory()->create([
            "start_date" => Carbon::now()->subYear(),
            "end_date" => Carbon::now()->addSeconds(3),
        ]);
        $current = Subscription::factory()->create([
            "start_date" => Carbon::now()->subYear(),
            "end_date" => Carbon::now()->addWeek(),
        ]);
        $starting = Subscription::factory()->create([
            "start_date" => Carbon::now()->subSeconds(3),
            "end_date" => Carbon::now()->addYear(),
        ]);
        $future = Subscription::factory()->create([
            "start_date" => Carbon::now()->addDay(),
            "end_date" => Carbon::now()->addYear(),
        ]);

        $activeSubscriptions = Subscription::active()->get();
        $this->assertCount(3, $activeSubscriptions);
        $this->assertEqualsCanonicalizing(
            [$ending->id, $current->id, $starting->id],
            $activeSubscriptions->map(fn($s) => $s->id)->toArray()
        );
    }

    public function testSubscriptionLoanableTypes()
    {
        $subscription = Subscription::factory()->create();
        \DB::table("subscription_loanable_types")->insert([
            [
                "subscription_id" => $subscription->id,
                "pricing_loanable_type" => PricingLoanableTypeValues::CarSmall,
            ],
            [
                "subscription_id" => $subscription->id,
                "pricing_loanable_type" =>
                    PricingLoanableTypeValues::BikeElectric,
            ],
        ]);
        $subscription->refresh();

        self::assertEqualsCanonicalizing(
            [
                PricingLoanableTypeValues::CarSmall,
                PricingLoanableTypeValues::BikeElectric,
            ],
            $subscription->loanableTypes
        );
    }
}
