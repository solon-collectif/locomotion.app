<?php

use App\Http\Controllers\CommunityController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\GbfsController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\LibraryController;
use App\Http\Controllers\LoanableController;
use App\Http\Controllers\LoanableUserRolesController;
use App\Http\Controllers\MailingListIntegrationController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\PayoutController;
use App\Http\Controllers\PublishableReportController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPublishableReportController;

Route::prefix("v1")->group(function () {
    Route::get("/", "StaticController@blank");
    Route::get("/status", "StaticController@status");
    Route::get("/stats", "StaticController@stats");

    Route::post("auth/login", "AuthController@login");
    Route::post("auth/register", "AuthController@register");
    Route::post(
        "auth/token/refresh",
        "AuthController@refreshToken"
    )->middleware("auth:api");
    Route::post("auth/password/request", "AuthController@passwordRequest");
    Route::post("auth/password/reset", "AuthController@passwordReset");
    Route::get(
        "auth/password/mandate/{userId}",
        "AuthController@mandate"
    )->middleware("auth:api");

    Route::get(
        "/invitations/validate/{id}",
        "InvitationController@validateInvitation"
    )
        ->where("id", "[0-9]+")
        ->middleware("throttle:30,1");

    // This one needs to handle atomicity of requests per user, which requires it to handle its own
    // transactions
    Route::put("auth/user/claim", "UserController@claimBalance")->middleware(
        "auth:api"
    );

    Route::middleware(["auth:api", "transaction"])->group(function () {
        Route::prefix("auth")->group(function () {
            Route::get("user", "AuthController@retrieveUser");
            Route::put("user", "AuthController@updateUser");
            Route::get("user/balance", "AuthController@getUserBalance");
            Route::put("user/balance", "AuthController@addToUserBalance");
            Route::put("logout", "AuthController@logout");
        });

        Route::post("acceptConditions", [
            UserController::class,
            "acceptConditions",
        ]);

        Route::get("/communities", [CommunityController::class, "index"])->name(
            "communities.index"
        );
        Route::post("/communities", [
            CommunityController::class,
            "create",
        ])->name("communities.create");
        Route::get("/communities/{community}", [
            CommunityController::class,
            "view",
        ])
            ->name("communities.retrieve")
            ->where("community", "[0-9]+");
        Route::put("/communities/{community}", [
            CommunityController::class,
            "update",
        ])
            ->name("communities.update")
            ->where("community", "[0-9]+");
        Route::put("/communities/{community}/restore", [
            CommunityController::class,
            "restore",
        ])
            ->withTrashed()
            ->name("communities.restore")
            ->where("community", "[0-9]+");
        Route::delete("/communities/{community}", [
            CommunityController::class,
            "destroy",
        ])
            ->name("communities.destroy")
            ->where("community1", "[0-9]+");
        Route::options("/communities", [
            CommunityController::class,
            "template",
        ])->name("communities.template");

        Route::post("/files", [FileController::class, "create"])->name(
            "files.create"
        );
        Route::get("/files/{file}", [FileController::class, "view"])->name(
            "files.retrieve"
        );
        Route::post("/images", [ImageController::class, "create"])->name(
            "images.create"
        );
        Route::get("/images/{image}", [
            ImageController::class,
            "viewImage",
        ])->name("images.retrieve");

        Route::get("/payment_methods", [
            PaymentMethodController::class,
            "index",
        ])->name("payment_methods.index");
        Route::post("/payment_methods", [
            PaymentMethodController::class,
            "create",
        ])->name("payment_methods.create");
        Route::get("/payment_methods/{paymentMethod}", [
            PaymentMethodController::class,
            "view",
        ])->name("payment_methods.retrieve");
        Route::delete("/payment_methods/{paymentMethod}", [
            PaymentMethodController::class,
            "destroy",
        ])->name("payment_methods.destroy");
        Route::options("/payment_methods", [
            PaymentMethodController::class,
            "template",
        ])->name("payment_methods.template");

        Route::get("/invoices", [InvoiceController::class, "index"])->name(
            "invoices.index"
        );
        Route::post("/invoices", [InvoiceController::class, "create"])->name(
            "invoices.create"
        );
        Route::get("/invoices/{invoice}", [
            InvoiceController::class,
            "view",
        ])->name("invoices.retrieve");
        Route::options("/invoices", [
            InvoiceController::class,
            "template",
        ])->name("invoices.template");

        Route::get("invoiceitems", "InvoiceItemController@index")->name(
            "invoiceitems.index"
        );

        Route::post("invoice/estimate", "InvoiceController@estimate")
            ->where("id", "[0-9]+")
            ->name("invoice.estimate");

        Route::put("users/send/{type}", "UserController@sendEmail");
        Route::put(
            "users/add_to_community/{communityId}",
            "UserController@addAllToCommunity"
        );

        Route::get("/users", [UserController::class, "index"])->name(
            "users.index"
        );
        Route::post("/users", [UserController::class, "create"])->name(
            "users.create"
        );
        Route::get("/users/me", [UserController::class, "retrieveSelf"])->name(
            "users.retrieve_self"
        );
        Route::get("/users/{user}", [UserController::class, "view"])->name(
            "users.retrieve"
        );
        Route::put("/users/{user}", [UserController::class, "update"])->name(
            "users.update"
        );
        Route::put("/users/{user}/restore", [UserController::class, "restore"])
            ->withTrashed()
            ->name("users.restore");
        Route::delete("/users/{user}", [UserController::class, "destroy"])
            ->withTrashed()
            ->name("users.destroy");
        Route::options("/users", [UserController::class, "template"])->name(
            "users.template"
        );

        Route::post("users/{user}/email", "UserController@updateEmail");
        Route::post("users/{user}/password", "UserController@updatePassword");

        Route::put(
            "/users/{user}/borrower/submit",
            "UserController@submitBorrower"
        );
        Route::put(
            "/users/{user}/borrower/reset",
            "UserController@resetBorrowerStatus"
        );
        Route::put(
            "/users/{user}/borrower/approve",
            "UserController@approveBorrower"
        );
        Route::put(
            "/users/{user}/borrower/suspend",
            "UserController@suspendBorrower"
        );
        Route::delete(
            "/users/{user}/borrower/suspend",
            "UserController@unsuspendBorrower"
        );

        Route::get("/communityUsers", "CommunityUserController@index");
        Route::put(
            "/communityUsers/{communityUser}",
            "CommunityUserController@updateCommunityUser"
        );
        Route::put(
            "/communityUsers/{communityUser}/suspend",
            "CommunityUserController@suspend"
        );
        Route::put(
            "/communityUsers/{communityUser}/approve",
            "CommunityUserController@approve"
        );

        Route::put(
            "/communityUsers/{communityUser}/unsuspend",
            "CommunityUserController@unsuspend"
        );
        Route::put(
            "/communityUsers/{communityUser}/proofs",
            "CommunityUserController@updateProofs"
        );

        Route::get(
            "/communities/{community_id}/users/mailboxes",
            "CommunityUserController@exportMailboxes"
        );

        Route::post(
            "/communities/{community}/users",
            "CommunityController@addMember"
        );
        Route::delete(
            "/communities/{community}/users/{user}",
            "CommunityController@removeMember"
        );
        Route::put(
            "/communities/{community}/approve/sample",
            "CommunityController@sendApprovingSample"
        );

        Route::get("/incidents", "IncidentController@index");
        Route::post("/incidents", "IncidentController@create");
        Route::put(
            "/incidents/{incident}/complete",
            "IncidentController@complete"
        );
        Route::put("/incidents/{incident}/reopen", "IncidentController@reopen");
        Route::post("/incidents/{incident}/note", "IncidentController@addNote");
        Route::put(
            "/incidents/{incident}/note/{note}",
            "IncidentController@updateNote"
        );
        Route::delete(
            "/incidents/{incident}/note/{note}",
            "IncidentController@deleteNote"
        );
        Route::put(
            "/incidents/{incident}/block",
            "IncidentController@setBlockingUntil"
        );
        Route::put("/incidents/{incident}", "IncidentController@update");
        Route::put(
            "/incidents/{incident}/assignee",
            "IncidentController@setAssignee"
        );
        Route::put(
            "/incidents/{incident}/subscription",
            "IncidentController@changeSubscriptionLevel"
        );

        Route::post("/loans", "LoanController@create")->name("loans.create");
        Route::put("/loans/{loan}/dates", "LoanController@updateDates")
            ->name("loans.update.dates")
            ->where("loan", "[0-9]+");
        Route::put("/loans/{loan}/factors", "LoanController@updateFactors")
            ->name("loans.update.factors")
            ->where("loan", "[0-9]+");
        Route::put("/loans/{loan}/tip", "LoanController@updateTip")
            ->name("loans.update.tip")
            ->where("loan", "[0-9]+");
        Route::get("/loans", "LoanController@index")->name("loans.index");
        Route::get("/loans/{loan}", "LoanController@get")
            ->name("loans.get")
            ->where("loan", "[0-9]+");

        Route::put("/loans/{loan}/prepay", "LoanController@prepay")
            ->name("loans.prepay")
            ->where("loan", "[0-9]+");

        Route::put("/loans/{loan}/cancel", "LoanController@cancel");
        Route::put("/loans/{loan}/reject", "LoanController@reject");
        Route::put("/loans/{loan}/resume", "LoanController@resume");

        Route::post("/loans/{loan}/comment", "LoanController@comment")
            ->name("loans.comment.add")
            ->where("loan", "[0-9]+");
        Route::delete(
            "/loans/{loan}/comment/{comment}",
            "LoanController@deleteComment"
        )
            ->name("loans.comment.delete")
            ->where("loan", "[0-9]+")
            ->where("comment", "[0-9]+");

        Route::put("/loans/{loan}/extension", "LoanController@requestExtension")
            ->name("loans.requestExtension")
            ->where("loan", "[0-9]+");
        Route::put(
            "/loans/{loan}/extension/accept",
            "LoanController@acceptExtension"
        )
            ->name("loans.acceptExtension")
            ->where("loan", "[0-9]+");
        Route::put(
            "/loans/{loan}/extension/reject",
            "LoanController@rejectExtension"
        )
            ->name("loans.rejectExtension")
            ->where("loan", "[0-9]+");
        Route::put(
            "/loans/{loan}/extension/cancel",
            "LoanController@cancelExtension"
        );

        Route::put(
            "/loans/{loan}/notifications",
            "LoanController@changeNotificationLevel"
        );

        Route::put(
            "/loans/{loan}/notifications/seen",
            "LoanController@markNotificationsSeen"
        );

        Route::put(
            "/loans/{loan}/validate",
            "LoanController@validateInformation"
        );
        Route::put("/loans/{loan}/autovalidate", "LoanController@autoValidate");
        Route::put("/loans/{loan}/accept", "LoanController@accept");
        Route::put("/loans/{loan}/pay", "LoanController@pay");
        Route::get("/loans/{loan}/estimate", "LoanController@estimateChanges");
        Route::put("/loans/{loan}/return", "LoanController@earlyReturn");

        Route::put(
            "/pricings/{pricing}/evaluate",
            "PricingController@evaluate"
        )->name("pricings.evaluate");

        Route::get("/pricings", "PricingController@index");
        Route::put("/pricings/{pricing}", "PricingController@update")->name(
            "pricings.update"
        );
        Route::put("/pricings", "PricingController@create")->name(
            "pricings.create"
        );
        Route::delete("/pricings/{pricing}", "PricingController@destroy")->name(
            "pricings.destroy"
        );

        Route::get("/loanables", [LoanableController::class, "index"])->name(
            "loanables.index"
        );
        Route::post("/loanables", [LoanableController::class, "create"])->name(
            "loanables.create"
        );
        Route::get("/loanables/{loanable}", [LoanableController::class, "view"])
            ->name("loanables.retrieve")
            ->where("loanable", "[0-9]+");
        Route::put("/loanables/{loanable}", [
            LoanableController::class,
            "update",
        ])
            ->name("loanables.update")
            ->where("loanable", "[0-9]+");
        Route::put("/loanables/{loanable}/restore", [
            LoanableController::class,
            "restore",
        ])
            ->withTrashed()
            ->name("loanables.restore")
            ->where("loanable", "[0-9]+")
            ->where("loanable", "[0-9]+");
        Route::delete("/loanables/{loanable}", [
            LoanableController::class,
            "destroy",
        ])->name("loanables.destroy");
        Route::options("/loanables", [
            LoanableController::class,
            "template",
        ])->name("loanables.template");
        Route::get(
            "/loanables/{loanable}/test",
            "LoanableController@test"
        )->where("loanable", "[0-9]+");
        Route::get("/loanables/search", "LoanableController@search")->name(
            "loanables.search"
        );
        Route::put("/loanables/{loanable}/publish", [
            LoanableController::class,
            "publish",
        ])
            ->name("loanables.publish")
            ->where("loanable", "[0-9]+");

        Route::get(
            "/loanables/listByType",
            "LoanableController@listByType"
        )->name("loanables.list");

        Route::get(
            "/loanables/availability",
            "LoanableController@genericAvailability"
        );

        Route::get(
            "/loanables/{loanable}/availability",
            "LoanableController@availability"
        );

        Route::get(
            "/loanables/{loanable}/events",
            "LoanableController@events"
        )->where("loanable", "[0-9]+");

        Route::get(
            "/loanables/{loanable}/loans/unavailable",
            "LoanableController@loansDuringUnavailabilities"
        )->where("loanable", "[0-9]+");

        Route::get("/loanableTypes", "GetLoanableTypesController");

        Route::get("/loanables/roles/", [
            LoanableUserRolesController::class,
            "index",
        ]);

        Route::delete("/loanables/roles/{role}", [
            LoanableUserRolesController::class,
            "delete",
        ])->where("role", "[0-9]+");

        Route::put("/loanables/roles/{role}", [
            LoanableUserRolesController::class,
            "update",
        ])->where("role", "[0-9]+");

        Route::get("/libraries/{library}/roles", [
            LibraryController::class,
            "viewUserRoles",
        ]);

        Route::get("/loanables/{loanable}/roles", [
            LoanableController::class,
            "viewUserRoles",
        ]);
        Route::post("/libraries/{library}/roles", [
            LibraryController::class,
            "createUserRole",
        ]);
        Route::post("/loanables/{loanable}/roles", [
            LoanableController::class,
            "createUserRole",
        ]);

        Route::get("/libraries", "LibraryController@index");
        Route::get("/libraries/{library}", "LibraryController@show");
        Route::get("/libraries/{library}/events", "LibraryController@events");
        Route::post("/libraries", "LibraryController@create");
        Route::put("/libraries/{library}", "LibraryController@update");
        Route::delete("/libraries/{library}", "LibraryController@destroy");

        Route::post(
            "/libraries/{library}/loanables/{loanable}",
            "LibraryController@addLoanable"
        );
        Route::get(
            "/libraries/{library}/loanables",
            "LibraryController@viewLoanables"
        );

        Route::delete(
            "/libraries/{library}/loanables/{loanable}",
            "LibraryController@removeLoanable"
        );
        Route::get(
            "/libraries/{library}/communities",
            "LibraryController@viewCommunities"
        );

        Route::post(
            "/libraries/{library}/communities/{community}",
            "LibraryController@addCommunity"
        );
        Route::delete(
            "/libraries/{library}/communities/{community}",
            "LibraryController@removeCommunity"
        );

        Route::get("/loans/dashboard", "LoanController@dashboard");
        Route::get("/loanables/dashboard", "LoanableController@dashboard");

        Route::delete("/mailinglists/{mailingListIntegration}", [
            MailingListIntegrationController::class,
            "destroy",
        ]);

        Route::post("/mailinglists", [
            MailingListIntegrationController::class,
            "create",
        ]);
        Route::get("/mailinglists/lists", [
            MailingListIntegrationController::class,
            "getLists",
        ]);

        Route::get("/mailinglists", [
            MailingListIntegrationController::class,
            "index",
        ]);

        Route::get("/exports/", "ExportController@index");
        Route::delete("/exports/", "ExportController@destroyBatch");
        Route::put("/exports/cancel", "ExportController@cancelBatch");

        Route::get("/exports/{id}", "ExportController@view")->where(
            "id",
            "[0-9]+"
        );
        Route::delete("/exports/{id}", "ExportController@destroy")->where(
            "id",
            "[0-9]+"
        );
        Route::put("/exports/{id}/cancel", "ExportController@cancel")->where(
            "id",
            "[0-9]+"
        );

        Route::prefix("/invitations")->group(function () {
            Route::get("/", "InvitationController@index");
            Route::put("/", "InvitationController@create");
            Route::put("/batch", "InvitationController@createMany");
            Route::delete("/{id}", "InvitationController@destroy")->where(
                "id",
                "[0-9]+"
            );
            Route::post(
                "/accept/registration",
                "InvitationController@acceptOnRegistration"
            );
            Route::post(
                "/{id}/accept",
                "InvitationController@acceptForSingleCommunity"
            )->where("id", "[0-9]+");
            Route::post(
                "/{id}/refuse",
                "InvitationController@refuseSingle"
            )->where("id", "[0-9]+");
            Route::put("/sample", "InvitationController@sendSample");
        });

        Route::prefix("/subscriptions")->group(function () {
            Route::put("/grant", "SubscriptionController@grant");
            Route::put("/estimate", "SubscriptionController@estimate");
            Route::put("/pay", "SubscriptionController@pay");
            Route::get("/my-targets", "SubscriptionController@myTargets");
            Route::delete("/{subscription}", "SubscriptionController@destroy");
            Route::put(
                "/{subscription}/restore",
                "SubscriptionController@restore"
            );
        });

        Route::put("gbfs_datasets", [GbfsController::class, "create"]);

        Route::get("gbfs_datasets", [GbfsController::class, "index"]);

        Route::delete("gbfs_datasets/{id}", [GbfsController::class, "destroy"]);

        Route::put("gbfs_datasets/{gbfsDataset}", [
            GbfsController::class,
            "update",
        ]);

        Route::get("gbfs_datasets/{gbfsDataset}", [
            GbfsController::class,
            "view",
        ]);

        Route::put("gbfs_dataset_communities", [
            GbfsController::class,
            "addDatasetCommunity",
        ]);

        Route::get("gbfs_dataset_communities", [
            GbfsController::class,
            "listDatasetCommunities",
        ]);

        Route::delete("gbfs_dataset_communities/{id}", [
            GbfsController::class,
            "destroyDatasetCommunity",
        ]);

        Route::get("reports/balance/generate", [
            ReportController::class,
            "generateBalanceReport",
        ]);
        Route::get("reports/insurance/generate", [
            ReportController::class,
            "generateInsuranceReport",
        ]);

        Route::get("reports", [PublishableReportController::class, "index"]);

        Route::put("reports/{report}/publish", [
            PublishableReportController::class,
            "publish",
        ]);

        Route::get("reports/yearly/{year}/income/{user}/generate", [
            PublishableReportController::class,
            "generateYearlyReport",
        ]);
        Route::get("reports/yearly/{year}/income", [
            PublishableReportController::class,
            "getYearlyIncomeReport",
        ]);
        Route::post("reports/yearly/{year}/income/generate", [
            PublishableReportController::class,
            "generateAllYearlyReports",
        ]);

        Route::get("/user_reports/{userReport}/download", [
            UserPublishableReportController::class,
            "download",
        ]);
        Route::get("user_reports", [
            UserPublishableReportController::class,
            "index",
        ]);
        Route::get("payouts", [PayoutController::class, "index"]);
        Route::put("payouts/{payout}", [PayoutController::class, "update"]);
    });

    Route::get("/communities/overview", "CommunityController@overview")->name(
        "communities.overview"
    );
    Route::get(
        "/scheduler/actions/complete/{appKey}",
        "SchedulerController@loansForward"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/loans/forward/{appKey}",
        "SchedulerController@loansForward"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/email/loan/upcoming/{appKey}",
        "SchedulerController@emailLoanUpcoming"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/email/loan/prepayement/{appKey}",
        "SchedulerController@emailPrePayement"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/files/clean/db/{appKey}",
        "SchedulerController@filesCleanDB"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/files/clean/storage/{appKey}",
        "SchedulerController@filesCleanStorage"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/images/generate/sizes/{appKey}",
        "SchedulerController@imagesGenerateSizes"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/db/maintain/{appKey}",
        "SchedulerController@maintainDb"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/password/expiration/{appKey}",
        "SchedulerController@updatePasswordExpiration"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/contributions/split/{appKey}",
        "SchedulerController@contributionsSplit"
    )->where("appKey", ".*");

    Route::get("/scheduler/queue/work", "SchedulerController@queueWork");

    Route::get("/{any?}", "StaticController@notFound")->where("any", ".*");
});

GbfsController::getRoutes();
