<?php

Route::get("/auth/callback", "AuthController@callback");
Route::get("/auth/google", "AuthController@google");

Route::get("/password/reset", "StaticController@app")->name("password.reset");
Route::get("/status", "StaticController@status")->name("status");
