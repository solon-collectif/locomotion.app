# Contribuer à locomotion.app

Bienvenue sur le guide de contribution de locomotion.app.

Tout d'abord, merci de considérer contribuer à Locomotion.app. Nous essayons d'incarner une plateforme "coop", plus locale, axée vers la transition socio-écologique et donc avec une gouvernance la plus ouverte et démocratique possible.

Contribuer à Locomotion, c'est renforcer ces principes. Merci pour cela.

Avant de soumettre une contribution, assurez-vous d'avoir lu ce guide.

Nous sommes ouverts à une variété de contributions :

-   soumission de bugs;
-   correction de bugs;
-   proposition de nouvelles fonctionnalités;
-   amélioration de la couverture par des tests automatisés;
-   proposition d'améliorations à la simplicité et la clarté du code;
-   proposition de bonnes pratiques et standards à adopter;
-   amélioration de la documentation
-   amélioration de l'accessibilité universelle et élaboration de bonnes pratiques;
-   etc.

Si vous débutez, nous maintenons une liste d'incidents à l'intention des
nouveaux contributeurs : ~18698008.

Si vous vous sentez prêt pour l'aventure, nous vous invitons à contribuer aux prochains [sprints](https://gitlab.com/solon-collectif/locomotion.app/-/milestones).

## Code de conduite

Nous souhaitons promouvoir un espace sécuritaire pour tou-te-s.
Merci de respecter certaines règles de bonnes conduites :

-   Adopter un ton poli et respectueux des opinions différentes;
-   Faire preuve d'empathie et de bienveillance;
-   Partager des commentaires constructifs (pas de dénigrement, harcèlement ou mauvais esprit)

Merci de votre collaboration !

## License de contribution

Pour plus d'informations concernant le contrat de license qui se rapporte à notre projet, rendez vous sur notre [contribution license agreement](https://gitlab.com/solon-collectif/locomotion.app/-/blob/main/Contribution%20Licence%20Agreement).

## Rapporter un bug

Pour rapporter un bug, rendez vous sur notre [gestionnaire d'incidents](https://gitlab.com/Solon-collectif/locomotion.app/-/issues).

Conseil: vérifiez d'abord si le même problème n'a pas déjà été rapporté.

Si oui, n'hésitez pas à commenter l'incident concerné.

Si le problème est nouveau, indiquez avec autant de détails que nécessaire, les
étapes qui permettent de reproduire le problème. Décrivez le comportement
observé et le comportement attendu. N'hésitez pas à fournir des captures
d'écran, ou même des extraits vidéo.

Utilisez les mots clés appropriés, qui faciliteront la recherche. Dans le même
esprit, favorisez l'utilisation des mots en entier plutôt que leurs
abréviations ou acronymes.

## Corriger un bug

À moins qu'il ne s'agisse d'une erreur évidente, dont la correction n'ait pas
besoin de mise en contexte (faute d'orthographe, etc.), les bugs doivent
d'abord être documentés dans un rapport d'incident. Voir "Rapporter un bug".

Si vous souhaitez corriger des anomalies, il vous faudra certains droits supplémentaires. Faites signe pour que l'on vous accorde les permissions nécessaires.

Bonnes pratiques de correction d'un bug :

-   créer un test unitaire qui met le bug en évidence;
-   corriger le bug;
-   valider la correction du bug avec le test.

Documentez et commentez le code autant que nécessaire.

Une fois le problème corrigé, vous pourrez créer une demande de fusion (merge
request).

## Proposer une fonctionnalité et demandes de fusion (merge request)

Vous pouvez documenter toute proposition pour une nouvelle
fonctionnalité dans un rapport d'incident.

Si cette fonctionnalité s'inscrit dans l'esprit de locomotion.app, nous pourrons la mettre en oeuvre et il nous fera plaisir d'accepter votre merge request.

Dans tous les cas, n'hésitez pas à nous solliciter !

## Sécurité

Merci de votre vigilance !
Contactez-nous **par courriel** pour tout sujet relatif à la securité des données et de la plateforme à
[dev+securite@solon-collectif.org](mailto:dev+securite@solon-collectif.org).
