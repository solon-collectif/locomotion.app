#/bin/bash

# putting the secrets at the laravel designated place
ln -s -f $OAUTH_PRIVATE_PATH '/var/www/html/storage/oauth-private.key' 
ln -s -f $OAUTH_PUBLIC_PATH '/var/www/html/storage/oauth-public.key' 

# migrate need the cache but the cache table need to exist first 
# so using the array cache for the migration
export CACHE_DRIVER_OLD=$CACHE_DRIVER 
export CACHE_DRIVER=array 
php artisan migrate --force 
export CACHE_DRIVER=$CACHE_DRIVER_OLD

if [ "$USE_OCTANE" = 1 ]
then
  if [ "$APP_ENV" = "local" ]
  then
    php artisan octane:start --port=80 --host=0.0.0.0 --watch
  else
    php artisan config:cache
    php artisan octane:start --port=80 --host=0.0.0.0
  fi
else
  apache2-foreground
fi
