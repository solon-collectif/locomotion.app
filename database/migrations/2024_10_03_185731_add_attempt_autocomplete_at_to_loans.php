<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dateTimeTz("attempt_autocomplete_at")->nullable();
        });

        DB::table("loans")
            ->whereNotNull("meta->sent_stalled_email_at")
            ->update([
                "attempt_autocomplete_at" => DB::raw(
                    "timestamptz (meta->>'sent_stalled_email_at') + interval '2 days'"
                ),
            ]);
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("attempt_autocomplete_at");
        });
    }
};
