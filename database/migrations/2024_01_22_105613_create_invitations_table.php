<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("invitations", function (Blueprint $table) {
            $table->id();
            $table->string("email");
            $table->unsignedBigInteger("inviter_user_id");
            $table->unsignedBigInteger("community_id");
            $table->string("confirmation_code");
            $table->text("reason")->nullable();
            $table->text("message")->nullable();
            $table->boolean("auto_approve")->default(false);
            $table->date("expires_at")->nullable();
            $table->dateTimeTz("accepted_at")->nullable();
            $table->dateTimeTz("refused_at")->nullable();
            $table->boolean("validated_on_registration")->default(false);

            $table
                ->foreign("inviter_user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");

            $table
                ->foreign("community_id")
                ->references("id")
                ->on("communities")
                ->onDelete("cascade");

            $table->index("email");

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("invitations");
    }
};
