<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::rename("coowners", "loanable_user_roles");
        Schema::table("loanable_user_roles", function (Blueprint $table) {
            $table
                ->enum("role", ["coowner", "trusted_borrower"])
                ->default("coowner");
        });
    }

    public function down(): void
    {
        Schema::rename("coowners", "loanable_user_roles");
    }
};
