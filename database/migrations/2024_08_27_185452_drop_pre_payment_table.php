<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dateTimeTz("prepaid_at")->nullable();
        });

        DB::table("loans")
            ->join("pre_payments", "loans.id", "=", "pre_payments.loan_id")
            ->where("pre_payments.status", "completed")
            ->updateFrom([
                "prepaid_at" => DB::raw("pre_payments.executed_at"),
            ]);

        Schema::drop("pre_payments");
    }
};
