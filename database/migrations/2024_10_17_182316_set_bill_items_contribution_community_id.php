<?php

use App\Enums\BillItemTypes;
use App\Models\BillItem;
use App\Models\Pricing;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $invoiceItems = BillItem::whereIn("item_type", [
            BillItemTypes::loanContribution,
            BillItemTypes::contributionYearly,
        ])->cursor();

        $pricingCommunitiesById = Pricing::pluck("community_id", "id")->all();

        foreach ($invoiceItems as $item) {
            $itemMeta = $item->meta;

            $communityId = $item->contribution_community_id;

            if (!$communityId) {
                $communityId = isset($itemMeta["pricing_id"])
                    ? $pricingCommunitiesById[$itemMeta["pricing_id"]]
                    : null;

                if ($communityId) {
                    $item->contribution_community_id = $communityId;
                    $item->withoutTimestamps(fn() => $item->save());
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Nothing to do.
    }
};
