<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("final_purchases_amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->decimal("final_purchases_amount", 8, 2)
                ->nullable()
                ->default(null);
        });
    }
};
