<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("mailing_list_integrations", function (
            Blueprint $table
        ) {
            $table->id();
            $table->timestamps();
            $table
                ->foreignId("community_id")
                ->references("id")
                ->on("communities")
                ->onDelete("cascade");
            $table
                ->foreignId("created_by_user_id")
                ->references("id")
                ->on("users")
                ->noActionOnDelete();

            $table->enum("provider", ["brevo", "mailchimp"]);
            $table->string("api_key");
            $table->string("list_id");
            $table->string("list_name")->nullable();

            $table->enum("status", ["active", "suspended", "synchronizing"]);
            $table->unsignedInteger("consecutive_error_count")->default(0);
            $table->text("last_error")->nullable();

            $table->boolean("community_editable");
            $table->string("tag")->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("mailing_list_integrations");
    }
};
