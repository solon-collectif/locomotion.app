<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("publishable_reports", function (Blueprint $table) {
            $table->id();
            $table->enum("status", [
                "generating",
                "generated",
                "failed",
                "published",
            ]);
            $table->enum("type", ["yearly-owner-income"]);
            $table->string("report_key");
            $table
                ->string("batch_id")
                ->constrained("job_batches")
                ->nullable();
            $table->timestamps();

            $table->unique(["report_key", "type"]);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("reports");
    }
};
