<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedInteger("estimated_distance")
                ->nullable()
                ->change();
            $table->dropColumn("estimated_price");
            $table->dropColumn("estimated_insurance");
        });
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedInteger("estimated_distance")
                ->nullable()
                ->change();
            $table->decimal("estimated_price", 8, 2);
            $table->decimal("estimated_insurance", 8, 2);
        });
    }
};
