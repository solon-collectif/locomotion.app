<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->bigInteger("reported_by_user_id")->nullable();
            $table->bigInteger("resolved_by_user_id")->nullable();

            $table
                ->foreign("reported_by_user_id")
                ->references("id")
                ->on("users")
                ->noActionOnDelete();
            $table
                ->foreign("resolved_by_user_id")
                ->references("id")
                ->on("users")
                ->noActionOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dropColumn("reported_by_user_id");
            $table->dropColumn("resolved_by_user_id");
        });
    }
};
