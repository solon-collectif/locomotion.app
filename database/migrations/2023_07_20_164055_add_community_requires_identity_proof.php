<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->boolean("requires_identity_proof")->default(false);
        });
    }

    public function down(): void
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->dropColumn("requires_identity_proof");
        });
    }
};
