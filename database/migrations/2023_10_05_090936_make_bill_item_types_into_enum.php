<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table
                ->enum("item_types", [
                    "balance.provision",
                    "balance.refund",
                    "loan.price",
                    "loan.expenses",
                    "loan.insurance",
                    "donation.loan",
                    "donation.balance",
                    "fees.stripe",
                ])
                ->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table->string("item_types")->nullable();
        });
    }
};
