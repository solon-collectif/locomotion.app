<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("cars", function (Blueprint $table) {
            $table->boolean("has_onboard_notebook")->default(false);
            $table->boolean("has_report_in_notebook")->default(false);
        });
    }

    public function down(): void
    {
        Schema::table("cars", function (Blueprint $table) {
            $table->dropColumn("has_report_in_notebook");
            $table->dropColumn("has_onboard_notebook");
        });
    }
};
