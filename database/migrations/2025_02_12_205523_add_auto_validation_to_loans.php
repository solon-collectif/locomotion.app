<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->dateTimeTz("auto_validated_at")
                ->nullable()
                ->default(null);
            $table
                ->foreignId("auto_validated_by_user_id")
                ->nullable()
                ->constrained("users")
                ->noActionOnDelete();
        });
    }
};
