<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("community_user", function (Blueprint $table) {
            $table
                ->enum("proof_evaluation", ["unevaluated", "valid", "invalid"])
                ->default("unevaluated")
                ->nullable(false);
        });

        DB::table("community_user")
            ->join("users", "users.id", "community_user.user_id")
            ->where("users.is_proof_invalid", true)
            ->update([
                "proof_evaluation" => "invalid",
            ]);

        DB::table("community_user")
            ->whereNotNull("approved_at")
            ->whereNull("suspended_at")
            ->update([
                "proof_evaluation" => "valid",
            ]);

        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("is_proof_invalid");
        });
    }

    public function down(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->boolean("is_proof_invalid")->default(false);
        });

        DB::table("users", "u")->update([
            "is_proof_invalid" => DB::raw("
                exists(Select * from community_user 
                    where u.id = community_user.user_id 
                    and community_user.proof_evaluation = 'invalid'
                )"),
        ]);

        Schema::table("community_user", function (Blueprint $table) {
            $table->dropColumn("proof_evaluation");
        });
    }
};
