<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Builder;

return new class extends Migration {
    public function up(): void
    {
        \App\Helpers\MigrationHelper::changeEnumOptions("loans", "status", [
            "requested",
            "accepted",
            "confirmed",
            "ongoing",
            "ended",
            "validated",
            "completed",
            "canceled",
            "rejected",

            // To be removed:
            "in_process",
        ]);

        $now = \Carbon\CarbonImmutable::now();
        DB::table("loans")
            ->where("status", "in_process")
            ->whereNull("accepted_at")
            ->where("is_self_service", false)
            ->update(["status" => "requested"]);

        DB::table("loans")
            ->where("status", "in_process")
            ->where(
                fn(Builder $q) => $q
                    ->whereNotNull("accepted_at")
                    ->orWhere("is_self_service", true)
            )
            ->whereNull("prepaid_at")
            ->join("loanables", "loans.loanable_id", "=", "loanables.id")
            ->where("loanables.type", "car")
            ->update(["status" => "accepted"]);

        DB::table("loans")
            ->where("status", "in_process")
            ->where("departure_at", ">", $now)
            ->update([
                "status" => "confirmed",
            ]);

        DB::table("loans")
            ->where("status", "in_process")
            ->where("actual_return_at", ">", $now)
            ->update(["status" => "ongoing"]);

        DB::table("loans")
            ->where("status", "in_process")
            ->join("loanables", "loans.loanable_id", "=", "loanables.id")
            ->where("loanables.type", "car")
            ->where(
                fn(Builder $q) => $q
                    ->where("borrower_validated_at")
                    ->orWhereNull("owner_validated_at")
                    ->orWhereNull("mileage_end")
                    ->orWhereNull("mileage_start")
            )
            ->update(["status" => "ended"]);

        DB::table("loans")
            ->where("status", "in_process")
            ->update(["status" => "validated"]);

        DB::table("loans")
            ->where("status", "canceled")
            ->whereNotNull("rejected_at")
            ->update(["status" => "rejected"]);

        \App\Helpers\MigrationHelper::changeEnumOptions("loans", "status", [
            "requested",
            "accepted",
            "confirmed",
            "ongoing",
            "ended",
            "validated",
            "completed",
            "canceled",
            "rejected",
        ]);
    }
};
