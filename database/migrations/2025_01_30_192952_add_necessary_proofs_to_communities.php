<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->boolean("requires_residency_proof")->default(true);
            $table->boolean("requires_custom_proof")->default(false);

            $table->string("custom_proof_name")->nullable();
            $table->text("custom_proof_desc")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
};
