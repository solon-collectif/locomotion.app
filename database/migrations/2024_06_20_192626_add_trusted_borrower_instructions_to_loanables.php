<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->text("trusted_borrower_instructions")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("trusted_borrower_instructions");
        });
    }
};
