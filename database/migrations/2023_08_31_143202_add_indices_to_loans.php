<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->index("departure_at");
            $table->index("status");
        });
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropIndex("loans_departure_at_index");
            $table->dropIndex("loans_status_index");
        });
    }
};
