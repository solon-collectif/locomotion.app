<?php

use App\Models\Handover;
use Illuminate\Database\Migrations\Migration;

class SkipTakeoverForSelfService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $loans = DB::table("loans")
            ->where("loans.status", "in_process")
            ->join("loanables", "loanables.id", "=", "loans.loanable_id")
            ->where("loanables.is_self_service")
            ->join("takeovers", "takeovers.loan_id", "=", "loans.id")
            ->where("takeovers.status", "in_process")
            ->pluck("loans.id");

        DB::table("takeovers")
            ->whereIn("loan_id", $loans)
            ->update([
                "status" => "completed",
                "executed_at" => \Carbon\Carbon::now(),
            ]);

        DB::table("handovers")->insert(
            $loans
                ->map(
                    fn($loan_id) => [
                        "loan_id" => $loan_id,
                        "status" => "in_process",
                    ]
                )
                ->toArray()
        );
    }

    public function down()
    {
        // Do nothing.
    }
}
