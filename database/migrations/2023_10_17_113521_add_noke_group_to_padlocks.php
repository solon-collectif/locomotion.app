<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("padlocks", function (Blueprint $table) {
            $table->unsignedInteger("noke_group_id")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("padlocks", function (Blueprint $table) {
            $table->dropColumn("noke_group_id");
        });
    }
};
