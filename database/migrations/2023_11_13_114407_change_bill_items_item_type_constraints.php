<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table
                ->enum("item_type", [
                    "balance.provision",
                    "balance.refund",
                    "loan.price",
                    "loan.expenses",
                    "loan.insurance",
                    "donation.loan",
                    "donation.balance",
                    "fees.stripe",
                ])
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table
                ->string("item_type")
                ->nullable()
                ->change();
        });
    }
};
