<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("padlocks", function (Blueprint $table) {
            $table->drop();
        });
    }

    public function down(): void
    {
        Schema::table("padlocks", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->timestamps();
            $table->softDeletes();

            $table->string("mac_address")->unique();
            $table->string("loanable_type");

            $table->string("name");
            $table->string("external_id");
            $table
                ->unsignedBigInteger("loanable_id")
                ->nullable()
                ->index();

            $table->unsignedInteger("noke_group_id")->nullable();
        });
    }
};
