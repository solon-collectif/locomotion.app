<?php

use App\Helpers\MigrationHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        MigrationHelper::changeEnumOptions("loanable_user_roles", "role", [
            "owner",
            "coowner",
            "trusted_borrower",
        ]);

        \DB::statement(
            "INSERT INTO loanable_user_roles
                 (user_id, loanable_id, created_at, updated_at, show_as_contact, pays_loan_price, pays_loan_insurance, role)
             SELECT owners.user_id, loanables.id AS loanable_id, owners.created_at AS created_at, owners.updated_at AS updated_at,
                 show_owner_as_contact AS show_as_contact, FALSE AS pays_loan_price, FALSE AS pays_loan_insurance,
                 'owner' AS role
             FROM loanables
             INNER JOIN owners ON loanables.owner_id = owners.id
           "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DELETE FROM loanable_user_roles WHERE role = 'owner'");

        MigrationHelper::changeEnumOptions("loanable_user_roles", "role", [
            "coowner",
            "trusted_borrower",
        ]);
    }
};
