<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("is_self_service");
        });
    }

    public function down(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->boolean("is_self_service")->default(false);
        });

        DB::table("loanables")
            ->where("sharing_mode", "self_service")
            ->update([
                "is_self_service" => true,
            ]);
    }
};
