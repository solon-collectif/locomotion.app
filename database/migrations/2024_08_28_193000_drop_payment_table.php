<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->foreignId("owner_invoice_id")
                ->nullable()
                ->references("id")
                ->on("invoices")
                ->nullOnDelete();
            $table
                ->foreignId("borrower_invoice_id")
                ->nullable()
                ->references("id")
                ->on("invoices")
                ->nullOnDelete();
            $table->dateTimeTz("paid_at")->nullable();
        });

        DB::table("loans")
            ->join("payments", "loans.id", "=", "payments.loan_id")
            ->where("payments.status", "completed")
            ->updateFrom([
                "owner_invoice_id" => DB::raw("payments.owner_invoice_id"),
                "borrower_invoice_id" => DB::raw(
                    "payments.borrower_invoice_id"
                ),
                "paid_at" => DB::raw("payments.executed_at"),
            ]);

        Schema::drop("payments");
    }
};
