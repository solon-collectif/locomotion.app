<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table->unsignedBigInteger("contribution_community_id")->nullable();

            $table
                ->foreign("contribution_community_id")
                ->references("id")
                ->on("communities")
                ->noActionOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table("bill_items", function (Blueprint $table) {
            $table->dropColumn("contribution_community_id");
        });
    }
};
