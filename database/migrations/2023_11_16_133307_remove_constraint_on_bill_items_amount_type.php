<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "ALTER TABLE bill_items DROP CONSTRAINT bill_items_amount_type_check"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $types = ["debit", "credit"];
        $result = join(
            ", ",
            array_map(function ($value) {
                return sprintf("'%s'::character varying", $value);
            }, $types)
        );

        DB::statement(
            "ALTER TABLE bill_items ADD CONSTRAINT bill_items_amount_type_check CHECK (amount_type::text = ANY (ARRAY[$result]::text[]))"
        );
    }
};
