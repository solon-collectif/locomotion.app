<?php

use App\Models\Incident;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up(): void
    {
        Incident::where("created_at", "<", CarbonImmutable::today()->subWeek())
            ->whereHas(
                "loan",
                fn(Builder $query) => $query->where(
                    "status",
                    "!=",
                    "in_process"
                )
            )
            ->orWhereDoesntHave("loan") // Archive incidents for deleted loans
            ->update([
                "status" => "completed",
            ]);
    }
};
