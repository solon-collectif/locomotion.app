<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        // Add legacy types for migration
        Schema::table("pricings", function (Blueprint $table) {
            $table
                ->enum("pricing_type", [
                    "legacy",
                    "price",
                    "insurance",
                    "contribution",
                ])
                ->default("legacy");
        });

        $pricings = \App\Models\Pricing::where("pricing_type", "legacy")
            ->whereNotNull("rule")
            ->where("rule", "<>", "")
            ->where("rule", "<>", "0")
            ->get();

        $failureCount = 0;

        //        Split pricings into single types. Convert pricings with rules returning an object
        //        {time:..., insurance:...}, into two pricings returning a single value.
        foreach ($pricings as $pricing) {
            $ruleLines = collect(explode("\n", $pricing->rule))->filter(
                fn($line) => $line !== "" && !str_starts_with($line, "#")
            );

            $priceLines = [];
            $insuranceLines = [];

            $failed = false;

            foreach ($ruleLines as $ruleLine) {
                $matches = [];
                // Extract the time formula and insurance formulas from the combined pricing.
                // For example, for a pricing with {time: 3*$KM, insurance: 2*foo+bar}, we can
                // extract 3*$KM, and 2*foo+bar
                preg_match(
                    "/(.*)\{\s*time\s*:\s*(.*),\s*insurance\s*:\s*(.*)}/",
                    $ruleLine,
                    $matches
                );
                if (count($matches) < 4) {
                    Log::warning("Unexpected rule line: $ruleLine");
                    $failed = true;
                    continue;
                }

                $priceLines[] = "$matches[1] $matches[2]";
                $insuranceLines[] = "$matches[1] $matches[3]";
            }

            if ($failed) {
                $failureCount++;
                continue;
            }

            DB::table("pricings")->insert([
                "rule" => implode("\n", $priceLines),
                "community_id" => $pricing->community_id,
                "object_type" => $pricing->object_type,
                "name" => $pricing->name . " (prix)",
                "pricing_type" => "price",
            ]);

            DB::table("pricings")->insert([
                "rule" => implode("\n", $insuranceLines),
                "community_id" => $pricing->community_id,
                "object_type" => $pricing->object_type,
                "name" => $pricing->name . " (assurance)",
                "pricing_type" => "insurance",
            ]);

            $pricing->delete();
        }

        if ($failureCount > 0) {
            throw new Exception();
        }

        // Only rule=0 or empty pricings should remain, which can be archived.
        \App\Models\Pricing::where("pricing_type", "legacy")->delete();
    }

    public function down(): void
    {
        \App\Models\Pricing::withTrashed()
            ->where("pricing_type", "legacy")
            ->restore();
        \App\Models\Pricing::where(
            "pricing_type",
            "!=",
            "legacy"
        )->forceDelete();
        Schema::table("pricings", function (Blueprint $table) {
            $table->dropColumn("pricing_type");
        });
    }
};
