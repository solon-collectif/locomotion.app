<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    const loanableColumns = [
        "name",
        "position",
        "location_description",
        "comments",
        "instructions",
        "is_self_service",
        "availability_mode",
        "availability_json",
        "owner_id",
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    public function up()
    {
        \DB::statement("DROP MATERIALIZED VIEW loanables");

        Schema::create("loanables", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->enum("type", ["car", "trailer", "bike"]);
            $this->addLoanableColumns($table);

            $table->unique(["id", "type"]);
            $table->index(["id", "type"]);
        });
        DB::statement(
            "ALTER TABLE loanables ALTER COLUMN id SET DEFAULT nextval('loanables_id_seq'::regclass)"
        );

        $sharedColumns = ["id", ...self::loanableColumns];

        DB::table("loanables")->insertUsing(
            [...$sharedColumns, "type"],
            DB::table("cars")->select([...$sharedColumns, DB::raw("'car'")])
        );
        DB::table("loanables")->insertUsing(
            [...$sharedColumns, "type"],
            DB::table("bikes")->select([...$sharedColumns, DB::raw("'bike'")])
        );
        DB::table("loanables")->insertUsing(
            [...$sharedColumns, "type"],
            DB::table("trailers")->select([
                ...$sharedColumns,
                DB::raw("'trailer'"),
            ])
        );

        foreach (["trailers", "cars", "bikes"] as $loanableType) {
            Schema::table($loanableType, function (Blueprint $table) {
                $table
                    ->foreign("id")
                    ->references("id")
                    ->on("loanables")
                    ->onDelete("cascade");

                $table->dropColumn(self::loanableColumns);
            });
        }

        DB::table("images")
            ->where("imageable_type", "car")
            ->orWhere("imageable_type", "bike")
            ->orWhere("imageable_type", "trailer")
            ->update(["imageable_type" => "loanable"]);
    }

    public function down()
    {
        foreach (["trailers", "cars", "bikes"] as $loanableType) {
            Schema::table($loanableType, function (Blueprint $table) use (
                $loanableType
            ) {
                $this->addLoanableColumns($table);
                $table->dropForeign("{$loanableType}_id_foreign");
            });

            foreach (
                DB::table("loanables")
                    ->where("type", substr($loanableType, 0, -1))
                    ->select(["id", ...self::loanableColumns])
                    ->get()
                as $loanable
            ) {
                DB::table($loanableType)
                    ->where("id", $loanable->id)
                    ->update((array) $loanable);
            }
        }

        Schema::drop("loanables");

        DB::statement(
            <<<SQL
CREATE MATERIALIZED VIEW loanables
(id, type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id, created_at, updated_at, deleted_at) AS
    SELECT id, 'car' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id,created_at, updated_at, deleted_at FROM cars
UNION
    SELECT id, 'bike' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id,  created_at, updated_at, deleted_at FROM bikes
UNION
    SELECT id, 'trailer' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id,  created_at, updated_at, deleted_at FROM trailers
SQL
        );
        DB::statement(
            <<<SQL
CREATE UNIQUE INDEX loanables_index
ON loanables (id, type);
SQL
        );
    }

    /**
     * @param Blueprint $table
     * @return void
     */
    private function addLoanableColumns(Blueprint $table): void
    {
        $table->string("name")->nullable();
        $table->point("position")->nullable();
        $table->text("location_description")->nullable();
        $table->text("comments")->nullable();
        $table->text("instructions")->nullable();
        $table->boolean("is_self_service")->default(false);
        $table
            ->enum("availability_mode", ["always", "never"])
            ->default("never");
        $table->text("availability_json")->default("[]");

        $table->unsignedBigInteger("owner_id")->nullable();

        $table->timestamps();
        $table->softDeletes();

        $table
            ->foreign("owner_id")
            ->references("id")
            ->on("owners")
            ->onDelete("cascade");
    }
};
