<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Remove invalid loans before setting non nullable constraint.
        \DB::statement("DELETE FROM loans WHERE borrower_id IS NULL");

        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedBigInteger("borrower_id")
                ->nullable(false)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedBigInteger("borrower_id")
                ->nullable()
                ->change();
        });
    }
};
