<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create("loanable_type_details", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name");
        });

        DB::table("loanable_type_details")->insert([
            ["name" => "car"],
            ["name" => "trailer"],
            ["name" => "bike"],
        ]);

        Schema::create("community_loanable_types", function (Blueprint $table) {
            $table->unsignedBigInteger("community_id");
            $table->unsignedBigInteger("loanable_type_id");

            $table
                ->foreign("community_id")
                ->references("id")
                ->on("communities")
                ->onDelete("cascade");

            $table
                ->foreign("loanable_type_id")
                ->references("id")
                ->on("loanable_type_details")
                ->onDelete("cascade");
        });

        // Initially all types are allowed for all communities
        DB::table("community_loanable_types")->insertUsing(
            ["community_id", "loanable_type_id"],
            DB::table("communities")
                ->crossJoin("loanable_type_details")
                ->select("communities.id", "loanable_type_details.id")
        );
    }

    public function down()
    {
        Schema::drop("community_loanable_types");
        Schema::drop("loanable_types");
    }
};
