<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("exports", function (Blueprint $table) {
            $table->bigIncrements("id");

            $table->unsignedBigInteger("user_id");
            $table->string("model");
            $table->string("batch_id")->nullable();
            $table
                ->enum("status", [
                    "not_started",
                    "in_process",
                    "completed",
                    "failed",
                    "canceled",
                ])
                ->default("not_started");
            $table->float("progress")->default(0);

            $table
                ->foreign("user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");

            $table
                ->foreign("batch_id")
                ->references("id")
                ->on("job_batches")
                ->nullOnDelete();

            $table->timestamp("started_at")->nullable();
            $table->unsignedFloat("duration_in_seconds")->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("exports");
    }
};
