<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->index("actual_return_at");
        });

        Schema::table("coowners", function (Blueprint $table) {
            $table
                ->foreign("loanable_id")
                ->references("id")
                ->on("loanables")
                ->onDelete("cascade");
        });

        Schema::table("users", function (Blueprint $table) {
            $table->index("created_at");
        });
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropIndex("loans_actual_return_at_index");
        });

        Schema::table("coowners", function (Blueprint $table) {
            $table->dropForeign("coowners_loanable_id_foreign");
        });

        Schema::table("users", function (Blueprint $table) {
            $table->dropIndex("users_created_at_index");
        });
    }
};
