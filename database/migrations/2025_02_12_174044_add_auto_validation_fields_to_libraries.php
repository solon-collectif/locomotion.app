<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("libraries", function (Blueprint $table) {
            $table->text("loan_auto_validation_question")->nullable();
            $table->string("loan_auto_validation_answer")->nullable();
        });
    }
};
