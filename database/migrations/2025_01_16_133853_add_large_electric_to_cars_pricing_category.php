<?php

use App\Helpers\MigrationHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        MigrationHelper::changeEnumOptions("cars", "pricing_category", [
            "small",
            "large",
            "electric",
            "small_electric",
            "large_electric",
        ]);

        DB::statement(
            "UPDATE cars SET pricing_category = 'small_electric' WHERE pricing_category = 'electric'"
        );

        MigrationHelper::changeEnumOptions("cars", "pricing_category", [
            "small",
            "large",
            "small_electric",
            "large_electric",
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
    }
};
