<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("subscriptions", function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->dateTime("start_date");
            $table->dateTime("end_date");
            $table->enum("type", ["paid", "granted"]);
            $table->text("reason")->nullable();

            $table->unsignedBigInteger("community_user_id");
            $table->unsignedBigInteger("granted_by_user_id");
            $table->unsignedBigInteger("invoice_id")->nullable();

            $table
                ->foreign("community_user_id")
                ->references("id")
                ->on("community_user")
                ->onDelete("cascade");

            $table
                ->foreign("granted_by_user_id")
                ->references("id")
                ->on("users")
                ->noActionOnDelete();

            $table
                ->foreign("invoice_id")
                ->references("id")
                ->on("invoices")
                ->noActionOnDelete();
        });

        Schema::create("subscription_loanable_types", function (
            Blueprint $table
        ) {
            $table->unsignedBigInteger("subscription_id");

            $table->enum("pricing_loanable_type", [
                "car_small",
                "car_large",
                "car_electric",
                "trailer",
                "bike_regular",
                "bike_electric",
                "bike_cargo_regular",
                "bike_cargo_electric",
            ]);

            $table
                ->foreign("subscription_id")
                ->references("id")
                ->on("subscriptions")
                ->cascadeOnDelete();

            $table->primary(["subscription_id", "pricing_loanable_type"]);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("subscription_loanable_types");
        Schema::dropIfExists("subscriptions");
    }
};
