<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("cars", function (Blueprint $table) {
            $table
                ->year("year_of_circulation")
                ->nullable()
                ->change();
            $table
                ->enum("transmission_mode", ["automatic", "manual"])
                ->nullable()
                ->change();
            $table
                ->enum("engine", ["fuel", "diesel", "electric", "hybrid"])
                ->nullable()
                ->change();
            $table
                ->enum("pricing_category", ["small", "large", "electric"])
                ->nullable()
                ->change();
            $table
                ->enum("papers_location", ["in_the_car", "to_request_with_car"])
                ->nullable()
                ->change();

            $table
                ->string("brand")
                ->nullable()
                ->change();
            $table
                ->string("model")
                ->nullable()
                ->change();
            $table
                ->string("plate_number")
                ->nullable()
                ->change();
            $table
                ->string("insurer")
                ->nullable()
                ->change();
        });

        Schema::table("bikes", function (Blueprint $table) {
            $table
                ->string("model")
                ->nullable()
                ->change();
            $table
                ->enum("bike_type", [
                    "regular",
                    "cargo",
                    "electric",
                    "fixed_wheel",
                ])
                ->nullable()
                ->change();
            $table
                ->enum("size", ["big", "medium", "small", "kid"])
                ->nullable()
                ->change();
        });

        Schema::table("trailers", function (Blueprint $table) {
            $table
                ->string("maximum_charge")
                ->nullable()
                ->change();
        });
    }

    public function down(): void
    {
        Schema::table("cars", function (Blueprint $table) {
            $table
                ->year("year_of_circulation")
                ->nullable(false)
                ->change();
            $table
                ->enum("transmission_mode", ["automatic", "manual"])
                ->nullable()
                ->change();
            $table
                ->enum("engine", ["fuel", "diesel", "electric", "hybrid"])
                ->nullable()
                ->change();
            $table
                ->enum("pricing_category", ["small", "large", "electric"])
                ->nullable()
                ->change();
            $table
                ->enum("papers_location", ["in_the_car", "to_request_with_car"])
                ->nullable()
                ->change();

            $table
                ->string("brand")
                ->nullable()
                ->change();
            $table
                ->string("model")
                ->nullable()
                ->change();
            $table
                ->string("plate_number")
                ->nullable()
                ->change();
            $table
                ->string("insurer")
                ->nullable()
                ->change();
        });

        Schema::table("bikes", function (Blueprint $table) {
            $table->string("model")->change();
            $table
                ->enum("bike_type", [
                    "regular",
                    "cargo",
                    "electric",
                    "fixed_wheel",
                ])
                ->change();
            $table->enum("size", ["big", "medium", "small", "kid"])->change();
        });

        Schema::table("trailers", function (Blueprint $table) {
            $table->string("maximum_charge")->change();
        });
    }
};
