<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->unsignedInteger("mileage_end")->nullable();
            $table->decimal("expenses_amount", 8, 2)->nullable();
        });

        DB::table("loans")
            ->join("handovers", "loans.id", "=", "handovers.loan_id")
            ->where("handovers.status", "completed")
            ->updateFrom([
                "mileage_end" => DB::raw("handovers.mileage_end"),
                "expenses_amount" => DB::raw("handovers.purchases_amount"),
            ]);

        // comments by borrower
        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("handovers")
                ->join("loans", "loans.id", "=", "handovers.loan_id")
                ->select(
                    "loan_id",
                    "loans.borrower_user_id",
                    "comments_by_borrower",
                    "executed_at"
                )
                ->where("comments_by_borrower", "!=", "")
                ->whereNotNull("comments_by_borrower")
        );

        // comments by owner
        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("handovers")
                ->join("loans", "loans.id", "=", "handovers.loan_id")
                ->join("loanables", "loans.loanable_id", "=", "loanables.id")
                ->join(
                    "loanable_user_roles as owners",
                    fn(\Illuminate\Database\Query\JoinClause $join) => $join
                        ->on("owners.loanable_id", "=", "loanables.id")
                        ->where(
                            "owners.role",
                            "=",
                            \App\Enums\LoanableUserRoles::Owner
                        )
                )
                ->select(
                    "loan_id",
                    "owners.user_id",
                    "comments_by_owner",
                    "executed_at"
                )
                ->where("comments_by_owner", "!=", "")
                ->whereNotNull("comments_by_owner")
        );
        // comments_on_contestation
        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("handovers")
                ->join("loans", "loans.id", "=", "handovers.loan_id")
                ->select(
                    "loan_id",
                    "loans.borrower_user_id",
                    "comments_on_contestation",
                    "contested_at"
                )
                ->where("comments_on_contestation", "!=", "")
                ->whereNotNull("comments_on_contestation")
        );

        DB::table("images")
            ->join("handovers", "handovers.id", "=", "images.imageable_id")
            ->where("imageable_type", "handover")
            ->where("field", "image")
            ->updateFrom([
                "imageable_type" => "loan",
                "field" => "mileage_end",
                "imageable_id" => DB::raw("handovers.loan_id"),
            ]);

        DB::table("images")
            ->join("handovers", "handovers.id", "=", "images.imageable_id")
            ->where("imageable_type", "handover")
            ->where("field", "expense")
            ->updateFrom([
                "imageable_type" => "loan",
                "field" => "expense",
                "imageable_id" => DB::raw("handovers.loan_id"),
            ]);

        Schema::drop("handovers");
    }
};
