<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        $tablesToUpdate = DB::table("information_schema.columns")
            ->where("data_type", "timestamp without time zone")
            ->select([
                "table_name",
                DB::raw("array_agg(column_name::text) as columns"),
            ])
            ->groupBy("table_name")
            ->pluck("columns", "table_name")
            ->all();

        foreach ($tablesToUpdate as $table => $columnsPgList) {
            Schema::table($table, function (Blueprint $schema) {
                $schema->boolean("_migrated_to_utc")->default(false);
            });

            $columns = explode(",", rtrim(ltrim($columnsPgList, "{"), "}"));
            $columnValues = [];
            foreach ($columns as $column) {
                $columnValues[$column] = DB::raw(
                    "$column at time zone 'America/Toronto'"
                );
            }
            $columnValues["_migrated_to_utc"] = true;

            DB::table($table)
                ->where("_migrated_to_utc", false)
                ->update($columnValues);
        }

        foreach ($tablesToUpdate as $table => $columnsPgList) {
            Schema::table($table, function (Blueprint $schema) {
                $schema->dropColumn("_migrated_to_utc");
            });
        }
    }

    public function down(): void
    {
        $tablesToUpdate = DB::table("information_schema.columns")
            ->where("data_type", "timestamp without time zone")
            ->select([
                "table_name",
                DB::raw("array_agg(column_name::text) as columns"),
            ])
            ->groupBy("table_name")
            ->pluck("columns", "table_name")
            ->all();

        foreach ($tablesToUpdate as $table => $columnsPgList) {
            Schema::table($table, function (Blueprint $schema) {
                $schema->boolean("_migrated_to_toronto")->default(false);
            });

            $columns = explode(",", rtrim(ltrim($columnsPgList, "{"), "}"));
            $columnValues = [];
            foreach ($columns as $column) {
                $columnValues[$column] = DB::raw(
                    "$column at time zone 'UTC' at time zone 'America/Toronto'"
                );
            }
            $columnValues["_migrated_to_toronto"] = true;

            DB::table($table)
                ->where("_migrated_to_toronto", false)
                ->update($columnValues);
        }

        foreach ($tablesToUpdate as $table => $columnsPgList) {
            Schema::table($table, function (Blueprint $schema) {
                $schema->dropColumn("_migrated_to_toronto");
            });
        }
    }
};
