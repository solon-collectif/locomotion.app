<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->string("bank_account_number")->nullable();
            $table->string("bank_institution_number")->nullable();
            $table->string("bank_transit_number")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("bank_institution_number");
            $table->dropColumn("bank_account_number");
            $table->dropColumn("bank_transit_number");
        });
    }
};
