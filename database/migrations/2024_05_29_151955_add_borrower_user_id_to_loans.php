<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->unsignedBigInteger("borrower_user_id")->nullable();
        });

        // Set borrower_user_id from borrowers.user_id
        \DB::statement(
            "UPDATE loans SET borrower_user_id = b.user_id FROM borrowers b WHERE b.id = loans.borrower_id "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("borrower_user_id");
        });
    }
};
