<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("pricings", function (Blueprint $table) {
            $table->date("start_date")->nullable();
            $table->date("end_date")->nullable();
        });

        DB::table("pricings")->update([
            "start_date" => DB::raw(
                "COALESCE(DATE(pricings.created_at), DATE('2023-01-01'))"
            ),
        ]);

        Schema::table("pricings", function (Blueprint $table) {
            $table
                ->date("start_date")
                ->nullable(false)
                ->change();
        });
    }

    public function down(): void
    {
        Schema::table("pricings", function (Blueprint $table) {
            $table->dropColumn("start_date");
            $table->dropColumn("end_date");
        });
    }
};
