<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->boolean("is_self_service")->default(false);
        });

        \App\Models\Loan::whereHas(
            "loanable",
            fn(Builder $q) => $q->where("is_self_service", true)
        )->update([
            "is_self_service" => true,
        ]);
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("is_self_service");
        });
    }
};
