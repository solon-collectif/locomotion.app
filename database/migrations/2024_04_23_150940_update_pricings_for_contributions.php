<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("pricings", function (Blueprint $table) {
            $table
                ->enum("loanable_ownership_type", ["all", "fleet", "non_fleet"])
                ->nullable(false)
                ->default("all");

            $table->decimal("yearly_target_per_user", 8, 2)->nullable(true);

            $table
                ->boolean("is_mandatory")
                ->nullable()
                ->default(null);

            $table
                ->bigInteger("community_id")
                ->nullable(true)
                ->change();

            $table->text("description")->nullable(true);
        });

        Schema::create("pricing_loanable_types", function (Blueprint $table) {
            $table->unsignedBigInteger("pricing_id");

            $table->enum("pricing_loanable_type", [
                "car_small",
                "car_large",
                "car_electric",
                "trailer",
                "bike_regular",
                "bike_electric",
                "bike_cargo_regular",
                "bike_cargo_electric",
            ]);

            $table
                ->foreign("pricing_id")
                ->references("id")
                ->on("pricings")
                ->cascadeOnDelete();

            $table->primary(["pricing_id", "pricing_loanable_type"]);
        });

        $carPricings = \App\Models\Pricing::whereNotNull("rule")
            ->where("rule", "<>", "")
            ->where("rule", "<>", "0")
            ->where("object_type", "App\Models\Car")
            ->get();

        foreach ($carPricings as $pricing) {
            $ruleLines = collect(explode("\n", $pricing->rule))->filter(
                fn($line) => $line !== "" && !str_starts_with($line, "#")
            );

            $basePricing = [
                "community_id" => $pricing->community_id,
                "pricing_type" => $pricing->pricing_type,
                "name" => $pricing->name,
            ];
            $sharedLines = [];
            $coveredLarge = false;
            $coveredElectric = false;
            $coveredSmall = false;

            foreach ($ruleLines as $ruleLine) {
                if (
                    !$coveredLarge &&
                    $this->extractCarPricingLine(
                        $ruleLine,
                        $basePricing,
                        "large",
                        "car_large"
                    )
                ) {
                    $coveredLarge = true;
                    continue;
                }
                if (
                    !$coveredSmall &&
                    $this->extractCarPricingLine(
                        $ruleLine,
                        $basePricing,
                        "small",
                        "car_small"
                    )
                ) {
                    $coveredSmall = true;
                    continue;
                }

                if (
                    !$coveredElectric &&
                    $this->extractCarPricingLine(
                        $ruleLine,
                        $basePricing,
                        "electric",
                        "car_electric"
                    )
                ) {
                    $coveredElectric = true;
                    continue;
                }

                $sharedLines[] = $ruleLine;
            }

            // If covered all cases, no need for default.
            if (
                empty($sharedLines) ||
                ($coveredElectric && $coveredSmall && $coveredLarge)
            ) {
                $pricing->delete();
            } else {
                Log::warning("Empty lines:", $sharedLines);
                $pricing->update([
                    "rule" => implode("\n", $sharedLines),
                ]);
            }
        }

        $this->assignPricingLoanableType("App\Models\Car", "car_large");
        $this->assignPricingLoanableType("App\Models\Car", "car_small");
        $this->assignPricingLoanableType("App\Models\Car", "car_electric");

        $this->assignPricingLoanableType("App\Models\Trailer", "trailer");

        $this->assignPricingLoanableType("App\Models\Bike", "bike_regular");
        $this->assignPricingLoanableType("App\Models\Bike", "bike_electric");
        $this->assignPricingLoanableType(
            "App\Models\Bike",
            "bike_cargo_regular"
        );
        $this->assignPricingLoanableType(
            "App\Models\Bike",
            "bike_cargo_electric"
        );

        Schema::table("pricings", function (Blueprint $table) {
            $table->dropColumn("object_type");
        });
    }

    public function down(): void
    {
        Schema::table("pricings", function (Blueprint $table) {
            $table->string("object_type")->nullable();
            $table->dropColumn("loanable_ownership_type");
            $table->dropColumn("yearly_target_per_user");
            $table->dropColumn("is_mandatory");
            $table->dropColumn("description");
        });

        DB::table("pricings")
            ->join(
                "pricing_loanable_types",
                "pricing_loanable_types.pricing_id",
                "=",
                "pricings.id"
            )
            ->whereIn("pricing_loanable_type", [
                "car_electric",
                "car_small",
                "car_large",
            ])
            ->update([
                "object_type" => "App\Models\Car",
            ]);

        DB::table("pricings")
            ->join(
                "pricing_loanable_types",
                "pricing_loanable_types.pricing_id",
                "=",
                "pricings.id"
            )
            ->where("pricing_loanable_type", "trailer")
            ->update([
                "object_type" => "App\Models\Trailer",
            ]);

        DB::table("pricings")
            ->join(
                "pricing_loanable_types",
                "pricing_loanable_types.pricing_id",
                "=",
                "pricings.id"
            )
            ->whereIn("pricing_loanable_type", [
                "bike_regular",
                "bike_electric",
                "bike_cargo_regular",
                "bike_cargo_electric",
            ])
            ->update([
                "object_type" => "App\Models\Bike",
            ]);

        Schema::drop("pricing_loanable_types");
    }

    private function assignPricingLoanableType(
        $objectType,
        $pricingLoanableType
    ): void {
        DB::table("pricing_loanable_types")->insertUsing(
            ["pricing_id", "pricing_loanable_type"],
            DB::table("pricings")
                ->where("object_type", $objectType)
                ->select([
                    DB::raw("id::int as pricing_id"),
                    DB::raw("'$pricingLoanableType' as pricing_loanable_type"),
                ])
        );
    }

    private function extractCarPricingLine(
        $ruleLine,
        $basePricing,
        $pricingCategory,
        $pricingLoanableType
    ) {
        $matches = [];
        if (
            preg_match(
                "/SI\s*\\\$OBJET\.pricing_category\s*==\s*'$pricingCategory'\s*ALORS\s*(.*)/",
                $ruleLine,
                $matches
            )
        ) {
            $existingPricing = DB::table("pricings")
                ->where("rule", $matches[1])
                ->where("community_id", $basePricing["community_id"])
                ->where("pricing_type", $basePricing["pricing_type"])
                ->first();

            if ($existingPricing) {
                $pricingId = $existingPricing->id;
            } else {
                $pricingId = DB::table("pricings")->insertGetId([
                    ...$basePricing,
                    "rule" => $matches[1],
                ]);
            }

            DB::table("pricing_loanable_types")->insert([
                "pricing_id" => $pricingId,
                "pricing_loanable_type" => $pricingLoanableType,
            ]);
            return true;
        }
        return false;
    }
};
