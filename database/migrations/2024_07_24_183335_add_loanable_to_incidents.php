<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->bigInteger("loanable_id")->nullable();

            $table
                ->foreign("loanable_id")
                ->references("id")
                ->on("loanables")
                ->noActionOnDelete();
        });

        DB::table("incidents")
            ->join("loans as l", "incidents.loan_id", "l.id")
            ->update([
                "incidents.loanable_id" => DB::raw(
                    "(Select loanable_id from loans where loans.id = incidents.loan_id)"
                ),
            ]);

        Schema::table("incidents", function (Blueprint $table) {
            $table
                ->bigInteger("loanable_id")
                ->nullable(false)
                ->change();
        });
    }

    public function down(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dropColumn("loanable_id");
        });
    }
};
