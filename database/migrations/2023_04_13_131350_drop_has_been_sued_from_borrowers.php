<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropHasBeenSuedFromBorrowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("borrowers", function (Blueprint $table) {
            $table->dropColumn("has_been_sued_last_ten_years");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("borrowers", function (Blueprint $table) {
            $table->boolean("has_been_sued_last_ten_years")->default(false);
        });
    }
}
