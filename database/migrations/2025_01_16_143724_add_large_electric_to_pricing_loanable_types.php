<?php

use App\Helpers\MigrationHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        MigrationHelper::changeEnumOptions(
            "pricing_loanable_types",
            "pricing_loanable_type",
            [
                "car_small",
                "car_large",
                "car_electric",
                "car_small_electric",
                "car_large_electric",
                "trailer",
                "bike_regular",
                "bike_electric",
                "bike_cargo_regular",
                "bike_cargo_electric",
            ]
        );

        // Rename car_electric -> car_small_electric
        DB::statement(
            "UPDATE pricing_loanable_types SET pricing_loanable_type = 'car_small_electric' WHERE pricing_loanable_type = 'car_electric'"
        );

        // Ensure large electric cars have the same pricings as small electric cars (for now).
        DB::statement(
            "INSERT INTO pricing_loanable_types (pricing_id, pricing_loanable_type) (SELECT pricing_id, 'car_large_electric' FROM pricing_loanable_types WHERE pricing_loanable_type = 'car_small_electric')"
        );

        MigrationHelper::changeEnumOptions(
            "pricing_loanable_types",
            "pricing_loanable_type",
            [
                "car_small",
                "car_large",
                "car_small_electric",
                "car_large_electric",
                "trailer",
                "bike_regular",
                "bike_electric",
                "bike_cargo_regular",
                "bike_cargo_electric",
            ]
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table("pricing_loanable_types", function (Blueprint $table) {
            //
        });
    }
};
