<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loan_notifications", function (Blueprint $table) {
            $table->dateTimeTz("last_seen")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("loan_notifications", function (Blueprint $table) {
            $table->dropColumn("last_seen");
        });
    }
};
