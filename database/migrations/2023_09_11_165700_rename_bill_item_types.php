<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\BillItem;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\BillItem::where("item_type", "provision.net")->update([
            "item_type" => "balance.provision",
        ]);
        \App\Models\BillItem::where(
            "item_type",
            "provision.transactionFees"
        )->update([
            "item_type" => "fees.stripe",
        ]);

        \App\Models\BillItem::where("item_type", "loan.platformTip")->update([
            "item_type" => "donation.loan",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\BillItem::where("item_type", "balance.provision")->update([
            "item_type" => "provision.net",
        ]);
        \App\Models\BillItem::where("item_type", "fees.stripe")->update([
            "item_type" => "provision.transactionFees",
        ]);

        \App\Models\BillItem::where("item_type", "donation.loan")->update([
            "item_type" => "loan.platformTip",
        ]);
    }
};
