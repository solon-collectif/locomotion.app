<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("community_loanable_types", function (Blueprint $table) {
            $table->unique(["community_id", "loanable_type_id"]);
        });
    }
};
