<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("loans")
                ->select(
                    "id",
                    "borrower_user_id",
                    "message_for_owner",
                    "created_at"
                )
                ->where("message_for_owner", "!=", "")
                ->whereNotNull("message_for_owner")
        );

        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("message_for_owner");
        });
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->text("message_for_owner")->default("");
        });
    }
};
