<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("community_library", function (Blueprint $table) {
            $table->id();

            $table
                ->foreignId("community_id")
                ->constrained("communities")
                ->cascadeOnDelete();
            $table
                ->foreignId("library_id")
                ->constrained("libraries")
                ->cascadeOnDelete();

            $table->unique(["community_id", "library_id"]);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("community_library");
    }
};
