<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    const tzTablesToColums = [
        "borrowers" => ["submitted_at", "approved_at", "suspended_at"],
        "community_user" => ["approved_at", "suspended_at"],
        "exports" => ["started_at"],
        "intentions" => ["executed_at"],
        "pre_payments" => ["executed_at"],
        "takeovers" => ["executed_at", "contested_at"],
        "handovers" => ["executed_at", "contested_at"],
        "payments" => ["executed_at"],
        "extensions" => ["executed_at"],
        "incidents" => ["executed_at"],
        "invitations" => ["accepted_at", "refused_at"],
        "invoices" => ["paid_at"],
        "loans" => [
            "departure_at",
            "canceled_at",
            "actual_return_at",
            "owner_validated_at",
            "borrower_validated_at",
        ],
        "users" => ["email_verified_at", "conditions_accepted_at"],
    ];

    const tzTableColumnsToDrop = [
        "extensions" => ["contested_at"],
        "owners" => ["submitted_at", "approved_at"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::tzTablesToColums as $table => $columns) {
            Schema::table($table, function (Blueprint $table) use ($columns) {
                foreach ($columns as $column) {
                    $table->dateTime($column)->change();
                }
            });
        }

        foreach (self::tzTableColumnsToDrop as $table => $columns) {
            Schema::table($table, function (Blueprint $table) use ($columns) {
                $table->dropColumn($columns);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::tzTablesToColums as $table => $columns) {
            Schema::table($table, function (Blueprint $table) use ($columns) {
                foreach ($columns as $column) {
                    $table->dateTimeTz($column)->change();
                }
            });
        }

        foreach (self::tzTableColumnsToDrop as $table => $columns) {
            Schema::table($table, function (Blueprint $table) use ($columns) {
                foreach ($columns as $column) {
                    $table->dateTimeTz($column)->nullable();
                }
            });
        }
    }
};
