<?php

use App\Enums\LoanableUserRoles;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("loan_notifications", function (Blueprint $table) {
            $table->id();

            $table->bigInteger("user_id");
            $table->bigInteger("loan_id");
            $table->enum("level", ["all", "none", "messages_only"]);

            $table
                ->foreign("user_id")
                ->references("id")
                ->on("users")
                ->cascadeOnDelete();

            $table
                ->foreign("loan_id")
                ->references("id")
                ->on("loans")
                ->cascadeOnDelete();

            $table->timestamps();
        });

        // (Co-)Owners in on demand loans => notification level = all
        DB::table("loan_notifications")->insertUsing(
            ["loan_id", "user_id", "level"],
            DB::table("loans")
                ->join("loanables", "loans.loanable_id", "=", "loanables.id")
                ->join(
                    "loanable_user_roles",
                    "loanables.id",
                    "=",
                    "loanable_user_roles.loanable_id"
                )
                ->select(
                    "loans.id",
                    "loanable_user_roles.user_id",
                    DB::raw("'all'")
                )
                ->whereIn("loanable_user_roles.role", [
                    \App\Enums\LoanableUserRoles::Coowner,
                    LoanableUserRoles::Owner,
                ])
                ->where("loans.is_self_service", false)
                ->whereIn(
                    "loans.status",
                    \App\Enums\LoanStatus::IN_PROCESS_STATES
                )
        );

        // (Co-)Owners in on demand loans => notification level = messages_only
        DB::table("loan_notifications")->insertUsing(
            ["loan_id", "user_id", "level"],
            DB::table("loans")
                ->join("loanables", "loans.loanable_id", "=", "loanables.id")
                ->join(
                    "loanable_user_roles",
                    "loanables.id",
                    "=",
                    "loanable_user_roles.loanable_id"
                )
                ->select(
                    "loans.id",
                    "loanable_user_roles.user_id",
                    DB::raw("'messages_only'")
                )
                ->whereIn("loanable_user_roles.role", [
                    \App\Enums\LoanableUserRoles::Coowner,
                    LoanableUserRoles::Owner,
                ])
                ->where("loans.is_self_service", true)
                ->whereIn(
                    "loans.status",
                    \App\Enums\LoanStatus::IN_PROCESS_STATES
                )
        );

        DB::table("loan_notifications")->insertUsing(
            ["loan_id", "user_id", "level"],
            DB::table("loans")
                ->select("loans.id", "borrower_user_id", DB::raw("'all'"))
                ->whereIn(
                    "loans.status",
                    \App\Enums\LoanStatus::IN_PROCESS_STATES
                )
        );
    }

    public function down(): void
    {
        Schema::dropIfExists("loan_notifications");
    }
};
