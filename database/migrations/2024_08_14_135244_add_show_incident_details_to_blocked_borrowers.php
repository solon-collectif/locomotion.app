<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table
                ->boolean("show_details_to_blocked_borrowers")
                ->default(false);
        });
    }

    public function down(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dropColumn("show_details_to_blocked_borrowers");
        });
    }
};
