<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up(): void
    {
        DB::table("files")
            ->where("fileable_type", "communityUser")
            ->where("field", "proof")
            ->join("community_user", "community_user.id", "files.fileable_id")
            ->update([
                "fileable_type" => "user",
                "fileable_id" => DB::raw(
                    "(SELECT community_user.user_id FROM community_user WHERE community_user.id = files.fileable_id)"
                ),
            ]);
    }

    public function down(): void
    {
        DB::table("files")
            ->where("fileable_type", "user")
            ->where("field", "proof")
            ->update([
                "fileable_type" => "communityUser",
                "fileable_id" => DB::raw(
                    "(SELECT community_user.id FROM community_user WHERE community_user.user_id = files.fileable_id)"
                ),
            ]);
    }
};
