<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table
                ->foreignId("library_id")
                ->nullable()
                ->constrained("libraries")
                ->nullOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropConstrainedForeignId("library_id");
        });
    }
};
