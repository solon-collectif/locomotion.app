<?php

use App\Models\Loanable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->boolean("published")->default(false);
        });

        /*
          Query extracted from previous statement:

              Loanable::query()
                  ->hasValidType()
                  ->update([
                      "published" => true,
                  ]);

          Model Loanable expects loanable_user_roles to exist, which is created later and this statement crashed.
        */
        \DB::raw(
            <<<SQL
    UPDATE loanables
    SET published = '1'
    WHERE EXISTS (
        SELECT * FROM owners
        WHERE loanables.owner_id = owners.id
        AND EXISTS (
            SELECT *
            FROM users
            WHERE owners.user_id = users.id
            AND EXISTS (
                SELECT *
                FROM communities
                INNER JOIN community_user ON communities.id = community_user.community_id
                WHERE users.id = community_user.user_id
                AND EXISTS (
                    SELECT *
                    FROM loanable_type_details INNER JOIN community_loanable_types ON loanable_type_details.id = community_loanable_types.loanable_type_id
                    WHERE communities.id = community_loanable_types.community_id
                    AND name = loanables.type
                )
                AND approved_at IS NOT NULL
                AND suspended_at IS NULL
                AND communities.deleted_at IS NULL
            )
        )
        AND owners.deleted_at IS NULL
    )
    AND loanables.deleted_at IS NULL
SQL
        );
    }

    public function down(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("published");
        });
    }
};
