<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedInteger("extension_duration_in_minutes")
                ->nullable();
        });

        $maxExtensionQuery = DB::table("extensions")
            ->select("loan_id", DB::raw("MAX(new_duration) as max_duration"))
            ->groupBy("loan_id");

        DB::table("loans")
            ->joinSub(
                (clone $maxExtensionQuery)->where("status", "completed"),
                "completed_extensions",
                "completed_extensions.loan_id",
                "=",
                "loans.id"
            )
            ->updateFrom([
                "duration_in_minutes" => DB::raw(
                    "completed_extensions.max_duration"
                ),
            ]);

        DB::table("loans")
            ->joinSub(
                (clone $maxExtensionQuery)->where("status", "in_process"),
                "ongoing_extensions",
                "ongoing_extensions.loan_id",
                "=",
                "loans.id"
            )
            ->where("loans.status", \App\Enums\LoanStatus::IN_PROCESS_STATES)
            ->updateFrom([
                "extension_duration_in_minutes" => DB::raw(
                    "ongoing_extensions.max_duration"
                ),
            ]);

        Schema::drop("extensions");
    }
};
