<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->text("reason")
                ->nullable()
                ->change();

            $table
                ->enum("alternative_to", [
                    "car",
                    "public_transit",
                    "delivery",
                    "bike",
                    "walking",
                    "other",
                ])
                ->nullable();

            $table->text("alternative_to_other")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("alternative_to");
            $table->dropColumn("alternative_to_other");
        });
    }
};
