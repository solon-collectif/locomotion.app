<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dateTimeTz("accepted_at")->nullable();
            $table->dateTimeTz("rejected_at")->nullable();
        });

        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("intentions")
                ->join("loans", "loans.id", "=", "intentions.loan_id")
                ->join("loanables", "loans.loanable_id", "=", "loanables.id")

                ->join(
                    "loanable_user_roles as owners",
                    fn(\Illuminate\Database\Query\JoinClause $join) => $join
                        ->on("owners.loanable_id", "=", "loanables.id")
                        ->where(
                            "owners.role",
                            "=",
                            \App\Enums\LoanableUserRoles::Owner
                        )
                )
                ->select(
                    "loan_id",
                    "owners.user_id",
                    "message_for_borrower",
                    "executed_at"
                )
                ->where("message_for_borrower", "!=", "")
                ->whereNotNull("message_for_borrower")
                ->whereNotNull("executed_at")
        );

        DB::table("loans")
            ->join("intentions", "loans.id", "=", "intentions.loan_id")
            ->where("intentions.status", "canceled")
            ->updateFrom([
                "rejected_at" => DB::raw("intentions.executed_at"),
                "canceled_at" => null,
            ]);
        DB::table("loans")
            ->join("intentions", "loans.id", "=", "intentions.loan_id")
            ->where("loans.status", "canceled")
            ->where("intentions.status", "in_process")
            ->whereNull("rejected_at")
            ->updateFrom([
                "rejected_at" => DB::raw("loans.canceled_at"),
                "canceled_at" => null,
            ]);

        DB::table("loans")
            ->join("intentions", "loans.id", "=", "intentions.loan_id")
            ->where("intentions.status", "completed")
            ->updateFrom([
                "accepted_at" => DB::raw("intentions.executed_at"),
            ]);

        Schema::drop("intentions");
    }
};
