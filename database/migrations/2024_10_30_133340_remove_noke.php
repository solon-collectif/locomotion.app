<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("noke_id");
        });

        Schema::table("padlocks", function (Blueprint $table) {
            $table->dropColumn("noke_group_id");
        });

        Schema::table("communities", function (Blueprint $table) {
            $table->dropColumn("uses_noke");
        });
    }

    public function down(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->string("noke_id")->nullable();
        });

        Schema::table("padlocks", function (Blueprint $table) {
            $table->unsignedInteger("noke_group_id")->nullable();
        });

        Schema::table("communities", function (Blueprint $table) {
            $table->boolean("uses_noke")->default(false);
        });
    }
};
