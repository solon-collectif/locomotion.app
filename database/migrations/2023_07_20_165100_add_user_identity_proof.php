<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up(): void
    {
        // All existing proofs where specifically residency proofs.
        \App\Models\File::where("field", "proof")->update([
            "field" => "residency_proof",
        ]);
    }

    public function down(): void
    {
        \App\Models\File::where("field", "residency_proof")->update([
            "field" => "proof",
        ]);
    }
};
