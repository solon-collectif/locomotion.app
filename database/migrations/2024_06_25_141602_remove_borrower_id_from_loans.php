<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->dropColumn("borrower_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->unsignedBigInteger("borrower_id")
                ->nullable(false)
                ->change();

            $table
                ->foreign("borrower_id")
                ->references("id")
                ->on("borrowers")
                ->onDelete("cascade");
        });
    }
};
