<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dateTimeTz("blocking_until")->nullable();
        });
    }

    public function down(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dropColumn("blocking_until");
        });
    }
};
