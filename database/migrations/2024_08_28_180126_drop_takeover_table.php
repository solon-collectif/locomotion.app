<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->unsignedInteger("mileage_start")->nullable();
        });

        DB::table("loans")
            ->join("takeovers", "loans.id", "=", "takeovers.loan_id")
            ->where("takeovers.status", "completed")
            ->whereNotNull("takeovers.mileage_beginning")
            ->updateFrom([
                "mileage_start" => DB::raw("takeovers.mileage_beginning"),
            ]);

        DB::table("loan_comments")->insertUsing(
            ["loan_id", "author_id", "text", "created_at"],
            DB::table("takeovers")
                ->join("loans", "loans.id", "=", "takeovers.loan_id")
                ->select(
                    "loan_id",
                    "loans.borrower_user_id",
                    "comments_on_contestation",
                    "contested_at"
                )
                ->where("comments_on_contestation", "!=", "")
                ->whereNotNull("comments_on_contestation")
        );

        DB::table("images")
            ->join("takeovers", "takeovers.id", "=", "images.imageable_id")
            ->where("imageable_type", "takeover")
            ->updateFrom([
                "imageable_type" => "loan",
                "field" => "mileage_start",
                "imageable_id" => DB::raw("takeovers.loan_id"),
            ]);

        Schema::drop("takeovers");
    }
};
