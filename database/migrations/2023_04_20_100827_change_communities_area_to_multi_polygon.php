<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up(): void
    {
        DB::statement(
            "ALTER TABLE communities ALTER COLUMN area TYPE GEOGRAPHY(MultiPolygon, 4326) using ST_Multi(area::geometry)"
        );
    }

    public function down(): void
    {
        DB::statement(
            "ALTER TABLE communities ALTER COLUMN area TYPE GEOGRAPHY(Polygon, 4326) using ST_GeometryN(area::geometry, 1)"
        );
    }
};
