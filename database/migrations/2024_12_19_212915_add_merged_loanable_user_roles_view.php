<?php

use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up(): void
    {
        DB::statement(
            <<<SQL
create or replace view merged_loanable_user_roles as
    select loanable_user_roles.*, coalesce(loanables.id, ressource_id) as loanable_id
    from loanable_user_roles
        left join libraries on ressource_id = libraries.id and ressource_type = 'library'
        left join loanables on libraries.id = loanables.library_id
    where ressource_type = 'loanable' or loanables.id is not null;
SQL
        );
    }

    public function down(): void
    {
        DB::statement(
            <<<SQL
drop view merged_loanable_user_roles;
SQL
        );
    }
};
