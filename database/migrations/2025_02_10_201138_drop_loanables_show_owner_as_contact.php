<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        DB::table("loanable_user_roles")
            ->join(
                "loanables",
                "loanable_user_roles.ressource_id",
                "=",
                "loanables.id"
            )
            ->where("loanable_user_roles.ressource_type", "loanable")
            ->where("loanable_user_roles.role", "owner")
            ->updateFrom([
                "show_as_contact" => DB::raw("loanables.show_owner_as_contact"),
            ]);

        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("show_owner_as_contact");
        });
    }
};
