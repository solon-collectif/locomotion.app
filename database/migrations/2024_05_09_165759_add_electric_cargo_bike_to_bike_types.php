<?php

use App\Helpers\MigrationHelper;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    public function up()
    {
        MigrationHelper::changeEnumOptions("bikes", "bike_type", [
            "regular",
            "cargo",
            "cargo_electric",
            "electric",
            "fixed_wheel",
        ]);
    }

    public function down()
    {
        DB::table("bikes")
            ->where("bike_type", "cargo_electric")
            ->update([
                "bike_type" => "cargo",
            ]);

        MigrationHelper::changeEnumOptions("bikes", "bike_type", [
            "regular",
            "cargo",
            "electric",
            "fixed_wheel",
        ]);
    }
};
