<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->bigInteger("assignee_id")->nullable();

            $table
                ->foreign("assignee_id")
                ->references("id")
                ->on("users")
                ->nullOnDelete();
        });
    }

    public function down(): void
    {
        Schema::table("incidents", function (Blueprint $table) {
            $table->dropColumn("assignee_id");
        });
    }
};
