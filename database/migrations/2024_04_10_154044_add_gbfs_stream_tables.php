<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("gbfs_datasets", function (Blueprint $table) {
            $table->string("name")->primary();
            $table->text("notes")->nullable();
        });

        Schema::create("gbfs_dataset_communities", function (Blueprint $table) {
            $table->id();

            $table->string("gbfs_dataset_name");
            $table->bigInteger("community_id");

            $table
                ->foreign("gbfs_dataset_name")
                ->references("name")
                ->on("gbfs_datasets")
                ->onDelete("cascade");

            $table
                ->foreign("community_id")
                ->references("id")
                ->on("communities")
                ->onDelete("cascade");
        });

        Schema::table("loanables", function (Blueprint $table) {
            $table->boolean("shared_publicly")->default(false);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("gbfs_dataset_communities");
        Schema::dropIfExists("gbfs_datasets");
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("shared_publicly");
        });
    }
};
