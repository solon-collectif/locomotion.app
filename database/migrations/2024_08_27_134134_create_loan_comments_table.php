<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("loan_comments", function (Blueprint $table) {
            $table->id();
            $table
                ->foreignId("loan_id")
                ->references("id")
                ->on("loans")
                ->onDelete("cascade");
            $table
                ->foreignId("author_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");
            $table->text("text");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("loan_comments");
    }
};
