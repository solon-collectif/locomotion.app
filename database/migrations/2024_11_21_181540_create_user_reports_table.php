<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("user_publishable_reports", function (Blueprint $table) {
            $table->id();
            $table
                ->foreignId("user_id")
                ->constrained("users")
                ->onDelete("cascade");
            $table
                ->foreignId("publishable_report_id")
                ->constrained("publishable_reports")
                ->onDelete("cascade");
            $table->string("path")->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("user_publishable_reports");
    }
};
