<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->text("custom_registration_approved_email")->nullable();
        });
    }
};
