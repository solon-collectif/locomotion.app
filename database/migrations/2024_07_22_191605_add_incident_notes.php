<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create("incident_notes", function (Blueprint $table) {
            $table->id();

            $table->bigInteger("incident_id");
            $table
                ->foreign("incident_id")
                ->references("id")
                ->on("incidents")
                ->cascadeOnDelete();

            $table->bigInteger("author_id");
            $table
                ->foreign("author_id")
                ->references("id")
                ->on("users")
                ->noActionOnDelete();

            $table->text("text");

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists("incident_notes");
    }
};
