<?php

use App\Models\Pivots\CommunityUser;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("community_user", function (Blueprint $table) {
            $table
                ->enum("join_method", ["geo", "manual", "invitation"])
                ->default("manual");
            $table->bigInteger("approver_id")->nullable();
            $table->text("approval_note")->nullable();

            $table
                ->foreign("approver_id")
                ->references("id")
                ->on("users");
        });

        CommunityUser::leftJoin(
            "users",
            "users.id",
            "=",
            "community_user.user_id"
        )
            ->leftJoin(
                "communities",
                "communities.id",
                "=",
                "community_user.community_id"
            )
            ->whereNotNull("users.address_position")
            ->whereNotNull("communities.area")
            ->where("communities.type", "borough")
            ->whereRaw(
                "public.ST_Contains(communities.area::geometry, users.address_position::geometry)"
            )
            ->update(["join_method" => "geo"]);
    }

    public function down(): void
    {
        Schema::table("community_user", function (Blueprint $table) {
            $table->dropForeign("community_user_approver_id_foreign");
            $table->dropColumn("join_method");
            $table->dropColumn("approver_id");
            $table->dropColumn("approval_note");
        });
    }
};
