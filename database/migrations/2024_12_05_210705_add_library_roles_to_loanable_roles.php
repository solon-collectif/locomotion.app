<?php

use App\Helpers\MigrationHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanable_user_roles", function (Blueprint $table) {
            MigrationHelper::changeEnumOptions("loanable_user_roles", "role", [
                "owner",
                "manager",
                "coowner",
                "trusted_borrower",
            ]);
            $table
                ->enum("ressource_type", ["loanable", "library"])
                ->default("loanable");
            $table->dropForeign("coowners_loanable_id_foreign");
            $table->renameColumn("loanable_id", "ressource_id");

            $table->unique([
                "ressource_type",
                "user_id",
                "ressource_id",
                "role",
            ]);
        });
    }

    public function down(): void
    {
        Schema::table("loanable_user_roles", function (Blueprint $table) {
            MigrationHelper::changeEnumOptions("loanable_user_roles", "role", [
                "owner",
                "coowner",
                "trusted_borrower",
            ]);
            $table->dropUnique(["ressource_type", "user_id", "ressource_id"]);
            $table->dropColumn("ressource_type");
            $table->renameColumn("ressource_id", "loanable_id");
        });
    }
};
