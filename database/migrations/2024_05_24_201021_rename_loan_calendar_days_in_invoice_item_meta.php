<?php

use App\Models\BillItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $invoiceItems = BillItem::where(
            "meta",
            "like",
            "%loanCalendarDays%"
        )->get();

        foreach ($invoiceItems as $invoiceItem) {
            $itemMeta = $invoiceItem->meta;

            if (array_key_exists("loanCalendarDays", $itemMeta)) {
                $itemMeta["calendarDays"] = $itemMeta["loanCalendarDays"];
                unset($itemMeta["loanCalendarDays"]);

                $invoiceItem->meta = $itemMeta;

                $invoiceItem->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $invoiceItems = BillItem::where(
            "meta",
            "like",
            "%calendarDays%"
        )->get();

        foreach ($invoiceItems as $invoiceItem) {
            $itemMeta = $invoiceItem->meta;

            if (array_key_exists("calendarDays", $itemMeta)) {
                $itemMeta["loanCalendarDays"] = $itemMeta["calendarDays"];
                unset($itemMeta["calendarDays"]);

                $invoiceItem->meta = $itemMeta;

                $invoiceItem->save();
            }
        }
    }
};
