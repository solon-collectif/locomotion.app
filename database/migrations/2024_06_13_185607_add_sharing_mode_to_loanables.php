<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table
                ->enum("sharing_mode", ["on_demand", "self_service", "hybrid"])
                ->default("on_demand");
        });

        DB::table("loanables")
            ->where("is_self_service", true)
            ->update([
                "sharing_mode" => "self_service",
            ]);
    }

    public function down(): void
    {
        Schema::table("loanables", function (Blueprint $table) {
            $table->dropColumn("sharing_mode");
        });
    }
};
