--
-- PostgreSQL database dump
--

-- Dumped from database version 12.20 (Debian 12.20-1.pgdg120+1)
-- Dumped by pg_dump version 15.10 (Debian 15.10-0+deb12u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

-- *not* creating schema, since initdb creates it


--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- Name: actions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.actions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loanables_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loanables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bikes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bikes (
    id bigint DEFAULT nextval('public.loanables_id_seq'::regclass) NOT NULL,
    model character varying(255),
    size character varying(255),
    bike_type character varying(255) DEFAULT 'regular'::character varying,
    CONSTRAINT bikes_bike_type_check CHECK (((bike_type)::text = ANY (ARRAY['regular'::text, 'cargo'::text, 'cargo_electric'::text, 'electric'::text, 'fixed_wheel'::text]))),
    CONSTRAINT bikes_size_check CHECK (((size)::text = ANY ((ARRAY['big'::character varying, 'medium'::character varying, 'small'::character varying, 'kid'::character varying])::text[])))
);


--
-- Name: bikes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bikes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bikes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bikes_id_seq OWNED BY public.bikes.id;


--
-- Name: bill_items; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bill_items (
    id bigint NOT NULL,
    label character varying(255) NOT NULL,
    amount numeric(8,2) NOT NULL,
    invoice_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    taxes_tps numeric(8,2) NOT NULL,
    taxes_tvq numeric(8,2) NOT NULL,
    meta jsonb,
    item_type character varying(255) NOT NULL,
    contribution_community_id bigint
);


--
-- Name: bill_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bill_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bill_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bill_items_id_seq OWNED BY public.bill_items.id;


--
-- Name: borrowers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.borrowers (
    id bigint NOT NULL,
    drivers_license_number character varying(255),
    user_id bigint NOT NULL,
    submitted_at timestamp(0) without time zone,
    approved_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    suspended_at timestamp(0) without time zone,
    has_not_been_sued_last_ten_years boolean DEFAULT false NOT NULL
);


--
-- Name: borrowers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.borrowers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: borrowers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.borrowers_id_seq OWNED BY public.borrowers.id;


--
-- Name: cache; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cache (
    key character varying(255) NOT NULL,
    value text NOT NULL,
    expiration integer NOT NULL
);


--
-- Name: cars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cars (
    id bigint DEFAULT nextval('public.loanables_id_seq'::regclass) NOT NULL,
    brand character varying(255),
    model character varying(255),
    year_of_circulation integer,
    transmission_mode character varying(255),
    engine character varying(255),
    plate_number character varying(255),
    papers_location character varying(255),
    insurer character varying(255),
    has_informed_insurer boolean DEFAULT false NOT NULL,
    pricing_category character varying(255) DEFAULT 'small'::character varying,
    value_category character varying(255),
    has_onboard_notebook boolean DEFAULT false NOT NULL,
    has_report_in_notebook boolean DEFAULT false NOT NULL,
    CONSTRAINT cars_engine_check CHECK (((engine)::text = ANY ((ARRAY['fuel'::character varying, 'diesel'::character varying, 'electric'::character varying, 'hybrid'::character varying])::text[]))),
    CONSTRAINT cars_papers_location_check CHECK (((papers_location)::text = ANY ((ARRAY['in_the_car'::character varying, 'to_request_with_car'::character varying])::text[]))),
    CONSTRAINT cars_pricing_category_check CHECK (((pricing_category)::text = ANY (ARRAY[('small'::character varying)::text, ('large'::character varying)::text, ('electric'::character varying)::text]))),
    CONSTRAINT cars_transmission_mode_check CHECK (((transmission_mode)::text = ANY ((ARRAY['automatic'::character varying, 'manual'::character varying])::text[]))),
    CONSTRAINT cars_value_category_check CHECK (((value_category)::text = ANY ((ARRAY['lte50k'::character varying, 'lte70k'::character varying, 'lte100k'::character varying])::text[])))
);


--
-- Name: cars_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cars_id_seq OWNED BY public.cars.id;


--
-- Name: communities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.communities (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    area public.geography(MultiPolygon,4326),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    type character varying(255) DEFAULT 'private'::character varying NOT NULL,
    long_description text DEFAULT ''::text NOT NULL,
    chat_group_url character varying(255),
    starting_guide_url character varying(255),
    requires_identity_proof boolean DEFAULT false NOT NULL,
    contact_email character varying(255),
    exempt_from_contributions boolean DEFAULT false NOT NULL,
    CONSTRAINT communities_type_check CHECK (((type)::text = ANY ((ARRAY['private'::character varying, 'neighborhood'::character varying, 'borough'::character varying])::text[])))
);


--
-- Name: communities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.communities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: communities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.communities_id_seq OWNED BY public.communities.id;


--
-- Name: community_loanable_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.community_loanable_types (
    community_id bigint NOT NULL,
    loanable_type_id bigint NOT NULL
);


--
-- Name: community_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.community_user (
    community_id bigint NOT NULL,
    user_id bigint NOT NULL,
    role character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id bigint NOT NULL,
    approved_at timestamp(0) without time zone,
    suspended_at timestamp(0) without time zone,
    proof_evaluation character varying(255) DEFAULT 'unevaluated'::character varying NOT NULL,
    join_method character varying(255) DEFAULT 'manual'::character varying NOT NULL,
    approver_id bigint,
    approval_note text,
    CONSTRAINT community_user_join_method_check CHECK (((join_method)::text = ANY ((ARRAY['geo'::character varying, 'manual'::character varying, 'invitation'::character varying])::text[]))),
    CONSTRAINT community_user_proof_evaluation_check CHECK (((proof_evaluation)::text = ANY ((ARRAY['unevaluated'::character varying, 'valid'::character varying, 'invalid'::character varying])::text[])))
);


--
-- Name: community_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.community_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: community_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.community_user_id_seq OWNED BY public.community_user.id;


--
-- Name: loanable_user_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loanable_user_roles (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    loanable_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    show_as_contact boolean DEFAULT false NOT NULL,
    title character varying(255),
    pays_loan_price boolean DEFAULT true NOT NULL,
    pays_loan_insurance boolean DEFAULT true NOT NULL,
    role character varying(255) DEFAULT 'coowner'::character varying NOT NULL,
    granted_by_user_id bigint,
    CONSTRAINT loanable_user_roles_role_check CHECK (((role)::text = ANY (ARRAY['owner'::text, 'coowner'::text, 'trusted_borrower'::text])))
);


--
-- Name: coowners_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.coowners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: coowners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.coowners_id_seq OWNED BY public.loanable_user_roles.id;


--
-- Name: exports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.exports (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    model character varying(255) NOT NULL,
    batch_id character varying(255),
    status character varying(255) DEFAULT 'not_started'::character varying NOT NULL,
    progress double precision DEFAULT '0'::double precision NOT NULL,
    started_at timestamp(0) without time zone,
    duration_in_seconds double precision,
    CONSTRAINT exports_status_check CHECK (((status)::text = ANY ((ARRAY['not_started'::character varying, 'in_process'::character varying, 'completed'::character varying, 'failed'::character varying, 'canceled'::character varying])::text[])))
);


--
-- Name: exports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.exports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: exports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.exports_id_seq OWNED BY public.exports.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: files; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.files (
    id bigint NOT NULL,
    fileable_type character varying(255),
    fileable_id integer,
    path character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    original_filename character varying(255) NOT NULL,
    filesize character varying(255) NOT NULL,
    field character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.files_id_seq OWNED BY public.files.id;


--
-- Name: gbfs_dataset_communities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gbfs_dataset_communities (
    id bigint NOT NULL,
    gbfs_dataset_name character varying(255) NOT NULL,
    community_id bigint NOT NULL
);


--
-- Name: gbfs_dataset_communities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gbfs_dataset_communities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: gbfs_dataset_communities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gbfs_dataset_communities_id_seq OWNED BY public.gbfs_dataset_communities.id;


--
-- Name: gbfs_datasets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gbfs_datasets (
    name character varying(255) NOT NULL,
    notes text
);


--
-- Name: google_accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.google_accounts (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    provider_user_id character varying(255) NOT NULL,
    provider character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: google_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.google_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: google_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.google_accounts_id_seq OWNED BY public.google_accounts.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.images (
    id bigint NOT NULL,
    imageable_type character varying(255),
    imageable_id integer,
    path character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    original_filename character varying(255) NOT NULL,
    field character varying(255),
    width integer NOT NULL,
    height integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    filesize character varying(255) NOT NULL,
    "order" integer,
    blur bytea
);


--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.images_id_seq OWNED BY public.images.id;


--
-- Name: incident_notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.incident_notes (
    id bigint NOT NULL,
    incident_id bigint NOT NULL,
    author_id bigint NOT NULL,
    text text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: incident_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.incident_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: incident_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.incident_notes_id_seq OWNED BY public.incident_notes.id;


--
-- Name: incident_notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.incident_notifications (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    incident_id bigint NOT NULL,
    level character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT incident_notifications_level_check CHECK (((level)::text = ANY ((ARRAY['all'::character varying, 'none'::character varying, 'resolved_only'::character varying])::text[])))
);


--
-- Name: incident_notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.incident_notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: incident_notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.incident_notifications_id_seq OWNED BY public.incident_notifications.id;


--
-- Name: incidents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.incidents (
    id bigint DEFAULT nextval('public.actions_id_seq'::regclass) NOT NULL,
    executed_at timestamp(0) without time zone,
    status character varying(255) DEFAULT 'in_process'::character varying NOT NULL,
    loan_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    comments_on_incident text NOT NULL,
    incident_type character varying(255) NOT NULL,
    reported_by_user_id bigint,
    resolved_by_user_id bigint,
    assignee_id bigint,
    loanable_id bigint NOT NULL,
    blocking_until timestamp(0) with time zone,
    show_details_to_blocked_borrowers boolean DEFAULT false NOT NULL,
    CONSTRAINT incidents_incident_type_check1 CHECK (((incident_type)::text = ANY ((ARRAY['accident'::character varying, 'small_incident'::character varying, 'general'::character varying])::text[]))),
    CONSTRAINT incidents_status_check CHECK (((status)::text = ANY ((ARRAY['in_process'::character varying, 'canceled'::character varying, 'completed'::character varying])::text[])))
);


--
-- Name: incidents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.incidents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: incidents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.incidents_id_seq OWNED BY public.incidents.id;


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.invitations (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    inviter_user_id bigint NOT NULL,
    community_id bigint NOT NULL,
    confirmation_code character varying(255) NOT NULL,
    reason text,
    message text,
    auto_approve boolean DEFAULT false NOT NULL,
    expires_at date,
    accepted_at timestamp(0) without time zone,
    refused_at timestamp(0) without time zone,
    validated_on_registration boolean DEFAULT false NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.invitations_id_seq OWNED BY public.invitations.id;


--
-- Name: invoices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.invoices (
    id bigint NOT NULL,
    period character varying(255) DEFAULT ''::character varying NOT NULL,
    user_id bigint NOT NULL,
    payment_method_id bigint,
    paid_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


--
-- Name: invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.invoices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.invoices_id_seq OWNED BY public.invoices.id;


--
-- Name: job_batches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.job_batches (
    id character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    total_jobs integer NOT NULL,
    pending_jobs integer NOT NULL,
    failed_jobs integer NOT NULL,
    failed_job_ids text NOT NULL,
    options text,
    cancelled_at integer,
    created_at integer NOT NULL,
    finished_at integer
);


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.jobs (
    id bigint NOT NULL,
    queue character varying(255) NOT NULL,
    payload text NOT NULL,
    attempts smallint NOT NULL,
    reserved_at integer,
    available_at integer NOT NULL,
    created_at integer NOT NULL
);


--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: loan_comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loan_comments (
    id bigint NOT NULL,
    loan_id bigint NOT NULL,
    author_id bigint NOT NULL,
    text text NOT NULL,
    deleted_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: loan_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loan_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loan_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loan_comments_id_seq OWNED BY public.loan_comments.id;


--
-- Name: loan_notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loan_notifications (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    loan_id bigint NOT NULL,
    level character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    last_seen timestamp(0) with time zone,
    CONSTRAINT loan_notifications_level_check CHECK (((level)::text = ANY ((ARRAY['all'::character varying, 'none'::character varying, 'messages_only'::character varying])::text[])))
);


--
-- Name: loan_notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loan_notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loan_notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loan_notifications_id_seq OWNED BY public.loan_notifications.id;


--
-- Name: loanable_type_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loanable_type_details (
    id bigint NOT NULL,
    name text NOT NULL
);


--
-- Name: loanable_type_details_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loanable_type_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loanable_type_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loanable_type_details_id_seq OWNED BY public.loanable_type_details.id;


--
-- Name: loanables; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loanables (
    id bigint DEFAULT nextval('public.loanables_id_seq'::regclass) NOT NULL,
    type character varying(255) NOT NULL,
    name character varying(255),
    "position" public.geography(Point,4326),
    location_description text,
    comments text,
    instructions text,
    availability_mode character varying(255) DEFAULT 'never'::character varying NOT NULL,
    availability_json text DEFAULT '[]'::text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    show_owner_as_contact boolean DEFAULT true NOT NULL,
    timezone character varying(255) DEFAULT 'America/Toronto'::character varying NOT NULL,
    published boolean DEFAULT false NOT NULL,
    shared_publicly boolean DEFAULT false NOT NULL,
    sharing_mode character varying(255) DEFAULT 'on_demand'::character varying NOT NULL,
    trusted_borrower_instructions text,
    CONSTRAINT loanables_availability_mode_check CHECK (((availability_mode)::text = ANY ((ARRAY['always'::character varying, 'never'::character varying])::text[]))),
    CONSTRAINT loanables_sharing_mode_check CHECK (((sharing_mode)::text = ANY ((ARRAY['on_demand'::character varying, 'self_service'::character varying, 'hybrid'::character varying])::text[]))),
    CONSTRAINT loanables_type_check CHECK (((type)::text = ANY ((ARRAY['car'::character varying, 'trailer'::character varying, 'bike'::character varying])::text[])))
);


--
-- Name: loanables_id_seq1; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loanables_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loanables_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loanables_id_seq1 OWNED BY public.loanables.id;


--
-- Name: loans; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loans (
    id bigint NOT NULL,
    departure_at timestamp(0) without time zone NOT NULL,
    duration_in_minutes integer NOT NULL,
    loanable_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    estimated_distance integer,
    reason text,
    community_id bigint,
    platform_tip numeric(8,2),
    meta jsonb DEFAULT '{}'::json NOT NULL,
    canceled_at timestamp(0) without time zone,
    status character varying(255),
    actual_return_at timestamp(0) without time zone,
    owner_validated_at timestamp(0) without time zone,
    borrower_validated_at timestamp(0) without time zone,
    alternative_to character varying(255),
    alternative_to_other text,
    borrower_user_id bigint,
    is_self_service boolean DEFAULT false NOT NULL,
    accepted_at timestamp(0) with time zone,
    rejected_at timestamp(0) with time zone,
    prepaid_at timestamp(0) with time zone,
    mileage_start integer,
    mileage_end integer,
    expenses_amount numeric(8,2),
    owner_invoice_id bigint,
    borrower_invoice_id bigint,
    paid_at timestamp(0) with time zone,
    extension_duration_in_minutes integer,
    attempt_autocomplete_at timestamp(0) with time zone,
    CONSTRAINT loans_alternative_to_check CHECK (((alternative_to)::text = ANY ((ARRAY['car'::character varying, 'public_transit'::character varying, 'delivery'::character varying, 'bike'::character varying, 'walking'::character varying, 'other'::character varying])::text[]))),
    CONSTRAINT loans_status_check CHECK (((status)::text = ANY (ARRAY['requested'::text, 'accepted'::text, 'confirmed'::text, 'ongoing'::text, 'ended'::text, 'validated'::text, 'completed'::text, 'canceled'::text, 'rejected'::text])))
);


--
-- Name: loans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.loans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: loans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.loans_id_seq OWNED BY public.loans.id;


--
-- Name: mailing_list_integrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mailing_list_integrations (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    community_id bigint NOT NULL,
    created_by_user_id bigint NOT NULL,
    provider character varying(255) NOT NULL,
    api_key character varying(255) NOT NULL,
    list_id character varying(255) NOT NULL,
    list_name character varying(255),
    status character varying(255) NOT NULL,
    consecutive_error_count integer DEFAULT 0 NOT NULL,
    last_error text,
    community_editable boolean NOT NULL,
    tag character varying(255),
    CONSTRAINT mailing_list_integrations_provider_check CHECK (((provider)::text = ANY ((ARRAY['brevo'::character varying, 'mailchimp'::character varying])::text[]))),
    CONSTRAINT mailing_list_integrations_status_check CHECK (((status)::text = ANY ((ARRAY['active'::character varying, 'suspended'::character varying, 'synchronizing'::character varying])::text[])))
);


--
-- Name: mailing_list_integrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mailing_list_integrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mailing_list_integrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mailing_list_integrations_id_seq OWNED BY public.mailing_list_integrations.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_access_tokens (
    id character varying(100) NOT NULL,
    user_id bigint,
    client_id character varying(100) NOT NULL,
    name character varying(255),
    scopes text,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone
);


--
-- Name: oauth_auth_codes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_auth_codes (
    id character varying(100) NOT NULL,
    user_id bigint NOT NULL,
    client_id character varying(100) NOT NULL,
    scopes text,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_clients (
    id character varying(100) NOT NULL,
    user_id bigint,
    name character varying(255) NOT NULL,
    secret character varying(100),
    provider character varying(255),
    redirect text NOT NULL,
    personal_access_client boolean NOT NULL,
    password_client boolean NOT NULL,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: oauth_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_clients_id_seq OWNED BY public.oauth_clients.id;


--
-- Name: oauth_personal_access_clients; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_personal_access_clients (
    id bigint NOT NULL,
    client_id character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.oauth_personal_access_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.oauth_personal_access_clients_id_seq OWNED BY public.oauth_personal_access_clients.id;


--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_refresh_tokens (
    id character varying(100) NOT NULL,
    access_token_id character varying(100) NOT NULL,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


--
-- Name: payment_methods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.payment_methods (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    external_id character varying(255) NOT NULL,
    four_last_digits integer,
    credit_card_type character varying(255),
    user_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    is_default boolean DEFAULT false NOT NULL,
    country character(2)
);


--
-- Name: payment_methods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.payment_methods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_methods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.payment_methods_id_seq OWNED BY public.payment_methods.id;


--
-- Name: payouts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.payouts (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    invoice_id bigint NOT NULL,
    kiwili_expense_id character varying(255),
    amount numeric(8,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


--
-- Name: payouts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.payouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.payouts_id_seq OWNED BY public.payouts.id;


--
-- Name: pricing_loanable_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pricing_loanable_types (
    pricing_id bigint NOT NULL,
    pricing_loanable_type character varying(255) NOT NULL,
    CONSTRAINT pricing_loanable_types_pricing_loanable_type_check CHECK (((pricing_loanable_type)::text = ANY ((ARRAY['car_small'::character varying, 'car_large'::character varying, 'car_electric'::character varying, 'trailer'::character varying, 'bike_regular'::character varying, 'bike_electric'::character varying, 'bike_cargo_regular'::character varying, 'bike_cargo_electric'::character varying])::text[])))
);


--
-- Name: pricings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pricings (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    rule text NOT NULL,
    community_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    pricing_type character varying(255) DEFAULT 'legacy'::character varying NOT NULL,
    loanable_ownership_type character varying(255) DEFAULT 'all'::character varying NOT NULL,
    yearly_target_per_user numeric(8,2),
    is_mandatory boolean,
    description text,
    start_date date NOT NULL,
    end_date date,
    CONSTRAINT pricings_loanable_ownership_type_check CHECK (((loanable_ownership_type)::text = ANY ((ARRAY['all'::character varying, 'fleet'::character varying, 'non_fleet'::character varying])::text[]))),
    CONSTRAINT pricings_pricing_type_check CHECK (((pricing_type)::text = ANY ((ARRAY['legacy'::character varying, 'price'::character varying, 'insurance'::character varying, 'contribution'::character varying])::text[])))
);


--
-- Name: pricings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pricings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pricings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pricings_id_seq OWNED BY public.pricings.id;


--
-- Name: publishable_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.publishable_reports (
    id bigint NOT NULL,
    status character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    report_key character varying(255) NOT NULL,
    batch_id character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT publishable_reports_status_check CHECK (((status)::text = ANY ((ARRAY['generating'::character varying, 'generated'::character varying, 'failed'::character varying, 'published'::character varying])::text[]))),
    CONSTRAINT publishable_reports_type_check CHECK (((type)::text = 'yearly-owner-income'::text))
);


--
-- Name: publishable_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.publishable_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: publishable_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.publishable_reports_id_seq OWNED BY public.publishable_reports.id;


--
-- Name: subscription_loanable_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscription_loanable_types (
    subscription_id bigint NOT NULL,
    pricing_loanable_type character varying(255) NOT NULL,
    CONSTRAINT subscription_loanable_types_pricing_loanable_type_check CHECK (((pricing_loanable_type)::text = ANY ((ARRAY['car_small'::character varying, 'car_large'::character varying, 'car_electric'::character varying, 'trailer'::character varying, 'bike_regular'::character varying, 'bike_electric'::character varying, 'bike_cargo_regular'::character varying, 'bike_cargo_electric'::character varying])::text[])))
);


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscriptions (
    id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    start_date timestamp(0) without time zone NOT NULL,
    end_date timestamp(0) without time zone NOT NULL,
    type character varying(255) NOT NULL,
    reason text,
    community_user_id bigint NOT NULL,
    granted_by_user_id bigint NOT NULL,
    invoice_id bigint,
    CONSTRAINT subscriptions_type_check CHECK (((type)::text = ANY ((ARRAY['paid'::character varying, 'granted'::character varying])::text[])))
);


--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subscriptions_id_seq OWNED BY public.subscriptions.id;


--
-- Name: trailers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.trailers (
    id bigint DEFAULT nextval('public.loanables_id_seq'::regclass) NOT NULL,
    maximum_charge character varying(255),
    dimensions character varying(255)
);


--
-- Name: trailers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.trailers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: trailers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.trailers_id_seq OWNED BY public.trailers.id;


--
-- Name: user_publishable_reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_publishable_reports (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    publishable_report_id bigint NOT NULL,
    path character varying(255)
);


--
-- Name: user_publishable_reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_publishable_reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_publishable_reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_publishable_reports_id_seq OWNED BY public.user_publishable_reports.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    last_name character varying(255) DEFAULT ''::character varying NOT NULL,
    email public.citext NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    description text,
    date_of_birth date,
    address text DEFAULT ''::text NOT NULL,
    postal_code character varying(255) DEFAULT ''::character varying NOT NULL,
    phone character varying(255) DEFAULT ''::character varying NOT NULL,
    is_smart_phone boolean DEFAULT false NOT NULL,
    other_phone character varying(255) DEFAULT ''::character varying NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role character varying(255),
    balance numeric(8,2) DEFAULT '0'::numeric NOT NULL,
    deleted_at timestamp(0) without time zone,
    accept_conditions boolean DEFAULT false NOT NULL,
    meta jsonb DEFAULT '{}'::json NOT NULL,
    conditions_accepted_at timestamp(0) without time zone,
    bank_account_number character varying(255),
    bank_institution_number character varying(255),
    bank_transit_number character varying(255),
    address_position public.geography(Point,4326),
    data_deleted_at timestamp(0) without time zone,
    is_fleet boolean DEFAULT false NOT NULL,
    bank_info_updated_at timestamp(0) with time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: bill_items id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill_items ALTER COLUMN id SET DEFAULT nextval('public.bill_items_id_seq'::regclass);


--
-- Name: borrowers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.borrowers ALTER COLUMN id SET DEFAULT nextval('public.borrowers_id_seq'::regclass);


--
-- Name: communities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.communities ALTER COLUMN id SET DEFAULT nextval('public.communities_id_seq'::regclass);


--
-- Name: community_user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_user ALTER COLUMN id SET DEFAULT nextval('public.community_user_id_seq'::regclass);


--
-- Name: exports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exports ALTER COLUMN id SET DEFAULT nextval('public.exports_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: files id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.files ALTER COLUMN id SET DEFAULT nextval('public.files_id_seq'::regclass);


--
-- Name: gbfs_dataset_communities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gbfs_dataset_communities ALTER COLUMN id SET DEFAULT nextval('public.gbfs_dataset_communities_id_seq'::regclass);


--
-- Name: google_accounts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.google_accounts ALTER COLUMN id SET DEFAULT nextval('public.google_accounts_id_seq'::regclass);


--
-- Name: images id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images ALTER COLUMN id SET DEFAULT nextval('public.images_id_seq'::regclass);


--
-- Name: incident_notes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notes ALTER COLUMN id SET DEFAULT nextval('public.incident_notes_id_seq'::regclass);


--
-- Name: incident_notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notifications ALTER COLUMN id SET DEFAULT nextval('public.incident_notifications_id_seq'::regclass);


--
-- Name: invitations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invitations ALTER COLUMN id SET DEFAULT nextval('public.invitations_id_seq'::regclass);


--
-- Name: invoices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices ALTER COLUMN id SET DEFAULT nextval('public.invoices_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: loan_comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_comments ALTER COLUMN id SET DEFAULT nextval('public.loan_comments_id_seq'::regclass);


--
-- Name: loan_notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_notifications ALTER COLUMN id SET DEFAULT nextval('public.loan_notifications_id_seq'::regclass);


--
-- Name: loanable_type_details id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_type_details ALTER COLUMN id SET DEFAULT nextval('public.loanable_type_details_id_seq'::regclass);


--
-- Name: loanable_user_roles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_user_roles ALTER COLUMN id SET DEFAULT nextval('public.coowners_id_seq'::regclass);


--
-- Name: loans id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loans ALTER COLUMN id SET DEFAULT nextval('public.loans_id_seq'::regclass);


--
-- Name: mailing_list_integrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_list_integrations ALTER COLUMN id SET DEFAULT nextval('public.mailing_list_integrations_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: oauth_personal_access_clients id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_personal_access_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_personal_access_clients_id_seq'::regclass);


--
-- Name: payment_methods id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payment_methods ALTER COLUMN id SET DEFAULT nextval('public.payment_methods_id_seq'::regclass);


--
-- Name: payouts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts ALTER COLUMN id SET DEFAULT nextval('public.payouts_id_seq'::regclass);


--
-- Name: pricings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pricings ALTER COLUMN id SET DEFAULT nextval('public.pricings_id_seq'::regclass);


--
-- Name: publishable_reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishable_reports ALTER COLUMN id SET DEFAULT nextval('public.publishable_reports_id_seq'::regclass);


--
-- Name: subscriptions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions ALTER COLUMN id SET DEFAULT nextval('public.subscriptions_id_seq'::regclass);


--
-- Name: user_publishable_reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_publishable_reports ALTER COLUMN id SET DEFAULT nextval('public.user_publishable_reports_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: bikes bikes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bikes
    ADD CONSTRAINT bikes_pkey PRIMARY KEY (id);


--
-- Name: bill_items bill_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill_items
    ADD CONSTRAINT bill_items_pkey PRIMARY KEY (id);


--
-- Name: borrowers borrowers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.borrowers
    ADD CONSTRAINT borrowers_pkey PRIMARY KEY (id);


--
-- Name: cache cache_key_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cache
    ADD CONSTRAINT cache_key_unique UNIQUE (key);


--
-- Name: cars cars_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- Name: communities communities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.communities
    ADD CONSTRAINT communities_pkey PRIMARY KEY (id);


--
-- Name: community_user community_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_user
    ADD CONSTRAINT community_user_pkey PRIMARY KEY (id);


--
-- Name: loanable_user_roles coowners_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_user_roles
    ADD CONSTRAINT coowners_pkey PRIMARY KEY (id);


--
-- Name: exports exports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exports
    ADD CONSTRAINT exports_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: files files_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: gbfs_dataset_communities gbfs_dataset_communities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gbfs_dataset_communities
    ADD CONSTRAINT gbfs_dataset_communities_pkey PRIMARY KEY (id);


--
-- Name: gbfs_datasets gbfs_datasets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gbfs_datasets
    ADD CONSTRAINT gbfs_datasets_pkey PRIMARY KEY (name);


--
-- Name: google_accounts google_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.google_accounts
    ADD CONSTRAINT google_accounts_pkey PRIMARY KEY (id);


--
-- Name: images images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: incident_notes incident_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notes
    ADD CONSTRAINT incident_notes_pkey PRIMARY KEY (id);


--
-- Name: incident_notifications incident_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notifications
    ADD CONSTRAINT incident_notifications_pkey PRIMARY KEY (id);


--
-- Name: incidents incidents_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_pkey PRIMARY KEY (id);


--
-- Name: invitations invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_pkey PRIMARY KEY (id);


--
-- Name: invoices invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: job_batches job_batches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_batches
    ADD CONSTRAINT job_batches_pkey PRIMARY KEY (id);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: loan_comments loan_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_comments
    ADD CONSTRAINT loan_comments_pkey PRIMARY KEY (id);


--
-- Name: loan_notifications loan_notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_notifications
    ADD CONSTRAINT loan_notifications_pkey PRIMARY KEY (id);


--
-- Name: loanable_type_details loanable_type_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_type_details
    ADD CONSTRAINT loanable_type_details_pkey PRIMARY KEY (id);


--
-- Name: loanables loanables_id_type_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanables
    ADD CONSTRAINT loanables_id_type_unique UNIQUE (id, type);


--
-- Name: loanables loanables_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanables
    ADD CONSTRAINT loanables_pkey PRIMARY KEY (id);


--
-- Name: loans loans_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loans
    ADD CONSTRAINT loans_pkey PRIMARY KEY (id);


--
-- Name: mailing_list_integrations mailing_list_integrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_list_integrations
    ADD CONSTRAINT mailing_list_integrations_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_auth_codes oauth_auth_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (id);


--
-- Name: oauth_clients oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_personal_access_clients oauth_personal_access_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_personal_access_clients
    ADD CONSTRAINT oauth_personal_access_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_refresh_tokens oauth_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: payment_methods payment_methods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payment_methods
    ADD CONSTRAINT payment_methods_pkey PRIMARY KEY (id);


--
-- Name: payouts payouts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT payouts_pkey PRIMARY KEY (id);


--
-- Name: pricing_loanable_types pricing_loanable_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pricing_loanable_types
    ADD CONSTRAINT pricing_loanable_types_pkey PRIMARY KEY (pricing_id, pricing_loanable_type);


--
-- Name: pricings pricings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pricings
    ADD CONSTRAINT pricings_pkey PRIMARY KEY (id);


--
-- Name: publishable_reports publishable_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishable_reports
    ADD CONSTRAINT publishable_reports_pkey PRIMARY KEY (id);


--
-- Name: publishable_reports publishable_reports_report_key_type_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishable_reports
    ADD CONSTRAINT publishable_reports_report_key_type_unique UNIQUE (report_key, type);


--
-- Name: subscription_loanable_types subscription_loanable_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_loanable_types
    ADD CONSTRAINT subscription_loanable_types_pkey PRIMARY KEY (subscription_id, pricing_loanable_type);


--
-- Name: subscriptions subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: trailers trailers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trailers
    ADD CONSTRAINT trailers_pkey PRIMARY KEY (id);


--
-- Name: user_publishable_reports user_publishable_reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_publishable_reports
    ADD CONSTRAINT user_publishable_reports_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: bill_items_invoice_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX bill_items_invoice_id_index ON public.bill_items USING btree (invoice_id);


--
-- Name: borrowers_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX borrowers_user_id_index ON public.borrowers USING btree (user_id);


--
-- Name: community_user_community_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX community_user_community_id_index ON public.community_user USING btree (community_id);


--
-- Name: community_user_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX community_user_user_id_index ON public.community_user USING btree (user_id);


--
-- Name: files_path_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX files_path_index ON public.files USING btree (path);


--
-- Name: files_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX files_user_id_index ON public.files USING btree (user_id);


--
-- Name: images_path_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX images_path_index ON public.images USING btree (path);


--
-- Name: incidents_loan_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX incidents_loan_id_index ON public.incidents USING btree (loan_id);


--
-- Name: invitations_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invitations_email_index ON public.invitations USING btree (email);


--
-- Name: invoices_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invoices_user_id_index ON public.invoices USING btree (user_id);


--
-- Name: jobs_queue_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_queue_index ON public.jobs USING btree (queue);


--
-- Name: loanables_id_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX loanables_id_type_index ON public.loanables USING btree (id, type);


--
-- Name: loans_actual_return_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX loans_actual_return_at_index ON public.loans USING btree (actual_return_at);


--
-- Name: loans_departure_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX loans_departure_at_index ON public.loans USING btree (departure_at);


--
-- Name: loans_status_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX loans_status_index ON public.loans USING btree (status);


--
-- Name: oauth_access_tokens_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX oauth_access_tokens_user_id_index ON public.oauth_access_tokens USING btree (user_id);


--
-- Name: oauth_auth_codes_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX oauth_auth_codes_user_id_index ON public.oauth_auth_codes USING btree (user_id);


--
-- Name: oauth_clients_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX oauth_clients_user_id_index ON public.oauth_clients USING btree (user_id);


--
-- Name: oauth_refresh_tokens_access_token_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX oauth_refresh_tokens_access_token_id_index ON public.oauth_refresh_tokens USING btree (access_token_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: users_created_at_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX users_created_at_index ON public.users USING btree (created_at);


--
-- Name: bikes bikes_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bikes
    ADD CONSTRAINT bikes_id_foreign FOREIGN KEY (id) REFERENCES public.loanables(id) ON DELETE CASCADE;


--
-- Name: bill_items bill_items_contribution_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill_items
    ADD CONSTRAINT bill_items_contribution_community_id_foreign FOREIGN KEY (contribution_community_id) REFERENCES public.communities(id);


--
-- Name: bill_items bill_items_invoice_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill_items
    ADD CONSTRAINT bill_items_invoice_id_foreign FOREIGN KEY (invoice_id) REFERENCES public.invoices(id) ON DELETE CASCADE;


--
-- Name: borrowers borrowers_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.borrowers
    ADD CONSTRAINT borrowers_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: cars cars_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_id_foreign FOREIGN KEY (id) REFERENCES public.loanables(id) ON DELETE CASCADE;


--
-- Name: community_loanable_types community_loanable_types_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_loanable_types
    ADD CONSTRAINT community_loanable_types_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: community_loanable_types community_loanable_types_loanable_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_loanable_types
    ADD CONSTRAINT community_loanable_types_loanable_type_id_foreign FOREIGN KEY (loanable_type_id) REFERENCES public.loanable_type_details(id) ON DELETE CASCADE;


--
-- Name: community_user community_user_approver_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_user
    ADD CONSTRAINT community_user_approver_id_foreign FOREIGN KEY (approver_id) REFERENCES public.users(id);


--
-- Name: community_user community_user_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_user
    ADD CONSTRAINT community_user_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: community_user community_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_user
    ADD CONSTRAINT community_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: loanable_user_roles coowners_loanable_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_user_roles
    ADD CONSTRAINT coowners_loanable_id_foreign FOREIGN KEY (loanable_id) REFERENCES public.loanables(id) ON DELETE CASCADE;


--
-- Name: loanable_user_roles coowners_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_user_roles
    ADD CONSTRAINT coowners_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: exports exports_batch_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exports
    ADD CONSTRAINT exports_batch_id_foreign FOREIGN KEY (batch_id) REFERENCES public.job_batches(id) ON DELETE SET NULL;


--
-- Name: exports exports_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.exports
    ADD CONSTRAINT exports_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: files files_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: gbfs_dataset_communities gbfs_dataset_communities_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gbfs_dataset_communities
    ADD CONSTRAINT gbfs_dataset_communities_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: gbfs_dataset_communities gbfs_dataset_communities_gbfs_dataset_name_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gbfs_dataset_communities
    ADD CONSTRAINT gbfs_dataset_communities_gbfs_dataset_name_foreign FOREIGN KEY (gbfs_dataset_name) REFERENCES public.gbfs_datasets(name) ON DELETE CASCADE;


--
-- Name: google_accounts google_accounts_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.google_accounts
    ADD CONSTRAINT google_accounts_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: incident_notes incident_notes_author_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notes
    ADD CONSTRAINT incident_notes_author_id_foreign FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- Name: incident_notes incident_notes_incident_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notes
    ADD CONSTRAINT incident_notes_incident_id_foreign FOREIGN KEY (incident_id) REFERENCES public.incidents(id) ON DELETE CASCADE;


--
-- Name: incident_notifications incident_notifications_incident_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notifications
    ADD CONSTRAINT incident_notifications_incident_id_foreign FOREIGN KEY (incident_id) REFERENCES public.incidents(id) ON DELETE CASCADE;


--
-- Name: incident_notifications incident_notifications_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incident_notifications
    ADD CONSTRAINT incident_notifications_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: incidents incidents_assignee_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_assignee_id_foreign FOREIGN KEY (assignee_id) REFERENCES public.users(id) ON DELETE SET NULL;


--
-- Name: incidents incidents_loan_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_loan_id_foreign FOREIGN KEY (loan_id) REFERENCES public.loans(id) ON DELETE CASCADE;


--
-- Name: incidents incidents_loanable_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_loanable_id_foreign FOREIGN KEY (loanable_id) REFERENCES public.loanables(id);


--
-- Name: incidents incidents_reported_by_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_reported_by_user_id_foreign FOREIGN KEY (reported_by_user_id) REFERENCES public.users(id);


--
-- Name: incidents incidents_resolved_by_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.incidents
    ADD CONSTRAINT incidents_resolved_by_user_id_foreign FOREIGN KEY (resolved_by_user_id) REFERENCES public.users(id);


--
-- Name: invitations invitations_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_inviter_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_inviter_user_id_foreign FOREIGN KEY (inviter_user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: invoices invoices_payment_method_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT invoices_payment_method_id_foreign FOREIGN KEY (payment_method_id) REFERENCES public.payment_methods(id) ON DELETE CASCADE;


--
-- Name: invoices invoices_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT invoices_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: loan_comments loan_comments_author_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_comments
    ADD CONSTRAINT loan_comments_author_id_foreign FOREIGN KEY (author_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: loan_comments loan_comments_loan_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_comments
    ADD CONSTRAINT loan_comments_loan_id_foreign FOREIGN KEY (loan_id) REFERENCES public.loans(id) ON DELETE CASCADE;


--
-- Name: loan_notifications loan_notifications_loan_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_notifications
    ADD CONSTRAINT loan_notifications_loan_id_foreign FOREIGN KEY (loan_id) REFERENCES public.loans(id) ON DELETE CASCADE;


--
-- Name: loan_notifications loan_notifications_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan_notifications
    ADD CONSTRAINT loan_notifications_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: loanable_user_roles loanable_user_roles_granted_by_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loanable_user_roles
    ADD CONSTRAINT loanable_user_roles_granted_by_user_id_foreign FOREIGN KEY (granted_by_user_id) REFERENCES public.users(id);


--
-- Name: loans loans_borrower_invoice_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loans
    ADD CONSTRAINT loans_borrower_invoice_id_foreign FOREIGN KEY (borrower_invoice_id) REFERENCES public.invoices(id) ON DELETE SET NULL;


--
-- Name: loans loans_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loans
    ADD CONSTRAINT loans_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: loans loans_owner_invoice_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loans
    ADD CONSTRAINT loans_owner_invoice_id_foreign FOREIGN KEY (owner_invoice_id) REFERENCES public.invoices(id) ON DELETE SET NULL;


--
-- Name: mailing_list_integrations mailing_list_integrations_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_list_integrations
    ADD CONSTRAINT mailing_list_integrations_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: mailing_list_integrations mailing_list_integrations_created_by_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mailing_list_integrations
    ADD CONSTRAINT mailing_list_integrations_created_by_user_id_foreign FOREIGN KEY (created_by_user_id) REFERENCES public.users(id);


--
-- Name: payment_methods payment_methods_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payment_methods
    ADD CONSTRAINT payment_methods_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: payouts payouts_invoice_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT payouts_invoice_id_foreign FOREIGN KEY (invoice_id) REFERENCES public.invoices(id);


--
-- Name: payouts payouts_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT payouts_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: pricing_loanable_types pricing_loanable_types_pricing_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pricing_loanable_types
    ADD CONSTRAINT pricing_loanable_types_pricing_id_foreign FOREIGN KEY (pricing_id) REFERENCES public.pricings(id) ON DELETE CASCADE;


--
-- Name: pricings pricings_community_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pricings
    ADD CONSTRAINT pricings_community_id_foreign FOREIGN KEY (community_id) REFERENCES public.communities(id) ON DELETE CASCADE;


--
-- Name: subscription_loanable_types subscription_loanable_types_subscription_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_loanable_types
    ADD CONSTRAINT subscription_loanable_types_subscription_id_foreign FOREIGN KEY (subscription_id) REFERENCES public.subscriptions(id) ON DELETE CASCADE;


--
-- Name: subscriptions subscriptions_community_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_community_user_id_foreign FOREIGN KEY (community_user_id) REFERENCES public.community_user(id) ON DELETE CASCADE;


--
-- Name: subscriptions subscriptions_granted_by_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_granted_by_user_id_foreign FOREIGN KEY (granted_by_user_id) REFERENCES public.users(id);


--
-- Name: subscriptions subscriptions_invoice_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscriptions
    ADD CONSTRAINT subscriptions_invoice_id_foreign FOREIGN KEY (invoice_id) REFERENCES public.invoices(id);


--
-- Name: trailers trailers_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.trailers
    ADD CONSTRAINT trailers_id_foreign FOREIGN KEY (id) REFERENCES public.loanables(id) ON DELETE CASCADE;


--
-- Name: user_publishable_reports user_publishable_reports_publishable_report_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_publishable_reports
    ADD CONSTRAINT user_publishable_reports_publishable_report_id_foreign FOREIGN KEY (publishable_report_id) REFERENCES public.publishable_reports(id) ON DELETE CASCADE;


--
-- Name: user_publishable_reports user_publishable_reports_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_publishable_reports
    ADD CONSTRAINT user_publishable_reports_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.20 (Debian 12.20-1.pgdg120+1)
-- Dumped by pg_dump version 15.10 (Debian 15.10-0+deb12u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_06_01_000001_create_oauth_auth_codes_table	1
4	2016_06_01_000002_create_oauth_access_tokens_table	1
5	2016_06_01_000003_create_oauth_refresh_tokens_table	1
6	2016_06_01_000004_create_oauth_clients_table	1
7	2016_06_01_000005_create_oauth_personal_access_clients_table	1
8	2019_05_13_142045_oauth_id_string	1
9	2019_12_02_173510_create_communities_table	1
10	2019_12_02_173830_create_pricings_table	1
11	2019_12_03_132127_create_owners_table	1
12	2019_12_03_132426_create_borrowers_table	1
13	2019_12_03_132515_create_files_table	1
14	2019_12_03_132523_create_images_table	1
15	2019_12_03_132541_create_tags_table	1
16	2019_12_03_132551_create_cars_table	1
17	2019_12_03_132753_create_bikes_table	1
18	2019_12_03_132819_create_trailers_table	1
19	2019_12_03_132911_create_padlocks_table	1
20	2019_12_03_133114_create_payment_methods_table	1
21	2019_12_03_133508_create_loans_table	1
22	2019_12_03_133519_create_intentions_table	1
23	2019_12_03_133838_create_takeovers_table	1
24	2019_12_03_133851_create_handovers_table	1
25	2019_12_03_133905_create_incidents_table	1
26	2019_12_03_133917_create_extensions_table	1
27	2019_12_03_133928_create_invoices_table	1
28	2019_12_03_134040_create_billable_items_table	1
29	2019_12_03_134041_create_payments_table	1
30	2020_01_09_151330_add_community_user_table	1
31	2020_01_09_152305_add_role_on_user	1
32	2020_01_16_195911_add_users_name_default	1
33	2020_01_17_191721_add_community_type_enum	1
34	2020_01_29_204805_add_actions_materialized_view	1
35	2020_01_30_164833_add_loanables_materialized_view	1
36	2020_02_03_224232_add_indices_to_materialized_views	1
37	2020_02_05_212001_rename_type_fields_on_sub_loanables	1
38	2020_02_07_193755_add_filesize_to_images	1
39	2020_02_07_194529_remove_orientation_from_images	1
40	2020_02_10_215225_add_approbation_fields_to_community_user	1
41	2020_02_13_193306_change_intention_executed_at_to_nullable	1
42	2020_02_13_201020_add_default_to_tag_type	1
43	2020_02_13_210406_add_default_to_intention_status_enum	1
44	2020_02_15_204059_change_payment_executed_at_to_nullable	1
45	2020_02_15_204249_add_default_to_payment_status_enum	1
46	2020_02_24_185623_remove_period_and_payment_method_not_null	1
47	2020_02_24_191008_make_paid_at_nullable_on_invoices	1
48	2020_03_04_113247_add_availability_fields_to_loanables	1
49	2020_03_05_115546_add_accout_balance_to_users	1
50	2020_03_06_091911_remove_variable_from_pricings	1
51	2020_03_06_151510_fix_materialized_views_sequences	1
52	2020_03_06_171332_add_image_id_to_materialized_view	1
53	2020_03_06_175555_add_index_on_materialized_view	1
54	2020_03_09_132551_add_delete_at_to_users	1
55	2020_03_11_102800_add_estimated_distance_to_loans	1
56	2020_03_12_172334_add_reason_and_message_for_owner_to_loans	1
57	2020_03_12_174320_add_estimated_price_to_loans	1
58	2020_03_13_124535_make_loan_relations_nullable	1
59	2020_03_13_170126_add_prepayment_action	1
60	2020_03_13_170249_recreate_actions_materialized_view	1
61	2020_03_14_165527_fix_actions_sequence	1
62	2020_03_16_134337_change_prepayment_executed_at_nullable	1
63	2020_03_16_140439_add_message_for_borrower_to_intention	1
64	2020_03_16_173834_change_executed_at_nullable_on_other_actions	1
65	2020_03_16_174352_set_default_vales_on_takeovers	1
66	2020_03_18_134057_add_transaction_id_to_users	1
67	2020_03_19_173910_set_mileage_beginning_to_integer	1
68	2020_03_19_180128_make_handover_value_nullable	1
69	2020_03_20_151751_add_link_to_community_on_loans	1
70	2020_03_20_154735_set_payment_relations_nullable	1
71	2020_03_20_160715_add_final_price_to_loans	1
72	2020_03_27_103537_remove_loanable_type_on_loans	1
73	2020_03_30_094331_add_slug_to_tags	1
74	2020_04_01_175501_add_suspended_at_to_borrowers	1
75	2020_04_05_130448_make_loan_id_nullable_on_extensions	1
76	2020_04_05_131219_recreate_actions_view_with_priority_order	1
77	2020_04_05_133934_add_message_for_borrower_to_extension	1
78	2020_04_05_164010_make_loan_id_nullable_on_incidents	1
79	2020_04_06_140018_add_padlock_name_to_padlocks	1
80	2020_05_04_112347_add_pricing_category_to_cars	1
81	2020_05_04_135454_add_estimated_insurance_to_loan	1
82	2020_05_04_171903_remove_loanable_type_from_padlocks	1
83	2020_05_05_104557_remove_insurance_default_value	1
84	2020_05_05_151625_add_cargo_to_bike_types	1
85	2020_05_05_153140_fix_view_bug_on_duplicate_images	1
86	2020_05_05_180307_create_failed_jobs_table	1
87	2020_05_06_104904_create_assets_view	1
88	2020_05_06_152122_add_taxable_to_bill_items	1
89	2020_05_07_120740_add_solon_tip_to_loans	1
90	2020_05_07_163519_add_final_platform_tip_to_loans	1
91	2020_05_07_171256_change_bill_items_foreign_keys_on_payments	1
92	2020_05_08_104724_add_comments_on_incident_to_incidents	1
93	2020_05_12_112307_remove_property_type	1
94	2020_05_12_114253_remove_fuel_end_and_beginning	1
95	2020_05_12_120251_remove_has_accident_report	1
96	2020_05_12_160047_create_google_accounts_table	1
97	2020_05_12_161503_change_google_id_in_users	1
98	2020_05_14_154515_change_values_for_incident_type	1
99	2020_05_28_101646_change_email_to_citext	1
100	2020_07_17_112725_add_parent_community_to_communities	1
101	2020_07_17_130742_add__share_on_parent_communities_to_loanables	1
102	2020_07_22_153633_add_opt_in_newsletter_to_users	1
103	2020_07_29_154701_add_accept_conditions_to_users	1
104	2020_08_12_165039_add_long_description_to_communities	1
105	2020_08_14_150719_add_chat_group_url_to_communities	1
106	2020_08_17_174830_change_values_for_incident_type_again	1
107	2020_09_01_145959_add_meta_to_users	1
108	2020_09_09_220913_add_meta_to_loans	1
109	2021_01_05_153300_add_explicited_canceled_at_on_loans	1
110	2021_01_05_180819_add_final_purchases_amount_to_loans	1
111	2021_08_01_184752_create_cache_table	1
112	2021_09_08_151200_add_provider_field_to_oauth_clients	1
113	2021_09_09_create_personal_access_client	1
114	2021_09_15_161827_remove_deleted_padlocks	1
115	2021_09_15_170553_add_unique_constraint_on_mac_address_in_padlocks	1
116	2021_09_29_201056_create_jobs_table	1
117	2021_10_01_130210_add_indexes_to_some_tables	1
118	2021_10_12_161244_add_deactivated_to_users	1
119	2021_11_04_140812_add_availability_to_loanables	1
120	2021_11_09_164942_remove_availability_ics	1
121	2021_11_25_102012_remove_opt_in_newsletter_from_users	1
122	2021_12_15_170511_add_is_self_service_to_loanables	1
123	2022_03_23_120906_add_rejected_status_in_extensions	1
124	2022_04_12_110150_add_status_field_to_loans	1
125	2022_04_12_144045_set_loan_status	1
126	2022_04_26_083315_add_amount_type_to_bill_items	1
127	2022_04_27_163144_add_type_column_to_invoices_table	1
128	2022_06_03_111002_add_actual_return_at_field_to_loans	1
129	2022_06_07_104823_add_electric_to_pricing_categories	1
130	2022_06_08_093323_set_actual_return_at	1
131	2022_06_14_102435_migration_users_to_borough	1
132	2022_08_12_164823_add_has_not_been_sued_to_borrowers	1
133	2022_09_08_154054_convert_community_user_proof_to_file	1
134	2022_10_05_140050_increase_cache_value_size	1
135	2022_10_12_163956_add_trailer_dimensions	1
136	2022_10_17_095415_add_payment_method_card_country	1
137	2022_10_31_153818_skip_takeover_for_self_service	1
138	2022_11_09_083204_add_loan_validation	1
139	2022_11_16_083331_add_loanable_co_owners	1
140	2022_12_01_172317_add_title_and_display_flag_to_coowners	1
141	2022_12_13_101409_drop_user_submitted_at_column	1
142	2023_01_10_112755_add_terms_approved_at_date	1
143	2023_02_07_112815_remove_neighborhoods	1
144	2023_02_07_120032_add_morph_map	1
145	2023_02_07_155623_remove_community_from_loanables	1
146	2023_02_07_160000_remove_loanables_materialized_view	1
147	2023_02_09_094323_add_show_owner_as_contact_to_loanables	1
148	2023_02_16_000000_add_community_loanable_types	1
149	2023_03_15_161335_add_uses_noke_to_communities_table	1
150	2023_04_03_130608_add_starting_guide_urls_to_communities	1
151	2023_04_06_110140_add_car_value_to_cars	1
152	2023_04_13_131350_drop_has_been_sued_from_borrowers	1
153	2023_04_13_144815_allow_null_estimated_distance_and_remove_estimated_prices	1
154	2023_04_20_092447_delete_tags	1
155	2023_04_20_100827_change_communities_area_to_multi_polygon	1
156	2023_04_27_140742_drop_actions_materialized_view	1
157	2023_05_02_115018_delete_assets_view	1
158	2023_05_05_095824_use_soft_deletes_for_users	1
159	2023_05_16_112358_add_car_notebook_fields	1
160	2023_05_16_135513_add_user_claim_credit_settings	1
161	2023_05_17_110830_remove_bank_account_payment_methods	1
162	2023_05_18_114619_move_community_user_proof_to_users	1
163	2023_05_18_123250_add_community_users_proof_invalid	1
164	2023_05_24_110150_add_address_position_to_users	1
165	2023_05_30_095114_remove_noke_id_from_borrowers	1
166	2023_05_31_114803_add_meta_to_bill_items	1
167	2023_06_01_155612_add_item_type_to_bill_items	1
168	2023_06_06_160150_add_balance_before_and_after_to_invoices	1
169	2023_06_09_115438_allow_loan_platform_tip_to_be_null	1
170	2023_07_04_103721_add_noke_id_to_users	1
171	2023_07_20_164055_add_community_requires_identity_proof	1
172	2023_07_20_165100_add_user_identity_proof	1
173	2023_07_26_200652_add_file_image_path_index	1
174	2023_08_01_132247_correct_user_noke_id_column_type	1
175	2023_08_09_163458_change_bill_items_item_date_nullable	1
176	2023_08_14_053404_create_job_batches_table	1
177	2023_08_30_110231_move_proof_state_to_community_user	1
178	2023_08_31_143202_add_indices_to_loans	1
179	2023_09_11_165700_rename_bill_item_types	1
180	2023_09_27_100054_create_exports_table	1
181	2023_10_05_090936_make_bill_item_types_into_enum	1
182	2023_10_12_103030_add_order_to_images	1
183	2023_10_17_113521_add_noke_group_to_padlocks	1
184	2023_10_21_114941_add_indices	1
185	2023_11_07_100925_remove_balance_before_and_after_from_invoices	1
186	2023_11_13_114407_change_bill_items_item_type_constraints	1
187	2023_11_13_140020_add_blur_to_images	1
188	2023_11_14_164851_remove_item_types_from_bill_items	1
189	2023_11_16_133307_remove_constraint_on_bill_items_amount_type	1
190	2023_12_07_114808_drop_is_value_over_fifty_thousand_from_cars	1
191	2023_12_07_134955_remove_transaction_id_from_users	1
192	2024_01_15_143852_add_loan_alternative	1
193	2024_01_22_105613_create_invitations_table	1
194	2024_01_23_092359_add_community_user_joining_information	1
195	2024_02_02_102141_remove_type_from_invoices	1
196	2024_02_09_134941_remove_bill_items_amount_type	1
197	2024_02_13_162850_remove_item_date_from_bill_items	1
198	2024_02_14_115308_remove_timezones	1
199	2024_02_14_115901_shift_datetimes_to_utc	1
200	2024_02_14_142011_add_timezone_to_loanables	1
201	2024_02_16_103359_remove_final_platform_tip_from_loans	1
202	2024_02_21_133016_add_data_deleted_at_to_users	1
203	2024_02_22_143441_add_contact_email_to_communities	1
204	2024_03_04_135636_add_published_state_to_loanables	1
205	2024_03_11_153924_make_loanable_details_fields_nullable	1
206	2024_03_28_203252_remove_final_purchases_amount_from_loans	1
207	2024_04_03_145838_remove_soft_deletes_from_bill_items	1
208	2024_04_10_154044_add_gbfs_stream_tables	1
209	2024_04_23_150000_add_pricings_types	1
210	2024_04_23_150940_update_pricings_for_contributions	1
211	2024_04_23_194309_remove_final_price_and_insurance_from_loans	1
212	2024_05_01_142044_add_is_fleet_to_users	1
213	2024_05_08_173448_add_paid_amount_configuration_to_coowners	1
214	2024_05_09_165759_add_electric_cargo_bike_to_bike_types	1
215	2024_05_13_140925_add_community_id_to_bill_items	1
216	2024_05_16_155825_add_subscription_table	1
217	2024_05_24_201021_rename_loan_calendar_days_in_invoice_item_meta	1
218	2024_05_29_151932_make_loan_borrower_id_not_nullable	1
219	2024_05_29_151955_add_borrower_user_id_to_loans	1
220	2024_06_03_134749_add_exempt_from_contributions_to_communities	1
221	2024_06_13_185607_add_sharing_mode_to_loanables	1
222	2024_06_13_193955_add_is_self_service_to_loans	1
223	2024_06_13_201958_remove_is_self_service_from_loanables	1
224	2024_06_17_170025_rename_coowners_to_user_loanable_roles	1
225	2024_06_17_200250_add_granted_by_to_loanable_user_roles	1
226	2024_06_20_192626_add_trusted_borrower_instructions_to_loanables	1
227	2024_06_25_141602_remove_borrower_id_from_loans	1
228	2024_06_26_200617_move_owners_to_loanable_user_roles	1
229	2024_07_18_151944_add_pricing_dates	1
230	2024_07_22_180259_add_incident_reporter_and_resolver	1
231	2024_07_22_191605_add_incident_notes	1
232	2024_07_22_201047_add_assignees_to_incidents	1
233	2024_07_24_183335_add_loanable_to_incidents	1
234	2024_07_24_185129_archive_old_incidents	1
235	2024_07_25_170346_add_blocking_until_date_to_incidents	1
236	2024_08_12_145925_add_incident_notifications	1
237	2024_08_14_135244_add_show_incident_details_to_blocked_borrowers	1
238	2024_08_27_134134_create_loan_comments_table	1
239	2024_08_27_140143_move_loan_message_for_owner_to_comments	1
240	2024_08_27_140753_drop_intention_table	1
241	2024_08_27_185452_drop_pre_payment_table	1
242	2024_08_28_180126_drop_takeover_table	1
243	2024_08_28_190026_drop_handover_table	1
244	2024_08_28_193000_drop_payment_table	1
245	2024_08_28_202401_change_loan_states	1
246	2024_09_09_180812_drop_loan_extension_table	1
247	2024_09_30_145925_add_loan_notifications	1
248	2024_10_02_182358_add_last_seen_to_loan_notifications	1
249	2024_10_03_185731_add_attempt_autocomplete_at_to_loans	1
250	2024_10_17_182316_set_bill_items_contribution_community_id	1
251	2024_10_30_133340_remove_noke	1
252	2024_10_30_134658_drop_padlocks	1
253	2024_10_31_151231_create_mailing_lists_table	1
254	2024_11_19_142906_create_payouts_table	1
255	2024_11_20_135058_add_bank_info_updated_at_to_users	1
256	2024_11_21_181141_create_reports_table	1
257	2024_11_21_181540_create_user_reports_table	1
258	2024_11_29_200616_remove_owner_id_from_loanables	1
259	2024_11_29_200640_drop_table_owners	1
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.migrations_id_seq', 259, true);

--
-- PostgreSQL database dump complete
--

