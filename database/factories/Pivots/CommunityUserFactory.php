<?php

namespace Database\Factories\Pivots;

use App\Models\Community;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommunityUserFactory extends Factory
{
    public function definition(): array
    {
        return [
            "user_id" => User::factory(),
            "community_id" => Community::factory(),
        ];
    }
}
