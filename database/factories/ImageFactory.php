<?php
namespace Database\Factories;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;

class ImageFactory extends Factory
{
    protected $model = Image::class;
    public function definition(): array
    {
        return [
            "original_filename" => $this->faker->word . ".png",
            "path" => $this->faker->word . "/" . $this->faker->word,
            "filename" => $this->faker->word . ".png",
            "width" => $this->faker->numberBetween(10, 100),
            "height" => $this->faker->numberBetween(10, 100),
            "filesize" => $this->faker->numberBetween(1000, 10_000),
        ];
    }

    public function withStoredImage(): static
    {
        return $this->afterMaking(function (Image $image) {
            $uploadedImage = UploadedFile::fake()->image(
                $image->original_filename,
                $image->width,
                $image->height
            );
            \Storage::put($image->full_path, $uploadedImage->getContent());
        });
    }
}
