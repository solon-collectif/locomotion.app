<?php

namespace Database\Factories;

use App\Enums\LoanableUserRoles;
use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoanableUserRoleFactory extends Factory
{
    protected $model = LoanableUserRole::class;

    public function definition(): array
    {
        return [
            "show_as_contact" => $this->faker->boolean(),
            "title" => $this->faker->word(),
            "role" => LoanableUserRoles::Coowner,
            "user_id" => User::factory(),
            "pays_loan_price" => $this->faker->boolean(),
            "pays_loan_insurance" => $this->faker->boolean(),
        ];
    }
}
