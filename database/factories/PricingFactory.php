<?php

namespace Database\Factories;

use App\Enums\PricingLoanableTypeValues;
use App\Models\Loan;
use App\Models\Pricing;
use Illuminate\Database\Eloquent\Factories\Factory;

class PricingFactory extends Factory
{
    protected $model = Pricing::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "pricing_type" => "price",
            "start_date" => "1999-01-01",
            "rule" => "10",
        ];
    }

    public function forAllTypes(): PricingFactory
    {
        return $this->afterCreating(function (Pricing $pricing) {
            \DB::table("pricing_loanable_types")->insert(
                array_map(
                    fn($type) => [
                        "pricing_id" => $pricing->id,
                        "pricing_loanable_type" => $type,
                    ],
                    PricingLoanableTypeValues::possibleTypes
                )
            );
        });
    }

    public function forType(PricingLoanableTypeValues $type): PricingFactory
    {
        return $this->afterCreating(
            fn(Pricing $pricing) => \DB::table(
                "pricing_loanable_types"
            )->insert([
                [
                    "pricing_id" => $pricing->id,
                    "pricing_loanable_type" => $type,
                ],
            ])
        );
    }

    public function forLoan(Loan $loan): PricingFactory
    {
        return $this->state([
            "community_id" => $loan->community_id,
        ])->forType(PricingLoanableTypeValues::forLoanable($loan->loanable));
    }
}
