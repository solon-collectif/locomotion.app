<?php

namespace Database\Factories;

use App\Models\Loan;
use App\Models\LoanComment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class LoanCommentFactory extends Factory
{
    protected $model = LoanComment::class;

    public function definition(): array
    {
        return [
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "text" => $this->faker->text(),

            "loan_id" => Loan::factory(),
            "author_id" => User::factory(),
        ];
    }
}
