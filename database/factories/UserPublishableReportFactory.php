<?php

namespace Database\Factories;

use App\Models\PublishableReport;
use App\Models\User;
use App\Models\UserPublishableReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserPublishableReportFactory extends Factory
{
    protected $model = UserPublishableReport::class;

    public function definition(): array
    {
        return [
            "path" => $this->faker->word(),
            "user_id" => User::factory(),
            "publishable_report_id" => PublishableReport::factory(),
        ];
    }
}
