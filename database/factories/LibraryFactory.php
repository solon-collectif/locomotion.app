<?php

namespace Database\Factories;

use App\Enums\LoanableUserRoles;
use App\Models\Community;
use App\Models\Library;
use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class LibraryFactory extends Factory
{
    protected $model = Library::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name(),
            "phone_number" => $this->faker->phoneNumber(),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ];
    }

    public function withUserRole(
        UserFactory|User $user = null,
        array $roleAttributes = []
    ): LibraryFactory {
        if ($user) {
            return $this->has(
                LoanableUserRole::factory([
                    "user_id" => $user,
                    ...$roleAttributes,
                ]),
                "userRoles"
            );
        }
        return $this->has(
            LoanableUserRole::factory([...$roleAttributes]),
            "userRoles"
        );
    }

    public function withOwner(
        UserFactory|User $user = null,
        array $roleAttributes = []
    ): LibraryFactory {
        if (!$user) {
            $user = User::factory()->withCommunity();
        }

        return $this->withUserRole($user, [
            "role" => LoanableUserRoles::Owner,
            ...$roleAttributes,
        ]);
    }
    public function withManager(
        UserFactory|User $user = null,
        array $roleAttributes = []
    ): LibraryFactory {
        if (!$user) {
            $user = User::factory()->withCommunity();
        }

        return $this->withUserRole($user, [
            "role" => LoanableUserRoles::Manager,
            ...$roleAttributes,
        ]);
    }

    public function withCommunity(Community $community): LibraryFactory
    {
        return $this->afterCreating(
            fn(Library $library) => $library->communities()->save($community)
        );
    }
}
