<?php

namespace Database\Factories;

use App\Enums\SharingModes;
use App\Http\Controllers\LoanController;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoanFactory extends Factory
{
    protected $model = Loan::class;

    public function definition(): array
    {
        return [
            "departure_at" => CarbonImmutable::now(),
            // Over 15 minutes.
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3),
            "alternative_to" => "car",
            "platform_tip" => $this->faker->randomNumber(1),
            "is_self_service" => false,
            "loanable_id" => Loanable::factory()->withOwner(),
            "borrower_user_id" => User::factory()->withBorrower(),
        ];
    }

    public function configure(): LoanFactory
    {
        return $this->afterMaking(function (Loan $loan) {
            if (!$loan->community_id) {
                $loan->community_id =
                    $loan->loanable->owner_user->main_community->id;
            }
        })->afterCreating(LoanController::subscribeAllConcernedUsers(...));
    }

    public function requested(): LoanFactory
    {
        return $this->state([
            "departure_at" => CarbonImmutable::now()->addHour(),
            "loanable_id" => Loanable::factory([
                "sharing_mode" => SharingModes::OnDemand,
            ]),
        ]);
    }

    public function accepted(): LoanFactory
    {
        return $this->state([
            "departure_at" => CarbonImmutable::now()->addHour(),
            "community_id" => Community::factory()->withDefault10DollarsPricing(),
            "borrower_user_id" => User::factory([
                "balance" => 0,
            ])->withBorrower(),
        ])->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusAccepted();
            $loan->save();
        });
    }

    public function confirmed(): LoanFactory
    {
        return $this->state([
            "departure_at" => CarbonImmutable::now()->addHour(),
        ])->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusConfirmed();
            $loan->save();
        });
    }

    public function canceled(): LoanFactory
    {
        return $this->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusCanceled();
            $loan->save();
        });
    }

    public function ongoing($mileage = null): LoanFactory
    {
        return $this->state([
            "departure_at" => CarbonImmutable::now()->subHour(),
            "duration_in_minutes" => 120,
            "mileage_start" => $mileage ?? $this->faker->randomNumber(5),
        ])->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusOngoing();
            $loan->save();
        });
    }

    public function ended(
        int $mileageStart = null,
        int $mileageEnd = null
    ): LoanFactory {
        $mileageEnd = $mileageEnd ?? $this->faker->numberBetween(1000, 300_000);
        $mileageStart =
            $mileageStart ??
            $this->faker->numberBetween($mileageEnd - 500, $mileageEnd);

        return $this->state([
            "departure_at" => CarbonImmutable::now()->subHours(2),
            "duration_in_minutes" => 60,
            "mileage_start" => $mileageStart,
            "mileage_end" => $mileageEnd,
            "community_id" => Community::factory()->withKMDollarsPricing(),
            "loanable_id" => Loanable::factory([
                "sharing_mode" => SharingModes::OnDemand,
            ])->withCar(),
        ])->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusEnded();
            $loan->save();
        });
    }

    public function validated(): LoanFactory
    {
        return $this->state([
            "departure_at" => CarbonImmutable::now()->subHours(2),
            "duration_in_minutes" => 60,
            "community_id" => Community::factory()->withKMDollarsPricing(),
            "borrower_user_id" => User::factory([
                "balance" => 0,
            ])->withBorrower(),
            "borrower_validated_at" => CarbonImmutable::now(),
            "owner_validated_at" => CarbonImmutable::now(),
            "mileage_start" => 10,
            "mileage_end" => 20,
        ])->afterCreating(function (Loan $loan) {
            $loan->setLoanStatusValidated();
            $loan->save();
        });
    }

    public function needsValidation(): LoanFactory
    {
        return $this->state([
            "loanable_id" => Loanable::factory([
                "sharing_mode" => SharingModes::OnDemand,
            ])->withCar(),
            "community_id" => Community::factory()->withKMDollarsPricing(),
        ]);
    }

    public function withMileageBasedCompensation(): LoanFactory
    {
        return $this->state([
            "community_id" => Community::factory()->withKMDollarsPricing(),
        ]);
    }

    public function inFreeCommunity(): LoanFactory
    {
        return $this->state([
            "community_id" => Community::factory()->withDefaultFreePricing(),
        ]);
    }

    public function paid(CarbonInterface $paid_at = null): LoanFactory
    {
        return $this->afterCreating(function (Loan $loan) use ($paid_at) {
            $loan->setLoanStatusPaid();
            if ($paid_at) {
                $loan->paid_at = $paid_at->toISOString();
            }
            $loan->save();
        });
    }
}
