<?php

namespace Database\Factories;

use App\Casts\Point;
use App\Enums\LoanableTypes;
use App\Enums\LoanableUserRoles;
use App\Models\Community;
use App\Models\Image;
use App\Models\Loanable;
use App\Models\LoanableUserRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoanableFactory extends Factory
{
    protected $model = Loanable::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "type" => LoanableTypes::Bike,
            "position" => new Point(
                $this->faker->latitude,
                $this->faker->longitude
            ),
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "return_instructions" => $this->faker->paragraph,
            "availability_mode" => "always",
            "timezone" => "America/Toronto",
            "published" => true,
        ];
    }

    public function configure(): LoanableFactory
    {
        return $this->createDetails();
    }

    private function getFactory(LoanableTypes $type): Factory
    {
        return $type->getLoanableModel()::factory();
    }

    private function createDetails(array $attributes = []): LoanableFactory
    {
        return $this->afterCreating(function (Loanable $loanable) use (
            $attributes
        ) {
            $factory = $this->getFactory($loanable->type);

            if (!$loanable->details) {
                $factory->for($loanable)->create($attributes);
                $loanable->load("details");
            } else {
                $loanable->details->fill($attributes)->save();
            }
        });
    }

    public function withBike(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Bike,
        ])->createDetails($attributes);
    }

    public function withTrailer(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Trailer,
        ])->createDetails($attributes);
    }

    public function withCar(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Car,
        ])->createDetails($attributes);
    }

    public function withCommunity(): LoanableFactory
    {
        return $this->has(Community::factory()->withDefaultFreePricing());
    }

    public function withUserRole(
        UserFactory|User $user = null,
        array $roleAttributes = []
    ): LoanableFactory {
        if ($user) {
            return $this->has(
                LoanableUserRole::factory([
                    "user_id" => $user,
                    "ressource_type" => "loanable",
                    ...$roleAttributes,
                ]),
                "userRoles"
            );
        }
        return $this->has(
            LoanableUserRole::factory([
                "role" => LoanableUserRoles::Coowner,
                "ressource_type" => "loanable",
                ...$roleAttributes,
            ]),
            "userRoles"
        );
    }

    public function withOwner(
        UserFactory|User $user = null,
        array $roleAttributes = []
    ): LoanableFactory {
        if (!$user) {
            $user = User::factory()->withCommunity();
        }

        if (!array_key_exists("show_as_contact", $roleAttributes)) {
            $roleAttributes["show_as_contact"] = true;
        }

        return $this->withUserRole($user, [
            "role" => LoanableUserRoles::Owner,
            ...$roleAttributes,
        ]);
    }

    public function withCoowner(
        UserFactory|User $user = null,
        array $coownerAttributes = []
    ): LoanableFactory {
        return $this->withUserRole($user, [
            "role" => LoanableUserRoles::Coowner,
            "show_as_contact" => true,
            ...$coownerAttributes,
        ]);
    }

    public function withImage(): LoanableFactory
    {
        return $this->afterCreating(function (Loanable $loanable) {
            // Without events to avoid triggering Image move
            Image::withoutEvents(
                fn() => $loanable->image()->save(
                    Image::factory()->create([
                        "imageable_type" => Loanable::class,
                        "imageable_id" => $loanable->id,
                        "field" => "image",
                    ])
                )
            );
            $loanable->save();
        });
    }
}
