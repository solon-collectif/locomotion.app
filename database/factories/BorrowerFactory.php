<?php
namespace Database\Factories;
use App\Models\Borrower;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BorrowerFactory extends Factory
{
    protected $model = Borrower::class;

    public function definition(): array
    {
        return [
            "drivers_license_number" => $this->faker->numberBetween(
                1_111_111_111,
                999_999_999
            ),
            "has_not_been_sued_last_ten_years" => $this->faker->boolean,
            "submitted_at" => date("Y-m-d"),
            "approved_at" => null,
        ];
    }
    public function configure(): BorrowerFactory
    {
        return $this->afterMaking(function ($borrower) {
            if (!$borrower->user_id) {
                $user = User::factory()
                    ->withCommunity()
                    ->create();
                $borrower->user_id = $user->id;
            }
        });
    }

    public function approved(): BorrowerFactory
    {
        return $this->state([
            "approved_at" => date("Y-m-d"),
        ]);
    }
}
