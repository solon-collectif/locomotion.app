<?php
namespace Database\Factories;
use App\Enums\BillItemTypes;
use App\Models\BillItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillItemFactory extends Factory
{
    protected $model = BillItem::class;

    public function definition(): array
    {
        $amount = $this->faker->numberBetween(0, 300_000);

        return [
            "label" => $this->faker->word,
            "amount" => $amount,
            "taxes_tps" => $amount * 0.05,
            "taxes_tvq" => $amount * 0.09975,
            "item_type" => $this->faker->randomElement(BillItemTypes::cases()),
        ];
    }
}
