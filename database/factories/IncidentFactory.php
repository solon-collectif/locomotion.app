<?php
namespace Database\Factories;
use App\Models\Incident;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class IncidentFactory extends Factory
{
    protected $model = Incident::class;
    public function definition(): array
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
            "incident_type" => $this->faker->randomElement([
                "accident",
                "small_incident",
            ]),
            "comments_on_incident" => $this->faker->sentence,
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }
}
