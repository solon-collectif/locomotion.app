<?php

namespace Database\Factories;

use App\Enums\ReportStatus;
use App\Enums\ReportType;
use App\Models\PublishableReport;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PublishableReportFactory extends Factory
{
    protected $model = PublishableReport::class;

    public function definition(): array
    {
        return [
            "status" => ReportStatus::Generating,
            "type" => ReportType::YearlyOwnerIncome,
            "report_key" => $this->faker->uuid(),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ];
    }
}
