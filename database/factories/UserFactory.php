<?php
namespace Database\Factories;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        return [
            "accept_conditions" => true,
            "name" => $this->faker->firstName,
            "last_name" => $this->faker->lastName,
            "email" => $this->faker->unique()->safeEmail,
            "email_verified_at" => Carbon::now(),
            "password" =>
                '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            "description" => null,
            "date_of_birth" => "1992-01-13",
            "address" => $this->faker->address,
            "postal_code" => $this->faker->postCode,
            "phone" => $this->faker->numerify("+1 ### ### ####"),
            "is_smart_phone" => false,
            "other_phone" => "",
            "remember_token" => Str::random(10),
        ];
    }

    public function withBorrower(
        BorrowerFactory|Borrower|null $borrower = null
    ): UserFactory {
        return $this->has($borrower ?? Borrower::factory());
    }

    public function withCommunity(
        CommunityFactory|Community $communityFactory = null
    ): UserFactory {
        if (!$communityFactory) {
            $communityFactory = Community::factory();
        }

        return $this->hasAttached($communityFactory, [
            "approved_at" => new \DateTime(),
        ]);
    }

    public function adminOfCommunity(
        CommunityFactory|Community $communityFactory = null
    ): UserFactory {
        if (!$communityFactory) {
            $communityFactory = Community::factory();
        }

        return $this->hasAttached($communityFactory, [
            "approved_at" => new \DateTime(),
            "role" => "admin",
        ]);
    }

    public function withPaidCommunity(): UserFactory
    {
        return $this->has(Community::factory()->withDefault10DollarsPricing());
    }

    public function withFreeCommunity(): UserFactory
    {
        return $this->has(Community::factory()->withDefaultFreePricing());
    }
}
