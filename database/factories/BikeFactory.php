<?php
namespace Database\Factories;

use App\Models\Bike;
use Illuminate\Database\Eloquent\Factories\Factory;

class BikeFactory extends Factory
{
    protected $model = Bike::class;

    public function definition(): array
    {
        return [
            "model" => $this->faker->sentence,
            "bike_type" => $this->faker->randomElement([
                "regular",
                "cargo",
                "electric",
                "fixed_wheel",
            ]),
            "size" => $this->faker->randomElement([
                "big",
                "medium",
                "small",
                "kid",
            ]),
        ];
    }
}
