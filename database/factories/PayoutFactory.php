<?php

namespace Database\Factories;

use App\Models\Invoice;
use App\Models\Payout;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class PayoutFactory extends Factory
{
    protected $model = Payout::class;

    public function definition(): array
    {
        return [
            "kiwili_expense_id" => $this->faker->word(),
            "amount" => $this->faker->numberBetween(1, 200),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),

            "user_id" => User::factory(),
            "invoice_id" => Invoice::factory(),
        ];
    }
}
