<?php

namespace Database\Factories;

use App\Enums\PricingLoanableTypeValues;
use App\Models\Community;
use App\Models\Pivots\CommunityUser;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class SubscriptionFactory extends Factory
{
    protected $model = Subscription::class;

    public function definition(): array
    {
        return [
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "start_date" => Carbon::now()
                ->subDay()
                ->startOfDay(),
            "end_date" => Carbon::now()->addYear(),
            "type" => $this->faker->randomElement(["granted", "paid"]),

            "community_user_id" => CommunityUser::factory(),
            "granted_by_user_id" => User::factory(),
            "invoice_id" => null,
        ];
    }

    public function forCommunityUser(
        Community $community,
        User $user
    ): SubscriptionFactory {
        $communityUser = CommunityUser::where("community_id", $community->id)
            ->where("user_id", $user->id)
            ->firstOrFail();

        return $this->state([
            "community_user_id" => $communityUser,
        ]);
    }

    public function forType(
        PricingLoanableTypeValues $type
    ): SubscriptionFactory {
        return $this->afterCreating(
            fn(Subscription $subscription) => \DB::table(
                "subscription_loanable_types"
            )->insert([
                [
                    "subscription_id" => $subscription->id,
                    "pricing_loanable_type" => $type,
                ],
            ])
        );
    }
}
