<?php

namespace Database\Factories;

use App\Models\Community;
use App\Models\MailingListIntegration;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MailingListIntegrationFactory extends Factory
{
    protected $model = MailingListIntegration::class;

    public function definition(): array
    {
        return [
            "provider" => "mailchimp",
            "api_key" => $this->faker->word(),
            "list_id" => $this->faker->randomNumber(),
            "list_name" => $this->faker->word(),
            "status" => "active",
            "consecutive_error_count" => 0,
            "last_error" => null,
            "community_editable" => false,

            "community_id" => Community::factory(),
            "created_by_user_id" => User::factory(),
        ];
    }
}
