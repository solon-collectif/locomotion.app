<?php
namespace Database\Factories;
use App\Models\Community;
use App\Models\LoanableTypeDetails;
use App\Models\Pricing;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommunityFactory extends Factory
{
    protected $model = Community::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "description" => $this->faker->sentence,
            "area" => null,
        ];
    }

    public function configure()
    {
        return $this->afterCreating(
            fn(Community $community) => $community
                ->allowedLoanableTypes()
                ->sync(LoanableTypeDetails::all())
        );
    }

    public function withDefaultFreePricing(): CommunityFactory
    {
        return $this;
    }

    public function withDefault10DollarsPricing(): CommunityFactory
    {
        return $this->has(
            Pricing::factory()
                ->state(
                    fn(array $attributes, Community $community) => [
                        "rule" => "10",
                        "community_id" => $community->id,
                    ]
                )
                ->forAllTypes()
        );
    }

    public function withKMDollarsPricing(): CommunityFactory
    {
        return $this->has(
            Pricing::factory()
                ->state(
                    fn(array $attributes, Community $community) => [
                        "rule" => "\$KM",
                        "community_id" => $community->id,
                    ]
                )
                ->forAllTypes()
        );
    }
}
