<?php

namespace Database\Factories;

use App\Models\Community;
use App\Models\Invitation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class InvitationFactory extends Factory
{
    protected $model = Invitation::class;

    public function definition(): array
    {
        $communityFactory = Community::factory();
        return [
            "email" => $this->faker->unique()->safeEmail(),
            "inviter_user_id" => User::factory()->withCommunity(
                $communityFactory
            ),
            "community_id" => $communityFactory,
            "confirmation_code" => $this->faker->word(),
            "reason" => $this->faker->sentence(),
            "auto_approve" => $this->faker->boolean(),
            "expires_at" => Carbon::now()->addWeeks(4),
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
            "message" => $this->faker->paragraphs(asText: true),
            "validated_on_registration" => false,
        ];
    }
}
