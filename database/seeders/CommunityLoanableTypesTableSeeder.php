<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommunityLoanableTypesTableSeeder extends Seeder
{
    public function run(): void
    {
        $allCommunities = \DB::table("communities")->pluck("id");
        $types = \DB::table("loanable_type_details")->pluck("id");
        $rows = [];
        foreach ($allCommunities as $community) {
            foreach ($types as $type) {
                $rows[] = [
                    "community_id" => $community,
                    "loanable_type_id" => $type,
                ];
            }
        }

        \DB::table("community_loanable_types")->insertOrIgnore($rows);
    }
}
