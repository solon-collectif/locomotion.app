<?php

namespace Database\Seeders;

use App\Models\Loan;
use App\Models\LoanComment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LoansTableSeeder extends Seeder
{
    public function run()
    {
        Loan::factory()
            ->requested()
            ->has(
                LoanComment::factory([
                    "author_id" => 7,
                    "text" => "Bonjour, j'aimerais emprunter ce véhicule",
                ]),
                "comments"
            )
            ->create([
                "id" => 2,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->addHours(3)
                    ->addMinutes(15)
                    ->startOfHour()
                    ->toISOString(),
            ]);
        Loan::factory()
            ->accepted()
            ->create([
                "id" => 3,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "platform_tip" => null,
                "duration_in_minutes" => 45,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->addHours(2)
                    ->addMinutes(15)
                    ->startOfHour()
                    ->toISOString(),
            ]);
        Loan::factory()
            ->confirmed()
            ->create([
                "id" => 4,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->addHour()
                    ->startOfHour()
                    ->addMinutes(15)
                    ->toISOString(),
            ]);
        Loan::factory()
            ->ongoing()
            ->create([
                "id" => 5,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 60,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->startOfHour()
                    ->toISOString(),
            ]);
        Loan::factory()
            ->ended()
            ->create([
                "id" => 6,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->subHour()
                    ->startOfHour()
                    ->toISOString(),
            ]);
        Loan::factory()
            ->validated()
            ->create([
                "id" => 7,
                "borrower_user_id" => 7,
                "loanable_id" => 1001, // the car
                "duration_in_minutes" => 45,
                "estimated_distance" => 15,
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "departure_at" => Carbon::now("America/Toronto")
                    ->subHours(2)
                    ->startOfHour()
                    ->toISOString(),
            ]);

        \DB::statement(
            "SELECT setval('loans_id_seq'::regclass, (SELECT MAX(id) FROM loans) + 1)"
        );
    }
}
