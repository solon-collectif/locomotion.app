<?php

namespace Database\Seeders;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Pricing;
use Illuminate\Database\Seeder;

class PricingsTableSeeder extends Seeder
{
    public function run()
    {
        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->create([
                "pricing_type" => "price",
                "name" => "Tarif par défaut grosse auto LocoMotion",
                "rule" =>
                    "MAX(\$KM * 0.27 + PLANCHER(\$MINUTES / (60*24)) * 22.77 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (22.77/4), 15)",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->create([
                "pricing_type" => "price",
                "name" => "Tarif par défaut petite auto LocoMotion",
                "rule" =>
                    "MAX(\$KM * 0.19 + PLANCHER(\$MINUTES / (60*24)) * 15.62 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (15.62/4), 15)",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->create([
                "pricing_type" => "price",
                "name" => "Tarif par défaut petite auto électrique LocoMotion",
                "rule" =>
                    "MAX(\$KM * 0.08 + PLANCHER(\$MINUTES / (60*24)) * 14.66 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (14.66/4), 15)",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->create([
                "pricing_type" => "price",
                "name" => "Tarif par défaut grosse auto électrique LocoMotion",
                "rule" =>
                    "MAX(\$KM * 0.08 + PLANCHER(\$MINUTES / (60*24)) * 14.66 + MIN(((\$MINUTES - (PLANCHER(\$MINUTES / (60*24)) * (60*24))) / 60), 4) * (14.66/4), 15)",
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->create([
                "pricing_type" => "insurance",
                "name" => "Assurance Auto Desjardins",
                "rule" =>
                    "\$EMPRUNT.days * 4.16 * \$FACTEUR_PRIME_PAR_JOUR * 1.09",
            ]);

        Pricing::factory()
            ->forAllTypes()
            ->create([
                "pricing_type" => "contribution",
                "name" => "Contribution pour le Réseau LocoMotion",
                "rule" => "3",
                "yearly_target_per_user" => 12,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->create([
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "pricing_type" => "contribution",
                "name" => "Contribution auto pour la Petite Patrie",
                "rule" => "1",
                "yearly_target_per_user" => 3,
            ]);

        Pricing::factory()
            ->forType(PricingLoanableTypeValues::BikeElectric)
            ->forType(PricingLoanableTypeValues::BikeRegular)
            ->create([
                "community_id" => CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID,
                "pricing_type" => "contribution",
                "name" => "Contribution vélo pour la Petite Patrie",
                "rule" => "3",
                "yearly_target_per_user" => 9,
            ]);
    }
}
