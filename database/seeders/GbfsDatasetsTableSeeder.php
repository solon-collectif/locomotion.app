<?php

namespace Database\Seeders;

use App\Models\GbfsDataset;
use App\Models\GbfsDatasetCommunity;
use Illuminate\Database\Seeder;

class GbfsDatasetsTableSeeder extends Seeder
{
    public function run()
    {
        $gbfsDatasets = [
            [
                "name" => "montreal",
                "communities" => [8, 9, 15],
            ],
            [
                "name" => "sherbrooke",
                "communities" => [4],
            ],
        ];

        foreach ($gbfsDatasets as $gbfsDataset) {
            GbfsDataset::updateOrCreate(["name" => $gbfsDataset["name"]]);
            foreach ($gbfsDataset["communities"] as $communityId) {
                GbfsDatasetCommunity::updateOrCreate([
                    "gbfs_dataset_name" => $gbfsDataset["name"],
                    "community_id" => "$communityId",
                ]);
            }
        }
    }
}
