<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        switch (app()->environment()) {
            case "testing":
                $this->call(PassportSeeder::class);
                break;
            case "local":
            case "staging":
                $this->call(CommunitiesTableSeeder::class);
                $this->call(CommunityLoanableTypesTableSeeder::class);
                $this->call(UsersTableSeeder::class);

                $this->call(BikesTableSeeder::class);
                $this->call(CarsTableSeeder::class);
                $this->call(TrailersTableSeeder::class);
                $this->call(BorrowersTableSeeder::class);
                $this->call(PricingsTableSeeder::class);

                $this->call(LoanableUserRolesTableSeeder::class);
                $this->call(LoansTableSeeder::class);

                $this->call(SubscriptionsTableSeeder::class);

                $this->call(ImagesTableSeeder::class);
                $this->call(FilesTableSeeder::class);
                $this->call(GbfsDatasetsTableSeeder::class);
                break;
            case "production":
                //$this->call(CommunitiesTableSeeder::class);
                //$this->call(ProductionPricingsTableSeeder::class);
                //$this->call(AdminsTableSeeder::class);
                break;
        }

        if (app()->environment() === "local") {
            $this->call(PassportSeeder::class);
        }
    }
}
