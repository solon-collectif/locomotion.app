<?php

namespace Database\Seeders;
use App\Helpers\Path;
use App\Models\Image;
use DB;
use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    public function run()
    {
        $images = [
            [
                "id" => 2,
                "imageable_type" => "user",
                "imageable_id" => UsersTableSeeder::RPP_BORROWER_USER_SEED_ID,
                "path" => "seeds",
                "filename" => "user.png",
                "original_filename" => "user.png",
                "field" => "avatar",
                "width" => "256",
                "height" => "256",
                "filesize" => 5865,
            ],
            [
                "id" => 3,
                "imageable_type" => "loanable",
                "imageable_id" => 1,
                "path" => "seeds",
                "filename" => "trailer-bike.jpeg",
                "original_filename" => "trailer-bike.jpeg",
                "field" => "image",
                "width" => "1008",
                "height" => "756",
                "filesize" => 231342,
            ],
            [
                "id" => 4,
                "imageable_type" => "user",
                "imageable_id" => UsersTableSeeder::RPP_OWNER_USER_SEED_ID,
                "path" => "seeds",
                "filename" => "user2.jpg",
                "original_filename" => "user2.jpg",
                "field" => "avatar",
                "width" => "1630",
                "height" => "2048",
                "filesize" => 1819187,
            ],
            [
                "id" => 5,
                "imageable_type" => "loanable",
                "imageable_id" => CarsTableSeeder::$RPP_OWNER_CAR_ID,
                "path" => "seeds",
                "filename" => "car.jpg",
                "original_filename" => "car.jpg",
                "field" => "image",
                "width" => "440",
                "height" => "342",
                "filesize" => 30534,
            ],
        ];

        foreach ($images as $image) {
            // Store the seeds images
            \Storage::cloud()->putFileAs(
                $image["path"],
                storage_path(Path::join($image["path"], $image["filename"])),
                $image["filename"]
            );

            if (!Image::where("id", $image["id"])->exists()) {
                Image::create($image);
            } else {
                Image::where("id", $image["id"])->update($image);
            }
        }

        DB::statement(
            "SELECT setval('images_id_seq'::regclass, (SELECT MAX(id) FROM images) + 1)"
        );
    }
}
