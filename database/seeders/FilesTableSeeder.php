<?php

namespace Database\Seeders;
use App\Helpers\Path;
use App\Models\File;
use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    public function run()
    {
        $files = [
            [
                "id" => 1,
                "fileable_type" => "user",
                "fileable_id" => UsersTableSeeder::RPP_OWNER_USER_SEED_ID,
                "path" => "seeds",
                "filename" => "preuve de residence.txt",
                "original_filename" =>
                    "preuve de résidence pour Proprietaire Petite Patrie.txt ",
                "field" => "residency_proof",
                "filesize" => 3817,
            ],
            [
                "id" => 2,
                "fileable_type" => "user",
                "fileable_id" => UsersTableSeeder::RPP_BORROWER_USER_SEED_ID,
                "path" => "seeds",
                "filename" => "preuve de residence.txt",
                "original_filename" =>
                    "preuve de résidence pour Emprunteur Petite patrie.txt",
                "field" => "residency_proof",
                "filesize" => 3817,
            ],
        ];

        foreach ($files as $file) {
            // Store the seeds files
            \Storage::cloud()->put(
                Path::join($file["path"], $file["filename"]),
                "Une belle preuve : " . $file["original_filename"]
            );

            if (!File::where("id", $file["id"])->exists()) {
                File::create($file);
            } else {
                File::where("id", $file["id"])->update($file);
            }
        }

        \DB::statement(
            "SELECT setval('files_id_seq'::regclass, (SELECT MAX(id) FROM images) + 1)"
        );
    }
}
