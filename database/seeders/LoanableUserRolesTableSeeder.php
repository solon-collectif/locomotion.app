<?php

namespace Database\Seeders;

use App\Enums\LoanableUserRoles;
use App\Models\LoanableUserRole;
use Illuminate\Database\Seeder;

class LoanableUserRolesTableSeeder extends Seeder
{
    public function run()
    {
        // Comencer par id, piuis loanable id, puis user id ,pour indiquer que c<wst loanable en premier en ce qiu concerne les owners.
        //              $this->call(CarsTableSeeder::class);
        //              $this->call(TrailersTableSeeder::class);

        $loanableUserRoles = [
            [
                "ressource_id" => 1,
                "ressource_type" => "loanable",
                // solonpetitepatrie@locomotion.app
                "user_id" => 3,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2,
                "ressource_type" => "loanable",
                // solonahuntsic@locomotion.app
                "user_id" => 2,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 3,
                "ressource_type" => "loanable",
                // solonpetitepatrie@locomotion.app
                "user_id" => 3,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 101,
                "ressource_type" => "loanable",
                // proprietairepetitepatrie@locomotion.app
                "user_id" => 6,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 102,
                "ressource_type" => "loanable",
                // proprietairepetitepatrie@locomotion.app
                "user_id" => 6,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => CarsTableSeeder::$RPP_OWNER_CAR_ID,
                "ressource_type" => "loanable",
                // proprietairepetitepatrie@locomotion.app
                "user_id" => 6,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => CarsTableSeeder::$RPP_OWNER_CAR_ID,
                "ressource_type" => "loanable",
                "user_id" => UsersTableSeeder::RPP_COOWNER_USER_SEED_ID,
                "role" => LoanableUserRoles::Coowner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2001,
                "ressource_type" => "loanable",
                // solonpetitepatrie@locomotion.app
                "user_id" => 3,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2002,
                "ressource_type" => "loanable",
                // solonahuntsic@locomotion.app
                "user_id" => 2,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2003,
                "ressource_type" => "loanable",
                // solonpetitepatrie@locomotion.app
                "user_id" => 3,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2101,
                "ressource_type" => "loanable",
                // proprietairepetitepatrie@locomotion.app
                "user_id" => 6,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
            [
                "ressource_id" => 2102,
                "ressource_type" => "loanable",
                // proprietairepetitepatrie@locomotion.app
                "user_id" => 6,
                "role" => LoanableUserRoles::Owner,
                "show_as_contact" => true,
            ],
        ];

        foreach ($loanableUserRoles as $role) {
            LoanableUserRole::create($role);
        }
    }
}
