<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class PassportSeeder extends Seeder
{
    public function run()
    {
        // Only for developpement purposes. Personal tokens are used for 'mandating' users.
        if (\DB::table("oauth_personal_access_clients")->count() == 0) {
            \Artisan::call("passport:client", [
                "--personal" => true,
                "--name" => "dev personal access client",
            ]);
        }

        // Only for development purposes, do not use in production
        // Prefer output of a command like `php artisan passport:install`

        \DB::statement(
            "INSERT INTO oauth_clients
(id, name, secret, redirect, personal_access_client, password_client, revoked,
    created_at, updated_at)
VALUES (
    '" .
                config("auth.client.id") .
                "',
    'LocoMotion Password Grant Client',
    '" .
                config("auth.client.secret") .
                "',
    'http://localhost',
    false,
    true,
    false,
    current_timestamp,
    current_timestamp
) ON CONFLICT DO NOTHING"
        );
    }
}
