<?php

namespace Database\Seeders;
use App\Enums\PricingLoanableTypeValues;
use App\Models\Pivots\CommunityUser;
use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    public function run()
    {
        $communityUser = CommunityUser::where(
            "community_id",
            CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID
        )
            ->where(
                "user_id",
                UsersTableSeeder::RPP_COMMUNITY_ADMIN_USER_SEED_ID
            )
            ->firstOrFail();

        Subscription::factory()
            ->forType(PricingLoanableTypeValues::CarLarge)
            ->forType(PricingLoanableTypeValues::CarSmall)
            ->forType(PricingLoanableTypeValues::CarLargeElectric)
            ->forType(PricingLoanableTypeValues::CarSmallElectric)
            ->create([
                "community_user_id" => $communityUser,
                "granted_by_user_id" =>
                    UsersTableSeeder::RPP_BORROWER_USER_SEED_ID,
                "type" => "paid",
            ]);
    }
}
